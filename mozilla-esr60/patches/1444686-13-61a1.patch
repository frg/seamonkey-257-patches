# HG changeset patch
# User Boris Zbarsky <bzbarsky@mit.edu>
# Date 1520972640 14400
# Node ID 6da907cf2ab00ecc86f8c719c6047fdd804595a6
# Parent  fa85d42806e38b6e9896da2e56e856ed25595bf7
Bug 1444686 part 13.  Remove remaining nsIDOMDataTransfer uses.  r=mystor

MozReview-Commit-ID: EFauqLMGz5S

diff --git a/dom/events/DataTransfer.h b/dom/events/DataTransfer.h
--- a/dom/events/DataTransfer.h
+++ b/dom/events/DataTransfer.h
@@ -55,20 +55,16 @@ class DataTransfer final : public nsIDOM
 
   NS_DECL_CYCLE_COLLECTING_ISUPPORTS
   NS_DECL_NSIDOMDATATRANSFER
 
   NS_DECL_CYCLE_COLLECTION_SCRIPT_HOLDER_CLASS(DataTransfer)
 
   friend class mozilla::EventStateManager;
 
-  static DataTransfer* Cast(nsIDOMDataTransfer* aArg) {
-    return static_cast<DataTransfer*>(aArg);
-  }
-
   /// An enum which represents which "Drag Data Store Mode" the DataTransfer is
   /// in according to the spec.
   enum class Mode : uint8_t {
     ReadWrite,
     ReadOnly,
     Protected,
   };
 
diff --git a/dom/events/DataTransferItemList.cpp b/dom/events/DataTransferItemList.cpp
--- a/dom/events/DataTransferItemList.cpp
+++ b/dom/events/DataTransferItemList.cpp
@@ -217,23 +217,23 @@ already_AddRefed<FileList> DataTransferI
   //
   // It is not legal to expose an identical DataTransfer object is to multiple
   // different principals without using the `Clone` method or similar to copy it
   // first. If that happens, this method will assert, and return nullptr in
   // release builds. If this functionality is required in the future, a more
   // advanced caching mechanism for the FileList objects will be required.
   RefPtr<FileList> files;
   if (nsContentUtils::IsSystemPrincipal(aPrincipal)) {
-    files = new FileList(static_cast<nsIDOMDataTransfer*>(mDataTransfer));
+    files = new FileList(mDataTransfer);
     GenerateFiles(files, aPrincipal);
     return files.forget();
   }
 
   if (!mFiles) {
-    mFiles = new FileList(static_cast<nsIDOMDataTransfer*>(mDataTransfer));
+    mFiles = new FileList(mDataTransfer);
     mFilesPrincipal = aPrincipal;
     RegenerateFiles();
   }
 
   if (!aPrincipal->Subsumes(mFilesPrincipal)) {
     MOZ_ASSERT(false,
                "This DataTransfer should only be accessed by the system "
                "and a single principal");
diff --git a/editor/libeditor/HTMLEditorDataTransfer.cpp b/editor/libeditor/HTMLEditorDataTransfer.cpp
--- a/editor/libeditor/HTMLEditorDataTransfer.cpp
+++ b/editor/libeditor/HTMLEditorDataTransfer.cpp
@@ -1133,22 +1133,22 @@ nsresult HTMLEditor::InsertFromTransfera
 
   // Try to scroll the selection into view if the paste succeeded
   if (NS_SUCCEEDED(rv)) {
     ScrollSelectionIntoView(false);
   }
   return rv;
 }
 
-static void GetStringFromDataTransfer(nsIDOMDataTransfer* aDataTransfer,
+static void GetStringFromDataTransfer(DataTransfer* aDataTransfer,
                                       const nsAString& aType, int32_t aIndex,
                                       nsAString& aOutputString) {
   nsCOMPtr<nsIVariant> variant;
-  DataTransfer::Cast(aDataTransfer)
-      ->GetDataAtNoSecurityCheck(aType, aIndex, getter_AddRefs(variant));
+  aDataTransfer->GetDataAtNoSecurityCheck(aType, aIndex,
+                                          getter_AddRefs(variant));
   if (variant) {
     variant->GetAsAString(aOutputString);
   }
 }
 
 nsresult HTMLEditor::InsertFromDataTransfer(DataTransfer* aDataTransfer,
                                             int32_t aIndex,
                                             nsIDOMDocument* aSourceDoc,
@@ -1173,18 +1173,18 @@ nsresult HTMLEditor::InsertFromDataTrans
     types->Item(t, type);
 
     if (!isText) {
       if (type.EqualsLiteral(kFileMime) || type.EqualsLiteral(kJPEGImageMime) ||
           type.EqualsLiteral(kJPGImageMime) ||
           type.EqualsLiteral(kPNGImageMime) ||
           type.EqualsLiteral(kGIFImageMime)) {
         nsCOMPtr<nsIVariant> variant;
-        DataTransfer::Cast(aDataTransfer)
-            ->GetDataAtNoSecurityCheck(type, aIndex, getter_AddRefs(variant));
+        aDataTransfer->GetDataAtNoSecurityCheck(type, aIndex,
+                                                getter_AddRefs(variant));
         if (variant) {
           nsCOMPtr<nsISupports> object;
           variant->GetAsISupports(getter_AddRefs(object));
           return InsertObject(NS_ConvertUTF16toUTF8(type), object, isSafe,
                               aSourceDoc, aDestinationNode, aDestOffset,
                               aDoDeleteSelection);
         }
       } else if (type.EqualsLiteral(kNativeHTMLMime)) {
diff --git a/editor/libeditor/TextEditorDataTransfer.cpp b/editor/libeditor/TextEditorDataTransfer.cpp
--- a/editor/libeditor/TextEditorDataTransfer.cpp
+++ b/editor/libeditor/TextEditorDataTransfer.cpp
@@ -134,19 +134,18 @@ nsresult TextEditor::InsertTextFromTrans
 
 nsresult TextEditor::InsertFromDataTransfer(DataTransfer* aDataTransfer,
                                             int32_t aIndex,
                                             nsIDOMDocument* aSourceDoc,
                                             nsIDOMNode* aDestinationNode,
                                             int32_t aDestOffset,
                                             bool aDoDeleteSelection) {
   nsCOMPtr<nsIVariant> data;
-  DataTransfer::Cast(aDataTransfer)
-      ->GetDataAtNoSecurityCheck(NS_LITERAL_STRING("text/plain"), aIndex,
-                                 getter_AddRefs(data));
+  aDataTransfer->GetDataAtNoSecurityCheck(NS_LITERAL_STRING("text/plain"),
+                                          aIndex, getter_AddRefs(data));
   if (data) {
     nsAutoString insertText;
     data->GetAsAString(insertText);
     nsContentUtils::PlatformToDOMLineBreaks(insertText);
 
     AutoPlaceholderBatch beginBatching(this);
     return InsertTextAt(insertText, aDestinationNode, aDestOffset,
                         aDoDeleteSelection);
diff --git a/widget/nsBaseDragService.cpp b/widget/nsBaseDragService.cpp
--- a/widget/nsBaseDragService.cpp
+++ b/widget/nsBaseDragService.cpp
@@ -426,19 +426,19 @@ nsBaseDragService::EndDragSession(bool a
   mEndDragPoint = LayoutDeviceIntPoint(0, 0);
   mInputSource = nsIDOMMouseEvent::MOZ_SOURCE_MOUSE;
 
   return NS_OK;
 }
 
 void nsBaseDragService::DiscardInternalTransferData() {
   if (mDataTransfer && mSourceNode) {
-    MOZ_ASSERT(!!DataTransfer::Cast(mDataTransfer));
+    MOZ_ASSERT(mDataTransfer);
 
-    DataTransferItemList* items = DataTransfer::Cast(mDataTransfer)->Items();
+    DataTransferItemList* items = mDataTransfer->Items();
     for (size_t i = 0; i < items->Length(); i++) {
       bool found;
       DataTransferItem* item = items->IndexedGetter(i, found);
 
       // Non-OTHER items may still be needed by JS. Skip them.
       if (!found || item->Kind() != DataTransferItem::KIND_OTHER) {
         continue;
       }
diff --git a/widget/windows/nsDragService.cpp b/widget/windows/nsDragService.cpp
--- a/widget/windows/nsDragService.cpp
+++ b/widget/windows/nsDragService.cpp
@@ -298,18 +298,17 @@ nsDragService::StartInvokingDragSession(
   } else {
     NS_NOTREACHED("When did our data object stop being async");
   }
 
   // Call the native D&D method
   HRESULT res = ::DoDragDrop(aDataObj, nativeDragSrc, effects, &winDropRes);
 
   // In  cases where the drop operation completed outside the application,
-  // update the source node's nsIDOMDataTransfer dropEffect value so it is up to
-  // date.
+  // update the source node's DataTransfer dropEffect value so it is up to date.
   if (!mSentLocalDropEvent) {
     uint32_t dropResult;
     // Order is important, since multiple flags can be returned.
     if (winDropRes & DROPEFFECT_COPY)
       dropResult = DRAGDROP_ACTION_COPY;
     else if (winDropRes & DROPEFFECT_LINK)
       dropResult = DRAGDROP_ACTION_LINK;
     else if (winDropRes & DROPEFFECT_MOVE)
diff --git a/widget/windows/nsNativeDragSource.h b/widget/windows/nsNativeDragSource.h
--- a/widget/windows/nsNativeDragSource.h
+++ b/widget/windows/nsNativeDragSource.h
@@ -1,17 +1,16 @@
 /* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 #ifndef _nsNativeDragSource_h_
 #define _nsNativeDragSource_h_
 
 #include "nscore.h"
-#include "nsIDOMDataTransfer.h"
 #include <ole2.h>
 #include <oleidl.h>
 #include "mozilla/Attributes.h"
 #include "mozilla/RefPtr.h"
 
 namespace mozilla {
 namespace dom {
 class DataTransfer;
