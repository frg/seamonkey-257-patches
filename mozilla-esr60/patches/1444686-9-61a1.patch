# HG changeset patch
# User Boris Zbarsky <bzbarsky@mit.edu>
# Date 1520972640 14400
# Node ID e1cd8692d296c47d3f0ebaa098d254dfef6c2cfb
# Parent  916cdd0e77edd0c4d0f70e2a06d1491edbd09172
Bug 1444686 part 9.  Remove use of nsIDOMDataTransfer from nsITreeView.  r=mystor

MozReview-Commit-ID: Dpn7YSZpDsc

diff --git a/layout/inspector/inDOMView.cpp b/layout/inspector/inDOMView.cpp
--- a/layout/inspector/inDOMView.cpp
+++ b/layout/inspector/inDOMView.cpp
@@ -505,24 +505,24 @@ inDOMView::IsSelectable(int32_t row, nsI
 NS_IMETHODIMP
 inDOMView::IsSeparator(int32_t index, bool* _retval) { return NS_OK; }
 
 NS_IMETHODIMP
 inDOMView::IsSorted(bool* _retval) { return NS_OK; }
 
 NS_IMETHODIMP
 inDOMView::CanDrop(int32_t index, int32_t orientation,
-                   nsIDOMDataTransfer* aDataTransfer, bool* _retval) {
+                   nsISupports* aDataTransfer, bool *_retval) {
   *_retval = false;
   return NS_OK;
 }
 
 NS_IMETHODIMP
 inDOMView::Drop(int32_t row, int32_t orientation,
-                nsIDOMDataTransfer* aDataTransfer) {
+                nsISupports* aDataTransfer) {
   return NS_OK;
 }
 
 NS_IMETHODIMP
 inDOMView::PerformAction(const char16_t* action) { return NS_OK; }
 
 NS_IMETHODIMP
 inDOMView::PerformActionOnRow(const char16_t* action, int32_t row) {
diff --git a/layout/xul/tree/nsITreeView.idl b/layout/xul/tree/nsITreeView.idl
--- a/layout/xul/tree/nsITreeView.idl
+++ b/layout/xul/tree/nsITreeView.idl
@@ -3,17 +3,16 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "nsISupports.idl"
 
 interface nsITreeBoxObject;
 interface nsITreeSelection;
 interface nsITreeColumn;
-interface nsIDOMDataTransfer;
 
 [scriptable, uuid(091116f0-0bdc-4b32-b9c8-c8d5a37cb088)]
 interface nsITreeView : nsISupports
 {
   /**
    * The total number of rows in the tree (including the offscreen rows).
    */
   readonly attribute long rowCount;
@@ -71,24 +70,28 @@ interface nsITreeView : nsISupports
   const short DROP_BEFORE = -1;
   const short DROP_ON = 0;
   const short DROP_AFTER = 1;
   /**
    * Methods used by the drag feedback code to determine if a drag is allowable at
    * the current location. To get the behavior where drops are only allowed on
    * items, such as the mailNews folder pane, always return false when
    * the orientation is not DROP_ON.
+   *
+   * @param dataTransfer should be a DataTransfer once bug 1444991 is fixed.
    */
-  boolean canDrop(in long index, in long orientation, in nsIDOMDataTransfer dataTransfer);
+  boolean canDrop(in long index, in long orientation, in nsISupports dataTransfer);
 
   /**
    * Called when the user drops something on this view. The |orientation| param
    * specifies before/on/after the given |row|.
+   *
+   * @param dataTransfer should be a DataTransfer once bug 1444991 is fixed.
    */
-  void drop(in long row, in long orientation, in nsIDOMDataTransfer dataTransfer);
+  void drop(in long row, in long orientation, in nsISupports dataTransfer);
 
   /**
    * Methods used by the tree to draw thread lines in the tree.
    * getParentIndex is used to obtain the index of a parent row.
    * If there is no parent row, getParentIndex returns -1.
    */
   long getParentIndex(in long rowIndex);
 
diff --git a/layout/xul/tree/nsTreeContentView.cpp b/layout/xul/tree/nsTreeContentView.cpp
--- a/layout/xul/tree/nsTreeContentView.cpp
+++ b/layout/xul/tree/nsTreeContentView.cpp
@@ -299,45 +299,54 @@ nsTreeContentView::IsSeparator(int32_t a
 NS_IMETHODIMP
 nsTreeContentView::IsSorted(bool* _retval) {
   *_retval = IsSorted();
 
   return NS_OK;
 }
 
 bool nsTreeContentView::CanDrop(int32_t aRow, int32_t aOrientation,
-                                DataTransfer* aDataTransfer,
                                 ErrorResult& aError) {
   if (!IsValidRowIndex(aRow)) {
     aError.Throw(NS_ERROR_INVALID_ARG);
   }
   return false;
 }
 
+bool nsTreeContentView::CanDrop(int32_t aRow, int32_t aOrientation,
+                                DataTransfer* aDataTransfer,
+                                ErrorResult& aError) {
+  return CanDrop(aRow, aOrientation, aError);
+}
+
 NS_IMETHODIMP
 nsTreeContentView::CanDrop(int32_t aIndex, int32_t aOrientation,
-                           nsIDOMDataTransfer* aDataTransfer, bool* _retval) {
+                           nsISupports* aDataTransfer, bool *_retval) {
   ErrorResult rv;
-  *_retval =
-      CanDrop(aIndex, aOrientation, DataTransfer::Cast(aDataTransfer), rv);
+  *_retval = CanDrop(aIndex, aOrientation, rv);
   return rv.StealNSResult();
 }
 
 void nsTreeContentView::Drop(int32_t aRow, int32_t aOrientation,
-                             DataTransfer* aDataTransfer, ErrorResult& aError) {
+                             ErrorResult& aError) {
   if (!IsValidRowIndex(aRow)) {
     aError.Throw(NS_ERROR_INVALID_ARG);
   }
 }
 
+void nsTreeContentView::Drop(int32_t aRow, int32_t aOrientation,
+                             DataTransfer* aDataTransfer, ErrorResult& aError) {
+  Drop(aRow, aOrientation, aError);
+}
+
 NS_IMETHODIMP
 nsTreeContentView::Drop(int32_t aRow, int32_t aOrientation,
-                        nsIDOMDataTransfer* aDataTransfer) {
+                        nsISupports* aDataTransfer) {
   ErrorResult rv;
-  Drop(aRow, aOrientation, DataTransfer::Cast(aDataTransfer), rv);
+  Drop(aRow, aOrientation, rv);
   return rv.StealNSResult();
 }
 
 int32_t nsTreeContentView::GetParentIndex(int32_t aRow, ErrorResult& aError) {
   if (!IsValidRowIndex(aRow)) {
     aError.Throw(NS_ERROR_INVALID_ARG);
     return 0;
   }
diff --git a/layout/xul/tree/nsTreeContentView.h b/layout/xul/tree/nsTreeContentView.h
--- a/layout/xul/tree/nsTreeContentView.h
+++ b/layout/xul/tree/nsTreeContentView.h
@@ -145,16 +145,20 @@ class nsTreeContentView final : public n
   void CloseContainer(int32_t aIndex);
 
   int32_t FindContent(nsIContent* aContent);
 
   void UpdateSubtreeSizes(int32_t aIndex, int32_t aCount);
 
   void UpdateParentIndexes(int32_t aIndex, int32_t aSkip, int32_t aCount);
 
+  bool CanDrop(int32_t aRow, int32_t aOrientation,
+               mozilla::ErrorResult& aError);
+  void Drop(int32_t aRow, int32_t aOrientation, mozilla::ErrorResult& aError);
+
   // Content helpers.
   mozilla::dom::Element* GetCell(nsIContent* aContainer, nsTreeColumn& aCol);
 
  private:
   bool IsValidRowIndex(int32_t aRowIndex);
 
   nsCOMPtr<nsITreeBoxObject> mBoxObject;
   nsCOMPtr<nsITreeSelection> mSelection;
diff --git a/security/manager/pki/nsASN1Tree.cpp b/security/manager/pki/nsASN1Tree.cpp
--- a/security/manager/pki/nsASN1Tree.cpp
+++ b/security/manager/pki/nsASN1Tree.cpp
@@ -326,24 +326,24 @@ NS_IMETHODIMP
 nsNSSASN1Tree::PerformActionOnRow(const char16_t*, int32_t) { return NS_OK; }
 
 NS_IMETHODIMP
 nsNSSASN1Tree::PerformActionOnCell(const char16_t*, int32_t, nsITreeColumn*) {
   return NS_OK;
 }
 
 NS_IMETHODIMP
-nsNSSASN1Tree::CanDrop(int32_t, int32_t, nsIDOMDataTransfer*, bool* _retval) {
+nsNSSASN1Tree::CanDrop(int32_t, int32_t, nsISupports*, bool* _retval) {
   NS_ENSURE_ARG_POINTER(_retval);
   *_retval = false;
   return NS_OK;
 }
 
 NS_IMETHODIMP
-nsNSSASN1Tree::Drop(int32_t, int32_t, nsIDOMDataTransfer*) { return NS_OK; }
+nsNSSASN1Tree::Drop(int32_t, int32_t, nsISupports*) { return NS_OK; }
 
 NS_IMETHODIMP
 nsNSSASN1Tree::IsSorted(bool* _retval) {
   NS_ENSURE_ARG_POINTER(_retval);
   *_retval = false;
   return NS_OK;
 }
 
diff --git a/security/manager/ssl/nsCertTree.cpp b/security/manager/ssl/nsCertTree.cpp
--- a/security/manager/ssl/nsCertTree.cpp
+++ b/security/manager/ssl/nsCertTree.cpp
@@ -1087,29 +1087,29 @@ void nsCertTree::dumpMap() {
   }
 }
 #endif
 
 //
 // CanDrop
 //
 NS_IMETHODIMP nsCertTree::CanDrop(int32_t index, int32_t orientation,
-                                  nsIDOMDataTransfer *aDataTransfer,
+                                  nsISupports* aDataTransfer,
                                   bool *_retval) {
   NS_ENSURE_ARG_POINTER(_retval);
   *_retval = false;
 
   return NS_OK;
 }
 
 //
 // Drop
 //
 NS_IMETHODIMP nsCertTree::Drop(int32_t row, int32_t orient,
-                               nsIDOMDataTransfer *aDataTransfer) {
+                               nsISupports* aDataTransfer) {
   return NS_OK;
 }
 
 //
 // IsSorted
 //
 // ...
 //
