# HG changeset patch
# User Jeff Walden <jwalden@mit.edu>
# Date 1530242067 25200
# Node ID 234fc6b955c94397b0d17f1b9b273315b91b92bc
# Parent  238907bff5b3be9a7782b40ee191c674538d2344
Bug 1472066 - Specialize TokenStreamCharsBase::fillCharBufferWithTemplateStringContents for char16_t now that its alternative UTF-8 implementation will have to function a bit differently to write data into a char16_t charBuffer.  r=arai

diff --git a/js/src/frontend/TokenStream.h b/js/src/frontend/TokenStream.h
--- a/js/src/frontend/TokenStream.h
+++ b/js/src/frontend/TokenStream.h
@@ -1148,52 +1148,57 @@ class TokenStreamCharsBase : public Toke
     MOZ_ASSERT(!sourceUnits.atEnd(), "must have units to consume");
 #ifdef DEBUG
     CharT next =
 #endif
         sourceUnits.getCodeUnit();
     MOZ_ASSERT(next == unit, "must be consuming the correct unit");
   }
 
-  MOZ_MUST_USE bool fillCharBufferWithTemplateStringContents(const CharT* cur,
-                                                             const CharT* end) {
-    while (cur < end) {
-      // U+2028 LINE SEPARATOR and U+2029 PARAGRAPH SEPARATOR are
-      // interpreted literally inside template literal contents; only
-      // literal CRLF sequences are normalized to '\n'.  See
-      // <https://tc39.github.io/ecma262/#sec-static-semantics-tv-and-trv>.
-      CharT ch = *cur;
-      if (ch == '\r') {
-        ch = '\n';
-        if ((cur + 1 < end) && (*(cur + 1) == '\n'))
-          cur++;
-      }
-
-      if (!charBuffer.append(ch))
-        return false;
-
-      cur++;
-    }
-
-    return true;
-  }
+  MOZ_MUST_USE inline bool fillCharBufferWithTemplateStringContents(
+      const CharT* cur, const CharT* end);
 
  protected:
   /** Code units in the source code being tokenized. */
   SourceUnits sourceUnits;
 };
 
 template <>
 /* static */ MOZ_ALWAYS_INLINE JSAtom*
 TokenStreamCharsBase<char16_t>::atomizeSourceChars(JSContext* cx,
                                                    const char16_t* chars,
                                                    size_t length) {
   return AtomizeChars(cx, chars, length);
 }
 
+template<>
+MOZ_MUST_USE inline bool
+TokenStreamCharsBase<char16_t>::fillCharBufferWithTemplateStringContents(
+    const char16_t* cur, const char16_t* end) {
+  MOZ_ASSERT(charBuffer.length() == 0);
+
+  while (cur < end) {
+    // U+2028 LINE SEPARATOR and U+2029 PARAGRAPH SEPARATOR are
+    // interpreted literally inside template literal contents; only
+    // literal CRLF sequences are normalized to '\n'.  See
+    // <https://tc39.github.io/ecma262/#sec-static-semantics-tv-and-trv>.
+    char16_t ch = *cur++;
+    if (ch == '\r') {
+      ch = '\n';
+      if (cur < end && *cur == '\n')
+        cur++;
+    }
+
+    if (!charBuffer.append(ch))
+      return false;
+  }
+
+  return true;
+}
+
 /** A small class encapsulating computation of the start-offset of a Token. */
 class TokenStart {
   uint32_t startOffset_;
 
  public:
   /**
    * Compute a starting offset that is the current offset of |sourceUnits|,
    * offset by |adjust|.  (For example, |adjust| of -1 indicates the code
