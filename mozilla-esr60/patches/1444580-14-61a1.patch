# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1520769841 -3600
# Node ID 66f34961b2a09b4782aa2568e8c5c5512bd9973c
# Parent  f2a082746f436d3e794c93eee3e2ca06cd1f4ce0
Bug 1444580: Devirtualize the responsive content stuff. r=smaug

MozReview-Commit-ID: LCz01o31RoC

diff --git a/dom/base/nsDocument.cpp b/dom/base/nsDocument.cpp
--- a/dom/base/nsDocument.cpp
+++ b/dom/base/nsDocument.cpp
@@ -9086,35 +9086,20 @@ static bool AllSubDocumentPluginEnum(nsI
 void nsDocument::GetPlugins(nsTArray<nsIObjectLoadingContent*>& aPlugins) {
   aPlugins.SetCapacity(aPlugins.Length() + mPlugins.Count());
   for (auto iter = mPlugins.ConstIter(); !iter.Done(); iter.Next()) {
     aPlugins.AppendElement(iter.Get()->GetKey());
   }
   EnumerateSubDocuments(AllSubDocumentPluginEnum, &aPlugins);
 }
 
-nsresult nsDocument::AddResponsiveContent(nsIContent* aContent) {
-  MOZ_ASSERT(aContent);
-  MOZ_ASSERT(aContent->IsHTMLElement(nsGkAtoms::img));
-  mResponsiveContent.PutEntry(aContent);
-  return NS_OK;
-}
-
-void nsDocument::RemoveResponsiveContent(nsIContent* aContent) {
-  MOZ_ASSERT(aContent);
-  mResponsiveContent.RemoveEntry(aContent);
-}
-
-void nsDocument::NotifyMediaFeatureValuesChanged() {
+void nsIDocument::NotifyMediaFeatureValuesChanged() {
   for (auto iter = mResponsiveContent.ConstIter(); !iter.Done(); iter.Next()) {
-    nsCOMPtr<nsIContent> content = iter.Get()->GetKey();
-    if (content->IsHTMLElement(nsGkAtoms::img)) {
-      auto* imageElement = static_cast<HTMLImageElement*>(content.get());
-      imageElement->MediaFeatureValuesChanged();
-    }
+    RefPtr<HTMLImageElement> imageElement = iter.Get()->GetKey();
+    imageElement->MediaFeatureValuesChanged();
   }
 }
 
 already_AddRefed<Touch> nsIDocument::CreateTouch(
     nsGlobalWindowInner* aView, EventTarget* aTarget, int32_t aIdentifier,
     int32_t aPageX, int32_t aPageY, int32_t aScreenX, int32_t aScreenY,
     int32_t aClientX, int32_t aClientY, int32_t aRadiusX, int32_t aRadiusY,
     float aRotationAngle, float aForce) {
diff --git a/dom/base/nsDocument.h b/dom/base/nsDocument.h
--- a/dom/base/nsDocument.h
+++ b/dom/base/nsDocument.h
@@ -607,26 +607,16 @@ class nsDocument : public nsIDocument,
   // RemovePlugin removes a plugin-related element to mPlugins when the
   // element is removed from the tree.
   virtual void RemovePlugin(nsIObjectLoadingContent* aPlugin) override;
   // GetPlugins returns the plugin-related elements from
   // the frame and any subframes.
   virtual void GetPlugins(
       nsTArray<nsIObjectLoadingContent*>& aPlugins) override;
 
-  // Adds an element to mResponsiveContent when the element is
-  // added to the tree.
-  virtual nsresult AddResponsiveContent(nsIContent* aContent) override;
-  // Removes an element from mResponsiveContent when the element is
-  // removed from the tree.
-  virtual void RemoveResponsiveContent(nsIContent* aContent) override;
-  // Notifies any responsive content added by AddResponsiveContent upon media
-  // features values changing.
-  virtual void NotifyMediaFeatureValuesChanged() override;
-
   virtual nsresult GetStateObject(nsIVariant** aResult) override;
 
   // Returns the size of the mBlockedTrackingNodes array. (nsIDocument.h)
   //
   // This array contains nodes that have been blocked to prevent
   // user tracking. They most likely have had their nsIChannel
   // canceled by the URL classifier (Safebrowsing).
   //
@@ -799,19 +789,16 @@ class nsDocument : public nsIDocument,
   nsCOMPtr<nsILayoutHistoryState> mLayoutHistoryState;
 
   // Currently active onload blockers
   uint32_t mOnloadBlockCount;
   // Onload blockers which haven't been activated yet
   uint32_t mAsyncOnloadBlockCount;
   nsCOMPtr<nsIRequest> mOnloadBlocker;
 
-  // A set of responsive images keyed by address pointer.
-  nsTHashtable<nsPtrHashKey<nsIContent>> mResponsiveContent;
-
   nsTArray<RefPtr<nsFrameLoader>> mInitializableFrameLoaders;
   nsTArray<nsCOMPtr<nsIRunnable>> mFrameLoaderFinalizers;
   RefPtr<nsRunnableMethod<nsDocument>> mFrameLoaderRunner;
 
   nsCOMPtr<nsIRunnable> mMaybeEndOutermostXBLUpdateRunner;
 
   nsExternalResourceMap mExternalResourceMap;
 
diff --git a/dom/base/nsIDocument.h b/dom/base/nsIDocument.h
--- a/dom/base/nsIDocument.h
+++ b/dom/base/nsIDocument.h
@@ -152,16 +152,17 @@ class Element;
 struct ElementCreationOptions;
 class Event;
 class EventTarget;
 class FontFaceSet;
 class FrameRequestCallback;
 struct FullscreenRequest;
 class ImageTracker;
 class HTMLBodyElement;
+class HTMLImageElement;
 struct LifecycleCallbackArgs;
 class Link;
 class Location;
 class MediaQueryList;
 class GlobalObject;
 class NodeFilter;
 class NodeIterator;
 enum class OrientationType : uint8_t;
@@ -2515,19 +2516,33 @@ class nsIDocument : public nsINode,
   bool InUnlinkOrDeletion() { return mInUnlinkOrDeletion; }
 
   mozilla::dom::ImageTracker* ImageTracker();
 
   virtual nsresult AddPlugin(nsIObjectLoadingContent* aPlugin) = 0;
   virtual void RemovePlugin(nsIObjectLoadingContent* aPlugin) = 0;
   virtual void GetPlugins(nsTArray<nsIObjectLoadingContent*>& aPlugins) = 0;
 
-  virtual nsresult AddResponsiveContent(nsIContent* aContent) = 0;
-  virtual void RemoveResponsiveContent(nsIContent* aContent) = 0;
-  virtual void NotifyMediaFeatureValuesChanged() = 0;
+  // Adds an element to mResponsiveContent when the element is
+  // added to the tree.
+  void AddResponsiveContent(mozilla::dom::HTMLImageElement* aContent) {
+    MOZ_ASSERT(aContent);
+    mResponsiveContent.PutEntry(aContent);
+  }
+
+  // Removes an element from mResponsiveContent when the element is
+  // removed from the tree.
+  void RemoveResponsiveContent(mozilla::dom::HTMLImageElement* aContent) {
+    MOZ_ASSERT(aContent);
+    mResponsiveContent.RemoveEntry(aContent);
+  }
+
+  // Notifies any responsive content added by AddResponsiveContent upon media
+  // features values changing.
+  void NotifyMediaFeatureValuesChanged();
 
   virtual nsresult GetStateObject(nsIVariant** aResult) = 0;
 
   nsDOMNavigationTiming* GetNavigationTiming() const { return mTiming; }
 
   void SetNavigationTiming(nsDOMNavigationTiming* aTiming);
 
   Element* FindImageMap(const nsAString& aNormalizedMapName);
@@ -3692,16 +3707,19 @@ class nsIDocument : public nsINode,
   // The root of the doc tree in which this document is in. This is only
   // non-null when this document is in fullscreen mode.
   nsWeakPtr mFullscreenRoot;
 
   RefPtr<mozilla::dom::DOMImplementation> mDOMImplementation;
 
   RefPtr<nsContentList> mImageMaps;
 
+  // A set of responsive images keyed by address pointer.
+  nsTHashtable<nsPtrHashKey<mozilla::dom::HTMLImageElement>> mResponsiveContent;
+
  public:
   js::ExpandoAndGeneration mExpandoAndGeneration;
 
  protected:
 
   nsTArray<RefPtr<mozilla::StyleSheet>> mOnDemandBuiltInUASheets;
   nsTArray<RefPtr<mozilla::StyleSheet>>
       mAdditionalSheets[AdditionalSheetTypeCount];
