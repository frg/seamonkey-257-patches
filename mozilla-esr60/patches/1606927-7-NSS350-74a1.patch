# HG changeset patch
# User Kevin Jacobs <kjacobs@mozilla.com>
# Date 1580839773 0
# Node ID dfa143ba202ee475dfeb5e7f51729d8897ed0df1
# Parent  d52fb225037bde8f8b092a487a787261b891f857
Bug 1606927 - land NSS de6ba04bb1f4 UPGRADE_NSS_RELEASE, r=jcj

2020-02-03  Kai Engert  <kaie@kuix.de>

	* automation/release/nspr-version.txt:
	Bug 1612623 - NSS 3.50 should depend on NSPR 4.25. r=kjacobs

	[de6ba04bb1f4] [NSS_3_50_BETA1]

2020-01-27  Giulio Benetti  <giulio.benetti@benettiengineering.com>

	* coreconf/config.gypi, coreconf/config.mk, lib/freebl/Makefile,
	lib/freebl/freebl.gyp, lib/freebl/gcm.h:
	Bug 1608151 - Introduce NSS_DISABLE_ALTIVEC and disable_altivec
	r=jcj

	At the moment NSS assumes that every PowerPC64 architecture supports
	Altivec but it's not true and this leads to build failure. So add
	NSS_DISABLE_ALTIVEC environment variable(and disable_altivec for
	gyp) to disable Altivec extension on PowerPC build that don't
	support Altivec.
	[f2d947817850]

Differential Revision: https://phabricator.services.mozilla.com/D61574

diff --git a/security/nss/TAG-INFO b/security/nss/TAG-INFO
--- a/security/nss/TAG-INFO
+++ b/security/nss/TAG-INFO
@@ -1,1 +1,1 @@
-4bf79c4d2954
\ No newline at end of file
+de6ba04bb1f4
\ No newline at end of file
diff --git a/security/nss/automation/release/nspr-version.txt b/security/nss/automation/release/nspr-version.txt
--- a/security/nss/automation/release/nspr-version.txt
+++ b/security/nss/automation/release/nspr-version.txt
@@ -1,9 +1,9 @@
-4.24
+4.25
 
 # The first line of this file must contain the human readable NSPR
 # version number, which is the minimum required version of NSPR
 # that is supported by this version of NSS.
 #
 # This information is used by release automation,
 # when creating an NSS source archive.
 #
diff --git a/security/nss/coreconf/config.gypi b/security/nss/coreconf/config.gypi
--- a/security/nss/coreconf/config.gypi
+++ b/security/nss/coreconf/config.gypi
@@ -97,16 +97,17 @@
     'cc_use_gnu_ld%': '<(cc_use_gnu_ld)',
     # Some defaults
     'disable_arm_hw_aes%': 0,
     'disable_tests%': 0,
     'disable_chachapoly%': 0,
     'disable_dbm%': 1,
     'disable_libpkix%': 1,
     'disable_werror%': 0,
+    'disable_altivec%': 0,
     'mozilla_client%': 0,
     'comm_client%': 0,
     'moz_fold_libs%': 0,
     'moz_folded_library_name%': '',
     'sanitizer_flags%': 0,
     'static_libs%': 0,
     'no_zdefs%': 0,
     'fuzz%': 0,
diff --git a/security/nss/coreconf/config.mk b/security/nss/coreconf/config.mk
--- a/security/nss/coreconf/config.mk
+++ b/security/nss/coreconf/config.mk
@@ -191,16 +191,21 @@ endif
 
 # Avoid building object leak test code for optimized library
 ifndef BUILD_OPT
 ifdef PKIX_OBJECT_LEAK_TEST
 DEFINES += -DPKIX_OBJECT_LEAK_TEST
 endif
 endif
 
+# Avoid building with PowerPC's Altivec acceleration
+ifdef NSS_DISABLE_ALTIVEC
+DEFINES += -DNSS_DISABLE_ALTIVEC
+endif
+
 # This allows all library and tools code to use the util function
 # implementations directly from libnssutil3, rather than the wrappers
 # in libnss3 which are present for binary compatibility only
 DEFINES += -DUSE_UTIL_DIRECTLY
 USE_UTIL_DIRECTLY = 1
 
 # Build with NO_NSPR_10_SUPPORT to avoid using obsolete NSPR features
 DEFINES += -DNO_NSPR_10_SUPPORT
diff --git a/security/nss/coreconf/coreconf.dep b/security/nss/coreconf/coreconf.dep
--- a/security/nss/coreconf/coreconf.dep
+++ b/security/nss/coreconf/coreconf.dep
@@ -5,8 +5,9 @@
 
 /*
  * A dummy header file that is a dependency for all the object files.
  * Used to force a full recompilation of NSS in Mozilla's Tinderbox
  * depend builds.  See comments in rules.mk.
  */
 
 #error "Do not include this header file."
+
diff --git a/security/nss/lib/freebl/Makefile b/security/nss/lib/freebl/Makefile
--- a/security/nss/lib/freebl/Makefile
+++ b/security/nss/lib/freebl/Makefile
@@ -773,16 +773,18 @@ USES_SOFTFLOAT_ABI := $(shell $(CC) -o -
 $(OBJDIR)/$(PROG_PREFIX)gcm-arm32-neon$(OBJ_SUFFIX): CFLAGS += -mfpu=neon$(if $(USES_SOFTFLOAT_ABI), -mfloat-abi=softfp)
 endif
 ifeq ($(CPU_ARCH),aarch64)
 $(OBJDIR)/$(PROG_PREFIX)aes-armv8$(OBJ_SUFFIX): CFLAGS += -march=armv8-a+crypto
 $(OBJDIR)/$(PROG_PREFIX)gcm-aarch64$(OBJ_SUFFIX): CFLAGS += -march=armv8-a+crypto
 endif
 
 ifeq ($(CPU_ARCH),ppc)
+ifndef NSS_DISABLE_ALTIVEC
 $(OBJDIR)/$(PROG_PREFIX)gcm-ppc$(OBJ_SUFFIX): CFLAGS += -mcrypto -maltivec -mvsx
 $(OBJDIR)/$(PROG_PREFIX)gcm$(OBJ_SUFFIX): CFLAGS += -mcrypto -maltivec -mvsx
 $(OBJDIR)/$(PROG_PREFIX)rijndael$(OBJ_SUFFIX): CFLAGS += -mcrypto -maltivec -mvsx
 endif
+endif
 
 $(OBJDIR)/$(PROG_PREFIX)Hacl_Chacha20_Vec128$(OBJ_SUFFIX): CFLAGS += -mssse3 -msse4 -mavx -maes 
 $(OBJDIR)/$(PROG_PREFIX)Hacl_Chacha20Poly1305_128$(OBJ_SUFFIX): CFLAGS += -mssse3 -msse4 -mavx -maes 
 $(OBJDIR)/$(PROG_PREFIX)Hacl_Poly1305_128$(OBJ_SUFFIX): CFLAGS += -mssse3 -msse4 -mavx -maes -mpclmul
diff --git a/security/nss/lib/freebl/freebl.gyp b/security/nss/lib/freebl/freebl.gyp
--- a/security/nss/lib/freebl/freebl.gyp
+++ b/security/nss/lib/freebl/freebl.gyp
@@ -269,21 +269,26 @@
             'gcm-aes-arm32-neon_c_lib',
           ],
         }],
         [ 'target_arch=="arm64" or target_arch=="aarch64"', {
           'dependencies': [
             'gcm-aes-aarch64_c_lib',
           ],
         }],
-        [ 'target_arch=="ppc64le"', {
+        [ 'disable_altivec==0 and (target_arch=="ppc64" or target_arch=="ppc64le")', {
           'dependencies': [
             'gcm-aes-ppc_c_lib',
           ],
         }],
+        [ 'disable_altivec==1 and (target_arch=="ppc64" or target_arch=="ppc64le")', {
+          'defines!': [
+            'NSS_DISABLE_ALTIVEC',
+          ],
+        }],
         [ 'OS=="linux"', {
           'defines!': [
             'FREEBL_NO_DEPEND',
             'FREEBL_LOWHASH',
             'USE_HW_AES',
             'INTEL_GCM',
           ],
           'conditions': [
@@ -325,21 +330,26 @@
             'gcm-aes-arm32-neon_c_lib',
           ],
         }],
         [ 'target_arch=="arm64" or target_arch=="aarch64"', {
           'dependencies': [
             'gcm-aes-aarch64_c_lib',
           ],
         }],
-        [ 'target_arch=="ppc64" or target_arch=="ppc64le"', {
+        [ 'disable_altivec==0 and (target_arch=="ppc64" or target_arch=="ppc64le")', {
           'dependencies': [
             'gcm-aes-ppc_c_lib',
           ],
         }],
+        [ 'disable_altivec==1 and (target_arch=="ppc64" or target_arch=="ppc64le")', {
+          'defines!': [
+            'NSS_DISABLE_ALTIVEC',
+          ],
+        }],
         [ 'OS!="linux"', {
           'conditions': [
             [ 'moz_fold_libs==0', {
               'dependencies': [
                 '<(DEPTH)/lib/util/util.gyp:nssutil3',
               ],
             }, {
               'libraries': [
diff --git a/security/nss/lib/freebl/gcm.h b/security/nss/lib/freebl/gcm.h
--- a/security/nss/lib/freebl/gcm.h
+++ b/security/nss/lib/freebl/gcm.h
@@ -25,17 +25,17 @@
 #pragma GCC pop_options
 #endif /* NSS_DISABLE_SSE2 */
 #endif
 
 #ifdef __aarch64__
 #include <arm_neon.h>
 #endif
 
-#ifdef __powerpc64__
+#if defined(__powerpc64__) && !defined(NSS_DISABLE_ALTIVEC)
 #include "altivec-types.h"
 
 /* The ghash freebl test tries to use this in C++, and gcc defines conflict. */
 #ifdef __cplusplus
 #undef pixel
 #undef vector
 #undef bool
 #endif

