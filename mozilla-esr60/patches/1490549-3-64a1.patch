# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1536948083 0
# Node ID e545bb3cfccacdd124562a5025f7978d19b52739
# Parent  ec968c07f4b13813448934cdadce63c4e754481a
Bug 1490549 - Make configure choose clang by default on all platforms r=froydnj

Now that we ship builds using clang on all platforms, pick it during
configure. It is still possible to opt-in to building other compilers by
setting CC/CXX (or even only CC) to the desired compiler.

Depends on D5829

Differential Revision: https://phabricator.services.mozilla.com/D5637

diff --git a/build/moz.configure/toolchain.configure b/build/moz.configure/toolchain.configure
--- a/build/moz.configure/toolchain.configure
+++ b/build/moz.configure/toolchain.configure
@@ -675,29 +675,26 @@ def default_c_compilers(host_or_target, 
     from init.configure.
     `other_c_compiler` is the `target` C compiler when `host_or_target` is `host`.
     '''
     assert host_or_target in (host, target)
 
     other_c_compiler = () if other_c_compiler is None else (other_c_compiler,)
 
     @depends(host_or_target, target, toolchain_prefix, android_clang_compiler,
-             developer_options, *other_c_compiler)
+             *other_c_compiler)
     def default_c_compilers(host_or_target, target, toolchain_prefix,
-                            android_clang_compiler, developer_options,
-                            *other_c_compiler):
+                            android_clang_compiler, *other_c_compiler):
         if host_or_target.kernel == 'WINNT':
             supported = types = ('clang-cl', 'msvc', 'gcc', 'clang')
         elif host_or_target.kernel == 'Darwin':
             types = ('clang',)
             supported = ('clang', 'gcc')
-        elif developer_options:
+        else:
             supported = types = ('clang', 'gcc')
-        else:
-            supported = types = ('gcc', 'clang')
 
         info = other_c_compiler[0] if other_c_compiler else None
         if info and info.type in supported:
             # When getting default C compilers for the host, we prioritize the
             # same compiler as the target C compiler.
             prioritized = info.compiler
             if info.type == 'gcc':
                 same_arch = same_arch_different_bits()
diff --git a/python/mozbuild/mozbuild/test/configure/test_toolchain_configure.py b/python/mozbuild/mozbuild/test/configure/test_toolchain_configure.py
--- a/python/mozbuild/mozbuild/test/configure/test_toolchain_configure.py
+++ b/python/mozbuild/mozbuild/test/configure/test_toolchain_configure.py
@@ -492,36 +492,46 @@ class LinuxToolchainTest(BaseToolchainTe
         version='4.0.2',
         type='clang',
         compiler='/usr/bin/clang++-4.0',
         language='C++',
     )
     DEFAULT_CLANG_RESULT = CLANG_4_0_RESULT + {'compiler': '/usr/bin/clang'}
     DEFAULT_CLANGXX_RESULT = CLANGXX_4_0_RESULT + {'compiler': '/usr/bin/clang++'}
 
+    def test_default(self):
+        # We'll try clang and gcc, and find clang first.
+        self.do_toolchain_test(self.PATHS, {
+            'c_compiler': self.DEFAULT_CLANG_RESULT,
+            'cxx_compiler': self.DEFAULT_CLANGXX_RESULT,
+        })
+
     def test_gcc(self):
-        # We'll try gcc and clang, and find gcc first.
         self.do_toolchain_test(self.PATHS, {
             'c_compiler': self.DEFAULT_GCC_RESULT,
             'cxx_compiler': self.DEFAULT_GXX_RESULT,
+        }, environ={
+            'CC': 'gcc',
+            'CXX': 'g++',
         })
 
     def test_unsupported_gcc(self):
         self.do_toolchain_test(self.PATHS, {
             'c_compiler': self.GCC_4_9_RESULT,
         }, environ={
             'CC': 'gcc-4.9',
             'CXX': 'g++-4.9',
         })
 
         # Maybe this should be reporting the mismatched version instead.
         self.do_toolchain_test(self.PATHS, {
             'c_compiler': self.DEFAULT_GCC_RESULT,
             'cxx_compiler': self.GXX_4_9_RESULT,
         }, environ={
+            'CC': 'gcc',
             'CXX': 'g++-4.9',
         })
 
     def test_overridden_gcc(self):
         self.do_toolchain_test(self.PATHS, {
             'c_compiler': self.GCC_7_RESULT,
             'cxx_compiler': self.GXX_7_RESULT,
         }, environ={
@@ -541,65 +551,67 @@ class LinuxToolchainTest(BaseToolchainTe
     def test_mismatched_gcc(self):
         self.do_toolchain_test(self.PATHS, {
             'c_compiler': self.DEFAULT_GCC_RESULT,
             'cxx_compiler': (
                 'The target C compiler is version 6.4.0, while the target '
                 'C++ compiler is version 7.3.0. Need to use the same compiler '
                 'version.'),
         }, environ={
+            'CC': 'gcc',
             'CXX': 'g++-7',
         })
 
         self.do_toolchain_test(self.PATHS, {
             'c_compiler': self.DEFAULT_GCC_RESULT,
             'cxx_compiler': self.DEFAULT_GXX_RESULT,
             'host_c_compiler': self.DEFAULT_GCC_RESULT,
             'host_cxx_compiler': (
                 'The host C compiler is version 6.4.0, while the host '
                 'C++ compiler is version 7.3.0. Need to use the same compiler '
                 'version.'),
         }, environ={
+            'CC': 'gcc',
             'HOST_CXX': 'g++-7',
         })
 
     def test_mismatched_compiler(self):
         self.do_toolchain_test(self.PATHS, {
-            'c_compiler': self.DEFAULT_GCC_RESULT,
+            'c_compiler': self.DEFAULT_CLANG_RESULT,
             'cxx_compiler': (
-                'The target C compiler is gcc, while the target C++ compiler '
-                'is clang. Need to use the same compiler suite.'),
+                'The target C compiler is clang, while the target C++ compiler '
+                'is gcc. Need to use the same compiler suite.'),
         }, environ={
-            'CXX': 'clang++',
+            'CXX': 'g++',
         })
 
         self.do_toolchain_test(self.PATHS, {
-            'c_compiler': self.DEFAULT_GCC_RESULT,
-            'cxx_compiler': self.DEFAULT_GXX_RESULT,
-            'host_c_compiler': self.DEFAULT_GCC_RESULT,
+            'c_compiler': self.DEFAULT_CLANG_RESULT,
+            'cxx_compiler': self.DEFAULT_CLANGXX_RESULT,
+            'host_c_compiler': self.DEFAULT_CLANG_RESULT,
             'host_cxx_compiler': (
-                'The host C compiler is gcc, while the host C++ compiler '
-                'is clang. Need to use the same compiler suite.'),
+                'The host C compiler is clang, while the host C++ compiler '
+                'is gcc. Need to use the same compiler suite.'),
         }, environ={
-            'HOST_CXX': 'clang++',
+            'HOST_CXX': 'g++',
         })
 
         self.do_toolchain_test(self.PATHS, {
             'c_compiler': '`%s` is not a C compiler.'
             % mozpath.abspath('/usr/bin/g++'),
         }, environ={
             'CC': 'g++',
         })
 
         self.do_toolchain_test(self.PATHS, {
-            'c_compiler': self.DEFAULT_GCC_RESULT,
+            'c_compiler': self.DEFAULT_CLANG_RESULT,
             'cxx_compiler': '`%s` is not a C++ compiler.'
-            % mozpath.abspath('/usr/bin/gcc'),
+            % mozpath.abspath('/usr/bin/clang'),
         }, environ={
-            'CXX': 'gcc',
+            'CXX': 'clang',
         })
 
     def test_clang(self):
         # We'll try gcc and clang, but since there is no gcc (gcc-x.y doesn't
         # count), find clang.
         paths = {
             k: v for k, v in self.PATHS.iteritems()
             if os.path.basename(k) not in ('gcc', 'g++')
@@ -716,30 +728,30 @@ class LinuxSimpleCrossToolchainTest(Base
             'c_compiler': self.DEFAULT_GCC_RESULT + {
                 'flags': ['-m32']
             },
             'cxx_compiler': self.DEFAULT_GXX_RESULT + {
                 'flags': ['-m32']
             },
             'host_c_compiler': self.DEFAULT_GCC_RESULT,
             'host_cxx_compiler': self.DEFAULT_GXX_RESULT,
+        }, environ={
+            'CC': 'gcc'
         })
 
     def test_cross_clang(self):
         self.do_toolchain_test(self.PATHS, {
             'c_compiler': self.DEFAULT_CLANG_RESULT + {
                 'flags': ['--target=i686-linux-gnu'],
             },
             'cxx_compiler': self.DEFAULT_CLANGXX_RESULT + {
                 'flags': ['--target=i686-linux-gnu'],
             },
             'host_c_compiler': self.DEFAULT_CLANG_RESULT,
             'host_cxx_compiler': self.DEFAULT_CLANGXX_RESULT,
-        }, environ={
-            'CC': 'clang',
         })
 
 
 class LinuxX86_64CrossToolchainTest(BaseToolchainTest):
     HOST = 'i686-pc-linux-gnu'
     TARGET = 'x86_64-pc-linux-gnu'
     PATHS = {
         '/usr/bin/gcc': DEFAULT_GCC + GCC_PLATFORM_X86_LINUX,
@@ -757,30 +769,30 @@ class LinuxX86_64CrossToolchainTest(Base
             'c_compiler': self.DEFAULT_GCC_RESULT + {
                 'flags': ['-m64']
             },
             'cxx_compiler': self.DEFAULT_GXX_RESULT + {
                 'flags': ['-m64']
             },
             'host_c_compiler': self.DEFAULT_GCC_RESULT,
             'host_cxx_compiler': self.DEFAULT_GXX_RESULT,
+        }, environ={
+            'CC': 'gcc',
         })
 
     def test_cross_clang(self):
         self.do_toolchain_test(self.PATHS, {
             'c_compiler': self.DEFAULT_CLANG_RESULT + {
                 'flags': ['--target=x86_64-linux-gnu'],
             },
             'cxx_compiler': self.DEFAULT_CLANGXX_RESULT + {
                 'flags': ['--target=x86_64-linux-gnu'],
             },
             'host_c_compiler': self.DEFAULT_CLANG_RESULT,
             'host_cxx_compiler': self.DEFAULT_CLANGXX_RESULT,
-        }, environ={
-            'CC': 'clang',
         })
 
 
 class OSXToolchainTest(BaseToolchainTest):
     HOST = 'x86_64-apple-darwin11.2.0'
     PATHS = {
         '/usr/bin/gcc-5': GCC_5 + GCC_PLATFORM_X86_64_OSX,
         '/usr/bin/g++-5': GXX_5 + GCC_PLATFORM_X86_64_OSX,
@@ -1428,41 +1440,41 @@ class OSXCrossToolchainTest(BaseToolchai
                           'match --target kernel (Darwin)',
         }, environ={
             'CC': 'gcc',
         })
 
 
 class WindowsCrossToolchainTest(BaseToolchainTest):
     TARGET = 'x86_64-pc-mingw32'
-    DEFAULT_GCC_RESULT = LinuxToolchainTest.DEFAULT_GCC_RESULT
-    DEFAULT_GXX_RESULT = LinuxToolchainTest.DEFAULT_GXX_RESULT
+    DEFAULT_CLANG_RESULT = LinuxToolchainTest.DEFAULT_CLANG_RESULT
+    DEFAULT_CLANGXX_RESULT = LinuxToolchainTest.DEFAULT_CLANGXX_RESULT
 
     def test_wsl_cross(self):
         paths = {
             '/usr/bin/cl': VS_2017u6 + VS_PLATFORM_X86_64,
         }
         paths.update(LinuxToolchainTest.PATHS)
         self.do_toolchain_test(paths, {
             'c_compiler': WindowsToolchainTest.VS_2017u6_RESULT,
             'cxx_compiler': WindowsToolchainTest.VSXX_2017u6_RESULT,
-            'host_c_compiler': self.DEFAULT_GCC_RESULT,
-            'host_cxx_compiler': self.DEFAULT_GXX_RESULT,
+            'host_c_compiler': self.DEFAULT_CLANG_RESULT,
+            'host_cxx_compiler': self.DEFAULT_CLANGXX_RESULT,
         })
 
     def test_clang_cl_cross(self):
         paths = {
             '/usr/bin/clang-cl': CLANG_CL_3_9 + CLANG_CL_PLATFORM_X86_64,
         }
         paths.update(LinuxToolchainTest.PATHS)
         self.do_toolchain_test(paths, {
             'c_compiler': WindowsToolchainTest.CLANG_CL_3_9_RESULT,
             'cxx_compiler': WindowsToolchainTest.CLANGXX_CL_3_9_RESULT,
-            'host_c_compiler': self.DEFAULT_GCC_RESULT,
-            'host_cxx_compiler': self.DEFAULT_GXX_RESULT,
+            'host_c_compiler': self.DEFAULT_CLANG_RESULT,
+            'host_cxx_compiler': self.DEFAULT_CLANGXX_RESULT,
         })
 
 
 class OpenBSDToolchainTest(BaseToolchainTest):
     HOST = 'x86_64-unknown-openbsd6.1'
     TARGET = 'x86_64-unknown-openbsd6.1'
     PATHS = {
         '/usr/bin/gcc': DEFAULT_GCC + GCC_PLATFORM_X86_64 + GCC_PLATFORM_OPENBSD,
