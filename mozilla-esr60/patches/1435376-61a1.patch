# HG changeset patch
# User David Keeler <dkeeler@mozilla.com>
# Date 1520550832 28800
# Node ID 4622767450bdfbecdd6922cb350d67093cb760b2
# Parent  2d0b2e6a4a2cda2971b44799bbf70f405db2a881
bug 1435376 - set NSS_SDB_USE_CACHE to yes if the profile is on a remote drive (windows version) r=jcj,mhowell

MozReview-Commit-ID: 4b1v8ypljoR

diff --git a/security/manager/ssl/nsNSSComponent.cpp b/security/manager/ssl/nsNSSComponent.cpp
--- a/security/manager/ssl/nsNSSComponent.cpp
+++ b/security/manager/ssl/nsNSSComponent.cpp
@@ -1710,16 +1710,50 @@ nsresult nsNSSComponent::setEnabledTLSVe
       SSL_VersionRangeSetDefault(ssl_variant_stream, &filledInRange);
   if (srv != SECSuccess) {
     return NS_ERROR_FAILURE;
   }
 
   return NS_OK;
 }
 
+#ifdef XP_WIN
+// If the profile directory is on a networked drive, we want to set the
+// environment variable NSS_SDB_USE_CACHE to yes (as long as it hasn't been set
+// before).
+static void SetNSSDatabaseCacheModeAsAppropriate() {
+  nsCOMPtr<nsIFile> profileFile;
+  nsresult rv = NS_GetSpecialDirectory(NS_APP_USER_PROFILE_50_DIR,
+                                       getter_AddRefs(profileFile));
+  if (NS_FAILED(rv)) {
+    // We're probably running without a profile directory, so this is
+    // irrelevant.
+    return;
+  }
+
+  nsCOMPtr<nsILocalFileWin> profileFileWin(do_QueryInterface(profileFile));
+  if (!profileFileWin) {
+    MOZ_LOG(gPIPNSSLog, LogLevel::Debug, ("couldn't get nsILocalFileWin?"));
+    return;
+  }
+  auto profilePath = profileFile->NativePath();
+  wchar_t volPath[MAX_PATH];
+  if (::GetVolumePathNameW(profilePath.get(), volPath, MAX_PATH) &&
+      ::GetDriveTypeW(volPath) == DRIVE_REMOTE &&
+      !PR_GetEnv("NSS_SDB_USE_CACHE")) {
+    MOZ_LOG(gPIPNSSLog, LogLevel::Debug,
+            ("profile is remote (and NSS_SDB_USE_CACHE wasn't set): "
+             "setting NSS_SDB_USE_CACHE"));
+    PR_SetEnv("NSS_SDB_USE_CACHE=yes");
+  } else {
+    MOZ_LOG(gPIPNSSLog, LogLevel::Debug, ("not setting NSS_SDB_USE_CACHE"));
+  }
+}
+#endif // XP_WIN
+
 static nsresult GetNSSProfilePath(nsAutoCString& aProfilePath) {
   aProfilePath.Truncate();
   nsCOMPtr<nsIFile> profileFile;
   nsresult rv = NS_GetSpecialDirectory(NS_APP_USER_PROFILE_50_DIR,
                                        getter_AddRefs(profileFile));
   if (NS_FAILED(rv)) {
     NS_WARNING(
         "NSS will be initialized without a profile directory. "
@@ -1962,16 +1996,20 @@ nsresult nsNSSComponent::InitializeNSS()
   ConfigureInternalPKCS11Token();
 
   nsAutoCString profileStr;
   nsresult rv = GetNSSProfilePath(profileStr);
   if (NS_FAILED(rv)) {
     return NS_ERROR_NOT_AVAILABLE;
   }
 
+#ifdef XP_WIN
+  SetNSSDatabaseCacheModeAsAppropriate();
+#endif
+
   bool nocertdb = Preferences::GetBool("security.nocertdb", false);
   bool inSafeMode = true;
   nsCOMPtr<nsIXULRuntime> runtime(do_GetService("@mozilla.org/xre/runtime;1"));
   // There might not be an nsIXULRuntime in embedded situations. This will
   // default to assuming we are in safe mode (as a result, no external PKCS11
   // modules will be loaded).
   if (runtime) {
     rv = runtime->GetInSafeMode(&inSafeMode);
