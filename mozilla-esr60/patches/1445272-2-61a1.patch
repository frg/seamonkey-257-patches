# HG changeset patch
# User Benjamin Bouvier <benj@benj.me>
# Date 1521639638 -3600
# Node ID 40baddc5f0b6206f9a90bd694c596b0d8f8a9837
# Parent  9029e473946f31514f962504c15e332b852185bc
Bug 1445272: Add gcTypesEnabled to wasm::ModuleEnvironment and Metadata; r=luke

This is going to be used in subsequent patches, to know if AnyRef is accepted
as a ValType in misc places.

Note temporaryGcTypesEnabled should be removed once we've implemented proper GC
support instead of the hack planned in bug 1445277.

diff --git a/js/src/wasm/AsmJS.cpp b/js/src/wasm/AsmJS.cpp
--- a/js/src/wasm/AsmJS.cpp
+++ b/js/src/wasm/AsmJS.cpp
@@ -1682,16 +1682,17 @@ class MOZ_STACK_CLASS ModuleValidator {
         tables_(cx),
         globalMap_(cx),
         sigSet_(cx),
         funcImportMap_(cx),
         arrayViews_(cx),
         atomicsPresent_(false),
         simdPresent_(false),
         env_(CompileMode::Once, Tier::Ion, DebugEnabled::False,
+             HasGcTypes::False,
              cx->compartment()
                      ->creationOptions()
                      .getSharedMemoryAndAtomicsEnabled()
                  ? Shareable::True
                  : Shareable::False,
              ModuleKind::AsmJS),
         errorString_(nullptr),
         errorOffset_(UINT32_MAX),
diff --git a/js/src/wasm/WasmBinaryToAST.cpp b/js/src/wasm/WasmBinaryToAST.cpp
--- a/js/src/wasm/WasmBinaryToAST.cpp
+++ b/js/src/wasm/WasmBinaryToAST.cpp
@@ -86,16 +86,17 @@ class AstDecodeContext {
  public:
   AstDecodeContext(JSContext* cx, LifoAlloc& lifo, Decoder& d,
                    AstModule& module, bool generateNames)
       : cx(cx),
         lifo(lifo),
         d(d),
         generateNames(generateNames),
         env_(CompileMode::Once, Tier::Ion, DebugEnabled::False,
+             HasGcTypes::False,
              cx->compartment()
                      ->creationOptions()
                      .getSharedMemoryAndAtomicsEnabled()
                  ? Shareable::True
                  : Shareable::False),
         module_(module),
         iter_(nullptr),
         exprs_(lifo),
diff --git a/js/src/wasm/WasmCode.h b/js/src/wasm/WasmCode.h
--- a/js/src/wasm/WasmCode.h
+++ b/js/src/wasm/WasmCode.h
@@ -356,26 +356,28 @@ typedef Vector<ExprType, 0, SystemAllocP
 // The Metadata structure is split into tier-invariant and tier-variant parts;
 // the former points to instances of the latter.  Additionally, the asm.js
 // subsystem subclasses the Metadata, adding more tier-invariant data, some of
 // which is serialized.  See AsmJS.cpp.
 
 struct MetadataCacheablePod {
   ModuleKind kind;
   MemoryUsage memoryUsage;
+  HasGcTypes temporaryHasGcTypes;
   uint32_t minMemoryLength;
   uint32_t globalDataLength;
   Maybe<uint32_t> maxMemoryLength;
   Maybe<uint32_t> startFuncIndex;
   Maybe<NameInBytecode> moduleName;
   bool filenameIsURL;
 
   explicit MetadataCacheablePod(ModuleKind kind)
       : kind(kind),
         memoryUsage(MemoryUsage::None),
+        temporaryHasGcTypes(HasGcTypes::False),
         minMemoryLength(0),
         globalDataLength(0),
         filenameIsURL(false) {}
 };
 
 typedef uint8_t ModuleHash[8];
 
 struct Metadata : public ShareableBase<Metadata>, public MetadataCacheablePod {
diff --git a/js/src/wasm/WasmCompile.cpp b/js/src/wasm/WasmCompile.cpp
--- a/js/src/wasm/WasmCompile.cpp
+++ b/js/src/wasm/WasmCompile.cpp
@@ -79,21 +79,29 @@ static bool DecodeCodeSection(const Modu
 
   if (!d.finishSection(*env.codeSection, "code")) return false;
 
   return mg.finishFuncDefs();
 }
 
 bool CompileArgs::initFromContext(JSContext* cx,
                                   ScriptedCaller&& scriptedCaller) {
-  baselineEnabled = cx->options().wasmBaseline();
-  ionEnabled = cx->options().wasmIon();
+#ifdef ENABLE_WASM_GC
+  bool gcEnabled = cx->options().wasmGc();
+#else
+  bool gcEnabled = false;
+#endif
+
+  baselineEnabled = cx->options().wasmBaseline() || gcEnabled;
+  ionEnabled = cx->options().wasmIon() && !gcEnabled;
   sharedMemoryEnabled =
       cx->compartment()->creationOptions().getSharedMemoryAndAtomicsEnabled();
-  testTiering = cx->options().testWasmAwaitTier2() || JitOptions.wasmDelayTier2;
+  gcTypesEnabled = gcEnabled ? HasGcTypes::True : HasGcTypes::False;
+  testTiering = (cx->options().testWasmAwaitTier2() ||
+                 JitOptions.wasmDelayTier2) && !gcEnabled;
 
   // Debug information such as source view or debug traps will require
   // additional memory and permanently stay in baseline code, so we try to
   // only enable it when a developer actually cares: when the debugger tab
   // is open.
   debugEnabled = cx->compartment()->debuggerObservesAsmJS();
 
   this->scriptedCaller = Move(scriptedCaller);
@@ -405,17 +413,17 @@ SharedModule wasm::CompileBuffer(const C
   Decoder d(bytecode.bytes, 0, error);
 
   CompileMode mode;
   Tier tier;
   DebugEnabled debug;
   InitialCompileFlags(args, d, &mode, &tier, &debug);
 
   ModuleEnvironment env(
-      mode, tier, debug,
+      mode, tier, debug, args.gcTypesEnabled,
       args.sharedMemoryEnabled ? Shareable::True : Shareable::False);
   if (!DecodeModuleEnvironment(d, &env)) return nullptr;
 
   ModuleGenerator mg(args, &env, nullptr, error);
   if (!mg.init()) return nullptr;
 
   if (!DecodeCodeSection(env, d, mg)) return nullptr;
 
@@ -426,18 +434,21 @@ SharedModule wasm::CompileBuffer(const C
 
 void wasm::CompileTier2(const CompileArgs& args, Module& module,
                         Atomic<bool>* cancelled) {
   MOZ_RELEASE_ASSERT(wasm::HaveSignalHandlers());
 
   UniqueChars error;
   Decoder d(module.bytecode().bytes, 0, &error);
 
+  MOZ_ASSERT(args.gcTypesEnabled == HasGcTypes::False,
+             "can't ion-compile with gc types yet");
+
   ModuleEnvironment env(
-      CompileMode::Tier2, Tier::Ion, DebugEnabled::False,
+      CompileMode::Tier2, Tier::Ion, DebugEnabled::False, HasGcTypes::False,
       args.sharedMemoryEnabled ? Shareable::True : Shareable::False);
   if (!DecodeModuleEnvironment(d, &env)) return;
 
   ModuleGenerator mg(args, &env, cancelled, &error);
   if (!mg.init()) return;
 
   if (!DecodeCodeSection(env, d, mg)) return;
 
@@ -534,17 +545,17 @@ SharedModule wasm::CompileStreaming(cons
   {
     Decoder d(envBytes, 0, error);
 
     CompileMode mode;
     Tier tier;
     DebugEnabled debug;
     InitialCompileFlags(args, d, &mode, &tier, &debug);
 
-    env.emplace(mode, tier, debug,
+    env.emplace(mode, tier, debug, args.gcTypesEnabled,
                 args.sharedMemoryEnabled ? Shareable::True : Shareable::False);
     if (!DecodeModuleEnvironment(d, env.ptr())) return nullptr;
 
     MOZ_ASSERT(d.done());
   }
 
   ModuleGenerator mg(args, env.ptr(), &cancelled, error);
   if (!mg.init()) return nullptr;
diff --git a/js/src/wasm/WasmCompile.h b/js/src/wasm/WasmCompile.h
--- a/js/src/wasm/WasmCompile.h
+++ b/js/src/wasm/WasmCompile.h
@@ -38,25 +38,27 @@ struct ScriptedCaller {
 struct CompileArgs : ShareableBase<CompileArgs> {
   Assumptions assumptions;
   ScriptedCaller scriptedCaller;
   UniqueChars sourceMapURL;
   bool baselineEnabled;
   bool debugEnabled;
   bool ionEnabled;
   bool sharedMemoryEnabled;
+  HasGcTypes gcTypesEnabled;
   bool testTiering;
 
   CompileArgs(Assumptions&& assumptions, ScriptedCaller&& scriptedCaller)
       : assumptions(Move(assumptions)),
         scriptedCaller(Move(scriptedCaller)),
         baselineEnabled(false),
         debugEnabled(false),
         ionEnabled(false),
         sharedMemoryEnabled(false),
+        gcTypesEnabled(HasGcTypes::False),
         testTiering(false) {}
 
   // If CompileArgs is constructed without arguments, initFromContext() must
   // be called to complete initialization.
   CompileArgs() = default;
   bool initFromContext(JSContext* cx, ScriptedCaller&& scriptedCaller);
 };
 
diff --git a/js/src/wasm/WasmGenerator.cpp b/js/src/wasm/WasmGenerator.cpp
--- a/js/src/wasm/WasmGenerator.cpp
+++ b/js/src/wasm/WasmGenerator.cpp
@@ -805,16 +805,17 @@ bool ModuleGenerator::finishMetadata(con
     MOZ_ASSERT(debugTrapFarJumpOffset >= last);
     last = debugTrapFarJumpOffset;
   }
 #endif
 
   // Copy over data from the ModuleEnvironment.
 
   metadata_->memoryUsage = env_->memoryUsage;
+  metadata_->temporaryHasGcTypes = env_->gcTypesEnabled;
   metadata_->minMemoryLength = env_->minMemoryLength;
   metadata_->maxMemoryLength = env_->maxMemoryLength;
   metadata_->startFuncIndex = env_->startFuncIndex;
   metadata_->moduleName = env_->moduleName;
   metadata_->tables = Move(env_->tables);
   metadata_->globals = Move(env_->globals);
   metadata_->funcNames = Move(env_->funcNames);
   metadata_->customSections = Move(env_->customSections);
diff --git a/js/src/wasm/WasmTypes.h b/js/src/wasm/WasmTypes.h
--- a/js/src/wasm/WasmTypes.h
+++ b/js/src/wasm/WasmTypes.h
@@ -410,16 +410,18 @@ class Tiers {
 };
 
 // A Module can either be asm.js or wasm.
 
 enum ModuleKind { Wasm, AsmJS };
 
 enum class Shareable { False, True };
 
+enum class HasGcTypes { False, True };
+
 // The Val class represents a single WebAssembly value of a given value type,
 // mostly for the purpose of numeric literals and initializers. A Val does not
 // directly map to a JS value since there is not (currently) a precise
 // representation of i64 values. A Val may contain non-canonical NaNs since,
 // within WebAssembly, floats are not canonicalized. Canonicalization must
 // happen at the JS boundary.
 
 class Val {
diff --git a/js/src/wasm/WasmValidate.cpp b/js/src/wasm/WasmValidate.cpp
--- a/js/src/wasm/WasmValidate.cpp
+++ b/js/src/wasm/WasmValidate.cpp
@@ -1766,18 +1766,25 @@ bool wasm::DecodeModuleTail(Decoder& d, 
 }
 
 // Validate algorithm.
 
 bool wasm::Validate(JSContext* cx, const ShareableBytes& bytecode,
                     UniqueChars* error) {
   Decoder d(bytecode.bytes, 0, error);
 
+#ifdef ENABLE_WASM_GC
+  HasGcTypes gcSupport = cx->options().wasmGc() ? HasGcTypes::True
+                                                : HasGcTypes::False;
+#else
+  HasGcTypes gcSupport = HasGcTypes::False;
+#endif
+
   ModuleEnvironment env(
-      CompileMode::Once, Tier::Ion, DebugEnabled::False,
+      CompileMode::Once, Tier::Ion, DebugEnabled::False, gcSupport,
       cx->compartment()->creationOptions().getSharedMemoryAndAtomicsEnabled()
           ? Shareable::True
           : Shareable::False);
   if (!DecodeModuleEnvironment(d, &env)) return false;
 
   if (!DecodeCodeSection(d, &env)) return false;
 
   if (!DecodeModuleTail(d, &env)) return false;
diff --git a/js/src/wasm/WasmValidate.h b/js/src/wasm/WasmValidate.h
--- a/js/src/wasm/WasmValidate.h
+++ b/js/src/wasm/WasmValidate.h
@@ -51,16 +51,17 @@ typedef Maybe<SectionRange> MaybeSection
 // conditions.
 
 struct ModuleEnvironment {
   // Constant parameters for the entire compilation:
   const DebugEnabled debug;
   const ModuleKind kind;
   const CompileMode mode;
   const Shareable sharedMemoryEnabled;
+  const HasGcTypes gcTypesEnabled;
   const Tier tier;
 
   // Module fields decoded from the module environment (or initialized while
   // validating an asm.js module) and immutable during compilation:
   MemoryUsage memoryUsage;
   uint32_t minMemoryLength;
   Maybe<uint32_t> maxMemoryLength;
   SigWithIdVector sigs;
@@ -79,22 +80,24 @@ struct ModuleEnvironment {
   DataSegmentVector dataSegments;
   Maybe<NameInBytecode> moduleName;
   NameInBytecodeVector funcNames;
   CustomSectionVector customSections;
 
   explicit ModuleEnvironment(CompileMode mode,
                              Tier tier,
                              DebugEnabled debug,
+                             HasGcTypes hasGcTypes,
                              Shareable sharedMemoryEnabled,
                              ModuleKind kind = ModuleKind::Wasm)
       : debug(debug),
         kind(kind),
         mode(mode),
         sharedMemoryEnabled(sharedMemoryEnabled),
+        gcTypesEnabled(hasGcTypes),
         tier(tier),
         memoryUsage(MemoryUsage::None),
         minMemoryLength(0) {}
 
   size_t numTables() const { return tables.length(); }
   size_t numSigs() const { return sigs.length(); }
   size_t numFuncs() const { return funcSigs.length(); }
   size_t numFuncImports() const { return funcImportGlobalDataOffsets.length(); }
