# HG changeset patch
# User Jeff Walden <jwalden@mit.edu>
# Date 1526529659 25200
# Node ID 1df7d4219611559f5289e869e51c28c0b42a2853
# Parent  03fdea0bac02409467f4a53fc45b2452814f4b93
Bug 1461556 - Replace a PodZero of js::gcstats::Statistics::phaseTimes with a loop overwriting every element value with a default-initialized (i.e. zeroed) value.  r=jandem

diff --git a/js/src/gc/Statistics.cpp b/js/src/gc/Statistics.cpp
--- a/js/src/gc/Statistics.cpp
+++ b/js/src/gc/Statistics.cpp
@@ -3,17 +3,16 @@
  * This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "gc/Statistics.h"
 
 #include "mozilla/ArrayUtils.h"
 #include "mozilla/DebugOnly.h"
-#include "mozilla/PodOperations.h"
 #include "mozilla/Sprintf.h"
 #include "mozilla/TimeStamp.h"
 
 #include <ctype.h>
 #include <stdarg.h>
 #include <stdio.h>
 #include <type_traits>
 
@@ -28,17 +27,16 @@
 #include "vm/Time.h"
 
 using namespace js;
 using namespace js::gc;
 using namespace js::gcstats;
 
 using mozilla::DebugOnly;
 using mozilla::EnumeratedArray;
-using mozilla::PodZero;
 using mozilla::TimeDuration;
 using mozilla::TimeStamp;
 
 /*
  * If this fails, then you can either delete this assertion and allow all
  * larger-numbered reasons to pile up in the last telemetry bucket, or switch
  * to GC_REASON_3 and bump the max value.
  */
@@ -1018,23 +1016,29 @@ void Statistics::endSlice() {
   // Do this after the slice callback since it uses these values.
   if (last) {
     for (auto& count : counts) count = 0;
 
     // Clear the timers at the end of a GC, preserving the data for
     // PhaseKind::MUTATOR.
     auto mutatorStartTime = phaseStartTimes[Phase::MUTATOR];
     auto mutatorTime = phaseTimes[Phase::MUTATOR];
+
     for (mozilla::TimeStamp& t : phaseStartTimes)
       t = TimeStamp();
 #ifdef DEBUG
     for (mozilla::TimeStamp& t : phaseEndTimes)
       t = TimeStamp();
 #endif
-    PodZero(&phaseTimes);
+
+    for (TimeDuration& duration : phaseTimes) {
+      duration = TimeDuration();
+      MOZ_ASSERT(duration.IsZero());
+    }
+
     phaseStartTimes[Phase::MUTATOR] = mutatorStartTime;
     phaseTimes[Phase::MUTATOR] = mutatorTime;
   }
 
   aborted = false;
 }
 
 void Statistics::reportLongestPhaseInMajorGC(PhaseKind longest,
