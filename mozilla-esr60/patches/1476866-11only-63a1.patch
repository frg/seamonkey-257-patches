# HG changeset patch
# User Jeff Walden <jwalden@mit.edu>
# Date 1531249613 25200
# Node ID ff1852065b5c6a4f2a1829b290f91dae26f7804f
# Parent  e4dd6862fad4e00ff2e93334f8c32a1b06150444
Bug 1476866 - Rename SourceUnits::findEOLMax to SourceUnits::findWindowEnd, and make it include only valid UTF-16.  r=arai

diff --git a/devtools/client/webconsole/new-console-output/test/fixtures/stubs/pageError.js b/devtools/client/webconsole/new-console-output/test/fixtures/stubs/pageError.js
--- a/devtools/client/webconsole/new-console-output/test/fixtures/stubs/pageError.js
+++ b/devtools/client/webconsole/new-console-output/test/fixtures/stubs/pageError.js
@@ -265,17 +265,17 @@ stubPackets.set(`ReferenceError: asdf is
 
 stubPackets.set(`SyntaxError: redeclaration of let a`, {
   "from": "server1.conn0.child1/consoleActor2",
   "type": "pageError",
   "pageError": {
     "errorMessage": "SyntaxError: redeclaration of let a",
     "errorMessageName": "JSMSG_REDECLARED_VAR",
     "sourceName": "http://example.com/browser/devtools/client/webconsole/new-console-output/test/fixtures/stub-generators/test-console-api.html",
-    "lineText": "  let a, a;\n",
+    "lineText": "  let a, a;",
     "lineNumber": 2,
     "columnNumber": 9,
     "category": "content javascript",
     "timeStamp": 1487992945524,
     "warning": false,
     "error": false,
     "exception": true,
     "strict": false,
diff --git a/js/src/frontend/TokenStream.cpp b/js/src/frontend/TokenStream.cpp
--- a/js/src/frontend/TokenStream.cpp
+++ b/js/src/frontend/TokenStream.cpp
@@ -39,16 +39,17 @@
 #include "vm/Realm.h"
 
 using mozilla::ArrayLength;
 using mozilla::AssertedCast;
 using mozilla::IsAscii;
 using mozilla::IsAsciiAlpha;
 using mozilla::IsAsciiDigit;
 using mozilla::MakeScopeExit;
+using mozilla::PointerRangeSize;
 
 struct ReservedWordInfo {
   const char* chars;  // C string with reserved word text
   js::frontend::TokenKind tokentype;
 };
 
 static const ReservedWordInfo reservedWords[] = {
 #define RESERVED_WORD_INFO(word, name, type) \
@@ -576,32 +577,67 @@ void TokenStreamChars<char16_t, AnyChars
   MOZ_ASSERT(SourceUnits::isRawEOLChar(last));
 
   if (last == '\n')
     this->sourceUnits.ungetOptionalCRBeforeLF();
 
   anyCharsAccess().undoInternalUpdateLineInfoForEOL();
 }
 
-template <typename CharT>
-size_t SourceUnits<CharT>::findEOLMax(size_t start, size_t max) {
-  const CharT* p = codeUnitPtrAt(start);
-
-  size_t n = 0;
+template<>
+size_t SourceUnits<char16_t>::findWindowEnd(size_t offset) {
+  const char16_t* const initial = codeUnitPtrAt(offset);
+  const char16_t* p = initial;
+
+  auto HalfWindowSize = [&initial, &p]() {
+    return PointerRangeSize(initial, p);
+  };
+
   while (true) {
-    if (p >= limit_) break;
-    if (n >= max) break;
-    n++;
+    MOZ_ASSERT(p <= limit_);
+    MOZ_ASSERT(HalfWindowSize() <= WindowRadius);
+    if (p >= limit_ || HalfWindowSize() >= WindowRadius)
+      break;
+
+    char16_t c = *p;
 
     // This stops at U+2028 LINE SEPARATOR or U+2029 PARAGRAPH SEPARATOR in
     // string and template literals.  These code points do affect line and
     // column coordinates, even as they encode their literal values.
-    if (isRawEOLChar(*p++)) break;
+    if (isRawEOLChar(c))
+      break;
+
+    // Don't allow invalid UTF-16 in post-context.  (Current users don't
+    // require this, and this behavior isn't currently imposed on
+    // pre-context, but these facts might change someday.)
+
+    if (MOZ_UNLIKELY(unicode::IsTrailSurrogate(c)))
+      break;
+
+    // Optimistically consume the code unit, ungetting it below if needed.
+    p++;
+
+    // If it's not a surrogate at all, keep going.
+    if (MOZ_LIKELY(!unicode::IsLeadSurrogate(c)))
+      continue;
+
+    // Retract if the lead surrogate would stand alone at the end of the
+    // window.
+    if (HalfWindowSize() >= WindowRadius || // split pair
+        p >= limit_ || // half-pair at end of source
+        !unicode::IsTrailSurrogate(*p)) // no paired trail surrogate
+    {
+      p--;
+      break;
+    }
+
+    p++;
   }
-  return start + n;
+
+  return offset + HalfWindowSize();
 }
 
 template <typename CharT, class AnyCharsAccess>
 bool TokenStreamSpecific<CharT, AnyCharsAccess>::advance(size_t position) {
   const CharT* end = this->sourceUnits.codeUnitPtrAt(position);
   while (this->sourceUnits.addressOfNextCodeUnit() < end) {
     int32_t c;
     if (!getCodePoint(&c))
@@ -756,33 +792,33 @@ bool TokenStreamSpecific<CharT, AnyChars
   TokenStreamAnyChars& anyChars = anyCharsAccess();
 
   // We only have line-start information for the current line.  If the error
   // is on a different line, we can't easily provide context.  (This means
   // any error in a multi-line token, e.g. an unterminated multiline string
   // literal, won't have context.)
   if (err->lineNumber != anyChars.lineno) return true;
 
-  constexpr size_t windowRadius = ErrorMetadata::lineOfContextRadius;
+  constexpr size_t windowRadius = SourceUnits::WindowRadius;
 
   // The window must start within the current line, no earlier than
   // |windowRadius| characters before |offset|.
   MOZ_ASSERT(offset >= anyChars.linebase);
   size_t windowStart = (offset - anyChars.linebase > windowRadius)
                            ? offset - windowRadius
                            : anyChars.linebase;
 
   // The window must start within the portion of the current line that we
   // actually have in our buffer.
   if (windowStart < this->sourceUnits.startOffset())
     windowStart = this->sourceUnits.startOffset();
 
   // The window must end within the current line, no later than
   // windowRadius after offset.
-  size_t windowEnd = this->sourceUnits.findEOLMax(offset, windowRadius);
+  size_t windowEnd = this->sourceUnits.findWindowEnd(offset);
   size_t windowLength = windowEnd - windowStart;
   MOZ_ASSERT(windowLength <= windowRadius * 2);
 
   // Create the windowed string, not including the potential line
   // terminator.
   StringBuffer windowBuf(anyChars.cx);
   if (!windowBuf.append(codeUnitPtrAt(windowStart), windowLength) ||
       !windowBuf.append('\0')) {
diff --git a/js/src/frontend/TokenStream.h b/js/src/frontend/TokenStream.h
--- a/js/src/frontend/TokenStream.h
+++ b/js/src/frontend/TokenStream.h
@@ -1049,19 +1049,35 @@ class SourceUnits {
 #endif
   }
 
   static bool isRawEOLChar(int32_t c) {
     return c == '\n' || c == '\r' || c == unicode::LINE_SEPARATOR ||
            c == unicode::PARA_SEPARATOR;
     }
 
-  // Returns the offset of the next EOL, but stops once 'max' characters
-  // have been scanned (*including* the char at startOffset_).
-  size_t findEOLMax(size_t start, size_t max);
+  /**
+   * The maximum radius of code around the location of an error that should
+   * be included in a syntax error message -- this many code units to either
+   * side.  The resulting window of data is then accordinngly trimmed so that
+   * the window contains only validly-encoded data.
+   *
+   * Because this number is the same for both UTF-8 and UTF-16, windows in
+   * UTF-8 may contain fewer code points than windows in UTF-16.  As we only
+   * use this for error messages, we don't particularly care.
+   */
+  static constexpr size_t WindowRadius = ErrorMetadata::lineOfContextRadius;
+
+  /**
+   * From absolute offset |offset|, find an absolute offset, no further than
+   * |WindowRadius| code units away from |offset|, such that all code units
+   * from |offset| to that offset encode valid, non-LineTerminator code
+   * points.
+   */
+  size_t findWindowEnd(size_t offset);
 
  private:
   /** Base of buffer. */
   const CharT* base_;
 
   /** Offset of base_[0]. */
   uint32_t startOffset_;
 
diff --git a/js/src/jsapi-tests/moz.build b/js/src/jsapi-tests/moz.build
--- a/js/src/jsapi-tests/moz.build
+++ b/js/src/jsapi-tests/moz.build
@@ -24,16 +24,17 @@ UNIFIED_SOURCES += [
     'testDebugger.cpp',
     'testDeepFreeze.cpp',
     'testDefineGetterSetterNonEnumerable.cpp',
     'testDefineProperty.cpp',
     'testDefinePropertyIgnoredAttributes.cpp',
     'testDeflateStringToUTF8Buffer.cpp',
     'testDifferentNewTargetInvokeConstructor.cpp',
     'testErrorCopying.cpp',
+    'testErrorLineOfContext.cpp',
     'testException.cpp',
     'testExecuteInJSMEnvironment.cpp',
     'testExternalArrayBuffer.cpp',
     'testExternalStrings.cpp',
     'testFindSCCs.cpp',
     'testForceLexicalInitialization.cpp',
     'testForOfIterator.cpp',
     'testForwardSetProperty.cpp',
diff --git a/js/src/jsapi-tests/testErrorLineOfContext.cpp b/js/src/jsapi-tests/testErrorLineOfContext.cpp
new file mode 100644
--- /dev/null
+++ b/js/src/jsapi-tests/testErrorLineOfContext.cpp
@@ -0,0 +1,71 @@
+/* This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+#include "jsfriendapi.h"
+#include "jsapi-tests/tests.h"
+#include "vm/ErrorReporting.h"
+
+BEGIN_TEST(testErrorLineOfContext)
+{
+    static const char16_t fullLineR[] = u"\n  var x = @;  \r  ";
+    CHECK(testLineOfContextHasNoLineTerminator(fullLineR, ' '));
+
+    static const char16_t fullLineN[] = u"\n  var x = @; !\n  ";
+    CHECK(testLineOfContextHasNoLineTerminator(fullLineN, '!'));
+
+    static const char16_t fullLineLS[] = u"\n  var x = @; +\u2028  ";
+    CHECK(testLineOfContextHasNoLineTerminator(fullLineLS, '+'));
+
+    static const char16_t fullLinePS[] = u"\n  var x = @; #\u2029  ";
+    CHECK(testLineOfContextHasNoLineTerminator(fullLinePS, '#'));
+
+    static_assert(js::ErrorMetadata::lineOfContextRadius == 60,
+                  "current max count past offset is 60, hits 'X' below");
+
+    static const char16_t truncatedLine[] =
+        u"@ + 4567890123456789012345678901234567890123456789012345678XYZW\n";
+    CHECK(testLineOfContextHasNoLineTerminator(truncatedLine, 'X'));
+
+    return true;
+}
+
+bool
+eval(const char16_t* chars, size_t len, JS::MutableHandleValue rval)
+{
+    JS::RealmOptions globalOptions;
+    JS::RootedObject global(cx, JS_NewGlobalObject(cx, getGlobalClass(), nullptr,
+						   JS::FireOnNewGlobalHook, globalOptions));
+    CHECK(global);
+
+    JSAutoRealm ar(cx, global);
+    JS::CompileOptions options(cx);
+    return JS::Evaluate(cx, options, chars, len, rval);
+}
+
+template<size_t N>
+bool
+testLineOfContextHasNoLineTerminator(const char16_t (&chars)[N], char16_t expectedLast)
+{
+    JS::RootedValue rval(cx);
+    CHECK(!eval(chars, N - 1, &rval));
+
+    JS::RootedValue exn(cx);
+    CHECK(JS_GetPendingException(cx, &exn));
+    JS_ClearPendingException(cx);
+
+    js::ErrorReport report(cx);
+    CHECK(report.init(cx, exn, js::ErrorReport::WithSideEffects));
+
+    const auto* errorReport = report.report();
+
+    const char16_t* lineOfContext = errorReport->linebuf();
+    size_t lineOfContextLength = errorReport->linebufLength();
+
+    CHECK(lineOfContext[lineOfContextLength] == '\0');
+    char16_t last = lineOfContext[lineOfContextLength - 1];
+    CHECK(last == expectedLast);
+
+    return true;
+}
+END_TEST(testErrorLineOfContext)
