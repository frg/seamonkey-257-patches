# HG changeset patch
# User Adrian Wielgosik <adrian.wielgosik@gmail.com>
# Date 1519920866 -3600
# Node ID 1a96c53c1c7bf5338cf426e8ffaab46ecd035447
# Parent  9754263a1749760244a032f5a0612728893967a8
Bug 1445408 - Remove nsIDOMClientRectList. r=bz

MozReview-Commit-ID: 22sQNVs0wFP

diff --git a/dom/base/DOMRect.cpp b/dom/base/DOMRect.cpp
--- a/dom/base/DOMRect.cpp
+++ b/dom/base/DOMRect.cpp
@@ -68,35 +68,23 @@ already_AddRefed<DOMRect> DOMRect::Const
 }
 
 // -----------------------------------------------------------------------------
 
 NS_IMPL_CYCLE_COLLECTION_WRAPPERCACHE(DOMRectList, mParent, mArray)
 
 NS_INTERFACE_TABLE_HEAD(DOMRectList)
   NS_WRAPPERCACHE_INTERFACE_TABLE_ENTRY
-  NS_INTERFACE_TABLE(DOMRectList, nsIDOMClientRectList)
+  NS_INTERFACE_TABLE0(DOMRectList)
   NS_INTERFACE_TABLE_TO_MAP_SEGUE_CYCLE_COLLECTION(DOMRectList)
 NS_INTERFACE_MAP_END
 
 NS_IMPL_CYCLE_COLLECTING_ADDREF(DOMRectList)
 NS_IMPL_CYCLE_COLLECTING_RELEASE(DOMRectList)
 
-NS_IMETHODIMP
-DOMRectList::GetLength(uint32_t* aLength) {
-  *aLength = Length();
-  return NS_OK;
-}
-
-NS_IMETHODIMP
-DOMRectList::Item(uint32_t aIndex, nsIDOMClientRect** aReturn) {
-  NS_IF_ADDREF(*aReturn = Item(aIndex));
-  return NS_OK;
-}
-
 JSObject* DOMRectList::WrapObject(JSContext* cx,
                                   JS::Handle<JSObject*> aGivenProto) {
   return mozilla::dom::DOMRectListBinding::Wrap(cx, this, aGivenProto);
 }
 
 static double RoundFloat(double aValue) { return floor(aValue + 0.5); }
 
 void DOMRect::SetLayoutRect(const nsRect& aLayoutRect) {
diff --git a/dom/base/DOMRect.h b/dom/base/DOMRect.h
--- a/dom/base/DOMRect.h
+++ b/dom/base/DOMRect.h
@@ -3,17 +3,16 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef MOZILLA_DOMRECT_H_
 #define MOZILLA_DOMRECT_H_
 
 #include "nsIDOMClientRect.h"
-#include "nsIDOMClientRectList.h"
 #include "nsTArray.h"
 #include "nsCOMPtr.h"
 #include "nsWrapperCache.h"
 #include "nsCycleCollectionParticipant.h"
 #include "mozilla/Attributes.h"
 #include "mozilla/dom/BindingDeclarations.h"
 #include "mozilla/ErrorResult.h"
 #include <algorithm>
@@ -109,50 +108,32 @@ class DOMRect final : public DOMRectRead
 
  protected:
   double mX, mY, mWidth, mHeight;
 
  private:
   ~DOMRect(){};
 };
 
-class DOMRectList final : public nsIDOMClientRectList, public nsWrapperCache {
+class DOMRectList final : public nsISupports, public nsWrapperCache {
   ~DOMRectList() {}
 
  public:
   explicit DOMRectList(nsISupports* aParent) : mParent(aParent) {}
 
   NS_DECL_CYCLE_COLLECTING_ISUPPORTS
   NS_DECL_CYCLE_COLLECTION_SCRIPT_HOLDER_CLASS(DOMRectList)
 
-  NS_DECL_NSIDOMCLIENTRECTLIST
-
   virtual JSObject* WrapObject(JSContext* cx,
                                JS::Handle<JSObject*> aGivenProto) override;
 
   nsISupports* GetParentObject() { return mParent; }
 
   void Append(DOMRect* aElement) { mArray.AppendElement(aElement); }
 
-  static DOMRectList* FromSupports(nsISupports* aSupports) {
-#ifdef DEBUG
-    {
-      nsCOMPtr<nsIDOMClientRectList> list_qi = do_QueryInterface(aSupports);
-
-      // If this assertion fires the QI implementation for the object in
-      // question doesn't use the nsIDOMClientRectList pointer as the
-      // nsISupports pointer. That must be fixed, or we'll crash...
-      NS_ASSERTION(list_qi == static_cast<nsIDOMClientRectList*>(aSupports),
-                   "Uh, fix QI!");
-    }
-#endif
-
-    return static_cast<DOMRectList*>(aSupports);
-  }
-
   uint32_t Length() { return mArray.Length(); }
   DOMRect* Item(uint32_t aIndex) { return mArray.SafeElementAt(aIndex); }
   DOMRect* IndexedGetter(uint32_t aIndex, bool& aFound) {
     aFound = aIndex < mArray.Length();
     if (!aFound) {
       return nullptr;
     }
     return mArray[aIndex];
diff --git a/dom/base/nsRange.cpp b/dom/base/nsRange.cpp
--- a/dom/base/nsRange.cpp
+++ b/dom/base/nsRange.cpp
@@ -3139,22 +3139,16 @@ static nsresult GetPartialTextRect(nsLay
     if (frame) {
       nsLayoutUtils::GetAllInFlowRectsAndTexts(
           frame, nsLayoutUtils::GetContainingBlockForClientRect(frame),
           aCollector, aTextList, nsLayoutUtils::RECTS_ACCOUNT_FOR_TRANSFORMS);
     }
   } while (!iter.IsDone());
 }
 
-NS_IMETHODIMP
-nsRange::GetBoundingClientRect(nsIDOMClientRect** aResult) {
-  *aResult = GetBoundingClientRect(true).take();
-  return NS_OK;
-}
-
 already_AddRefed<DOMRect> nsRange::GetBoundingClientRect(bool aClampToEdge,
                                                          bool aFlushLayout) {
   RefPtr<DOMRect> rect = new DOMRect(ToSupports(this));
   if (!mStart.Container()) {
     return rect.forget();
   }
 
   nsLayoutUtils::RectAccumulator accumulator;
@@ -3163,22 +3157,16 @@ already_AddRefed<DOMRect> nsRange::GetBo
                             aClampToEdge, aFlushLayout);
 
   nsRect r = accumulator.mResultRect.IsEmpty() ? accumulator.mFirstRect
                                                : accumulator.mResultRect;
   rect->SetLayoutRect(r);
   return rect.forget();
 }
 
-NS_IMETHODIMP
-nsRange::GetClientRects(nsIDOMClientRectList** aResult) {
-  *aResult = GetClientRects(true).take();
-  return NS_OK;
-}
-
 already_AddRefed<DOMRectList> nsRange::GetClientRects(bool aClampToEdge,
                                                       bool aFlushLayout) {
   if (!mStart.Container()) {
     return nullptr;
   }
 
   RefPtr<DOMRectList> rectList =
       new DOMRectList(static_cast<nsIDOMRange*>(this));
diff --git a/dom/interfaces/base/domstubs.idl b/dom/interfaces/base/domstubs.idl
--- a/dom/interfaces/base/domstubs.idl
+++ b/dom/interfaces/base/domstubs.idl
@@ -21,17 +21,16 @@ interface nsIDOMComment;
 interface nsIDOMDocument;
 interface nsIDOMDocumentFragment;
 interface nsIDOMElement;
 interface nsIDOMNode;
 interface nsIDOMNodeList;
 interface nsIDOMProcessingInstruction;
 interface nsIDOMText;
 interface nsIDOMClientRect;
-interface nsIDOMClientRectList;
 
 // Needed for raises() in our IDL
 %{C++
 namespace mozilla {
 namespace dom {
 class DOMException;
 }
 }
diff --git a/dom/interfaces/base/moz.build b/dom/interfaces/base/moz.build
--- a/dom/interfaces/base/moz.build
+++ b/dom/interfaces/base/moz.build
@@ -12,17 +12,16 @@ XPIDL_SOURCES += [
     'nsIBrowser.idl',
     'nsIBrowserDOMWindow.idl',
     'nsIContentPermissionPrompt.idl',
     'nsIContentPrefService2.idl',
     'nsIContentProcess.idl',
     'nsIContentURIGrouper.idl',
     'nsIDOMChromeWindow.idl',
     'nsIDOMClientRect.idl',
-    'nsIDOMClientRectList.idl',
     'nsIDOMConstructor.idl',
     'nsIDOMGlobalPropertyInitializer.idl',
     'nsIDOMScreen.idl',
     'nsIDOMWindow.idl',
     'nsIDOMWindowCollection.idl',
     'nsIDOMWindowUtils.idl',
     'nsIFocusManager.idl',
     'nsIIdleObserver.idl',
diff --git a/dom/interfaces/base/nsIDOMClientRectList.idl b/dom/interfaces/base/nsIDOMClientRectList.idl
deleted file mode 100644
--- a/dom/interfaces/base/nsIDOMClientRectList.idl
+++ /dev/null
@@ -1,13 +0,0 @@
-/* -*- Mode: IDL; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
-/* This Source Code Form is subject to the terms of the Mozilla Public
- * License, v. 2.0. If a copy of the MPL was not distributed with this
- * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
-
-#include "domstubs.idl"
-
-[uuid(f474c567-cbcb-458f-abad-ae42363da287)]
-interface nsIDOMClientRectList : nsISupports
-{
-  readonly attribute unsigned long length;
-  nsIDOMClientRect        item(in unsigned long index);
-};
diff --git a/dom/interfaces/range/nsIDOMRange.idl b/dom/interfaces/range/nsIDOMRange.idl
--- a/dom/interfaces/range/nsIDOMRange.idl
+++ b/dom/interfaces/range/nsIDOMRange.idl
@@ -64,14 +64,9 @@ interface nsIDOMRange : nsISupports
   //    1 if point is after the end boundary point.
   // Sort of a strcmp for ranges.
   short comparePoint(in nsIDOMNode parent, in unsigned long offset);
 
   /**
    * Returns whether the range intersects node.
    */
   boolean                   intersectsNode(in nsIDOMNode node);
-
-  // These methods come from 
-  // http://dev.w3.org/csswg/cssom-view/#extensions-to-the-range-interface
-  nsIDOMClientRectList getClientRects();
-  nsIDOMClientRect getBoundingClientRect();
 };
diff --git a/widget/android/nsAppShell.cpp b/widget/android/nsAppShell.cpp
--- a/widget/android/nsAppShell.cpp
+++ b/widget/android/nsAppShell.cpp
@@ -16,17 +16,16 @@
 #include "nsThreadUtils.h"
 #include "nsICommandLineRunner.h"
 #include "nsICrashReporter.h"
 #include "nsIObserverService.h"
 #include "nsIAppStartup.h"
 #include "nsIGeolocationProvider.h"
 #include "nsCacheService.h"
 #include "nsIDOMEventListener.h"
-#include "nsIDOMClientRectList.h"
 #include "nsIDOMClientRect.h"
 #include "nsIDOMWakeLockListener.h"
 #include "nsIPowerManagerService.h"
 #include "nsISpeculativeConnect.h"
 #include "nsITabChild.h"
 #include "nsIURIFixup.h"
 #include "nsCategoryManagerUtils.h"
 #include "nsCDefaultURIFixup.h"
diff --git a/xpcom/reflect/xptinfo/ShimInterfaceInfo.cpp b/xpcom/reflect/xptinfo/ShimInterfaceInfo.cpp
--- a/xpcom/reflect/xptinfo/ShimInterfaceInfo.cpp
+++ b/xpcom/reflect/xptinfo/ShimInterfaceInfo.cpp
@@ -4,17 +4,16 @@
  * This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "ShimInterfaceInfo.h"
 
 #include "nsIDOMCharacterData.h"
 #include "nsIDOMClientRect.h"
-#include "nsIDOMClientRectList.h"
 #include "nsIDOMComment.h"
 #include "nsIDOMCustomEvent.h"
 #ifdef MOZ_WEBRTC
 #include "nsIDOMDataChannel.h"
 #endif
 #include "nsIDOMDOMCursor.h"
 #include "nsIDOMDOMException.h"
 #include "nsIDOMDOMRequest.h"
@@ -50,17 +49,16 @@
 #include "nsIFrameLoader.h"
 #include "nsIListBoxObject.h"
 #include "nsISelection.h"
 #include "nsITreeBoxObject.h"
 #include "nsIWebBrowserPersistable.h"
 
 #include "mozilla/dom/CharacterDataBinding.h"
 #include "mozilla/dom/DOMRectBinding.h"
-#include "mozilla/dom/DOMRectListBinding.h"
 #include "mozilla/dom/CommentBinding.h"
 #include "mozilla/dom/CSSPrimitiveValueBinding.h"
 #include "mozilla/dom/CSSStyleDeclarationBinding.h"
 #include "mozilla/dom/CSSStyleSheetBinding.h"
 #include "mozilla/dom/CSSValueBinding.h"
 #include "mozilla/dom/CSSValueListBinding.h"
 #include "mozilla/dom/CustomEventBinding.h"
 #include "mozilla/dom/DOMCursorBinding.h"
@@ -169,17 +167,16 @@ struct ComponentsInterfaceShimEntry {
  *   interface only has an "nsIDOM" prefix prepended to the WebIDL name, you
  *   can use the DEFINE_SHIM macro and pass in the name of the WebIDL
  *   interface.  Otherwise, use DEFINE_SHIM_WITH_CUSTOM_INTERFACE.
  */
 
 const ComponentsInterfaceShimEntry kComponentsInterfaceShimMap[] = {
     DEFINE_SHIM(CharacterData),
     DEFINE_SHIM_WITH_CUSTOM_INTERFACE(nsIDOMClientRect, DOMRectReadOnly),
-    DEFINE_SHIM_WITH_CUSTOM_INTERFACE(nsIDOMClientRectList, DOMRectList),
     DEFINE_SHIM(Comment),
     DEFINE_SHIM(CustomEvent),
     DEFINE_SHIM(DOMCursor),
     DEFINE_SHIM(DOMException),
     DEFINE_SHIM(DOMRequest),
     DEFINE_SHIM(Document),
     DEFINE_SHIM(DocumentFragment),
     DEFINE_SHIM(DragEvent),
