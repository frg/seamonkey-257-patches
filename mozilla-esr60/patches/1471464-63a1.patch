# HG changeset patch
# User Jeff Walden <jwalden@mit.edu>
# Date 1530131173 25200
# Node ID f3b134e1e8f154a9df09490a459b8cf6fc218c34
# Parent  134496feb08a1adb5653fd437f267ea1c787cf92
Bug 1471464 - Make getNonAsciiCodePoint, getFullAsciiCodePoint, and isAsciiCodePoint all take |int32_t| and not |CharT|, because they're all passed |int32_t| from |getCodeUnit()| or a code point-getting mechanism.  r=arai

diff --git a/js/src/frontend/TokenStream.cpp b/js/src/frontend/TokenStream.cpp
--- a/js/src/frontend/TokenStream.cpp
+++ b/js/src/frontend/TokenStream.cpp
@@ -502,17 +502,18 @@ bool TokenStreamChars<char16_t, AnyChars
   if (!updateLineInfoForEOL()) return false;
 
   *cp = '\n';
   return true;
 }
 
 template<class AnyCharsAccess>
 bool TokenStreamChars<char16_t, AnyCharsAccess>::getNonAsciiCodePoint(
-    char16_t lead, int32_t* codePoint) {
+    int32_t lead, int32_t* codePoint) {
+  MOZ_ASSERT(lead != EOF);
   MOZ_ASSERT(!isAsciiCodePoint(lead),
              "ASCII code unit/point must be handled separately");
   MOZ_ASSERT(lead == sourceUnits.previousCodeUnit(),
              "getNonAsciiCodePoint called incorrectly");
 
   // The code point is usually |lead|: overwrite later if needed.
   *codePoint = lead;
 
@@ -1570,17 +1571,19 @@ MOZ_MUST_USE bool TokenStreamSpecific<Ch
 }
 
 template<typename CharT, class AnyCharsAccess>
 MOZ_MUST_USE bool TokenStreamSpecific<CharT, AnyCharsAccess>::regexpLiteral(
     TokenStart start, TokenKind* out) {
   MOZ_ASSERT(sourceUnits.previousCodeUnit() == '/');
   charBuffer.clear();
 
-  auto ProcessNonAsciiCodePoint = [this](CharT lead) {
+  auto ProcessNonAsciiCodePoint = [this](int32_t lead) {
+    MOZ_ASSERT(lead != EOF);
+
     int32_t codePoint;
     if (!this->getNonAsciiCodePoint(lead, &codePoint))
       return false;
 
     if (codePoint == '\n') {
       this->ungetLineTerminator();
       this->reportError(JSMSG_UNTERMINATED_REGEXP);
       return false;
diff --git a/js/src/frontend/TokenStream.h b/js/src/frontend/TokenStream.h
--- a/js/src/frontend/TokenStream.h
+++ b/js/src/frontend/TokenStream.h
@@ -1052,16 +1052,26 @@ class TokenStreamCharsShared {
  protected:
   explicit TokenStreamCharsShared(JSContext* cx) : charBuffer(cx) {}
 
   MOZ_MUST_USE bool appendCodePointToCharBuffer(uint32_t codePoint);
 
   MOZ_MUST_USE bool copyCharBufferTo(
       JSContext* cx, UniquePtr<char16_t[], JS::FreePolicy>* destination);
 
+  /**
+   * Determine whether a code unit constitutes a complete ASCII code point.
+   * (The code point's exact value might not be used, however, if subsequent
+   * code observes that |unit| is part of a LineTerminatorSequence.)
+   */
+  static constexpr MOZ_ALWAYS_INLINE MOZ_MUST_USE bool isAsciiCodePoint(
+      int32_t unit) {
+    return mozilla::IsAscii(unit);
+  }
+
  public:
   CharBuffer& getCharBuffer() { return charBuffer; }
 };
 
 template <typename CharT>
 class TokenStreamCharsBase : public TokenStreamCharsShared {
  protected:
   void ungetCodeUnit(int32_t c) {
@@ -1122,26 +1132,16 @@ class TokenStreamCharsBase : public Toke
         return false;
 
       cur++;
     }
 
     return true;
   }
 
-  /**
-   * Determine whether a code unit constitutes a complete ASCII code point.
-   * (The code point's exact value might not be used, however, if subsequent
-   * code observes that |unit| is part of a LineTerminatorSequence.)
-   */
-  static constexpr MOZ_ALWAYS_INLINE MOZ_MUST_USE bool isAsciiCodePoint(
-      CharT unit) {
-    return mozilla::IsAscii(unit);
-  }
-
  protected:
   /** Code units in the source code being tokenized. */
   SourceUnits sourceUnits;
 };
 
 template <>
 /* static */ MOZ_ALWAYS_INLINE JSAtom*
 TokenStreamCharsBase<char16_t>::atomizeChars(JSContext* cx,
@@ -1309,22 +1309,22 @@ class TokenStreamChars<char16_t, AnyChar
   using GeneralCharsBase = GeneralTokenStreamChars<char16_t, AnyCharsAccess>;
   using Self = TokenStreamChars<char16_t, AnyCharsAccess>;
 
   using GeneralCharsBase::asSpecific;
 
   using typename GeneralCharsBase::TokenStreamSpecific;
 
  protected:
-  using CharsBase::isAsciiCodePoint;
   using CharsBase::sourceUnits;
   using GeneralCharsBase::anyCharsAccess;
   using GeneralCharsBase::getCodeUnit;
   using GeneralCharsBase::ungetCodeUnit;
   using GeneralCharsBase::updateLineInfoForEOL;
+  using TokenStreamCharsShared::isAsciiCodePoint;
 
   using typename GeneralCharsBase::SourceUnits;
 
  protected:
   using GeneralCharsBase::GeneralCharsBase;
 
   // Try to get the next code point, normalizing '\r', '\r\n', '\n', and the
   // Unicode line/paragraph separators into '\n'.  Also updates internal
@@ -1343,17 +1343,17 @@ class TokenStreamChars<char16_t, AnyChar
    * point or LineTerminatorSequence (normalizing it to '\n') and store it in
    * |*codePoint|.  Return true on success, otherwise return false and leave
    * |*codePoint| undefined on failure.
    *
    * If a LineTerminatorSequence was consumed, also update line/column info.
    *
    * This may change the current |sourceUnits| offset.
    */
-  MOZ_MUST_USE bool getFullAsciiCodePoint(char16_t lead, int32_t* codePoint) {
+  MOZ_MUST_USE bool getFullAsciiCodePoint(int32_t lead, int32_t* codePoint) {
     MOZ_ASSERT(isAsciiCodePoint(lead),
                "non-ASCII code units must be handled separately");
     MOZ_ASSERT(lead == sourceUnits.previousCodeUnit(),
                "getFullAsciiCodePoint called incorrectly");
 
     if (MOZ_UNLIKELY(lead == '\r')) {
       if (MOZ_LIKELY(!sourceUnits.atEnd()))
         sourceUnits.matchCodeUnit('\n');
@@ -1378,17 +1378,17 @@ class TokenStreamChars<char16_t, AnyChar
    * consume a full code point or LineTerminatorSequence (normalizing it to
    * '\n') and store it in |*codePoint|.  Return true on success, otherwise
    * return false and leave |*codePoint| undefined on failure.
    *
    * If a LineTerminatorSequence was consumed, also update line/column info.
    *
    * This may change the current |sourceUnits| offset.
    */
-  MOZ_MUST_USE bool getNonAsciiCodePoint(char16_t lead, int32_t* cp);
+  MOZ_MUST_USE bool getNonAsciiCodePoint(int32_t lead, int32_t* cp);
 
   /**
    * Unget a full code point (ASCII or not) without altering line/column
    * state.  If line/column state must be updated, this must happen manually.
    * This method ungets a single code point, not a LineTerminatorSequence
    * that is multiple code points.  (Generally you shouldn't be in a state
    * where you've just consumed "\r\n" and want to unget that full sequence.)
    *
@@ -1397,17 +1397,17 @@ class TokenStreamChars<char16_t, AnyChar
    */
   void ungetCodePointIgnoreEOL(uint32_t codePoint);
 
   /**
    * Unget an originally non-ASCII, normalized code point, including undoing
    * line/column updates that were performed for it.  Don't use this if the
    * code point was gotten *without* line/column state being updated!
    */
-  void ungetNonAsciiNormalizedCodePoint(uint32_t codePoint) {
+  void ungetNonAsciiNormalizedCodePoint(int32_t codePoint) {
     MOZ_ASSERT_IF(isAsciiCodePoint(codePoint), codePoint == '\n');
     MOZ_ASSERT(codePoint != unicode::LINE_SEPARATOR,
                "should not be ungetting un-normalized code points");
     MOZ_ASSERT(codePoint != unicode::PARA_SEPARATOR,
                "should not be ungetting un-normalized code points");
 
     ungetCodePointIgnoreEOL(codePoint);
     if (codePoint == '\n')
@@ -1491,17 +1491,16 @@ class MOZ_STACK_CLASS TokenStreamSpecifi
 
  private:
   using typename CharsBase::SourceUnits;
 
  private:
   using CharsBase::atomizeChars;
   using CharsBase::consumeKnownCodeUnit;
   using CharsBase::fillCharBufferWithTemplateStringContents;
-  using CharsBase::isAsciiCodePoint;
   using CharsBase::matchCodeUnit;
   using CharsBase::peekCodeUnit;
   using CharsBase::sourceUnits;
   using GeneralCharsBase::badToken;
   using GeneralCharsBase::consumeRestOfSingleLineComment;
   using GeneralCharsBase::getCodeUnit;
   using GeneralCharsBase::matchUnicodeEscapeIdent;
   using GeneralCharsBase::matchUnicodeEscapeIdStart;
@@ -1517,16 +1516,17 @@ class MOZ_STACK_CLASS TokenStreamSpecifi
   using SpecializedCharsBase::getCodePoint;
   using SpecializedCharsBase::getFullAsciiCodePoint;
   using SpecializedCharsBase::getNonAsciiCodePoint;
   using SpecializedCharsBase::ungetCodePointIgnoreEOL;
   using SpecializedCharsBase::ungetNonAsciiNormalizedCodePoint;
   using TokenStreamCharsShared::appendCodePointToCharBuffer;
   using TokenStreamCharsShared::charBuffer;
   using TokenStreamCharsShared::copyCharBufferTo;
+  using TokenStreamCharsShared::isAsciiCodePoint;
 
   template<typename CharU>
   friend class TokenStreamPosition;
 
  public:
   TokenStreamSpecific(JSContext* cx, const ReadOnlyCompileOptions& options,
                       const CharT* base, size_t length);
 
