# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1529571942 -7200
# Node ID 72a1c5b1acb9e723325b71515c1017659ba7d4ac
# Parent  c30f9588aaf83cff1be494e0b3720b5b21a4eb7a
Bug 1469217 part 3 - Clean up TryResolvePropertyFromSpecs; remove a bunch of macros. r=anba

diff --git a/js/src/jsapi.h b/js/src/jsapi.h
--- a/js/src/jsapi.h
+++ b/js/src/jsapi.h
@@ -1417,60 +1417,39 @@ struct JSPropertySpec {
     MOZ_ASSERT(!accessors.getter.selfHosted.unused);
     MOZ_ASSERT(!accessors.setter.selfHosted.unused);
   }
 };
 
 namespace JS {
 namespace detail {
 
-/* NEVER DEFINED, DON'T USE.  For use by JS_CAST_NATIVE_TO only. */
-inline int CheckIsNative(JSNative native);
-
 /* NEVER DEFINED, DON'T USE.  For use by JS_CAST_STRING_TO only. */
 template <size_t N>
 inline int CheckIsCharacterLiteral(const char (&arr)[N]);
 
 /* NEVER DEFINED, DON'T USE.  For use by JS_CAST_INT32_TO only. */
 inline int CheckIsInt32(int32_t value);
 
-/* NEVER DEFINED, DON'T USE.  For use by JS_PROPERTYOP_GETTER only. */
-inline int CheckIsGetterOp(JSGetterOp op);
-
-/* NEVER DEFINED, DON'T USE.  For use by JS_PROPERTYOP_SETTER only. */
-inline int CheckIsSetterOp(JSSetterOp op);
-
 }  // namespace detail
 }  // namespace JS
 
-#define JS_CAST_NATIVE_TO(v, To)                            \
-  (static_cast<void>(sizeof(JS::detail::CheckIsNative(v))), \
-   reinterpret_cast<To>(v))
-
 #define JS_CAST_STRING_TO(s, To)                                      \
   (static_cast<void>(sizeof(JS::detail::CheckIsCharacterLiteral(s))), \
    reinterpret_cast<To>(s))
 
 #define JS_CAST_INT32_TO(s, To)                            \
   (static_cast<void>(sizeof(JS::detail::CheckIsInt32(s))), \
    reinterpret_cast<To>(s))
 
 #define JS_CHECK_ACCESSOR_FLAGS(flags)                                     \
   (static_cast<mozilla::EnableIf<                                          \
        ((flags) & ~(JSPROP_ENUMERATE | JSPROP_PERMANENT)) == 0>::Type>(0), \
    (flags))
 
-#define JS_PROPERTYOP_GETTER(v)                               \
-  (static_cast<void>(sizeof(JS::detail::CheckIsGetterOp(v))), \
-   reinterpret_cast<JSNative>(v))
-
-#define JS_PROPERTYOP_SETTER(v)                               \
-  (static_cast<void>(sizeof(JS::detail::CheckIsSetterOp(v))), \
-   reinterpret_cast<JSNative>(v))
-
 #define JS_PS_ACCESSOR_SPEC(name, getter, setter, flags, extraFlags) \
   {                                                                  \
     name, uint8_t(JS_CHECK_ACCESSOR_FLAGS(flags) | extraFlags), {    \
       { getter, setter }                                             \
     }                                                                \
   }
 #define JS_PS_VALUE_SPEC(name, value, flags)          \
   {                                                   \
diff --git a/js/xpconnect/wrappers/XrayWrapper.cpp b/js/xpconnect/wrappers/XrayWrapper.cpp
--- a/js/xpconnect/wrappers/XrayWrapper.cpp
+++ b/js/xpconnect/wrappers/XrayWrapper.cpp
@@ -429,45 +429,41 @@ static bool TryResolvePropertyFromSpecs(
     // JS_GetPropertyById at the top of JSXrayTraits::resolveOwnProperty.
     //
     // Note also that the public-facing API here doesn't give us a way to
     // pass along JITInfo. It's probably ok though, since Xrays are already
     // pretty slow.
     desc.value().setUndefined();
     unsigned flags = psMatch->flags;
     if (psMatch->isAccessor()) {
-      RootedFunction getterObj(cx);
-      RootedFunction setterObj(cx);
+      RootedObject getterObj(cx);
+      RootedObject setterObj(cx);
       if (psMatch->isSelfHosted()) {
-        getterObj = JS::GetSelfHostedFunction(
+        JSFunction* getterFun = JS::GetSelfHostedFunction(
             cx, psMatch->accessors.getter.selfHosted.funname, id, 0);
-        if (!getterObj) return false;
-        desc.setGetterObject(JS_GetFunctionObject(getterObj));
+        if (!getterFun)
+          return false;
+        getterObj = JS_GetFunctionObject(getterFun);
         if (psMatch->accessors.setter.selfHosted.funname) {
           MOZ_ASSERT(flags & JSPROP_SETTER);
-          setterObj = JS::GetSelfHostedFunction(
+          JSFunction* setterFun = JS::GetSelfHostedFunction(
               cx, psMatch->accessors.setter.selfHosted.funname, id, 0);
-          if (!setterObj) return false;
-          desc.setSetterObject(JS_GetFunctionObject(setterObj));
+          if (!setterFun)
+            return false;
+          setterObj = JS_GetFunctionObject(setterFun);
         }
+        if (!JS_DefinePropertyById(cx, holder, id, getterObj, setterObj, flags))
+          return false;
       } else {
-        desc.setGetter(
-            JS_CAST_NATIVE_TO(psMatch->accessors.getter.native.op, JSGetterOp));
-        desc.setSetter(
-            JS_CAST_NATIVE_TO(psMatch->accessors.setter.native.op, JSSetterOp));
-      }
-      desc.setAttributes(flags);
-      if (!JS_DefinePropertyById(cx, holder, id,
-                                 JS_PROPERTYOP_GETTER(desc.getter()),
-                                 JS_PROPERTYOP_SETTER(desc.setter()),
-                                 // This particular descriptor, unlike most,
-                                 // actually stores JSNatives directly,
-                                 // since we just set it up.
-                                 desc.attributes())) {
-        return false;
+        if (!JS_DefinePropertyById(cx, holder, id,
+                                   psMatch->accessors.getter.native.op,
+                                   psMatch->accessors.setter.native.op,
+                                   flags)) {
+          return false;
+        }
       }
     } else {
       RootedValue v(cx);
       if (!psMatch->getValue(cx, &v)) return false;
       if (!JS_DefinePropertyById(cx, holder, id, v,
                                  flags & ~JSPROP_INTERNAL_USE_BIT))
         return false;
     }
