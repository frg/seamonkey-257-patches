# HG changeset patch
# User Ted Campbell <tcampbell@mozilla.com>
# Date 1523415194 14400
# Node ID 0d3b241f3634887b37db68b2b0114743dc27e28f
# Parent  37ffe1efd0c842d1d9a41ff279907cef8b3e563b
Bug 1448563 - Part 3a: Add iterator to js::Fifo. r=luke

MozReview-Commit-ID: CoYdOBNnORg

diff --git a/js/src/ds/Fifo.h b/js/src/ds/Fifo.h
--- a/js/src/ds/Fifo.h
+++ b/js/src/ds/Fifo.h
@@ -74,16 +74,44 @@ class Fifo {
     return front_.length() + rear_.length();
   }
 
   bool empty() const {
     MOZ_ASSERT_IF(rear_.length() > 0, front_.length() > 0);  // Invariant 4.
     return front_.empty();
   }
 
+  // Iterator from oldest to yongest element.
+  struct ConstIterator {
+    const Fifo& self_;
+    size_t idx_;
+
+    ConstIterator(const Fifo& self, size_t idx) : self_(self), idx_(idx) {}
+
+    ConstIterator& operator++() {
+      ++idx_;
+      return *this;
+    }
+
+    const T& operator*() const {
+      // Iterate front in reverse, then rear.
+      size_t split = self_.front_.length();
+      return (idx_ < split) ? self_.front_[(split - 1) - idx_]
+                            : self_.rear_[idx_ - split];
+    }
+
+    bool operator!=(const ConstIterator& other) const {
+      return (&self_ != &other.self_) || (idx_ != other.idx_);
+    }
+  };
+
+  ConstIterator begin() const { return ConstIterator(*this, 0); }
+
+  ConstIterator end() const { return ConstIterator(*this, length()); }
+
   // Push an element to the back of the queue. This method can take either a
   // |const T&| or a |T&&|.
   template <typename U>
   MOZ_MUST_USE bool pushBack(U&& u) {
     if (!rear_.append(mozilla::Forward<U>(u))) return false;
     fixup();
     return true;
   }
