# HG changeset patch
# User Jeff Walden <jwalden@mit.edu>
# Date 1526529378 25200
# Node ID 6ceb0b9f6c804911e1d769307b429fc72d1beef0
# Parent  00460e9ca359285c12a6f7476bf3d143f1c3ffd7
Bug 1461556 - Replace a PodZero of js::gcstats::Statistics::totalTimes_ with a loop asserting every element was default-initialized to zero.  r=jandem

diff --git a/js/src/gc/Statistics.cpp b/js/src/gc/Statistics.cpp
--- a/js/src/gc/Statistics.cpp
+++ b/js/src/gc/Statistics.cpp
@@ -10,16 +10,17 @@
 #include "mozilla/DebugOnly.h"
 #include "mozilla/PodOperations.h"
 #include "mozilla/Sprintf.h"
 #include "mozilla/TimeStamp.h"
 
 #include <ctype.h>
 #include <stdarg.h>
 #include <stdio.h>
+#include <type_traits>
 
 #include "jsutil.h"
 
 #include "gc/GC.h"
 #include "gc/Memory.h"
 #include "util/Text.h"
 #include "vm/Debugger.h"
 #include "vm/HelperThreads.h"
@@ -638,17 +639,37 @@ Statistics::Statistics(JSRuntime* rt)
       triggerThreshold(0.0),
       maxPauseInInterval(0),
       sliceCallback(nullptr),
       nurseryCollectionCallback(nullptr),
       aborted(false),
       enableProfiling_(false),
       sliceCount_(0) {
   for (auto& count : counts) count = 0;
-  PodZero(&totalTimes_);
+
+#ifdef DEBUG
+  for (const auto& duration : totalTimes_) {
+#if defined(XP_WIN) || defined(XP_MACOSX) || (defined(XP_UNIX) && !defined(__clang__))
+    // build-linux64-asan/debug and static-analysis-linux64-st-an/debug
+    // currently use an STL that lacks std::is_trivially_constructible.
+    // This #ifdef probably isn't as precise as it could be, but given
+    // |totalTimes_| contains |TimeDuration| defined platform-independently
+    // it's not worth the trouble to nail down a more exact condition.
+    using ElementType =
+        typename mozilla::RemoveReference<decltype(duration)>::Type;
+    static_assert(!std::is_trivially_constructible<ElementType>::value,
+                  "Statistics::Statistics will only initialize "
+                  "totalTimes_'s elements if their default constructor is "
+                  "non-trivial");
+#endif  // mess'o'tests
+    MOZ_ASSERT(duration.IsZero(),
+               "totalTimes_ default-initialization should have "
+               "default-initialized every element of totalTimes_ to zero");
+  }
+#endif
 
   MOZ_ALWAYS_TRUE(phaseStack.reserve(MAX_PHASE_NESTING));
   MOZ_ALWAYS_TRUE(suspendedPhases.reserve(MAX_SUSPENDED_PHASES));
 
   const char* env = getenv("MOZ_GCTIMER");
   if (env) {
     if (strcmp(env, "none") == 0) {
       fp = nullptr;
