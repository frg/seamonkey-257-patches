# HG changeset patch
# User Jon Coppeard <jcoppeard@mozilla.com>
# Date 1532946111 -3600
# Node ID 7b8baf03f67e386546c9951a4241bc62f6dbbbe9
# Parent  8f3d818e67c147dcdeebac5c812759722c38b9d9
Bug 1478402 - Fix race reading/writing atoms zone concurrent use flag r=sfink

diff --git a/js/src/gc/Allocator.cpp b/js/src/gc/Allocator.cpp
--- a/js/src/gc/Allocator.cpp
+++ b/js/src/gc/Allocator.cpp
@@ -470,16 +470,20 @@ void Arena::arenaAllocatedDuringGC() {
       TenuredCell* cell = iter.getCell();
       MOZ_ASSERT(!cell->isMarkedAny());
       cell->markBlack();
     }
   }
 }
 
 void GCRuntime::setParallelAtomsAllocEnabled(bool enabled) {
+  // This can only be changed on the main thread otherwise we could race.
+  MOZ_ASSERT(CurrentThreadCanAccessRuntime(rt));
+  MOZ_ASSERT(enabled == rt->hasHelperThreadZones());
+
   atomsZone->arenas.setParallelAllocEnabled(enabled);
 }
 
 void ArenaLists::setParallelAllocEnabled(bool enabled) {
   MOZ_ASSERT(zone_->isAtomsZone());
 
   static const ConcurrentUse states[2] = {
     ConcurrentUse::None,
diff --git a/js/src/vm/HelperThreads.cpp b/js/src/vm/HelperThreads.cpp
--- a/js/src/vm/HelperThreads.cpp
+++ b/js/src/vm/HelperThreads.cpp
@@ -1832,17 +1832,20 @@ void JSContext::addPendingOutOfMemory() 
 void HelperThread::handleParseWorkload(AutoLockHelperThreadState& locked) {
   MOZ_ASSERT(HelperThreadState().canStartParseTask(locked));
   MOZ_ASSERT(idle());
 
   currentTask.emplace(HelperThreadState().parseWorklist(locked).popCopy());
   ParseTask* task = parseTask();
 
   JSRuntime* runtime = task->parseGlobal->runtimeFromAnyThread();
+
+#ifdef DEBUG
   runtime->incOffThreadParsesRunning();
+#endif
 
   {
     AutoUnlockHelperThreadState unlock(locked);
     AutoSetContextRuntime ascr(runtime);
 
     JSContext* cx = TlsContext.get();
 
     Zone* zone = task->parseGlobal->zoneFromAnyThread();
@@ -1860,17 +1863,19 @@ void HelperThread::handleParseWorkload(A
 
   // The callback is invoked while we are still off thread.
   task->callback(task, task->callbackData);
 
   // FinishOffThreadScript will need to be called on the script to
   // migrate it into the correct compartment.
   HelperThreadState().parseFinishedList(locked).insertBack(task);
 
+#ifdef DEBUG
   runtime->decOffThreadParsesRunning();
+#endif
 
   currentTask.reset();
 
   // Notify the main thread in case it is waiting for the parse/emit to finish.
   HelperThreadState().notifyAll(GlobalHelperThreadState::CONSUMER, locked);
 }
 
 void HelperThread::handleCompressionWorkload(
diff --git a/js/src/vm/Runtime.cpp b/js/src/vm/Runtime.cpp
--- a/js/src/vm/Runtime.cpp
+++ b/js/src/vm/Runtime.cpp
@@ -175,16 +175,19 @@ JSRuntime::JSRuntime(JSRuntime* parentRu
 JSRuntime::~JSRuntime() {
   JS_COUNT_DTOR(JSRuntime);
   MOZ_ASSERT(!initialized_);
 
   DebugOnly<size_t> oldCount = liveRuntimesCount--;
   MOZ_ASSERT(oldCount > 0);
 
   MOZ_ASSERT(wasmInstances.lock()->empty());
+
+  MOZ_ASSERT(offThreadParsesRunning_ == 0);
+  MOZ_ASSERT(!offThreadParsingBlocked_);
 }
 
 bool JSRuntime::init(JSContext* cx, uint32_t maxbytes,
                      uint32_t maxNurseryBytes) {
 #ifdef DEBUG
   MOZ_ASSERT(!initialized_);
   initialized_ = true;
 #endif
@@ -690,24 +693,29 @@ bool JSRuntime::activeGCInAtomsZone() {
           !gc.isVerifyPreBarriersEnabled()) ||
          zone->wasGCStarted();
 }
 
 void JSRuntime::setUsedByHelperThread(Zone* zone) {
   MOZ_ASSERT(!zone->usedByHelperThread());
   MOZ_ASSERT(!zone->wasGCStarted());
   MOZ_ASSERT(!isOffThreadParsingBlocked());
+
   zone->setUsedByHelperThread();
-  numActiveHelperThreadZones++;
+  if (numActiveHelperThreadZones++ == 0)
+    gc.setParallelAtomsAllocEnabled(true);
 }
 
 void JSRuntime::clearUsedByHelperThread(Zone* zone) {
   MOZ_ASSERT(zone->usedByHelperThread());
+
   zone->clearUsedByHelperThread();
-  numActiveHelperThreadZones--;
+  if (--numActiveHelperThreadZones == 0)
+    gc.setParallelAtomsAllocEnabled(false);
+
   JSContext* cx = mainContextFromOwnThread();
   if (gc.fullGCForAtomsRequested() && cx->canCollectAtoms())
     gc.triggerFullGCForAtoms(cx);
 }
 
 bool js::CurrentThreadCanAccessRuntime(const JSRuntime* rt) {
   return rt->mainContextFromAnyThread() == TlsContext.get();
 }
diff --git a/js/src/vm/Runtime.h b/js/src/vm/Runtime.h
--- a/js/src/vm/Runtime.h
+++ b/js/src/vm/Runtime.h
@@ -847,18 +847,18 @@ struct JSRuntime : public js::MallocProv
   void addSizeOfIncludingThis(mozilla::MallocSizeOf mallocSizeOf,
                               JS::RuntimeSizes* rtSizes);
 
  private:
   // Settings for how helper threads can be used.
   mozilla::Atomic<bool> offthreadIonCompilationEnabled_;
   mozilla::Atomic<bool> parallelParsingEnabled_;
 
+#ifdef DEBUG
   mozilla::Atomic<uint32_t> offThreadParsesRunning_;
-#ifdef DEBUG
   mozilla::Atomic<bool> offThreadParsingBlocked_;
 #endif
 
   js::MainThreadData<bool> autoWritableJitCodeActive_;
 
  public:
   // Note: these values may be toggled dynamically (in response to about:config
   // prefs changing).
@@ -868,31 +868,27 @@ struct JSRuntime : public js::MallocProv
   bool canUseOffthreadIonCompilation() const {
     return offthreadIonCompilationEnabled_;
   }
   void setParallelParsingEnabled(bool value) {
     parallelParsingEnabled_ = value;
   }
   bool canUseParallelParsing() const { return parallelParsingEnabled_; }
 
+#ifdef DEBUG
   void incOffThreadParsesRunning() {
     MOZ_ASSERT(!isOffThreadParsingBlocked());
-    if (!offThreadParsesRunning_)
-      gc.setParallelAtomsAllocEnabled(true);
     offThreadParsesRunning_++;
   }
 
   void decOffThreadParsesRunning() {
     MOZ_ASSERT(isOffThreadParseRunning());
     offThreadParsesRunning_--;
-    if (!offThreadParsesRunning_)
-      gc.setParallelAtomsAllocEnabled(false);
   }
 
-#ifdef DEBUG
   bool isOffThreadParseRunning() const { return offThreadParsesRunning_; }
 
   bool isOffThreadParsingBlocked() const { return offThreadParsingBlocked_; }
   void setOffThreadParsingBlocked(bool blocked) {
     MOZ_ASSERT(offThreadParsingBlocked_ != blocked);
     MOZ_ASSERT(!isOffThreadParseRunning());
     offThreadParsingBlocked_ = blocked;
   }
