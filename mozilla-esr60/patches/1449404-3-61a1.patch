# HG changeset patch
# User Boris Zbarsky <bzbarsky@mit.edu>
# Date 1522274507 14400
# Node ID 9e4c2c101487267af3d883bf5a91c6dd2ec55774
# Parent  21db1f8a58a68c75fb45fb8d8adb09c9c426679d
Bug 1449404 part 3.  Get rid of nsIContent::AppendTextTo.  r=mccr8

diff --git a/accessible/base/nsTextEquivUtils.cpp b/accessible/base/nsTextEquivUtils.cpp
--- a/accessible/base/nsTextEquivUtils.cpp
+++ b/accessible/base/nsTextEquivUtils.cpp
@@ -6,16 +6,17 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "nsTextEquivUtils.h"
 
 #include "Accessible-inl.h"
 #include "AccIterator.h"
 #include "nsCoreUtils.h"
 #include "nsIDOMXULLabeledControlEl.h"
+#include "mozilla/dom/Text.h"
 
 using namespace mozilla::a11y;
 
 /**
  * The accessible for which we are computing a text equivalent. It is useful
  * for bailing out during recursive text computation, or for special cases
  * like step f. of the ARIA implementation guide.
  */
@@ -95,17 +96,17 @@ nsresult nsTextEquivUtils::AppendTextEqu
   if (goThroughDOMSubtree) rv = AppendFromDOMNode(aContent, aString);
 
   sInitiatorAcc = nullptr;
   return rv;
 }
 
 nsresult nsTextEquivUtils::AppendTextEquivFromTextContent(nsIContent* aContent,
                                                           nsAString* aString) {
-  if (aContent->IsNodeOfType(nsINode::eTEXT)) {
+  if (aContent->IsText()) {
     bool isHTMLBlock = false;
 
     nsIContent* parentContent = aContent->GetFlattenedTreeParent();
     if (parentContent) {
       nsIFrame* frame = parentContent->GetPrimaryFrame();
       if (frame) {
         // If this text is inside a block level frame (as opposed to span
         // level), we need to add spaces around that block's text, so we don't
@@ -125,17 +126,17 @@ nsresult nsTextEquivUtils::AppendTextEqu
       nsIFrame* frame = aContent->GetPrimaryFrame();
       if (frame) {
         nsIFrame::RenderedText text = frame->GetRenderedText(
             0, UINT32_MAX, nsIFrame::TextOffsetType::OFFSETS_IN_CONTENT_TEXT,
             nsIFrame::TrailingWhitespace::DONT_TRIM_TRAILING_WHITESPACE);
         aString->Append(text.mString);
       } else {
         // If aContent is an object that is display: none, we have no a frame.
-        aContent->AppendTextTo(*aString);
+        aContent->GetAsText()->AppendTextTo(*aString);
       }
       if (isHTMLBlock && !aString->IsEmpty()) {
         aString->Append(char16_t(' '));
       }
     }
 
     return NS_OK;
   }
diff --git a/dom/base/CharacterData.h b/dom/base/CharacterData.h
--- a/dom/base/CharacterData.h
+++ b/dom/base/CharacterData.h
@@ -147,20 +147,27 @@ class CharacterData : public nsIContent 
    * the document is notified of the content change.
    */
   nsresult AppendText(const char16_t* aBuffer, uint32_t aLength,
                       bool aNotify);
 
   virtual bool TextIsOnlyWhitespace() override;
   bool ThreadSafeTextIsOnlyWhitespace() const final;
   virtual bool HasTextForTranslation() override;
-  virtual void AppendTextTo(nsAString& aResult) override;
+
+  /**
+   * Append the text content to aResult.
+   */
+  void AppendTextTo(nsAString& aResult);
+  /**
+   * Append the text content to aResult.
+   */
   MOZ_MUST_USE
-  virtual bool AppendTextTo(nsAString& aResult,
-                            const fallible_t&) override;
+  bool AppendTextTo(nsAString& aResult, const fallible_t&);
+
   virtual void SaveSubtreeState() override;
 
 #ifdef DEBUG
   virtual void List(FILE* out, int32_t aIndent) const override;
   virtual void DumpContent(FILE* out, int32_t aIndent,
                            bool aDumpAll) const override;
 #endif
 
diff --git a/dom/base/FragmentOrElement.cpp b/dom/base/FragmentOrElement.cpp
--- a/dom/base/FragmentOrElement.cpp
+++ b/dom/base/FragmentOrElement.cpp
@@ -1953,31 +1953,16 @@ uint32_t FragmentOrElement::TextLength()
 }
 
 bool FragmentOrElement::TextIsOnlyWhitespace() { return false; }
 
 bool FragmentOrElement::ThreadSafeTextIsOnlyWhitespace() const { return false; }
 
 bool FragmentOrElement::HasTextForTranslation() { return false; }
 
-void FragmentOrElement::AppendTextTo(nsAString& aResult) {
-  // We can remove this assertion if it turns out to be useful to be able
-  // to depend on this appending nothing.
-  NS_NOTREACHED("called FragmentOrElement::TextLength");
-}
-
-bool FragmentOrElement::AppendTextTo(nsAString& aResult,
-                                     const mozilla::fallible_t&) {
-  // We can remove this assertion if it turns out to be useful to be able
-  // to depend on this appending nothing.
-  NS_NOTREACHED("called FragmentOrElement::TextLength");
-
-  return false;
-}
-
 uint32_t FragmentOrElement::GetChildCount() const {
   return mAttrsAndChildren.ChildCount();
 }
 
 nsIContent* FragmentOrElement::GetChildAt_Deprecated(uint32_t aIndex) const {
   return mAttrsAndChildren.GetSafeChildAt(aIndex);
 }
 
diff --git a/dom/base/FragmentOrElement.h b/dom/base/FragmentOrElement.h
--- a/dom/base/FragmentOrElement.h
+++ b/dom/base/FragmentOrElement.h
@@ -126,20 +126,16 @@ class FragmentOrElement : public nsICont
 
   // nsIContent interface methods
   virtual already_AddRefed<nsINodeList> GetChildren(uint32_t aFilter) override;
   virtual const nsTextFragment* GetText() override;
   virtual uint32_t TextLength() const override;
   virtual bool TextIsOnlyWhitespace() override;
   virtual bool ThreadSafeTextIsOnlyWhitespace() const override;
   virtual bool HasTextForTranslation() override;
-  virtual void AppendTextTo(nsAString& aResult) override;
-  MOZ_MUST_USE
-  virtual bool AppendTextTo(nsAString& aResult,
-                            const mozilla::fallible_t&) override;
   virtual nsXBLBinding* DoGetXBLBinding() const override;
   virtual bool IsLink(nsIURI** aURI) const override;
 
   virtual void DestroyContent() override;
   virtual void SaveSubtreeState() override;
 
   nsIHTMLCollection* Children();
   uint32_t ChildElementCount() { return Children()->Length(); }
diff --git a/dom/base/nsContentUtils.cpp b/dom/base/nsContentUtils.cpp
--- a/dom/base/nsContentUtils.cpp
+++ b/dom/base/nsContentUtils.cpp
@@ -4991,42 +4991,42 @@ static bool AppendNodeTextContentsRecurs
                                           const fallible_t& aFallible) {
   for (nsIContent* child = aNode->GetFirstChild(); child;
        child = child->GetNextSibling()) {
     if (child->IsElement()) {
       bool ok = AppendNodeTextContentsRecurse(child, aResult, aFallible);
       if (!ok) {
         return false;
       }
-    } else if (child->IsNodeOfType(nsINode::eTEXT)) {
-      bool ok = child->AppendTextTo(aResult, aFallible);
+    } else if (Text* text = child->GetAsText()) {
+      bool ok = text->AppendTextTo(aResult, aFallible);
       if (!ok) {
         return false;
       }
     }
   }
 
   return true;
 }
 
 /* static */
 bool nsContentUtils::AppendNodeTextContent(nsINode* aNode, bool aDeep,
                                            nsAString& aResult,
                                            const fallible_t& aFallible) {
-  if (aNode->IsNodeOfType(nsINode::eTEXT)) {
-    return static_cast<nsIContent*>(aNode)->AppendTextTo(aResult, aFallible);
+  if (Text* text = aNode->GetAsText()) {
+    return text->AppendTextTo(aResult, aFallible);
   }
   if (aDeep) {
     return AppendNodeTextContentsRecurse(aNode, aResult, aFallible);
   }
 
   for (nsIContent* child = aNode->GetFirstChild(); child;
        child = child->GetNextSibling()) {
-    if (child->IsNodeOfType(nsINode::eTEXT)) {
-      bool ok = child->AppendTextTo(aResult, fallible);
+    if (Text* text = child->GetAsText()) {
+      bool ok = text->AppendTextTo(aResult, fallible);
       if (!ok) {
         return false;
       }
     }
   }
   return true;
 }
 
diff --git a/dom/base/nsFocusManager.cpp b/dom/base/nsFocusManager.cpp
--- a/dom/base/nsFocusManager.cpp
+++ b/dom/base/nsFocusManager.cpp
@@ -48,16 +48,17 @@
 #include "nsNumberControlFrame.h"
 #include "nsNetUtil.h"
 #include "nsRange.h"
 
 #include "mozilla/ContentEvents.h"
 #include "mozilla/dom/Element.h"
 #include "mozilla/dom/HTMLInputElement.h"
 #include "mozilla/dom/HTMLSlotElement.h"
+#include "mozilla/dom/Text.h"
 #include "mozilla/EventDispatcher.h"
 #include "mozilla/EventStateManager.h"
 #include "mozilla/EventStates.h"
 #include "mozilla/HTMLEditor.h"
 #include "mozilla/IMEStateManager.h"
 #include "mozilla/LookAndFeel.h"
 #include "mozilla/Preferences.h"
 #include "mozilla/Services.h"
@@ -2381,17 +2382,17 @@ nsresult nsFocusManager::GetSelectionLoc
     if (isCollapsed) {
       // Next check to see if our caret is at the very end of a node
       // If so, the caret is actually sitting in front of the next
       // logical frame's primary node - so for this case we need to
       // change caretContent to that node.
 
       if (startContent->NodeType() == nsINode::TEXT_NODE) {
         nsAutoString nodeValue;
-        startContent->AppendTextTo(nodeValue);
+        startContent->GetAsText()->AppendTextTo(nodeValue);
 
         bool isFormControl =
             startContent->IsNodeOfType(nsINode::eHTML_FORM_CONTROL);
 
         if (nodeValue.Length() == startOffset && !isFormControl &&
             startContent != aDocument->GetRootElement()) {
           // Yes, indeed we were at the end of the last node
           nsCOMPtr<nsIFrameEnumerator> frameTraversal;
diff --git a/dom/base/nsIContent.h b/dom/base/nsIContent.h
--- a/dom/base/nsIContent.h
+++ b/dom/base/nsIContent.h
@@ -373,29 +373,16 @@ class nsIContent : public nsINode {
    * Method to see if the text node contains data that is useful
    * for a translation: i.e., it consists of more than just whitespace,
    * digits and punctuation.
    * NOTE: Always returns false for elements.
    */
   virtual bool HasTextForTranslation() = 0;
 
   /**
-   * Append the text content to aResult.
-   * NOTE: This asserts and returns for elements
-   */
-  virtual void AppendTextTo(nsAString& aResult) = 0;
-
-  /**
-   * Append the text content to aResult.
-   * NOTE: This asserts and returns for elements
-   */
-  MOZ_MUST_USE
-  virtual bool AppendTextTo(nsAString& aResult, const mozilla::fallible_t&) = 0;
-
-  /**
    * Check if this content is focusable and in the current tab order.
    * Note: most callers should use nsIFrame::IsFocusable() instead as it
    *       checks visibility and other layout factors as well.
    * Tabbable is indicated by a nonnegative tabindex & is a subset of focusable.
    * For example, only the selected radio button in a group is in the
    * tab order, unless the radio group has no selection in which case
    * all of the visible, non-disabled radio buttons in the group are
    * in the tab order. On the other hand, all of the visible, non-disabled
diff --git a/dom/base/nsINode.cpp b/dom/base/nsINode.cpp
--- a/dom/base/nsINode.cpp
+++ b/dom/base/nsINode.cpp
@@ -23,16 +23,17 @@
 #include "mozilla/MemoryReporting.h"
 #include "mozilla/ServoBindings.h"
 #include "mozilla/Telemetry.h"
 #include "mozilla/TextEditor.h"
 #include "mozilla/TimeStamp.h"
 #ifdef MOZ_OLD_STYLE
 #include "mozilla/css/StyleRule.h"
 #endif
+#include "mozilla/dom/CharacterData.h"
 #include "mozilla/dom/DocumentType.h"
 #include "mozilla/dom/Element.h"
 #include "mozilla/dom/Event.h"
 #include "mozilla/dom/L10nUtilsBinding.h"
 #include "mozilla/dom/Promise.h"
 #include "mozilla/dom/PromiseNativeHandler.h"
 #include "mozilla/dom/ShadowRoot.h"
 #include "mozilla/dom/ScriptSettings.h"
@@ -917,20 +918,22 @@ bool nsINode::IsEqualNode(nsINode* aOthe
           }
         }
         break;
       }
       case TEXT_NODE:
       case COMMENT_NODE:
       case CDATA_SECTION_NODE:
       case PROCESSING_INSTRUCTION_NODE: {
+        MOZ_ASSERT(node1->IsCharacterData());
+        MOZ_ASSERT(node2->IsCharacterData());
         string1.Truncate();
-        static_cast<nsIContent*>(node1)->AppendTextTo(string1);
+        static_cast<CharacterData*>(node1)->AppendTextTo(string1);
         string2.Truncate();
-        static_cast<nsIContent*>(node2)->AppendTextTo(string2);
+        static_cast<CharacterData*>(node2)->AppendTextTo(string2);
 
         if (!string1.Equals(string2)) {
           return false;
         }
 
         break;
       }
       case DOCUMENT_NODE:
diff --git a/dom/html/HTMLOptionElement.cpp b/dom/html/HTMLOptionElement.cpp
--- a/dom/html/HTMLOptionElement.cpp
+++ b/dom/html/HTMLOptionElement.cpp
@@ -219,19 +219,18 @@ nsresult HTMLOptionElement::AfterSetAttr
       aNameSpaceID, aName, aValue, aOldValue, aSubjectPrincipal, aNotify);
 }
 
 void HTMLOptionElement::GetText(nsAString& aText) {
   nsAutoString text;
 
   nsIContent* child = nsINode::GetFirstChild();
   while (child) {
-    if (child->NodeType() == TEXT_NODE ||
-        child->NodeType() == CDATA_SECTION_NODE) {
-      child->AppendTextTo(text);
+    if (Text* textChild = child->GetAsText()) {
+      textChild->AppendTextTo(text);
     }
     if (child->IsHTMLElement(nsGkAtoms::script) ||
         child->IsSVGElement(nsGkAtoms::script)) {
       child = child->GetNextNonChildNode(this);
     } else {
       child = child->GetNextNode(this);
     }
   }
diff --git a/dom/xslt/xpath/txMozillaXPathTreeWalker.cpp b/dom/xslt/xpath/txMozillaXPathTreeWalker.cpp
--- a/dom/xslt/xpath/txMozillaXPathTreeWalker.cpp
+++ b/dom/xslt/xpath/txMozillaXPathTreeWalker.cpp
@@ -14,16 +14,17 @@
 #include "nsString.h"
 #include "nsTextFragment.h"
 #include "txXMLUtils.h"
 #include "txLog.h"
 #include "nsUnicharUtils.h"
 #include "nsAttrName.h"
 #include "nsTArray.h"
 #include "mozilla/dom/Attr.h"
+#include "mozilla/dom/CharacterData.h"
 #include "mozilla/dom/Element.h"
 #include <stdint.h>
 #include <algorithm>
 
 using namespace mozilla::dom;
 
 txXPathTreeWalker::txXPathTreeWalker(const txXPathTreeWalker& aOther)
     : mPosition(aOther.mPosition) {}
@@ -426,17 +427,18 @@ void txXPathNodeUtils::appendNodeValue(c
   if (aNode.isDocument() || aNode.mNode->IsElement() ||
       aNode.mNode->IsNodeOfType(nsINode::eDOCUMENT_FRAGMENT)) {
     nsContentUtils::AppendNodeTextContent(aNode.mNode, true, aResult,
                                           mozilla::fallible);
 
     return;
   }
 
-  aNode.Content()->AppendTextTo(aResult);
+  MOZ_ASSERT(aNode.mNode->IsCharacterData());
+  static_cast<CharacterData*>(aNode.Content())->AppendTextTo(aResult);
 }
 
 /* static */
 bool txXPathNodeUtils::isWhitespace(const txXPathNode& aNode) {
   NS_ASSERTION(aNode.isContent() && isText(aNode), "Wrong type!");
 
   return aNode.Content()->TextIsOnlyWhitespace();
 }
diff --git a/dom/xslt/xslt/txMozillaStylesheetCompiler.cpp b/dom/xslt/xslt/txMozillaStylesheetCompiler.cpp
--- a/dom/xslt/xslt/txMozillaStylesheetCompiler.cpp
+++ b/dom/xslt/xslt/txMozillaStylesheetCompiler.cpp
@@ -32,16 +32,17 @@
 #include "txStylesheetCompiler.h"
 #include "txXMLUtils.h"
 #include "nsAttrName.h"
 #include "nsIScriptError.h"
 #include "nsIURL.h"
 #include "nsError.h"
 #include "mozilla/Attributes.h"
 #include "mozilla/dom/Element.h"
+#include "mozilla/dom/Text.h"
 #include "mozilla/Encoding.h"
 #include "mozilla/UniquePtr.h"
 
 using namespace mozilla;
 using mozilla::net::ReferrerPolicy;
 
 static NS_DEFINE_CID(kCParserCID, NS_PARSER_CID);
 
@@ -499,19 +500,19 @@ static nsresult handleNode(nsINode* aNod
     for (nsIContent* child = element->GetFirstChild(); child;
          child = child->GetNextSibling()) {
       rv = handleNode(child, aCompiler);
       NS_ENSURE_SUCCESS(rv, rv);
     }
 
     rv = aCompiler->endElement();
     NS_ENSURE_SUCCESS(rv, rv);
-  } else if (aNode->IsNodeOfType(nsINode::eTEXT)) {
+  } else if (dom::Text* text = aNode->GetAsText()) {
     nsAutoString chars;
-    static_cast<nsIContent*>(aNode)->AppendTextTo(chars);
+    text->AppendTextTo(chars);
     rv = aCompiler->characters(chars);
     NS_ENSURE_SUCCESS(rv, rv);
   } else if (aNode->IsNodeOfType(nsINode::eDOCUMENT)) {
     for (nsIContent* child = aNode->GetFirstChild(); child;
          child = child->GetNextSibling()) {
       rv = handleNode(child, aCompiler);
       NS_ENSURE_SUCCESS(rv, rv);
     }
diff --git a/layout/base/nsBidiPresUtils.cpp b/layout/base/nsBidiPresUtils.cpp
--- a/layout/base/nsBidiPresUtils.cpp
+++ b/layout/base/nsBidiPresUtils.cpp
@@ -2,16 +2,17 @@
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "nsBidiPresUtils.h"
 
 #include "mozilla/IntegerRange.h"
+#include "mozilla/dom/Text.h"
 
 #include "gfxContext.h"
 #include "nsAutoPtr.h"
 #include "nsFontMetrics.h"
 #include "nsGkAtoms.h"
 #include "nsPresContext.h"
 #include "nsBidiUtils.h"
 #include "nsCSSFrameConstructor.h"
@@ -1073,24 +1074,24 @@ void nsBidiPresUtils::TraverseFrames(nsB
 
       // Append the content of the frame to the paragraph buffer
       LayoutFrameType frameType = frame->Type();
       if (LayoutFrameType::Text == frameType) {
         if (content != aBpd->mPrevContent) {
           aBpd->mPrevContent = content;
           if (!frame->StyleText()->NewlineIsSignificant(
                   static_cast<nsTextFrame*>(frame))) {
-            content->AppendTextTo(aBpd->mBuffer);
+            content->GetAsText()->AppendTextTo(aBpd->mBuffer);
           } else {
             /*
              * For preformatted text we have to do bidi resolution on each line
              * separately.
              */
             nsAutoString text;
-            content->AppendTextTo(text);
+            content->GetAsText()->AppendTextTo(text);
             nsIFrame* next;
             do {
               next = nullptr;
 
               int32_t start, end;
               frame->GetOffsets(start, end);
               int32_t endLine = text.FindChar('\n', start);
               if (endLine == -1) {
diff --git a/layout/xul/nsListBoxBodyFrame.cpp b/layout/xul/nsListBoxBodyFrame.cpp
--- a/layout/xul/nsListBoxBodyFrame.cpp
+++ b/layout/xul/nsListBoxBodyFrame.cpp
@@ -23,16 +23,17 @@
 #include "nsScrollbarFrame.h"
 #include "nsView.h"
 #include "nsViewManager.h"
 #include "nsStyleContext.h"
 #include "nsFontMetrics.h"
 #include "nsITimer.h"
 #include "mozilla/StyleSetHandle.h"
 #include "mozilla/StyleSetHandleInlines.h"
+#include "mozilla/dom/Text.h"
 #include "nsPIBoxObject.h"
 #include "nsLayoutUtils.h"
 #include "nsPIListBoxObject.h"
 #include "nsContentUtils.h"
 #include "ChildIterator.h"
 #include "gfxContext.h"
 #include "prtime.h"
 #include <algorithm>
@@ -630,19 +631,19 @@ nscoord nsListBoxBodyFrame::ComputeIntri
 
     FlattenedChildIterator iter(mContent);
     for (nsIContent* child = iter.GetNextChild(); child;
          child = iter.GetNextChild()) {
       if (child->IsXULElement(nsGkAtoms::listitem)) {
         gfxContext* rendContext = aBoxLayoutState.GetRenderingContext();
         if (rendContext) {
           nsAutoString value;
-          for (nsIContent* text = child->GetFirstChild(); text;
-               text = text->GetNextSibling()) {
-            if (text->IsNodeOfType(nsINode::eTEXT)) {
+          for (nsIContent* content = child->GetFirstChild();
+               content; content = content->GetNextSibling()) {
+            if (Text* text = content->GetAsText()) {
               text->AppendTextTo(value);
             }
           }
 
           RefPtr<nsFontMetrics> fm =
               nsLayoutUtils::GetFontMetricsForStyleContext(styleContext);
 
           nscoord textWidth = nsLayoutUtils::AppUnitWidthOfStringBidi(
