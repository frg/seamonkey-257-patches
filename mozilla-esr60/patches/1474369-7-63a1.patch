# HG changeset patch
# User Nika Layzell <nika@thelayzells.com>
# Date 1532459773 14400
# Node ID 3472197282cee26d8c34d3e38945176470bbc256
# Parent  760c3bfdc9f180e58c1c0d81e383f84b62cf3531
Bug 1474369 - Part 7: Rename [array] to LegacyArray within xpt and xpidl, r=mccr8

Summary:
This is done so we can use Array as the name for the new nsTArray-based
type, rather than having to come up with a new name.

LegacyArray was chosen as the [array] attribute is now effectively deprecated,
and we'd like to remove it ASAP.

Depends On D2334

Reviewers: mccr8!

Tags: #secure-revision

Bug #: 1474369

Differential Revision: https://phabricator.services.mozilla.com/D2335

diff --git a/js/xpconnect/src/XPCConvert.cpp b/js/xpconnect/src/XPCConvert.cpp
--- a/js/xpconnect/src/XPCConvert.cpp
+++ b/js/xpconnect/src/XPCConvert.cpp
@@ -338,17 +338,17 @@ bool XPCConvert::NativeData2JS(MutableHa
       RootedObject jsobj(cx, promise->PromiseObj());
       if (!JS_WrapObject(cx, &jsobj)) {
         return false;
       }
       d.setObject(*jsobj);
       return true;
     }
 
-    case nsXPTType::T_ARRAY:
+    case nsXPTType::T_LEGACY_ARRAY:
       return NativeArray2JS(d, *static_cast<const void* const*>(s),
                             type.ArrayElementType(), iid, arrlen, pErr);
 
     case nsXPTType::T_SEQUENCE: {
       auto* sequence = static_cast<const xpt::detail::UntypedSequence*>(s);
       return NativeArray2JS(d, sequence->Elements(), type.ArrayElementType(),
                             iid, sequence->Length(), pErr);
     }
@@ -752,17 +752,17 @@ bool XPCConvert::JSData2Native(void* d, 
       bool ok = !err.Failed();
       if (pErr) {
         *pErr = err.StealNSResult();
       }
 
       return ok;
     }
 
-    case nsXPTType::T_ARRAY: {
+    case nsXPTType::T_LEGACY_ARRAY: {
       void** dest = (void**)d;
       const nsXPTType& elty = type.ArrayElementType();
 
       *dest = nullptr;
 
       // FIXME: XPConnect historically has shortcut the JSArray2Native codepath
       // in its caller if arrlen is 0, allowing arbitrary values to be passed as
       // arrays and interpreted as the empty array (bug 1458987).
@@ -1417,17 +1417,17 @@ bool XPCConvert::JSArray2Native(JS::Hand
 // Internal implementation details for xpc::CleanupValue.
 
 void xpc::InnerCleanupValue(const nsXPTType& aType, void* aValue,
                             uint32_t aArrayLen) {
   MOZ_ASSERT(!aType.IsArithmetic(),
              "Arithmetic types should not get to InnerCleanupValue!");
   MOZ_ASSERT(aArrayLen == 0 || aType.Tag() == nsXPTType::T_PSTRING_SIZE_IS ||
              aType.Tag() == nsXPTType::T_PWSTRING_SIZE_IS ||
-             aType.Tag() == nsXPTType::T_ARRAY,
+             aType.Tag() == nsXPTType::T_LEGACY_ARRAY,
              "Array lengths may only appear for certain types!");
 
   switch (aType.Tag()) {
     // Pointer types
     case nsXPTType::T_DOMOBJECT:
       aType.GetDOMObjectInfo().Cleanup(*(void**)aValue);
       break;
 
@@ -1455,17 +1455,17 @@ void xpc::InnerCleanupValue(const nsXPTT
     case nsXPTType::T_CHAR_STR:
     case nsXPTType::T_WCHAR_STR:
     case nsXPTType::T_PSTRING_SIZE_IS:
     case nsXPTType::T_PWSTRING_SIZE_IS:
       free(*(void**)aValue);
       break;
 
     // Array Types
-    case nsXPTType::T_ARRAY: {
+    case nsXPTType::T_LEGACY_ARRAY: {
       const nsXPTType& elty = aType.ArrayElementType();
       void* elements = *(void**)aValue;
 
       for (uint32_t i = 0; i < aArrayLen; ++i) {
         CleanupValue(elty, elty.ElementPtr(elements, i));
       }
       free(elements);
       break;
diff --git a/js/xpconnect/src/XPCWrappedJSClass.cpp b/js/xpconnect/src/XPCWrappedJSClass.cpp
--- a/js/xpconnect/src/XPCWrappedJSClass.cpp
+++ b/js/xpconnect/src/XPCWrappedJSClass.cpp
@@ -603,17 +603,17 @@ JSObject* nsXPCWrappedJSClass::GetRootJS
   JSObject* inner = js::UncheckedUnwrap(result);
   if (inner) return inner;
   return result;
 }
 
 bool nsXPCWrappedJSClass::GetArraySizeFromParam(
     const nsXPTMethodInfo* method, const nsXPTType& type,
     nsXPTCMiniVariant* nativeParams, uint32_t* result) const {
-  if (type.Tag() != nsXPTType::T_ARRAY &&
+  if (type.Tag() != nsXPTType::T_LEGACY_ARRAY &&
       type.Tag() != nsXPTType::T_PSTRING_SIZE_IS &&
       type.Tag() != nsXPTType::T_PWSTRING_SIZE_IS) {
     *result = 0;
     return true;
   }
 
   uint8_t argnum = type.ArgNum();
   const nsXPTParamInfo& param = method->Param(argnum);
diff --git a/js/xpconnect/src/XPCWrappedNative.cpp b/js/xpconnect/src/XPCWrappedNative.cpp
--- a/js/xpconnect/src/XPCWrappedNative.cpp
+++ b/js/xpconnect/src/XPCWrappedNative.cpp
@@ -1190,17 +1190,17 @@ CallMethodHelper::~CallMethodHelper() {
 
     xpc::CleanupValue(param.type, &param.val, arraylen);
   }
 }
 
 bool CallMethodHelper::GetArraySizeFromParam(const nsXPTType& type,
                                              HandleValue maybeArray,
                                              uint32_t* result) {
-  if (type.Tag() != nsXPTType::T_ARRAY &&
+  if (type.Tag() != nsXPTType::T_LEGACY_ARRAY &&
       type.Tag() != nsXPTType::T_PSTRING_SIZE_IS &&
       type.Tag() != nsXPTType::T_PWSTRING_SIZE_IS) {
     *result = 0;
     return true;
   }
 
   uint8_t argnum = type.ArgNum();
   uint32_t* lengthp = &GetDispatchParam(argnum)->val.u32;
@@ -1585,17 +1585,17 @@ static void TraceParam(JSTracer* aTrc, v
                         "XPCWrappedNative::CallMethod param");
   } else if (aType.Tag() == nsXPTType::T_SEQUENCE) {
     auto* sequence = (xpt::detail::UntypedSequence*)aVal;
     const nsXPTType& elty = aType.ArrayElementType();
 
     for (uint32_t i = 0; i < sequence->Length(); ++i) {
       TraceParam(aTrc, elty.ElementPtr(sequence->Elements(), i), elty);
     }
-  } else if (aType.Tag() == nsXPTType::T_ARRAY && *(void**)aVal) {
+  } else if (aType.Tag() == nsXPTType::T_LEGACY_ARRAY && *(void**)aVal) {
     const nsXPTType& elty = aType.ArrayElementType();
 
     for (uint32_t i = 0; i < aArrayLen; ++i) {
       TraceParam(aTrc, elty.ElementPtr(*(void**)aVal, i), elty);
     }
   }
 }
 
diff --git a/js/xpconnect/src/xpcprivate.h b/js/xpconnect/src/xpcprivate.h
--- a/js/xpconnect/src/xpcprivate.h
+++ b/js/xpconnect/src/xpcprivate.h
@@ -2939,17 +2939,17 @@ nsIPrincipal* GetObjectPrincipal(JSObjec
 //   TD_PNSIID
 //     value : nsID* (free)
 //   TD_DOMSTRING, TD_ASTRING, TD_CSTRING, TD_UTF8STRING
 //     value : ns[C]String* (truncate)
 //   TD_PSTRING, TD_PWSTRING, TD_PSTRING_SIZE_IS, TD_PWSTRING_SIZE_IS
 //     value : char[16_t]** (free)
 //   TD_INTERFACE_TYPE, TD_INTERFACE_IS_TYPE
 //     value : nsISupports** (release)
-//   TD_ARRAY (NOTE: aArrayLen should be passed)
+//   TD_LEGACY_ARRAY (NOTE: aArrayLen should be passed)
 //     value : void** (cleanup elements & free)
 //   TD_DOMOBJECT
 //     value : T** (cleanup)
 //   TD_PROMISE
 //     value : dom::Promise** (release)
 //
 // Other types are ignored.
 //
diff --git a/xpcom/ds/nsIVariant.idl b/xpcom/ds/nsIVariant.idl
--- a/xpcom/ds/nsIVariant.idl
+++ b/xpcom/ds/nsIVariant.idl
@@ -34,17 +34,17 @@ struct nsIDataType
         VTYPE_WCHAR             = TD_WCHAR            ,
         VTYPE_VOID              = TD_VOID             ,
         VTYPE_ID                = TD_PNSIID           ,
         VTYPE_DOMSTRING         = TD_DOMSTRING        ,
         VTYPE_CHAR_STR          = TD_PSTRING          ,
         VTYPE_WCHAR_STR         = TD_PWSTRING         ,
         VTYPE_INTERFACE         = TD_INTERFACE_TYPE   ,
         VTYPE_INTERFACE_IS      = TD_INTERFACE_IS_TYPE,
-        VTYPE_ARRAY             = TD_ARRAY            ,
+        VTYPE_ARRAY             = TD_LEGACY_ARRAY     ,
         VTYPE_STRING_SIZE_IS    = TD_PSTRING_SIZE_IS  ,
         VTYPE_WSTRING_SIZE_IS   = TD_PWSTRING_SIZE_IS ,
         VTYPE_UTF8STRING        = TD_UTF8STRING       ,
         VTYPE_CSTRING           = TD_CSTRING          ,
         VTYPE_ASTRING           = TD_ASTRING          ,
 
         // Non-xpt variant types
         VTYPE_EMPTY_ARRAY       = 254                 ,
diff --git a/xpcom/idl-parser/xpidl/jsonxpt.py b/xpcom/idl-parser/xpidl/jsonxpt.py
--- a/xpcom/idl-parser/xpidl/jsonxpt.py
+++ b/xpcom/idl-parser/xpidl/jsonxpt.py
@@ -65,21 +65,21 @@ def get_type(type, calltype, iid_is=None
     if isinstance(type, xpidl.Sequence):
         # NB: For a Sequence<T> we pass down the iid_is to get the type of T.
         #     This allows Arrays of InterfaceIs types to work.
         return {
             'tag': 'TD_SEQUENCE',
             'element': get_type(type.type, calltype, iid_is),
         }
 
-    if isinstance(type, xpidl.Array):
-        # NB: For an Array<T> we pass down the iid_is to get the type of T.
-        #     This allows Arrays of InterfaceIs types to work.
+    if isinstance(type, xpidl.LegacyArray):
+        # NB: For a Legacy [array] T we pass down iid_is to get the type of T.
+        #     This allows [array] of InterfaceIs types to work.
         return {
-            'tag': 'TD_ARRAY',
+            'tag': 'TD_LEGACY_ARRAY',
             'size_is': size_is,
             'element': get_type(type.type, calltype, iid_is),
         }
 
     if isinstance(type, xpidl.Interface) or isinstance(type, xpidl.Forward):
         return {
             'tag': 'TD_INTERFACE_TYPE',
             'name': type.name,
diff --git a/xpcom/idl-parser/xpidl/runtests.py b/xpcom/idl-parser/xpidl/runtests.py
--- a/xpcom/idl-parser/xpidl/runtests.py
+++ b/xpcom/idl-parser/xpidl/runtests.py
@@ -76,17 +76,17 @@ long bar(in long a, in float b, [array] 
         self.assertEqual(xpidl.TypeId("long"), m.type)
         self.assertEqual(3, len(m.params))
         self.assertEqual(xpidl.TypeId("long"), m.params[0].type)
         self.assertEqual("in", m.params[0].paramtype)
         self.assertEqual(xpidl.TypeId("float"), m.params[1].type)
         self.assertEqual("in", m.params[1].paramtype)
         self.assertEqual(xpidl.TypeId("long"), m.params[2].type)
         self.assertEqual("in", m.params[2].paramtype)
-        self.assertTrue(isinstance(m.params[2].realtype, xpidl.Array))
+        self.assertTrue(isinstance(m.params[2].realtype, xpidl.LegacyArray))
         self.assertEqual("long", m.params[2].realtype.type.name)
 
     def testAttribute(self):
         i = self.p.parse("""[uuid(abc)] interface foo {
 attribute long bar;
 };""", filename='f')
         self.assertTrue(isinstance(i, xpidl.IDL))
         self.assertTrue(isinstance(i.productions[0], xpidl.Interface))
diff --git a/xpcom/idl-parser/xpidl/xpidl.py b/xpcom/idl-parser/xpidl/xpidl.py
--- a/xpcom/idl-parser/xpidl/xpidl.py
+++ b/xpcom/idl-parser/xpidl/xpidl.py
@@ -1201,17 +1201,17 @@ class Param(object):
                 elif name == 'optional':
                     self.optional = True
                 else:
                     raise IDLError("Unexpected attribute '%s'" % name, aloc)
 
     def resolve(self, method):
         self.realtype = method.iface.idl.getName(self.type, self.location)
         if self.array:
-            self.realtype = Array(self.realtype)
+            self.realtype = LegacyArray(self.realtype)
         if (self.null is not None and
                 getBuiltinOrNativeTypeName(self.realtype) != '[domstring]'):
             raise IDLError("'Null' attribute can only be used on DOMString",
                            self.location)
         if (self.undefined is not None and
                 getBuiltinOrNativeTypeName(self.realtype) != '[domstring]'):
             raise IDLError("'Undefined' attribute can only be used on DOMString",
                            self.location)
@@ -1246,17 +1246,17 @@ class Param(object):
 
     def toIDL(self):
         return "%s%s %s %s" % (paramAttlistToIDL(self.attlist),
                                self.paramtype,
                                self.type,
                                self.name)
 
 
-class Array(object):
+class LegacyArray(object):
     def __init__(self, basetype):
         self.type = basetype
         self.location = self.type.location
 
     def isScriptable(self):
         return self.type.isScriptable()
 
     def nativeType(self, calltype, const=False):
diff --git a/xpcom/reflect/xptinfo/xptcodegen.py b/xpcom/reflect/xptinfo/xptcodegen.py
--- a/xpcom/reflect/xptinfo/xptcodegen.py
+++ b/xpcom/reflect/xptinfo/xptcodegen.py
@@ -275,23 +275,23 @@ def link_to_cpp(interfaces, fd):
         elif tag.endswith('_size_is'):
             return '%s(size_is=%d)' % (tag, type['size_is'])
         return tag
 
     def lower_type(type, in_=False, out=False, optional=False):
         tag = type['tag']
         d1 = d2 = 0
 
-        if tag == 'TD_ARRAY':
+        if tag == 'TD_LEGACY_ARRAY':
             d1 = type['size_is']
             d2 = lower_extra_type(type['element'])
 
         elif tag == 'TD_SEQUENCE':
-            # NOTE: TD_SEQUENCE can hold 16 bits of type index, while TD_ARRAY
-            # can only hold 8.
+            # NOTE: TD_SEQUENCE can hold 16 bits of type index, while
+            # TD_LEGACY_ARRAY can only hold 8.
             d1, d2 = splitint(lower_extra_type(type['element']))
 
         elif tag == 'TD_INTERFACE_TYPE':
             d1, d2 = splitint(interface_idx(type['name']))
 
         elif tag == 'TD_INTERFACE_IS_TYPE':
             d1 = type['iid_is']
 
diff --git a/xpcom/reflect/xptinfo/xptinfo.h b/xpcom/reflect/xptinfo/xptinfo.h
--- a/xpcom/reflect/xptinfo/xptinfo.h
+++ b/xpcom/reflect/xptinfo/xptinfo.h
@@ -182,17 +182,17 @@ enum nsXPTTypeTag : uint8_t {
   //  - Outparams may be uninitialized by caller,
   //  - Supported in xptcall as raw pointer.
   TD_VOID              = 13,
   TD_PNSIID            = 14,
   TD_PSTRING           = 15,
   TD_PWSTRING          = 16,
   TD_INTERFACE_TYPE    = 17,
   TD_INTERFACE_IS_TYPE = 18,
-  TD_ARRAY             = 19,
+  TD_LEGACY_ARRAY      = 19,
   TD_PSTRING_SIZE_IS   = 20,
   TD_PWSTRING_SIZE_IS  = 21,
   TD_DOMOBJECT         = 22,
   TD_PROMISE           = 23,
   _TD_LAST_POINTER     = TD_PROMISE,
 
   // Complex Types
   //  - Require cleanup,
@@ -222,30 +222,30 @@ struct nsXPTType {
   nsXPTTypeTag Tag() const { return static_cast<nsXPTTypeTag>(mTag); }
 
   // The index in the function argument list which should be used when
   // determining the iid_is or size_is properties of this dependent type.
   uint8_t ArgNum() const {
     MOZ_ASSERT(Tag() == TD_INTERFACE_IS_TYPE ||
                Tag() == TD_PSTRING_SIZE_IS ||
                Tag() == TD_PWSTRING_SIZE_IS ||
-               Tag() == TD_ARRAY);
+               Tag() == TD_LEGACY_ARRAY);
     return mData1;
   }
 
 private:
   // Helper for reading 16-bit data values split between mData1 and mData2.
   uint16_t Data16() const { return ((uint16_t)mData1 << 8) | mData2; }
 
 public:
   // Get the type of the element in the current array or sequence. Arrays only
   // fit 8 bits of type data, while sequences support up to 16 bits of type data
   // due to not needing to store an ArgNum.
   const nsXPTType& ArrayElementType() const {
-    if (Tag() == TD_ARRAY) {
+    if (Tag() == TD_LEGACY_ARRAY) {
       return xpt::detail::GetType(mData2);
     }
     MOZ_ASSERT(Tag() == TD_SEQUENCE);
     return xpt::detail::GetType(Data16());
   }
 
   // We store the 16-bit iface value as two 8-bit values in order to
   // avoid 16-bit alignment requirements for XPTTypeDescriptor, which
@@ -267,23 +267,23 @@ public:
   bool IsComplex() const { return Tag() > _TD_LAST_POINTER; }
 
   bool IsInterfacePointer() const {
     return Tag() == TD_INTERFACE_TYPE || Tag() == TD_INTERFACE_IS_TYPE;
   }
 
   bool IsDependent() const {
     return (Tag() == TD_SEQUENCE && InnermostType().IsDependent()) ||
-           Tag() == TD_INTERFACE_IS_TYPE || Tag() == TD_ARRAY ||
+           Tag() == TD_INTERFACE_IS_TYPE || Tag() == TD_LEGACY_ARRAY ||
            Tag() == TD_PSTRING_SIZE_IS || Tag() == TD_PWSTRING_SIZE_IS;
   }
 
   // Unwrap a nested type to its innermost value (e.g. through arrays).
   const nsXPTType& InnermostType() const {
-    if (Tag() == TD_ARRAY || Tag() == TD_SEQUENCE) {
+    if (Tag() == TD_LEGACY_ARRAY || Tag() == TD_SEQUENCE) {
       return ArrayElementType().InnermostType();
     }
     return *this;
   }
 
   // In-memory size of native type in bytes.
   inline size_t Stride() const;
 
@@ -318,17 +318,17 @@ public:
     PSTRING,
     PWSTRING,
     INTERFACE_IS_TYPE
   };
 
   // Helper methods for fabricating nsXPTType values used by xpconnect.
   static nsXPTType MkArrayType(Idx aInner) {
     MOZ_ASSERT(aInner <= Idx::INTERFACE_IS_TYPE);
-    return { TD_ARRAY, false, false, false, 0, (uint8_t)aInner };
+    return { TD_LEGACY_ARRAY, false, false, false, 0, (uint8_t)aInner };
   }
   static const nsXPTType& Get(Idx aInner) {
     MOZ_ASSERT(aInner <= Idx::INTERFACE_IS_TYPE);
     return xpt::detail::GetType((uint8_t)aInner);
   }
 
   ///////////////////////////////////////
   // nsXPTType backwards compatibility //
@@ -353,17 +353,17 @@ public:
   TD_ALIAS_(T_WCHAR             , TD_WCHAR            );
   TD_ALIAS_(T_VOID              , TD_VOID             );
   TD_ALIAS_(T_IID               , TD_PNSIID           );
   TD_ALIAS_(T_DOMSTRING         , TD_DOMSTRING        );
   TD_ALIAS_(T_CHAR_STR          , TD_PSTRING          );
   TD_ALIAS_(T_WCHAR_STR         , TD_PWSTRING         );
   TD_ALIAS_(T_INTERFACE         , TD_INTERFACE_TYPE   );
   TD_ALIAS_(T_INTERFACE_IS      , TD_INTERFACE_IS_TYPE);
-  TD_ALIAS_(T_ARRAY             , TD_ARRAY            );
+  TD_ALIAS_(T_LEGACY_ARRAY      , TD_LEGACY_ARRAY     );
   TD_ALIAS_(T_PSTRING_SIZE_IS   , TD_PSTRING_SIZE_IS  );
   TD_ALIAS_(T_PWSTRING_SIZE_IS  , TD_PWSTRING_SIZE_IS );
   TD_ALIAS_(T_UTF8STRING        , TD_UTF8STRING       );
   TD_ALIAS_(T_CSTRING           , TD_CSTRING          );
   TD_ALIAS_(T_ASTRING           , TD_ASTRING          );
   TD_ALIAS_(T_JSVAL             , TD_JSVAL            );
   TD_ALIAS_(T_DOMOBJECT         , TD_DOMOBJECT        );
   TD_ALIAS_(T_PROMISE           , TD_PROMISE          );
@@ -652,17 +652,17 @@ inline size_t nsXPTType::Stride() const 
 
     case TD_VOID:              return sizeof(void*);
     case TD_PNSIID:            return sizeof(nsIID*);
     case TD_DOMSTRING:         return sizeof(nsString);
     case TD_PSTRING:           return sizeof(char*);
     case TD_PWSTRING:          return sizeof(char16_t*);
     case TD_INTERFACE_TYPE:    return sizeof(nsISupports*);
     case TD_INTERFACE_IS_TYPE: return sizeof(nsISupports*);
-    case TD_ARRAY:             return sizeof(void*);
+    case TD_LEGACY_ARRAY:      return sizeof(void*);
     case TD_PSTRING_SIZE_IS:   return sizeof(char*);
     case TD_PWSTRING_SIZE_IS:  return sizeof(char16_t*);
     case TD_DOMOBJECT:         return sizeof(void*);
     case TD_PROMISE:           return sizeof(void*);
 
     case TD_UTF8STRING:        return sizeof(nsCString);
     case TD_CSTRING:           return sizeof(nsCString);
     case TD_ASTRING:           return sizeof(nsString);
