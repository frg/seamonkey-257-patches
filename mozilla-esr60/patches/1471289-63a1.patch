# HG changeset patch
# User Lars T Hansen <lhansen@mozilla.com>
# Date 1530173163 -7200
# Node ID 7c3c2d6c17d7563fa2ea242ee2ae08be4685370e
# Parent  6ae6d234f63254df79771d28c6ac6b6d95876e48
Bug 1471289 - Handle 'new WebAssembly.Global' on i64 with default value. r=jseward

As a tweak the wasm CG has decided to allow the special case where an i64 global is
created from JS with the default zero value, while we're waiting for BigInt and
more general i64 support.  This amounts to removing the error we would otherwise
signal in this case.

diff --git a/js/src/jit-test/tests/wasm/globals.js b/js/src/jit-test/tests/wasm/globals.js
--- a/js/src/jit-test/tests/wasm/globals.js
+++ b/js/src/jit-test/tests/wasm/globals.js
@@ -310,35 +310,42 @@ wasmAssert(`(module
 // WebAssembly.Global
 {
     const Global = WebAssembly.Global;
 
     // These types should work:
     assertEq(new Global({value: "i32"}) instanceof Global, true);
     assertEq(new Global({value: "f32"}) instanceof Global, true);
     assertEq(new Global({value: "f64"}) instanceof Global, true);
+    assertEq(new Global({value: "i64"}) instanceof Global, true); // No initial value works
 
     // These types should not work:
-    assertErrorMessage(() => new Global({value: "i64"}),   TypeError, /bad type for a WebAssembly.Global/);
-    assertErrorMessage(() => new Global({}),               TypeError, /bad type for a WebAssembly.Global/);
-    assertErrorMessage(() => new Global({value: "fnord"}), TypeError, /bad type for a WebAssembly.Global/);
-    assertErrorMessage(() => new Global(),                 TypeError, /Global requires more than 0 arguments/);
+    assertErrorMessage(() => new Global({}),                TypeError, /bad type for a WebAssembly.Global/);
+    assertErrorMessage(() => new Global({value: "fnord"}),  TypeError, /bad type for a WebAssembly.Global/);
+    assertErrorMessage(() => new Global(),                  TypeError, /Global requires more than 0 arguments/);
+    assertErrorMessage(() => new Global({value: "i64"}, 0), TypeError, /bad type for a WebAssembly.Global/); // Initial value does not work
 
     // Coercion of init value; ".value" accessor
     assertEq((new Global({value: "i32"}, 3.14)).value, 3);
     assertEq((new Global({value: "f32"}, { valueOf: () => 33.5 })).value, 33.5);
     assertEq((new Global({value: "f64"}, "3.25")).value, 3.25);
 
     // Nothing special about NaN, it coerces just fine
     assertEq((new Global({value: "i32"}, NaN)).value, 0);
 
     // The default init value is zero.
     assertEq((new Global({value: "i32"})).value, 0);
     assertEq((new Global({value: "f32"})).value, 0);
     assertEq((new Global({value: "f64"})).value, 0);
+    let mod = wasmEvalText(`(module
+                             (import "" "g" (global i64))
+                             (func (export "f") (result i32)
+                              (i64.eqz (get_global 0))))`,
+                           {"":{g: new Global({value: "i64"})}});
+    assertEq(mod.exports.f(), 1);
 
     {
         // "value" is enumerable
         let x = new Global({value: "i32"});
         let s = "";
         for ( let i in x )
             s = s + i + ",";
         assertEq(s, "value,");
diff --git a/js/src/jit-test/tests/wasm/spec/jsapi.js b/js/src/jit-test/tests/wasm/spec/jsapi.js
--- a/js/src/jit-test/tests/wasm/spec/jsapi.js
+++ b/js/src/jit-test/tests/wasm/spec/jsapi.js
@@ -687,24 +687,27 @@ test(() => {
     const globalDesc = Object.getOwnPropertyDescriptor(WebAssembly, 'Global');
     assert_equals(Global, globalDesc.value);
     assert_equals(Global.length, 1);
     assert_equals(Global.name, "Global");
     assertThrows(() => Global(), TypeError);
     assertThrows(() => new Global(1), TypeError);
     assertThrows(() => new Global({}), TypeError);
     assertThrows(() => new Global({value: 'foo'}), TypeError);
-    assertThrows(() => new Global({value: 'i64'}), TypeError);
+    assertThrows(() => new Global({value: 'i64'}, 0), TypeError);
     assert_equals(new Global({value:'i32'}) instanceof Global, true);
+    assert_equals(new Global({value:'i64'}) instanceof Global, true);
     assert_equals(new Global({value:'f32'}) instanceof Global, true);
     assert_equals(new Global({value:'f64'}) instanceof Global, true);
     assert_equals(new Global({value:'i32', mutable: false}) instanceof Global, true);
+    assert_equals(new Global({value:'i64', mutable: false}) instanceof Global, true);
     assert_equals(new Global({value:'f64', mutable: false}) instanceof Global, true);
     assert_equals(new Global({value:'f64', mutable: false}) instanceof Global, true);
     assert_equals(new Global({value:'i32', mutable: true}) instanceof Global, true);
+    assert_equals(new Global({value:'i64', mutable: true}) instanceof Global, true);
     assert_equals(new Global({value:'f64', mutable: true}) instanceof Global, true);
     assert_equals(new Global({value:'f64', mutable: true}) instanceof Global, true);
     assert_equals(new Global({value:'i32'}, 0x132) instanceof Global, true);
     assert_equals(new Global({value:'f32'}, 0xf32) instanceof Global, true);
     assert_equals(new Global({value:'f64'}, 0xf64) instanceof Global, true);
     assert_equals(new Global({value:'i32', mutable: false}, 0x132) instanceof Global, true);
     assert_equals(new Global({value:'f32', mutable: false}, 0xf32) instanceof Global, true);
     assert_equals(new Global({value:'f64', mutable: false}, 0xf64) instanceof Global, true);
diff --git a/js/src/wasm/WasmJS.cpp b/js/src/wasm/WasmJS.cpp
--- a/js/src/wasm/WasmJS.cpp
+++ b/js/src/wasm/WasmJS.cpp
@@ -1948,16 +1948,20 @@ const Class WasmGlobalObject::class_ = {
   if (!typeStr) return false;
 
   RootedLinearString typeLinearStr(cx, typeStr->ensureLinear(cx));
   if (!typeLinearStr) return false;
 
   ValType globalType;
   if (StringEqualsAscii(typeLinearStr, "i32")) {
     globalType = ValType::I32;
+  } else if (args.length() == 1 && StringEqualsAscii(typeLinearStr, "i64")) {
+    // For the time being, i64 is allowed only if there is not an
+    // initializing value.
+    globalType = ValType::I64;
   } else if (StringEqualsAscii(typeLinearStr, "f32")) {
     globalType = ValType::F32;
   } else if (StringEqualsAscii(typeLinearStr, "f64")) {
     globalType = ValType::F64;
   } else {
     JS_ReportErrorNumberUTF8(cx, GetErrorMessage, nullptr,
                              JSMSG_WASM_BAD_GLOBAL_TYPE);
     return false;
@@ -1974,16 +1978,17 @@ const Class WasmGlobalObject::class_ = {
   if (args.length() >= 2) {
     RootedValue valueVal(cx, args.get(1));
 
     if (!ToWebAssemblyValue(cx, globalType, valueVal, &globalVal))
       return false;
   } else {
     switch (globalType.code()) {
       case ValType::I32: /* set above */ break;
+      case ValType::I64: globalVal = Val(uint64_t(0)); break;
       case ValType::F32: globalVal = Val(float(0.0));  break;
       case ValType::F64: globalVal = Val(double(0.0)); break;
       default: MOZ_CRASH();
     }
   }
 
   WasmGlobalObject* global = WasmGlobalObject::create(cx, globalVal, isMutable);
   if (!global) return false;
