# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1536080396 0
# Node ID 04eb549952d0f5eae672a84407e111cdf2a8423e
# Parent  54d25b75f4ab61a96441d36c17b4dffa077cc16a
Bug 1267297 - Use AutoEntryScript for script activity bookkeeping instead of the request machinery. r=bholley

Differential Revision: https://phabricator.services.mozilla.com/D4085

diff --git a/dom/script/ScriptSettings.cpp b/dom/script/ScriptSettings.cpp
--- a/dom/script/ScriptSettings.cpp
+++ b/dom/script/ScriptSettings.cpp
@@ -568,18 +568,21 @@ AutoEntryScript::AutoEntryScript(nsIGlob
     : AutoJSAPI(aGlobalObject, aIsMainThread, eEntryScript),
       mWebIDLCallerPrincipal(nullptr)
       // This relies on us having a cx() because the AutoJSAPI constructor
       // already ran.
       ,
       mCallerOverride(cx()) {
   MOZ_ASSERT(aGlobalObject);
 
-  if (aIsMainThread && gRunToCompletionListeners > 0) {
-    mDocShellEntryMonitor.emplace(cx(), aReason);
+  if (aIsMainThread) {
+    if (gRunToCompletionListeners > 0) {
+      mDocShellEntryMonitor.emplace(cx(), aReason);
+    }
+    mScriptActivity.emplace(true);
   }
 }
 
 AutoEntryScript::AutoEntryScript(JSObject* aObject, const char* aReason,
                                  bool aIsMainThread)
     : AutoEntryScript(xpc::NativeGlobal(aObject), aReason, aIsMainThread) {}
 
 AutoEntryScript::~AutoEntryScript() {}
@@ -696,24 +699,28 @@ AutoSafeJSContext::AutoSafeJSContext(
   MOZ_ASSERT(ok,
              "This is quite odd.  We should have crashed in the "
              "xpc::NativeGlobal() call if xpc::UnprivilegedJunkScope() "
              "returned null, and inited correctly otherwise!");
 }
 
 AutoSlowOperation::AutoSlowOperation(
     MOZ_GUARD_OBJECT_NOTIFIER_ONLY_PARAM_IN_IMPL)
-    : AutoJSAPI() {
+    : mIsMainThread(NS_IsMainThread()) {
   MOZ_GUARD_OBJECT_NOTIFIER_INIT;
 
-  Init();
+  if (mIsMainThread) {
+    mScriptActivity.emplace(true);
+  }
 }
 
 void AutoSlowOperation::CheckForInterrupt() {
   // For now we support only main thread!
   if (mIsMainThread) {
     // JS_CheckForInterrupt expects us to be in a realm.
-    JSAutoRealm ar(cx(), xpc::UnprivilegedJunkScope());
-    JS_CheckForInterrupt(cx());
+    AutoJSAPI jsapi;
+    if (jsapi.Init(xpc::UnprivilegedJunkScope())) {
+      JS_CheckForInterrupt(jsapi.cx());
+    }
   }
 }
 
 }  // namespace mozilla
diff --git a/dom/script/ScriptSettings.h b/dom/script/ScriptSettings.h
--- a/dom/script/ScriptSettings.h
+++ b/dom/script/ScriptSettings.h
@@ -7,16 +7,17 @@
 /* Utilities for managing the script settings object stack defined in webapps */
 
 #ifndef mozilla_dom_ScriptSettings_h
 #define mozilla_dom_ScriptSettings_h
 
 #include "MainThreadUtils.h"
 #include "nsIGlobalObject.h"
 #include "nsIPrincipal.h"
+#include "xpcpublic.h"
 
 #include "mozilla/Maybe.h"
 
 #include "jsapi.h"
 #include "js/Debug.h"
 
 class nsPIDOMWindowInner;
 class nsGlobalWindowInner;
@@ -362,16 +363,17 @@ class MOZ_STACK_CLASS AutoEntryScript : 
   // the aIsJSImplementedWebIDL case.  And in that case, the subject principal
   // is the principal of the callee function that is part of the CallArgs just a
   // bit up the stack, and which will outlive us.  So we know the principal
   // can't go away until then either.
   nsIPrincipal* MOZ_NON_OWNING_REF mWebIDLCallerPrincipal;
   friend nsIPrincipal* GetWebIDLCallerPrincipal();
 
   Maybe<DocshellEntryMonitor> mDocShellEntryMonitor;
+  Maybe<xpc::AutoScriptActivity> mScriptActivity;
   JS::AutoHideScriptedCaller mCallerOverride;
 };
 
 /*
  * A class that can be used to force a particular incumbent script on the stack.
  */
 class AutoIncumbentScript : protected ScriptSettingsStackEntry {
  public:
@@ -429,27 +431,31 @@ class MOZ_RAII AutoSafeJSContext : publi
  private:
   MOZ_DECL_USE_GUARD_OBJECT_NOTIFIER
 };
 
 /**
  * Use AutoSlowOperation when native side calls many JS callbacks in a row
  * and slow script dialog should be activated if too much time is spent going
  * through those callbacks.
- * AutoSlowOperation puts a JSAutoRequest on the stack so that we don't continue
- * to reset the watchdog and CheckForInterrupt can be then used to check whether
- * JS execution should be interrupted.
+ * AutoSlowOperation puts an AutoScriptActivity on the stack so that we don't
+ * continue to reset the watchdog. CheckForInterrupt can then be used to check
+ * whether JS execution should be interrupted.
+ * This class (including CheckForInterrupt) is a no-op when used off the main
+ * thread.
  */
-class MOZ_RAII AutoSlowOperation : public dom::AutoJSAPI {
+class MOZ_RAII AutoSlowOperation {
  public:
   explicit AutoSlowOperation(MOZ_GUARD_OBJECT_NOTIFIER_ONLY_PARAM);
   void CheckForInterrupt();
 
  private:
   MOZ_DECL_USE_GUARD_OBJECT_NOTIFIER
+  bool mIsMainThread;
+  Maybe<xpc::AutoScriptActivity> mScriptActivity;
 };
 
 /**
  * A class to disable interrupt callback temporary.
  */
 class MOZ_RAII AutoDisableJSInterruptCallback {
  public:
   explicit AutoDisableJSInterruptCallback(JSContext* aCx)
diff --git a/js/xpconnect/src/XPCJSContext.cpp b/js/xpconnect/src/XPCJSContext.cpp
--- a/js/xpconnect/src/XPCJSContext.cpp
+++ b/js/xpconnect/src/XPCJSContext.cpp
@@ -508,28 +508,48 @@ static void WatchdogMain(void* arg) {
 
 PRTime XPCJSContext::GetWatchdogTimestamp(WatchdogTimestampCategory aCategory) {
   AutoLockWatchdog lock(mWatchdogManager->GetWatchdog());
   return aCategory == TimestampContextStateChange
              ? mWatchdogManager->GetContextTimestamp(this, lock)
              : mWatchdogManager->GetTimestamp(aCategory, lock);
 }
 
-void xpc::SimulateActivityCallback(bool aActive) {
-  XPCJSContext::ActivityCallback(XPCJSContext::Get(), aActive);
+// static
+bool XPCJSContext::RecordScriptActivity(bool aActive) {
+  MOZ_ASSERT(NS_IsMainThread());
+
+  XPCJSContext* xpccx = XPCJSContext::Get();
+  if (!xpccx) {
+    // mozilla::SpinEventLoopUntil may use AutoScriptActivity(false) after
+    // we destroyed the XPCJSContext.
+    MOZ_ASSERT(!aActive);
+    return false;
+  }
+
+  bool oldValue = xpccx->SetHasScriptActivity(aActive);
+  if (aActive == oldValue) {
+    // Nothing to do.
+    return oldValue;
+  }
+
+  if (!aActive) {
+    ProcessHangMonitor::ClearHang();
+  }
+  xpccx->mWatchdogManager->RecordContextActivity(xpccx, aActive);
+
+  return oldValue;
 }
 
-// static
-void XPCJSContext::ActivityCallback(void* arg, bool active) {
-  if (!active) {
-    ProcessHangMonitor::ClearHang();
-  }
+AutoScriptActivity::AutoScriptActivity(bool aActive)
+    : mActive(aActive),
+      mOldValue(XPCJSContext::RecordScriptActivity(aActive)) {}
 
-  XPCJSContext* self = static_cast<XPCJSContext*>(arg);
-  self->mWatchdogManager->RecordContextActivity(self, active);
+AutoScriptActivity::~AutoScriptActivity() {
+  MOZ_ALWAYS_TRUE(mActive == XPCJSContext::RecordScriptActivity(mOldValue));
 }
 
 static inline bool IsWebExtensionPrincipal(nsIPrincipal* principal,
                                            nsAString& addonId) {
   if (auto policy = BasePrincipal::Cast(principal)->AddonPolicy()) {
     policy->GetId(addonId);
     return true;
   }
@@ -876,18 +896,16 @@ XPCJSContext::~XPCJSContext() {
 
   Preferences::UnregisterPrefixCallback(ReloadPrefsCallback, JS_OPTIONS_DOT_STR,
                                         this);
 
 #ifdef FUZZING
   Preferences::UnregisterCallback(ReloadPrefsCallback, "fuzzing.enabled", this);
 #endif
 
-  js::SetActivityCallback(Context(), nullptr, nullptr);
-
   // Clear any pending exception.  It might be an XPCWrappedJS, and if we try
   // to destroy it later we will crash.
   SetPendingException(nullptr);
 
   // If we're the last XPCJSContext around, clean up the watchdog manager.
   if (--sInstanceCount == 0) {
     if (mWatchdogManager->GetWatchdog()) {
       mWatchdogManager->StopWatchdog();
@@ -916,16 +934,17 @@ XPCJSContext::~XPCJSContext() {
 XPCJSContext::XPCJSContext()
     : mCallContext(nullptr),
       mAutoRoots(nullptr),
       mResolveName(JSID_VOID),
       mResolvingWrapper(nullptr),
       mWatchdogManager(GetWatchdogManager()),
       mSlowScriptSecondHalf(false),
       mTimeoutAccumulated(false),
+      mHasScriptActivity(false),
       mPendingResult(NS_OK),
       mActive(CONTEXT_INACTIVE),
       mLastStateChange(PR_Now()) {
   MOZ_COUNT_CTOR_INHERITED(XPCJSContext, CycleCollectedJSContext);
   MOZ_RELEASE_ASSERT(!gTlsContext.get());
   MOZ_ASSERT(mWatchdogManager);
   ++sInstanceCount;
   mWatchdogManager->RegisterContext(this);
@@ -1119,17 +1138,16 @@ nsresult XPCJSContext::Initialize(XPCJSC
   (void)kDefaultStackQuota;
 
   JS_SetNativeStackQuota(
       cx, kStackQuota, kStackQuota - kSystemCodeBuffer,
       kStackQuota - kSystemCodeBuffer - kTrustedScriptBuffer);
 
   PROFILER_SET_JS_CONTEXT(cx);
 
-  js::SetActivityCallback(cx, ActivityCallback, this);
   JS_AddInterruptCallback(cx, InterruptCallback);
 
   if (!aPrimaryContext) {
     Runtime()->Initialize(cx);
   }
 
   // Watch for the JS boolean options.
   ReloadPrefsCallback(nullptr, this);
diff --git a/js/xpconnect/src/XPCShellImpl.cpp b/js/xpconnect/src/XPCShellImpl.cpp
--- a/js/xpconnect/src/XPCShellImpl.cpp
+++ b/js/xpconnect/src/XPCShellImpl.cpp
@@ -524,24 +524,30 @@ static bool SetInterruptCallback(JSConte
     return false;
   }
 
   *sScriptedInterruptCallback = args[0];
 
   return true;
 }
 
-static bool SimulateActivityCallback(JSContext* cx, unsigned argc, Value* vp) {
+static bool SimulateNoScriptActivity(JSContext* cx, unsigned argc, Value* vp) {
   // Sanity-check args.
   JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
-  if (args.length() != 1 || !args[0].isBoolean()) {
-    JS_ReportErrorASCII(cx, "Wrong number of arguments");
+  if (args.length() != 1 || !args[0].isInt32() || args[0].toInt32() < 0) {
+    JS_ReportErrorASCII(cx, "Expected a positive integer argument");
     return false;
   }
-  xpc::SimulateActivityCallback(args[0].toBoolean());
+
+  // This mimics mozilla::SpinEventLoopUntil but instead of spinning the
+  // event loop we sleep, to make sure we don't run script.
+  xpc::AutoScriptActivity asa(false);
+  PR_Sleep(PR_SecondsToInterval(args[0].toInt32()));
+
+  args.rval().setUndefined();
   return true;
 }
 
 static bool RegisterAppManifest(JSContext* cx, unsigned argc, Value* vp) {
   JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
   if (args.length() != 1) {
     JS_ReportErrorASCII(cx, "Wrong number of arguments");
     return false;
@@ -596,17 +602,17 @@ static const JSFunctionSpec glob_functio
 #ifdef JS_GC_ZEAL
     JS_FN("gczeal",          GCZeal,         1,0),
 #endif
     JS_FN("options",         Options,        0,0),
     JS_FN("sendCommand",     SendCommand,    1,0),
     JS_FN("atob",            xpc::Atob,      1,0),
     JS_FN("btoa",            xpc::Btoa,      1,0),
     JS_FN("setInterruptCallback", SetInterruptCallback, 1,0),
-    JS_FN("simulateActivityCallback", SimulateActivityCallback, 1,0),
+    JS_FN("simulateNoScriptActivity", SimulateNoScriptActivity, 1,0),
     JS_FN("registerAppManifest", RegisterAppManifest, 1, 0),
 #ifdef ENABLE_TESTS
     JS_FN("registerXPCTestComponents", RegisterXPCTestComponents, 0, 0),
 #endif
     JS_FS_END
     // clang-format on
 };
 
diff --git a/js/xpconnect/src/xpcprivate.h b/js/xpconnect/src/xpcprivate.h
--- a/js/xpconnect/src/xpcprivate.h
+++ b/js/xpconnect/src/xpcprivate.h
@@ -370,17 +370,24 @@ class XPCJSContext final : public mozill
 
   AutoMarkingPtr** GetAutoRootsAdr() { return &mAutoRoots; }
 
   nsresult GetPendingResult() { return mPendingResult; }
   void SetPendingResult(nsresult rv) { mPendingResult = rv; }
 
   PRTime GetWatchdogTimestamp(WatchdogTimestampCategory aCategory);
 
-  static void ActivityCallback(void* arg, bool active);
+  static bool RecordScriptActivity(bool aActive);
+
+  bool SetHasScriptActivity(bool aActive) {
+    bool oldValue = mHasScriptActivity;
+    mHasScriptActivity = aActive;
+    return oldValue;
+  }
+
   static bool InterruptCallback(JSContext* cx);
 
   // Mapping of often used strings to jsid atoms that live 'forever'.
   //
   // To add a new string: add to this list and to XPCJSRuntime::mStrings
   // at the top of XPCJSRuntime.cpp
   enum {
     IDX_CONSTRUCTOR = 0,
@@ -457,16 +464,18 @@ class XPCJSContext final : public mozill
   // 2. mSlowScriptSecondHalf was set to true
   // (whichever comes later). We use it to determine whether the interrupt
   // callback needs to do anything.
   mozilla::TimeStamp mSlowScriptCheckpoint;
   // Accumulates total time we actually waited for telemetry
   mozilla::TimeDuration mSlowScriptActualWait;
   bool mTimeoutAccumulated;
 
+  bool mHasScriptActivity;
+
   // mPendingResult is used to implement Components.returnCode.  Only really
   // meaningful while calling through XPCWrappedJS.
   nsresult mPendingResult;
 
   // These members must be accessed via WatchdogManager.
   enum { CONTEXT_ACTIVE, CONTEXT_INACTIVE } mActive;
   PRTime mLastStateChange;
 
diff --git a/js/xpconnect/src/xpcpublic.h b/js/xpconnect/src/xpcpublic.h
--- a/js/xpconnect/src/xpcpublic.h
+++ b/js/xpconnect/src/xpcpublic.h
@@ -468,17 +468,23 @@ nsGlobalWindowInner* WindowOrNull(JSObje
 nsGlobalWindowInner* WindowGlobalOrNull(JSObject* aObj);
 
 /**
  * If |cx| is in a compartment whose global is a window, returns the associated
  * nsGlobalWindow. Otherwise, returns null.
  */
 nsGlobalWindowInner* CurrentWindowOrNull(JSContext* cx);
 
-void SimulateActivityCallback(bool aActive);
+class MOZ_RAII AutoScriptActivity {
+  bool mActive;
+  bool mOldValue;
+ public:
+  explicit AutoScriptActivity(bool aActive);
+  ~AutoScriptActivity();
+};
 
 // This function may be used off-main-thread, in which case it is benignly
 // racey.
 bool ShouldDiscardSystemSource();
 
 bool SharedMemoryEnabled();
 
 bool ExtraWarningsForSystemJS();
diff --git a/js/xpconnect/tests/unit/test_watchdog_hibernate.js b/js/xpconnect/tests/unit/test_watchdog_hibernate.js
--- a/js/xpconnect/tests/unit/test_watchdog_hibernate.js
+++ b/js/xpconnect/tests/unit/test_watchdog_hibernate.js
@@ -18,23 +18,21 @@ async function testBody() {
   Assert.ok(startHibernation < now);
   Assert.ok(stopHibernation < now);
 
   // When the watchdog runs, it hibernates if there's been no activity for the
   // last 2 seconds, otherwise it sleeps for 1 second. As such, given perfect
   // scheduling, we should never have more than 3 seconds of inactivity without
   // hibernating. To add some padding for automation, we mandate that hibernation
   // must begin between 2 and 5 seconds from now.
-  await new Promise(resolve => {
-    var timer = Cc["@mozilla.org/timer;1"].createInstance(Ci.nsITimer);
-    timer.initWithCallback(resolve, 10000, Ci.nsITimer.TYPE_ONE_SHOT);
-    simulateActivityCallback(false);
-  });
 
-  simulateActivityCallback(true);
+  // Sleep for 10 seconds. Note: we don't use nsITimer here because then we may run
+  // arbitrary (idle) events involving script before it fires.
+  simulateNoScriptActivity(10);
+
   busyWait(1000); // Give the watchdog time to wake up on the condvar.
   var stateChange = Cu.getWatchdogTimestamp("ContextStateChange");
   startHibernation = Cu.getWatchdogTimestamp("WatchdogHibernateStart");
   stopHibernation = Cu.getWatchdogTimestamp("WatchdogHibernateStop");
   do_log_info("Post-hibernation statistics:");
   do_log_info("stateChange: " + stateChange / 1000000);
   do_log_info("startHibernation: " + startHibernation / 1000000);
   do_log_info("stopHibernation: " + stopHibernation / 1000000);
diff --git a/xpcom/threads/nsThreadUtils.h b/xpcom/threads/nsThreadUtils.h
--- a/xpcom/threads/nsThreadUtils.h
+++ b/xpcom/threads/nsThreadUtils.h
@@ -16,19 +16,21 @@
 #include "nsINamed.h"
 #include "nsIRunnable.h"
 #include "nsIThreadManager.h"
 #include "nsITimer.h"
 #include "nsIThread.h"
 #include "nsString.h"
 #include "nsCOMPtr.h"
 #include "nsAutoPtr.h"
+#include "xpcpublic.h"
 #include "mozilla/Atomics.h"
 #include "mozilla/IndexSequence.h"
 #include "mozilla/Likely.h"
+#include "mozilla/Maybe.h"
 #include "mozilla/Move.h"
 #include "mozilla/TimeStamp.h"
 #include "mozilla/Tuple.h"
 #include "mozilla/TypeTraits.h"
 
 //-----------------------------------------------------------------------------
 // These methods are alternatives to the methods on nsIThreadManager, provided
 // for convenience.
@@ -302,16 +304,24 @@ enum class ProcessFailureBehavior {
 };
 
 template <
     ProcessFailureBehavior Behavior = ProcessFailureBehavior::ReportToCaller,
     typename Pred>
 bool SpinEventLoopUntil(Pred&& aPredicate, nsIThread* aThread = nullptr) {
   nsIThread* thread = aThread ? aThread : NS_GetCurrentThread();
 
+  // From a latency perspective, spinning the event loop is like leaving script
+  // and returning to the event loop. Tell the watchdog we stopped running
+  // script (until we return).
+  mozilla::Maybe<xpc::AutoScriptActivity> asa;
+  if (NS_IsMainThread()) {
+    asa.emplace(false);
+  }
+
   while (!aPredicate()) {
     bool didSomething = NS_ProcessNextEvent(thread, true);
 
     if (Behavior == ProcessFailureBehavior::IgnoreAndContinue) {
       // Don't care what happened, continue on.
       continue;
     } else if (!didSomething) {
       return false;
