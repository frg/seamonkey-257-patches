# HG changeset patch
# User Jason Orendorff <jorendorff@mozilla.com>
# Date 1520962883 18000
# Node ID 260f636f90793a27c01164ec9009916bf0c9362f
# Parent  28344c67780a11dda418818ef9eb92953180f086
Bug 1443583 - Part 2.6: Make DebuggerFrame_freeScriptFrameIterData a method of DebuggerFrame. r=jimb.

Most of the work is to smooth out the types.

diff --git a/js/src/vm/Debugger.cpp b/js/src/vm/Debugger.cpp
--- a/js/src/vm/Debugger.cpp
+++ b/js/src/vm/Debugger.cpp
@@ -775,18 +775,16 @@ bool Debugger::hasAnyLiveHooks(JSRuntime
   }
 
   return resumeMode;
 }
 
 static void DebuggerFrame_maybeDecrementFrameScriptStepModeCount(
     FreeOp* fop, AbstractFramePtr frame, NativeObject* frameobj);
 
-static void DebuggerFrame_freeScriptFrameIterData(FreeOp* fop, JSObject* obj);
-
 /*
  * Handle leaving a frame with debuggers watching. |frameOk| indicates whether
  * the frame is exiting normally or abruptly. Set |cx|'s exception and/or
  * |cx->fp()|'s return value, and return a new success value.
  */
 /* static */ bool Debugger::slowPathOnLeaveFrame(JSContext* cx,
                                                  AbstractFramePtr frame,
                                                  jsbytecode* pc, bool frameOk) {
@@ -3812,19 +3810,19 @@ void Debugger::removeDebuggeeGlobal(Free
    * Debugger objects that are no longer debugging the relevant global might
    * have live Frame objects. So we take the easy way out and kill them here.
    * This is a bug, since it's observable and contrary to the spec. One
    * possible fix would be to put such objects into a compartment-wide bag
    * which slowPathOnLeaveFrame would have to examine.
    */
   for (FrameMap::Enum e(frames); !e.empty(); e.popFront()) {
     AbstractFramePtr frame = e.front().key();
-    NativeObject* frameobj = e.front().value();
+    DebuggerFrame* frameobj = e.front().value();
     if (frame.global() == global) {
-      DebuggerFrame_freeScriptFrameIterData(fop, frameobj);
+      frameobj->freeFrameIterData(fop);
       DebuggerFrame_maybeDecrementFrameScriptStepModeCount(fop, frame,
                                                            frameobj);
       e.removeFront();
     }
   }
 
   auto* globalDebuggersVector = global->getDebuggers();
   auto* zoneDebuggersVector = global->zone()->getDebuggers();
@@ -5876,18 +5874,17 @@ bool Debugger::observesWasm(wasm::Instan
   auto removeToDebuggerFramesOnExit =
       MakeScopeExit([&] { removeFromFrameMapsAndClearBreakpointsIn(cx, to); });
 
   for (size_t i = 0; i < frames.length(); i++) {
     HandleDebuggerFrame frameobj = frames[i];
     Debugger* dbg = Debugger::fromChildJSObject(frameobj);
 
     // Update frame object's ScriptFrameIter::data pointer.
-    DebuggerFrame_freeScriptFrameIterData(cx->runtime()->defaultFreeOp(),
-                                          frameobj);
+    frameobj->freeFrameIterData(cx->runtime()->defaultFreeOp());
     ScriptFrameIter::Data* data = iter.copyData();
     if (!data) {
       // An OOM here means that some Debuggers' frame maps may still
       // contain entries for 'from' and some Debuggers' frame maps may
       // also contain entries for 'to'. Thus both
       // removeFromDebuggerFramesOnExit and
       // removeToDebuggerFramesOnExit must both run.
       //
@@ -5906,43 +5903,45 @@ bool Debugger::observesWasm(wasm::Instan
       // This OOM is subtle. At this point, both
       // removeFromDebuggerFramesOnExit and removeToDebuggerFramesOnExit
       // must both run for the same reason given above.
       //
       // The difference is that the current frameobj is no longer in its
       // Debugger's frame map, so it will not be cleaned up by neither
       // lambda. Manually clean it up here.
       FreeOp* fop = cx->runtime()->defaultFreeOp();
-      DebuggerFrame_freeScriptFrameIterData(fop, frameobj);
+      frameobj->freeFrameIterData(fop);
       DebuggerFrame_maybeDecrementFrameScriptStepModeCount(fop, to, frameobj);
 
       ReportOutOfMemory(cx);
       return false;
     }
   }
 
   // All frames successfuly replaced, cancel the rollback.
   removeToDebuggerFramesOnExit.release();
 
   return true;
 }
 
 /* static */ bool Debugger::inFrameMaps(AbstractFramePtr frame) {
   bool foundAny = false;
-  forEachDebuggerFrame(frame, [&](NativeObject* frameobj) { foundAny = true; });
+  forEachDebuggerFrame(frame, [&](DebuggerFrame* frameobj) {
+    foundAny = true;
+  });
   return foundAny;
 }
 
 /* static */ void Debugger::removeFromFrameMapsAndClearBreakpointsIn(
     JSContext* cx, AbstractFramePtr frame) {
-  forEachDebuggerFrame(frame, [&](NativeObject* frameobj) {
+  forEachDebuggerFrame(frame, [&](DebuggerFrame* frameobj) {
     Debugger* dbg = Debugger::fromChildJSObject(frameobj);
 
     FreeOp* fop = cx->runtime()->defaultFreeOp();
-    DebuggerFrame_freeScriptFrameIterData(fop, frameobj);
+    frameobj->freeFrameIterData(fop);
     DebuggerFrame_maybeDecrementFrameScriptStepModeCount(fop, frame, frameobj);
 
     dbg->frames.remove(frame);
   });
 
   /*
    * If this is an eval frame, then from the debugger's perspective the
    * script is about to be destroyed. Remove any breakpoints in it.
@@ -7469,21 +7468,20 @@ FrameIter::Data* DebuggerFrame::frameIte
     ReportValueErrorFlags(cx, JSREPORT_ERROR, JSMSG_DEBUG_BAD_REFERENT,
                           JSDVG_SEARCH_STACK, frameobj, nullptr,
                           "a script frame", nullptr);
     return false;
   }
   return true;
 }
 
-static void DebuggerFrame_freeScriptFrameIterData(FreeOp* fop, JSObject* obj) {
-  DebuggerFrame& frame = obj->as<DebuggerFrame>();
-  if (FrameIter::Data* data = frame.frameIterData()) {
+void DebuggerFrame::freeFrameIterData(FreeOp* fop) {
+  if (FrameIter::Data* data = frameIterData()) {
     fop->delete_(data);
-    frame.setPrivate(nullptr);
+    setPrivate(nullptr);
   }
 }
 
 static void DebuggerFrame_maybeDecrementFrameScriptStepModeCount(
     FreeOp* fop, AbstractFramePtr frame, NativeObject* frameobj) {
   /* If this frame has an onStep handler, decrement the script's count. */
   if (frameobj->getReservedSlot(JSSLOT_DEBUGFRAME_ONSTEP_HANDLER).isUndefined())
     return;
@@ -7493,20 +7491,21 @@ static void DebuggerFrame_maybeDecrement
         fop, frame.asWasmDebugFrame()->funcIndex());
   } else {
     frame.script()->decrementStepModeCount(fop);
   }
 }
 
 static void DebuggerFrame_finalize(FreeOp* fop, JSObject* obj) {
   MOZ_ASSERT(fop->maybeOnHelperThread());
-  DebuggerFrame_freeScriptFrameIterData(fop, obj);
-  OnStepHandler* onStepHandler = obj->as<DebuggerFrame>().onStepHandler();
+  DebuggerFrame& frameobj = obj->as<DebuggerFrame>();
+  frameobj.freeFrameIterData(fop);
+  OnStepHandler* onStepHandler = frameobj.onStepHandler();
   if (onStepHandler) onStepHandler->drop();
-  OnPopHandler* onPopHandler = obj->as<DebuggerFrame>().onPopHandler();
+  OnPopHandler* onPopHandler = frameobj.onPopHandler();
   if (onPopHandler) onPopHandler->drop();
 }
 
 static void DebuggerFrame_trace(JSTracer* trc, JSObject* obj) {
   OnStepHandler* onStepHandler = obj->as<DebuggerFrame>().onStepHandler();
   if (onStepHandler) onStepHandler->trace(trc);
   OnPopHandler* onPopHandler = obj->as<DebuggerFrame>().onPopHandler();
   if (onPopHandler) onPopHandler->trace(trc);
diff --git a/js/src/vm/Debugger.h b/js/src/vm/Debugger.h
--- a/js/src/vm/Debugger.h
+++ b/js/src/vm/Debugger.h
@@ -722,17 +722,17 @@ class Debugger : private mozilla::Linked
   static bool updateExecutionObservabilityOfFrames(
       JSContext* cx, const ExecutionObservableSet& obs, IsObserving observing);
   static bool updateExecutionObservabilityOfScripts(
       JSContext* cx, const ExecutionObservableSet& obs, IsObserving observing);
   static bool updateExecutionObservability(JSContext* cx,
                                            ExecutionObservableSet& obs,
                                            IsObserving observing);
 
-  template <typename FrameFn /* void (NativeObject*) */>
+  template <typename FrameFn /* void (DebuggerFrame*) */>
   static void forEachDebuggerFrame(AbstractFramePtr frame, FrameFn fn);
 
   /*
    * Return a vector containing all Debugger.Frame instances referring to
    * |frame|. |global| is |frame|'s global object; if nullptr or omitted, we
    * compute it ourselves from |frame|.
    */
   using DebuggerFrameVector = GCVector<DebuggerFrame*>;
@@ -1374,16 +1374,17 @@ class DebuggerFrame : public NativeObjec
 
   static const Class class_;
 
   static NativeObject* initClass(JSContext* cx, HandleObject dbgCtor,
                                  HandleObject objProto);
   static DebuggerFrame* create(JSContext* cx, HandleObject proto,
                                const FrameIter& iter,
                                HandleNativeObject debugger);
+  void freeFrameIterData(FreeOp* fop);
 
   static MOZ_MUST_USE bool getArguments(JSContext* cx,
                                         HandleDebuggerFrame frame,
                                         MutableHandleDebuggerArguments result);
   static MOZ_MUST_USE bool getCallee(JSContext* cx, HandleDebuggerFrame frame,
                                      MutableHandleDebuggerObject result);
   static MOZ_MUST_USE bool getIsConstructing(JSContext* cx,
                                              HandleDebuggerFrame frame,
