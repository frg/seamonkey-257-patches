# HG changeset patch
# User Jon Coppeard <jcoppeard@mozilla.com>
# Date 1524558734 -3600
# Node ID d736cc8113cf3b13f70e79ec5d7c0023c71cb654
# Parent  083e80637cd2f0718ad0f76c0e483203e71d0069
Bug 1455608 - Allow js::TraceRuntime to operate if GC is suppressed r=sfink

diff --git a/js/src/gc/RootMarking.cpp b/js/src/gc/RootMarking.cpp
--- a/js/src/gc/RootMarking.cpp
+++ b/js/src/gc/RootMarking.cpp
@@ -223,30 +223,33 @@ void PropertyDescriptor::trace(JSTracer*
     JSObject* tmp = JS_FUNC_TO_DATA_PTR(JSObject*, setter);
     TraceRoot(trc, &tmp, "Descriptor::set");
     setter = JS_DATA_TO_FUNC_PTR(JSSetterOp, tmp);
   }
 }
 
 void js::gc::GCRuntime::traceRuntimeForMajorGC(JSTracer* trc,
                                                AutoTraceSession& session) {
+  MOZ_ASSERT(!TlsContext.get()->suppressGC);
   MOZ_ASSERT_IF(atomsZone->isCollecting(), session.maybeLock.isSome());
 
   // FinishRoots will have asserted that every root that we do not expect
   // is gone, so we can simply skip traceRuntime here.
   if (rt->isBeingDestroyed()) return;
 
   gcstats::AutoPhase ap(stats(), gcstats::PhaseKind::MARK_ROOTS);
   if (atomsZone->isCollecting()) traceRuntimeAtoms(trc, session.lock());
   JSCompartment::traceIncomingCrossCompartmentEdgesForZoneGC(trc);
   traceRuntimeCommon(trc, MarkRuntime, session);
 }
 
 void js::gc::GCRuntime::traceRuntimeForMinorGC(JSTracer* trc,
                                                AutoTraceSession& session) {
+  MOZ_ASSERT(!TlsContext.get()->suppressGC);
+
   // Note that we *must* trace the runtime during the SHUTDOWN_GC's minor GC
   // despite having called FinishRoots already. This is because FinishRoots
   // does not clear the crossCompartmentWrapper map. It cannot do this
   // because Proxy's trace for CrossCompartmentWrappers asserts presence in
   // the map. And we can reach its trace function despite having finished the
   // roots via the edges stored by the pre-barrier verifier when we finish
   // the verifier for the last time.
   gcstats::AutoPhase ap(stats(), gcstats::PhaseKind::MARK_ROOTS);
@@ -281,18 +284,16 @@ void js::gc::GCRuntime::traceRuntimeAtom
   TraceAtoms(trc, lock);
   TraceWellKnownSymbols(trc);
   jit::JitRuntime::Trace(trc, lock);
 }
 
 void js::gc::GCRuntime::traceRuntimeCommon(JSTracer* trc,
                                            TraceOrMarkRuntime traceOrMark,
                                            AutoTraceSession& session) {
-  MOZ_ASSERT(!TlsContext.get()->suppressGC);
-
   {
     gcstats::AutoPhase ap(stats(), gcstats::PhaseKind::MARK_STACK);
 
     JSContext* cx = rt->mainContextFromOwnThread();
 
     // Trace active interpreter and JIT stack roots.
     TraceInterpreterActivations(cx, trc);
     jit::TraceJitActivations(cx, trc);
diff --git a/js/src/jit-test/tests/gc/bug-1455608.js b/js/src/jit-test/tests/gc/bug-1455608.js
new file mode 100644
--- /dev/null
+++ b/js/src/jit-test/tests/gc/bug-1455608.js
@@ -0,0 +1,25 @@
+// |jit-test| --wasm-gc
+
+var lfModule = new WebAssembly.Module(wasmTextToBinary(`
+    (module
+        (import "global" "func" (result i32))
+        (func (export "func_0") (result i32)
+         call 0 ;; calls the import, which is func #0
+        )
+    )
+`));
+processModule(lfModule, `
+  var dbg = new Debugger;
+  dbg.memory.takeCensus()
+`);
+function processModule(module, jscode) {
+    imports = {}
+    for (let descriptor of WebAssembly.Module.imports(module)) {
+        imports[descriptor.module] = {}
+        imports[descriptor.module][descriptor.name] = new Function("x", "y", "z", jscode);
+        instance = new WebAssembly.Instance(module, imports);
+        for (let descriptor of WebAssembly.Module.exports(module)) {
+            print(instance.exports[descriptor.name]())
+        }
+    }
+}
