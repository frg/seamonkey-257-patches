# HG changeset patch
# User Steve Fink <sfink@mozilla.com>
# Date 1521175353 25200
# Node ID 1085c5fbc6e87055747095ffc3a08456afa9bcd6
# Parent  af2b5a03b1198791f162b7e68bf9dbaebae097bd
Bug 1446254 - Update various files for move to non262/, r=Waldo

diff --git a/intl/update-icu.sh b/intl/update-icu.sh
--- a/intl/update-icu.sh
+++ b/intl/update-icu.sh
@@ -1,53 +1,37 @@
 #!/bin/sh
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 set -e
 
-# Update to an ICU release:
-#   Usage: update-icu.sh <URL of ICU GIT> <release tag name>
-#   E.g., for ICU 62.1: update-icu.sh https://github.com/unicode-org/icu.git release-62-1
-#
-# Update to an ICU maintenance branch:
-#   Usage: update-icu.sh <URL of ICU GIT> <maintenance name>
-#   E.g., for ICU 62.1: update-icu.sh https://github.com/unicode-org/icu.git maint/maint-62
+# Usage: update-icu.sh <URL of ICU SVN with release>
+# E.g., for ICU 58.2: update-icu.sh https://ssl.icu-project.org/repos/icu/tags/release-58-2/icu4c/
 
-if [ $# -lt 2 ]; then
-  echo "Usage: update-icu.sh <URL of ICU GIT> <release tag name>"
+if [ $# -lt 1 ]; then
+  echo "Usage: update-icu.sh <URL of ICU SVN with release>"
   exit 1
 fi
 
-# Ensure that $Date$ in the checked-out git files expands timezone-agnostically,
+# Ensure that $Date$ in the checked-out svn files expands timezone-agnostically,
 # so that this script's behavior is consistent when run from any time zone.
 export TZ=UTC
 
-# Also ensure GIT-INFO is consistently English.
+# Also ensure SVN-INFO is consistently English.
 export LANG=en_US.UTF-8
 export LANGUAGE=en_US
 export LC_ALL=en_US.UTF-8
 
 icu_dir=`dirname $0`/icu
 
 # Remove intl/icu/source, then replace it with a clean export.
 rm -rf ${icu_dir}/source
-tmpclonedir=$(mktemp -d)
-git clone --depth 1 --branch $2 $1 ${tmpclonedir}
-cp -r ${tmpclonedir}/icu4c/source ${icu_dir}/source
-
-# Record `git log`.
-# (This ensures that if ICU modifications are performed properly, it's always
-# possible to run the command at the top of this script and make no changes to
-# the tree.)
-git -C ${tmpclonedir} log -1 > ${icu_dir}/GIT-INFO
-
-# Clean up after ourselves.
-rm -rf ${tmpclonedir}
+svn export $1/source/ ${icu_dir}/source
 
 # remove layoutex, tests, and samples, but leave makefiles in place
 find ${icu_dir}/source/layoutex -name '*Makefile.in' -prune -or -type f -print | xargs rm
 find ${icu_dir}/source/test -name '*Makefile.in' -prune -or -type f -print | xargs rm
 find ${icu_dir}/source/samples -name '*Makefile.in' -prune -or -type f -print | xargs rm
 
 # remove data that we currently don't need
 rm -rf ${icu_dir}/source/data/brkitr/*
@@ -69,16 +53,23 @@ find ${icu_dir}/source/data/zone \
     -name root.txt -prune -or \
     -name tzdbNames.txt -prune -or \
     -name '*.txt' -print | xargs sed -i '/^\s\{8\}\"[A-Z]/, /^\s\{8\}}/ { d }'
 find ${icu_dir}/source/data/zone \
     -name root.txt -prune -or \
     -name tzdbNames.txt -prune -or \
     -name '*.txt' -print | xargs sed -i '/^\s\{4\}zoneStrings{/{N; s/^\s\{4\}zoneStrings{\n\s\{4\}}// }; /^$/d'
 
+# Record `svn info`, eliding the line that changes every time the entire ICU
+# repository (not just the path within it we care about) receives a commit.
+# (This ensures that if ICU modifications are performed properly, it's always
+# possible to run the command at the top of this script and make no changes to
+# the tree.)
+svn info $1 | grep -v '^Revision: [[:digit:]]\+$' > ${icu_dir}/SVN-INFO
+
 for patch in \
  bug-915735 \
  suppress-warnings.diff \
  bug-1172609-timezone-recreateDefault.diff \
  bug-1198952-workaround-make-3.82-bug.diff \
  bug-1422415-numberformat-sprintfl-windows.diff \
 ; do
   echo "Applying local patch $patch"
@@ -87,12 +78,12 @@ done
 
 topsrcdir=`dirname $0`/../
 python ${topsrcdir}/js/src/tests/non262/String/make-normalize-generateddata-input.py $topsrcdir
 
 # Update our moz.build files in config/external/icu, and
 # build a new ICU data file.
 python `dirname $0`/icu_sources_data.py $topsrcdir
 
-hg addremove "${icu_dir}/source" "${icu_dir}/GIT-INFO" ${topsrcdir}/config/external/icu
+hg addremove "${icu_dir}/source" "${icu_dir}/SVN-INFO" ${topsrcdir}/config/external/icu
 
 # Check local tzdata version.
 `dirname $0`/update-tzdata.sh -c
diff --git a/js/src/devtools/automation/arm64-jstests-slow.txt b/js/src/devtools/automation/arm64-jstests-slow.txt
--- a/js/src/devtools/automation/arm64-jstests-slow.txt
+++ b/js/src/devtools/automation/arm64-jstests-slow.txt
@@ -1,49 +1,45 @@
-ecma/Date/15.9.5.10-2.js
-ecma/Date/15.9.5.11-2.js
-ecma/Date/15.9.5.12-2.js
-ecma/Date/15.9.5.8.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-01-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-02-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-03-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-04-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-05-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-06-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-07-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-08-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-09-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-10-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-11-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-12-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-13-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-14-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-15-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-16-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-17-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-18-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-19-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-20-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-21-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-22-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-23-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-24-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-25-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-26-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-27-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-30-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-31-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-32-of-32.js
-ecma_5/Object/15.2.3.6-middle-redefinition-1-of-8.js
-ecma_5/Object/15.2.3.6-middle-redefinition-2-of-8.js
-ecma_5/Object/15.2.3.6-middle-redefinition-3-of-8.js
-ecma_5/Object/15.2.3.6-middle-redefinition-4-of-8.js
-ecma_5/Object/15.2.3.6-middle-redefinition-5-of-8.js
-ecma_5/Object/15.2.3.6-middle-redefinition-6-of-8.js
-ecma_5/Object/15.2.3.6-middle-redefinition-7-of-8.js
-ecma_5/Object/15.2.3.6-middle-redefinition-8-of-8.js
-ecma_5/Object/15.2.3.6-redefinition-1-of-4.js
-ecma_5/Object/15.2.3.6-redefinition-2-of-4.js
-ecma_5/Object/15.2.3.6-redefinition-3-of-4.js
-ecma_5/Object/15.2.3.6-redefinition-4-of-4.js
-js1_8_5/extensions/clone-complex-object.js
-js1_8_5/reflect-parse/classes.js
-js1_8_5/reflect-parse/destructuring-variable-declarations.js
+non262/object/15.2.3.6-dictionary-redefinition-01-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-02-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-03-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-04-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-05-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-06-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-07-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-08-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-09-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-10-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-11-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-12-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-13-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-14-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-15-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-16-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-17-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-18-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-19-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-20-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-21-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-22-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-23-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-24-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-25-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-26-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-27-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-30-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-31-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-32-of-32.js
+non262/object/15.2.3.6-middle-redefinition-1-of-8.js
+non262/object/15.2.3.6-middle-redefinition-2-of-8.js
+non262/object/15.2.3.6-middle-redefinition-3-of-8.js
+non262/object/15.2.3.6-middle-redefinition-4-of-8.js
+non262/object/15.2.3.6-middle-redefinition-5-of-8.js
+non262/object/15.2.3.6-middle-redefinition-6-of-8.js
+non262/object/15.2.3.6-middle-redefinition-7-of-8.js
+non262/object/15.2.3.6-middle-redefinition-8-of-8.js
+non262/object/15.2.3.6-redefinition-1-of-4.js
+non262/object/15.2.3.6-redefinition-2-of-4.js
+non262/object/15.2.3.6-redefinition-3-of-4.js
+non262/object/15.2.3.6-redefinition-4-of-4.js
+non262/extensions/clone-complex-object.js
+non262/reflect-parse/classes.js
+non262/reflect-parse/destructuring-variable-declarations.js
diff --git a/js/src/devtools/automation/cgc-jstests-slow.txt b/js/src/devtools/automation/cgc-jstests-slow.txt
--- a/js/src/devtools/automation/cgc-jstests-slow.txt
+++ b/js/src/devtools/automation/cgc-jstests-slow.txt
@@ -1,65 +1,65 @@
-ecma_5/Object/15.2.3.6-dictionary-redefinition-01-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-02-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-03-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-04-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-05-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-06-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-07-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-08-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-09-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-10-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-11-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-12-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-13-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-14-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-15-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-16-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-17-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-18-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-19-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-20-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-21-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-22-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-23-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-24-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-25-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-26-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-27-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-28-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-29-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-30-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-31-of-32.js
-ecma_5/Object/15.2.3.6-dictionary-redefinition-32-of-32.js
-ecma_5/Object/15.2.3.6-middle-redefinition-1-of-8.js
-ecma_5/Object/15.2.3.6-middle-redefinition-2-of-8.js
-ecma_5/Object/15.2.3.6-middle-redefinition-3-of-8.js
-ecma_5/Object/15.2.3.6-middle-redefinition-4-of-8.js
-ecma_5/Object/15.2.3.6-middle-redefinition-5-of-8.js
-ecma_5/Object/15.2.3.6-middle-redefinition-6-of-8.js
-ecma_5/Object/15.2.3.6-middle-redefinition-7-of-8.js
-ecma_5/Object/15.2.3.6-middle-redefinition-8-of-8.js
-ecma_5/Object/15.2.3.6-redefinition-1-of-4.js
-ecma_5/Object/15.2.3.6-redefinition-2-of-4.js
-ecma_5/Object/15.2.3.6-redefinition-3-of-4.js
-ecma_5/Object/15.2.3.6-redefinition-4-of-4.js
-ecma_6/extensions/array-isArray-proxy-recursion.js
-ecma_6/String/normalize-generateddata-part0.js
-ecma_6/String/normalize-generateddata-part1-not-listed.js
-ecma_6/String/normalize-generateddata-part1.js
-ecma_6/String/normalize-generateddata-part2.js
-ecma_6/String/normalize-generateddata-part3.js
-js1_5/GC/regress-203278-2.js
-js1_5/GC/regress-203278-3.js
-js1_5/GC/regress-278725.js
-js1_5/Regress/regress-312588.js
-js1_5/Regress/regress-321971.js
-js1_5/Regress/regress-360969-01.js
-js1_5/Regress/regress-360969-02.js
-js1_5/Regress/regress-360969-03.js
-js1_5/Regress/regress-360969-04.js
-js1_5/Regress/regress-360969-05.js
-js1_5/Regress/regress-360969-06.js
-js1_8_1/extensions/regress-477187.js
-js1_8_1/regress/regress-452498-052-a.js
-js1_8_5/extensions/clone-complex-object.js
-js1_8_5/extensions/clone-object-deep.js
+non262/object/15.2.3.6-dictionary-redefinition-01-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-02-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-03-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-04-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-05-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-06-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-07-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-08-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-09-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-10-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-11-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-12-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-13-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-14-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-15-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-16-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-17-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-18-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-19-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-20-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-21-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-22-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-23-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-24-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-25-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-26-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-27-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-28-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-29-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-30-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-31-of-32.js
+non262/object/15.2.3.6-dictionary-redefinition-32-of-32.js
+non262/object/15.2.3.6-middle-redefinition-1-of-8.js
+non262/object/15.2.3.6-middle-redefinition-2-of-8.js
+non262/object/15.2.3.6-middle-redefinition-3-of-8.js
+non262/object/15.2.3.6-middle-redefinition-4-of-8.js
+non262/object/15.2.3.6-middle-redefinition-5-of-8.js
+non262/object/15.2.3.6-middle-redefinition-6-of-8.js
+non262/object/15.2.3.6-middle-redefinition-7-of-8.js
+non262/object/15.2.3.6-middle-redefinition-8-of-8.js
+non262/object/15.2.3.6-redefinition-1-of-4.js
+non262/object/15.2.3.6-redefinition-2-of-4.js
+non262/object/15.2.3.6-redefinition-3-of-4.js
+non262/object/15.2.3.6-redefinition-4-of-4.js
+non262/extensions/array-isArray-proxy-recursion.js
+non262/String/normalize-generateddata-part0.js
+non262/String/normalize-generateddata-part1-not-listed.js
+non262/String/normalize-generateddata-part1.js
+non262/String/normalize-generateddata-part2.js
+non262/String/normalize-generateddata-part3.js
+non262/GC/regress-203278-2.js
+non262/GC/regress-203278-3.js
+non262/GC/regress-278725.js
+non262/regress/regress-312588.js
+non262/regress/regress-321971.js
+non262/regress/regress-360969-01.js
+non262/regress/regress-360969-02.js
+non262/regress/regress-360969-03.js
+non262/regress/regress-360969-04.js
+non262/regress/regress-360969-05.js
+non262/regress/regress-360969-06.js
+non262/extensions/regress-477187.js
+non262/regress/regress-452498-052-a.js
+non262/extensions/clone-complex-object.js
+non262/extensions/clone-object-deep.js
diff --git a/js/src/jsapi-tests/binast/parser/tester/split-001.js b/js/src/jsapi-tests/binast/parser/tester/split-001.js
--- a/js/src/jsapi-tests/binast/parser/tester/split-001.js
+++ b/js/src/jsapi-tests/binast/parser/tester/split-001.js
@@ -10,28 +10,28 @@
  *  Description:        Based on ECMA 2 Draft 7 February 1999
  *
  *  Author:             christine@netscape.com
  *  Date:               19 February 1999
  */
 
 /*
  * Since regular expressions have been part of JavaScript since 1.2, there
- * are already tests for regular expressions in the js1_2/regexp folder.
+ * are already tests for regular expressions in the non262/regexp folder.
  *
  * These new tests try to supplement the existing tests, and verify that
  * our implementation of RegExp conforms to the ECMA specification, but
  * does not try to be as exhaustive as in previous tests.
  *
  * The [,limit] argument to String.split is new, and not covered in any
  * existing tests.
  *
  * String.split cases are covered in ecma/String/15.5.4.8-*.js.
  * String.split where separator is a RegExp are in
- * js1_2/regexp/string_split.js
+ * non262/regexp/string_split.js
  *
  */
 
 var SECTION = "ecma_2/String/split-001.js";
 var VERSION = "ECMA_2";
 var TITLE   = "String.prototype.split( regexp, [,limit] )";
 
 startTest();
diff --git a/js/src/jsapi-tests/binast/parser/tester/split-002.js b/js/src/jsapi-tests/binast/parser/tester/split-002.js
--- a/js/src/jsapi-tests/binast/parser/tester/split-002.js
+++ b/js/src/jsapi-tests/binast/parser/tester/split-002.js
@@ -10,28 +10,28 @@
  *  Description:        Based on ECMA 2 Draft 7 February 1999
  *
  *  Author:             christine@netscape.com
  *  Date:               19 February 1999
  */
 
 /*
  * Since regular expressions have been part of JavaScript since 1.2, there
- * are already tests for regular expressions in the js1_2/regexp folder.
+ * are already tests for regular expressions in the non262/regexp folder.
  *
  * These new tests try to supplement the existing tests, and verify that
  * our implementation of RegExp conforms to the ECMA specification, but
  * does not try to be as exhaustive as in previous tests.
  *
  * The [,limit] argument to String.split is new, and not covered in any
  * existing tests.
  *
  * String.split cases are covered in ecma/String/15.5.4.8-*.js.
  * String.split where separator is a RegExp are in
- * js1_2/regexp/string_split.js
+ * non262/regexp/string_split.js
  *
  */
 
 var SECTION = "ecma_2/String/split-002.js";
 var VERSION = "ECMA_2";
 var TITLE   = "String.prototype.split( regexp, [,limit] )";
 
 startTest();
diff --git a/js/src/jsapi-tests/binast/parser/tester/split-003.js b/js/src/jsapi-tests/binast/parser/tester/split-003.js
--- a/js/src/jsapi-tests/binast/parser/tester/split-003.js
+++ b/js/src/jsapi-tests/binast/parser/tester/split-003.js
@@ -10,28 +10,28 @@
  *  Description:        Based on ECMA 2 Draft 7 February 1999
  *
  *  Author:             christine@netscape.com
  *  Date:               19 February 1999
  */
 
 /*
  * Since regular expressions have been part of JavaScript since 1.2, there
- * are already tests for regular expressions in the js1_2/regexp folder.
+ * are already tests for regular expressions in the non262/regexp folder.
  *
  * These new tests try to supplement the existing tests, and verify that
  * our implementation of RegExp conforms to the ECMA specification, but
  * does not try to be as exhaustive as in previous tests.
  *
  * The [,limit] argument to String.split is new, and not covered in any
  * existing tests.
  *
  * String.split cases are covered in ecma/String/15.5.4.8-*.js.
  * String.split where separator is a RegExp are in
- * js1_2/regexp/string_split.js
+ * non262/regexp/string_split.js
  *
  */
 
 var SECTION = "ecma_2/String/split-003.js";
 var VERSION = "ECMA_2";
 var TITLE   = "String.prototype.split( regexp, [,limit] )";
 
 startTest();
diff --git a/js/src/tests/non262/RegExp/octal-003.js b/js/src/tests/non262/RegExp/octal-003.js
--- a/js/src/tests/non262/RegExp/octal-003.js
+++ b/js/src/tests/non262/RegExp/octal-003.js
@@ -21,17 +21,17 @@
  *  This is incorrect: the string is a 4-character string consisting of
  *  the characters <'a'>, <nul>, <'1'>, <'1'>. By contrast, the \011 in the
  *  regexp should be parsed as a single token: it is the octal escape sequence
  *  for the horizontal tab character '\t' === '\u0009' === '\x09' === '\011'.
  *
  *  So the regexp consists of 2 characters: <any-character>, <'\t'>.
  *  There is no match between the regexp and the string.
  *
- *  See the testcase ecma_3/RegExp/octal-002.js for an elaboration.
+ *  See the testcase non262/RegExp/octal-002.js for an elaboration.
  *
  */
 var SECTION = "RegExp/octal-003.js";
 var TITLE   = "RegExp patterns that contain OctalEscapeSequences";
 var BUGNUMBER="http://scopus/bugsplat/show_bug.cgi?id=346132";
 
 printBugNumber(BUGNUMBER);
 
diff --git a/js/src/tests/non262/RegExp/regexp-space-character-class.js b/js/src/tests/non262/RegExp/regexp-space-character-class.js
--- a/js/src/tests/non262/RegExp/regexp-space-character-class.js
+++ b/js/src/tests/non262/RegExp/regexp-space-character-class.js
@@ -14,14 +14,14 @@ test();
 //-----------------------------------------------------------------------------
  
 function test()
 {
 printBugNumber(BUGNUMBER);
 printStatus (summary);
 
 // NOTE: White space and line terminators are now tested in
-// ecma_6/RegExp/character-class-escape-s.js.
+// non262/RegExp/character-class-escape-s.js.
 
 var non_space_chars = [ "\u200b", "\u200c", "\u200d" ];
 
 reportCompare(non_space_chars.some(function(ch){ return /\s/.test(ch); }), false, summary);
 }
diff --git a/js/src/tests/non262/class/extendBuiltinConstructors.js b/js/src/tests/non262/class/extendBuiltinConstructors.js
--- a/js/src/tests/non262/class/extendBuiltinConstructors.js
+++ b/js/src/tests/non262/class/extendBuiltinConstructors.js
@@ -95,16 +95,16 @@ testBuiltinTypedArrays();
 testBuiltin(DataView, new ArrayBuffer());
 testBuiltin(DataView, new (newGlobal().ArrayBuffer)());
 testBuiltin(String);
 testBuiltin(Array);
 testBuiltin(Array, 15);
 testBuiltin(Array, 3.0);
 testBuiltin(Array, "non-length one-arg");
 testBuiltin(Array, 5, 10, 15, "these are elements");
-// More Promise subclassing tests can be found in ecma_6/Promise/promise-subclassing.js
+// More Promise subclassing tests can be found in non262/Promise/promise-subclassing.js
 testBuiltin(Promise, _=>{});
 
 if (this.SharedArrayBuffer)
     testBuiltin(SharedArrayBuffer);
 
 if (typeof reportCompare === 'function')
     reportCompare(0,0,"OK");
diff --git a/js/src/tests/non262/extensions/browser.js b/js/src/tests/non262/extensions/browser.js
--- a/js/src/tests/non262/extensions/browser.js
+++ b/js/src/tests/non262/extensions/browser.js
@@ -1,6 +1,6 @@
 // The page loaded in the browser is jsreftest.html, which is located in
 // js/src/tests. That makes Worker script URLs resolve relative to the wrong
 // directory. workerDir is the workaround.
 workerDir = (document.location.href.replace(/\/[^/?]*(\?.*)?$/, '/') +
-             'js1_8_5/extensions/');
+             'non262/extensions/');
 
diff --git a/js/src/tests/non262/extensions/extension-methods-reject-null-undefined-this.js b/js/src/tests/non262/extensions/extension-methods-reject-null-undefined-this.js
--- a/js/src/tests/non262/extensions/extension-methods-reject-null-undefined-this.js
+++ b/js/src/tests/non262/extensions/extension-methods-reject-null-undefined-this.js
@@ -11,17 +11,17 @@ var summary =
 
 print(BUGNUMBER + ": " + summary);
 
 /**************
  * BEGIN TEST *
  **************/
 
 // This test fills out for the non-standard methods which
-// ecma_5/misc/builtin-methods-reject-null-undefined-this.js declines to test.
+// non262/misc/builtin-methods-reject-null-undefined-this.js declines to test.
 
 var ClassToMethodMap =
   {
     Object:   [/*
                 * Don't box this just yet for these methods -- they're used too
                 * much without qualification to do that.  :-(
                 */
                /* "__defineGetter__", "__defineSetter__", */
diff --git a/js/src/tests/non262/extensions/regress-226507.js b/js/src/tests/non262/extensions/regress-226507.js
--- a/js/src/tests/non262/extensions/regress-226507.js
+++ b/js/src/tests/non262/extensions/regress-226507.js
@@ -31,17 +31,17 @@
  * recursion in js_Emit takes 10 times more space the corresponding recursion
  * in the parser."
  *
  *
  * Note the use of the new -S option to the JS shell to limit stack size.
  * See http://bugzilla.mozilla.org/show_bug.cgi?id=225061. This in turn
  * can be passed to the JS shell by the test driver's -o option, as in:
  *
- * perl jsDriver.pl -e smdebug -fTEST.html -o "-S 100" -l js1_5/Regress
+ * perl jsDriver.pl -e smdebug -fTEST.html -o "-S 100" -l non262/Regress
  *
  */
 //-----------------------------------------------------------------------------
 var UBound = 0;
 var BUGNUMBER = 226507;
 var summary = 'Testing for recursion check in js_EmitTree';
 var status = '';
 var statusitems = [];
diff --git a/js/src/tests/non262/misc/builtin-methods-reject-null-undefined-this.js b/js/src/tests/non262/misc/builtin-methods-reject-null-undefined-this.js
--- a/js/src/tests/non262/misc/builtin-methods-reject-null-undefined-this.js
+++ b/js/src/tests/non262/misc/builtin-methods-reject-null-undefined-this.js
@@ -22,36 +22,36 @@ print(BUGNUMBER + ": " + summary);
 // do.  Why?  Ipse-dixitism.  *shrug*
 
 var ClassToMethodMap =
   {
     Object:  [/* "toString" has special |this| handling */
               "toLocaleString", "valueOf", "hasOwnProperty",
               /*
                * "isPrototypeOf" has special |this| handling already tested in
-               * ecma_5/Object/isPrototypeOf.js.
+               * non262/Object/isPrototypeOf.js.
                */
               /*
                * "isPrototypeOf" has special |this| handling already tested in
-               * ecma_5/Object/propertyIsEnumerable.js.
+               * non262/Object/propertyIsEnumerable.js.
                */],
     // Function methods often don't ToObject(this) as their very first step,
     // and they're already stepwise well-tested such that manual tests here
     // would be redundant.
     Array:   ["toString", "toLocaleString", "concat", "join", "pop", "push",
               "reverse", "shift", "slice", "sort", "splice", "unshift",
               "indexOf", "lastIndexOf", "every", "some", "forEach", "map",
               "filter", "reduce", "reduceRight"],
     String:  ["toString", "valueOf", "charAt", "charCodeAt", "concat",
               "indexOf", "lastIndexOf", "localeCompare", "match", "replace",
               "search", "slice", "split", "substring", "toLowerCase",
               "toLocaleLowerCase", "toUpperCase", "toLocaleUpperCase", "trim",
               /*
                * "trimLeft" and "trimRight" are non-standard and thus are tested
-               * in ecma_5/extensions/trim-extensions.js.
+               * in non262/extensions/trim-extensions.js.
                */
               ],
     Boolean: ["toString", "valueOf"],
     Number:  ["toString", "toLocaleString", "valueOf",
               /*
                * toFixed doesn't *immediately* test |this| for number or
                * Number-ness, but because the ToInteger(void 0) which arguably
                * precedes it in the toFixed algorithm won't throw in this test,
diff --git a/js/src/tests/non262/regress/regress-1383630.js b/js/src/tests/non262/regress/regress-1383630.js
--- a/js/src/tests/non262/regress/regress-1383630.js
+++ b/js/src/tests/non262/regress/regress-1383630.js
@@ -1,15 +1,15 @@
 /*
  * Any copyright is dedicated to the Public Domain.
  * http://creativecommons.org/licenses/publicdomain/
  */
 
 /* These tests are not checking whether an exception is thrown or not for
- * proxies:  those tests should already exist in js/src/tests/ecma_6/Proxy .
+ * proxies:  those tests should already exist in js/src/tests/non262/Proxy .
  * We expect TypeErrors to be thrown in these tests, with a stringification
  * of the error message showing whatever property name the error is being
  * reported for.
  *
  * Beyond the presence of the property name, these tests do not care about the
  * contents of the message.
  *
  * The reason for requiring the property name is simple:  with ECMAScript
