# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1523689377 -7200
# Node ID d38e3ca7457af2da96f3ca2390b1469894b3739b
# Parent  17dd09186aef4b6f5981862e7e247600d1cabaf2
Bug 1452982 part 7 - Rename ZoneGroupData to ZoneData. r=jonco

diff --git a/js/src/gc/ArenaList.h b/js/src/gc/ArenaList.h
--- a/js/src/gc/ArenaList.h
+++ b/js/src/gc/ArenaList.h
@@ -213,17 +213,17 @@ class ArenaLists {
    * For each arena kind its free list is represented as the first span with
    * free things. Initially all the spans are initialized as empty. After we
    * find a new arena with available things we move its first free span into
    * the list and set the arena as fully allocated. way we do not need to
    * update the arena after the initial allocation. When starting the
    * GC we only move the head of the of the list of spans back to the arena
    * only for the arena that was not fully allocated.
    */
-  ZoneGroupData<AllAllocKindArray<FreeSpan*>> freeLists_;
+  ZoneData<AllAllocKindArray<FreeSpan*>> freeLists_;
   AllAllocKindArray<FreeSpan*>& freeLists() { return freeLists_.ref(); }
   const AllAllocKindArray<FreeSpan*>& freeLists() const {
     return freeLists_.ref();
   }
 
   FreeSpan* freeList(AllocKind i) const { return freeLists()[i]; }
 
   inline void setFreeList(AllocKind i, FreeSpan* span);
@@ -264,24 +264,24 @@ class ArenaLists {
   }
 
   /* During incremental sweeping, a list of the arenas already swept. */
   ZoneGroupOrGCTaskData<AllocKind> incrementalSweptArenaKind;
   ZoneGroupOrGCTaskData<ArenaList> incrementalSweptArenas;
 
   // Arena lists which have yet to be swept, but need additional foreground
   // processing before they are swept.
-  ZoneGroupData<Arena*> gcShapeArenasToUpdate;
-  ZoneGroupData<Arena*> gcAccessorShapeArenasToUpdate;
-  ZoneGroupData<Arena*> gcScriptArenasToUpdate;
-  ZoneGroupData<Arena*> gcObjectGroupArenasToUpdate;
+  ZoneData<Arena*> gcShapeArenasToUpdate;
+  ZoneData<Arena*> gcAccessorShapeArenasToUpdate;
+  ZoneData<Arena*> gcScriptArenasToUpdate;
+  ZoneData<Arena*> gcObjectGroupArenasToUpdate;
 
   // The list of empty arenas which are collected during sweep phase and
   // released at the end of sweeping every sweep group.
-  ZoneGroupData<Arena*> savedEmptyArenas;
+  ZoneData<Arena*> savedEmptyArenas;
 
  public:
   explicit ArenaLists(JSRuntime* rt, JS::Zone* zone);
   ~ArenaLists();
 
   const void* addressOfFreeList(AllocKind thingKind) const {
     return reinterpret_cast<const void*>(&freeLists_.refNoCheck()[thingKind]);
   }
diff --git a/js/src/gc/Zone.h b/js/src/gc/Zone.h
--- a/js/src/gc/Zone.h
+++ b/js/src/gc/Zone.h
@@ -260,17 +260,17 @@ struct Zone : public JS::shadow::Zone,
   void sweepBreakpoints(js::FreeOp* fop);
   void sweepUniqueIds();
   void sweepWeakMaps();
   void sweepCompartments(js::FreeOp* fop, bool keepAtleastOne, bool lastGC);
 
   using DebuggerVector = js::Vector<js::Debugger*, 0, js::SystemAllocPolicy>;
 
  private:
-  js::ZoneGroupData<DebuggerVector*> debuggers;
+  js::ZoneData<DebuggerVector*> debuggers;
 
   js::jit::JitZone* createJitZone(JSContext* cx);
 
   bool isQueuedForBackgroundSweep() { return isOnList(); }
 
   // Side map for storing a unique ids for cells, independent of address.
   js::ZoneGroupOrGCTaskData<js::gc::UniqueIdMap> uniqueIds_;
 
@@ -289,17 +289,17 @@ struct Zone : public JS::shadow::Zone,
    * When true, skip calling the metadata callback. We use this:
    * - to avoid invoking the callback recursively;
    * - to avoid observing lazy prototype setup (which confuses callbacks that
    *   want to use the types being set up!);
    * - to avoid attaching allocation stacks to allocation stack nodes, which
    *   is silly
    * And so on.
    */
-  js::ZoneGroupData<bool> suppressAllocationMetadataBuilder;
+  js::ZoneData<bool> suppressAllocationMetadataBuilder;
 
   js::gc::ArenaLists arenas;
 
   js::TypeZone types;
 
  private:
   /* Live weakmaps in this zone. */
   js::ZoneGroupOrGCTaskData<mozilla::LinkedList<js::WeakMapBase>>
@@ -380,17 +380,17 @@ struct Zone : public JS::shadow::Zone,
   // There are no barriers here - the set contains only tenured objects so no
   // post-barrier is required, and these are weak references so no pre-barrier
   // is required.
   using TypeDescrObjectSet =
       js::GCHashSet<JSObject*, js::MovableCellHasher<JSObject*>,
                     js::SystemAllocPolicy>;
 
  private:
-  js::ZoneGroupData<JS::WeakCache<TypeDescrObjectSet>> typeDescrObjects_;
+  js::ZoneData<JS::WeakCache<TypeDescrObjectSet>> typeDescrObjects_;
 
   // Malloc counter to measure memory pressure for GC scheduling. This
   // counter should be used only when it's not possible to know the size of
   // a free.
   js::gc::MemoryCounter gcMallocCounter;
 
   // Counter of JIT code executable memory for GC scheduling. Also imprecise,
   // since wasm can generate code that outlives a zone.
@@ -491,63 +491,63 @@ struct Zone : public JS::shadow::Zone,
 
   // Thresholds used to trigger GC.
   js::gc::ZoneHeapThreshold threshold;
 
   // Amount of data to allocate before triggering a new incremental slice for
   // the current GC.
   js::UnprotectedData<size_t> gcDelayBytes;
 
-  js::ZoneGroupData<uint32_t> tenuredStrings;
-  js::ZoneGroupData<bool> allocNurseryStrings;
+  js::ZoneData<uint32_t> tenuredStrings;
+  js::ZoneData<bool> allocNurseryStrings;
 
  private:
   // Shared Shape property tree.
-  js::ZoneGroupData<js::PropertyTree> propertyTree_;
+  js::ZoneData<js::PropertyTree> propertyTree_;
 
  public:
   js::PropertyTree& propertyTree() { return propertyTree_.ref(); }
 
  private:
   // Set of all unowned base shapes in the Zone.
-  js::ZoneGroupData<js::BaseShapeSet> baseShapes_;
+  js::ZoneData<js::BaseShapeSet> baseShapes_;
 
  public:
   js::BaseShapeSet& baseShapes() { return baseShapes_.ref(); }
 
  private:
   // Set of initial shapes in the Zone. For certain prototypes -- namely,
   // those of various builtin classes -- there are two entries: one for a
   // lookup via TaggedProto, and one for a lookup via JSProtoKey. See
   // InitialShapeProto.
-  js::ZoneGroupData<js::InitialShapeSet> initialShapes_;
+  js::ZoneData<js::InitialShapeSet> initialShapes_;
 
  public:
   js::InitialShapeSet& initialShapes() { return initialShapes_.ref(); }
 
  private:
   // List of shapes that may contain nursery pointers.
   using NurseryShapeVector =
       js::Vector<js::AccessorShape*, 0, js::SystemAllocPolicy>;
-  js::ZoneGroupData<NurseryShapeVector> nurseryShapes_;
+  js::ZoneData<NurseryShapeVector> nurseryShapes_;
 
  public:
   NurseryShapeVector& nurseryShapes() { return nurseryShapes_.ref(); }
 
 #ifdef JSGC_HASH_TABLE_CHECKS
   void checkInitialShapesTableAfterMovingGC();
   void checkBaseShapeTableAfterMovingGC();
 #endif
   void fixupInitialShapeTable();
   void fixupAfterMovingGC();
 
   // Per-zone data for use by an embedder.
-  js::ZoneGroupData<void*> data;
+  js::ZoneData<void*> data;
 
-  js::ZoneGroupData<bool> isSystem;
+  js::ZoneData<bool> isSystem;
 
  private:
   // The helper thread context with exclusive access to this zone, if
   // usedByHelperThread(), or nullptr when on the main thread.
   js::UnprotectedData<JSContext*> helperThreadOwnerContext_;
 
  public:
   bool ownedByCurrentHelperThread();
@@ -577,17 +577,17 @@ struct Zone : public JS::shadow::Zone,
     helperThreadUse = HelperThreadUse::Active;
   }
   void clearUsedByHelperThread() {
     MOZ_ASSERT(helperThreadUse != HelperThreadUse::None);
     helperThreadUse = HelperThreadUse::None;
   }
 
 #ifdef DEBUG
-  js::ZoneGroupData<unsigned> gcLastSweepGroupIndex;
+  js::ZoneData<unsigned> gcLastSweepGroupIndex;
 #endif
 
   static js::HashNumber UniqueIdToHash(uint64_t uid) {
     return mozilla::HashGeneric(uid);
   }
 
   // Creates a HashNumber based on getUniqueId. Returns false on OOM.
   MOZ_MUST_USE bool getHashCode(js::gc::Cell* cell, js::HashNumber* hashp) {
@@ -719,22 +719,22 @@ struct Zone : public JS::shadow::Zone,
     p = static_cast<T*>(
         rt->onOutOfMemoryCanGC(js::AllocFunction::Calloc, bytes));
     if (!p) return nullptr;
     updateMallocCounter(bytes);
     return p;
   }
 
  private:
-  js::ZoneGroupData<js::jit::JitZone*> jitZone_;
+  js::ZoneData<js::jit::JitZone*> jitZone_;
 
   js::ActiveThreadData<bool> gcScheduled_;
   js::ActiveThreadData<bool> gcScheduledSaved_;
-  js::ZoneGroupData<bool> gcPreserveCode_;
-  js::ZoneGroupData<bool> keepShapeTables_;
+  js::ZoneData<bool> gcPreserveCode_;
+  js::ZoneData<bool> keepShapeTables_;
 
   // Allow zones to be linked into a list
   friend class js::gc::ZoneList;
   static Zone* const NotOnList;
   js::ActiveThreadOrGCTaskData<Zone*> listNext_;
   bool isOnList() const;
   Zone* nextZone() const;
 
diff --git a/js/src/threading/ProtectedData.cpp b/js/src/threading/ProtectedData.cpp
--- a/js/src/threading/ProtectedData.cpp
+++ b/js/src/threading/ProtectedData.cpp
@@ -32,17 +32,17 @@ static inline bool OnHelperThread() {
 
   return false;
 }
 
 void CheckThreadLocal::check() const {
   JSContext* cx = TlsContext.get();
   MOZ_ASSERT(cx);
 
-  // As for CheckZoneGroup, in a cooperatively scheduled runtime the active
+  // As for CheckZone, in a cooperatively scheduled runtime the active
   // thread is permitted access to thread local state for other suspended
   // threads in the same runtime.
   if (cx->isCooperativelyScheduled())
     MOZ_ASSERT(CurrentThreadCanAccessRuntime(cx->runtime()));
   else
     MOZ_ASSERT(id == ThisThread::GetId());
 }
 
@@ -54,37 +54,37 @@ void CheckActiveThread<Helper>::check() 
   MOZ_ASSERT(CurrentThreadCanAccessRuntime(cx->runtime()));
 }
 
 template class CheckActiveThread<AllowedHelperThread::None>;
 template class CheckActiveThread<AllowedHelperThread::GCTask>;
 template class CheckActiveThread<AllowedHelperThread::IonCompile>;
 
 template <AllowedHelperThread Helper>
-void CheckZoneGroup<Helper>::check() const {
+void CheckZone<Helper>::check() const {
   if (OnHelperThread<Helper>()) return;
 
   JSContext* cx = TlsContext.get();
   if (zone->isAtomsZone()) {
     // The atoms zone is protected by the exclusive access lock.
     MOZ_ASSERT(cx->runtime()->currentThreadHasExclusiveAccess());
   } else if (zone->usedByHelperThread()) {
     // This may only be accessed by the helper thread using this zone.
     MOZ_ASSERT(zone->ownedByCurrentHelperThread());
   } else {
     // The main thread is permitted access to all zones. These accesses
     // are threadsafe if the zone is not in use by a helper thread.
     MOZ_ASSERT(CurrentThreadCanAccessRuntime(cx->runtime()));
   }
 }
 
-template class CheckZoneGroup<AllowedHelperThread::None>;
-template class CheckZoneGroup<AllowedHelperThread::GCTask>;
-template class CheckZoneGroup<AllowedHelperThread::IonCompile>;
-template class CheckZoneGroup<AllowedHelperThread::GCTaskOrIonCompile>;
+template class CheckZone<AllowedHelperThread::None>;
+template class CheckZone<AllowedHelperThread::GCTask>;
+template class CheckZone<AllowedHelperThread::IonCompile>;
+template class CheckZone<AllowedHelperThread::GCTaskOrIonCompile>;
 
 template <GlobalLock Lock, AllowedHelperThread Helper>
 void CheckGlobalLock<Lock, Helper>::check() const {
   if (OnHelperThread<Helper>()) return;
 
   switch (Lock) {
     case GlobalLock::GCLock:
       MOZ_ASSERT(TlsContext.get()->runtime()->gc.currentThreadHasLockedGC());
diff --git a/js/src/threading/ProtectedData.h b/js/src/threading/ProtectedData.h
--- a/js/src/threading/ProtectedData.h
+++ b/js/src/threading/ProtectedData.h
@@ -230,48 +230,47 @@ template <typename T>
 using ActiveThreadOrGCTaskData =
     ProtectedDataNoCheckArgs<CheckActiveThread<AllowedHelperThread::GCTask>, T>;
 template <typename T>
 using ActiveThreadOrIonCompileData =
     ProtectedDataNoCheckArgs<CheckActiveThread<AllowedHelperThread::IonCompile>,
                              T>;
 
 template <AllowedHelperThread Helper>
-class CheckZoneGroup {
+class CheckZone {
 #ifdef JS_HAS_PROTECTED_DATA_CHECKS
   JS::Zone* zone;
 
  public:
-  explicit CheckZoneGroup(JS::Zone* zone) : zone(zone) {}
+  explicit CheckZone(JS::Zone* zone) : zone(zone) {}
   void check() const;
 #else
  public:
-  explicit CheckZoneGroup(JS::Zone* zone) {}
+  explicit CheckZone(JS::Zone* zone) {}
 #endif
 };
 
 // Data which may only be accessed by threads with exclusive access to the
 // associated zone group, or by the runtime's cooperatively scheduled
 // active thread for zone groups which are not in use by a helper thread.
 template <typename T>
-using ZoneGroupData =
-    ProtectedDataZoneGroupArg<CheckZoneGroup<AllowedHelperThread::None>, T>;
+using ZoneData =
+    ProtectedDataZoneGroupArg<CheckZone<AllowedHelperThread::None>, T>;
 
 // Data which may only be accessed by threads with exclusive access to the
 // associated zone group, or by various helper thread tasks.
 template <typename T>
 using ZoneGroupOrGCTaskData =
-    ProtectedDataZoneGroupArg<CheckZoneGroup<AllowedHelperThread::GCTask>, T>;
+    ProtectedDataZoneGroupArg<CheckZone<AllowedHelperThread::GCTask>, T>;
 template <typename T>
 using ZoneGroupOrIonCompileData =
-    ProtectedDataZoneGroupArg<CheckZoneGroup<AllowedHelperThread::IonCompile>,
-                              T>;
+    ProtectedDataZoneGroupArg<CheckZone<AllowedHelperThread::IonCompile>, T>;
 template <typename T>
 using ZoneGroupOrGCTaskOrIonCompileData = ProtectedDataZoneGroupArg<
-    CheckZoneGroup<AllowedHelperThread::GCTaskOrIonCompile>, T>;
+    CheckZone<AllowedHelperThread::GCTaskOrIonCompile>, T>;
 
 // Runtime wide locks which might protect some data.
 enum class GlobalLock {
   GCLock,
   ExclusiveAccessLock,
   ScriptDataLock,
   HelperThreadLock
 };
diff --git a/js/src/vm/TypeInference.h b/js/src/vm/TypeInference.h
--- a/js/src/vm/TypeInference.h
+++ b/js/src/vm/TypeInference.h
@@ -1268,42 +1268,42 @@ class HeapTypeSetKey {
 
 struct AutoEnterAnalysis;
 
 class TypeZone {
   JS::Zone* const zone_;
 
   /* Pool for type information in this zone. */
   static const size_t TYPE_LIFO_ALLOC_PRIMARY_CHUNK_SIZE = 8 * 1024;
-  ZoneGroupData<LifoAlloc> typeLifoAlloc_;
+  ZoneData<LifoAlloc> typeLifoAlloc_;
 
   // Under CodeGenerator::link, the id of the current compilation.
-  ZoneGroupData<mozilla::Maybe<IonCompilationId>> currentCompilationId_;
+  ZoneData<mozilla::Maybe<IonCompilationId>> currentCompilationId_;
 
   TypeZone(const TypeZone&) = delete;
   void operator=(const TypeZone&) = delete;
 
  public:
   // Current generation for sweeping.
   ZoneGroupOrGCTaskOrIonCompileData<uint32_t> generation;
 
   // During incremental sweeping, allocator holding the old type information
   // for the zone.
-  ZoneGroupData<LifoAlloc> sweepTypeLifoAlloc;
+  ZoneData<LifoAlloc> sweepTypeLifoAlloc;
 
   // During incremental sweeping, whether to try to destroy all type
   // information attached to scripts.
-  ZoneGroupData<bool> sweepReleaseTypes;
+  ZoneData<bool> sweepReleaseTypes;
 
-  ZoneGroupData<bool> sweepingTypes;
+  ZoneData<bool> sweepingTypes;
 
-  ZoneGroupData<bool> keepTypeScripts;
+  ZoneData<bool> keepTypeScripts;
 
   // The topmost AutoEnterAnalysis on the stack, if there is one.
-  ZoneGroupData<AutoEnterAnalysis*> activeAnalysis;
+  ZoneData<AutoEnterAnalysis*> activeAnalysis;
 
   explicit TypeZone(JS::Zone* zone);
   ~TypeZone();
 
   JS::Zone* zone() const { return zone_; }
 
   LifoAlloc& typeLifoAlloc() {
 #ifdef JS_CRASH_DIAGNOSTICS
