# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1528382704 -7200
# Node ID e3b458961e04bf00b4eff6aa08ec2ad6d6ee13e4
# Parent  6958c6b360d4fa41e90661c1a1ca970f5ea2cfaf
Bug 1466121 part 2 - Make Compartment and Zone classes instead of structs. r=luke

diff --git a/js/public/TypeDecls.h b/js/public/TypeDecls.h
--- a/js/public/TypeDecls.h
+++ b/js/public/TypeDecls.h
@@ -40,20 +40,20 @@ namespace JS {
 typedef unsigned char Latin1Char;
 
 class Symbol;
 #ifdef ENABLE_BIGINT
 class BigInt;
 #endif
 union Value;
 
-struct Compartment;
+class Compartment;
 class Realm;
 struct Runtime;
-struct Zone;
+class Zone;
 
 template <typename T>
 class Handle;
 template <typename T>
 class MutableHandle;
 template <typename T>
 class Rooted;
 template <typename T>
diff --git a/js/src/gc/Zone.h b/js/src/gc/Zone.h
--- a/js/src/gc/Zone.h
+++ b/js/src/gc/Zone.h
@@ -132,19 +132,20 @@ namespace JS {
 //   object.
 //
 // A zone remains alive as long as any GC things in the zone are alive. A
 // compartment remains alive as long as any JSObjects, scripts, shapes, or base
 // shapes within it are alive.
 //
 // We always guarantee that a zone has at least one live compartment by refusing
 // to delete the last compartment in a live zone.
-struct Zone : public JS::shadow::Zone,
-              public js::gc::GraphNodeBase<JS::Zone>,
-              public js::MallocProvider<JS::Zone> {
+class Zone : public JS::shadow::Zone,
+             public js::gc::GraphNodeBase<JS::Zone>,
+             public js::MallocProvider<JS::Zone> {
+ public:
   explicit Zone(JSRuntime* rt);
   ~Zone();
   MOZ_MUST_USE bool init(bool isSystem);
   void destroy(js::FreeOp* fop);
 
  private:
   enum class HelperThreadUse : uint32_t { None, Pending, Active };
   mozilla::Atomic<HelperThreadUse> helperThreadUse_;
diff --git a/js/src/threading/ProtectedData.h b/js/src/threading/ProtectedData.h
--- a/js/src/threading/ProtectedData.h
+++ b/js/src/threading/ProtectedData.h
@@ -4,17 +4,17 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef threading_ProtectedData_h
 #define threading_ProtectedData_h
 
 #include "threading/Thread.h"
 
-namespace JS { struct Zone; }
+namespace JS { class Zone; }
 
 namespace js {
 
 // This file provides classes for encapsulating pieces of data with a check
 // that ensures the data is only accessed if certain conditions are met.
 // Checking is only done in debug builds; in release builds these classes
 // have no space or time overhead. These classes are mainly used for ensuring
 // that data is used in threadsafe ways.
diff --git a/js/src/vm/JSCompartment.h b/js/src/vm/JSCompartment.h
--- a/js/src/vm/JSCompartment.h
+++ b/js/src/vm/JSCompartment.h
@@ -545,18 +545,17 @@ struct IteratorHashPolicy {
 } /* namespace js */
 
 namespace js {
 class DebugEnvironments;
 class ObjectWeakMap;
 class WeakMapBase;
 }  // namespace js
 
-struct JS::Compartment {
- private:
+class JS::Compartment {
   JS::Zone* zone_;
   JSRuntime* runtime_;
 
   js::WrapperMap crossCompartmentWrappers;
 
   using RealmVector = js::Vector<JS::Realm*, 1, js::SystemAllocPolicy>;
   RealmVector realms_;
 
diff --git a/js/src/vm/RegExpObject.h b/js/src/vm/RegExpObject.h
--- a/js/src/vm/RegExpObject.h
+++ b/js/src/vm/RegExpObject.h
@@ -16,20 +16,16 @@
 #include "gc/Marking.h"
 #include "js/GCHashTable.h"
 #include "proxy/Proxy.h"
 #include "vm/ArrayObject.h"
 #include "vm/JSContext.h"
 #include "vm/RegExpShared.h"
 #include "vm/Shape.h"
 
-namespace JS {
-struct Zone;
-}
-
 /*
  * JavaScript Regular Expressions
  *
  * There are several engine concepts associated with a single logical regexp:
  *
  *   RegExpObject:
  *     The JS-visible object whose .[[Class]] equals "RegExp".
  *   RegExpShared:
