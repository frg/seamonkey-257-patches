# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1526032892 -7200
# Node ID 14f7779dbc0d7785a639e57891ffa3a39cc01cbc
# Parent  4f6219b3fc0f7a51610a326f26632b19cca02080
Bug 1460341 - Give jsid a constructor that initializes it to a void id. r=jonco,bz

diff --git a/dom/bindings/DOMJSClass.h b/dom/bindings/DOMJSClass.h
--- a/dom/bindings/DOMJSClass.h
+++ b/dom/bindings/DOMJSClass.h
@@ -188,16 +188,20 @@ struct PropertyInfo {
   jsid id;
   // One of PropertyType, will be used for accessing the corresponding Duo in
   // NativePropertiesN.duos[].
   uint32_t type : NUM_BITS_PROPERTY_INFO_TYPE;
   // The index to the corresponding Preable in Duo.mPrefables[].
   uint32_t prefIndex : NUM_BITS_PROPERTY_INFO_PREF_INDEX;
   // The index to the corresponding spec in Duo.mPrefables[prefIndex].specs[].
   uint32_t specIndex : NUM_BITS_PROPERTY_INFO_SPEC_INDEX;
+
+  // Note: the default constructor is not constexpr because of the bit fields,
+  // so we need this one.
+  constexpr PropertyInfo() : id(), type(0), prefIndex(0), specIndex(0) {}
 };
 
 static_assert(
     ePropertyTypeCount <= 1ull << NUM_BITS_PROPERTY_INFO_TYPE,
     "We have property type count that is > (1 << NUM_BITS_PROPERTY_INFO_TYPE)");
 
 // Conceptually, NativeProperties has seven (Prefable<T>*, PropertyInfo*) duos
 // (where T is one of JSFunctionSpec, JSPropertySpec, or ConstantSpec), one for
diff --git a/js/public/Id.h b/js/public/Id.h
--- a/js/public/Id.h
+++ b/js/public/Id.h
@@ -22,29 +22,38 @@
 
 #include "jstypes.h"
 
 #include "js/HeapAPI.h"
 #include "js/RootingAPI.h"
 #include "js/TypeDecls.h"
 #include "js/Utility.h"
 
-struct jsid {
-  size_t asBits;
-  bool operator==(const jsid& rhs) const { return asBits == rhs.asBits; }
-  bool operator!=(const jsid& rhs) const { return asBits != rhs.asBits; }
-} JS_HAZ_GC_POINTER;
-#define JSID_BITS(id) (id.asBits)
-
 #define JSID_TYPE_STRING 0x0
 #define JSID_TYPE_INT 0x1
 #define JSID_TYPE_VOID 0x2
 #define JSID_TYPE_SYMBOL 0x4
 #define JSID_TYPE_MASK 0x7
 
+struct jsid {
+  size_t asBits;
+
+  constexpr jsid() : asBits(JSID_TYPE_VOID) {}
+
+  static constexpr jsid fromRawBits(size_t bits) {
+    jsid id;
+    id.asBits = bits;
+    return id;
+  }
+
+  bool operator==(const jsid& rhs) const { return asBits == rhs.asBits; }
+  bool operator!=(const jsid& rhs) const { return asBits != rhs.asBits; }
+} JS_HAZ_GC_POINTER;
+#define JSID_BITS(id) (id.asBits)
+
 // Avoid using canonical 'id' for jsid parameters since this is a magic word in
 // Objective-C++ which, apparently, wants to be able to #include jsapi.h.
 #define id iden
 
 static MOZ_ALWAYS_INLINE bool JSID_IS_STRING(jsid id) {
   return (JSID_BITS(id) & JSID_TYPE_MASK) == 0;
 }
 
@@ -120,17 +129,17 @@ static MOZ_ALWAYS_INLINE bool JSID_IS_VO
                 JSID_BITS(id) == JSID_TYPE_VOID);
   return (size_t)JSID_BITS(id) == JSID_TYPE_VOID;
 }
 
 static MOZ_ALWAYS_INLINE bool JSID_IS_EMPTY(const jsid id) {
   return (size_t)JSID_BITS(id) == JSID_TYPE_SYMBOL;
 }
 
-constexpr const jsid JSID_VOID = { size_t(JSID_TYPE_VOID) };
+constexpr const jsid JSID_VOID;
 extern JS_PUBLIC_DATA const jsid JSID_EMPTY;
 
 extern JS_PUBLIC_DATA const JS::HandleId JSID_VOIDHANDLE;
 extern JS_PUBLIC_DATA const JS::HandleId JSID_EMPTYHANDLE;
 
 namespace JS {
 
 template <>
diff --git a/js/src/jit/CacheIRCompiler.cpp b/js/src/jit/CacheIRCompiler.cpp
--- a/js/src/jit/CacheIRCompiler.cpp
+++ b/js/src/jit/CacheIRCompiler.cpp
@@ -883,17 +883,17 @@ void CacheIRWriter::copyStubData(uint8_t
         break;
       case StubField::Type::Symbol:
         InitGCPtr<JS::Symbol*>(destWords, field.asWord());
         break;
       case StubField::Type::String:
         InitGCPtr<JSString*>(destWords, field.asWord());
         break;
       case StubField::Type::Id:
-        InitGCPtr<jsid>(destWords, field.asWord());
+        AsGCPtr<jsid>(destWords)->init(jsid::fromRawBits(field.asWord()));
         break;
       case StubField::Type::RawInt64:
       case StubField::Type::DOMExpandoGeneration:
         *reinterpret_cast<uint64_t*>(destWords) = field.asInt64();
         break;
       case StubField::Type::Value:
         AsGCPtr<Value>(destWords)->init(
             Value::fromRawBits(uint64_t(field.asInt64())));
diff --git a/js/src/jit/IonCacheIRCompiler.cpp b/js/src/jit/IonCacheIRCompiler.cpp
--- a/js/src/jit/IonCacheIRCompiler.cpp
+++ b/js/src/jit/IonCacheIRCompiler.cpp
@@ -110,18 +110,17 @@ class MOZ_RAII IonCacheIRCompiler : publ
   }
   const Class* classStubField(uintptr_t offset) {
     return (const Class*)readStubWord(offset, StubField::Type::RawWord);
   }
   const void* proxyHandlerStubField(uintptr_t offset) {
     return (const void*)readStubWord(offset, StubField::Type::RawWord);
   }
   jsid idStubField(uint32_t offset) {
-    return mozilla::BitwiseCast<jsid>(
-        readStubWord(offset, StubField::Type::Id));
+    return jsid::fromRawBits(readStubWord(offset, StubField::Type::Id));
   }
   template <typename T>
   T rawWordStubField(uint32_t offset) {
     static_assert(sizeof(T) == sizeof(uintptr_t), "T must have word size");
     return (T)readStubWord(offset, StubField::Type::RawWord);
   }
   template <typename T>
   T rawInt64StubField(uint32_t offset) {
diff --git a/js/src/vm/Id.cpp b/js/src/vm/Id.cpp
--- a/js/src/vm/Id.cpp
+++ b/js/src/vm/Id.cpp
@@ -2,16 +2,16 @@
  * vim: set ts=8 sts=4 et sw=4 tw=99:
  * This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "js/Id.h"
 #include "js/RootingAPI.h"
 
-const jsid JSID_EMPTY = {size_t(JSID_TYPE_SYMBOL)};
+const jsid JSID_EMPTY = jsid::fromRawBits(size_t(JSID_TYPE_SYMBOL));
 
 static const jsid voidIdValue = JSID_VOID;
 static const jsid emptyIdValue = JSID_EMPTY;
 const JS::HandleId JSID_VOIDHANDLE =
     JS::HandleId::fromMarkedLocation(&voidIdValue);
 const JS::HandleId JSID_EMPTYHANDLE =
     JS::HandleId::fromMarkedLocation(&emptyIdValue);
