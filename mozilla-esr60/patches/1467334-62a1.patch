# HG changeset patch
# User Jeff Walden <jwalden@mit.edu>
# Date 1528362791 25200
# Node ID 75662fe4c9c525111f8a60d5de273aef22b9a65c
# Parent  834f24d7f4303c58b038c8fad5c46d16191f4e80
Bug 1467334 - Make TokenStreamAnyChars::isExprEnding a well-documented array of bool, zero-initialize it using a member initializer, then overwrite its few true elements in the constructor body.  r=arai

diff --git a/js/src/frontend/TokenStream.cpp b/js/src/frontend/TokenStream.cpp
--- a/js/src/frontend/TokenStream.cpp
+++ b/js/src/frontend/TokenStream.cpp
@@ -35,17 +35,16 @@
 #include "vm/JSAtom.h"
 #include "vm/JSContext.h"
 #include "vm/Realm.h"
 
 using mozilla::ArrayLength;
 using mozilla::IsAsciiAlpha;
 using mozilla::IsAsciiDigit;
 using mozilla::MakeScopeExit;
-using mozilla::PodArrayZero;
 using mozilla::PodCopy;
 
 struct ReservedWordInfo {
   const char* chars;  // C string with reserved word text
   js::frontend::TokenKind tokentype;
 };
 
 static const ReservedWordInfo reservedWords[] = {
@@ -390,28 +389,23 @@ TokenStreamAnyChars::TokenStreamAnyChars
       linebase(0),
       prevLinebase(size_t(-1)),
       filename_(options.filename()),
       displayURL_(nullptr),
       sourceMapURL_(nullptr),
       cx(cx),
       mutedErrors(options.mutedErrors()),
       strictModeGetter(smg) {
-  // Nb: the following tables could be static, but initializing them here is
-  // much easier.  Don't worry, the time to initialize them for each
-  // TokenStream is trivial.  See bug 639420.
-
-  // See Parser::assignExpr() for an explanation of isExprEnding[].
-  PodArrayZero(isExprEnding);
-  isExprEnding[size_t(TokenKind::Comma)] = 1;
-  isExprEnding[size_t(TokenKind::Semi)] = 1;
-  isExprEnding[size_t(TokenKind::Colon)] = 1;
-  isExprEnding[size_t(TokenKind::Rp)] = 1;
-  isExprEnding[size_t(TokenKind::Rb)] = 1;
-  isExprEnding[size_t(TokenKind::Rc)] = 1;
+  // |isExprEnding| was initially zeroed: overwrite the true entries here.
+  isExprEnding[size_t(TokenKind::Comma)] = true;
+  isExprEnding[size_t(TokenKind::Semi)] = true;
+  isExprEnding[size_t(TokenKind::Colon)] = true;
+  isExprEnding[size_t(TokenKind::Rp)] = true;
+  isExprEnding[size_t(TokenKind::Rb)] = true;
+  isExprEnding[size_t(TokenKind::Rc)] = true;
 }
 
 template <typename CharT>
 TokenStreamCharsBase<CharT>::TokenStreamCharsBase(JSContext* cx,
                                                   const CharT* chars,
                                                   size_t length,
                                                   size_t startOffset)
     : sourceUnits(chars, length, startOffset), tokenbuf(cx) {}
diff --git a/js/src/frontend/TokenStream.h b/js/src/frontend/TokenStream.h
--- a/js/src/frontend/TokenStream.h
+++ b/js/src/frontend/TokenStream.h
@@ -826,18 +826,40 @@ class TokenStreamAnyChars : public Token
   unsigned lineno;         // current line number
   TokenStreamFlags flags;  // flags -- see above
   size_t linebase;         // start of current line
   size_t
       prevLinebase;  // start of previous line;  size_t(-1) if on the first line
   const char* filename_;             // input filename or null
   UniqueTwoByteChars displayURL_;    // the user's requested source URL or null
   UniqueTwoByteChars sourceMapURL_;  // source map's filename or null
-  uint8_t isExprEnding[size_t(
-      TokenKind::Limit)];  // which tokens definitely terminate exprs?
+
+  /**
+   * An array storing whether a TokenKind observed while attempting to extend
+   * a valid AssignmentExpression into an even longer AssignmentExpression
+   * (e.g., extending '3' to '3 + 5') will terminate it without error.
+   *
+   * For example, ';' always ends an AssignmentExpression because it ends a
+   * Statement or declaration.  '}' always ends an AssignmentExpression
+   * because it terminates BlockStatement, FunctionBody, and embedded
+   * expressions in TemplateLiterals.  Therefore both entries are set to true
+   * in TokenStreamAnyChars construction.
+   *
+   * But e.g. '+' *could* extend an AssignmentExpression, so its entry here
+   * is false.  Meanwhile 'this' can't extend an AssignmentExpression, but
+   * it's only valid after a line break, so its entry here must be false.
+   *
+   * NOTE: This array could be static, but without C99's designated
+   *       initializers it's easier zeroing here and setting the true entries
+   *       in the constructor body.  (Having this per-instance might also aid
+   *       locality.)  Don't worry!  Initialization time for each TokenStream
+   *       is trivial.  See bug 639420.
+   */
+  bool isExprEnding[size_t(TokenKind::Limit)] = {}; // all-false initially
+
   JSContext* const cx;
   bool mutedErrors;
   StrictModeGetter* strictModeGetter;  // used to test for strict mode
 };
 
 // This is the low-level interface to the JS source code buffer.  It just gets
 // raw Unicode code units -- 16-bit char16_t units of source text that are not
 // (always) full code points, and 8-bit units of UTF-8 source text soon.
