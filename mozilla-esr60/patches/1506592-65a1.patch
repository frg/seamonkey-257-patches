# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1542106559 0
# Node ID 63fca07a71b8825782cbd1d0eb40eef4b45e12b4
# Parent  f7c62dde2f5570366c5ae6059297244145c9f33c
Bug 1506592 - Make sure to only display the broken image icon if there's a request at all. r=bzbarsky

This is enough to fix the devtools regression and matches what other browsers
do in the no-attribute case.

Also, I think this change over all makes sense: it doesn't make any sense to
display the broken image icon if there's no request, and we already assume in
EnsureIntrinsicSizeAndRatio() that we don't paint the icon for those (and make
the intrinsic size 0x0).

We still show the border, which matches other UAs (note that devtools
effectively masks the border away with mask-image).

This technically also can change behavior of <object> and <input>, but I think
it's better to be consistent, since EnsureIntrinsicSizeAndRatio also doesn't
special-case <img> either.

Differential Revision: https://phabricator.services.mozilla.com/D11659

diff --git a/dom/base/nsIImageLoadingContent.idl b/dom/base/nsIImageLoadingContent.idl
--- a/dom/base/nsIImageLoadingContent.idl
+++ b/dom/base/nsIImageLoadingContent.idl
@@ -66,17 +66,17 @@ interface nsIImageLoadingContent : imgIN
   [notxpcom, nostdcall] void setLoadingEnabled(in boolean aEnabled);
 
   /**
    * Returns the image blocking status (@see nsIContentPolicy).  This
    * will always be an nsIContentPolicy REJECT_* status for cases when
    * the image was blocked.  This status always refers to the
    * CURRENT_REQUEST load.
    */
-  [noscript] readonly attribute short imageBlockingStatus;
+  [noscript, infallible] readonly attribute short imageBlockingStatus;
 
   /**
    * Used to register an image decoder observer.  Typically, this will
    * be a proxy for a frame that wants to paint the image.
    * Notifications from ongoing image loads will be passed to all
    * registered observers.  Notifications for all request types,
    * current and pending, will be passed through.
    *
diff --git a/layout/generic/nsImageFrame.cpp b/layout/generic/nsImageFrame.cpp
--- a/layout/generic/nsImageFrame.cpp
+++ b/layout/generic/nsImageFrame.cpp
@@ -120,16 +120,48 @@ static bool HaveFixedSize(const ReflowIn
                "crappy reflowInput - null stylePosition");
   // Don't try to make this optimization when an image has percentages
   // in its 'width' or 'height'.  The percentages might be treated like
   // auto (especially for intrinsic width calculations and for heights).
   return aReflowInput.mStylePosition->mHeight.ConvertsToLength() &&
          aReflowInput.mStylePosition->mWidth.ConvertsToLength();
 }
 
+bool nsImageFrame::ShouldShowBrokenImageIcon() const {
+  // NOTE(emilio, https://github.com/w3c/csswg-drafts/issues/2832): WebKit and
+  // Blink behave differently here for content: url(..), for now adapt to
+  // Blink's behavior.
+  // if (mKind != Kind::ImageElement) {
+  //   return false;
+  // }
+
+  nsCOMPtr<nsIImageLoadingContent> imageLoader = do_QueryInterface(mContent);
+  
+  // MOZ_ASSERT(imageLoader);
+  if (!imageLoader) {
+    return true;
+  }
+
+  // check for broken images. valid null images (eg. img src="") are
+  // not considered broken because they have no image requests
+  nsCOMPtr<imgIRequest> currentRequest;
+  imageLoader->GetRequest(nsIImageLoadingContent::CURRENT_REQUEST,
+                          getter_AddRefs(currentRequest));
+
+  if (currentRequest) {
+    uint32_t imageStatus;
+    return NS_SUCCEEDED(currentRequest->GetImageStatus(&imageStatus)) &&
+           (imageStatus & imgIRequest::STATUS_ERROR);
+  }
+
+  int16_t imageBlockingStatus;
+  imageLoader->GetImageBlockingStatus(&imageBlockingStatus);
+  return imageBlockingStatus != nsIContentPolicy::ACCEPT;
+}
+
 nsIFrame* NS_NewImageFrame(nsIPresShell* aPresShell, nsStyleContext* aContext) {
   return new (aPresShell) nsImageFrame(aContext);
 }
 
 NS_IMPL_FRAMEARENA_HELPERS(nsImageFrame)
 
 nsImageFrame::nsImageFrame(nsStyleContext* aContext, ClassID aID)
     : nsAtomicContainerFrame(aContext, aID),
@@ -745,45 +777,24 @@ void nsImageFrame::EnsureIntrinsicSizeAn
   // image container.
   if (mIntrinsicSize.width.GetUnit() == eStyleUnit_Coord &&
       mIntrinsicSize.width.GetCoordValue() == 0 &&
       mIntrinsicSize.height.GetUnit() == eStyleUnit_Coord &&
       mIntrinsicSize.height.GetCoordValue() == 0) {
     if (mImage) {
       UpdateIntrinsicSize(mImage);
       UpdateIntrinsicRatio(mImage);
+      return;
     } else {
       // image request is null or image size not known, probably an
       // invalid image specified
       if (!(GetStateBits() & NS_FRAME_GENERATED_CONTENT)) {
-        bool imageInvalid = false;
-        // check for broken images. valid null images (eg. img src="") are
-        // not considered broken because they have no image requests
-        nsCOMPtr<nsIImageLoadingContent> imageLoader =
-            do_QueryInterface(mContent);
-        if (imageLoader) {
-          nsCOMPtr<imgIRequest> currentRequest;
-          imageLoader->GetRequest(nsIImageLoadingContent::CURRENT_REQUEST,
-                                  getter_AddRefs(currentRequest));
-          if (currentRequest) {
-            uint32_t imageStatus;
-            imageInvalid =
-                NS_SUCCEEDED(currentRequest->GetImageStatus(&imageStatus)) &&
-                (imageStatus & imgIRequest::STATUS_ERROR);
-          } else {
-            // check if images are user-disabled (or blocked for other
-            // reasons)
-            int16_t imageBlockingStatus;
-            imageLoader->GetImageBlockingStatus(&imageBlockingStatus);
-            imageInvalid = imageBlockingStatus != nsIContentPolicy::ACCEPT;
-          }
-        }
         // invalid image specified. make the image big enough for the "broken"
         // icon
-        if (imageInvalid) {
+        if (ShouldShowBrokenImageIcon()) {
           nscoord edgeLengthToUse = nsPresContext::CSSPixelsToAppUnits(
               ICON_SIZE + (2 * (ICON_PADDING + ALT_BORDER_WIDTH)));
           mIntrinsicSize.width.SetCoordValue(edgeLengthToUse);
           mIntrinsicSize.height.SetCoordValue(edgeLengthToUse);
           mIntrinsicRatio.SizeTo(1, 1);
         }
       }
     }
@@ -1319,17 +1330,18 @@ ImgDrawResult nsImageFrame::DisplayAltFe
   // Clip so we don't render outside the inner rect
   aRenderingContext.Save();
   aRenderingContext.Clip(NSRectToSnappedRect(
       inner, PresContext()->AppUnitsPerDevPixel(), *drawTarget));
 
   ImgDrawResult result = ImgDrawResult::NOT_READY;
 
   // Check if we should display image placeholders
-  if (!gIconLoad->mPrefShowPlaceholders ||
+  if (!ShouldShowBrokenImageIcon() ||
+      !gIconLoad->mPrefShowPlaceholders ||
       (isLoading && !gIconLoad->mPrefShowLoadingPlaceholder)) {
     result = ImgDrawResult::SUCCESS;
   } else {
     nscoord size = nsPresContext::CSSPixelsToAppUnits(ICON_SIZE);
 
     imgIRequest* request = isLoading ? nsImageFrame::gIconLoad->mLoadingImage
                                      : nsImageFrame::gIconLoad->mBrokenImage;
 
diff --git a/layout/generic/nsImageFrame.h b/layout/generic/nsImageFrame.h
--- a/layout/generic/nsImageFrame.h
+++ b/layout/generic/nsImageFrame.h
@@ -98,16 +98,18 @@ class nsImageFrame : public nsAtomicCont
                              nsIFrame::Cursor& aCursor) override;
   virtual nsresult AttributeChanged(int32_t aNameSpaceID, nsAtom* aAttribute,
                                     int32_t aModType) override;
 
   void OnVisibilityChange(
       Visibility aNewVisibility,
       const Maybe<OnNonvisible>& aNonvisibleAction = Nothing()) override;
 
+  bool ShouldShowBrokenImageIcon() const;
+
 #ifdef ACCESSIBILITY
   virtual mozilla::a11y::AccType AccessibleType() override;
 #endif
 
   virtual bool IsFrameOfType(uint32_t aFlags) const override {
     return nsAtomicContainerFrame::IsFrameOfType(
         aFlags & ~(nsIFrame::eReplaced | nsIFrame::eReplacedSizing));
   }
diff --git a/layout/reftests/image-element/broken-icon.html b/layout/reftests/image-element/broken-icon.html
new file mode 100644
--- /dev/null
+++ b/layout/reftests/image-element/broken-icon.html
@@ -0,0 +1,9 @@
+<!doctype html>
+<style>
+  img {
+    display: inline-block;
+    width: 100px;
+    height: 100px;
+  }
+</style>
+<img src="broken">
diff --git a/layout/reftests/image-element/empty-src.html b/layout/reftests/image-element/empty-src.html
new file mode 100644
--- /dev/null
+++ b/layout/reftests/image-element/empty-src.html
@@ -0,0 +1,9 @@
+<!doctype html>
+<style>
+  img {
+    display: inline-block;
+    width: 100px;
+    height: 100px;
+  }
+</style>
+<img src>
diff --git a/layout/reftests/image-element/invalid-src-2.html b/layout/reftests/image-element/invalid-src-2.html
new file mode 100644
--- /dev/null
+++ b/layout/reftests/image-element/invalid-src-2.html
@@ -0,0 +1,9 @@
+<!doctype html>
+<style>
+  img {
+    display: inline-block;
+    width: 100px;
+    height: 100px;
+  }
+</style>
+<img src="http://yada yada">
diff --git a/layout/reftests/image-element/invalid-src.html b/layout/reftests/image-element/invalid-src.html
new file mode 100644
--- /dev/null
+++ b/layout/reftests/image-element/invalid-src.html
@@ -0,0 +1,9 @@
+<!doctype html>
+<style>
+  img {
+    display: inline-block;
+    width: 100px;
+    height: 100px;
+  }
+</style>
+<img src="yada yada">
diff --git a/layout/reftests/image-element/no-src.html b/layout/reftests/image-element/no-src.html
new file mode 100644
--- /dev/null
+++ b/layout/reftests/image-element/no-src.html
@@ -0,0 +1,9 @@
+<!doctype html>
+<style>
+  img {
+    display: inline-block;
+    width: 100px;
+    height: 100px;
+  }
+</style>
+<img>
diff --git a/layout/reftests/image-element/reftest.list b/layout/reftests/image-element/reftest.list
--- a/layout/reftests/image-element/reftest.list
+++ b/layout/reftests/image-element/reftest.list
@@ -40,10 +40,15 @@ fuzzy(1,9674) random-if(!cocoaWidget) ==
 == gradient-html-06c.html gradient-html-06d.html
 == gradient-html-06d.html gradient-html-06e.html
 random-if(!cocoaWidget) fuzzy-if(cocoaWidget,2,42305) == gradient-html-07a.html gradient-html-07b.html
 fuzzy(1,16900) == gradient-html-07c.html gradient-html-07d.html
 HTTP == invalidate-1.html invalidate-1-ref.html
 == pattern-html-01.html pattern-html-01-ref.svg
 == pattern-html-02.html pattern-html-02-ref.svg
 == referenced-from-binding-01.html referenced-from-binding-01-ref.html
+!= broken-icon.html no-src.html
+!= broken-icon.html empty-src.html
+== empty-src.html no-src.html
+== broken-icon.html invalid-src.html
+fails == invalid-src.html invalid-src-2.html # bug 1506804
 
 fuzzy-if(skiaContent,1,30000) == mask-image-element.html mask-image-element-ref.html
