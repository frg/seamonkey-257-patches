# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1527933507 -7200
# Node ID 4975928725165a5d5918085f951a4ef2c37f1ad0
# Parent  647e48ebc9d3a19ad00f579a55c09aa9effe6c18
Bug 1466083 part 1 - Make IterateScripts take a realm instead of a compartment. r=luke

diff --git a/js/src/gc/GC.h b/js/src/gc/GC.h
--- a/js/src/gc/GC.h
+++ b/js/src/gc/GC.h
@@ -105,21 +105,21 @@ extern void IterateHeapUnbarrieredForZon
 extern void IterateChunks(JSContext* cx, void* data,
                           IterateChunkCallback chunkCallback);
 
 typedef void (*IterateScriptCallback)(JSRuntime* rt, void* data,
                                       JSScript* script,
                                       const JS::AutoRequireNoGC& nogc);
 
 /*
- * Invoke scriptCallback on every in-use script for
- * the given compartment or for all compartments if it is null.
+ * Invoke scriptCallback on every in-use script for the given realm or for all
+ * realms if it is null.
  */
-extern void IterateScripts(JSContext* cx, JSCompartment* compartment,
-                           void* data, IterateScriptCallback scriptCallback);
+extern void IterateScripts(JSContext* cx, JS::Realm* realm, void* data,
+                           IterateScriptCallback scriptCallback);
 
 JS::Realm* NewRealm(JSContext* cx, JSPrincipals* principals,
                     const JS::RealmOptions& options);
 
 namespace gc {
 
 void FinishGC(JSContext* cx);
 
diff --git a/js/src/gc/PublicIterators.cpp b/js/src/gc/PublicIterators.cpp
--- a/js/src/gc/PublicIterators.cpp
+++ b/js/src/gc/PublicIterators.cpp
@@ -72,28 +72,28 @@ void js::IterateChunks(JSContext* cx, vo
   AutoPrepareForTracing prep(cx);
   AutoLockGC lock(cx->runtime());
 
   for (auto chunk = cx->runtime()->gc.allNonEmptyChunks(lock); !chunk.done();
        chunk.next())
     chunkCallback(cx->runtime(), data, chunk);
 }
 
-void js::IterateScripts(JSContext* cx, JSCompartment* compartment, void* data,
+void js::IterateScripts(JSContext* cx, Realm* realm, void* data,
                         IterateScriptCallback scriptCallback) {
   MOZ_ASSERT(!cx->suppressGC);
   AutoEmptyNursery empty(cx);
   AutoPrepareForTracing prep(cx);
   JS::AutoSuppressGCAnalysis nogc;
 
-  if (compartment) {
-    Zone* zone = compartment->zone();
+  if (realm) {
+    Zone* zone = realm->zone();
     for (auto script = zone->cellIter<JSScript>(empty); !script.done();
          script.next()) {
-      if (script->compartment() == compartment)
+      if (script->realm() == realm)
         scriptCallback(cx->runtime(), data, script, nogc);
     }
   } else {
     for (ZonesIter zone(cx->runtime(), SkipAtoms); !zone.done(); zone.next()) {
       for (auto script = zone->cellIter<JSScript>(empty); !script.done();
            script.next())
         scriptCallback(cx->runtime(), data, script, nogc);
     }
diff --git a/js/src/jsapi-tests/testPreserveJitCode.cpp b/js/src/jsapi-tests/testPreserveJitCode.cpp
--- a/js/src/jsapi-tests/testPreserveJitCode.cpp
+++ b/js/src/jsapi-tests/testPreserveJitCode.cpp
@@ -18,17 +18,17 @@ static void ScriptCallback(JSRuntime* rt
 BEGIN_TEST(test_PreserveJitCode) {
   CHECK(testPreserveJitCode(false, 0));
   CHECK(testPreserveJitCode(true, 1));
   return true;
 }
 
 unsigned countIonScripts(JSObject* global) {
   unsigned count = 0;
-  js::IterateScripts(cx, global->compartment(), &count, ScriptCallback);
+  js::IterateScripts(cx, global->realm(), &count, ScriptCallback);
   return count;
 }
 
 bool testPreserveJitCode(bool preserveJitCode, unsigned remainingIonScripts) {
   cx->options().setBaseline(true);
   cx->options().setIon(true);
   cx->runtime()->setOffthreadIonCompilationEnabled(false);
 
diff --git a/js/src/vm/Debugger.cpp b/js/src/vm/Debugger.cpp
--- a/js/src/vm/Debugger.cpp
+++ b/js/src/vm/Debugger.cpp
@@ -3887,31 +3887,31 @@ static inline DebuggerSourceReferent Get
  */
 class MOZ_STACK_CLASS Debugger::ScriptQuery {
  public:
   /* Construct a ScriptQuery to use matching scripts for |dbg|. */
   ScriptQuery(JSContext* cx, Debugger* dbg)
       : cx(cx),
         debugger(dbg),
         iterMarker(&cx->runtime()->gc),
-        compartments(cx->zone()),
+        realms(cx->zone()),
         url(cx),
         displayURLString(cx),
         hasSource(false),
         source(cx, AsVariant(static_cast<ScriptSourceObject*>(nullptr))),
-        innermostForCompartment(cx->zone()),
+        innermostForRealm(cx->zone()),
         vector(cx, ScriptVector(cx)),
         wasmInstanceVector(cx, WasmInstanceObjectVector(cx)) {}
 
   /*
    * Initialize this ScriptQuery. Raise an error and return false if we
    * haven't enough memory.
    */
   bool init() {
-    if (!compartments.init() || !innermostForCompartment.init()) {
+    if (!realms.init() || !innermostForRealm.init()) {
       ReportOutOfMemory(cx);
       return false;
     }
 
     return true;
   }
 
   /*
@@ -4063,46 +4063,47 @@ class MOZ_STACK_CLASS Debugger::ScriptQu
     url.setUndefined();
     hasLine = false;
     innermost = false;
     displayURLString = nullptr;
     return matchAllDebuggeeGlobals();
   }
 
   /*
-   * Search all relevant compartments and the stack for scripts matching
+   * Search all relevant realms and the stack for scripts matching
    * this query, and append the matching scripts to |vector|.
    */
   bool findScripts() {
     if (!prepareQuery() || !delazifyScripts()) return false;
 
-    JSCompartment* singletonComp = nullptr;
-    if (compartments.count() == 1) singletonComp = compartments.all().front();
-
-    /* Search each compartment for debuggee scripts. */
+    Realm* singletonRealm = nullptr;
+    if (realms.count() == 1)
+      singletonRealm = realms.all().front();
+
+    /* Search each realm for debuggee scripts. */
     MOZ_ASSERT(vector.empty());
     oom = false;
-    IterateScripts(cx, singletonComp, this, considerScript);
+    IterateScripts(cx, singletonRealm, this, considerScript);
     if (oom) {
       ReportOutOfMemory(cx);
       return false;
     }
 
     /* We cannot touch the gray bits while isHeapBusy, so do this now. */
     for (JSScript** i = vector.begin(); i != vector.end(); ++i)
       JS::ExposeScriptToActiveJS(*i);
 
     /*
      * For most queries, we just accumulate results in 'vector' as we find
      * them. But if this is an 'innermost' query, then we've accumulated the
-     * results in the 'innermostForCompartment' map. In that case, we now need
-     * to walk that map and populate 'vector'.
+     * results in the 'innermostForRealm' map. In that case, we now need to
+     * walk that map and populate 'vector'.
      */
     if (innermost) {
-      for (CompartmentToScriptMap::Range r = innermostForCompartment.all();
+      for (RealmToScriptMap::Range r = innermostForRealm.all();
            !r.empty(); r.popFront()) {
         JS::ExposeScriptToActiveJS(r.front().value());
         if (!vector.append(r.front().value())) {
           ReportOutOfMemory(cx);
           return false;
         }
       }
     }
@@ -4131,26 +4132,23 @@ class MOZ_STACK_CLASS Debugger::ScriptQu
 
  private:
   /* The context in which we should do our work. */
   JSContext* cx;
 
   /* The debugger for which we conduct queries. */
   Debugger* debugger;
 
-  /* Require the set of compartments to stay fixed while the ScriptQuery is
-   * alive. */
+  /* Require the set of realms to stay fixed while the ScriptQuery is alive. */
   gc::AutoEnterIteration iterMarker;
 
-  typedef HashSet<JSCompartment*, DefaultHasher<JSCompartment*>,
-                  ZoneAllocPolicy>
-      CompartmentSet;
-
-  /* A script must be in one of these compartments to match the query. */
-  CompartmentSet compartments;
+  using RealmSet = HashSet<Realm*, DefaultHasher<Realm*>, ZoneAllocPolicy>;
+
+  /* A script must be in one of these realms to match the query. */
+  RealmSet realms;
 
   /* If this is a string, matching scripts have urls equal to it. */
   RootedValue url;
 
   /* url as a C string. */
   JSAutoByteString urlCString;
 
   /* If this is a string, matching scripts' sources have displayURLs equal to
@@ -4169,64 +4167,63 @@ class MOZ_STACK_CLASS Debugger::ScriptQu
   bool hasLine;
 
   /* The line matching scripts must cover. */
   unsigned int line;
 
   /* True if the query has an 'innermost' property whose value is true. */
   bool innermost;
 
-  typedef HashMap<JSCompartment*, JSScript*, DefaultHasher<JSCompartment*>,
-                  ZoneAllocPolicy>
-      CompartmentToScriptMap;
+  using RealmToScriptMap =
+      HashMap<Realm*, JSScript*, DefaultHasher<Realm*>, ZoneAllocPolicy>;
 
   /*
-   * For 'innermost' queries, a map from compartments to the innermost script
-   * we've seen so far in that compartment. (Template instantiation code size
+   * For 'innermost' queries, a map from realms to the innermost script
+   * we've seen so far in that realm. (Template instantiation code size
    * explosion ho!)
    */
-  CompartmentToScriptMap innermostForCompartment;
+  RealmToScriptMap innermostForRealm;
 
   /*
    * Accumulate the scripts in an Rooted<ScriptVector>, instead of creating
    * the JS array as we go, because we mustn't allocate JS objects or GC
    * while we use the CellIter.
    */
   Rooted<ScriptVector> vector;
 
   /*
    * Like above, but for wasm modules.
    */
   Rooted<WasmInstanceObjectVector> wasmInstanceVector;
 
   /* Indicates whether OOM has occurred while matching. */
   bool oom;
 
-  bool addCompartment(JSCompartment* comp) { return compartments.put(comp); }
+  bool addRealm(Realm* realm) { return realms.put(realm); }
 
   /* Arrange for this ScriptQuery to match only scripts that run in |global|. */
   bool matchSingleGlobal(GlobalObject* global) {
-    MOZ_ASSERT(compartments.count() == 0);
-    if (!addCompartment(global->compartment())) {
+    MOZ_ASSERT(realms.count() == 0);
+    if (!addRealm(global->realm())) {
       ReportOutOfMemory(cx);
       return false;
     }
     return true;
   }
 
   /*
    * Arrange for this ScriptQuery to match all scripts running in debuggee
    * globals.
    */
   bool matchAllDebuggeeGlobals() {
-    MOZ_ASSERT(compartments.count() == 0);
-    /* Build our compartment set from the debugger's set of debuggee globals. */
+    MOZ_ASSERT(realms.count() == 0);
+    /* Build our realm set from the debugger's set of debuggee globals. */
     for (WeakGlobalObjectSet::Range r = debugger->debuggees.all(); !r.empty();
          r.popFront()) {
-      if (!addCompartment(r.front()->compartment())) {
+      if (!addRealm(r.front()->realm())) {
         ReportOutOfMemory(cx);
         return false;
       }
     }
     return true;
   }
 
   /*
@@ -4239,45 +4236,45 @@ class MOZ_STACK_CLASS Debugger::ScriptQu
     if (url.isString()) {
       if (!urlCString.encodeLatin1(cx, url.toString())) return false;
     }
 
     return true;
   }
 
   bool delazifyScripts() {
-    // All scripts in debuggee compartments must be visible, so delazify
+    // All scripts in debuggee realms must be visible, so delazify
     // everything.
-    for (auto r = compartments.all(); !r.empty(); r.popFront()) {
-      JSCompartment* comp = r.front();
-      Realm* realm = JS::GetRealmForCompartment(comp);
+    for (auto r = realms.all(); !r.empty(); r.popFront()) {
+      Realm* realm = r.front();
       if (!realm->ensureDelazifyScriptsForDebugger(cx))
         return false;
     }
     return true;
   }
 
   static void considerScript(JSRuntime* rt, void* data, JSScript* script,
                              const JS::AutoRequireNoGC& nogc) {
     ScriptQuery* self = static_cast<ScriptQuery*>(data);
     self->consider(script, nogc);
   }
 
   /*
    * If |script| matches this query, append it to |vector| or place it in
-   * |innermostForCompartment|, as appropriate. Set |oom| if an out of memory
+   * |innermostForRealm|, as appropriate. Set |oom| if an out of memory
    * condition occurred.
    */
   void consider(JSScript* script, const JS::AutoRequireNoGC& nogc) {
     // We check for presence of script->code() because it is possible that
     // the script was created and thus exposed to GC, but *not* fully
     // initialized from fullyInit{FromEmitter,Trivial} due to errors.
     if (oom || script->selfHosted() || !script->code()) return;
-    JSCompartment* compartment = script->compartment();
-    if (!compartments.has(compartment)) return;
+    Realm* realm = script->realm();
+    if (!realms.has(realm))
+      return;
     if (urlCString.ptr()) {
       bool gotFilename = false;
       if (script->filename() &&
           strcmp(script->filename(), urlCString.ptr()) == 0)
         gotFilename = true;
 
       bool gotSourceURL = false;
       if (!gotFilename && script->scriptSource()->introducerFilename() &&
@@ -4305,39 +4302,37 @@ class MOZ_STACK_CLASS Debugger::ScriptQu
       return;
     }
 
     if (innermost) {
       /*
        * For 'innermost' queries, we don't place scripts in |vector| right
        * away; we may later find another script that is nested inside this
        * one. Instead, we record the innermost script we've found so far
-       * for each compartment in innermostForCompartment, and only
-       * populate |vector| at the bottom of findScripts, when we've
-       * traversed all the scripts.
+       * for each realm in innermostForRealm, and only populate |vector|
+       * at the bottom of findScripts, when we've traversed all the scripts.
        *
        * So: check this script against the innermost one we've found so
-       * far (if any), as recorded in innermostForCompartment, and replace
-       * that if it's better.
+       * far (if any), as recorded in innermostForRealm, and replace that
+       * if it's better.
        */
-      CompartmentToScriptMap::AddPtr p =
-          innermostForCompartment.lookupForAdd(compartment);
+      RealmToScriptMap::AddPtr p = innermostForRealm.lookupForAdd(realm);
       if (p) {
         /* Is our newly found script deeper than the last one we found? */
         JSScript* incumbent = p->value();
         if (script->innermostScope()->chainLength() >
             incumbent->innermostScope()->chainLength()) {
           p->value() = script;
         }
       } else {
         /*
          * This is the first matching script we've encountered for this
-         * compartment, so it is thus the innermost such script.
+         * realm, so it is thus the innermost such script.
          */
-        if (!innermostForCompartment.add(p, compartment, script)) {
+        if (!innermostForRealm.add(p, realm, script)) {
           oom = true;
           return;
         }
       }
     } else {
       /* Record this matching script in the results vector. */
       if (!vector.append(script)) {
         oom = true;
