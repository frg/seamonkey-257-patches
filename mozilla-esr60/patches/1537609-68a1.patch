# HG changeset patch
# User Jason Orendorff <jorendorff@mozilla.com>
# Date 1553699520 0
# Node ID 79f99b532456a7d5b31537cd9d7db92cfc3654d7
# Parent  354e432931cf6c1312a25f66f711f1be7b91841a
Bug 1537609 - Cap the stack size at 2MB on Windows. r=jandem

In bug 256180, the size of the stack on 64-bit Windows was changed from 2MB to
8MB, and on 32-bit Windows, from 1MB to 1.5MB. This is so large that it takes
significantly longer for a runaway recursive function to throw "too much
recursion", which causes terrible performance in scripts obfuscated using
obfuscator.io.

This patch leaves the actual stack size as-is, but changes the
JS-engine-specific stack quota back to 2MB on 64-bit Windows (6MB if ASAN is
enabled). 32-bit Windows is unaffected by the new cap.

Differential Revision: https://phabricator.services.mozilla.com/D24597

diff --git a/js/xpconnect/src/XPCJSContext.cpp b/js/xpconnect/src/XPCJSContext.cpp
--- a/js/xpconnect/src/XPCJSContext.cpp
+++ b/js/xpconnect/src/XPCJSContext.cpp
@@ -65,16 +65,18 @@
 
 #if defined(XP_LINUX) && !defined(ANDROID)
 // For getrlimit and min/max.
 #include <algorithm>
 #include <sys/resource.h>
 #endif
 
 #ifdef XP_WIN
+// For min.
+#  include <algorithm>
 #include <windows.h>
 #endif
 
 static MOZ_THREAD_LOCAL(XPCJSContext*) gTlsContext;
 
 using namespace mozilla;
 using namespace xpc;
 using namespace JS;
@@ -1087,26 +1089,31 @@ nsresult XPCJSContext::Initialize(XPCJSC
           : kStackQuotaMin;
 #if defined(MOZ_ASAN)
   // See the standalone MOZ_ASAN branch below for the ASan case.
   const size_t kTrustedScriptBuffer = 450 * 1024;
 #else
   const size_t kTrustedScriptBuffer = 180 * 1024;
 #endif
 #elif defined(XP_WIN)
-    // 1MB is the default stack size on Windows. We use the -STACK linker flag
-    // (see WIN32_EXE_LDFLAGS in config/config.mk) to request a larger stack,
-    // so we determine the stack size at runtime.
-    const size_t kStackQuota = GetWindowsStackSize();
+  // 1MB is the default stack size on Windows. We use the -STACK linker flag
+  // (see WIN32_EXE_LDFLAGS in config/config.mk) to request a larger stack, so
+  // we determine the stack size at runtime. But 8MB is more than the Web can
+  // handle (bug 1537609), so clamp to something remotely reasonable.
 #  if defined(MOZ_ASAN)
-    // See the standalone MOZ_ASAN branch below for the ASan case.
-    const size_t kTrustedScriptBuffer = 450 * 1024;
+  // See the standalone MOZ_ASAN branch below for the ASan case.
+  const size_t kStackQuota =
+      std::min(GetWindowsStackSize(), size_t(6 * 1024 * 1024));
+  const size_t kTrustedScriptBuffer = 450 * 1024;
 #  else
-    const size_t kTrustedScriptBuffer = (sizeof(size_t) == 8) ? 180 * 1024   //win64
-                                                              : 120 * 1024;  //win32
+  const size_t kStackQuota =
+      std::min(GetWindowsStackSize(), size_t(2 * 1024 * 1024));
+  const size_t kTrustedScriptBuffer = (sizeof(size_t) == 8)
+                                          ? 180 * 1024   // win64
+                                          : 120 * 1024;  // win32
 #  endif
 #elif defined(MOZ_ASAN)
   // ASan requires more stack space due to red-zones, so give it double the
   // default (1MB on 32-bit, 2MB on 64-bit). ASAN stack frame measurements
   // were not taken at the time of this writing, so we hazard a guess that
   // ASAN builds have roughly thrice the stack overhead as normal builds.
   // On normal builds, the largest stack frame size we might encounter is
   // 9.0k (see above), so let's use a buffer of 9.0 * 5 * 10 = 450k.
