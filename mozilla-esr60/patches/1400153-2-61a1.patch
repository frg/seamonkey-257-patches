# HG changeset patch
# User Steve Fink <sfink@mozilla.com>
# Date 1524249328 25200
# Node ID 5bfd3b3d6cc2c45e0682342071d0f635aca86741
# Parent  57e8b53747e0d237aa905ae28677d4f42e5d61d4
Bug 1400153 - Add another check for timestamp problems, and attempt to get all the intermittents routed to one bug, r=jonco

diff --git a/js/src/gc/Statistics.cpp b/js/src/gc/Statistics.cpp
--- a/js/src/gc/Statistics.cpp
+++ b/js/src/gc/Statistics.cpp
@@ -779,17 +779,17 @@ static PhaseKind LongestPhaseSelfTimeInM
   // time.
   for (auto i : AllPhases()) {
     Phase parent = phases[i].parent;
     if (parent != Phase::NONE) {
       bool ok = CheckSelfTime(parent, i, times, selfTimes, times[i]);
 
       // This happens very occasionally in release builds. Skip collecting
       // longest phase telemetry if it does.
-      MOZ_ASSERT(ok, "Inconsistent time data");
+      MOZ_ASSERT(ok, "Inconsistent time data; see bug 1400153");
       if (!ok) return PhaseKind::NONE;
 
       selfTimes[parent] -= times[i];
     }
   }
 
   // Sum expanded phases corresponding to the same phase.
   EnumeratedArray<PhaseKind, PhaseKind::LIMIT, TimeDuration> phaseTimes;
@@ -995,16 +995,19 @@ void Statistics::endSlice() {
   if (last) {
     for (auto& count : counts) count = 0;
 
     // Clear the timers at the end of a GC, preserving the data for
     // PhaseKind::MUTATOR.
     auto mutatorStartTime = phaseStartTimes[Phase::MUTATOR];
     auto mutatorTime = phaseTimes[Phase::MUTATOR];
     PodZero(&phaseStartTimes);
+#ifdef DEBUG
+    PodZero(&phaseEndTimes);
+#endif
     PodZero(&phaseTimes);
     phaseStartTimes[Phase::MUTATOR] = mutatorStartTime;
     phaseTimes[Phase::MUTATOR] = mutatorTime;
   }
 
   aborted = false;
 }
 
@@ -1094,18 +1097,18 @@ void Statistics::recordPhaseBegin(Phase 
   MOZ_ASSERT(phaseStack.length() < MAX_PHASE_NESTING);
 
   Phase current = currentPhase();
   MOZ_ASSERT(phases[phase].parent == current);
 
   TimeStamp now = TimeStamp::Now();
 
   if (current != Phase::NONE) {
-    // Sadly this happens sometimes.
-    MOZ_ASSERT(now >= phaseStartTimes[currentPhase()]);
+    MOZ_ASSERT(now >= phaseStartTimes[currentPhase()],
+               "Inconsistent time data; see bug 1400153");
     if (now < phaseStartTimes[currentPhase()]) {
       now = phaseStartTimes[currentPhase()];
       aborted = true;
     }
   }
 
   phaseStack.infallibleAppend(phase);
   phaseStartTimes[phase] = now;
@@ -1113,31 +1116,56 @@ void Statistics::recordPhaseBegin(Phase 
 
 void Statistics::recordPhaseEnd(Phase phase) {
   MOZ_ASSERT(CurrentThreadCanAccessRuntime(runtime));
 
   MOZ_ASSERT(phaseStartTimes[phase]);
 
   TimeStamp now = TimeStamp::Now();
 
-  // Sadly this happens sometimes.
-  MOZ_ASSERT(now >= phaseStartTimes[phase]);
+  // Make sure this phase ends after it starts.
+  MOZ_ASSERT(now >= phaseStartTimes[phase],
+             "Inconsistent time data; see bug 1400153");
+
+#ifdef DEBUG
+  // Make sure this phase ends after all of its children. Note that some
+  // children might not have run in this instance, in which case they will
+  // have run in a previous instance of this parent or not at all.
+  for (Phase kid = phases[phase].firstChild; kid != Phase::NONE;
+       kid = phases[kid].nextSibling) {
+    if (phaseEndTimes[kid].IsNull())
+      continue;
+    if (phaseEndTimes[kid] > now)
+      fprintf(stderr,
+              "Parent %s ended at %.3fms, before child %s ended at %.3fms?\n",
+              phases[phase].name, t(now - TimeStamp::ProcessCreation()),
+              phases[kid].name,
+              t(phaseEndTimes[kid] - TimeStamp::ProcessCreation()));
+    MOZ_ASSERT(phaseEndTimes[kid] <= now,
+               "Inconsistent time data; see bug 1400153");
+  }
+#endif
+
   if (now < phaseStartTimes[phase]) {
     now = phaseStartTimes[phase];
     aborted = true;
   }
 
   if (phase == Phase::MUTATOR) timedGCStart = now;
 
   phaseStack.popBack();
 
   TimeDuration t = now - phaseStartTimes[phase];
   if (!slices_.empty()) slices_.back().phaseTimes[phase] += t;
   phaseTimes[phase] += t;
   phaseStartTimes[phase] = TimeStamp();
+
+#ifdef DEBUG
+  phaseEndTimes[phase] = now;
+#endif
 }
 
 void Statistics::endPhase(PhaseKind phaseKind) {
   Phase phase = currentPhase();
   MOZ_ASSERT(phase != Phase::NONE);
   MOZ_ASSERT(phases[phase].phaseKind == phaseKind);
 
   recordPhaseEnd(phase);
diff --git a/js/src/gc/Statistics.h b/js/src/gc/Statistics.h
--- a/js/src/gc/Statistics.h
+++ b/js/src/gc/Statistics.h
@@ -270,16 +270,21 @@ struct Statistics {
 
   gc::AbortReason nonincrementalReason_;
 
   SliceDataVector slices_;
 
   /* Most recent time when the given phase started. */
   EnumeratedArray<Phase, Phase::LIMIT, TimeStamp> phaseStartTimes;
 
+#ifdef DEBUG
+  /* Most recent time when the given phase ended. */
+  EnumeratedArray<Phase, Phase::LIMIT, TimeStamp> phaseEndTimes;
+#endif
+
   /* Bookkeeping for GC timings when timingMutator is true */
   TimeStamp timedGCStart;
   TimeDuration timedGCTime;
 
   /* Total time in a given phase for this GC. */
   PhaseTimeTable phaseTimes;
   PhaseTimeTable parallelTimes;
 
