# HG changeset patch
# User Boris Zbarsky <bzbarsky@mit.edu>
# Date 1521487087 14400
# Node ID e43988b4ba26410a0d6f1a9969364074833458ed
# Parent  e7979805ce1f3c7a7481fc030279bcb96fcff6aa
Bug 1446533 part 3.  Remove nsIDOMCharacterData::Get/SetData.  r=mystor

MozReview-Commit-ID: 5YeaCPwvIJH

diff --git a/dom/base/CharacterData.cpp b/dom/base/CharacterData.cpp
--- a/dom/base/CharacterData.cpp
+++ b/dom/base/CharacterData.cpp
@@ -127,53 +127,53 @@ NS_INTERFACE_MAP_BEGIN(CharacterData)
   NS_INTERFACE_MAP_ENTRY_AMBIGUOUS(nsISupports, nsIContent)
 NS_INTERFACE_MAP_END
 
 NS_IMPL_MAIN_THREAD_ONLY_CYCLE_COLLECTING_ADDREF(CharacterData)
 NS_IMPL_MAIN_THREAD_ONLY_CYCLE_COLLECTING_RELEASE_WITH_LAST_RELEASE(
     CharacterData, nsNodeUtils::LastRelease(this))
 
 void CharacterData::GetNodeValueInternal(nsAString& aNodeValue) {
-  DebugOnly<nsresult> rv = GetData(aNodeValue);
-  NS_ASSERTION(NS_SUCCEEDED(rv), "GetData() failed!");
+  GetData(aNodeValue);
 }
 
 void CharacterData::SetNodeValueInternal(const nsAString& aNodeValue,
                                          ErrorResult& aError) {
   aError = SetTextInternal(0, mText.GetLength(), aNodeValue.BeginReading(),
                            aNodeValue.Length(), true);
 }
 
 //----------------------------------------------------------------------
 
 // Implementation of nsIDOMCharacterData
 
-nsresult CharacterData::GetData(nsAString& aData) const {
+void CharacterData::GetData(nsAString& aData) const {
   if (mText.Is2b()) {
     aData.Truncate();
     mText.AppendTo(aData);
   } else {
     // Must use Substring() since nsDependentCString() requires null
     // terminated strings.
 
     const char* data = mText.Get1b();
 
     if (data) {
       CopyASCIItoUTF16(Substring(data, data + mText.GetLength()), aData);
     } else {
       aData.Truncate();
     }
   }
-
-  return NS_OK;
 }
 
-nsresult CharacterData::SetData(const nsAString& aData) {
-  return SetTextInternal(0, mText.GetLength(), aData.BeginReading(),
-                         aData.Length(), true);
+void CharacterData::SetData(const nsAString& aData, ErrorResult& aRv) {
+  nsresult rv = SetTextInternal(0, mText.GetLength(), aData.BeginReading(),
+                                aData.Length(), true);
+  if (NS_FAILED(rv)) {
+    aRv.Throw(rv);
+  }
 }
 
 nsresult CharacterData::GetLength(uint32_t* aLength) {
   *aLength = mText.GetLength();
   return NS_OK;
 }
 
 nsresult CharacterData::SubstringData(uint32_t aStart, uint32_t aCount,
diff --git a/dom/base/CharacterData.h b/dom/base/CharacterData.h
--- a/dom/base/CharacterData.h
+++ b/dom/base/CharacterData.h
@@ -88,18 +88,16 @@ class CharacterData : public nsIContent 
     SetFlags(NS_MAYBE_MODIFIED_FREQUENTLY);
   }
 
   virtual void GetNodeValueInternal(nsAString& aNodeValue) override;
   virtual void SetNodeValueInternal(const nsAString& aNodeValue,
                                     ErrorResult& aError) override;
 
   // Implementation for nsIDOMCharacterData
-  nsresult GetData(nsAString& aData) const;
-  nsresult SetData(const nsAString& aData);
   nsresult GetLength(uint32_t* aLength);
   nsresult SubstringData(uint32_t aOffset, uint32_t aCount, nsAString& aReturn);
   nsresult AppendData(const nsAString& aArg);
   nsresult InsertData(uint32_t aOffset, const nsAString& aArg);
   nsresult DeleteData(uint32_t aOffset, uint32_t aCount);
   nsresult ReplaceData(uint32_t aOffset, uint32_t aCount,
                        const nsAString& aArg);
 
@@ -171,20 +169,18 @@ class CharacterData : public nsIContent 
     if (!*aResult) {
       return NS_ERROR_OUT_OF_MEMORY;
     }
 
     return NS_OK;
   }
 
   // WebIDL API
-  // Our XPCOM GetData is just fine for WebIDL
-  virtual void SetData(const nsAString& aData, ErrorResult& rv) {
-    rv = SetData(aData);
-  }
+  void GetData(nsAString& aData) const;
+  virtual void SetData(const nsAString& aData, ErrorResult& rv);
   // nsINode::Length() returns the right thing for our length attribute
   void SubstringData(uint32_t aStart, uint32_t aCount, nsAString& aReturn,
                      ErrorResult& rv);
   void AppendData(const nsAString& aData, ErrorResult& rv) {
     rv = AppendData(aData);
   }
   void InsertData(uint32_t aOffset, const nsAString& aData, ErrorResult& rv) {
     rv = InsertData(aOffset, aData);
diff --git a/dom/base/Comment.h b/dom/base/Comment.h
--- a/dom/base/Comment.h
+++ b/dom/base/Comment.h
@@ -34,17 +34,16 @@ class Comment final : public CharacterDa
     Init();
   }
 
   // nsISupports
   NS_DECL_ISUPPORTS_INHERITED
 
   // nsIDOMCharacterData
   NS_FORWARD_NSIDOMCHARACTERDATA(CharacterData::)
-  using CharacterData::SetData; // Prevent hiding overloaded virtual function.
 
   // nsINode
   virtual bool IsNodeOfType(uint32_t aFlags) const override;
 
   virtual CharacterData* CloneDataNode(mozilla::dom::NodeInfo *aNodeInfo,
                                        bool aCloneText) const override;
 
   virtual nsIDOMNode* AsDOMNode() override { return this; }
diff --git a/dom/base/nsTextNode.h b/dom/base/nsTextNode.h
--- a/dom/base/nsTextNode.h
+++ b/dom/base/nsTextNode.h
@@ -38,17 +38,16 @@ class nsTextNode : public mozilla::dom::
     Init();
   }
 
   // nsISupports
   NS_DECL_ISUPPORTS_INHERITED
 
   // nsIDOMCharacterData
   NS_FORWARD_NSIDOMCHARACTERDATA(mozilla::dom::CharacterData::)
-  using CharacterData::SetData; // Prevent hiding overloaded virtual function.
 
   // nsINode
   virtual bool IsNodeOfType(uint32_t aFlags) const override;
 
   virtual CharacterData* CloneDataNode(mozilla::dom::NodeInfo *aNodeInfo,
                                        bool aCloneText) const override;
 
   virtual nsresult BindToTree(nsIDocument* aDocument, nsIContent* aParent,
diff --git a/dom/base/nsXMLContentSerializer.cpp b/dom/base/nsXMLContentSerializer.cpp
--- a/dom/base/nsXMLContentSerializer.cpp
+++ b/dom/base/nsXMLContentSerializer.cpp
@@ -237,25 +237,23 @@ nsXMLContentSerializer::AppendCDATASecti
   return NS_OK;
 }
 
 NS_IMETHODIMP
 nsXMLContentSerializer::AppendProcessingInstruction(ProcessingInstruction* aPI,
                                                     int32_t aStartOffset,
                                                     int32_t aEndOffset,
                                                     nsAString& aStr) {
-  nsresult rv;
   nsAutoString target, data, start;
 
   NS_ENSURE_TRUE(MaybeAddNewlineForRootNode(aStr), NS_ERROR_OUT_OF_MEMORY);
 
   aPI->GetTarget(target);
 
-  rv = aPI->GetData(data);
-  if (NS_FAILED(rv)) return NS_ERROR_FAILURE;
+  aPI->GetData(data);
 
   NS_ENSURE_TRUE(start.AppendLiteral("<?", mozilla::fallible),
                  NS_ERROR_OUT_OF_MEMORY);
   NS_ENSURE_TRUE(start.Append(target, mozilla::fallible),
                  NS_ERROR_OUT_OF_MEMORY);
 
   if (mDoRaw || PreLevel() > 0) {
     NS_ENSURE_TRUE(AppendToString(start, aStr), NS_ERROR_OUT_OF_MEMORY);
@@ -281,21 +279,19 @@ nsXMLContentSerializer::AppendProcessing
   MaybeFlagNewlineForRootNode(aPI);
 
   return NS_OK;
 }
 
 NS_IMETHODIMP
 nsXMLContentSerializer::AppendComment(Comment* aComment, int32_t aStartOffset,
                                       int32_t aEndOffset, nsAString& aStr) {
-  nsresult rv;
   nsAutoString data;
 
-  rv = aComment->GetData(data);
-  if (NS_FAILED(rv)) return NS_ERROR_FAILURE;
+  aComment->GetData(data);
 
   int32_t dataLength = data.Length();
   if (aStartOffset || (aEndOffset != -1 && aEndOffset < dataLength)) {
     int32_t length =
         (aEndOffset == -1) ? dataLength : std::min(aEndOffset, dataLength);
     length -= aStartOffset;
 
     nsAutoString frag;
diff --git a/dom/interfaces/core/nsIDOMCharacterData.idl b/dom/interfaces/core/nsIDOMCharacterData.idl
--- a/dom/interfaces/core/nsIDOMCharacterData.idl
+++ b/dom/interfaces/core/nsIDOMCharacterData.idl
@@ -11,20 +11,16 @@
  * 
  * For more information on this interface please see 
  * http://www.w3.org/TR/DOM-Level-2-Core/
  */
 
 [uuid(4109a2d2-e7af-445d-bb72-c7c9b875f35e)]
 interface nsIDOMCharacterData : nsIDOMNode
 {
-           attribute DOMString            data;
-                                  // raises(DOMException) on setting
-                                  // raises(DOMException) on retrieval
-
   readonly attribute unsigned long        length;
   DOMString                 substringData(in unsigned long offset, 
                                           in unsigned long count)
                                   raises(DOMException);
   void                      appendData(in DOMString arg)
                                   raises(DOMException);
   void                      insertData(in unsigned long offset, 
                                        in DOMString arg)
diff --git a/dom/webbrowserpersist/WebBrowserPersistLocalDocument.cpp b/dom/webbrowserpersist/WebBrowserPersistLocalDocument.cpp
--- a/dom/webbrowserpersist/WebBrowserPersistLocalDocument.cpp
+++ b/dom/webbrowserpersist/WebBrowserPersistLocalDocument.cpp
@@ -380,20 +380,18 @@ nsresult ResourceReader::OnWalkAttribute
   if (uriSpec.IsEmpty()) {
     return NS_OK;
   }
   return OnWalkURI(uriSpec);
 }
 
 static nsresult GetXMLStyleSheetLink(dom::ProcessingInstruction *aPI,
                                      nsAString& aHref) {
-  nsresult rv;
   nsAutoString data;
-  rv = aPI->GetData(data);
-  NS_ENSURE_SUCCESS(rv, NS_ERROR_FAILURE);
+  aPI->GetData(data);
 
   nsContentUtils::GetPseudoAttributeValue(data, nsGkAtoms::href, aHref);
   return NS_OK;
 }
 
 nsresult ResourceReader::OnWalkDOMNode(nsIDOMNode* aNode) {
   nsCOMPtr<nsIContent> content = do_QueryInterface(aNode);
   if (!content) {
@@ -719,21 +717,19 @@ static void AppendXMLAttr(const nsAStrin
     }
   }
   aBuffer.Append('"');
 }
 
 nsresult PersistNodeFixup::FixupXMLStyleSheetLink(
     dom::ProcessingInstruction* aPI, const nsAString& aHref) {
   NS_ENSURE_ARG_POINTER(aPI);
-  nsresult rv = NS_OK;
 
   nsAutoString data;
-  rv = aPI->GetData(data);
-  NS_ENSURE_SUCCESS(rv, NS_ERROR_FAILURE);
+  aPI->GetData(data);
 
   nsAutoString href;
   nsContentUtils::GetPseudoAttributeValue(data, nsGkAtoms::href, href);
 
   // Construct and set a new data value for the xml-stylesheet
   if (!aHref.IsEmpty() && !href.IsEmpty()) {
     nsAutoString alternate;
     nsAutoString charset;
@@ -760,20 +756,20 @@ nsresult PersistNodeFixup::FixupXMLStyle
       AppendXMLAttr(NS_LITERAL_STRING("type"), type, newData);
     }
     if (!charset.IsEmpty()) {
       AppendXMLAttr(NS_LITERAL_STRING("charset"), charset, newData);
     }
     if (!alternate.IsEmpty()) {
       AppendXMLAttr(NS_LITERAL_STRING("alternate"), alternate, newData);
     }
-    aPI->SetData(newData);
+    aPI->SetData(newData, IgnoreErrors());
   }
 
-  return rv;
+  return NS_OK;
 }
 
 NS_IMETHODIMP
 PersistNodeFixup::FixupNode(nsIDOMNode* aNodeIn, bool* aSerializeCloneKids,
                             nsIDOMNode** aNodeOut) {
   nsCOMPtr<nsINode> nodeIn = do_QueryInterface(aNodeIn);
   nsCOMPtr<nsINode> nodeOut;
   nsresult rv = FixupNode(nodeIn, aSerializeCloneKids, getter_AddRefs(nodeOut));
diff --git a/dom/xml/CDATASection.h b/dom/xml/CDATASection.h
--- a/dom/xml/CDATASection.h
+++ b/dom/xml/CDATASection.h
@@ -36,18 +36,16 @@ class CDATASection final : public Text, 
     Init();
   }
 
   // nsISupports
   NS_DECL_ISUPPORTS_INHERITED
 
   // nsIDOMCharacterData
   NS_FORWARD_NSIDOMCHARACTERDATA(CharacterData::)
-  using CharacterData::SetData; // Prevent hiding overloaded virtual function.
-  using CharacterData::GetData;
 
   // nsINode
   virtual bool IsNodeOfType(uint32_t aFlags) const override;
 
   virtual CharacterData* CloneDataNode(mozilla::dom::NodeInfo *aNodeInfo,
                                        bool aCloneText) const override;
 
   virtual nsIDOMNode* AsDOMNode() override { return this; }
diff --git a/dom/xml/ProcessingInstruction.h b/dom/xml/ProcessingInstruction.h
--- a/dom/xml/ProcessingInstruction.h
+++ b/dom/xml/ProcessingInstruction.h
@@ -20,18 +20,16 @@ class ProcessingInstruction : public Cha
   ProcessingInstruction(already_AddRefed<mozilla::dom::NodeInfo>&& aNodeInfo,
                         const nsAString& aData);
 
   // nsISupports
   NS_DECL_ISUPPORTS_INHERITED
 
   // nsIDOMCharacterData
   NS_FORWARD_NSIDOMCHARACTERDATA(CharacterData::)
-  using CharacterData::SetData; // Prevent hiding overloaded virtual function.
-  using CharacterData::GetData;
 
   // nsINode
   virtual bool IsNodeOfType(uint32_t aFlags) const override;
 
   virtual CharacterData* CloneDataNode(mozilla::dom::NodeInfo *aNodeInfo,
                                        bool aCloneText) const override;
 
 #ifdef DEBUG
diff --git a/dom/xml/XMLStylesheetProcessingInstruction.h b/dom/xml/XMLStylesheetProcessingInstruction.h
--- a/dom/xml/XMLStylesheetProcessingInstruction.h
+++ b/dom/xml/XMLStylesheetProcessingInstruction.h
@@ -62,19 +62,16 @@ class XMLStylesheetProcessingInstruction
   virtual void SetData(const nsAString& aData,
                        mozilla::ErrorResult& rv) override {
     CharacterData::SetData(aData, rv);
     if (rv.Failed()) {
       return;
     }
     UpdateStyleSheetInternal(nullptr, nullptr, true);
   }
-  using ProcessingInstruction::SetData;  // Prevent hiding overloaded virtual
-                                         // function.
-  using ProcessingInstruction::GetData;
 
  protected:
   virtual ~XMLStylesheetProcessingInstruction();
 
   nsCOMPtr<nsIURI> mOverriddenBaseURI;
 
   already_AddRefed<nsIURI> GetStyleSheetURL(
       bool* aIsInline, nsIPrincipal** aTriggeringPrincipal) final;
diff --git a/editor/libeditor/EditorBase.cpp b/editor/libeditor/EditorBase.cpp
--- a/editor/libeditor/EditorBase.cpp
+++ b/editor/libeditor/EditorBase.cpp
@@ -2820,17 +2820,19 @@ nsresult EditorBase::SetTextImpl(Selecti
       listener->WillDeleteText(
           static_cast<nsIDOMCharacterData*>(aCharData.AsDOMNode()), 0, length);
     }
   }
 
   // We don't support undo here, so we don't really need all of the transaction
   // machinery, therefore we can run our transaction directly, breaking all of
   // the rules!
-  nsresult rv = aCharData.SetData(aString);
+  ErrorResult res;
+  aCharData.SetData(aString, res);
+  nsresult rv = res.StealNSResult();
   if (NS_WARN_IF(NS_FAILED(rv))) {
     return rv;
   }
 
   {
     // Create a nested scope to not overwrite rv from the outer scope.
     RefPtr<Selection> selection = GetSelection();
     DebugOnly<nsresult> rv = selection->Collapse(&aCharData, aString.Length());
@@ -2982,17 +2984,17 @@ void EditorBase::SplitNodeImpl(const Edi
     Text* rightAsText = aStartOfRightNode.GetContainerAsText();
     Text* leftAsText = aNewLeftNode.GetAsText();
     if (rightAsText && leftAsText) {
       // Fix right node
       nsAutoString leftText;
       rightAsText->SubstringData(0, aStartOfRightNode.Offset(), leftText);
       rightAsText->DeleteData(0, aStartOfRightNode.Offset());
       // Fix left node
-      leftAsText->GetAsText()->SetData(leftText);
+      leftAsText->GetAsText()->SetData(leftText, IgnoreErrors());
     } else {
       MOZ_DIAGNOSTIC_ASSERT(!rightAsText && !leftAsText);
       // Otherwise it's an interior node, so shuffle around the children. Go
       // through list backwards so deletes don't interfere with the iteration.
       if (!firstChildOfRightNode) {
         MoveAllChildren(*aStartOfRightNode.GetContainer(),
                         EditorRawDOMPoint(&aNewLeftNode, 0), aError);
         NS_WARNING_ASSERTION(
@@ -3145,17 +3147,17 @@ nsresult EditorBase::JoinNodesImpl(nsINo
   // OK, ready to do join now.
   // If it's a text node, just shuffle around some text.
   if (IsTextNode(aNodeToKeep) && IsTextNode(aNodeToJoin)) {
     nsAutoString rightText;
     nsAutoString leftText;
     aNodeToKeep->GetAsText()->GetData(rightText);
     aNodeToJoin->GetAsText()->GetData(leftText);
     leftText += rightText;
-    aNodeToKeep->GetAsText()->SetData(leftText);
+    aNodeToKeep->GetAsText()->SetData(leftText, IgnoreErrors());
   } else {
     // Otherwise it's an interior node, so shuffle around the children.
     nsCOMPtr<nsINodeList> childNodes = aNodeToJoin->ChildNodes();
     MOZ_ASSERT(childNodes);
 
     // Remember the first child in aNodeToKeep, we'll insert all the children of
     // aNodeToJoin in front of it GetFirstChild returns nullptr firstNode if
     // aNodeToKeep has no children, that's OK.
diff --git a/editor/libeditor/HTMLEditorDataTransfer.cpp b/editor/libeditor/HTMLEditorDataTransfer.cpp
--- a/editor/libeditor/HTMLEditorDataTransfer.cpp
+++ b/editor/libeditor/HTMLEditorDataTransfer.cpp
@@ -1908,18 +1908,17 @@ nsresult FindTargetNode(nsINode* aStart,
     }
     return NS_OK;
   }
 
   do {
     // Is this child the magical cookie?
     if (child->IsNodeOfType(nsINode::eCOMMENT)) {
       nsAutoString data;
-      nsresult rv = static_cast<Comment*>(child.get())->GetData(data);
-      NS_ENSURE_SUCCESS(rv, rv);
+      static_cast<Comment*>(child.get())->GetData(data);
 
       if (data.EqualsLiteral(kInsertCookie)) {
         // Yes it is! Return an error so we bubble out and short-circuit the
         // search.
         aResult = aStart;
 
         child->Remove();
 
diff --git a/editor/libeditor/TextEditRules.cpp b/editor/libeditor/TextEditRules.cpp
--- a/editor/libeditor/TextEditRules.cpp
+++ b/editor/libeditor/TextEditRules.cpp
@@ -1195,21 +1195,17 @@ nsresult TextEditRules::WillOutputText(S
   // just empty.
   if (!text) {
     aOutString->Truncate();
     *aHandled = true;
     return NS_OK;
   }
 
   // Otherwise, the text is the value.
-  nsresult rv = text->GetData(*aOutString);
-  if (NS_WARN_IF(NS_FAILED(rv))) {
-    // Fall back to the expensive path if it fails.
-    return NS_OK;
-  }
+  text->GetData(*aOutString);
 
   *aHandled = true;
   return NS_OK;
 }
 
 nsresult TextEditRules::DidOutputText(Selection* aSelection, nsresult aResult) {
   return NS_OK;
 }
diff --git a/layout/base/nsCounterManager.cpp b/layout/base/nsCounterManager.cpp
--- a/layout/base/nsCounterManager.cpp
+++ b/layout/base/nsCounterManager.cpp
@@ -160,17 +160,17 @@ void nsCounterList::RecalcAll() {
     if (node->mType == nsCounterNode::USE) {
       nsCounterUseNode* useNode = node->UseNode();
       // Null-check mText, since if the frame constructor isn't
       // batching, we could end up here while the node is being
       // constructed.
       if (useNode->mText) {
         nsAutoString text;
         useNode->GetText(text);
-        useNode->mText->SetData(text);
+        useNode->mText->SetData(text, IgnoreErrors());
       }
     }
   }
 }
 
 bool nsCounterManager::AddCounterResetsAndIncrements(nsIFrame* aFrame) {
   const nsStyleContent* styleContent = aFrame->StyleContent();
   if (!styleContent->CounterIncrementCount() &&
diff --git a/layout/base/nsQuoteList.cpp b/layout/base/nsQuoteList.cpp
--- a/layout/base/nsQuoteList.cpp
+++ b/layout/base/nsQuoteList.cpp
@@ -4,16 +4,19 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 /* implementation of quotes for the CSS 'content' property */
 
 #include "nsQuoteList.h"
 #include "nsReadableUtils.h"
 #include "nsIContent.h"
+#include "mozilla/ErrorResult.h"
+
+using namespace mozilla;
 
 bool nsQuoteNode::InitTextFrame(nsGenConList* aList, nsIFrame* aPseudoFrame,
                                 nsIFrame* aTextFrame) {
   nsGenConNode::InitTextFrame(aList, aPseudoFrame, aTextFrame);
 
   nsQuoteList* quoteList = static_cast<nsQuoteList*>(aList);
   bool dirty = false;
   quoteList->Insert(this);
@@ -65,17 +68,17 @@ void nsQuoteList::Calc(nsQuoteNode* aNod
 }
 
 void nsQuoteList::RecalcAll() {
   for (nsQuoteNode* node = FirstNode(); node; node = Next(node)) {
     int32_t oldDepth = node->mDepthBefore;
     Calc(node);
 
     if (node->mDepthBefore != oldDepth && node->mText && node->IsRealQuote())
-      node->mText->SetData(*node->Text());
+      node->mText->SetData(*node->Text(), IgnoreErrors());
   }
 }
 
 #ifdef DEBUG
 void nsQuoteList::PrintChain() {
   printf("Chain: \n");
   for (nsQuoteNode* node = FirstNode(); node; node = Next(node)) {
     printf("  %p %d - ", static_cast<void*>(node), node->mDepthBefore);
