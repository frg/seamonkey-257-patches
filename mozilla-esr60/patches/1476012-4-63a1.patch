# HG changeset patch
# User Jon Coppeard <jcoppeard@mozilla.com>
# Date 1531829234 -3600
# Node ID dc942361448ddd7af5e31213f3e4b45343b8f9ab
# Parent  50c225b3afa64c3567070ed2fe68e35542de215a
Bug 1476012 - Remove the dependency of JitcodeMap.h on CodeGenerator-shared.h r=nbp

diff --git a/js/src/gc/GC.cpp b/js/src/gc/GC.cpp
--- a/js/src/gc/GC.cpp
+++ b/js/src/gc/GC.cpp
@@ -216,16 +216,17 @@
 #include "gc/GCInternals.h"
 #include "gc/GCTrace.h"
 #include "gc/Memory.h"
 #include "gc/Policy.h"
 #include "gc/WeakMap.h"
 #include "jit/BaselineJIT.h"
 #include "jit/IonCode.h"
 #include "jit/JitcodeMap.h"
+#include "jit/JitRealm.h"
 #include "js/SliceBudget.h"
 #include "proxy/DeadObjectProxy.h"
 #include "util/Windows.h"
 #ifdef ENABLE_BIGINT
 #include "vm/BigIntType.h"
 #endif
 #include "vm/Debugger.h"
 #include "vm/GeckoProfiler.h"
diff --git a/js/src/jit/JitcodeMap.cpp b/js/src/jit/JitcodeMap.cpp
--- a/js/src/jit/JitcodeMap.cpp
+++ b/js/src/jit/JitcodeMap.cpp
@@ -9,16 +9,17 @@
 #include "mozilla/ArrayUtils.h"
 #include "mozilla/MathAlgorithms.h"
 #include "mozilla/Maybe.h"
 #include "mozilla/Sprintf.h"
 
 #include "gc/Marking.h"
 #include "gc/Statistics.h"
 #include "jit/BaselineJIT.h"
+#include "jit/JitRealm.h"
 #include "jit/JitSpewer.h"
 #include "js/Vector.h"
 #include "vm/GeckoProfiler.h"
 
 #include "vm/GeckoProfiler-inl.h"
 #include "vm/JSScript-inl.h"
 #include "vm/TypeInference-inl.h"
 
@@ -1070,18 +1071,17 @@ void JitcodeGlobalEntry::IonCacheEntry::
     pcDeltaU |= ~ENC4_PC_DELTA_MAX;
   *pcDelta = pcDeltaU;
 
   MOZ_ASSERT(*pcDelta != 0);
   MOZ_ASSERT_IF(*nativeDelta == 0, *pcDelta <= 0);
 }
 
 /* static */ uint32_t JitcodeRegionEntry::ExpectedRunLength(
-    const CodeGeneratorShared::NativeToBytecode* entry,
-    const CodeGeneratorShared::NativeToBytecode* end) {
+    const NativeToBytecode* entry, const NativeToBytecode* end) {
   MOZ_ASSERT(entry < end);
 
   // We always use the first entry, so runLength starts at 1
   uint32_t runLength = 1;
 
   uint32_t curNativeOffset = entry->nativeOffset.offset();
   uint32_t curBytecodeOffset = entry->tree->script()->pcToOffset(entry->pc);
 
@@ -1154,17 +1154,17 @@ struct JitcodeMapBufferWriteSpewer {
   void spewAndAdvance(const char* name) {}
 #endif  // JS_JITSPEW
 };
 
 // Write a run, starting at the given NativeToBytecode entry, into the given
 // buffer writer.
 /* static */ bool JitcodeRegionEntry::WriteRun(
     CompactBufferWriter& writer, JSScript** scriptList, uint32_t scriptListSize,
-    uint32_t runLength, const CodeGeneratorShared::NativeToBytecode* entry) {
+    uint32_t runLength, const NativeToBytecode* entry) {
   MOZ_ASSERT(runLength > 0);
   MOZ_ASSERT(runLength <= MAX_RUN_LENGTH);
 
   // Calculate script depth.
   MOZ_ASSERT(entry->tree->depth() <= 0xff);
   uint8_t scriptDepth = entry->tree->depth();
   uint32_t regionNativeOffset = entry->nativeOffset.offset();
 
@@ -1391,19 +1391,18 @@ uint32_t JitcodeIonTable::findRegionEntr
       count -= step;
     }
   }
   return idx;
 }
 
 /* static */ bool JitcodeIonTable::WriteIonTable(
     CompactBufferWriter& writer, JSScript** scriptList, uint32_t scriptListSize,
-    const CodeGeneratorShared::NativeToBytecode* start,
-    const CodeGeneratorShared::NativeToBytecode* end, uint32_t* tableOffsetOut,
-    uint32_t* numRegionsOut) {
+    const NativeToBytecode* start, const NativeToBytecode* end,
+    uint32_t* tableOffsetOut, uint32_t* numRegionsOut) {
   MOZ_ASSERT(tableOffsetOut != nullptr);
   MOZ_ASSERT(numRegionsOut != nullptr);
   MOZ_ASSERT(writer.length() == 0);
   MOZ_ASSERT(scriptListSize > 0);
 
   JitSpew(JitSpew_Profiling,
           "Writing native to bytecode map for %s:%u (%zu entries)",
           scriptList[0]->filename(), scriptList[0]->lineno(),
@@ -1412,17 +1411,17 @@ uint32_t JitcodeIonTable::findRegionEntr
   JitSpew(JitSpew_Profiling, "  ScriptList of size %d", int(scriptListSize));
   for (uint32_t i = 0; i < scriptListSize; i++) {
     JitSpew(JitSpew_Profiling, "  Script %d - %s:%u", int(i),
             scriptList[i]->filename(), scriptList[i]->lineno());
   }
 
   // Write out runs first.  Keep a vector tracking the positive offsets from
   // payload start to the run.
-  const CodeGeneratorShared::NativeToBytecode* curEntry = start;
+  const NativeToBytecode* curEntry = start;
   js::Vector<uint32_t, 32, SystemAllocPolicy> runOffsets;
 
   while (curEntry != end) {
     // Calculate the length of the next run.
     uint32_t runLength = JitcodeRegionEntry::ExpectedRunLength(curEntry, end);
     MOZ_ASSERT(runLength > 0);
     MOZ_ASSERT(runLength <= uintptr_t(end - curEntry));
     JitSpew(JitSpew_Profiling, "  Run at entry %d, length %d, buffer offset %d",
diff --git a/js/src/jit/JitcodeMap.h b/js/src/jit/JitcodeMap.h
--- a/js/src/jit/JitcodeMap.h
+++ b/js/src/jit/JitcodeMap.h
@@ -6,17 +6,16 @@
 
 #ifndef jit_JitcodeMap_h
 #define jit_JitcodeMap_h
 
 #include "jit/CompactBuffer.h"
 #include "jit/CompileInfo.h"
 #include "jit/ExecutableAllocator.h"
 #include "jit/OptimizationTracking.h"
-#include "jit/shared/CodeGenerator-shared.h"
 
 namespace js {
 namespace jit {
 
 /*
  * The Ion jitcode map implements tables to allow mapping from addresses in ion
  * jitcode to the list of (JSScript*, jsbytecode*) pairs that are implicitly
  * active in the frame at that point in the native code.
@@ -32,16 +31,22 @@ namespace jit {
  */
 
 class JitcodeGlobalTable;
 class JitcodeIonTable;
 class JitcodeRegionEntry;
 
 class JitcodeGlobalEntry;
 
+struct NativeToBytecode {
+  CodeOffset nativeOffset;
+  InlineScriptTree* tree;
+  jsbytecode* pc;
+};
+
 class JitcodeSkiplistTower {
  public:
   static const unsigned MAX_HEIGHT = 32;
 
  private:
   uint8_t height_;
   bool isFree_;
   JitcodeGlobalEntry* ptrs_[1];
@@ -1160,26 +1165,25 @@ class JitcodeRegionEntry {
   static void WriteDelta(CompactBufferWriter& writer, uint32_t nativeDelta,
                          int32_t pcDelta);
   static void ReadDelta(CompactBufferReader& reader, uint32_t* nativeDelta,
                         int32_t* pcDelta);
 
   // Given a pointer into an array of NativeToBytecode (and a pointer to the end
   // of the array), compute the number of entries that would be consume by
   // outputting a run starting at this one.
-  static uint32_t ExpectedRunLength(
-      const CodeGeneratorShared::NativeToBytecode* entry,
-      const CodeGeneratorShared::NativeToBytecode* end);
+  static uint32_t ExpectedRunLength(const NativeToBytecode* entry,
+                                    const NativeToBytecode* end);
 
   // Write a run, starting at the given NativeToBytecode entry, into the given
   // buffer writer.
   static MOZ_MUST_USE bool WriteRun(
       CompactBufferWriter& writer, JSScript** scriptList,
       uint32_t scriptListSize, uint32_t runLength,
-      const CodeGeneratorShared::NativeToBytecode* entry);
+      const NativeToBytecode* entry);
 
   // Delta Run entry formats are encoded little-endian:
   //
   //  byte 0
   //  NNNN-BBB0
   //      Single byte format.  nativeDelta in [0, 15], pcDelta in [0, 7]
   //
   static const uint32_t ENC1_MASK = 0x1;
@@ -1409,18 +1413,17 @@ class JitcodeIonTable {
   const uint8_t* payloadStart() const {
     // The beginning of the payload the beginning of the first region are the
     // same.
     return payloadEnd() - regionOffset(0);
   }
 
   static MOZ_MUST_USE bool WriteIonTable(
       CompactBufferWriter& writer, JSScript** scriptList,
-      uint32_t scriptListSize,
-      const CodeGeneratorShared::NativeToBytecode* start,
-      const CodeGeneratorShared::NativeToBytecode* end,
-      uint32_t* tableOffsetOut, uint32_t* numRegionsOut);
+      uint32_t scriptListSize, const NativeToBytecode* start,
+      const NativeToBytecode* end, uint32_t* tableOffsetOut,
+      uint32_t* numRegionsOut);
 };
 
 }  // namespace jit
 }  // namespace js
 
 #endif /* jit_JitcodeMap_h */
diff --git a/js/src/jit/OptimizationTracking.h b/js/src/jit/OptimizationTracking.h
--- a/js/src/jit/OptimizationTracking.h
+++ b/js/src/jit/OptimizationTracking.h
@@ -15,17 +15,22 @@
 #include "jit/JitSpewer.h"
 #include "js/TrackedOptimizationInfo.h"
 #include "vm/TypeInference.h"
 
 namespace js {
 
 namespace jit {
 
-struct NativeToTrackedOptimizations;
+struct NativeToTrackedOptimizations {
+  // [startOffset, endOffset]
+  CodeOffset startOffset;
+  CodeOffset endOffset;
+  const TrackedOptimizations* optimizations;
+};
 
 class OptimizationAttempt {
   JS::TrackedStrategy strategy_;
   JS::TrackedOutcome outcome_;
 
  public:
   OptimizationAttempt(JS::TrackedStrategy strategy, JS::TrackedOutcome outcome)
       : strategy_(strategy), outcome_(outcome) {}
diff --git a/js/src/jit/shared/CodeGenerator-shared.h b/js/src/jit/shared/CodeGenerator-shared.h
--- a/js/src/jit/shared/CodeGenerator-shared.h
+++ b/js/src/jit/shared/CodeGenerator-shared.h
@@ -6,16 +6,17 @@
 
 #ifndef jit_shared_CodeGenerator_shared_h
 #define jit_shared_CodeGenerator_shared_h
 
 #include "mozilla/Alignment.h"
 #include "mozilla/Move.h"
 #include "mozilla/TypeTraits.h"
 
+#include "jit/JitcodeMap.h"
 #include "jit/JitFrames.h"
 #include "jit/LIR.h"
 #include "jit/MacroAssembler.h"
 #include "jit/MIRGenerator.h"
 #include "jit/MIRGraph.h"
 #include "jit/OptimizationTracking.h"
 #include "jit/Safepoints.h"
 #include "jit/Snapshots.h"
@@ -34,26 +35,16 @@ class OutOfLineCallVM;
 
 class OutOfLineTruncateSlow;
 
 struct ReciprocalMulConstants {
   int64_t multiplier;
   int32_t shiftAmount;
 };
 
-// This should be nested in CodeGeneratorShared, but it is used in
-// optimization tracking implementation and nested classes cannot be
-// forward-declared.
-struct NativeToTrackedOptimizations {
-  // [startOffset, endOffset]
-  CodeOffset startOffset;
-  CodeOffset endOffset;
-  const TrackedOptimizations* optimizations;
-};
-
 class CodeGeneratorShared : public LElementVisitor {
   js::Vector<OutOfLineCode*, 0, SystemAllocPolicy> outOfLineCode_;
 
   MacroAssembler& ensureMasm(MacroAssembler* masm);
   mozilla::Maybe<IonHeapMacroAssembler> maybeMasm_;
 
  public:
   MacroAssembler& masm;
@@ -103,23 +94,16 @@ class CodeGeneratorShared : public LElem
     const char* event;
     PatchableTLEvent(CodeOffset offset, const char* event)
         : offset(offset), event(event) {}
   };
   js::Vector<PatchableTLEvent, 0, SystemAllocPolicy> patchableTLEvents_;
   js::Vector<CodeOffset, 0, SystemAllocPolicy> patchableTLScripts_;
 #endif
 
- public:
-  struct NativeToBytecode {
-    CodeOffset nativeOffset;
-    InlineScriptTree* tree;
-    jsbytecode* pc;
-  };
-
  protected:
   js::Vector<NativeToBytecode, 0, SystemAllocPolicy> nativeToBytecodeList_;
   uint8_t* nativeToBytecodeMap_;
   uint32_t nativeToBytecodeMapSize_;
   uint32_t nativeToBytecodeTableOffset_;
   uint32_t nativeToBytecodeNumRegions_;
 
   JSScript** nativeToBytecodeScriptList_;
diff --git a/js/src/shell/js.cpp b/js/src/shell/js.cpp
--- a/js/src/shell/js.cpp
+++ b/js/src/shell/js.cpp
@@ -70,24 +70,24 @@
 #include "frontend/BinSource.h"
 #endif  // defined(JS_BUILD_BINAST)
 #include "frontend/Parser.h"
 #include "gc/PublicIterators.h"
 #include "jit/arm/Simulator-arm.h"
 #include "jit/InlinableNatives.h"
 #include "jit/Ion.h"
 #include "jit/JitcodeMap.h"
-#include "jit/OptimizationTracking.h"
+#include "jit/JitRealm.h"
+#include "jit/shared/CodeGenerator-shared.h"
 #include "js/Debug.h"
 #include "js/GCVector.h"
 #include "js/Initialization.h"
 #include "js/Printf.h"
 #include "js/StructuredClone.h"
 #include "js/SweepingAPI.h"
-#include "js/TrackedOptimizationInfo.h"
 #include "js/Wrapper.h"
 #include "perf/jsperf.h"
 #include "shell/jsoptparse.h"
 #include "shell/jsshell.h"
 #include "shell/OSObject.h"
 #include "threading/ConditionVariable.h"
 #include "threading/ExclusiveData.h"
 #include "threading/LockGuard.h"
diff --git a/js/src/vm/GeckoProfiler.cpp b/js/src/vm/GeckoProfiler.cpp
--- a/js/src/vm/GeckoProfiler.cpp
+++ b/js/src/vm/GeckoProfiler.cpp
@@ -5,21 +5,23 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "vm/GeckoProfiler-inl.h"
 
 #include "mozilla/DebugOnly.h"
 
 #include "jsnum.h"
 
+#include "gc/GC.h"
 #include "gc/PublicIterators.h"
 #include "jit/BaselineFrame.h"
 #include "jit/BaselineJIT.h"
 #include "jit/JitcodeMap.h"
 #include "jit/JitFrames.h"
+#include "jit/JitRealm.h"
 #include "jit/JSJitFrameIter.h"
 #include "util/StringBuffer.h"
 #include "vm/JSScript.h"
 
 #include "gc/Marking-inl.h"
 
 using namespace js;
 
diff --git a/js/src/vm/Stack.cpp b/js/src/vm/Stack.cpp
--- a/js/src/vm/Stack.cpp
+++ b/js/src/vm/Stack.cpp
@@ -7,16 +7,17 @@
 #include "vm/Stack-inl.h"
 
 #include <utility>
 
 #include "gc/Marking.h"
 #include "jit/BaselineFrame.h"
 #include "jit/JitcodeMap.h"
 #include "jit/JitRealm.h"
+#include "jit/shared/CodeGenerator-shared.h"
 #include "vm/Debugger.h"
 #include "vm/JSContext.h"
 #include "vm/Opcodes.h"
 
 #include "jit/JSJitFrameIter-inl.h"
 #include "vm/Compartment-inl.h"
 #include "vm/EnvironmentObject-inl.h"
 #include "vm/Interpreter-inl.h"
