# HG changeset patch
# User Ted Campbell <tcampbell@mozilla.com>
# Date 1523473065 14400
# Node ID d873c3d7b7bfebb7fb4590ce0e9010e606c69ef9
# Parent  d082b7f7d345d17f8de2e29d6af1445624a7eb29
Bug 1448563 - Part 4: Add memory reporting for off-thread parse. r=jandem

MozReview-Commit-ID: 2qH6cwFRrfG

diff --git a/js/public/MemoryMetrics.h b/js/public/MemoryMetrics.h
--- a/js/public/MemoryMetrics.h
+++ b/js/public/MemoryMetrics.h
@@ -458,17 +458,18 @@ struct NotableScriptSourceInfo : public 
   char* filename_;
 
  private:
   NotableScriptSourceInfo(const NotableScriptSourceInfo& info) = delete;
 };
 
 struct HelperThreadStats {
 #define FOR_EACH_SIZE(MACRO) \
-  MACRO(_, MallocHeap, stateData)
+  MACRO(_, MallocHeap, stateData) \
+  MACRO(_, MallocHeap, parseTask)
 
   explicit HelperThreadStats()
       : FOR_EACH_SIZE(ZERO_SIZE)
         idleThreadCount(0),
         activeThreadCount(0) {}
 
   FOR_EACH_SIZE(DECL_SIZE)
 
diff --git a/js/src/jsapi.cpp b/js/src/jsapi.cpp
--- a/js/src/jsapi.cpp
+++ b/js/src/jsapi.cpp
@@ -3422,16 +3422,23 @@ JS::OwningCompileOptions::OwningCompileO
 
 JS::OwningCompileOptions::~OwningCompileOptions() {
   // OwningCompileOptions always owns these, so these casts are okay.
   js_free(const_cast<char*>(filename_));
   js_free(const_cast<char16_t*>(sourceMapURL_));
   js_free(const_cast<char*>(introducerFilename_));
 }
 
+size_t JS::OwningCompileOptions::sizeOfExcludingThis(
+    mozilla::MallocSizeOf mallocSizeOf) const {
+  return mallocSizeOf(filename_) +
+         mallocSizeOf(sourceMapURL_) +
+         mallocSizeOf(introducerFilename_);
+}
+
 bool JS::OwningCompileOptions::copy(JSContext* cx,
                                     const ReadOnlyCompileOptions& rhs) {
   copyPODOptions(rhs);
 
   setElement(rhs.element());
   setElementAttributeName(rhs.elementAttributeName());
   setIntroductionScript(rhs.introductionScript());
 
diff --git a/js/src/jsapi.h b/js/src/jsapi.h
--- a/js/src/jsapi.h
+++ b/js/src/jsapi.h
@@ -3716,16 +3716,18 @@ class JS_FRIEND_API OwningCompileOptions
     introductionType = intro;
     introductionLineno = line;
     introductionScriptRoot = script;
     introductionOffset = offset;
     hasIntroductionInfo = true;
     return true;
   }
 
+  size_t sizeOfExcludingThis(mozilla::MallocSizeOf mallocSizeOf) const;
+
  private:
   void operator=(const CompileOptions& rhs) = delete;
 };
 
 /**
  * Compilation options stored on the stack. An instance of this type
  * simply holds references to dynamically allocated resources (element;
  * filename; source map URL) that are owned by something else. If you
diff --git a/js/src/vm/HelperThreads.cpp b/js/src/vm/HelperThreads.cpp
--- a/js/src/vm/HelperThreads.cpp
+++ b/js/src/vm/HelperThreads.cpp
@@ -429,16 +429,23 @@ void ParseTask::trace(JSTracer* trc) {
     return;
   }
 
   TraceManuallyBarrieredEdge(trc, &parseGlobal, "ParseTask::parseGlobal");
   scripts.trace(trc);
   sourceObjects.trace(trc);
 }
 
+size_t ParseTask::sizeOfExcludingThis(
+    mozilla::MallocSizeOf mallocSizeOf) const {
+  return options.sizeOfExcludingThis(mallocSizeOf) +
+         alloc.sizeOfExcludingThis(mallocSizeOf) +
+         errors.sizeOfExcludingThis(mallocSizeOf);
+}
+
 ScriptParseTask::ScriptParseTask(JSContext* cx, const char16_t* chars,
                                  size_t length,
                                  JS::OffThreadCompileCallback callback,
                                  void* callbackData)
     : ParseTask(ParseTaskKind::Script, cx, callback, callbackData),
       data(TwoByteChars(chars, length)) {}
 
 void ScriptParseTask::parse(JSContext* cx) {
@@ -1043,16 +1050,24 @@ void GlobalHelperThreadState::addSizeOfI
       parseFinishedList_.sizeOfExcludingThis(mallocSizeOf) +
       parseWaitingOnGC_.sizeOfExcludingThis(mallocSizeOf) +
       compressionPendingList_.sizeOfExcludingThis(mallocSizeOf) +
       compressionWorklist_.sizeOfExcludingThis(mallocSizeOf) +
       compressionFinishedList_.sizeOfExcludingThis(mallocSizeOf) +
       gcHelperWorklist_.sizeOfExcludingThis(mallocSizeOf) +
       gcParallelWorklist_.sizeOfExcludingThis(mallocSizeOf);
 
+  // Report ParseTasks on wait lists
+  for (auto task : parseWorklist_)
+    htStats.parseTask += task->sizeOfIncludingThis(mallocSizeOf);
+  for (auto task : parseFinishedList_)
+    htStats.parseTask += task->sizeOfIncludingThis(mallocSizeOf);
+  for (auto task : parseWaitingOnGC_)
+    htStats.parseTask += task->sizeOfIncludingThis(mallocSizeOf);
+
   // Report number of helper threads.
   MOZ_ASSERT(htStats.idleThreadCount == 0);
   if (threads) {
     for (auto& thread : *threads) {
       if (thread.idle())
         htStats.idleThreadCount++;
       else
         htStats.activeThreadCount++;
diff --git a/js/src/vm/HelperThreads.h b/js/src/vm/HelperThreads.h
--- a/js/src/vm/HelperThreads.h
+++ b/js/src/vm/HelperThreads.h
@@ -668,16 +668,21 @@ struct ParseTask {
   virtual void parse(JSContext* cx) = 0;
   bool finish(JSContext* cx);
 
   bool runtimeMatches(JSRuntime* rt) {
     return parseGlobal->runtimeFromAnyThread() == rt;
   }
 
   void trace(JSTracer* trc);
+
+  size_t sizeOfExcludingThis(mozilla::MallocSizeOf mallocSizeOf) const;
+  size_t sizeOfIncludingThis(mozilla::MallocSizeOf mallocSizeOf) const {
+    return mallocSizeOf(this) + sizeOfExcludingThis(mallocSizeOf);
+  }
 };
 
 struct ScriptParseTask : public ParseTask {
   JS::TwoByteChars data;
 
   ScriptParseTask(JSContext* cx, const char16_t* chars, size_t length,
                   JS::OffThreadCompileCallback callback, void* callbackData);
   void parse(JSContext* cx) override;
diff --git a/js/xpconnect/src/XPCJSRuntime.cpp b/js/xpconnect/src/XPCJSRuntime.cpp
--- a/js/xpconnect/src/XPCJSRuntime.cpp
+++ b/js/xpconnect/src/XPCJSRuntime.cpp
@@ -2346,16 +2346,21 @@ void JSReporter::CollectReports(WindowPa
       "The memory used for the tracelogger, including the graph and events.");
 
   // Report HelperThreadState.
 
   REPORT_BYTES(
       NS_LITERAL_CSTRING("explicit/js-non-window/helper-thread/heap-other"),
       KIND_HEAP, gStats.helperThread.stateData,
       "Memory used by HelperThreadState.");
+
+  REPORT_BYTES(
+      NS_LITERAL_CSTRING("explicit/js-non-window/helper-thread/parse-task"),
+      KIND_HEAP, gStats.helperThread.parseTask,
+      "The memory used by ParseTasks waiting in HelperThreadState.");
 }
 
 static nsresult JSSizeOfTab(JSObject* objArg, size_t* jsObjectsSize,
                             size_t* jsStringsSize, size_t* jsPrivateSize,
                             size_t* jsOtherSize) {
   JSContext* cx = XPCJSContext::Get()->Context();
   JS::RootedObject obj(cx, objArg);
 
