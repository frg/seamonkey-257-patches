# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1545350732 0
# Node ID 2cb3a85f0a64c571495768b9a432c4210eb85fbc
# Parent  28c8818e06b81f0ee302a6fec2582902bfaa1741
Bug 1515577 - Don't quote shell strings when they contain a ~, except in first position. r=ted

We only need to quote strings that would be treated specially by the
shell, and "foo~bar" doesn't get any sort of expansion, while "~foo"
gets a user expansion, and to avoid that expansion, those latter strings
need to be quoted, but not the former.

Differential Revision: https://phabricator.services.mozilla.com/D15065

diff --git a/python/mozbuild/mozbuild/shellutil.py b/python/mozbuild/mozbuild/shellutil.py
--- a/python/mozbuild/mozbuild/shellutil.py
+++ b/python/mozbuild/mozbuild/shellutil.py
@@ -36,17 +36,17 @@ DOUBLY_QUOTED_TOKENS_RE = _tokens2re(
   special='\$',
   backslashed=r'\\[^\\"]',
 )
 
 ESCAPED_NEWLINES_RE = re.compile(r'\\\n')
 
 # This regexp contains the same characters as all those listed in
 # UNQUOTED_TOKENS_RE. Please keep in sync.
-SHELL_QUOTE_RE = re.compile(r'[\\\t\r\n \'\"#<>&|`~(){}$;\*\?]')
+SHELL_QUOTE_RE = re.compile(r'[\\\t\r\n \'\"#<>&|`(){}$;\*\?]')
 
 
 class MetaCharacterException(Exception):
     def __init__(self, char):
         self.char = char
 
 
 class _ClineSplitter(object):
@@ -179,17 +179,17 @@ def _quote(s):
 
     As a special case, if given an int, returns a string containing the int,
     not enclosed in quotes.
     '''
     if type(s) == int:
         return '%d' % s
 
     # Empty strings need to be quoted to have any significance
-    if s and not SHELL_QUOTE_RE.search(s):
+    if s and not SHELL_QUOTE_RE.search(s) and not s.startswith('~'):
         return s
 
     # Single quoted strings can contain any characters unescaped except the
     # single quote itself, which can't even be escaped, so the string needs to
     # be closed, an escaped single quote added, and reopened.
     t = type(s)
     return t("'%s'") % s.replace(t("'"), t("'\\''"))
 
diff --git a/python/mozbuild/mozbuild/test/configure/test_checks_configure.py b/python/mozbuild/mozbuild/test/configure/test_checks_configure.py
--- a/python/mozbuild/mozbuild/test/configure/test_checks_configure.py
+++ b/python/mozbuild/mozbuild/test/configure/test_checks_configure.py
@@ -15,16 +15,17 @@ from mozunit import (
     MockedOpen,
 )
 
 from mozbuild.configure import (
     ConfigureError,
     ConfigureSandbox,
 )
 from mozbuild.util import exec_
+from mozbuild.shellutil import quote as shell_quote
 from mozpack import path as mozpath
 
 from buildconfig import topsrcdir
 from common import (
     ConfigureTestSandbox,
     ensure_exe_extension,
     fake_short_path,
 )
@@ -179,18 +180,18 @@ class TestChecksConfigure(unittest.TestC
         self.assertEqual(status, 0)
         self.assertEqual(config, {'FOO': self.KNOWN_B})
         self.assertEqual(out, 'checking for foo... %s\n' % self.KNOWN_B)
 
         config, out, status = self.get_result(
             'check_prog("FOO", ("unknown", "unknown-2", "known c"))')
         self.assertEqual(status, 0)
         self.assertEqual(config, {'FOO': fake_short_path(self.KNOWN_C)})
-        self.assertEqual(out, "checking for foo... '%s'\n"
-                              % fake_short_path(self.KNOWN_C))
+        self.assertEqual(out, "checking for foo... %s\n"
+                              % shell_quote(fake_short_path(self.KNOWN_C)))
 
         config, out, status = self.get_result(
             'check_prog("FOO", ("unknown",))')
         self.assertEqual(status, 1)
         self.assertEqual(config, {})
         self.assertEqual(out, textwrap.dedent('''\
             checking for foo... not found
             DEBUG: foo: Trying unknown
@@ -260,18 +261,18 @@ class TestChecksConfigure(unittest.TestC
             ERROR: Cannot find foo
         ''') % path)
 
         config, out, status = self.get_result(
             'check_prog("FOO", ("unknown",))',
             ['FOO=known c'])
         self.assertEqual(status, 0)
         self.assertEqual(config, {'FOO': fake_short_path(self.KNOWN_C)})
-        self.assertEqual(out, "checking for foo... '%s'\n"
-                              % fake_short_path(self.KNOWN_C))
+        self.assertEqual(out, "checking for foo... %s\n"
+                              % shell_quote(fake_short_path(self.KNOWN_C)))
 
         config, out, status = self.get_result(
             'check_prog("FOO", ("unknown", "unknown-2", "unknown 3"), '
             'allow_missing=True)', ['FOO=unknown'])
         self.assertEqual(status, 1)
         self.assertEqual(config, {})
         self.assertEqual(out, textwrap.dedent('''\
             checking for foo... not found

