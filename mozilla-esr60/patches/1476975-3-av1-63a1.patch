# HG changeset patch
# User Jean-Yves Avenard <jyavenard@mozilla.com>
# Date 1532075096 -7200
# Node ID 32bdeb5086b0a968fdb6cb268281c14f318d4825
# Parent  a773328deeb832450663ab9848191e9fe71f425f
Bug 1476975 - P3. Fix canPlayType/isTypeSupported for AV1 content. r=dminor

AV1 support is behind a pref, as such, the result of canPlayType should depends on the value of that pref.

Additionally to this change we remove AOMDecoder::IsSupportedCodec as it implied confusion on what a codec mimetype is. There are two type of codec mimetype: the one describing the container content ("av1") and the one describing the codec itself "video/av1")
AOMDecoder shouldn't know anything about containers (e.g. mp4 or webm)

diff --git a/dom/media/VideoUtils.cpp b/dom/media/VideoUtils.cpp
--- a/dom/media/VideoUtils.cpp
+++ b/dom/media/VideoUtils.cpp
@@ -634,16 +634,21 @@ bool IsVP9CodecString(const nsAString& a
   uint8_t profile = 0;
   uint8_t level = 0;
   uint8_t bitDepth = 0;
   return aCodec.EqualsLiteral("vp9") || aCodec.EqualsLiteral("vp9.0") ||
          (StartsWith(NS_ConvertUTF16toUTF8(aCodec), "vp09") &&
           ExtractVPXCodecDetails(aCodec, profile, level, bitDepth));
 }
 
+bool IsAV1CodecString(const nsAString& aCodec)
+{
+  return aCodec.EqualsLiteral("av1"); // AV1
+}
+
 UniquePtr<TrackInfo> CreateTrackInfoWithMIMEType(
     const nsACString& aCodecMIMEType) {
   UniquePtr<TrackInfo> trackInfo;
   if (StartsWith(aCodecMIMEType, "audio/")) {
     trackInfo.reset(new AudioInfo());
     trackInfo->mMimeType = aCodecMIMEType;
   } else if (StartsWith(aCodecMIMEType, "video/")) {
     trackInfo.reset(new VideoInfo());
diff --git a/dom/media/VideoUtils.h b/dom/media/VideoUtils.h
--- a/dom/media/VideoUtils.h
+++ b/dom/media/VideoUtils.h
@@ -322,16 +322,18 @@ bool ParseCodecsString(const nsAString& 
 bool IsH264CodecString(const nsAString& aCodec);
 
 bool IsAACCodecString(const nsAString& aCodec);
 
 bool IsVP8CodecString(const nsAString& aCodec);
 
 bool IsVP9CodecString(const nsAString& aCodec);
 
+bool IsAV1CodecString(const nsAString& aCodec);
+
 // Try and create a TrackInfo with a given codec MIME type.
 UniquePtr<TrackInfo> CreateTrackInfoWithMIMEType(
     const nsACString& aCodecMIMEType);
 
 // Try and create a TrackInfo with a given codec MIME type, and optional extra
 // parameters from a container type (its MIME type and codecs are ignored).
 UniquePtr<TrackInfo> CreateTrackInfoWithMIMETypeAndContainerTypeExtraParameters(
     const nsACString& aCodecMIMEType, const MediaContainerType& aContainerType);
diff --git a/dom/media/mediasource/MediaSource.cpp b/dom/media/mediasource/MediaSource.cpp
--- a/dom/media/mediasource/MediaSource.cpp
+++ b/dom/media/mediasource/MediaSource.cpp
@@ -1,19 +1,16 @@
 /* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "MediaSource.h"
 
-#if MOZ_AV1
-#include "AOMDecoder.h"
-#endif
 #include "AsyncEventRunner.h"
 #include "DecoderTraits.h"
 #include "Benchmark.h"
 #include "DecoderDoctorDiagnostics.h"
 #include "MediaContainerType.h"
 #include "MediaResult.h"
 #include "MediaSourceDemuxer.h"
 #include "MediaSourceUtils.h"
@@ -117,20 +114,19 @@ nsresult MediaSource::IsTypeSupported(co
     }
     return NS_OK;
   }
   if (mimeType == MEDIAMIMETYPE("video/webm")) {
     if (!(Preferences::GetBool("media.mediasource.webm.enabled", false) ||
           containerType->ExtendedType().Codecs().Contains(
               NS_LITERAL_STRING("vp8")) ||
 #ifdef MOZ_AV1
-          // FIXME: Temporary comparison with the full codecs attribute.
-          // See bug 1377015.
-          AOMDecoder::IsSupportedCodec(
-              containerType->ExtendedType().Codecs().AsString()) ||
+          (Preferences::GetBool("media.av1.enabled", true) &&
+           IsAV1CodecString(
+             containerType->ExtendedType().Codecs().AsString())) ||
 #endif
           IsWebMForced(aDiagnostics))) {
       return NS_ERROR_DOM_NOT_SUPPORTED_ERR;
     }
     return NS_OK;
   }
   if (mimeType == MEDIAMIMETYPE("audio/webm")) {
     if (!(Preferences::GetBool("media.mediasource.webm.enabled", false) ||
diff --git a/dom/media/platforms/agnostic/AOMDecoder.cpp b/dom/media/platforms/agnostic/AOMDecoder.cpp
--- a/dom/media/platforms/agnostic/AOMDecoder.cpp
+++ b/dom/media/platforms/agnostic/AOMDecoder.cpp
@@ -296,21 +296,16 @@ RefPtr<MediaDataDecoder::DecodePromise> 
 }
 
 /* static */
 bool AOMDecoder::IsAV1(const nsACString& aMimeType) {
   return aMimeType.EqualsLiteral("video/av1");
 }
 
 /* static */
-bool AOMDecoder::IsSupportedCodec(const nsAString& aCodecType) {
-  return aCodecType.EqualsLiteral("av1");
-}
-
-/* static */
 bool AOMDecoder::IsKeyframe(Span<const uint8_t> aBuffer) {
   aom_codec_stream_info_t info;
   PodZero(&info);
 
   auto res = aom_codec_peek_stream_info(aom_codec_av1_dx(), aBuffer.Elements(),
                                         aBuffer.Length(), &info);
   if (res != AOM_CODEC_OK) {
     LOG_STATIC_RESULT(
diff --git a/dom/media/platforms/agnostic/AOMDecoder.h b/dom/media/platforms/agnostic/AOMDecoder.h
--- a/dom/media/platforms/agnostic/AOMDecoder.h
+++ b/dom/media/platforms/agnostic/AOMDecoder.h
@@ -29,19 +29,16 @@ class AOMDecoder : public MediaDataDecod
   nsCString GetDescriptionName() const override {
     return NS_LITERAL_CSTRING("av1 libaom video decoder");
   }
 
   // Return true if aMimeType is a one of the strings used
   // by our demuxers to identify AV1 streams.
   static bool IsAV1(const nsACString& aMimeType);
 
-  // Return true if aCodecType is a supported codec description.
-  static bool IsSupportedCodec(const nsAString& aCodecType);
-
   // Return true if a sample is a keyframe.
   static bool IsKeyframe(Span<const uint8_t> aBuffer);
 
   // Return the frame dimensions for a sample.
   static gfx::IntSize GetFrameSize(Span<const uint8_t> aBuffer);
 
  private:
   ~AOMDecoder();
diff --git a/dom/media/webm/WebMDecoder.cpp b/dom/media/webm/WebMDecoder.cpp
--- a/dom/media/webm/WebMDecoder.cpp
+++ b/dom/media/webm/WebMDecoder.cpp
@@ -64,18 +64,18 @@ bool WebMDecoder::IsSupportedType(const 
           if (!platform->Supports(*trackInfo, nullptr)) {
             return false;
           }
         }
         continue;
       }
     }
 #ifdef MOZ_AV1
-    if (isVideo && MediaPrefs::AV1Enabled() &&
-       IsAV1CodecString(codec)) { 
+    if (isVideo &&
+        MediaPrefs::AV1Enabled() && IsAV1CodecString(codec)) {
       continue;
     }
 #endif
     // Some unsupported codec.
     return false;
   }
   return true;
 }
