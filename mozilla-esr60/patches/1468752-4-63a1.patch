# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1530874440 -7200
# Node ID cea2eefe9e5dc7e5bcf29bdf995cc1cb661e1311
# Parent  26b95b7546f8c0bb98b577e433adfa9e12845262
Bug 1468752 part 4 - Remove JS_GetGlobalForObject. r=bz

diff --git a/dom/base/WindowNamedPropertiesHandler.cpp b/dom/base/WindowNamedPropertiesHandler.cpp
--- a/dom/base/WindowNamedPropertiesHandler.cpp
+++ b/dom/base/WindowNamedPropertiesHandler.cpp
@@ -93,18 +93,17 @@ bool WindowNamedPropertiesHandler::getOw
     return false;
   }
 
   if (str.IsEmpty()) {
     return true;
   }
 
   // Grab the DOM window.
-  JS::Rooted<JSObject*> global(aCx, JS_GetGlobalForObject(aCx, aProxy));
-  nsGlobalWindowInner* win = xpc::WindowOrNull(global);
+  nsGlobalWindowInner* win = xpc::WindowGlobalOrNull(aProxy);
   if (win->Length() > 0) {
     nsCOMPtr<nsPIDOMWindowOuter> childWin = win->GetChildWindow(str);
     if (childWin && ShouldExposeChildWindow(str, childWin)) {
       // We found a subframe of the right name. Shadowing via |var foo| in
       // global scope is still allowed, since |var| only looks up |own|
       // properties. But unqualified shadowing will fail, per-spec.
       JS::Rooted<JS::Value> v(aCx);
       if (!WrapObject(aCx, childWin, &v)) {
@@ -160,18 +159,17 @@ bool WindowNamedPropertiesHandler::ownPr
     JSContext* aCx, JS::Handle<JSObject*> aProxy, unsigned flags,
     JS::AutoIdVector& aProps) const {
   if (!(flags & JSITER_HIDDEN)) {
     // None of our named properties are enumerable.
     return true;
   }
 
   // Grab the DOM window.
-  nsGlobalWindowInner* win =
-      xpc::WindowOrNull(JS_GetGlobalForObject(aCx, aProxy));
+  nsGlobalWindowInner* win = xpc::WindowGlobalOrNull(aProxy);
   nsTArray<nsString> names;
   // The names live on the outer window, which might be null
   nsGlobalWindowOuter* outer = win->GetOuterWindowInternal();
   if (outer) {
     nsDOMWindowList* childWindows = outer->GetWindowList();
     if (childWindows) {
       uint32_t length = childWindows->GetLength();
       for (uint32_t i = 0; i < length; ++i) {
diff --git a/dom/base/nsDocument.cpp b/dom/base/nsDocument.cpp
--- a/dom/base/nsDocument.cpp
+++ b/dom/base/nsDocument.cpp
@@ -2416,22 +2416,17 @@ bool nsDocument::IsSynthesized() {
   if (internalChan) {
     DebugOnly<nsresult> rv = internalChan->GetResponseSynthesized(&synthesized);
     MOZ_ASSERT(NS_SUCCEEDED(rv), "GetResponseSynthesized shouldn't fail.");
   }
   return synthesized;
 }
 
 bool nsDocument::IsShadowDOMEnabled(JSContext* aCx, JSObject* aObject) {
-  JS::Rooted<JSObject*> obj(aCx, aObject);
-
-  JSAutoRealm ar(aCx, obj);
-  JS::Rooted<JSObject*> global(aCx, JS_GetGlobalForObject(aCx, obj));
-  nsCOMPtr<nsPIDOMWindowInner> window =
-      do_QueryInterface(nsJSUtils::GetStaticScriptGlobal(global));
+  nsCOMPtr<nsPIDOMWindowInner> window = xpc::WindowGlobalOrNull(aObject);
 
   nsIDocument* doc = window ? window->GetExtantDoc() : nullptr;
   if (!doc) {
     return false;
   }
 
   return doc->IsShadowDOMEnabled();
 }
diff --git a/dom/indexedDB/IDBFactory.cpp b/dom/indexedDB/IDBFactory.cpp
--- a/dom/indexedDB/IDBFactory.cpp
+++ b/dom/indexedDB/IDBFactory.cpp
@@ -231,18 +231,17 @@ nsresult IDBFactory::CreateForJSInternal
     JSContext* aCx, JS::Handle<JSObject*> aOwningObject,
     nsAutoPtr<PrincipalInfo>& aPrincipalInfo, uint64_t aInnerWindowID,
     IDBFactory** aFactory) {
   MOZ_ASSERT(aCx);
   MOZ_ASSERT(aOwningObject);
   MOZ_ASSERT(aPrincipalInfo);
   MOZ_ASSERT(aPrincipalInfo->type() != PrincipalInfo::T__None);
   MOZ_ASSERT(aFactory);
-  MOZ_ASSERT(JS_GetGlobalForObject(aCx, aOwningObject) == aOwningObject,
-             "Not a global object!");
+  MOZ_ASSERT(JS_IsGlobalObject(aOwningObject));
 
   if (aPrincipalInfo->type() != PrincipalInfo::TContentPrincipalInfo &&
       aPrincipalInfo->type() != PrincipalInfo::TSystemPrincipalInfo) {
     NS_WARNING("IndexedDB not allowed for this principal!");
     aPrincipalInfo = nullptr;
     *aFactory = nullptr;
     return NS_OK;
   }
diff --git a/dom/xbl/nsXBLProtoImplField.cpp b/dom/xbl/nsXBLProtoImplField.cpp
--- a/dom/xbl/nsXBLProtoImplField.cpp
+++ b/dom/xbl/nsXBLProtoImplField.cpp
@@ -267,17 +267,17 @@ static bool FieldSetter(JSContext* cx, u
   return JS::CallNonGenericMethod<ValueHasISupportsPrivate, FieldSetterImpl>(
       cx, args);
 }
 
 nsresult nsXBLProtoImplField::InstallAccessors(
     JSContext* aCx, JS::Handle<JSObject*> aTargetClassObject) {
   MOZ_ASSERT(js::IsObjectInContextCompartment(aTargetClassObject, aCx));
   JS::Rooted<JSObject*> globalObject(
-      aCx, JS_GetGlobalForObject(aCx, aTargetClassObject));
+      aCx, JS::GetNonCCWObjectGlobal(aTargetClassObject));
   JS::Rooted<JSObject*> scopeObject(
       aCx, xpc::GetXBLScopeOrGlobal(aCx, globalObject));
   NS_ENSURE_TRUE(scopeObject, NS_ERROR_OUT_OF_MEMORY);
 
   // Don't install it if the field is empty; see also InstallField which also
   // must implement the not-empty requirement.
   if (IsEmpty()) {
     return NS_OK;
diff --git a/dom/xbl/nsXBLProtoImplMethod.cpp b/dom/xbl/nsXBLProtoImplMethod.cpp
--- a/dom/xbl/nsXBLProtoImplMethod.cpp
+++ b/dom/xbl/nsXBLProtoImplMethod.cpp
@@ -86,17 +86,17 @@ nsresult nsXBLProtoImplMethod::InstallMe
     JSContext* aCx, JS::Handle<JSObject*> aTargetClassObject) {
   NS_PRECONDITION(IsCompiled(),
                   "Should not be installing an uncompiled method");
   MOZ_ASSERT(js::IsObjectInContextCompartment(aTargetClassObject, aCx));
 
 #ifdef DEBUG
   {
     JS::Rooted<JSObject*> globalObject(
-        aCx, JS_GetGlobalForObject(aCx, aTargetClassObject));
+        aCx, JS::GetNonCCWObjectGlobal(aTargetClassObject));
     MOZ_ASSERT(xpc::IsInContentXBLScope(globalObject) ||
                globalObject == xpc::GetXBLScope(aCx, globalObject));
     MOZ_ASSERT(JS::CurrentGlobalOrNull(aCx) == globalObject);
   }
 #endif
 
   JS::Rooted<JSObject*> jsMethodObject(aCx, GetCompiledMethod());
   if (jsMethodObject) {
diff --git a/dom/xbl/nsXBLProtoImplProperty.cpp b/dom/xbl/nsXBLProtoImplProperty.cpp
--- a/dom/xbl/nsXBLProtoImplProperty.cpp
+++ b/dom/xbl/nsXBLProtoImplProperty.cpp
@@ -118,17 +118,17 @@ nsresult nsXBLProtoImplProperty::Install
   NS_PRECONDITION(mIsCompiled,
                   "Should not be installing an uncompiled property");
   MOZ_ASSERT(mGetter.IsCompiled() && mSetter.IsCompiled());
   MOZ_ASSERT(js::IsObjectInContextCompartment(aTargetClassObject, aCx));
 
 #ifdef DEBUG
   {
     JS::Rooted<JSObject*> globalObject(
-        aCx, JS_GetGlobalForObject(aCx, aTargetClassObject));
+        aCx, JS::GetNonCCWObjectGlobal(aTargetClassObject));
     MOZ_ASSERT(xpc::IsInContentXBLScope(globalObject) ||
                globalObject == xpc::GetXBLScope(aCx, globalObject));
     MOZ_ASSERT(JS::CurrentGlobalOrNull(aCx) == globalObject);
   }
 #endif
 
   JS::Rooted<JSObject*> getter(aCx, mGetter.GetJSFunction());
   JS::Rooted<JSObject*> setter(aCx, mSetter.GetJSFunction());
diff --git a/js/ductwork/debugger/JSDebugger.cpp b/js/ductwork/debugger/JSDebugger.cpp
--- a/js/ductwork/debugger/JSDebugger.cpp
+++ b/js/ductwork/debugger/JSDebugger.cpp
@@ -44,21 +44,21 @@ JSDebugger::AddClass(JS::Handle<JS::Valu
   }
 
   JS::RootedObject obj(cx, &global.toObject());
   obj = js::UncheckedUnwrap(obj, /* stopAtWindowProxy = */ false);
   if (!obj) {
     return NS_ERROR_FAILURE;
   }
 
-  JSAutoRealm ar(cx, obj);
-  if (JS_GetGlobalForObject(cx, obj) != obj) {
+  if (!JS_IsGlobalObject(obj)) {
     return NS_ERROR_INVALID_ARG;
   }
 
+  JSAutoRealm ar(cx, obj);
   if (!JS_DefineDebuggerObject(cx, obj)) {
     return NS_ERROR_FAILURE;
   }
 
   return NS_OK;
 }
 
 }  // namespace jsdebugger
diff --git a/js/src/jsapi.cpp b/js/src/jsapi.cpp
--- a/js/src/jsapi.cpp
+++ b/js/src/jsapi.cpp
@@ -1014,22 +1014,16 @@ JS_PUBLIC_API JSProtoKey JS_IdToProtoKey
 
   if (GlobalObject::skipDeselectedConstructor(cx, stdnm->key))
     return JSProto_Null;
 
   MOZ_ASSERT(MOZ_ARRAY_LENGTH(standard_class_names) == JSProto_LIMIT + 1);
   return static_cast<JSProtoKey>(stdnm - standard_class_names);
 }
 
-JS_PUBLIC_API JSObject* JS_GetGlobalForObject(JSContext* cx, JSObject* obj) {
-  AssertHeapIsIdle();
-  assertSameCompartment(cx, obj);
-  return &obj->deprecatedGlobal();
-}
-
 extern JS_PUBLIC_API bool JS_IsGlobalObject(JSObject* obj) {
   return obj->is<GlobalObject>();
 }
 
 extern JS_PUBLIC_API JSObject* JS_GlobalLexicalEnvironment(JSObject* obj) {
   return &obj->as<GlobalObject>().lexicalEnvironment();
 }
 
diff --git a/js/src/jsapi.h b/js/src/jsapi.h
--- a/js/src/jsapi.h
+++ b/js/src/jsapi.h
@@ -1144,19 +1144,16 @@ extern JS_PUBLIC_API JSProtoKey Identify
 
 extern JS_PUBLIC_API void ProtoKeyToId(JSContext* cx, JSProtoKey key,
                                        JS::MutableHandleId idp);
 
 } /* namespace JS */
 
 extern JS_PUBLIC_API JSProtoKey JS_IdToProtoKey(JSContext* cx, JS::HandleId id);
 
-extern JS_PUBLIC_API JSObject* JS_GetGlobalForObject(JSContext* cx,
-                                                     JSObject* obj);
-
 extern JS_PUBLIC_API bool JS_IsGlobalObject(JSObject* obj);
 
 extern JS_PUBLIC_API JSObject* JS_GlobalLexicalEnvironment(JSObject* obj);
 
 extern JS_PUBLIC_API bool JS_HasExtensibleLexicalEnvironment(JSObject* obj);
 
 extern JS_PUBLIC_API JSObject* JS_ExtensibleLexicalEnvironment(JSObject* obj);
 
diff --git a/js/src/shell/js.cpp b/js/src/shell/js.cpp
--- a/js/src/shell/js.cpp
+++ b/js/src/shell/js.cpp
@@ -1665,17 +1665,17 @@ static bool Evaluate(JSContext* cx, unsi
   bool saveIncrementalBytecode = false;
   bool assertEqBytecode = false;
   JS::AutoObjectVector envChain(cx);
   RootedObject callerGlobal(cx, cx->global());
 
   options.setIntroductionType("js shell evaluate")
       .setFileAndLine("@evaluate", 1);
 
-  global = JS_GetGlobalForObject(cx, &args.callee());
+  global = JS::CurrentGlobalOrNull(cx);
   MOZ_ASSERT(global);
 
   if (args.length() == 2) {
     RootedObject opts(cx, &args[1].toObject());
     RootedValue v(cx);
 
     if (!ParseCompileOptions(cx, options, opts, fileNameBytes)) return false;
 
diff --git a/js/src/shell/jsshell.cpp b/js/src/shell/jsshell.cpp
--- a/js/src/shell/jsshell.cpp
+++ b/js/src/shell/jsshell.cpp
@@ -76,18 +76,17 @@ bool GenerateInterfaceHelp(JSContext* cx
   s = buf.finishString();
   if (!s || !JS_DefineProperty(cx, obj, "usage", s, 0)) return false;
 
   return true;
 }
 
 bool CreateAlias(JSContext* cx, const char* dstName,
                  JS::HandleObject namespaceObj, const char* srcName) {
-  RootedObject global(cx, JS_GetGlobalForObject(cx, namespaceObj));
-  if (!global) return false;
+  RootedObject global(cx, JS::GetNonCCWObjectGlobal(namespaceObj));
 
   RootedValue val(cx);
   if (!JS_GetProperty(cx, namespaceObj, srcName, &val)) return false;
 
   if (!val.isObject()) {
     JS_ReportErrorASCII(cx, "attempted to alias nonexistent function");
     return false;
   }
diff --git a/js/xpconnect/src/XPCComponents.cpp b/js/xpconnect/src/XPCComponents.cpp
--- a/js/xpconnect/src/XPCComponents.cpp
+++ b/js/xpconnect/src/XPCComponents.cpp
@@ -2203,27 +2203,21 @@ nsXPCComponents_Utils::CallFunctionWithA
 }
 
 NS_IMETHODIMP
 nsXPCComponents_Utils::GetGlobalForObject(HandleValue object, JSContext* cx,
                                           MutableHandleValue retval) {
   // First argument must be an object.
   if (object.isPrimitive()) return NS_ERROR_XPC_BAD_CONVERT_JS;
 
-  // Wrappers are parented to their the global in their home compartment. But
-  // when getting the global for a cross-compartment wrapper, we really want
+  // When getting the global for a cross-compartment wrapper, we really want
   // a wrapper for the foreign global. So we need to unwrap before getting the
-  // parent, enter the compartment for the duration of the call, and wrap the
-  // result.
+  // global and then wrap the result.
   Rooted<JSObject*> obj(cx, &object.toObject());
-  obj = js::UncheckedUnwrap(obj);
-  {
-    JSAutoRealm ar(cx, obj);
-    obj = JS_GetGlobalForObject(cx, obj);
-  }
+  obj = JS::GetNonCCWObjectGlobal(js::UncheckedUnwrap(obj));
 
   if (!JS_WrapObject(cx, &obj)) return NS_ERROR_FAILURE;
 
   // Get the WindowProxy if necessary.
   obj = js::ToWindowProxyIfWindow(obj);
 
   retval.setObject(*obj);
   return NS_OK;
diff --git a/js/xpconnect/wrappers/WrapperFactory.cpp b/js/xpconnect/wrappers/WrapperFactory.cpp
--- a/js/xpconnect/wrappers/WrapperFactory.cpp
+++ b/js/xpconnect/wrappers/WrapperFactory.cpp
@@ -237,17 +237,17 @@ void WrapperFactory::PrepareForWrapping(
       // that we don't create a second JS object for it: create a security
       // wrapper.
       if (js::GetObjectCompartment(scope) !=
           js::GetObjectCompartment(wrapScope)) {
         retObj.set(waive ? WaiveXray(cx, obj) : obj);
         return;
       }
 
-      RootedObject currentScope(cx, JS_GetGlobalForObject(cx, obj));
+      RootedObject currentScope(cx, JS::GetNonCCWObjectGlobal(obj));
       if (MOZ_UNLIKELY(wrapScope != currentScope)) {
         // The wrapper claims it wants to be in the new scope, but
         // currently has a reflection that lives in the old scope. This
         // can mean one of two things, both of which are rare:
         //
         // 1 - The object has a PreCreate hook (we checked for it above),
         // but is deciding to request one-wrapper-per-scope (rather than
         // one-wrapper-per-native) for some reason. Usually, a PreCreate
