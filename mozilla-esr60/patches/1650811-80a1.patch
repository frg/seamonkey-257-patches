# HG changeset patch
# User Andrea Marchesini <amarchesini@mozilla.com>
# Date 1594629247 0
#      Mon Jul 13 08:34:07 2020 +0000
# Node ID 708298a65c5780828a23b3befde7f7ef62972e6e
# Parent  282b27fc3ce5dbf33cae6b822b99a0914343fbb7
Bug 1650811 - Make Base64 compatible with ReadSegments() with small buffers, r=asuth,necko-reviewers

Differential Revision: https://phabricator.services.mozilla.com/D82522

diff --git a/netwerk/test/gtest/TestBase64Stream.cpp b/netwerk/test/gtest/TestBase64Stream.cpp
new file mode 100644
--- /dev/null
+++ b/netwerk/test/gtest/TestBase64Stream.cpp
@@ -0,0 +1,95 @@
+/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
+/* This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+#include "gtest/gtest.h"
+#include "mozilla/Base64.h"
+#include "nsIInputStream.h"
+
+namespace mozilla {
+namespace net {
+
+// An input stream whose ReadSegments method calls aWriter with writes of size
+// aStep from the provided aInput in order to test edge-cases related to small
+// buffers.
+class TestStream final : public nsIInputStream {
+ public:
+  NS_DECL_ISUPPORTS;
+
+  TestStream(const nsACString& aInput, uint32_t aStep)
+      : mInput(aInput), mStep(aStep) {}
+
+  NS_IMETHOD Close() override { MOZ_CRASH("This should not be called"); }
+
+  NS_IMETHOD Available(uint64_t* aLength) override {
+    *aLength = mInput.Length() - mPos;
+    return NS_OK;
+  }
+
+  NS_IMETHOD Read(char* aBuffer, uint32_t aCount,
+                  uint32_t* aReadCount) override {
+    MOZ_CRASH("This should not be called");
+  }
+
+  NS_IMETHOD ReadSegments(nsWriteSegmentFun aWriter, void* aClosure,
+                          uint32_t aCount, uint32_t* aResult) override {
+    *aResult = 0;
+
+    if (mPos == mInput.Length()) {
+      return NS_OK;
+    }
+
+    while (aCount > 0) {
+      uint32_t amt = std::min(mStep, (uint32_t)(mInput.Length() - mPos));
+
+      uint32_t read = 0;
+      nsresult rv =
+          aWriter(this, aClosure, mInput.get() + mPos, *aResult, amt, &read);
+      if (NS_WARN_IF(NS_FAILED(rv))) {
+        return rv;
+      }
+
+      *aResult += read;
+      aCount -= read;
+      mPos += read;
+    }
+
+    return NS_OK;
+  }
+
+  NS_IMETHOD IsNonBlocking(bool* aNonBlocking) override {
+    *aNonBlocking = true;
+    return NS_OK;
+  }
+
+ private:
+  ~TestStream() = default;
+
+  nsCString mInput;
+  const uint32_t mStep;
+  uint32_t mPos = 0;
+};
+
+NS_IMPL_ISUPPORTS(TestStream, nsIInputStream)
+
+// Test the base64 encoder with writer buffer sizes between 1 byte and the
+// entire length of "Hello World!" in order to exercise various edge cases.
+TEST(TestBase64Stream, Run)
+{
+  nsCString input;
+  input.AssignLiteral("Hello World!");
+
+  for (uint32_t step = 1; step <= input.Length(); ++step) {
+    RefPtr<TestStream> ts = new TestStream(input, step);
+
+    nsAutoString encodedData;
+    nsresult rv = Base64EncodeInputStream(ts, encodedData, input.Length());
+    ASSERT_TRUE(NS_SUCCEEDED(rv));
+
+    EXPECT_TRUE(encodedData.EqualsLiteral("SGVsbG8gV29ybGQh"));
+  }
+}
+
+}  // namespace net
+}  // namespace mozilla
diff --git a/netwerk/test/gtest/moz.build b/netwerk/test/gtest/moz.build
--- a/netwerk/test/gtest/moz.build
+++ b/netwerk/test/gtest/moz.build
@@ -1,15 +1,16 @@
 # -*- Mode: python; c-basic-offset: 4; indent-tabs-mode: nil; tab-width: 40 -*-
 # vim: set filetype=python:
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 UNIFIED_SOURCES += [
+    'TestBase64Stream.cpp',
     'TestBufferedInputStream.cpp',
     'TestHeaders.cpp',
     'TestHttpAuthUtils.cpp',
     'TestIsValidIp.cpp',
     'TestMozURL.cpp',
     'TestPartiallySeekableInputStream.cpp',
     'TestProtocolProxyService.cpp',
     'TestReadStreamToString.cpp',
diff --git a/xpcom/io/Base64.cpp b/xpcom/io/Base64.cpp
--- a/xpcom/io/Base64.cpp
+++ b/xpcom/io/Base64.cpp
@@ -97,55 +97,73 @@ struct EncodeInputStream_State {
   uint8_t charsOnStack;
   typename T::char_type* buffer;
 };
 
 template <typename T>
 nsresult EncodeInputStream_Encoder(nsIInputStream* aStream, void* aClosure,
                                    const char* aFromSegment, uint32_t aToOffset,
                                    uint32_t aCount, uint32_t* aWriteCount) {
-  NS_ASSERTION(aCount > 0, "Er, what?");
+  MOZ_ASSERT(aCount > 0, "Er, what?");
 
   EncodeInputStream_State<T>* state =
       static_cast<EncodeInputStream_State<T>*>(aClosure);
 
+  // We consume the whole data always.
+  *aWriteCount = aCount;
+
   // If we have any data left from last time, encode it now.
   uint32_t countRemaining = aCount;
   const unsigned char* src = (const unsigned char*)aFromSegment;
   if (state->charsOnStack) {
+    MOZ_ASSERT(state->charsOnStack == 1 || state->charsOnStack == 2);
+
+    // Not enough data to compose a triple.
+    if (state->charsOnStack == 1 && countRemaining == 1) {
+      state->charsOnStack = 2;
+      state->c[1] = src[0];
+      return NS_OK;
+    }
+
+    uint32_t consumed = 0;
     unsigned char firstSet[4];
     if (state->charsOnStack == 1) {
       firstSet[0] = state->c[0];
       firstSet[1] = src[0];
-      firstSet[2] = (countRemaining > 1) ? src[1] : '\0';
+      firstSet[2] = src[1];
       firstSet[3] = '\0';
+      consumed = 2;
     } else /* state->charsOnStack == 2 */ {
       firstSet[0] = state->c[0];
       firstSet[1] = state->c[1];
       firstSet[2] = src[0];
       firstSet[3] = '\0';
+      consumed = 1;
     }
+
     Encode(firstSet, 3, state->buffer);
     state->buffer += 4;
-    countRemaining -= (3 - state->charsOnStack);
-    src += (3 - state->charsOnStack);
+    countRemaining -= consumed;
+    src += consumed;
     state->charsOnStack = 0;
+
+    // Nothing is left.
+    if (!countRemaining) {
+      return NS_OK;
+    }
   }
 
-  // Encode the bulk of the
+  // Encode as many full triplets as possible.
   uint32_t encodeLength = countRemaining - countRemaining % 3;
   MOZ_ASSERT(encodeLength % 3 == 0, "Should have an exact number of triplets!");
   Encode(src, encodeLength, state->buffer);
   state->buffer += (encodeLength / 3) * 4;
   src += encodeLength;
   countRemaining -= encodeLength;
 
-  // We must consume all data, so if there's some data left stash it
-  *aWriteCount = aCount;
-
   if (countRemaining) {
     // We should never have a full triplet left at this point.
     MOZ_ASSERT(countRemaining < 3, "We should have encoded more!");
     state->c[0] = src[0];
     state->c[1] = (countRemaining == 2) ? src[1] : '\0';
     state->charsOnStack = countRemaining;
   }
 
