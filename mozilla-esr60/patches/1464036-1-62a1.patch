# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1527336850 -7200
# Node ID 7381b7b49ef2e48f829ad233854cb46999d8282e
# Parent  80d3c38b91da10ad412d381dddd35ce9bb8e177b
Bug 1464036 - Remove PropertyInfo constructor to keep MSVC from generating static initializers. r=bz

diff --git a/dom/bindings/BindingUtils.cpp b/dom/bindings/BindingUtils.cpp
--- a/dom/bindings/BindingUtils.cpp
+++ b/dom/bindings/BindingUtils.cpp
@@ -1044,19 +1044,19 @@ bool VariantToJsval(JSContext* aCx, nsIV
 }
 
 static int CompareIdsAtIndices(const void* aElement1, const void* aElement2,
                                void* aClosure) {
   const uint16_t index1 = *static_cast<const uint16_t*>(aElement1);
   const uint16_t index2 = *static_cast<const uint16_t*>(aElement2);
   const PropertyInfo* infos = static_cast<PropertyInfo*>(aClosure);
 
-  MOZ_ASSERT(JSID_BITS(infos[index1].id) != JSID_BITS(infos[index2].id));
-
-  return JSID_BITS(infos[index1].id) < JSID_BITS(infos[index2].id) ? -1 : 1;
+  MOZ_ASSERT(JSID_BITS(infos[index1].Id()) != JSID_BITS(infos[index2].Id()));
+
+  return JSID_BITS(infos[index1].Id()) < JSID_BITS(infos[index2].Id()) ? -1 : 1;
 }
 
 template <typename SpecT>
 static bool InitIdsInternal(JSContext* cx, const Prefable<SpecT>* pref,
                             PropertyInfo* infos, PropertyType type) {
   MOZ_ASSERT(pref);
   MOZ_ASSERT(pref->specs);
 
@@ -1066,19 +1066,21 @@ static bool InitIdsInternal(JSContext* c
   do {
     // We ignore whether the set of ids is enabled and just intern all the IDs,
     // because this is only done once per application runtime.
     const SpecT* spec = pref->specs;
     // Index of the property/function/constant spec for our current PropertyInfo
     // in the "specs" array of the relevant Prefable.
     uint32_t specIndex = 0;
     do {
-      if (!JS::PropertySpecNameToPermanentId(cx, spec->name, &infos->id)) {
+      jsid id;
+      if (!JS::PropertySpecNameToPermanentId(cx, spec->name, &id)) {
         return false;
       }
+      infos->SetId(id);
       infos->type = type;
       infos->prefIndex = prefIndex;
       infos->specIndex = specIndex++;
       ++infos;
     } while ((++spec)->name);
     ++prefIndex;
   } while ((++pref)->specs);
 
@@ -1268,20 +1270,20 @@ struct IdToIndexComparator {
   // The id we're searching for.
   const jsid& mId;
   // The list of ids we're searching in.
   const PropertyInfo* mInfos;
 
   explicit IdToIndexComparator(const jsid& aId, const PropertyInfo* aInfos)
       : mId(aId), mInfos(aInfos) {}
   int operator()(const uint16_t aIndex) const {
-    if (JSID_BITS(mId) == JSID_BITS(mInfos[aIndex].id)) {
+    if (JSID_BITS(mId) == JSID_BITS(mInfos[aIndex].Id())) {
       return 0;
     }
-    return JSID_BITS(mId) < JSID_BITS(mInfos[aIndex].id) ? -1 : 1;
+    return JSID_BITS(mId) < JSID_BITS(mInfos[aIndex].Id()) ? -1 : 1;
   }
 };
 
 static const PropertyInfo* XrayFindOwnPropertyInfo(
     JSContext* cx, JS::Handle<jsid> id,
     const NativeProperties* nativeProperties) {
   if (MOZ_UNLIKELY(nativeProperties->iteratorAliasMethodIndex >= 0) &&
       id == SYMBOL_TO_JSID(
@@ -1685,17 +1687,17 @@ bool XrayAppendPropertyKeys(JSContext* c
                             const Prefable<const SpecType>* pref,
                             const PropertyInfo* infos, unsigned flags,
                             JS::AutoIdVector& props) {
   do {
     bool prefIsEnabled = pref->isEnabled(cx, obj);
     if (prefIsEnabled) {
       const SpecType* spec = pref->specs;
       do {
-        const jsid& id = infos++->id;
+        const jsid id = infos++->Id();
         if (((flags & JSITER_HIDDEN) || (spec->flags & JSPROP_ENUMERATE)) &&
             ((flags & JSITER_SYMBOLS) || !JSID_IS_SYMBOL(id)) &&
             !props.append(id)) {
           return false;
         }
       } while ((++spec)->name);
     }
     // Break if we have reached the end of pref.
@@ -1718,17 +1720,17 @@ bool XrayAppendPropertyKeys<ConstantSpec
     JSContext* cx, JS::Handle<JSObject*> obj,
     const Prefable<const ConstantSpec>* pref, const PropertyInfo* infos,
     unsigned flags, JS::AutoIdVector& props) {
   do {
     bool prefIsEnabled = pref->isEnabled(cx, obj);
     if (prefIsEnabled) {
       const ConstantSpec* spec = pref->specs;
       do {
-        if (!props.append(infos++->id)) {
+        if (!props.append(infos++->Id())) {
           return false;
         }
       } while ((++spec)->name);
     }
     // Break if we have reached the end of pref.
     if (!(++pref)->specs) {
       break;
     }
diff --git a/dom/bindings/DOMJSClass.h b/dom/bindings/DOMJSClass.h
--- a/dom/bindings/DOMJSClass.h
+++ b/dom/bindings/DOMJSClass.h
@@ -180,28 +180,37 @@ enum PropertyType {
   ePropertyTypeCount
 };
 
 #define NUM_BITS_PROPERTY_INFO_TYPE 3
 #define NUM_BITS_PROPERTY_INFO_PREF_INDEX 13
 #define NUM_BITS_PROPERTY_INFO_SPEC_INDEX 16
 
 struct PropertyInfo {
-  jsid id;
+ private:
+  // MSVC generates static initializers if we store a jsid here, even if
+  // PropertyInfo has a constexpr constructor. See bug 1460341 and bug 1464036.
+  uintptr_t mIdBits;
+ public:
   // One of PropertyType, will be used for accessing the corresponding Duo in
   // NativePropertiesN.duos[].
   uint32_t type : NUM_BITS_PROPERTY_INFO_TYPE;
   // The index to the corresponding Preable in Duo.mPrefables[].
   uint32_t prefIndex : NUM_BITS_PROPERTY_INFO_PREF_INDEX;
   // The index to the corresponding spec in Duo.mPrefables[prefIndex].specs[].
   uint32_t specIndex : NUM_BITS_PROPERTY_INFO_SPEC_INDEX;
 
-  // Note: the default constructor is not constexpr because of the bit fields,
-  // so we need this one.
-  constexpr PropertyInfo() : id(), type(0), prefIndex(0), specIndex(0) {}
+  void SetId(jsid aId) {
+    static_assert(sizeof(jsid) == sizeof(mIdBits),
+                  "jsid should fit in mIdBits");
+    mIdBits = JSID_BITS(aId);
+  }
+  MOZ_ALWAYS_INLINE jsid Id() const {
+    return jsid::fromRawBits(mIdBits);
+  }
 };
 
 static_assert(
     ePropertyTypeCount <= 1ull << NUM_BITS_PROPERTY_INFO_TYPE,
     "We have property type count that is > (1 << NUM_BITS_PROPERTY_INFO_TYPE)");
 
 // Conceptually, NativeProperties has seven (Prefable<T>*, PropertyInfo*) duos
 // (where T is one of JSFunctionSpec, JSPropertySpec, or ConstantSpec), one for
diff --git a/js/public/Id.h b/js/public/Id.h
--- a/js/public/Id.h
+++ b/js/public/Id.h
@@ -33,17 +33,17 @@
 #define JSID_TYPE_SYMBOL 0x4
 #define JSID_TYPE_MASK 0x7
 
 struct jsid {
   size_t asBits;
 
   constexpr jsid() : asBits(JSID_TYPE_VOID) {}
 
-  static constexpr jsid fromRawBits(size_t bits) {
+  static constexpr MOZ_ALWAYS_INLINE jsid fromRawBits(size_t bits) {
     jsid id;
     id.asBits = bits;
     return id;
   }
 
   bool operator==(const jsid& rhs) const { return asBits == rhs.asBits; }
   bool operator!=(const jsid& rhs) const { return asBits != rhs.asBits; }
 } JS_HAZ_GC_POINTER;
