# HG changeset patch
# User Lars T Hansen <lhansen@mozilla.com>
# Date 1521816350 -3600
# Node ID 3d46648331c1bd98b2f856f543d315fd87890f91
# Parent  5fc173ae57550813ab474e203e040faed741ef65
Bug 1445277 - Suppress GC when wasm is active and running with wasm-gc support.  r=bbouvier

We emulate the effect of AutoSuppressGC whenever we enter wasm code
via a stub.

This is obviously a gross hack and only allowable since it is
nightly-only and under a pref.  Strictly intended for rapid
prototyping; not intended to ship in this form.  Eventually we'll have
proper GC support for wasm frames.

diff --git a/js/src/threading/ProtectedData.h b/js/src/threading/ProtectedData.h
--- a/js/src/threading/ProtectedData.h
+++ b/js/src/threading/ProtectedData.h
@@ -132,16 +132,18 @@ class ProtectedData {
     if (!AutoNoteSingleThreadedRegion::count) check.check();
 #endif
     return value;
   }
 
   T& refNoCheck() { return value; }
   const T& refNoCheck() const { return value; }
 
+  static size_t offsetOfValue() { return offsetof(ThisType, value); }
+
  private:
   T value;
 #ifdef JS_HAS_PROTECTED_DATA_CHECKS
   Check check;
 #endif
 };
 
 // Intermediate class for protected data whose checks take no constructor
diff --git a/js/src/wasm/WasmCode.cpp b/js/src/wasm/WasmCode.cpp
--- a/js/src/wasm/WasmCode.cpp
+++ b/js/src/wasm/WasmCode.cpp
@@ -553,17 +553,18 @@ struct ProjectLazyFuncIndex {
       : funcExports(funcExports) {}
   uint32_t operator[](size_t index) const {
     return funcExports[index].funcIndex;
   }
 };
 
 static constexpr unsigned LAZY_STUB_LIFO_DEFAULT_CHUNK_SIZE = 8 * 1024;
 
-bool LazyStubTier::createMany(const Uint32Vector& funcExportIndices,
+bool LazyStubTier::createMany(HasGcTypes gcTypesEnabled,
+                              const Uint32Vector& funcExportIndices,
                               const CodeTier& codeTier,
                               size_t* stubSegmentIndex) {
   MOZ_ASSERT(funcExportIndices.length());
 
   LifoAlloc lifo(LAZY_STUB_LIFO_DEFAULT_CHUNK_SIZE);
   TempAllocator alloc(&lifo);
   JitContext jitContext(&alloc);
   WasmMacroAssembler masm(alloc);
@@ -576,19 +577,20 @@ bool LazyStubTier::createMany(const Uint
   DebugOnly<uint32_t> numExpectedRanges = 0;
   for (uint32_t funcExportIndex : funcExportIndices) {
     const FuncExport& fe = funcExports[funcExportIndex];
     numExpectedRanges += fe.sig().temporarilyUnsupportedAnyRef() ? 1 : 2;
     void* calleePtr = moduleSegmentBase +
                       moduleRanges[fe.interpCodeRangeIndex()].funcNormalEntry();
     Maybe<ImmPtr> callee;
     callee.emplace(calleePtr, ImmPtr::NoCheckToken());
-    if (!GenerateEntryStubs(masm, funcExportIndex, fe, callee, /* asmjs*/ false,
-                            &codeRanges))
+    if (!GenerateEntryStubs(masm, funcExportIndex, fe, callee,
+                            /* asmjs */ false, gcTypesEnabled, &codeRanges)) {
       return false;
+    }
   }
   MOZ_ASSERT(codeRanges.length() == numExpectedRanges,
              "incorrect number of entries per function");
 
   masm.finish();
 
   MOZ_ASSERT(masm.callSites().empty());
   MOZ_ASSERT(masm.callSiteTargets().empty());
@@ -660,17 +662,20 @@ bool LazyStubTier::createMany(const Uint
 }
 
 bool LazyStubTier::createOne(uint32_t funcExportIndex,
                              const CodeTier& codeTier) {
   Uint32Vector funcExportIndexes;
   if (!funcExportIndexes.append(funcExportIndex)) return false;
 
   size_t stubSegmentIndex;
-  if (!createMany(funcExportIndexes, codeTier, &stubSegmentIndex)) return false;
+  if (!createMany(codeTier.code().metadata().temporaryHasGcTypes,
+                  funcExportIndexes, codeTier, &stubSegmentIndex)) {
+    return false;
+  }
 
   const UniqueLazyStubSegment& segment = stubSegments_[stubSegmentIndex];
   const CodeRangeVector& codeRanges = segment->codeRanges();
 
   // Functions that have anyref in their sig don't get a jit entry.
   if (codeTier.metadata()
           .funcExports[funcExportIndex]
           .sig()
@@ -685,23 +690,26 @@ bool LazyStubTier::createOne(uint32_t fu
 
   const CodeRange& cr = codeRanges[codeRanges.length() - 1];
   MOZ_ASSERT(cr.isJitEntry());
 
   codeTier.code().setJitEntry(cr.funcIndex(), segment->base() + cr.begin());
   return true;
 }
 
-bool LazyStubTier::createTier2(const Uint32Vector& funcExportIndices,
+bool LazyStubTier::createTier2(HasGcTypes gcTypesEnabled,
+                               const Uint32Vector& funcExportIndices,
                                const CodeTier& codeTier,
                                Maybe<size_t>* outStubSegmentIndex) {
   if (!funcExportIndices.length()) return true;
 
   size_t stubSegmentIndex;
-  if (!createMany(funcExportIndices, codeTier, &stubSegmentIndex)) return false;
+  if (!createMany(gcTypesEnabled, funcExportIndices, codeTier,
+                  &stubSegmentIndex))
+    return false;
 
   outStubSegmentIndex->emplace(stubSegmentIndex);
   return true;
 }
 
 void LazyStubTier::setJitEntries(const Maybe<size_t>& stubSegmentIndex,
                                  const Code& code) {
   if (!stubSegmentIndex) return;
diff --git a/js/src/wasm/WasmCode.h b/js/src/wasm/WasmCode.h
--- a/js/src/wasm/WasmCode.h
+++ b/js/src/wasm/WasmCode.h
@@ -540,17 +540,18 @@ using LazyFuncExportVector = Vector<Lazy
 // write lazy stubs at any time while a background thread can regenerate lazy
 // stubs for tier2 at any time.
 
 class LazyStubTier {
   LazyStubSegmentVector stubSegments_;
   LazyFuncExportVector exports_;
   size_t lastStubSegmentIndex_;
 
-  bool createMany(const Uint32Vector& funcExportIndices,
+  bool createMany(HasGcTypes gcTypesEnabled,
+                  const Uint32Vector& funcExportIndices,
                   const CodeTier& codeTier, size_t* stubSegmentIndex);
 
  public:
   LazyStubTier() : lastStubSegmentIndex_(0) {}
 
   bool empty() const { return stubSegments_.empty(); }
   bool hasStub(uint32_t funcIndex) const;
 
@@ -561,17 +562,18 @@ class LazyStubTier {
   // Creates one lazy stub for the exported function, for which the jit entry
   // will be set to the lazily-generated one.
   bool createOne(uint32_t funcExportIndex, const CodeTier& codeTier);
 
   // Create one lazy stub for all the functions in funcExportIndices, putting
   // them in a single stub. Jit entries won't be used until
   // setJitEntries() is actually called, after the Code owner has committed
   // tier2.
-  bool createTier2(const Uint32Vector& funcExportIndices,
+  bool createTier2(HasGcTypes gcTypesEnabled,
+                   const Uint32Vector& funcExportIndices,
                    const CodeTier& codeTier, Maybe<size_t>* stubSegmentIndex);
   void setJitEntries(const Maybe<size_t>& stubSegmentIndex, const Code& code);
 
   void addSizeOfMisc(MallocSizeOf mallocSizeOf, size_t* code,
                      size_t* data) const;
 };
 
 // CodeTier contains all the data related to a given compilation tier. It is
diff --git a/js/src/wasm/WasmModule.cpp b/js/src/wasm/WasmModule.cpp
--- a/js/src/wasm/WasmModule.cpp
+++ b/js/src/wasm/WasmModule.cpp
@@ -191,18 +191,21 @@ bool Module::finishTier2(UniqueLinkDataT
     for (size_t i = 0; i < metadataTier1.funcExports.length(); i++) {
       const FuncExport& fe = metadataTier1.funcExports[i];
       if (fe.hasEagerStubs()) continue;
       MOZ_ASSERT(!env2->isAsmJS(), "only wasm functions are lazily exported");
       if (!stubs1->hasStub(fe.funcIndex())) continue;
       if (!funcExportIndices.emplaceBack(i)) return false;
     }
 
+    HasGcTypes gcTypesEnabled = code().metadata().temporaryHasGcTypes;
+
     Maybe<size_t> stub2Index;
-    if (!stubs2->createTier2(funcExportIndices, *tier2, &stub2Index))
+    if (!stubs2->createTier2(gcTypesEnabled, funcExportIndices, *tier2,
+                             &stub2Index))
       return false;
 
     // Install the data in the data structures. They will not be visible
     // yet.
 
     MOZ_ASSERT(!code().hasTier2());
     linkData().setTier2(Move(linkData2));
     code().setTier2(Move(tier2));
diff --git a/js/src/wasm/WasmStubs.cpp b/js/src/wasm/WasmStubs.cpp
--- a/js/src/wasm/WasmStubs.cpp
+++ b/js/src/wasm/WasmStubs.cpp
@@ -264,74 +264,90 @@ static const LiveRegisterSet NonVolatile
 #if defined(JS_CODEGEN_NONE)
 static const unsigned NonVolatileRegsPushSize = 0;
 #else
 static const unsigned NonVolatileRegsPushSize =
     NonVolatileRegs.gprs().size() * sizeof(intptr_t) +
     NonVolatileRegs.fpus().getPushSizeInBytes();
 #endif
 
-#if defined(JS_CODEGEN_ARM64)
-// Stacks are 16-byte aligned, hence the extra word.
+#ifdef ENABLE_WASM_GC
+static const unsigned NumExtraPushed = 2; // tls and argv
+#else
+static const unsigned NumExtraPushed = 1; // argv
+#endif
+
+#ifdef JS_CODEGEN_ARM64
+static const unsigned WasmPushSize = 16;
+#else
+static const unsigned WasmPushSize = sizeof(void*);
+#endif
+
 static const unsigned FramePushedBeforeAlign =
-    NonVolatileRegsPushSize + 2 * sizeof(void*);
-#else
-static const unsigned FramePushedBeforeAlign =
-    NonVolatileRegsPushSize + sizeof(void*);
-#endif
+    NonVolatileRegsPushSize + NumExtraPushed * WasmPushSize;
 
 static void AssertExpectedSP(const MacroAssembler& masm) {
 #ifdef JS_CODEGEN_ARM64
   MOZ_ASSERT(sp.Is(masm.GetStackPointer64()));
 #endif
 }
 
 template <class Operand>
 static void WasmPush(MacroAssembler& masm, const Operand& op) {
 #ifdef JS_CODEGEN_ARM64
   // Allocate a pad word so that SP can remain properly aligned.
-  masm.reserveStack(16);
+  masm.reserveStack(WasmPushSize);
   masm.storePtr(op, Address(masm.getStackPointer(), 0));
 #else
   masm.Push(op);
 #endif
 }
 
 static void WasmPop(MacroAssembler& masm, Register r) {
 #ifdef JS_CODEGEN_ARM64
   // Also pop the pad word allocated by WasmPush.
   masm.loadPtr(Address(masm.getStackPointer(), 0), r);
-  masm.freeStack(16);
+  masm.freeStack(WasmPushSize);
 #else
   masm.Pop(r);
 #endif
 }
 
 static void MoveSPForJitABI(MacroAssembler& masm) {
 #ifdef JS_CODEGEN_ARM64
   masm.moveStackPtrTo(PseudoStackPointer);
 #endif
 }
 
+#ifdef ENABLE_WASM_GC
+static void SuppressGC(MacroAssembler& masm, int32_t increment,
+                       Register scratch) {
+  masm.loadPtr(Address(WasmTlsReg, offsetof(TlsData, cx)), scratch);
+  masm.add32(Imm32(increment),
+             Address(scratch, offsetof(JSContext, suppressGC) +
+                              js::ThreadLocalData<int32_t>::offsetOfValue()));
+}
+#endif
+
 static void CallFuncExport(MacroAssembler& masm, const FuncExport& fe,
                            const Maybe<ImmPtr>& funcPtr) {
   MOZ_ASSERT(fe.hasEagerStubs() == !funcPtr);
   if (funcPtr)
     masm.call(*funcPtr);
   else
     masm.call(CallSiteDesc(CallSiteDesc::Func), fe.funcIndex());
 }
 
 // Generate a stub that enters wasm from a C++ caller via the native ABI. The
 // signature of the entry point is Module::ExportFuncPtr. The exported wasm
 // function has an ABI derived from its specific signature, so this function
 // must map from the ABI of ExportFuncPtr to the export's signature's ABI.
 static bool GenerateInterpEntry(MacroAssembler& masm, const FuncExport& fe,
                                 const Maybe<ImmPtr>& funcPtr,
-                                Offsets* offsets) {
+                                HasGcTypes gcTypesEnabled, Offsets* offsets) {
   AssertExpectedSP(masm);
   masm.haltingAlign(CodeAlignment);
 
   offsets->begin = masm.currentOffset();
 
   // Save the return address if it wasn't already saved by the call insn.
 #ifdef JS_USE_LINK_REGISTER
 #if defined(JS_CODEGEN_ARM)
@@ -376,16 +392,22 @@ static bool GenerateInterpEntry(MacroAss
   arg = abi.next(MIRType::Pointer);
   if (arg.kind() == ABIArg::GPR)
     masm.movePtr(arg.gpr(), WasmTlsReg);
   else
     masm.loadPtr(
         Address(masm.getStackPointer(), argBase + arg.offsetFromArgBase()),
         WasmTlsReg);
 
+#ifdef ENABLE_WASM_GC
+  WasmPush(masm, WasmTlsReg);
+  if (gcTypesEnabled == HasGcTypes::True)
+    SuppressGC(masm, 1, scratch);
+#endif
+
   // Save 'argv' on the stack so that we can recover it after the call.
   WasmPush(masm, argv);
 
   // Since we're about to dynamically align the stack, reset the frame depth
   // so we can still assert static stack depth balancing.
   MOZ_ASSERT(masm.framePushed() == FramePushedBeforeAlign);
   masm.setFramePushed(0);
 
@@ -428,16 +450,22 @@ static bool GenerateInterpEntry(MacroAss
   masm.PopStackPtr();
 #endif
   MOZ_ASSERT(masm.framePushed() == 0);
   masm.setFramePushed(FramePushedBeforeAlign);
 
   // Recover the 'argv' pointer which was saved before aligning the stack.
   WasmPop(masm, argv);
 
+#ifdef ENABLE_WASM_GC
+  WasmPop(masm, WasmTlsReg);
+  if (gcTypesEnabled == HasGcTypes::True)
+    SuppressGC(masm, -1, WasmTlsReg);
+#endif
+
   // Store the return value in argv[0].
   StoreABIReturn(masm, fe, argv);
 
   // After the ReturnReg is stored into argv[0] but before fp is clobbered by
   // the PopRegsInMask(NonVolatileRegs) below, set the return value based on
   // whether fp is null (which is the case for successful returns) or the
   // FailFP magic value (set by the throw stub);
   Label success, join;
@@ -454,17 +482,17 @@ static bool GenerateInterpEntry(MacroAss
   masm.move32(Imm32(true), ReturnReg);
   masm.bind(&join);
 
   // Restore clobbered non-volatile registers of the caller.
   masm.PopRegsInMask(NonVolatileRegs);
   MOZ_ASSERT(masm.framePushed() == 0);
 
 #if defined(JS_CODEGEN_ARM64)
-  masm.setFramePushed(16);
+  masm.setFramePushed(WasmPushSize);
   WasmPop(masm, lr);
   masm.abiret();
 #else
   masm.ret();
 #endif
 
   return FinishOffsets(masm, offsets);
 }
@@ -532,42 +560,54 @@ static void GenerateJitEntryThrow(MacroA
 // Generate a stub that enters wasm from a jit code caller via the jit ABI.
 //
 // ARM64 note: This does not save the PseudoStackPointer so we must be sure to
 // recompute it on every return path, be it normal return or exception return.
 // The JIT code we return to assumes it is correct.
 
 static bool GenerateJitEntry(MacroAssembler& masm, size_t funcExportIndex,
                              const FuncExport& fe, const Maybe<ImmPtr>& funcPtr,
-                             Offsets* offsets) {
+                             HasGcTypes gcTypesEnabled, Offsets* offsets) {
   AssertExpectedSP(masm);
 
   RegisterOrSP sp = masm.getStackPointer();
 
   GenerateJitEntryPrologue(masm, offsets);
 
   // The jit caller has set up the following stack layout (sp grows to the
   // left):
   // <-- retAddr | descriptor | callee | argc | this | arg1..N
 
-  unsigned normalBytesNeeded = StackArgBytes(fe.sig().args());
+#ifdef ENABLE_WASM_GC
+  // Save WasmTlsReg in the uppermost part of the reserved area, because we
+  // need it directly after the call.
+  unsigned savedTlsSize = AlignBytes(sizeof(void*), WasmStackAlignment);
+#else
+  unsigned savedTlsSize = 0;
+#endif
+
+  unsigned normalBytesNeeded = StackArgBytes(fe.sig().args()) + savedTlsSize;
 
   MIRTypeVector coerceArgTypes;
   MOZ_ALWAYS_TRUE(coerceArgTypes.append(MIRType::Int32));
   MOZ_ALWAYS_TRUE(coerceArgTypes.append(MIRType::Pointer));
   MOZ_ALWAYS_TRUE(coerceArgTypes.append(MIRType::Pointer));
   unsigned oolBytesNeeded = StackArgBytes(coerceArgTypes);
 
   unsigned bytesNeeded = Max(normalBytesNeeded, oolBytesNeeded);
 
   // Note the jit caller ensures the stack is aligned *after* the call
   // instruction.
   unsigned frameSize =
       StackDecrementForCall(WasmStackAlignment, 0, bytesNeeded);
 
+#ifdef ENABLE_WASM_GC
+  unsigned savedTlsOffset = frameSize - sizeof(void*);
+#endif
+
   // Reserve stack space for wasm ABI arguments, set up like this:
   // <-- ABI args | padding
   masm.reserveStack(frameSize);
 
   GenerateJitEntryLoadTls(masm, frameSize);
 
   if (fe.sig().hasI64ArgOrRet()) {
     CallSymbolicAddress(masm, !fe.hasEagerStubs(),
@@ -730,22 +770,36 @@ static bool GenerateJitEntry(MacroAssemb
       }
       default: { MOZ_CRASH("unexpected input argument when calling from jit"); }
     }
   }
 
   // Setup wasm register state.
   masm.loadWasmPinnedRegsFromTls();
 
+#ifdef ENABLE_WASM_GC
+  if (gcTypesEnabled == HasGcTypes::True) {
+    masm.storePtr(WasmTlsReg, Address(sp, savedTlsOffset));
+    SuppressGC(masm, 1, ScratchIonEntry);
+  }
+#endif
+
   // Call into the real function. Note that, due to the throw stub, fp, tls
   // and pinned registers may be clobbered.
   masm.assertStackAlignment(WasmStackAlignment);
   CallFuncExport(masm, fe, funcPtr);
   masm.assertStackAlignment(WasmStackAlignment);
 
+#ifdef ENABLE_WASM_GC
+  if (gcTypesEnabled == HasGcTypes::True) {
+    masm.loadPtr(Address(sp, savedTlsOffset), WasmTlsReg);
+    SuppressGC(masm, -1, WasmTlsReg);
+  }
+#endif
+
   // If fp is equal to the FailFP magic value (set by the throw stub), then
   // report the exception to the JIT caller by jumping into the exception
   // stub; otherwise the FP value is still set to the parent ion frame value.
   Label exception;
   masm.branchPtr(Assembler::Equal, FramePointer, Imm32(FailFP), &exception);
 
   // Pop arguments.
   masm.freeStack(frameSize);
@@ -1745,29 +1799,32 @@ static bool GenerateDebugTrapStub(MacroA
 
   GenerateExitEpilogue(masm, 0, ExitReason::Fixed::DebugTrap, offsets);
 
   return FinishOffsets(masm, offsets);
 }
 
 bool wasm::GenerateEntryStubs(MacroAssembler& masm, size_t funcExportIndex,
                               const FuncExport& fe, const Maybe<ImmPtr>& callee,
-                              bool isAsmJS, CodeRangeVector* codeRanges) {
+                              bool isAsmJS, HasGcTypes gcTypesEnabled,
+                              CodeRangeVector* codeRanges) {
   MOZ_ASSERT(!callee == fe.hasEagerStubs());
   MOZ_ASSERT_IF(isAsmJS, fe.hasEagerStubs());
 
   Offsets offsets;
-  if (!GenerateInterpEntry(masm, fe, callee, &offsets)) return false;
+  if (!GenerateInterpEntry(masm, fe, callee, gcTypesEnabled, &offsets))
+    return false;
   if (!codeRanges->emplaceBack(CodeRange::InterpEntry, fe.funcIndex(), offsets))
     return false;
 
   if (isAsmJS || fe.sig().temporarilyUnsupportedAnyRef())
     return true;
 
-  if (!GenerateJitEntry(masm, funcExportIndex, fe, callee, &offsets))
+  if (!GenerateJitEntry(masm, funcExportIndex, fe, callee, gcTypesEnabled,
+                        &offsets))
     return false;
   if (!codeRanges->emplaceBack(CodeRange::JitEntry, fe.funcIndex(), offsets))
     return false;
 
   return true;
 }
 
 bool wasm::GenerateStubs(const ModuleEnvironment& env,
@@ -1806,18 +1863,19 @@ bool wasm::GenerateStubs(const ModuleEnv
 
   JitSpew(JitSpew_Codegen, "# Emitting wasm export stubs");
 
   Maybe<ImmPtr> noAbsolute;
   for (size_t i = 0; i < exports.length(); i++) {
     const FuncExport& fe = exports[i];
     if (!fe.hasEagerStubs()) continue;
     if (!GenerateEntryStubs(masm, i, fe, noAbsolute, env.isAsmJS(),
-                            &code->codeRanges))
+                            env.gcTypesEnabled, &code->codeRanges)) {
       return false;
+    }
   }
 
   JitSpew(JitSpew_Codegen, "# Emitting wasm exit stubs");
 
   Offsets offsets;
 
   if (!GenerateOutOfBoundsExit(masm, &throwLabel, &offsets)) return false;
   if (!code->codeRanges.emplaceBack(CodeRange::OutOfBoundsExit, offsets))
diff --git a/js/src/wasm/WasmStubs.h b/js/src/wasm/WasmStubs.h
--- a/js/src/wasm/WasmStubs.h
+++ b/js/src/wasm/WasmStubs.h
@@ -36,14 +36,15 @@ extern bool GenerateImportFunctions(cons
 extern bool GenerateStubs(const ModuleEnvironment& env,
                           const FuncImportVector& imports,
                           const FuncExportVector& exports, CompiledCode* code);
 
 extern bool GenerateEntryStubs(jit::MacroAssembler& masm,
                                size_t funcExportIndex,
                                const FuncExport& funcExport,
                                const Maybe<jit::ImmPtr>& callee, bool isAsmJS,
+                               HasGcTypes gcTypesEnabled,
                                CodeRangeVector* codeRanges);
 
 }  // namespace wasm
 }  // namespace js
 
 #endif  // wasm_stubs_h
