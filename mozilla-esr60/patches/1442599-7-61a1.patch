# HG changeset patch
# User Andre Bargull <andre.bargull@gmail.com>
# Date 1521113021 25200
# Node ID 2c450d858f010cec47a94afd24b566d7a65c544e
# Parent  74716a15f4ca4d329b15f53e699ee124e7680369
Bug 1442599 - Part 7: Clean up using and includes in jsutil. r=jorendorff

And remove "using mozilla::Maybe" from gc/Statistics.h and handle its fallout.

diff --git a/js/src/builtin/TestingFunctions.cpp b/js/src/builtin/TestingFunctions.cpp
--- a/js/src/builtin/TestingFunctions.cpp
+++ b/js/src/builtin/TestingFunctions.cpp
@@ -3,16 +3,17 @@
  * This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "builtin/TestingFunctions.h"
 
 #include "mozilla/Atomics.h"
 #include "mozilla/FloatingPoint.h"
+#include "mozilla/Maybe.h"
 #include "mozilla/Move.h"
 #include "mozilla/Sprintf.h"
 #include "mozilla/TextUtils.h"
 #include "mozilla/Unused.h"
 
 #include <algorithm>
 #include <cmath>
 #include <cstdlib>
@@ -66,16 +67,17 @@
 #include "vm/EnvironmentObject-inl.h"
 #include "vm/JSContext-inl.h"
 #include "vm/JSObject-inl.h"
 #include "vm/NativeObject-inl.h"
 
 using namespace js;
 
 using mozilla::ArrayLength;
+using mozilla::Maybe;
 using mozilla::Move;
 
 // If fuzzingSafe is set, remove functionality that could cause problems with
 // fuzzers. Set this via the environment variable MOZ_FUZZING_SAFE.
 mozilla::Atomic<bool> fuzzingSafe(false);
 
 // If disableOOMFunctions is set, disable functionality that causes artificial
 // OOM conditions.
diff --git a/js/src/frontend/Parser.h b/js/src/frontend/Parser.h
--- a/js/src/frontend/Parser.h
+++ b/js/src/frontend/Parser.h
@@ -1604,32 +1604,28 @@ class MOZ_STACK_CLASS AutoAwaitIsKeyword
   ~AutoAwaitIsKeyword() { parser_->setAwaitHandling(oldAwaitHandling_); }
 };
 
 template <typename Scope>
 extern typename Scope::Data* NewEmptyBindingData(JSContext* cx,
                                                  LifoAlloc& alloc,
                                                  uint32_t numBindings);
 
-Maybe<GlobalScope::Data*> NewGlobalScopeData(JSContext* context,
-                                             ParseContext::Scope& scope,
-                                             LifoAlloc& alloc,
-                                             ParseContext* pc);
-Maybe<EvalScope::Data*> NewEvalScopeData(JSContext* context,
-                                         ParseContext::Scope& scope,
-                                         LifoAlloc& alloc, ParseContext* pc);
-Maybe<FunctionScope::Data*> NewFunctionScopeData(JSContext* context,
-                                                 ParseContext::Scope& scope,
-                                                 bool hasParameterExprs,
-                                                 LifoAlloc& alloc,
-                                                 ParseContext* pc);
-Maybe<VarScope::Data*> NewVarScopeData(JSContext* context,
-                                       ParseContext::Scope& scope,
-                                       LifoAlloc& alloc, ParseContext* pc);
-Maybe<LexicalScope::Data*> NewLexicalScopeData(JSContext* context,
-                                               ParseContext::Scope& scope,
-                                               LifoAlloc& alloc,
-                                               ParseContext* pc);
+mozilla::Maybe<GlobalScope::Data*> NewGlobalScopeData(
+    JSContext* context, ParseContext::Scope& scope, LifoAlloc& alloc,
+    ParseContext* pc);
+mozilla::Maybe<EvalScope::Data*> NewEvalScopeData(
+    JSContext* context, ParseContext::Scope& scope, LifoAlloc& alloc,
+    ParseContext* pc);
+mozilla::Maybe<FunctionScope::Data*> NewFunctionScopeData(
+    JSContext* context, ParseContext::Scope& scope, bool hasParameterExprs,
+    LifoAlloc& alloc, ParseContext* pc);
+mozilla::Maybe<VarScope::Data*> NewVarScopeData(
+    JSContext* context, ParseContext::Scope& scope, LifoAlloc& alloc,
+    ParseContext* pc);
+mozilla::Maybe<LexicalScope::Data*> NewLexicalScopeData(
+    JSContext* context, ParseContext::Scope& scope, LifoAlloc& alloc,
+    ParseContext* pc);
 
 } /* namespace frontend */
 } /* namespace js */
 
 #endif /* frontend_Parser_h */
diff --git a/js/src/gc/PublicIterators.h b/js/src/gc/PublicIterators.h
--- a/js/src/gc/PublicIterators.h
+++ b/js/src/gc/PublicIterators.h
@@ -6,16 +6,18 @@
 
 /*
  * Iterators for various data structures.
  */
 
 #ifndef gc_PublicIterators_h
 #define gc_PublicIterators_h
 
+#include "mozilla/Maybe.h"
+
 #include "gc/Zone.h"
 
 namespace js {
 
 // Iterate over all zone groups except those which may be in use by helper
 // thread parse tasks.
 class ZoneGroupsIter {
   gc::AutoEnterIteration iterMarker;
@@ -82,17 +84,17 @@ class ZonesInGroupIter {
   operator JS::Zone*() const { return get(); }
   JS::Zone* operator->() const { return get(); }
 };
 
 // Iterate over all zones in the runtime, except those which may be in use by
 // parse threads.
 class ZonesIter {
   ZoneGroupsIter group;
-  Maybe<ZonesInGroupIter> zone;
+  mozilla::Maybe<ZonesInGroupIter> zone;
   JS::Zone* atomsZone;
 
  public:
   ZonesIter(JSRuntime* rt, ZoneSelector selector)
       : group(rt),
         atomsZone(selector == WithAtoms ? rt->gc.atomsZone.ref() : nullptr) {
     if (!atomsZone && !done()) next();
   }
diff --git a/js/src/gc/Statistics.h b/js/src/gc/Statistics.h
--- a/js/src/gc/Statistics.h
+++ b/js/src/gc/Statistics.h
@@ -6,31 +6,29 @@
 
 #ifndef gc_Statistics_h
 #define gc_Statistics_h
 
 #include "mozilla/Array.h"
 #include "mozilla/Atomics.h"
 #include "mozilla/EnumeratedArray.h"
 #include "mozilla/IntegerRange.h"
-#include "mozilla/Maybe.h"
 #include "mozilla/PodOperations.h"
+#include "mozilla/TimeStamp.h"
 
 #include "jspubtd.h"
 #include "NamespaceImports.h"
 
 #include "gc/GCEnum.h"
 #include "js/AllocPolicy.h"
 #include "js/SliceBudget.h"
 #include "js/UniquePtr.h"
 #include "js/Vector.h"
 #include "vm/JSONPrinter.h"
 
-using mozilla::Maybe;
-
 namespace js {
 namespace gcstats {
 
 // Phase data is generated by a script. If you need to add phases, edit
 // js/src/gc/GenerateStatsPhases.py
 
 #include "gc/StatsPhasesGenerated.h"
 
diff --git a/js/src/jit/CacheIRCompiler.h b/js/src/jit/CacheIRCompiler.h
--- a/js/src/jit/CacheIRCompiler.h
+++ b/js/src/jit/CacheIRCompiler.h
@@ -2,16 +2,18 @@
  * vim: set ts=8 sts=4 et sw=4 tw=99:
  * This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef jit_CacheIRCompiler_h
 #define jit_CacheIRCompiler_h
 
+#include "mozilla/Maybe.h"
+
 #include "jit/CacheIR.h"
 
 namespace js {
 namespace jit {
 
 // The ops below are defined in CacheIRCompiler and codegen is shared between
 // BaselineCacheIRCompiler and IonCacheIRCompiler.
 #define CACHE_IR_SHARED_OPS(_)           \
@@ -528,21 +530,21 @@ class MOZ_RAII CacheIRCompiler {
   Vector<FailurePath, 4, SystemAllocPolicy> failurePaths;
 
   // Float registers that are live. Registers not in this set can be
   // clobbered and don't need to be saved before performing a VM call.
   // Doing this for non-float registers is a bit more complicated because
   // the IC register allocator allocates GPRs.
   LiveFloatRegisterSet liveFloatRegs_;
 
-  Maybe<TypedOrValueRegister> outputUnchecked_;
+  mozilla::Maybe<TypedOrValueRegister> outputUnchecked_;
   Mode mode_;
 
   // Whether this IC may read double values from uint32 arrays.
-  Maybe<bool> allowDoubleResult_;
+  mozilla::Maybe<bool> allowDoubleResult_;
 
   CacheIRCompiler(JSContext* cx, const CacheIRWriter& writer, Mode mode)
       : cx_(cx),
         reader(writer),
         writer_(writer),
         allocator(writer_),
         liveFloatRegs_(FloatRegisterSet::All()),
         mode_(mode) {
diff --git a/js/src/jit/IonBuilder.h b/js/src/jit/IonBuilder.h
--- a/js/src/jit/IonBuilder.h
+++ b/js/src/jit/IonBuilder.h
@@ -6,16 +6,17 @@
 
 #ifndef jit_IonBuilder_h
 #define jit_IonBuilder_h
 
 // This file declares the data structures for building a MIRGraph from a
 // JSScript.
 
 #include "mozilla/LinkedList.h"
+#include "mozilla/Maybe.h"
 
 #include "jit/BaselineInspector.h"
 #include "jit/BytecodeAnalysis.h"
 #include "jit/IonAnalysis.h"
 #include "jit/IonControlFlow.h"
 #include "jit/IonOptimizationLevels.h"
 #include "jit/MIR.h"
 #include "jit/MIRGenerator.h"
@@ -874,40 +875,38 @@ class IonBuilder : public MIRGenerator,
   InliningResult inlineCallsite(const InliningTargets& targets,
                                 CallInfo& callInfo);
   AbortReasonOr<Ok> inlineCalls(CallInfo& callInfo,
                                 const InliningTargets& targets,
                                 BoolVector& choiceSet,
                                 MGetPropertyCache* maybeCache);
 
   // Inlining helpers.
-  AbortReasonOr<Ok> inlineGenericFallback(const Maybe<CallTargets>& targets,
-                                          CallInfo& callInfo,
-                                          MBasicBlock* dispatchBlock);
-  AbortReasonOr<Ok> inlineObjectGroupFallback(const Maybe<CallTargets>& targets,
-                                              CallInfo& callInfo,
-                                              MBasicBlock* dispatchBlock,
-                                              MObjectGroupDispatch* dispatch,
-                                              MGetPropertyCache* cache,
-                                              MBasicBlock** fallbackTarget);
+  AbortReasonOr<Ok> inlineGenericFallback(
+      const mozilla::Maybe<CallTargets>& targets, CallInfo& callInfo,
+      MBasicBlock* dispatchBlock);
+  AbortReasonOr<Ok> inlineObjectGroupFallback(
+      const mozilla::Maybe<CallTargets>& targets, CallInfo& callInfo,
+      MBasicBlock* dispatchBlock, MObjectGroupDispatch* dispatch,
+      MGetPropertyCache* cache, MBasicBlock** fallbackTarget);
 
   enum AtomicCheckResult { DontCheckAtomicResult, DoCheckAtomicResult };
 
   bool atomicsMeetsPreconditions(
       CallInfo& callInfo, Scalar::Type* arrayElementType,
       bool* requiresDynamicCheck,
       AtomicCheckResult checkResult = DoCheckAtomicResult);
   void atomicsCheckBounds(CallInfo& callInfo, MInstruction** elements,
                           MDefinition** index);
 
   bool testNeedsArgumentCheck(JSFunction* target, CallInfo& callInfo);
 
-  AbortReasonOr<MCall*> makeCallHelper(const Maybe<CallTargets>& targets,
-                                       CallInfo& callInfo);
-  AbortReasonOr<Ok> makeCall(const Maybe<CallTargets>& targets,
+  AbortReasonOr<MCall*> makeCallHelper(
+      const mozilla::Maybe<CallTargets>& targets, CallInfo& callInfo);
+  AbortReasonOr<Ok> makeCall(const mozilla::Maybe<CallTargets>& targets,
                              CallInfo& callInfo);
   AbortReasonOr<Ok> makeCall(JSFunction* target, CallInfo& callInfo);
 
   MDefinition* patchInlinedReturn(CallInfo& callInfo, MBasicBlock* exit,
                                   MBasicBlock* bottom);
   MDefinition* patchInlinedReturns(CallInfo& callInfo, MIRGraphReturns& returns,
                                    MBasicBlock* bottom);
   MDefinition* specializeInlinedReturn(MDefinition* rdef, MBasicBlock* exit);
diff --git a/js/src/jit/IonCacheIRCompiler.cpp b/js/src/jit/IonCacheIRCompiler.cpp
--- a/js/src/jit/IonCacheIRCompiler.cpp
+++ b/js/src/jit/IonCacheIRCompiler.cpp
@@ -1,15 +1,16 @@
 /* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
  * vim: set ts=8 sts=4 et sw=4 tw=99:
  * This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "mozilla/DebugOnly.h"
+#include "mozilla/Maybe.h"
 
 #include "jit/BaselineIC.h"
 #include "jit/CacheIRCompiler.h"
 #include "jit/IonIC.h"
 #include "jit/JSJitFrameIter.h"
 #include "jit/Linker.h"
 #include "jit/SharedICHelpers.h"
 #include "proxy/DeadObjectProxy.h"
@@ -19,16 +20,17 @@
 #include "jit/MacroAssembler-inl.h"
 #include "vm/JSCompartment-inl.h"
 #include "vm/TypeInference-inl.h"
 
 using namespace js;
 using namespace js::jit;
 
 using mozilla::DebugOnly;
+using mozilla::Maybe;
 
 namespace js {
 namespace jit {
 
 // IonCacheIRCompiler compiles CacheIR to IonIC native code.
 class MOZ_RAII IonCacheIRCompiler : public CacheIRCompiler {
  public:
   friend class AutoSaveLiveRegisters;
diff --git a/js/src/jit/arm/MacroAssembler-arm.cpp b/js/src/jit/arm/MacroAssembler-arm.cpp
--- a/js/src/jit/arm/MacroAssembler-arm.cpp
+++ b/js/src/jit/arm/MacroAssembler-arm.cpp
@@ -5,16 +5,17 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "jit/arm/MacroAssembler-arm.h"
 
 #include "mozilla/Attributes.h"
 #include "mozilla/Casting.h"
 #include "mozilla/DebugOnly.h"
 #include "mozilla/MathAlgorithms.h"
+#include "mozilla/Maybe.h"
 
 #include "jit/arm/Simulator-arm.h"
 #include "jit/AtomicOp.h"
 #include "jit/AtomicOperations.h"
 #include "jit/Bailouts.h"
 #include "jit/BaselineFrame.h"
 #include "jit/JitFrames.h"
 #include "jit/MacroAssembler.h"
@@ -23,16 +24,17 @@
 #include "jit/MacroAssembler-inl.h"
 
 using namespace js;
 using namespace jit;
 
 using mozilla::Abs;
 using mozilla::BitwiseCast;
 using mozilla::IsPositiveZero;
+using mozilla::Maybe;
 
 bool isValueDTRDCandidate(ValueOperand& val) {
   // In order to be used for a DTRD memory function, the two target registers
   // need to be a) Adjacent, with the tag larger than the payload, and b)
   // Aligned to a multiple of two.
   if ((val.typeReg().code() != (val.payloadReg().code() + 1))) return false;
   if ((val.payloadReg().code() & 1) != 0) return false;
   return true;
diff --git a/js/src/jsapi-tests/testBinTokenReaderTester.cpp b/js/src/jsapi-tests/testBinTokenReaderTester.cpp
--- a/js/src/jsapi-tests/testBinTokenReaderTester.cpp
+++ b/js/src/jsapi-tests/testBinTokenReaderTester.cpp
@@ -10,23 +10,27 @@
 
 #if defined(XP_WIN)
 #include <windows.h>
 #elif defined(XP_UNIX)
 #include <fcntl.h>
 #include <unistd.h>
 #endif  // defined (XP_WIN) || defined (XP_UNIX)
 
+#include "mozilla/Maybe.h"
+
 #include "frontend/BinTokenReaderTester.h"
 #include "gc/Zone.h"
 
 #include "js/Vector.h"
 
 #include "jsapi-tests/tests.h"
 
+using mozilla::Maybe;
+
 using Tokenizer = js::frontend::BinTokenReaderTester;
 using Chars = Tokenizer::Chars;
 
 // Hack: These tests need access to resources, which are present in the source
 // dir but not copied by our build system. To simplify things, we chdir to the
 // source dir at the start of each test and return to the previous directory
 // afterwards.
 
diff --git a/js/src/jsnum.cpp b/js/src/jsnum.cpp
--- a/js/src/jsnum.cpp
+++ b/js/src/jsnum.cpp
@@ -7,16 +7,17 @@
 /*
  * JS number type and wrapper class.
  */
 
 #include "jsnum.h"
 
 #include "mozilla/ArrayUtils.h"
 #include "mozilla/FloatingPoint.h"
+#include "mozilla/Maybe.h"
 #include "mozilla/RangedPtr.h"
 #include "mozilla/TextUtils.h"
 
 #ifdef HAVE_LOCALECONV
 #include <locale.h>
 #endif
 #include <math.h>
 #include <string.h>
@@ -38,16 +39,17 @@
 #include "vm/StringType-inl.h"
 
 using namespace js;
 
 using mozilla::Abs;
 using mozilla::ArrayLength;
 using mozilla::AsciiAlphanumericToNumber;
 using mozilla::IsAsciiAlphanumeric;
+using mozilla::Maybe;
 using mozilla::MinNumberValue;
 using mozilla::NegativeInfinity;
 using mozilla::PositiveInfinity;
 using mozilla::RangedPtr;
 
 using JS::AutoCheckCannotGC;
 using JS::GenericNaN;
 using JS::ToInt16;
diff --git a/js/src/jsutil.cpp b/js/src/jsutil.cpp
--- a/js/src/jsutil.cpp
+++ b/js/src/jsutil.cpp
@@ -4,33 +4,31 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 /* Various JS utility functions. */
 
 #include "jsutil.h"
 
 #include "mozilla/Assertions.h"
-#include "mozilla/MathAlgorithms.h"
+#include "mozilla/Atomics.h"
 #include "mozilla/Maybe.h"
-#include "mozilla/PodOperations.h"
 #include "mozilla/ThreadLocal.h"
 
 #include <stdio.h>
 
 #include "jstypes.h"
 
 #include "js/Utility.h"
 #include "util/Windows.h"
 #include "vm/HelperThreads.h"
 
 using namespace js;
 
-using mozilla::CeilingLog2Size;
-using mozilla::PodArrayZero;
+using mozilla::Maybe;
 
 #if defined(DEBUG) || defined(JS_OOM_BREAKPOINT)
 /* For OOM testing functionality in Utility.h. */
 namespace js {
 
 mozilla::Atomic<AutoEnterOOMUnsafeRegion*> AutoEnterOOMUnsafeRegion::owner_;
 
 namespace oom {
diff --git a/js/src/vm/JSScript.cpp b/js/src/vm/JSScript.cpp
--- a/js/src/vm/JSScript.cpp
+++ b/js/src/vm/JSScript.cpp
@@ -6,16 +6,17 @@
 
 /*
  * JS script operations.
  */
 
 #include "vm/JSScript-inl.h"
 
 #include "mozilla/DebugOnly.h"
+#include "mozilla/Maybe.h"
 #include "mozilla/MemoryReporting.h"
 #include "mozilla/PodOperations.h"
 #include "mozilla/ScopeExit.h"
 #include "mozilla/Sprintf.h"
 #include "mozilla/Unused.h"
 #include "mozilla/Vector.h"
 
 #include <algorithm>
@@ -60,16 +61,17 @@
 #include "vm/NativeObject-inl.h"
 #include "vm/SharedImmutableStringsCache-inl.h"
 #include "vm/Stack-inl.h"
 
 using namespace js;
 using namespace js::gc;
 using namespace js::frontend;
 
+using mozilla::Maybe;
 using mozilla::PodCopy;
 using mozilla::PodZero;
 
 // Check that JSScript::data hasn't experienced obvious memory corruption.
 // This is a diagnositic for Bug 1367896.
 static void CheckScriptDataIntegrity(JSScript* script) {
   ScopeArray* sa = script->scopes();
   uint8_t* ptr = reinterpret_cast<uint8_t*>(sa->vector);
diff --git a/js/src/vm/SavedStacks.h b/js/src/vm/SavedStacks.h
--- a/js/src/vm/SavedStacks.h
+++ b/js/src/vm/SavedStacks.h
@@ -4,16 +4,17 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef vm_SavedStacks_h
 #define vm_SavedStacks_h
 
 #include "mozilla/Attributes.h"
 #include "mozilla/FastBernoulliTrial.h"
+#include "mozilla/Maybe.h"
 
 #include "jsmath.h"
 
 #include "js/HashTable.h"
 #include "js/Wrapper.h"
 #include "vm/JSContext.h"
 #include "vm/SavedFrame.h"
 #include "vm/Stack.h"
@@ -165,17 +166,17 @@ class SavedStacks {
   MOZ_MUST_USE bool init();
   bool initialized() const { return frames.initialized(); }
   MOZ_MUST_USE bool saveCurrentStack(
       JSContext* cx, MutableHandleSavedFrame frame,
       JS::StackCapture&& capture = JS::StackCapture(JS::AllFrames()));
   MOZ_MUST_USE bool copyAsyncStack(JSContext* cx, HandleObject asyncStack,
                                    HandleString asyncCause,
                                    MutableHandleSavedFrame adoptedStack,
-                                   const Maybe<size_t>& maxFrameCount);
+                                   const mozilla::Maybe<size_t>& maxFrameCount);
   void sweep();
   void trace(JSTracer* trc);
   uint32_t count();
   void clear();
   void chooseSamplingProbability(JSCompartment*);
 
   // Set the sampling random number generator's state to |state0| and
   // |state1|. One or the other must be non-zero. See the comments for
@@ -216,20 +217,19 @@ class SavedStacks {
       stacks.creatingSavedFrame = true;
     }
 
     ~AutoReentrancyGuard() { stacks.creatingSavedFrame = false; }
   };
 
   MOZ_MUST_USE bool insertFrames(JSContext* cx, MutableHandleSavedFrame frame,
                                  JS::StackCapture&& capture);
-  MOZ_MUST_USE bool adoptAsyncStack(JSContext* cx,
-                                    MutableHandleSavedFrame asyncStack,
-                                    HandleAtom asyncCause,
-                                    const Maybe<size_t>& maxFrameCount);
+  MOZ_MUST_USE bool adoptAsyncStack(
+      JSContext* cx, MutableHandleSavedFrame asyncStack, HandleAtom asyncCause,
+      const mozilla::Maybe<size_t>& maxFrameCount);
   MOZ_MUST_USE bool checkForEvalInFramePrev(JSContext* cx,
                                             SavedFrame::HandleLookup lookup);
   SavedFrame* getOrCreateSavedFrame(JSContext* cx,
                                     SavedFrame::HandleLookup lookup);
   SavedFrame* createFrameFromLookup(JSContext* cx,
                                     SavedFrame::HandleLookup lookup);
 
   // Cache for memoizing PCToLineNumber lookups.
diff --git a/js/src/vm/Stack-inl.h b/js/src/vm/Stack-inl.h
--- a/js/src/vm/Stack-inl.h
+++ b/js/src/vm/Stack-inl.h
@@ -4,16 +4,17 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef vm_Stack_inl_h
 #define vm_Stack_inl_h
 
 #include "vm/Stack.h"
 
+#include "mozilla/Maybe.h"
 #include "mozilla/PodOperations.h"
 
 #include "jit/BaselineFrame.h"
 #include "jit/RematerializedFrame.h"
 #include "js/Debug.h"
 #include "vm/EnvironmentObject.h"
 #include "vm/GeneratorObject.h"
 #include "vm/JSContext.h"
@@ -770,17 +771,17 @@ inline bool InterpreterActivation::resum
   InterpreterStack& stack = cx_->interpreterStack();
   if (!stack.resumeGeneratorCallFrame(cx_, regs_, callee, newTarget, envChain))
     return false;
 
   MOZ_ASSERT(regs_.fp()->script()->compartment() == compartment_);
   return true;
 }
 
-/* static */ inline Maybe<LiveSavedFrameCache::FramePtr>
+/* static */ inline mozilla::Maybe<LiveSavedFrameCache::FramePtr>
 LiveSavedFrameCache::FramePtr::create(const FrameIter& iter) {
   if (iter.done()) return mozilla::Nothing();
 
   if (iter.isPhysicalJitFrame())
     return mozilla::Some(FramePtr(iter.physicalJitFrame()));
 
   if (!iter.hasUsableAbstractFramePtr()) return mozilla::Nothing();
 
