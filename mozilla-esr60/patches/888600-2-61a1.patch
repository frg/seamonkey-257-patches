# HG changeset patch
# User Peter Van der Beken <peterv@propagandism.org>
# Date 1499184198 -7200
# Node ID ddffc18a48a16458d3cc25996469824b19ead16d
# Parent  6fd37875b4d6080f2cca3ec9d8b55ce7a1d475da
Bug 888600 - Move ContentFrameMessageManager to WebIDL. Part 2: Various test fixes to prepare for using WebIDL bindings for MessageManager classes. r=bz.

diff --git a/dom/file/tests/test_ipc_messagemanager_blob.html b/dom/file/tests/test_ipc_messagemanager_blob.html
--- a/dom/file/tests/test_ipc_messagemanager_blob.html
+++ b/dom/file/tests/test_ipc_messagemanager_blob.html
@@ -67,20 +67,20 @@
 
           // Make sure this one is always last.
           new Blob(["this ", "is ", "a ", "great ", "success!"],
                    {"type" : "text\/plain"}),
         ];
         let receivedMessageIndex = 0;
 
         let mm = SpecialPowers.getBrowserFrameMessageManager(iframe);
-        mm.addMessageListener("test:ipcClonedMessage", function(message) {
+        mm.addMessageListener("test:ipcClonedMessage", SpecialPowers.wrapCallback(function(message) {
           let data = message.json;
 
-          if (data instanceof Blob) {
+          if (SpecialPowers.call_Instanceof(data, Blob)) {
             is(receivedMessageIndex, messages.length - 1, "Blob is last");
             is (data.size,
                 messages[receivedMessageIndex].size,
                 "Correct blob size");
             is (data.type,
                 messages[receivedMessageIndex].type,
                 "Correct blob type");
 
@@ -99,25 +99,25 @@
             reader2.onload = function() {
               result2 = reader2.result == blobString ? reader2.result : "bad2";
               if (result1) {
                 is(result1, result2, "Same results");
                 done();
               }
             };
 
-            reader1.readAsText(data);
+            SpecialPowers.wrap(reader1).readAsText(data);
             reader2.readAsText(messages[receivedMessageIndex]);
             return;
           }
 
           is(message.json,
              messages[receivedMessageIndex++],
              "Got correct round-tripped response");
-        });
+        }));
         mm.loadFrameScript("data:,(" + childFrameScript.toString() + ")();",
                            false);
 
         for (let message of messages) {
           mm.sendAsyncMessage("test:ipcClonedMessage", message);
         }
       });
 
diff --git a/dom/indexedDB/test/head.js b/dom/indexedDB/test/head.js
--- a/dom/indexedDB/test/head.js
+++ b/dom/indexedDB/test/head.js
@@ -75,21 +75,24 @@ function dismissNotification(popup)
     EventUtils.synthesizeKey("KEY_Escape");
   });
 }
 
 function waitForMessage(aMessage, browser)
 {
   return new Promise((resolve, reject) => {
     /* eslint-disable no-undef */
+    // When contentScript runs, "this" is a ContentFrameMessageManager (so that's where
+    // addEventListener will add the listener), but the non-bubbling "message" event is
+    // sent to the Window involved, so we need a capturing listener.
     function contentScript() {
       addEventListener("message", function(event) {
         sendAsyncMessage("testLocal:message",
           {message: event.data});
-      }, {once: true}, true);
+      }, {once: true, capture: true}, true);
     }
     /* eslint-enable no-undef */
 
     let script = "data:,(" + contentScript.toString() + ")();";
 
     let mm = browser.selectedBrowser.messageManager;
 
     mm.addMessageListener("testLocal:message", function listener(msg) {
diff --git a/dom/indexedDB/test/test_message_manager_ipc.html b/dom/indexedDB/test/test_message_manager_ipc.html
--- a/dom/indexedDB/test/test_message_manager_ipc.html
+++ b/dom/indexedDB/test/test_message_manager_ipc.html
@@ -209,69 +209,69 @@ function parentFrameScript(mm) {
 
   function grabAndContinue(arg) {
     testGenerator.next(arg);
   }
 
   function* testSteps() {
     let result = yield undefined;
 
-    is(Array.isArray(result), true, "Child delivered an array of results");
+    is(SpecialPowers.Cu.getClassName(result, true), "Array", "Child delivered an array of results");
     is(result.length, 2, "Child delivered two results");
 
     let blob = result[0];
-    is(blob instanceof Blob, true, "Child delivered a blob");
+    is(SpecialPowers.call_Instanceof(blob, Blob), true, "Child delivered a blob");
     is(blob.size, blobText.length, "Blob has correct size");
     is(blob.type, blobType, "Blob has correct type");
 
     let slice = result[1];
-    is(slice instanceof Blob, true, "Child delivered a slice");
+    is(SpecialPowers.call_Instanceof(slice, Blob), true, "Child delivered a slice");
     is(slice.size, blobData[0].length, "Slice has correct size");
     is(slice.type, blobType, "Slice has correct type");
 
     info("Reading blob");
 
     let reader = new FileReader();
     reader.onload = grabAndContinue;
-    reader.readAsText(blob);
+    SpecialPowers.wrap(reader).readAsText(blob);
     yield undefined;
 
     is(reader.result, blobText, "Blob has correct data");
 
     info("Reading slice");
 
     reader = new FileReader();
     reader.onload = grabAndContinue;
-    reader.readAsText(slice);
+    SpecialPowers.wrap(reader).readAsText(slice);
     yield undefined;
 
     is(reader.result, blobData[0], "Slice has correct data");
 
     slice = blob.slice(0, blobData[0].length, blobType);
 
-    is(slice instanceof Blob, true, "Made a new slice from blob");
+    is(SpecialPowers.call_Instanceof(slice, Blob), true, "Child delivered a slice");
     is(slice.size, blobData[0].length, "Second slice has correct size");
     is(slice.type, blobType, "Second slice has correct type");
 
     info("Reading second slice");
 
     reader = new FileReader();
     reader.onload = grabAndContinue;
-    reader.readAsText(slice);
+    SpecialPowers.wrap(reader).readAsText(slice);
     yield undefined;
 
     is(reader.result, blobData[0], "Second slice has correct data");
 
     SimpleTest.finish();
   }
 
   let testGenerator = testSteps();
   testGenerator.next();
 
-  mm.addMessageListener(messageName, function(message) {
+  mm.addMessageListener(messageName, SpecialPowers.wrapCallback(function(message) {
     let data = message.data;
     switch (data.op) {
       case "info": {
         info(data.msg);
         break;
       }
 
       case "ok": {
@@ -284,17 +284,17 @@ function parentFrameScript(mm) {
         break;
       }
 
       default: {
         ok(false, "Unknown op: " + data.op);
         SimpleTest.finish();
       }
     }
-  });
+  }));
 
   mm.loadFrameScript("data:,(" + childFrameScript.toString() + ")();",
                       false);
 
   mm.sendAsyncMessage(messageName, blob);
 }
 
 function setup() {
diff --git a/dom/ipc/tests/test_blob_sliced_from_child_process.html b/dom/ipc/tests/test_blob_sliced_from_child_process.html
--- a/dom/ipc/tests/test_blob_sliced_from_child_process.html
+++ b/dom/ipc/tests/test_blob_sliced_from_child_process.html
@@ -48,37 +48,37 @@ function parentFrameScript(mm) {
   const sliceText = "an";
 
   let receivedBlob = false;
   let receivedSlice = false;
 
   let finishedTestingBlob = false;
   let finishedTestingSlice = false;
 
-  mm.addMessageListener(messageName, function(message) {
+  mm.addMessageListener(messageName, SpecialPowers.wrapCallback(function(message) {
     if ("blob" in message.data) {
       is(receivedBlob, false, "Have not yet received Blob");
       is(receivedSlice, false, "Have not yet received Slice");
       is(finishedTestingBlob, false, "Have not yet finished testing Blob");
       is(finishedTestingSlice, false, "Have not yet finished testing Slice");
 
       receivedBlob = true;
 
       let blob = message.data.blob;
 
-      ok(blob instanceof Blob, "Received a Blob");
+      ok(SpecialPowers.call_Instanceof(blob, Blob), "Received a Blob");
       is(blob.size, blobText.length, "Blob has correct size");
       is(blob.type, blobType, "Blob has correct type");
 
       let slice = blob.slice(blobText.length -
                                 blobData[blobData.length - 1].length,
                               blob.size,
                               blobType);
 
-      ok(slice instanceof Blob, "Slice returned a Blob");
+      ok(SpecialPowers.call_Instanceof(slice, Blob), "Slice returned a Blob");
       is(slice.size,
           blobData[blobData.length - 1].length,
           "Slice has correct size");
       is(slice.type, blobType, "Slice has correct type");
 
       let reader = new FileReader();
       reader.onload = function() {
         is(reader.result,
@@ -86,63 +86,63 @@ function parentFrameScript(mm) {
             "Slice has correct data");
 
         finishedTestingBlob = true;
 
         if (finishedTestingSlice) {
           SimpleTest.finish();
         }
       };
-      reader.readAsText(slice);
+      SpecialPowers.wrap(reader).readAsText(slice);
 
       return;
     }
 
     if ("slice" in message.data) {
       is(receivedBlob, true, "Already received Blob");
       is(receivedSlice, false, "Have not yet received Slice");
       is(finishedTestingSlice, false, "Have not yet finished testing Slice");
 
       receivedSlice = true;
 
       let slice = message.data.slice;
 
-      ok(slice instanceof Blob, "Received a Blob for slice");
+      ok(SpecialPowers.call_Instanceof(slice, Blob), "Received a Blob for slice");
       is(slice.size, sliceText.length, "Slice has correct size");
       is(slice.type, blobType, "Slice has correct type");
 
       let reader = new FileReader();
       reader.onload = function() {
         is(reader.result, sliceText, "Slice has correct data");
 
         let slice2 = slice.slice(1, 2, blobType);
 
-        ok(slice2 instanceof Blob, "Slice returned a Blob");
+        ok(SpecialPowers.call_Instanceof(slice2, Blob), "Slice returned a Blob");
         is(slice2.size, 1, "Slice has correct size");
         is(slice2.type, blobType, "Slice has correct type");
 
         let reader2 = new FileReader();
         reader2.onload = function() {
           is(reader2.result, sliceText[1], "Slice has correct data");
 
           finishedTestingSlice = true;
 
           if (finishedTestingBlob) {
             SimpleTest.finish();
           }
         };
-        reader2.readAsText(slice2);
+        SpecialPowers.wrap(reader2).readAsText(slice2);
       };
-      reader.readAsText(slice);
+      SpecialPowers.wrap(reader).readAsText(slice);
 
       return;
     }
 
     ok(false, "Received a bad message: " + JSON.stringify(message.data));
-  });
+  }));
 
   mm.loadFrameScript("data:,(" + childFrameScript.toString() + ")();",
                       false);
 }
 
 function setup() {
   info("Got load event");
 
diff --git a/dom/ipc/tests/test_blob_sliced_from_parent_process.html b/dom/ipc/tests/test_blob_sliced_from_parent_process.html
--- a/dom/ipc/tests/test_blob_sliced_from_parent_process.html
+++ b/dom/ipc/tests/test_blob_sliced_from_parent_process.html
@@ -112,33 +112,33 @@ function parentFrameScript(mm) {
 
   function grabAndContinue(arg) {
     testGenerator.next(arg);
   }
 
   function* testSteps() {
     let slice = yield undefined;
 
-    ok(slice instanceof Blob, "Received a Blob");
+    ok(SpecialPowers.call_Instanceof(slice, Blob), "Received a Blob");
     is(slice.size, sliceText.length, "Slice has correct size");
     is(slice.type, blobType, "Slice has correct type");
 
     let reader = new FileReader();
     reader.onload = grabAndContinue;
-    reader.readAsText(slice);
+    SpecialPowers.wrap(reader).readAsText(slice);
     yield undefined;
 
     is(reader.result, sliceText, "Slice has correct data");
     SimpleTest.finish();
   }
 
   let testGenerator = testSteps();
   testGenerator.next();
 
-  mm.addMessageListener(messageName, function(message) {
+  mm.addMessageListener(messageName, SpecialPowers.wrapCallback(function(message) {
     let data = message.data;
     switch (data.op) {
       case "info": {
         info(data.msg);
         break;
       }
 
       case "ok": {
@@ -151,17 +151,17 @@ function parentFrameScript(mm) {
         break;
       }
 
       default: {
         ok(false, "Unknown op: " + data.op);
         SimpleTest.finish();
       }
     }
-  });
+  }));
 
   mm.loadFrameScript("data:,(" + childFrameScript.toString() + ")();",
                       false);
 
   let blob = new Blob(blobData, { type: blobType });
   mm.sendAsyncMessage(messageName, blob);
 }
 
diff --git a/dom/ipc/tests/test_bug1086684.html b/dom/ipc/tests/test_bug1086684.html
--- a/dom/ipc/tests/test_bug1086684.html
+++ b/dom/ipc/tests/test_bug1086684.html
@@ -47,17 +47,18 @@
     let test;
     function* testStructure(mm) {
       let value;
 
       function testDone(msg) {
         test.next(msg.data.value);
       }
 
-      mm.addMessageListener("testBug1086684:childDone", testDone);
+      mm.addMessageListener("testBug1086684:childDone",
+                            SpecialPowers.wrapCallback(testDone));
 
       let blob = new Blob([]);
       let file = new File([blob], "helloworld.txt", { type: "text/plain" });
 
       mm.sendAsyncMessage("testBug1086684:parentReady", { file });
       value = yield;
 
       // Note that the "helloworld.txt" passed in above doesn't affect the
diff --git a/dom/ipc/tests/test_cpow_cookies.html b/dom/ipc/tests/test_cpow_cookies.html
--- a/dom/ipc/tests/test_cpow_cookies.html
+++ b/dom/ipc/tests/test_cpow_cookies.html
@@ -35,17 +35,18 @@
     let test;
     function* testStructure(mm) {
       let lastResult;
 
       function testDone(msg) {
         test.next(msg.data);
       }
 
-      mm.addMessageListener("testCPOWCookies:test1Finished", testDone);
+      mm.addMessageListener("testCPOWCookies:test1Finished",
+                            SpecialPowers.wrapCallback(testDone));
 
       mm.sendAsyncMessage("testCPOWCookies:test1", {});
       lastResult = yield;
       ok(lastResult.pass, "got the right answer and didn't crash");
 
       SimpleTest.finish();
     }
 
diff --git a/dom/presentation/tests/mochitest/test_presentation_dc_receiver_oop.html b/dom/presentation/tests/mochitest/test_presentation_dc_receiver_oop.html
--- a/dom/presentation/tests/mochitest/test_presentation_dc_receiver_oop.html
+++ b/dom/presentation/tests/mochitest/test_presentation_dc_receiver_oop.html
@@ -125,27 +125,27 @@ function setup() {
     gScript.addMessageListener('control-channel-closed', function controlChannelClosedHandler(aReason) {
       gScript.removeMessageListener('control-channel-closed', controlChannelClosedHandler);
       is(aReason, SpecialPowers.Cr.NS_OK, "The control channel is closed normally.");
     });
 
     var mm = SpecialPowers.getBrowserFrameMessageManager(receiverIframe);
     mm.addMessageListener('check-navigator', function checknavigatorHandler(aSuccess) {
       mm.removeMessageListener('check-navigator', checknavigatorHandler);
-      ok(aSuccess.data.data, "buildDataChannel get correct window object");
+      ok(SpecialPowers.wrap(aSuccess).data.data, "buildDataChannel get correct window object");
     });
 
     mm.addMessageListener('data-transport-notification-enabled', function dataTransportNotificationEnabledHandler() {
       mm.removeMessageListener('data-transport-notification-enabled', dataTransportNotificationEnabledHandler);
       info("Data notification is enabled for data transport channel.");
     });
 
     mm.addMessageListener('data-transport-closed', function dataTransportClosedHandler(aReason) {
       mm.removeMessageListener('data-transport-closed', dataTransportClosedHandler);
-      is(aReason.data.data, SpecialPowers.Cr.NS_OK, "The data transport should be closed normally.");
+      is(SpecialPowers.wrap(aReason).data.data, SpecialPowers.Cr.NS_OK, "The data transport should be closed normally.");
     });
 
     aResolve();
   });
 }
 
 function testIncomingSessionRequest() {
   return new Promise(function(aResolve, aReject) {
diff --git a/dom/quota/test/head.js b/dom/quota/test/head.js
--- a/dom/quota/test/head.js
+++ b/dom/quota/test/head.js
@@ -75,21 +75,24 @@ function dismissNotification(popup, win)
   executeSoon(function () {
     EventUtils.synthesizeKey("VK_ESCAPE", {}, win);
   });
 }
 
 function waitForMessage(aMessage, browser)
 {
   return new Promise((resolve, reject) => {
+    // When contentScript runs, "this" is a ContentFrameMessageManager (so that's where
+    // addEventListener will add the listener), but the non-bubbling "message" event is
+    // sent to the Window involved, so we need a capturing listener.
     function contentScript() {
       addEventListener("message", function(event) {
         sendAsyncMessage("testLocal:persisted",
           {persisted: event.data});
-      }, {once: true}, true);
+      }, {once: true, capture: true}, true);
     }
 
     let script = "data:,(" + contentScript.toString() + ")();";
 
     let mm = browser.selectedBrowser.messageManager;
 
     mm.addMessageListener("testLocal:persisted", function listener(msg) {
       mm.removeMessageListener("testLocal:persisted", listener);
diff --git a/testing/marionette/proxy.js b/testing/marionette/proxy.js
--- a/testing/marionette/proxy.js
+++ b/testing/marionette/proxy.js
@@ -305,17 +305,17 @@ proxy.AsyncMessageChannel = class {
   }
 
   removeListener_(path) {
     if (!this.listeners_.has(path)) {
       return true;
     }
 
     let l = this.listeners_.get(path);
-    globalMessageManager.removeMessageListener(path, l[1]);
+    globalMessageManager.removeMessageListener(path, l);
     return this.listeners_.delete(path);
   }
 
   removeAllListeners_() {
     let ok = true;
     for (let [p] of this.listeners_) {
       ok |= this.removeListener_(p);
     }
diff --git a/toolkit/components/contentprefs/tests/mochitest/test_remoteContentPrefs.html b/toolkit/components/contentprefs/tests/mochitest/test_remoteContentPrefs.html
--- a/toolkit/components/contentprefs/tests/mochitest/test_remoteContentPrefs.html
+++ b/toolkit/components/contentprefs/tests/mochitest/test_remoteContentPrefs.html
@@ -232,38 +232,38 @@
         d.resolve = resolve;
         d.reject = reject;
       });
       return d;
     }
 
     async function testStructure(mm, isPrivate) {
       var curTest;
-      function testDone(msg) {
+      var testDone = SpecialPowers.wrapCallback(function testDone(msg) {
         info(`in testDone ${msg.name}`);
         curTest.resolve();
-      }
+      });
 
       mm.addMessageListener("testRemoteContentPrefs:test1Finished", testDone);
       mm.addMessageListener("testRemoteContentPrefs:test2Finished", testDone);
       mm.addMessageListener("testRemoteContentPrefs:test3Finished", testDone);
       mm.addMessageListener("testRemoteContentPrefs:test4Finished", testDone);
 
-      mm.addMessageListener("testRemoteContentPrefs:fail", function(msg) {
+      mm.addMessageListener("testRemoteContentPrefs:fail", SpecialPowers.wrapCallback(function(msg) {
         ok(false, msg.data.reason);
         SimpleTest.finish();
-      });
+      }));
 
-      mm.addMessageListener("testRemoteContentPrefs:ok", (msg) => {
+      mm.addMessageListener("testRemoteContentPrefs:ok", SpecialPowers.wrapCallback((msg) => {
         let test = msg.data.test;
         ok(...test);
-      });
-      mm.addMessageListener("testRemoteContentPrefs:info", (msg) => {
+      }));
+      mm.addMessageListener("testRemoteContentPrefs:info", SpecialPowers.wrapCallback((msg) => {
         info(msg.data.note);
-      });
+      }));
 
       curTest = Defer();
       mm.sendAsyncMessage("testRemoteContentPrefs:test1", {});
       await curTest.promise;
 
       curTest = Defer();
       var cps = SpecialPowers.Cc["@mozilla.org/content-pref/service;1"]
                              .getService(SpecialPowers.Ci.nsIContentPrefService2);
@@ -278,33 +278,33 @@
       mm.sendAsyncMessage("testRemoteContentPrefs:test2", {});
       await curTest.promise;
 
       curTest = Defer();
       mm.sendAsyncMessage("testRemoteContentPrefs:test3", {});
       await curTest.promise;
 
       curTest = Defer();
-      mm.addMessageListener("testRemoteContentPrefs:getPref", function(msg) {
+      mm.addMessageListener("testRemoteContentPrefs:getPref", SpecialPowers.wrapCallback(function(msg) {
         let results = [];
         cps.getByDomainAndName(msg.data.group, msg.data.name, null, {
           handleResult(pref) {
             info("received handleResult");
             results.push(pref);
           },
           handleCompletion(reason) {
             mm.sendAsyncMessage("testRemoteContentPrefs:prefResults",
                                 { results });
           },
           handleError(rv) {
             ok(false, `failed to get pref ${rv}`);
             curTest.reject("got unexpected error");
           }
         });
-      });
+      }));
 
       mm.sendAsyncMessage("testRemoteContentPrefs:test4", {});
       await curTest.promise;
 
       document.getElementById("iframe").remove();
     }
 
     function runTest(isPrivate) {
diff --git a/toolkit/mozapps/extensions/test/mochitest/test_bug687194.html b/toolkit/mozapps/extensions/test/mochitest/test_bug687194.html
--- a/toolkit/mozapps/extensions/test/mochitest/test_bug687194.html
+++ b/toolkit/mozapps/extensions/test/mochitest/test_bug687194.html
@@ -42,19 +42,19 @@
                                         { threw, result });
       });
     }
 
     let test;
     function* testStructure(mm) {
       let lastResult;
 
-      mm.addMessageListener("test687194:resolveChromeURI:Answer", function(msg) {
+      mm.addMessageListener("test687194:resolveChromeURI:Answer", SpecialPowers.wrapCallback(function(msg) {
         test.next(msg.data);
-      });
+      }));
 
       mm.sendAsyncMessage("test687194:resolveChromeURI",
                           { URI: "chrome://bug687194/content/e10sbug.js" });
       lastResult = yield;
       is(lastResult.threw, true, "URI shouldn't resolve to begin with");
 
       let { AddonManager } = SpecialPowers.Cu.import("resource://gre/modules/AddonManager.jsm", {});
       const INSTALL_URI =
