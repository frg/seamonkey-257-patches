# HG changeset patch
# User Dan Minor <dminor@mozilla.com>
# Date 1524513807 14400
# Node ID bff5f718c89dcd428d8849918fa334ea3a23e70a
# Parent  191c5364d124166a6e229eb531b469a33fb5f0df
Bug 1445683 - Updates to AOMDecoder; r=jya

MozReview-Commit-ID: GuTvnPkR1Er

diff --git a/dom/media/platforms/agnostic/AOMDecoder.cpp b/dom/media/platforms/agnostic/AOMDecoder.cpp
--- a/dom/media/platforms/agnostic/AOMDecoder.cpp
+++ b/dom/media/platforms/agnostic/AOMDecoder.cpp
@@ -54,16 +54,17 @@ static MediaResult InitContext(AOMDecode
     decode_threads = 4;
   }
   decode_threads = std::min(decode_threads, PR_GetNumberOfProcessors());
 
   aom_codec_dec_cfg_t config;
   PodZero(&config);
   config.threads = decode_threads;
   config.w = config.h = 0;  // set after decode
+  config.allow_lowbitdepth = true;
 
   aom_codec_flags_t flags = 0;
 
   auto res = aom_codec_dec_init(aCtx, dx, &config, flags);
   if (res != AOM_CODEC_OK) {
     LOGEX_RESULT(&aAOMDecoder, res, "Codec initialization failed, res=%d",
                  int(res));
     return MediaResult(NS_ERROR_DOM_MEDIA_FATAL_ERR,
@@ -120,26 +121,24 @@ static aom_codec_err_t highbd_img_downsh
     return AOM_CODEC_INVALID_PARAM;
   if (dst->fmt != (src->fmt & ~AOM_IMG_FMT_HIGHBITDEPTH))
     return AOM_CODEC_INVALID_PARAM;
   if (down_shift < 0) return AOM_CODEC_INVALID_PARAM;
   switch (dst->fmt) {
     case AOM_IMG_FMT_I420:
     case AOM_IMG_FMT_I422:
     case AOM_IMG_FMT_I444:
-    case AOM_IMG_FMT_I440:
       break;
     default:
       return AOM_CODEC_INVALID_PARAM;
   }
   switch (src->fmt) {
     case AOM_IMG_FMT_I42016:
     case AOM_IMG_FMT_I42216:
     case AOM_IMG_FMT_I44416:
-    case AOM_IMG_FMT_I44016:
       break;
     default:
       // We don't support anything that's not 16 bit
       return AOM_CODEC_UNSUP_BITSTREAM;
   }
   for (plane = 0; plane < 3; plane++) {
     int w = src->d_w;
     int h = src->d_h;
@@ -168,18 +167,17 @@ RefPtr<MediaDataDecoder::DecodePromise> 
   MOZ_ASSERT(mTaskQueue->IsCurrentThreadIn());
 
 #if defined(DEBUG)
   NS_ASSERTION(
       IsKeyframe(*aSample) == aSample->mKeyframe,
       "AOM Decode Keyframe error sample->mKeyframe and si.si_kf out of sync");
 #endif
 
-  if (aom_codec_err_t r = aom_codec_decode(&mCodec, aSample->Data(),
-                                           aSample->Size(), nullptr, 0)) {
+  if (aom_codec_err_t r = aom_codec_decode(&mCodec, aSample->Data(), aSample->Size(), nullptr)) {
     LOG_RESULT(r, "Decode error!");
     return DecodePromise::CreateAndReject(
         MediaResult(NS_ERROR_DOM_MEDIA_DECODE_ERR,
                     RESULT_DETAIL("AOM error decoding AV1 sample: %s",
                                   aom_codec_err_to_string(r))),
         __func__);
   }
 
@@ -299,22 +297,17 @@ RefPtr<MediaDataDecoder::DecodePromise> 
 
 /* static */
 bool AOMDecoder::IsAV1(const nsACString& aMimeType) {
   return aMimeType.EqualsLiteral("video/av1");
 }
 
 /* static */
 bool AOMDecoder::IsSupportedCodec(const nsAString& aCodecType) {
-  // While AV1 is under development, we describe support
-  // for a specific aom commit hash so sites can check
-  // compatibility.
-  auto version = NS_ConvertASCIItoUTF16("av1.experimental.");
-  version.AppendLiteral("d14c5bb4f336ef1842046089849dee4a301fbbf0");
-  return aCodecType.EqualsLiteral("av1") || aCodecType.Equals(version);
+  return aCodecType.EqualsLiteral("av1");
 }
 
 /* static */
 bool AOMDecoder::IsKeyframe(Span<const uint8_t> aBuffer) {
   aom_codec_stream_info_t info;
   PodZero(&info);
 
   auto res = aom_codec_peek_stream_info(aom_codec_av1_dx(), aBuffer.Elements(),
