# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1520768931 -3600
# Node ID 0ec71c2eb97798cd1ba7623399f6283ff39e2444
# Parent  3780637ec170ee9b2e236b6fec3643ccd9b8d306
Bug 1444580: Devirtualize nsIDocument::CreateElement / CreateElementNS. r=smaug

MozReview-Commit-ID: KSd1xNIT7te

diff --git a/dom/base/nsDocument.cpp b/dom/base/nsDocument.cpp
--- a/dom/base/nsDocument.cpp
+++ b/dom/base/nsDocument.cpp
@@ -5251,17 +5251,17 @@ static CSSPseudoElementType GetPseudoEle
     aRv.Throw(NS_ERROR_DOM_SYNTAX_ERR);
     return CSSPseudoElementType::NotPseudo;
   }
   RefPtr<nsAtom> pseudo = NS_Atomize(Substring(aString, 1));
   return nsCSSPseudoElements::GetPseudoType(
       pseudo, nsCSSProps::EnabledState::eInUASheets);
 }
 
-already_AddRefed<Element> nsDocument::CreateElement(
+already_AddRefed<Element> nsIDocument::CreateElement(
     const nsAString& aTagName, const ElementCreationOptionsOrString& aOptions,
     ErrorResult& rv) {
   rv = nsContentUtils::CheckQName(aTagName, false);
   if (rv.Failed()) {
     return nullptr;
   }
 
   bool needsLowercase = IsHTMLDocument() && !IsLowercaseASCII(aTagName);
@@ -5302,17 +5302,17 @@ already_AddRefed<Element> nsDocument::Cr
 
   if (is) {
     elem->SetAttr(kNameSpaceID_None, nsGkAtoms::is, *is, true);
   }
 
   return elem.forget();
 }
 
-already_AddRefed<Element> nsDocument::CreateElementNS(
+already_AddRefed<Element> nsIDocument::CreateElementNS(
     const nsAString& aNamespaceURI, const nsAString& aQualifiedName,
     const ElementCreationOptionsOrString& aOptions, ErrorResult& rv) {
   RefPtr<mozilla::dom::NodeInfo> nodeInfo;
   rv = nsContentUtils::GetNodeInfoFromQName(aNamespaceURI, aQualifiedName,
                                             mNodeInfoManager, ELEMENT_NODE,
                                             getter_AddRefs(nodeInfo));
   if (rv.Failed()) {
     return nullptr;
@@ -7279,17 +7279,17 @@ already_AddRefed<Element> nsDocument::Cr
   nsAutoString qName;
   if (aPrefix) {
     aPrefix->ToString(qName);
     qName.Append(':');
   }
   qName.Append(aName);
 
   // Note: "a:b:c" is a valid name in non-namespaces XML, and
-  // nsDocument::CreateElement can call us with such a name and no prefix,
+  // nsIDocument::CreateElement can call us with such a name and no prefix,
   // which would cause an error if we just used true here.
   bool nsAware = aPrefix != nullptr || aNamespaceID != GetDefaultNamespaceID();
   NS_ASSERTION(NS_SUCCEEDED(nsContentUtils::CheckQName(qName, nsAware)),
                "Don't pass invalid prefixes to nsDocument::CreateElem, "
                "check caller.");
 #endif
 
   RefPtr<mozilla::dom::NodeInfo> nodeInfo;
diff --git a/dom/base/nsDocument.h b/dom/base/nsDocument.h
--- a/dom/base/nsDocument.h
+++ b/dom/base/nsDocument.h
@@ -665,26 +665,16 @@ class nsDocument : public nsIDocument,
   mozilla::dom::Promise* GetOrientationPendingPromise() const override;
 
   virtual void DocAddSizeOfExcludingThis(
       nsWindowSizes& aWindowSizes) const override;
   // DocAddSizeOfIncludingThis is inherited from nsIDocument.
 
   virtual nsIDOMNode* AsDOMNode() override { return this; }
 
-  // WebIDL bits
-  virtual already_AddRefed<Element> CreateElement(
-      const nsAString& aTagName,
-      const mozilla::dom::ElementCreationOptionsOrString& aOptions,
-      ErrorResult& rv) override;
-  virtual already_AddRefed<Element> CreateElementNS(
-      const nsAString& aNamespaceURI, const nsAString& aQualifiedName,
-      const mozilla::dom::ElementCreationOptionsOrString& aOptions,
-      mozilla::ErrorResult& rv) override;
-
   virtual void UnblockDOMContentLoaded() override;
 
  protected:
   friend class nsNodeUtils;
 
   void DispatchContentLoadedEvents();
 
   void RetrieveRelevantHeaders(nsIChannel* aChannel);
diff --git a/dom/base/nsIDocument.h b/dom/base/nsIDocument.h
--- a/dom/base/nsIDocument.h
+++ b/dom/base/nsIDocument.h
@@ -2639,24 +2639,24 @@ class nsIDocument : public nsINode,
     eConnected,
     eDisconnected,
     eAdopted,
     eAttributeChanged
   };
 
   nsIDocument* GetTopLevelContentDocument();
 
-  virtual already_AddRefed<Element> CreateElement(
+  already_AddRefed<Element> CreateElement(
       const nsAString& aTagName,
       const mozilla::dom::ElementCreationOptionsOrString& aOptions,
-      mozilla::ErrorResult& rv) = 0;
-  virtual already_AddRefed<Element> CreateElementNS(
+      mozilla::ErrorResult& rv);
+  already_AddRefed<Element> CreateElementNS(
       const nsAString& aNamespaceURI, const nsAString& aQualifiedName,
       const mozilla::dom::ElementCreationOptionsOrString& aOptions,
-      mozilla::ErrorResult& rv) = 0;
+      mozilla::ErrorResult& rv);
   already_AddRefed<mozilla::dom::DocumentFragment> CreateDocumentFragment()
       const;
   already_AddRefed<nsTextNode> CreateTextNode(const nsAString& aData) const;
   already_AddRefed<nsTextNode> CreateEmptyTextNode() const;
   already_AddRefed<mozilla::dom::Comment> CreateComment(
       const nsAString& aData) const;
   already_AddRefed<mozilla::dom::ProcessingInstruction>
   CreateProcessingInstruction(const nsAString& target, const nsAString& data,
