# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1528021348 -7200
# Node ID f58f5333e3755cc51368041782e59adf79509a4f
# Parent  54cc97c4435be5ef9ed4e00e5afdaa92ea74f7c6
Bug 1464772 part 2 - Remove RealmBehaviorsRef(obj), change RealmBehaviorsRef(compartment) to take a realm. r=luke

At some point I'll audit the obj->realm() calls for CCWs; removing RealmBehaviorsRef(obj) prepares for that.

Also, RealmBehaviorsRef(realm) could be removed and we could use RealmBehaviorsRef(cx) everywhere, but it seems reasonable to keep it.

diff --git a/js/src/jsapi-tests/testSourcePolicy.cpp b/js/src/jsapi-tests/testSourcePolicy.cpp
--- a/js/src/jsapi-tests/testSourcePolicy.cpp
+++ b/js/src/jsapi-tests/testSourcePolicy.cpp
@@ -2,17 +2,17 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "jsapi-tests/tests.h"
 #include "vm/JSScript.h"
 
 BEGIN_TEST(testBug795104) {
   JS::CompileOptions opts(cx);
-  JS::RealmBehaviorsRef(cx->compartment()).setDiscardSource(true);
+  JS::RealmBehaviorsRef(cx->realm()).setDiscardSource(true);
 
   const size_t strLen = 60002;
   char* s = static_cast<char*>(JS_malloc(cx, strLen));
   CHECK(s);
 
   s[0] = '"';
   memset(s + 1, 'x', strLen - 2);
   s[strLen - 1] = '"';
diff --git a/js/src/jsapi.cpp b/js/src/jsapi.cpp
--- a/js/src/jsapi.cpp
+++ b/js/src/jsapi.cpp
@@ -1602,22 +1602,18 @@ bool JS::RealmCreationOptions::getShared
 JS::RealmCreationOptions&
 JS::RealmCreationOptions::setSharedMemoryAndAtomicsEnabled(bool flag) {
 #if defined(ENABLE_SHARED_ARRAY_BUFFER)
   sharedMemoryAndAtomics_ = flag;
 #endif
   return *this;
 }
 
-JS::RealmBehaviors& JS::RealmBehaviorsRef(JSCompartment* compartment) {
-  return JS::GetRealmForCompartment(compartment)->behaviors();
-}
-
-JS::RealmBehaviors& JS::RealmBehaviorsRef(JSObject* obj) {
-  return obj->realm()->behaviors();
+JS::RealmBehaviors& JS::RealmBehaviorsRef(JS::Realm* realm) {
+  return realm->behaviors();
 }
 
 JS::RealmBehaviors& JS::RealmBehaviorsRef(JSContext* cx) {
   return cx->realm()->behaviors();
 }
 
 JS_PUBLIC_API JSObject* JS_NewGlobalObject(
     JSContext* cx, const JSClass* clasp, JSPrincipals* principals,
diff --git a/js/src/jsapi.h b/js/src/jsapi.h
--- a/js/src/jsapi.h
+++ b/js/src/jsapi.h
@@ -1899,19 +1899,17 @@ JS_PUBLIC_API const RealmCreationOptions
     JSCompartment* compartment);
 
 JS_PUBLIC_API const RealmCreationOptions& RealmCreationOptionsRef(
     JSObject* obj);
 
 JS_PUBLIC_API const RealmCreationOptions& RealmCreationOptionsRef(
     JSContext* cx);
 
-JS_PUBLIC_API RealmBehaviors& RealmBehaviorsRef(JSCompartment* compartment);
-
-JS_PUBLIC_API RealmBehaviors& RealmBehaviorsRef(JSObject* obj);
+JS_PUBLIC_API RealmBehaviors& RealmBehaviorsRef(JS::Realm* realm);
 
 JS_PUBLIC_API RealmBehaviors& RealmBehaviorsRef(JSContext* cx);
 
 /**
  * During global creation, we fire notifications to callbacks registered
  * via the Debugger API. These callbacks are arbitrary script, and can touch
  * the global in arbitrary ways. When that happens, the global should not be
  * in a half-baked state. But this creates a problem for consumers that need
diff --git a/js/xpconnect/src/XPCShellImpl.cpp b/js/xpconnect/src/XPCShellImpl.cpp
--- a/js/xpconnect/src/XPCShellImpl.cpp
+++ b/js/xpconnect/src/XPCShellImpl.cpp
@@ -1228,24 +1228,24 @@ int XRE_XPCShellMain(int argc, char** ar
     CodeCoverageHandler::Init();
 #endif
 
     {
       if (!glob) {
         return 1;
       }
 
+      backstagePass->SetGlobalObject(glob);
+
+      JSAutoRealm ar(cx, glob);
+
       // Even if we're building in a configuration where source is
       // discarded, there's no reason to do that on XPCShell, and doing so
       // might break various automation scripts.
-      JS::RealmBehaviorsRef(glob).setDiscardSource(false);
-
-      backstagePass->SetGlobalObject(glob);
-
-      JSAutoRealm ar(cx, glob);
+      JS::RealmBehaviorsRef(cx).setDiscardSource(false);
 
       if (!JS_InitReflectParse(cx, glob)) {
         return 1;
       }
 
       if (!JS_DefineFunctions(cx, glob, glob_functions) ||
           !JS_DefineProfilingFunctions(cx, glob)) {
         return 1;
