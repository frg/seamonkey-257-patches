# HG changeset patch
# User J. Ryan Stinnett <jryans@gmail.com>
# Date 1521503626 18000
# Node ID 8e69d0a1591abb3e7dbecd3054bf685b7c5ade5e
# Parent  b83c7c6b4a4f158fc6982328eb926cf4a96c5cf0
Bug 1446222 - Move DevTools frame startup to new dir. r=jdescottes

Move and rename the server's frame script that starts DevTools in remote frames
from `child.js` to `startup/frame.js`.  `connectToChild` also becomes the more
generic `connectToFrame`.  A few b2g-isms like "app" are also removed.

These code paths will likely change more as Site Isolation work continues, but
for now, we have this light cleanup to gather startup-related paths together.

MozReview-Commit-ID: El8a0OE99gZ

diff --git a/devtools/client/framework/target.js b/devtools/client/framework/target.js
--- a/devtools/client/framework/target.js
+++ b/devtools/client/framework/target.js
@@ -402,17 +402,17 @@ TabTarget.prototype = {
     this._remote = defer();
 
     if (this.isLocalTab) {
       // Since a remote protocol connection will be made, let's start the
       // DebuggerServer here, once and for all tools.
       DebuggerServer.init();
 
       // When connecting to a local tab, we only need the root actor.
-      // Then we are going to call DebuggerServer.connectToChild and talk
+      // Then we are going to call DebuggerServer.connectToFrame and talk
       // directly with actors living in the child process.
       // We also need browser actors for actor registry which enabled addons
       // to register custom actors.
       // TODO: the comment and implementation are out of sync here. See Bug 1420134.
       DebuggerServer.registerAllActors();
 
       this._client = new DebuggerClient(DebuggerServer.connectPipe());
       // A local TabTarget will never perform chrome debugging.
diff --git a/devtools/client/responsive.html/browser/tunnel.js b/devtools/client/responsive.html/browser/tunnel.js
--- a/devtools/client/responsive.html/browser/tunnel.js
+++ b/devtools/client/responsive.html/browser/tunnel.js
@@ -443,17 +443,17 @@ MessageManagerTunnel.prototype = {
     "Printing:",
     "PageMetadata:",
     // Messages sent to viewSourceUtils.js
     "ViewSource:",
   ],
 
   OUTER_TO_INNER_FRAME_SCRIPTS: [
     // DevTools server for OOP frames
-    "resource://devtools/server/child.js"
+    "resource://devtools/server/startup/frame.js"
   ],
 
   get outer() {
     return this.outerRef.get();
   },
 
   get outerParentMM() {
     if (!this.outer[FRAME_LOADER]) {
diff --git a/devtools/server/actors/webbrowser.js b/devtools/server/actors/webbrowser.js
--- a/devtools/server/actors/webbrowser.js
+++ b/devtools/server/actors/webbrowser.js
@@ -716,17 +716,17 @@ BrowserTabActor.prototype = {
         // Reject the update promise if the tab was destroyed while requesting an update
         this._deferredUpdate.reject({
           error: "tabDestroyed",
           message: "Tab destroyed while performing a BrowserTabActor update"
         });
       }
       this.exit();
     };
-    let connect = DebuggerServer.connectToChild(this._conn, this._browser, onDestroy);
+    let connect = DebuggerServer.connectToFrame(this._conn, this._browser, onDestroy);
     let form = await connect;
 
     this._form = form;
     if (this.options.favicons) {
       this._form.favicon = await this.getFaviconData();
     }
 
     return this;
diff --git a/devtools/server/actors/webextension-parent.js b/devtools/server/actors/webextension-parent.js
--- a/devtools/server/actors/webextension-parent.js
+++ b/devtools/server/actors/webextension-parent.js
@@ -156,17 +156,17 @@ ProxyChildActor.prototype = {
     }
 
     // Called when the debug browser element has been destroyed
     // (no actor is using it anymore to connect the child extension process).
     const onDestroy = this.destroy.bind(this);
 
     this._browser = await ExtensionParent.DebugUtils.getExtensionProcessBrowser(this);
 
-    this._form = await DebuggerServer.connectToChild(this._conn, this._browser, onDestroy,
+    this._form = await DebuggerServer.connectToFrame(this._conn, this._browser, onDestroy,
                                                      {addonId: this.addonId});
 
     this._childActorID = this._form.actor;
 
     // Exit the proxy child actor if the child actor has been destroyed.
     this._mm.addMessageListener("debug:webext_child_exit", this._onChildExit);
 
     return this._form;
diff --git a/devtools/server/actors/webextension.js b/devtools/server/actors/webextension.js
--- a/devtools/server/actors/webextension.js
+++ b/devtools/server/actors/webextension.js
@@ -42,17 +42,17 @@ const FALLBACK_DOC_MESSAGE = "Your addon
  * add-on).
  * - When the WebExtensions OOP mode has been introduced, this actor has been refactored
  * and moved from the main process to the new child extension process.
  *
  * @param {DebuggerServerConnection} conn
  *        The connection to the client.
  * @param {nsIMessageSender} chromeGlobal.
  *        The chromeGlobal where this actor has been injected by the
- *        DebuggerServer.connectToChild method.
+ *        DebuggerServer.connectToFrame method.
  * @param {string} prefix
  *        the custom RDP prefix to use.
  * @param {string} addonId
  *        the addonId of the target WebExtension.
  */
 function WebExtensionChildActor(conn, chromeGlobal, prefix, addonId) {
   ChromeActor.call(this, conn);
 
diff --git a/devtools/server/main.js b/devtools/server/main.js
--- a/devtools/server/main.js
+++ b/devtools/server/main.js
@@ -968,36 +968,37 @@ var DebuggerServer = {
   },
 
   /**
    * Live list of all currenctly attached child's message managers.
    */
   _childMessageManagers: new Set(),
 
   /**
-   * Connect to a child process.
+   * Start a DevTools server in a remote frame's process and add it as a child server for
+   * an active connection.
    *
    * @param object connection
    *        The debugger server connection to use.
    * @param nsIDOMElement frame
-   *        The browser element that holds the child process.
+   *        The frame element with remote content to connect to.
    * @param function [onDestroy]
    *        Optional function to invoke when the child process closes
    *        or the connection shuts down. (Need to forget about the
    *        related TabActor)
    * @return object
    *         A promise object that is resolved once the connection is
    *         established.
    */
-  connectToChild(connection, frame, onDestroy, {addonId} = {}) {
+  connectToFrame(connection, frame, onDestroy, {addonId} = {}) {
     return new Promise(resolve => {
       // Get messageManager from XUL browser (which might be a specialized tunnel for RDM)
       // or else fallback to asking the frameLoader itself.
       let mm = frame.messageManager || frame.frameLoader.messageManager;
-      mm.loadFrameScript("resource://devtools/server/child.js", false);
+      mm.loadFrameScript("resource://devtools/server/startup/frame.js", false);
 
       let trackMessageManager = () => {
         frame.addEventListener("DevTools:BrowserSwap", onBrowserSwap);
         mm.addMessageListener("debug:setup-in-parent", onSetupInParent);
         if (!actor) {
           mm.addMessageListener("debug:actor", onActorCreated);
         }
         DebuggerServer._childMessageManagers.add(mm);
@@ -1016,17 +1017,17 @@ var DebuggerServer = {
       let prefix = connection.allocID("child");
       // Compute the same prefix that's used by DebuggerServerConnection
       let connPrefix = prefix + "/";
 
       // provides hook to actor modules that need to exchange messages
       // between e10s parent and child processes
       let parentModules = [];
       let onSetupInParent = function (msg) {
-        // We may have multiple connectToChild instance running for the same tab
+        // We may have multiple connectToFrame instance running for the same tab
         // and need to filter the messages.
         if (msg.json.prefix != connPrefix) {
           return false;
         }
 
         let { module, setupParent } = msg.json;
         let m;
 
@@ -1062,17 +1063,17 @@ var DebuggerServer = {
         childTransport.hooks = {
           onPacket: connection.send.bind(connection),
           onClosed() {}
         };
         childTransport.ready();
 
         connection.setForwarding(prefix, childTransport);
 
-        dumpn("establishing forwarding for app with prefix " + prefix);
+        dumpn(`Start forwarding for frame with prefix ${prefix}`);
 
         actor = msg.json.actor;
         resolve(actor);
       }).bind(this);
 
       // Listen for browser frame swap
       let onBrowserSwap = ({ detail: newFrame }) => {
         // Remove listeners from old frame and mm
@@ -1124,24 +1125,24 @@ var DebuggerServer = {
           try {
             // Bug 1169643: Ignore any exception as the child process
             // may already be destroyed by now.
             mm.sendAsyncMessage("debug:disconnect", { prefix });
           } catch (e) {
             // Nothing to do
           }
         } else {
-          // Otherwise, the app has been closed before the actor
+          // Otherwise, the frame has been closed before the actor
           // had a chance to be created, so we are not able to create
           // the actor.
           resolve(null);
         }
         if (actor) {
           // The ContentActor within the child process doesn't necessary
-          // have time to uninitialize itself when the app is closed/killed.
+          // have time to uninitialize itself when the frame is closed/killed.
           // So ensure telling the client that the related actor is detached.
           connection.send({ from: actor.actor, type: "tabDetached" });
           actor = null;
         }
 
         if (onDestroy) {
           onDestroy(mm);
         }
diff --git a/devtools/server/moz.build b/devtools/server/moz.build
--- a/devtools/server/moz.build
+++ b/devtools/server/moz.build
@@ -5,24 +5,24 @@
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 include('../templates.mozbuild')
 
 DIRS += [
     'actors',
     'performance',
     'socket',
+    'startup',
 ]
 
 BROWSER_CHROME_MANIFESTS += ['tests/browser/browser.ini']
 MOCHITEST_CHROME_MANIFESTS += ['tests/mochitest/chrome.ini']
 XPCSHELL_TESTS_MANIFESTS += ['tests/unit/xpcshell.ini']
 
 DevToolsModules(
-    'child.js',
     'content-process-debugger-server.js',
     'content-server.jsm',
     'main.js',
     'service-worker-child.js',
     'worker.js'
 )
 
 with Files('**'):
diff --git a/devtools/server/child.js b/devtools/server/startup/frame.js
rename from devtools/server/child.js
rename to devtools/server/startup/frame.js
--- a/devtools/server/child.js
+++ b/devtools/server/startup/frame.js
@@ -1,16 +1,22 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 "use strict";
 
 /* global addEventListener, addMessageListener, removeMessageListener, sendAsyncMessage */
 
+/*
+ * Frame script that listens for requests to start a `DebuggerServer` for a
+ * frame in a content process.  Loaded into content process frames by the main
+ * process during `DebuggerServer.connectToFrame`.
+ */
+
 try {
   var chromeGlobal = this;
 
   // Encapsulate in its own scope to allows loading this frame script more than once.
   (function () {
     const { require } = ChromeUtils.import("resource://devtools/shared/Loader.jsm", {});
 
     const DevToolsUtils = require("devtools/shared/DevToolsUtils");
@@ -123,10 +129,10 @@ try {
     addEventListener("unload", () => {
       for (let conn of connections.values()) {
         conn.close();
       }
       connections.clear();
     });
   })();
 } catch (e) {
-  dump(`Exception in app child process: ${e}\n`);
+  dump(`Exception in DevTools frame startup: ${e}\n`);
 }
diff --git a/devtools/server/startup/moz.build b/devtools/server/startup/moz.build
new file mode 100644
--- /dev/null
+++ b/devtools/server/startup/moz.build
@@ -0,0 +1,9 @@
+# -*- Mode: python; indent-tabs-mode: nil; tab-width: 40 -*-
+# vim: set filetype=python:
+# This Source Code Form is subject to the terms of the Mozilla Public
+# License, v. 2.0. If a copy of the MPL was not distributed with this
+# file, You can obtain one at http://mozilla.org/MPL/2.0/.
+
+DevToolsModules(
+    'frame.js',
+)
diff --git a/devtools/server/tests/mochitest/chrome.ini b/devtools/server/tests/mochitest/chrome.ini
--- a/devtools/server/tests/mochitest/chrome.ini
+++ b/devtools/server/tests/mochitest/chrome.ini
@@ -26,17 +26,17 @@ support-files =
   nonchrome_unsafeDereference.html
   small-image.gif
   setup-in-child.js
   setup-in-parent.js
   webconsole-helpers.js
   webextension-helpers.js
 [test_animation_actor-lifetime.html]
 [test_connection-manager.html]
-[test_connectToChild.html]
+[test_connectToFrame.html]
 [test_css-logic.html]
 [test_css-logic-media-queries.html]
 [test_css-logic-specificity.html]
 [test_css-properties.html]
 [test_Debugger.Source.prototype.introductionScript.html]
 [test_Debugger.Source.prototype.introductionType.html]
 [test_Debugger.Source.prototype.element.html]
 [test_Debugger.Script.prototype.global.html]
diff --git a/devtools/server/tests/mochitest/test_connectToChild.html b/devtools/server/tests/mochitest/test_connectToFrame.html
rename from devtools/server/tests/mochitest/test_connectToChild.html
rename to devtools/server/tests/mochitest/test_connectToFrame.html
--- a/devtools/server/tests/mochitest/test_connectToChild.html
+++ b/devtools/server/tests/mochitest/test_connectToFrame.html
@@ -1,12 +1,12 @@
 <!DOCTYPE HTML>
 <html>
 <!--
-Bug 966991 - Test DebuggerServer.connectToChild
+Bug 966991 - Test DebuggerServer.connectToFrame
 -->
 <head>
   <meta charset="utf-8">
   <title>Mozilla Bug</title>
   <script type="application/javascript" src="chrome://mochikit/content/tests/SimpleTest/SimpleTest.js"></script>
   <link rel="stylesheet" type="text/css" href="chrome://mochikit/content/tests/SimpleTest/test.css">
 </head>
 <body>
@@ -73,17 +73,17 @@ function runTests() {
     DebuggerServer.registerAllActors();
   }
 
   function firstClient() {
     // Fake a first connection to an iframe
     let transport = DebuggerServer.connectPipe();
     let conn = transport._serverConnection;
     let client = new DebuggerClient(transport);
-    DebuggerServer.connectToChild(conn, iframe).then(actor => {
+    DebuggerServer.connectToFrame(conn, iframe).then(actor => {
       ok(actor.testActor, "Got the test actor");
 
       // Ensure sending at least one request to our actor,
       // otherwise it won't be instanciated, nor be destroyed...
       client.request({
         to: actor.testActor,
         type: "hello",
       }, function (response) {
@@ -102,17 +102,17 @@ function runTests() {
     });
   }
 
   function secondClient(firstActor) {
     // Then fake a second one, that should spawn a new set of tab actors
     let transport = DebuggerServer.connectPipe();
     let conn = transport._serverConnection;
     let client = new DebuggerClient(transport);
-    DebuggerServer.connectToChild(conn, iframe).then(actor => {
+    DebuggerServer.connectToFrame(conn, iframe).then(actor => {
       ok(actor.testActor, "Got a test actor for the second connection");
       isnot(actor.testActor, firstActor,
             "We get different actor instances between two connections");
 
       client.close(cleanup);
     });
   }
 
diff --git a/devtools/server/tests/mochitest/test_setupInParentChild.html b/devtools/server/tests/mochitest/test_setupInParentChild.html
--- a/devtools/server/tests/mochitest/test_setupInParentChild.html
+++ b/devtools/server/tests/mochitest/test_setupInParentChild.html
@@ -75,17 +75,17 @@ function runTests() {
     is(args[0], true, "Got `mm` argument, a message manager");
     ok(args[1].match(/server\d+.conn\d+.child\d+/), "Got `prefix` argument");
 
     cleanup();
   };
   Services.obs.addObserver(onParent, "test:setupParent");
 
   // Instanciate e10s machinery and call setupInChild
-  DebuggerServer.connectToChild(conn, iframe).then(actor => {
+  DebuggerServer.connectToFrame(conn, iframe).then(actor => {
     DebuggerServer.setupInChild({
       module: "chrome://mochitests/content/chrome/devtools/server/tests/mochitest/setup-in-child.js",
       setupChild: "setupChild",
       args: [1, "two", {three: true}]
     });
   });
 
   function cleanup() {
