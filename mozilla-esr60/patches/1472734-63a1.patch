# HG changeset patch
# User Jon Coppeard <jcoppeard@mozilla.com>
# Date 1531388136 -3600
# Node ID e72e7f5db9a668895f1081050e8ecb33a480de16
# Parent  f13f77b8a4c61f3aaa48c29d9ca2913b5ac7ea2d
Bug 1472734 - Allow ClearEdgesTracer when GC tracer is required to accomodate GCManagedDeletePolicy during sweeping r=sfink a=abillings

diff --git a/js/src/gc/DeletePolicy.h b/js/src/gc/DeletePolicy.h
--- a/js/src/gc/DeletePolicy.h
+++ b/js/src/gc/DeletePolicy.h
@@ -2,17 +2,16 @@
  * vim: set ts=8 sts=4 et sw=4 tw=99:
  * This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef gc_DeletePolicy_h
 #define gc_DeletePolicy_h
 
-#include "gc/Barrier.h"
 #include "js/TracingAPI.h"
 #ifdef ENABLE_BIGINT
 #include "vm/BigIntType.h"
 #endif
 
 namespace js {
 namespace gc {
 
@@ -66,23 +65,18 @@ inline bool IsClearEdgesTracer(JSTracer*
  * trigger barriers while doing so. This will remove any store buffer pointers
  * into the object and make it safe to delete.
  */
 template <typename T>
 struct GCManagedDeletePolicy {
   void operator()(const T* constPtr) {
     if (constPtr) {
       auto ptr = const_cast<T*>(constPtr);
-      if (JS::RuntimeHeapIsCollecting()) {
-        MOZ_ASSERT(js::CurrentThreadIsGCSweeping());
-        // Do not attempt to clear out storebuffer edges.
-      } else {
-        gc::ClearEdgesTracer trc;
-        ptr->trace(&trc);
-      }
+      gc::ClearEdgesTracer trc;
+      ptr->trace(&trc);
       js_delete(ptr);
     }
   }
 };
 
 }  // namespace js
 
 #endif  // gc_DeletePolicy_h
diff --git a/js/src/gc/Marking.cpp b/js/src/gc/Marking.cpp
--- a/js/src/gc/Marking.cpp
+++ b/js/src/gc/Marking.cpp
@@ -227,17 +227,18 @@ void js::CheckTracedThing(JSTracer* trc,
   /*
    * Do not check IsMarkingTracer directly -- it should only be used in paths
    * where we cannot be the gray buffering tracer.
    */
   bool isGcMarkingTracer = trc->isMarkingTracer();
 
   MOZ_ASSERT_IF(zone->requireGCTracer(), isGcMarkingTracer ||
                                              IsBufferGrayRootsTracer(trc) ||
-                                             IsUnmarkGrayTracer(trc));
+                                             IsUnmarkGrayTracer(trc) ||
+                                             IsClearEdgesTracer(trc));
 
   if (isGcMarkingTracer) {
     GCMarker* gcMarker = GCMarker::fromTracer(trc);
     MOZ_ASSERT_IF(gcMarker->shouldCheckCompartments(),
                   zone->isCollecting() || zone->isAtomsZone());
 
     MOZ_ASSERT_IF(gcMarker->markColor() == MarkColor::Gray,
                   !zone->isGCMarkingBlack() || zone->isAtomsZone());
diff --git a/js/src/jit-test/tests/gc/bug-1472734.js b/js/src/jit-test/tests/gc/bug-1472734.js
new file mode 100644
--- /dev/null
+++ b/js/src/jit-test/tests/gc/bug-1472734.js
@@ -0,0 +1,14 @@
+if (!('oomTest' in this) || helperThreadCount() === 0)
+    quit();
+
+try {
+    oomTest(function() {
+      eval(`
+        function eval(source) {
+          offThreadCompileModule(source);
+          minorgc();
+        }
+        eval("");
+      `);
+    });
+} catch (exc) {}
