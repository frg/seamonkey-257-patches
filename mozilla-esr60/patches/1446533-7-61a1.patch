# HG changeset patch
# User Boris Zbarsky <bzbarsky@mit.edu>
# Date 1521488284 14400
# Node ID 3e3ac3c3b9893746b775c682d564fba950a3a55b
# Parent  dec5844af098c0484b36cf194577591b462cc872
Bug 1446533 part 7.  Remove nsIDOMCharacterData::InsertData.  r=mystor

MozReview-Commit-ID: 48XZ2J9ewHP

diff --git a/dom/base/CharacterData.cpp b/dom/base/CharacterData.cpp
--- a/dom/base/CharacterData.cpp
+++ b/dom/base/CharacterData.cpp
@@ -195,28 +195,28 @@ void CharacterData::SubstringData(uint32
     const char* data = mText.Get1b() + aStart;
     CopyASCIItoUTF16(Substring(data, data + amount), aReturn);
   }
 }
 
 //----------------------------------------------------------------------
 
 void CharacterData::AppendData(const nsAString& aData, ErrorResult& aRv) {
-  nsresult rv = SetTextInternal(mText.GetLength(), 0, aData.BeginReading(),
+  InsertData(mText.Length(), aData, aRv);
+}
+
+void CharacterData::InsertData(uint32_t aOffset, const nsAString& aData,
+                          ErrorResult& aRv) {
+  nsresult rv = SetTextInternal(aOffset, 0, aData.BeginReading(),
                                 aData.Length(), true);
   if (NS_FAILED(rv)) {
     aRv.Throw(rv);
   }
 }
 
-nsresult CharacterData::InsertData(uint32_t aOffset, const nsAString& aData) {
-  return SetTextInternal(aOffset, 0, aData.BeginReading(), aData.Length(),
-                         true);
-}
-
 nsresult CharacterData::DeleteData(uint32_t aOffset, uint32_t aCount) {
   return SetTextInternal(aOffset, aCount, nullptr, 0, true);
 }
 
 nsresult CharacterData::ReplaceData(uint32_t aOffset, uint32_t aCount,
                                     const nsAString& aData) {
   return SetTextInternal(aOffset, aCount, aData.BeginReading(), aData.Length(),
                          true);
diff --git a/dom/base/CharacterData.h b/dom/base/CharacterData.h
--- a/dom/base/CharacterData.h
+++ b/dom/base/CharacterData.h
@@ -90,17 +90,16 @@ class CharacterData : public nsIContent 
 
   NS_IMPL_FROMCONTENT_HELPER(CharacterData, IsCharacterData())
 
   virtual void GetNodeValueInternal(nsAString& aNodeValue) override;
   virtual void SetNodeValueInternal(const nsAString& aNodeValue,
                                     ErrorResult& aError) override;
 
   // Implementation for nsIDOMCharacterData
-  nsresult InsertData(uint32_t aOffset, const nsAString& aArg);
   nsresult DeleteData(uint32_t aOffset, uint32_t aCount);
   nsresult ReplaceData(uint32_t aOffset, uint32_t aCount,
                        const nsAString& aArg);
 
   // nsINode methods
   virtual uint32_t GetChildCount() const override;
   virtual nsIContent* GetChildAt_Deprecated(uint32_t aIndex) const override;
   virtual int32_t ComputeIndexOf(const nsINode* aPossibleChild) const override;
@@ -174,19 +173,17 @@ class CharacterData : public nsIContent 
 
   // WebIDL API
   void GetData(nsAString& aData) const;
   virtual void SetData(const nsAString& aData, ErrorResult& rv);
   // nsINode::Length() returns the right thing for our length attribute
   void SubstringData(uint32_t aStart, uint32_t aCount, nsAString& aReturn,
                      ErrorResult& rv);
   void AppendData(const nsAString& aData, ErrorResult& rv);
-  void InsertData(uint32_t aOffset, const nsAString& aData, ErrorResult& rv) {
-    rv = InsertData(aOffset, aData);
-  }
+  void InsertData(uint32_t aOffset, const nsAString& aData, ErrorResult& rv);
   void DeleteData(uint32_t aOffset, uint32_t aCount, ErrorResult& rv) {
     rv = DeleteData(aOffset, aCount);
   }
   void ReplaceData(uint32_t aOffset, uint32_t aCount, const nsAString& aData,
                    ErrorResult& rv) {
     rv = ReplaceData(aOffset, aCount, aData);
   }
 
diff --git a/dom/interfaces/core/nsIDOMCharacterData.idl b/dom/interfaces/core/nsIDOMCharacterData.idl
--- a/dom/interfaces/core/nsIDOMCharacterData.idl
+++ b/dom/interfaces/core/nsIDOMCharacterData.idl
@@ -11,19 +11,16 @@
  * 
  * For more information on this interface please see 
  * http://www.w3.org/TR/DOM-Level-2-Core/
  */
 
 [uuid(4109a2d2-e7af-445d-bb72-c7c9b875f35e)]
 interface nsIDOMCharacterData : nsIDOMNode
 {
-  void                      insertData(in unsigned long offset, 
-                                       in DOMString arg)
-                                  raises(DOMException);
   void                      deleteData(in unsigned long offset, 
                                        in unsigned long count)
                                   raises(DOMException);
   void                      replaceData(in unsigned long offset, 
                                         in unsigned long count, 
                                         in DOMString arg)
                                   raises(DOMException);
 };
diff --git a/editor/libeditor/CompositionTransaction.cpp b/editor/libeditor/CompositionTransaction.cpp
--- a/editor/libeditor/CompositionTransaction.cpp
+++ b/editor/libeditor/CompositionTransaction.cpp
@@ -91,19 +91,20 @@ CompositionTransaction::DoTransaction() 
 
   // Fail before making any changes if there's no selection controller
   nsCOMPtr<nsISelectionController> selCon;
   mEditorBase->GetSelectionController(getter_AddRefs(selCon));
   NS_ENSURE_TRUE(selCon, NS_ERROR_NOT_INITIALIZED);
 
   // Advance caret: This requires the presentation shell to get the selection.
   if (mReplaceLength == 0) {
-    nsresult rv = mTextNode->InsertData(mOffset, mStringToInsert);
-    if (NS_WARN_IF(NS_FAILED(rv))) {
-      return rv;
+    ErrorResult rv;
+    mTextNode->InsertData(mOffset, mStringToInsert, rv);
+    if (NS_WARN_IF(rv.Failed())) {
+      return rv.StealNSResult();
     }
     mEditorBase->RangeUpdaterRef().SelAdjInsertText(*mTextNode, mOffset,
                                                     mStringToInsert);
   } else {
     uint32_t replaceableLength = mTextNode->TextLength() - mOffset;
     nsresult rv =
         mTextNode->ReplaceData(mOffset, mReplaceLength, mStringToInsert);
     if (NS_WARN_IF(NS_FAILED(rv))) {
diff --git a/editor/libeditor/DeleteTextTransaction.cpp b/editor/libeditor/DeleteTextTransaction.cpp
--- a/editor/libeditor/DeleteTextTransaction.cpp
+++ b/editor/libeditor/DeleteTextTransaction.cpp
@@ -137,12 +137,14 @@ DeleteTextTransaction::DoTransaction() {
 
 // XXX: We may want to store the selection state and restore it properly.  Was
 //     it an insertion point or an extended selection?
 NS_IMETHODIMP
 DeleteTextTransaction::UndoTransaction() {
   if (NS_WARN_IF(!mCharData)) {
     return NS_ERROR_NOT_INITIALIZED;
   }
-  return mCharData->InsertData(mOffset, mDeletedText);
+  ErrorResult rv;
+  mCharData->InsertData(mOffset, mDeletedText, rv);
+  return rv.StealNSResult();
 }
 
 }  // namespace mozilla
diff --git a/editor/libeditor/InsertTextTransaction.cpp b/editor/libeditor/InsertTextTransaction.cpp
--- a/editor/libeditor/InsertTextTransaction.cpp
+++ b/editor/libeditor/InsertTextTransaction.cpp
@@ -49,19 +49,20 @@ NS_INTERFACE_MAP_BEGIN_CYCLE_COLLECTION(
 NS_INTERFACE_MAP_END_INHERITING(EditTransactionBase)
 
 NS_IMETHODIMP
 InsertTextTransaction::DoTransaction() {
   if (NS_WARN_IF(!mEditorBase) || NS_WARN_IF(!mTextNode)) {
     return NS_ERROR_NOT_AVAILABLE;
   }
 
-  nsresult rv = mTextNode->InsertData(mOffset, mStringToInsert);
-  if (NS_WARN_IF(NS_FAILED(rv))) {
-    return rv;
+  ErrorResult rv;
+  mTextNode->InsertData(mOffset, mStringToInsert, rv);
+  if (NS_WARN_IF(rv.Failed())) {
+    return rv.StealNSResult();
   }
 
   // Only set selection to insertion point if editor gives permission
   if (mEditorBase->GetShouldTxnSetSelection()) {
     RefPtr<Selection> selection = mEditorBase->GetSelection();
     if (NS_WARN_IF(!selection)) {
       return NS_ERROR_FAILURE;
     }
diff --git a/layout/printing/nsPrintJob.cpp b/layout/printing/nsPrintJob.cpp
--- a/layout/printing/nsPrintJob.cpp
+++ b/layout/printing/nsPrintJob.cpp
@@ -2478,17 +2478,17 @@ static nsresult DeleteUnselectedNodes(ns
 
     if (NS_SUCCEEDED(rv) && !range->Collapsed()) {
       selection->AddRange(range);
 
       // Unless we've already added an ellipsis at the start, if we ended mid
       // text node then add ellipsis.
       Text* text = endNode->GetAsText();
       if (!ellipsisOffset && text && endOffset && endOffset < text->Length()) {
-        text->InsertData(endOffset, kEllipsis);
+        text->InsertData(endOffset, kEllipsis, IgnoreErrors());
         ellipsisOffset += kEllipsis.Length();
       }
     }
 
     // Next new start is end of original range.
     startNode =
         GetCorrespondingNodeInDocument(origRange->GetEndContainer(), aDoc);
 
@@ -2496,17 +2496,17 @@ static nsresult DeleteUnselectedNodes(ns
     if (startNode != endNode) {
       ellipsisOffset = 0;
     }
     startOffset = origRange->EndOffset() + ellipsisOffset;
 
     // If the next node will start mid text node then add ellipsis.
     Text* text = startNode ? startNode->GetAsText() : nullptr;
     if (text && startOffset && startOffset < text->Length()) {
-      text->InsertData(startOffset, kEllipsis);
+      text->InsertData(startOffset, kEllipsis, IgnoreErrors());
       startOffset += kEllipsis.Length();
       ellipsisOffset += kEllipsis.Length();
     }
   }
 
   // Add in the last range to the end of the body.
   RefPtr<nsRange> lastRange;
   nsresult rv = nsRange::CreateRange(startNode, startOffset, bodyNode,
