# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1535383696 -7200
# Node ID c44d81b8909ab05e294a897ec76f02c04b729331
# Parent  dd7508e665336c40e704fe52c222d4ee19d5f054
Bug 1486444 - Remove bogus/over-allocating no-arg versions of js_pod_malloc/js_pod_calloc. r=tcampbell

These functions incorrectly passed sizeof(T) instead of 1, so we would allocate sizeof(T) * sizeof(T) bytes instead of sizeof(T) bytes. This was used for PcScriptCache where we would allocate a few extra megabytes due to this bug. The patch changes PcScriptCache to use UniquePtr + MakeUnique.

Differential Revision: https://phabricator.services.mozilla.com/D4343

diff --git a/js/public/Utility.h b/js/public/Utility.h
--- a/js/public/Utility.h
+++ b/js/public/Utility.h
@@ -550,40 +550,30 @@ static MOZ_ALWAYS_INLINE T* js_pod_arena
 }
 
 template <class T>
 static MOZ_ALWAYS_INLINE T* js_pod_malloc(size_t numElems) {
   return js_pod_arena_malloc<T>(js::MallocArena, numElems);
 }
 
 template <class T>
-static MOZ_ALWAYS_INLINE T* js_pod_malloc() {
-  return js_pod_malloc<T>(sizeof(T));
-}
-
-template <class T>
 static MOZ_ALWAYS_INLINE T* js_pod_arena_calloc(arena_id_t arena,
                                                 size_t numElems) {
   size_t bytes;
   if (MOZ_UNLIKELY(!js::CalculateAllocSize<T>(numElems, &bytes)))
     return nullptr;
   return static_cast<T*>(js_arena_calloc(arena, bytes, 1));
 }
 
 template <class T>
 static MOZ_ALWAYS_INLINE T* js_pod_calloc(size_t numElems) {
   return js_pod_arena_calloc<T>(js::MallocArena, numElems);
 }
 
 template <class T>
-static MOZ_ALWAYS_INLINE T* js_pod_calloc() {
-  return js_pod_calloc<T>(sizeof(T));
-}
-
-template <class T>
 static MOZ_ALWAYS_INLINE T* js_pod_realloc(T* prior, size_t oldSize,
                                            size_t newSize) {
   MOZ_ASSERT(!(oldSize & mozilla::tl::MulOverflowMask<sizeof(T)>::value));
   size_t bytes;
   if (MOZ_UNLIKELY(!js::CalculateAllocSize<T>(newSize, &bytes))) return nullptr;
   return static_cast<T*>(js_realloc(prior, bytes));
 }
 
diff --git a/js/src/jit/JitFrames.cpp b/js/src/jit/JitFrames.cpp
--- a/js/src/jit/JitFrames.cpp
+++ b/js/src/jit/JitFrames.cpp
@@ -1347,26 +1347,25 @@ void GetPcScript(JSContext* cx, JSScript
   }
 
   uint32_t hash;
   if (retAddr) {
     hash = PcScriptCache::Hash(retAddr);
 
     // Lazily initialize the cache. The allocation may safely fail and will not
     // GC.
-    if (MOZ_UNLIKELY(cx->ionPcScriptCache == nullptr)) {
-      cx->ionPcScriptCache = js_pod_malloc<PcScriptCache>();
-      if (cx->ionPcScriptCache)
-        cx->ionPcScriptCache->clear(cx->runtime()->gc.gcNumber());
+    if (MOZ_UNLIKELY(cx->ionPcScriptCache == nullptr))
+      cx->ionPcScriptCache =
+          MakeUnique<PcScriptCache>(cx->runtime()->gc.gcNumber());
+
+    if (cx->ionPcScriptCache.ref() &&
+        cx->ionPcScriptCache->get(cx->runtime(), hash, retAddr, scriptRes,
+                                  pcRes)) {
+      return;
     }
-
-    if (cx->ionPcScriptCache &&
-        cx->ionPcScriptCache->get(cx->runtime(), hash, retAddr, scriptRes,
-                                  pcRes))
-      return;
   }
 
   // Lookup failed: undertake expensive process to recover the innermost inlined
   // frame.
   jsbytecode* pc = nullptr;
   if (it.frame().isIonJS() || it.frame().isBailoutJS()) {
     InlineFrameIterator ifi(cx, &it.frame());
     *scriptRes = ifi.script();
@@ -1374,17 +1373,17 @@ void GetPcScript(JSContext* cx, JSScript
   } else {
     MOZ_ASSERT(it.frame().isBaselineJS());
     it.frame().baselineScriptAndPc(scriptRes, &pc);
   }
 
   if (pcRes) *pcRes = pc;
 
   // Add entry to cache.
-  if (retAddr && cx->ionPcScriptCache)
+  if (retAddr && cx->ionPcScriptCache.ref())
     cx->ionPcScriptCache->add(hash, retAddr, pc, *scriptRes);
 }
 
 uint32_t OsiIndex::returnPointDisplacement() const {
   // In general, pointer arithmetic on code is bad, but in this case,
   // getting the return address from a call instruction, stepping over pools
   // would be wrong.
   return callPointDisplacement_ + Assembler::PatchWrite_NearCallSize();
diff --git a/js/src/jit/PcScriptCache.h b/js/src/jit/PcScriptCache.h
--- a/js/src/jit/PcScriptCache.h
+++ b/js/src/jit/PcScriptCache.h
@@ -19,26 +19,30 @@ namespace jit {
 
 struct PcScriptCacheEntry {
   uint8_t* returnAddress;  // Key into the hash table.
   jsbytecode* pc;          // Cached PC.
   JSScript* script;        // Cached script.
 };
 
 struct PcScriptCache {
+ private:
   static const uint32_t Length = 73;
 
   // GC number at the time the cache was filled or created.
   // Storing and checking against this number allows us to not bother
   // clearing this cache on every GC -- only when actually necessary.
   uint64_t gcNumber;
 
   // List of cache entries.
   mozilla::Array<PcScriptCacheEntry, Length> entries;
 
+ public:
+  explicit PcScriptCache(uint64_t gcNumber) { clear(gcNumber); }
+
   void clear(uint64_t gcNumber) {
     for (uint32_t i = 0; i < Length; i++) entries[i].returnAddress = nullptr;
     this->gcNumber = gcNumber;
   }
 
   // Get a value from the cache. May perform lazy allocation.
   MOZ_MUST_USE bool get(JSRuntime* rt, uint32_t hash, uint8_t* addr,
                         JSScript** scriptRes, jsbytecode** pcRes) {
diff --git a/js/src/jit/arm/Simulator-arm.cpp b/js/src/jit/arm/Simulator-arm.cpp
--- a/js/src/jit/arm/Simulator-arm.cpp
+++ b/js/src/jit/arm/Simulator-arm.cpp
@@ -1155,18 +1155,19 @@ class Redirection {
     Redirection* current = SimulatorProcess::redirection();
     for (; current != nullptr; current = current->next_) {
       if (current->nativeFunction_ == nativeFunction) {
         MOZ_ASSERT(current->type() == type);
         return current;
       }
     }
 
+    // Note: we can't use js_new here because the constructor is private.
     AutoEnterOOMUnsafeRegion oomUnsafe;
-    Redirection* redir = js_pod_malloc<Redirection>();
+    Redirection* redir = js_pod_malloc<Redirection>(1);
     if (!redir) oomUnsafe.crash("Simulator redirection");
     new (redir) Redirection(nativeFunction, type);
     return redir;
   }
 
   static Redirection* FromSwiInstruction(SimInstruction* swiInstruction) {
     uint8_t* addrOfSwi = reinterpret_cast<uint8_t*>(swiInstruction);
     uint8_t* addrOfRedirection =
diff --git a/js/src/jit/arm64/vixl/MozSimulator-vixl.cpp b/js/src/jit/arm64/vixl/MozSimulator-vixl.cpp
--- a/js/src/jit/arm64/vixl/MozSimulator-vixl.cpp
+++ b/js/src/jit/arm64/vixl/MozSimulator-vixl.cpp
@@ -385,18 +385,19 @@ class Redirection
     Redirection* current = SimulatorProcess::redirection();
     for (; current != nullptr; current = current->next_) {
       if (current->nativeFunction_ == nativeFunction) {
         VIXL_ASSERT(current->type() == type);
         return current;
       }
     }
 
+    // Note: we can't use js_new here because the constructor is private.
     js::AutoEnterOOMUnsafeRegion oomUnsafe;
-    Redirection* redir = js_pod_malloc<Redirection>();
+    Redirection* redir = js_pod_malloc<Redirection>(1);
     if (!redir)
         oomUnsafe.crash("Simulator redirection");
     new(redir) Redirection(nativeFunction, type);
     return redir;
   }
 
   static const Redirection* FromSvcInstruction(const Instruction* svcInstruction) {
     const uint8_t* addrOfSvc = reinterpret_cast<const uint8_t*>(svcInstruction);
diff --git a/js/src/jit/mips32/Simulator-mips32.cpp b/js/src/jit/mips32/Simulator-mips32.cpp
--- a/js/src/jit/mips32/Simulator-mips32.cpp
+++ b/js/src/jit/mips32/Simulator-mips32.cpp
@@ -1222,18 +1222,19 @@ class Redirection {
     Redirection* current = SimulatorProcess::redirection();
     for (; current != nullptr; current = current->next_) {
       if (current->nativeFunction_ == nativeFunction) {
         MOZ_ASSERT(current->type() == type);
         return current;
       }
     }
 
+    // Note: we can't use js_new here because the constructor is private.
     AutoEnterOOMUnsafeRegion oomUnsafe;
-    Redirection* redir = js_pod_malloc<Redirection>();
+    Redirection* redir = js_pod_malloc<Redirection>(1);
     if (!redir) {
       oomUnsafe.crash("Simulator redirection");
     }
     new (redir) Redirection(nativeFunction, type);
     return redir;
   }
 
   static Redirection* FromSwiInstruction(SimInstruction* swiInstruction) {
diff --git a/js/src/jit/mips64/Simulator-mips64.cpp b/js/src/jit/mips64/Simulator-mips64.cpp
--- a/js/src/jit/mips64/Simulator-mips64.cpp
+++ b/js/src/jit/mips64/Simulator-mips64.cpp
@@ -1240,18 +1240,19 @@ class Redirection {
     Redirection* current = SimulatorProcess::redirection();
     for (; current != nullptr; current = current->next_) {
       if (current->nativeFunction_ == nativeFunction) {
         MOZ_ASSERT(current->type() == type);
         return current;
       }
     }
 
+    // Note: we can't use js_new here because the constructor is private.
     AutoEnterOOMUnsafeRegion oomUnsafe;
-    Redirection* redir = js_pod_malloc<Redirection>();
+    Redirection* redir = js_pod_malloc<Redirection>(1);
     if (!redir) {
       oomUnsafe.crash("Simulator redirection");
     }
     new (redir) Redirection(nativeFunction, type);
     return redir;
   }
 
   static Redirection* FromSwiInstruction(SimInstruction* swiInstruction) {
diff --git a/js/src/jit/shared/Disassembler-shared.cpp b/js/src/jit/shared/Disassembler-shared.cpp
--- a/js/src/jit/shared/Disassembler-shared.cpp
+++ b/js/src/jit/shared/Disassembler-shared.cpp
@@ -186,17 +186,17 @@ DisassemblerSpew::Node* DisassemblerSpew
   for (p = nodes_; p && p->key != key; p = p->next)
     ;
   return p;
 }
 
 DisassemblerSpew::Node* DisassemblerSpew::add(const Label* key,
                                               uint32_t value) {
   MOZ_ASSERT(!lookup(key));
-  Node* node = js_pod_malloc<Node>();
+  Node* node = js_new<Node>();
   if (node) {
     node->key = key;
     node->value = value;
     node->bound = false;
     node->next = nodes_;
     nodes_ = node;
   }
   return node;
diff --git a/js/src/threading/ProtectedData.h b/js/src/threading/ProtectedData.h
--- a/js/src/threading/ProtectedData.h
+++ b/js/src/threading/ProtectedData.h
@@ -87,16 +87,22 @@ class ProtectedData {
 
   template <typename U>
   ThisType& operator=(const U& p) {
     this->ref() = p;
     return *this;
   }
 
   template <typename U>
+  ThisType& operator=(U&& p) {
+    this->ref() = std::move(p);
+    return *this;
+  }
+
+  template <typename U>
   T& operator+=(const U& rhs) {
     return ref() += rhs;
   }
   template <typename U>
   T& operator-=(const U& rhs) {
     return ref() -= rhs;
   }
   template <typename U>
@@ -145,46 +151,38 @@ class ProtectedData {
   Check check;
 #endif
 };
 
 // Intermediate class for protected data whose checks take no constructor
 // arguments.
 template <typename Check, typename T>
 class ProtectedDataNoCheckArgs : public ProtectedData<Check, T> {
-  typedef ProtectedDataNoCheckArgs<Check, T> ThisType;
+  using Base = ProtectedData<Check, T>;
 
  public:
   template <typename... Args>
   explicit ProtectedDataNoCheckArgs(Args&&... args)
       : ProtectedData<Check, T>(Check(), mozilla::Forward<Args>(args)...) {}
 
-  template <typename U>
-  ThisType& operator=(const U& p) {
-    this->ref() = p;
-    return *this;
-  }
+  using Base::operator=;
 };
 
 // Intermediate class for protected data whose checks take a Zone
 // constructor argument.
 template <typename Check, typename T>
 class ProtectedDataZoneArg : public ProtectedData<Check, T> {
-  typedef ProtectedDataZoneArg<Check, T> ThisType;
+  using Base = ProtectedData<Check, T>;
 
  public:
   template <typename... Args>
   explicit ProtectedDataZoneArg(JS::Zone* zone, Args&&... args)
       : ProtectedData<Check, T>(Check(zone), mozilla::Forward<Args>(args)...) {}
 
-  template <typename U>
-  ThisType& operator=(const U& p) {
-    this->ref() = p;
-    return *this;
-  }
+  using Base::operator=;
 };
 
 class CheckUnprotected {
 #ifdef JS_HAS_PROTECTED_DATA_CHECKS
  public:
   inline void check() const {}
 #endif
 };
diff --git a/js/src/vm/JSContext.cpp b/js/src/vm/JSContext.cpp
--- a/js/src/vm/JSContext.cpp
+++ b/js/src/vm/JSContext.cpp
@@ -1175,18 +1175,16 @@ JSContext::JSContext(JSRuntime* runtime,
 JSContext::~JSContext() {
   // Clear the ContextKind first, so that ProtectedData checks will allow us to
   // destroy this context even if the runtime is already gone.
   kind_ = ContextKind::HelperThread;
 
   /* Free the stuff hanging off of cx. */
   MOZ_ASSERT(!resolvingList);
 
-  js_delete(ionPcScriptCache.ref());
-
   if (dtoaState) DestroyDtoaState(dtoaState);
 
   fx.destroyInstance();
   freeOsrTempData();
 
 #ifdef JS_SIMULATOR
   js::jit::Simulator::Destroy(simulator_);
 #endif
diff --git a/js/src/vm/JSContext.h b/js/src/vm/JSContext.h
--- a/js/src/vm/JSContext.h
+++ b/js/src/vm/JSContext.h
@@ -608,17 +608,17 @@ struct JSContext : public JS::RootingCon
 
  public:
   js::LifoAlloc& tempLifoAlloc() { return tempLifoAlloc_.ref(); }
   const js::LifoAlloc& tempLifoAlloc() const { return tempLifoAlloc_.ref(); }
 
   js::ThreadData<uint32_t> debuggerMutations;
 
   // Cache for jit::GetPcScript().
-  js::ThreadData<js::jit::PcScriptCache*> ionPcScriptCache;
+  js::ThreadData<js::UniquePtr<js::jit::PcScriptCache>> ionPcScriptCache;
 
  private:
   /* Exception state -- the exception member is a GC root by definition. */
   js::ThreadData<bool> throwing; /* is there a pending exception? */
   js::ThreadData<JS::PersistentRooted<JS::Value>>
       unwrappedException_; /* most-recently-thrown exception */
 
   JS::Value& unwrappedException() {
