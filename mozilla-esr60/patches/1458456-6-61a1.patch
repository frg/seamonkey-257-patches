# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1525447745 -7200
# Node ID e186ada847ff99cbe00e0e7f19201d309beeba6f
# Parent  4bd78f62b7727c0d3bbec8b6fb27421060af59ea
Bug 1458456 part 6 - Some TSan automation changes. r=sfink

diff --git a/js/src/devtools/automation/autospider.py b/js/src/devtools/automation/autospider.py
--- a/js/src/devtools/automation/autospider.py
+++ b/js/src/devtools/automation/autospider.py
@@ -476,17 +476,17 @@ if 'jstests' in test_suites:
     results.append(run_test_command([MAKE, 'check-jstests']))
 
 # FIXME bug 1291449: This would be unnecessary if we could run msan with -mllvm
 # -msan-keep-going, but in clang 3.8 it causes a hang during compilation.
 if variant.get('ignore-test-failures'):
     print("Ignoring test results %s" % (results,))
     results = [0]
 
-if args.variant in ('tsan', 'msan'):
+if args.variant == 'msan':
     files = filter(lambda f: f.startswith("sanitize_log."), os.listdir(OUTDIR))
     fullfiles = [os.path.join(OUTDIR, f) for f in files]
 
     # Summarize results
     sites = Counter()
     errors = Counter()
     for filename in fullfiles:
         with open(os.path.join(OUTDIR, filename), 'rb') as fh:
@@ -508,53 +508,16 @@ if args.variant in ('tsan', 'msan'):
     print(open(summary_filename, 'rb').read())
 
     if 'max-errors' in variant:
         max_allowed = variant['max-errors']
         print("Found %d errors out of %d allowed" % (len(sites), max_allowed))
         if len(sites) > max_allowed:
             results.append(1)
 
-    if 'expect-errors' in variant:
-        # Line numbers may shift around between versions, so just look for
-        # matching filenames and function names. This will still produce false
-        # positives when functions are renamed or moved between files, or
-        # things change so that the actual race is in a different place. But it
-        # still seems preferable to saying "You introduced an additional race.
-        # Here are the 21 races detected; please ignore the 20 known ones in
-        # this other list."
-
-        for site in sites:
-            # Grab out the file and function names.
-            m = re.search(r'/([^/]+):\d+ in (.+)', site)
-            if m:
-                error = tuple(m.groups())
-            else:
-                # will get here if eg tsan symbolication fails
-                error = (site, '(unknown)')
-            errors[error] += 1
-
-        remaining = Counter(errors)
-        for expect in variant['expect-errors']:
-            # expect-errors is an array of (filename, function) tuples.
-            expect = tuple(expect)
-            if remaining[expect] == 0:
-                print("Did not see known error in %s function %s" % expect)
-            else:
-                remaining[expect] -= 1
-
-        status = 0
-        for filename, function in (e for e, c in remaining.items() if c > 0):
-            if AUTOMATION:
-                print("TinderboxPrint: tsan error<br/>%s function %s" % (filename, function))
-                status = 1
-            else:
-                print("*** tsan error in %s function %s" % (filename, function))
-        results.append(status)
-
     # Gather individual results into a tarball. Note that these are
     # distinguished only by pid of the JS process running within each test, so
     # given the 16-bit limitation of pids, it's totally possible that some of
     # these files will be lost due to being overwritten.
     command = ['tar', '-C', OUTDIR, '-zcf',
                os.path.join(env['MOZ_UPLOAD_DIR'], '%s.tar.gz' % args.variant)]
     command += files
     subprocess.call(command)
diff --git a/js/src/devtools/automation/variants/tsan b/js/src/devtools/automation/variants/tsan
--- a/js/src/devtools/automation/variants/tsan
+++ b/js/src/devtools/automation/variants/tsan
@@ -1,44 +1,11 @@
 {
     "configure-args": "--enable-debug-symbols='-gline-tables-only' --disable-jemalloc --enable-thread-sanitizer",
     "optimize": true,
     "debug": false,
     "compiler": "clang",
     "env": {
         "LLVM_SYMBOLIZER": "{TOOLTOOL_CHECKOUT}/clang/bin/llvm-symbolizer",
         "JITTEST_EXTRA_ARGS": "--jitflags=tsan --ignore-timeouts={DIR}/cgc-jittest-timeouts.txt --unusable-error-status --exclude-from={DIR}/tsan-sighandlers.txt",
-        "JSTESTS_EXTRA_ARGS": "--exclude-file={DIR}/cgc-jstests-slow.txt",
-        "TSAN_OPTIONS": "exitcode=0 log_path={OUTDIR}/sanitize_log"
+        "JSTESTS_EXTRA_ARGS": "--exclude-file={DIR}/cgc-jstests-slow.txt"
     },
-    "[comment on expect-errors]": "Note that expect-errors may contain duplicates. These indicate that tsan reports errors as two distinct line numbers in the same function. (Line number shift around, so we cannot just include them here.)",
-    "expect-errors": [
-        [ "Shape.h", "inDictionary" ],
-        [ "Shape.h", "maybeSlot" ],
-        [ "Shape.h", "slotSpan" ],
-        [ "Shape.h", "setSlotWithType" ],
-        [ "Shape.h", "search<js::MaybeAdding::NotAdding>" ],
-        [ "Shape.h", "setOverwritten" ],
-        [ "Shape.h", "incrementNumLinearSearches" ],
-        [ "Shape.h", "isBigEnoughForAShapeTable" ],
-        [ "Shape.h", "js::jit::CodeGenerator::emitGetPropertyPolymorphic(js::jit::LInstruction*, js::jit::Register, js::jit::Register, js::jit::TypedOrValueRegister const&)" ],
-        [ "ProtectedData.h", "operator=<js::CooperatingContext>" ],
-        [ "ProtectedData.h", "js::ZoneGroup::leave()" ],
-        [ "jsfriendapi.h", "GetObjectClass" ],
-        [ "jsfriendapi.h", "numFixedSlots" ],
-        [ "jsfriendapi.h", "numDynamicSlots" ],
-        [ "jsfriendapi.h", "EmulatesUndefined" ],
-        [ "jitprofiling.c", "iJIT_GetNewMethodID" ],
-        [ "Marking.cpp", "js::GCMarker::reset()" ],
-        [ "Statistics.h", "js::gc::GCRuntime::pickChunk(js::AutoLockGC const&, js::gc::AutoMaybeStartBackgroundAllocation&)" ],
-        [ "Statistics.h", "js::gc::GCRuntime::getOrAllocChunk(js::AutoLockGC const&, js::gc::AutoMaybeStartBackgroundAllocation&)" ],
-        [ "Barrier.h", "set" ],
-        [ "Stack.h", "context" ],
-        [ "Stack.h", "js::HelperThread::handleIonWorkload(js::AutoLockHelperThreadState&)" ],
-        [ "ObjectGroup.h", "addendumKind" ],
-        [ "ObjectGroup.h", "generation" ],
-        [ "ObjectGroup.h", "js::jit::MObjectState::New(js::jit::TempAllocator&, js::jit::MDefinition*)" ],
-        [ "TypeInference-inl.h", "setBasePropertyCount" ],
-        [ "jsscript.h", "decref" ],
-        [ "jsfun.h", "setResolvedLength" ],
-        [ "jsfun.h", "needsSomeEnvironmentObject" ]
-    ]
 }
