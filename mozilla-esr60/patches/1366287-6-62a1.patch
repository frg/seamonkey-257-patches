# HG changeset patch
# User Robin Templeton <robin@igalia.com>
# Date 1526094125 25200
# Node ID ca1a8526921e175db6ef1f03a00a0a31f15d2397
# Parent  75e96d97832a6127cae85b7b0a3386f515dfe4d5
bug 1366287 - Part 6: Implement boolean-to-BigInt conversion.  r=jwalden

diff --git a/js/src/vm/BigIntType.cpp b/js/src/vm/BigIntType.cpp
--- a/js/src/vm/BigIntType.cpp
+++ b/js/src/vm/BigIntType.cpp
@@ -59,24 +59,32 @@ void BigInt::init() {
 BigInt* BigInt::create(JSContext* cx) {
   BigInt* x = Allocate<BigInt>(cx);
   if (!x)
     return nullptr;
   mpz_init(x->num_);  // to zero
   return x;
 }
 
-BigInt* BigInt::create(JSContext* cx, double d) {
+BigInt* BigInt::createFromDouble(JSContext* cx, double d) {
   BigInt* x = Allocate<BigInt>(cx);
   if (!x)
     return nullptr;
   mpz_init_set_d(x->num_, d);
   return x;
 }
 
+BigInt* BigInt::createFromBoolean(JSContext* cx, bool b) {
+  BigInt* x = Allocate<BigInt>(cx);
+  if (!x)
+    return nullptr;
+  mpz_init_set_ui(x->num_, b);
+  return x;
+}
+
 // BigInt proposal section 5.1.1
 static bool IsInteger(double d) {
   // Step 1 is an assertion checked by the caller.
   // Step 2.
   if (!mozilla::IsFinite(d))
     return false;
 
   // Step 3.
@@ -96,17 +104,17 @@ BigInt* js::NumberToBigInt(JSContext* cx
   // Step 2.
   if (!IsInteger(d)) {
     JS_ReportErrorNumberASCII(cx, GetErrorMessage, nullptr,
                               JSMSG_NUMBER_TO_BIGINT);
     return nullptr;
   }
 
   // Step 3.
-  return BigInt::create(cx, d);
+  return BigInt::createFromDouble(cx, d);
 }
 
 BigInt* BigInt::copy(JSContext* cx, HandleBigInt x) {
   BigInt* bi = Allocate<BigInt>(cx);
   if (!bi)
     return nullptr;
   mpz_init_set(bi->num_, x->num_);
   return bi;
@@ -116,20 +124,23 @@ BigInt* BigInt::copy(JSContext* cx, Hand
 BigInt* js::ToBigInt(JSContext* cx, HandleValue val) {
   RootedValue v(cx, val);
 
   // Step 1.
   if (!ToPrimitive(cx, JSTYPE_NUMBER, &v))
     return nullptr;
 
   // Step 2.
-  // Boolean and string conversions are not yet supported.
+  // String conversions are not yet supported.
   if (v.isBigInt())
     return v.toBigInt();
 
+  if (v.isBoolean())
+    return BigInt::createFromBoolean(cx, v.toBoolean());
+
   JS_ReportErrorNumberASCII(cx, GetErrorMessage, nullptr, JSMSG_NOT_BIGINT);
   return nullptr;
 }
 
 JSLinearString* BigInt::toString(JSContext* cx, BigInt* x, uint8_t radix) {
   MOZ_ASSERT(2 <= radix && radix <= 36);
   // We need two extra chars for '\0' and potentially '-'.
   size_t strSize = mpz_sizeinbase(x->num_, 10) + 2;
diff --git a/js/src/vm/BigIntType.h b/js/src/vm/BigIntType.h
--- a/js/src/vm/BigIntType.h
+++ b/js/src/vm/BigIntType.h
@@ -28,17 +28,19 @@ class BigInt final : public js::gc::Tenu
     mpz_t num_;
     uint8_t unused_[js::gc::MinCellSize];
   };
 
  public:
   // Allocate and initialize a BigInt value
   static BigInt* create(JSContext* cx);
 
-  static BigInt* create(JSContext* cx, double d);
+  static BigInt* createFromDouble(JSContext* cx, double d);
+
+  static BigInt* createFromBoolean(JSContext* cx, bool b);
 
   static const JS::TraceKind TraceKind = JS::TraceKind::BigInt;
 
   void traceChildren(JSTracer* trc);
 
   void finalize(js::FreeOp* fop);
 
   js::HashNumber hash();
