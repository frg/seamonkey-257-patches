# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1592523941 0
# Node ID 98efceb86ec55e39c4306bca0ec27486366ec9ad
# Parent  f0d2b24116317762b41411ccded6bcb5677f2f72
Bug 1643258 - Avoid the use of the __cxa_thread_atexit_impl symbol. r=froydnj
Partial - Include all changes to stdc++compat.cpp.  For check_binary.py,
only include changes to check CXXABI symbol versions.

When linking a weak symbol in an object against a library where the
symbol is provided with a version, the final binary get a weak versioned
symbol reference.

It turns out weak versioned symbols still make the dynamic linker need
the symbol version, even if all symbols needed with that version are
weak.

Practically speaking, that means with bug 1634204, we now end up with
a weak versioned symbol reference to __cxa_thread_atexit_impl with
version GLIBC_2.18, and glibcs without the symbol can't fulfil that
version, even though the weak symbol is the only thing we need from that
version.

This means the check_binary changes in bug 1634204 are too
relaxed, so we revert them (although we keep the easier to read
conditions in check_dep_versions).

We also introduce a hack in stdc++compat.cpp (although it's not
technically entirely about libstdc++ compat) so that we avoid the weak
symbol reference while keeping the intended baseline for libstdc++ and
glibc.

Differential Revision: https://phabricator.services.mozilla.com/D79773

diff --git a/build/unix/stdc++compat/stdc++compat.cpp b/build/unix/stdc++compat/stdc++compat.cpp
--- a/build/unix/stdc++compat/stdc++compat.cpp
+++ b/build/unix/stdc++compat/stdc++compat.cpp
@@ -3,16 +3,17 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include <ostream>
 #include <istream>
 #include <string>
 #include <stdarg.h>
 #include <stdio.h>
 #include <mozilla/Assertions.h>
+#include <cxxabi.h>
 
 /* GLIBCXX_3.4.16 is from gcc 4.6.1 (172240)
    GLIBCXX_3.4.17 is from gcc 4.7.0 (174383)
    GLIBCXX_3.4.18 is from gcc 4.8.0 (190787)
    GLIBCXX_3.4.19 is from gcc 4.8.1 (199309)
    GLIBCXX_3.4.20 is from gcc 4.9.0 (199307)
    GLIBCXX_3.4.21 is from gcc 5.0 (210290)
    GLIBCXX_3.4.22 is from gcc 6.0 (222482)
@@ -139,17 +140,40 @@ struct StateWrapper : public thread::_Im
 #endif
 
 #if MOZ_LIBSTDCXX_VERSION >= GLIBCXX_VERSION(3, 4, 21)
 namespace std {
 /* Instantiate this template to avoid GLIBCXX_3.4.21 symbol versions
  * depending on optimization level */
 template basic_ios<char, char_traits<char> >::operator bool() const;
 }  // namespace std
+
+#if !defined(MOZ_ASAN) && !defined(MOZ_TSAN)
+/* operator delete with size is only available in CXXAPI_1.3.9, equivalent to
+ * GLIBCXX_3.4.21. */
+void operator delete(void* ptr, size_t size) noexcept(true) {
+  ::operator delete(ptr);
+}
+#endif
 #endif
 
 #if MOZ_LIBSTDCXX_VERSION >= GLIBCXX_VERSION(3, 4, 23)
 namespace std {
 /* Instantiate this template to avoid GLIBCXX_3.4.23 symbol versions
  * depending on optimization level */
 template basic_string<char, char_traits<char>, allocator<char>>::basic_string(const basic_string&, size_t, const allocator<char>&);
 } // namespace std
 #endif
+
+/* The __cxa_thread_atexit_impl symbol is only available on GLIBC 2.18, but we
+ * want things to keep working on 2.17. It's not actually used directly from
+ * C++ code, but through __cxa_thead_atexit in libstdc++. The problem we have,
+ * though, is that rust's libstd also uses it, introducing a dependency we
+ * don't actually want. Fortunately, we can fall back to libstdc++'s wrapper
+ * (which, on systems without __cxa_thread_atexit_impl, has its own compatible
+ * implementation).
+ * The __cxa_thread_atexit symbol itself is marked CXXABI_1.3.7, which is
+ * equivalent to GLIBCXX_3.4.18.
+ */
+extern "C" int __cxa_thread_atexit_impl(void (*dtor)(void*), void* obj,
+                                        void* dso_handle) {
+  return __cxxabiv1::__cxa_thread_atexit(dtor, obj, dso_handle);
+}
diff --git a/python/mozbuild/mozbuild/action/check_binary.py b/python/mozbuild/mozbuild/action/check_binary.py
--- a/python/mozbuild/mozbuild/action/check_binary.py
+++ b/python/mozbuild/mozbuild/action/check_binary.py
@@ -16,16 +16,17 @@ from mozbuild.util import memoize
 from mozpack.executables import (
     get_type,
     ELF,
     MACHO,
 )
 
 
 STDCXX_MAX_VERSION = Version('3.4.19')
+CXXABI_MAX_VERSION = Version('1.3.7')
 GLIBC_MAX_VERSION = Version('2.17')
 LIBGCC_MAX_VERSION = Version('4.8')
 
 HOST = {
     'MOZ_LIBSTDCXX_VERSION':
         buildconfig.substs.get('MOZ_LIBSTDCXX_HOST_VERSION'),
     'platform': buildconfig.substs['HOST_OS_ARCH'],
     'readelf': 'readelf',
@@ -129,16 +130,18 @@ def check_dep_versions(target, binary, l
         ] + [
             ' {} ({})'.format(s['name'], s['version']) for s in unwanted
         ]))
 
 
 def check_stdcxx(target, binary):
     check_dep_versions(
         target, binary, 'libstdc++', 'GLIBCXX', STDCXX_MAX_VERSION)
+    check_dep_versions(
+        target, binary, 'libstdc++', 'CXXABI', CXXABI_MAX_VERSION)
 
 
 def check_libgcc(target, binary):
     check_dep_versions(target, binary, 'libgcc', 'GCC', LIBGCC_MAX_VERSION)
 
 
 def check_glibc(target, binary):
     check_dep_versions(target, binary, 'libc', 'GLIBC', GLIBC_MAX_VERSION)
