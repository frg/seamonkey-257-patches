# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1520795253 -3600
# Node ID 55dc61b6ab44f479db63e97bd5bf223099738048
# Parent  7c658ae9a6826d5aa494218eab46e6c4253fda8f
Bug 1444580: Move mExpandoAndGeneration back to nsDocument for now. r=smaug

MozReview-Commit-ID: EArKdxEoXaJ

diff --git a/dom/base/nsDocument.cpp b/dom/base/nsDocument.cpp
--- a/dom/base/nsDocument.cpp
+++ b/dom/base/nsDocument.cpp
@@ -2929,57 +2929,61 @@ static void GetFormattedTimeString(PRTim
 void nsIDocument::GetLastModified(nsAString& aLastModified) const {
   if (!mLastModified.IsEmpty()) {
     aLastModified.Assign(mLastModified);
   } else {
     GetFormattedTimeString(PR_Now(), aLastModified);
   }
 }
 
+static void IncrementExpandoGeneration(nsIDocument& aDoc) {
+  ++static_cast<nsDocument&>(aDoc).mExpandoAndGeneration.generation;
+}
+
 void nsIDocument::AddToNameTable(Element* aElement, nsAtom* aName) {
   MOZ_ASSERT(
       nsGenericHTMLElement::ShouldExposeNameAsHTMLDocumentProperty(aElement),
       "Only put elements that need to be exposed as document['name'] in "
       "the named table.");
 
   nsIdentifierMapEntry* entry = mIdentifierMap.PutEntry(aName);
 
   // Null for out-of-memory
   if (entry) {
     if (!entry->HasNameElement() &&
         !entry->HasIdElementExposedAsHTMLDocumentProperty()) {
-      ++mExpandoAndGeneration.generation;
+      IncrementExpandoGeneration(*this);
     }
     entry->AddNameElement(this, aElement);
   }
 }
 
 void nsIDocument::RemoveFromNameTable(Element* aElement, nsAtom* aName) {
   // Speed up document teardown
   if (mIdentifierMap.Count() == 0) return;
 
   nsIdentifierMapEntry* entry = mIdentifierMap.GetEntry(aName);
   if (!entry)  // Could be false if the element was anonymous, hence never added
     return;
 
   entry->RemoveNameElement(aElement);
   if (!entry->HasNameElement() &&
       !entry->HasIdElementExposedAsHTMLDocumentProperty()) {
-    ++mExpandoAndGeneration.generation;
+    IncrementExpandoGeneration(*this);
   }
 }
 
 void nsIDocument::AddToIdTable(Element* aElement, nsAtom* aId) {
   nsIdentifierMapEntry* entry = mIdentifierMap.PutEntry(aId);
 
   if (entry) { /* True except on OOM */
     if (nsGenericHTMLElement::ShouldExposeIdAsHTMLDocumentProperty(aElement) &&
         !entry->HasNameElement() &&
         !entry->HasIdElementExposedAsHTMLDocumentProperty()) {
-      ++mExpandoAndGeneration.generation;
+      IncrementExpandoGeneration(*this);
     }
     entry->AddIdElement(aElement);
   }
 }
 
 void nsIDocument::RemoveFromIdTable(Element* aElement, nsAtom* aId) {
   NS_ASSERTION(aId, "huhwhatnow?");
 
@@ -2991,17 +2995,17 @@ void nsIDocument::RemoveFromIdTable(Elem
   nsIdentifierMapEntry* entry = mIdentifierMap.GetEntry(aId);
   if (!entry)  // Can be null for XML elements with changing ids.
     return;
 
   entry->RemoveIdElement(aElement);
   if (nsGenericHTMLElement::ShouldExposeIdAsHTMLDocumentProperty(aElement) &&
       !entry->HasNameElement() &&
       !entry->HasIdElementExposedAsHTMLDocumentProperty()) {
-    ++mExpandoAndGeneration.generation;
+    IncrementExpandoGeneration(*this);
   }
   if (entry->IsEmpty()) {
     mIdentifierMap.RemoveEntry(entry);
   }
 }
 
 nsIPrincipal* nsDocument::GetPrincipal() { return NodePrincipal(); }
 
@@ -8006,17 +8010,17 @@ void nsIDocument::MutationEventDispatche
 }
 
 void nsIDocument::DestroyElementMaps() {
 #ifdef DEBUG
   mStyledLinksCleared = true;
 #endif
   mStyledLinks.Clear();
   mIdentifierMap.Clear();
-  ++mExpandoAndGeneration.generation;
+  IncrementExpandoGeneration(*this);
 }
 
 void nsIDocument::RefreshLinkHrefs() {
   // Get a list of all links we know about.  We will reset them, which will
   // remove them from the document, so we need a copy of what is in the
   // hashtable.
   LinkArray linksToNotify(mStyledLinks.Count());
   for (auto iter = mStyledLinks.ConstIter(); !iter.Done(); iter.Next()) {
diff --git a/dom/base/nsDocument.h b/dom/base/nsDocument.h
--- a/dom/base/nsDocument.h
+++ b/dom/base/nsDocument.h
@@ -8,16 +8,17 @@
  * Base class for all our document implementations.
  */
 
 #ifndef nsDocument_h___
 #define nsDocument_h___
 
 #include "nsIDocument.h"
 
+#include "jsfriendapi.h"
 #include "nsCOMPtr.h"
 #include "nsAutoPtr.h"
 #include "nsCRT.h"
 #include "nsWeakReference.h"
 #include "nsWeakPtr.h"
 #include "nsTArray.h"
 #include "nsIdentifierMapEntry.h"
 #include "nsIDOMDocument.h"
@@ -363,16 +364,24 @@ class nsDocument : public nsIDocument,
 #endif
 
   explicit nsDocument(const char* aContentType);
   virtual ~nsDocument();
 
   void EnsureOnloadBlocker();
 
  public:
+  // FIXME(emilio): This needs to be here instead of in nsIDocument because Rust
+  // can't represent alignas(8) values on 32-bit architectures, which would
+  // cause nsIDocument's layout to be wrong in the Rust side.
+  //
+  // This can be fixed after updating to rust 1.25 and updating bindgen to
+  // include https://github.com/rust-lang-nursery/rust-bindgen/pull/1271.
+  js::ExpandoAndGeneration mExpandoAndGeneration;
+
   RefPtr<mozilla::EventListenerManager> mListenerManager;
 
   nsClassHashtable<nsStringHashKey, nsRadioGroupStruct> mRadioGroups;
 
   // Parser aborted. True if the parser of this document was forcibly
   // terminated instead of letting it finish at its own pace.
   bool mParserAborted : 1;
 
diff --git a/dom/base/nsIDocument.h b/dom/base/nsIDocument.h
--- a/dom/base/nsIDocument.h
+++ b/dom/base/nsIDocument.h
@@ -1,17 +1,16 @@
 /* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 #ifndef nsIDocument_h___
 #define nsIDocument_h___
 
-#include "jsfriendapi.h"
 #include "mozilla/FlushType.h"    // for enum
 #include "nsAttrAndChildArray.h"
 #include "nsAutoPtr.h"            // for member
 #include "nsCOMArray.h"           // for member
 #include "nsCompatibility.h"      // for member
 #include "nsCOMPtr.h"             // for member
 #include "nsGkAtoms.h"            // for static class members
 #include "nsIApplicationCache.h"
@@ -4008,21 +4007,16 @@ class nsIDocument : public nsINode,
   RefPtr<nsRunnableMethod<nsIDocument>> mFrameLoaderRunner;
 
   // The layout history state that should be used by nodes in this
   // document.  We only actually store a pointer to it when:
   // 1)  We have no script global object.
   // 2)  We haven't had Destroy() called on us yet.
   nsCOMPtr<nsILayoutHistoryState> mLayoutHistoryState;
 
- public:
-  js::ExpandoAndGeneration mExpandoAndGeneration;
-
- protected:
-
   nsTArray<RefPtr<mozilla::StyleSheet>> mOnDemandBuiltInUASheets;
   nsTArray<RefPtr<mozilla::StyleSheet>>
       mAdditionalSheets[AdditionalSheetTypeCount];
 
   // Member to store out last-selected stylesheet set.
   nsString mLastStyleSheetSet;
   RefPtr<nsDOMStyleSheetSetList> mStyleSheetSetList;
 
