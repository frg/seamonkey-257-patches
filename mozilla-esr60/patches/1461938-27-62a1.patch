# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1527097416 -7200
# Node ID 7d6335bbd6c3d2d7559c0cbdc6a3bdb797d75bad
# Parent  830d68c4eb72bed08674cc423422ee19cf4058c4
Bug 1461938 part 27 - Use UniquePtr for various compartment pointers. r=jonco

diff --git a/js/src/builtin/TypedObject.cpp b/js/src/builtin/TypedObject.cpp
--- a/js/src/builtin/TypedObject.cpp
+++ b/js/src/builtin/TypedObject.cpp
@@ -1184,17 +1184,17 @@ uint8_t* TypedObject::typedMemBase() con
   JSObject& owner = as<OutlineTypedObject>().owner();
   if (owner.is<ArrayBufferObject>())
     return owner.as<ArrayBufferObject>().dataPointer();
   return owner.as<InlineTypedObject>().inlineTypedMem();
 }
 
 bool TypedObject::isAttached() const {
   if (is<InlineTransparentTypedObject>()) {
-    ObjectWeakMap* table = compartment()->lazyArrayBuffers;
+    ObjectWeakMap* table = compartment()->lazyArrayBuffers.get();
     if (table) {
       JSObject* buffer = table->lookup(this);
       if (buffer) return !buffer->as<ArrayBufferObject>().isDetached();
     }
     return true;
   }
   if (is<InlineOpaqueTypedObject>()) return true;
   if (!as<OutlineTypedObject>().outOfLineTypedMem()) return false;
@@ -1917,22 +1917,25 @@ void OutlineTypedObject::notifyBufferDet
     nursery.setForwardingPointerWhileTenuring(oldData, newData, direct);
   }
 
   return 0;
 }
 
 ArrayBufferObject* InlineTransparentTypedObject::getOrCreateBuffer(
     JSContext* cx) {
-  ObjectWeakMap*& table = cx->compartment()->lazyArrayBuffers;
-  if (!table) {
-    table = cx->new_<ObjectWeakMap>(cx);
+  if (!cx->compartment()->lazyArrayBuffers) {
+    auto table = cx->make_unique<ObjectWeakMap>(cx);
     if (!table || !table->init()) return nullptr;
+
+    cx->compartment()->lazyArrayBuffers = Move(table);
   }
 
+  ObjectWeakMap* table = cx->compartment()->lazyArrayBuffers.get();
+
   JSObject* obj = table->lookup(this);
   if (obj) return &obj->as<ArrayBufferObject>();
 
   ArrayBufferObject::BufferContents contents =
       ArrayBufferObject::BufferContents::createPlain(inlineTypedMem());
   size_t nbytes = typeDescr().size();
 
   // Prevent GC under ArrayBufferObject::create, which might move this object
diff --git a/js/src/jsapi-tests/testGCGrayMarking.cpp b/js/src/jsapi-tests/testGCGrayMarking.cpp
--- a/js/src/jsapi-tests/testGCGrayMarking.cpp
+++ b/js/src/jsapi-tests/testGCGrayMarking.cpp
@@ -8,30 +8,38 @@
 #include "gc/Heap.h"
 #include "gc/WeakMap.h"
 #include "gc/Zone.h"
 #include "jsapi-tests/tests.h"
 
 using namespace js;
 using namespace js::gc;
 
+namespace js {
+
+struct GCManagedObjectWeakMap : public ObjectWeakMap {
+  using ObjectWeakMap::ObjectWeakMap;
+};
+
+}  // namespace js
+
 namespace JS {
 
 template <>
-struct DeletePolicy<js::ObjectWeakMap>
-    : public js::GCManagedDeletePolicy<js::ObjectWeakMap> {};
+struct DeletePolicy<js::GCManagedObjectWeakMap>
+    : public js::GCManagedDeletePolicy<js::GCManagedObjectWeakMap> {};
 
 template <>
-struct MapTypeToRootKind<js::ObjectWeakMap*> {
+struct MapTypeToRootKind<js::GCManagedObjectWeakMap*> {
   static const JS::RootKind kind = JS::RootKind::Traceable;
 };
 
 template <>
-struct GCPolicy<js::ObjectWeakMap*>
-    : public NonGCPointerPolicy<js::ObjectWeakMap*> {};
+struct GCPolicy<js::GCManagedObjectWeakMap*>
+    : public NonGCPointerPolicy<js::GCManagedObjectWeakMap*> {};
 
 }  // namespace JS
 
 class AutoNoAnalysisForTest {
  public:
   AutoNoAnalysisForTest() {}
 } JS_HAZ_GC_SUPPRESSED;
 
@@ -314,22 +322,22 @@ bool TestWeakMaps() {
   grayRoots.grayRoot1 = nullptr;
   grayRoots.grayRoot2 = nullptr;
 
   return true;
 }
 
 bool TestUnassociatedWeakMaps() {
   // Make a weakmap that's not associated with a JSObject.
-  auto weakMap = cx->make_unique<ObjectWeakMap>(cx);
+  auto weakMap = cx->make_unique<GCManagedObjectWeakMap>(cx);
   CHECK(weakMap);
   CHECK(weakMap->init());
 
   // Make sure this gets traced during GC.
-  Rooted<ObjectWeakMap*> rootMap(cx, weakMap.get());
+  Rooted<GCManagedObjectWeakMap*> rootMap(cx, weakMap.get());
 
   JSObject* key = AllocWeakmapKeyObject();
   CHECK(key);
 
   JSObject* value = AllocPlainObject();
   CHECK(value);
 
   CHECK(weakMap->add(cx, key, value));
diff --git a/js/src/jsfriendapi.cpp b/js/src/jsfriendapi.cpp
--- a/js/src/jsfriendapi.cpp
+++ b/js/src/jsfriendapi.cpp
@@ -1178,17 +1178,17 @@ js::AutoCTypesActivityCallback::AutoCTyp
 }
 
 JS_FRIEND_API void js::SetAllocationMetadataBuilder(
     JSContext* cx, const AllocationMetadataBuilder* callback) {
   cx->realm()->setAllocationMetadataBuilder(callback);
 }
 
 JS_FRIEND_API JSObject* js::GetAllocationMetadata(JSObject* obj) {
-  ObjectWeakMap* map = obj->compartment()->objectMetadataTable;
+  ObjectWeakMap* map = obj->compartment()->objectMetadataTable.get();
   if (map) return map->lookup(obj);
   return nullptr;
 }
 
 JS_FRIEND_API bool js::ReportIsNotFunction(JSContext* cx, HandleValue v) {
   assertSameCompartment(cx, v);
   return ReportIsNotFunction(cx, v, -1);
 }
diff --git a/js/src/vm/EnvironmentObject.cpp b/js/src/vm/EnvironmentObject.cpp
--- a/js/src/vm/EnvironmentObject.cpp
+++ b/js/src/vm/EnvironmentObject.cpp
@@ -2336,31 +2336,32 @@ void DebugEnvironments::checkHashTablesA
  * fine.
  */
 static bool CanUseDebugEnvironmentMaps(JSContext* cx) {
   return cx->realm()->isDebuggee();
 }
 
 DebugEnvironments* DebugEnvironments::ensureCompartmentData(JSContext* cx) {
   JSCompartment* c = cx->compartment();
-  if (c->debugEnvs) return c->debugEnvs;
+  if (c->debugEnvs)
+    return c->debugEnvs.get();
 
   auto debugEnvs = cx->make_unique<DebugEnvironments>(cx, cx->zone());
   if (!debugEnvs || !debugEnvs->init()) {
     ReportOutOfMemory(cx);
     return nullptr;
   }
 
-  c->debugEnvs = debugEnvs.release();
-  return c->debugEnvs;
+  c->debugEnvs = Move(debugEnvs);
+  return c->debugEnvs.get();
 }
 
 /* static */ DebugEnvironmentProxy* DebugEnvironments::hasDebugEnvironment(
     JSContext* cx, EnvironmentObject& env) {
-  DebugEnvironments* envs = env.compartment()->debugEnvs;
+  DebugEnvironments* envs = env.compartment()->debugEnvs.get();
   if (!envs) return nullptr;
 
   if (JSObject* obj = envs->proxiedEnvs.lookup(&env)) {
     MOZ_ASSERT(CanUseDebugEnvironmentMaps(cx));
     return &obj->as<DebugEnvironmentProxy>();
   }
 
   return nullptr;
@@ -2379,17 +2380,17 @@ DebugEnvironments* DebugEnvironments::en
 
   return envs->proxiedEnvs.add(cx, env, debugEnv);
 }
 
 /* static */ DebugEnvironmentProxy* DebugEnvironments::hasDebugEnvironment(
     JSContext* cx, const EnvironmentIter& ei) {
   MOZ_ASSERT(!ei.hasSyntacticEnvironment());
 
-  DebugEnvironments* envs = cx->compartment()->debugEnvs;
+  DebugEnvironments* envs = cx->compartment()->debugEnvs.get();
   if (!envs) return nullptr;
 
   if (MissingEnvironmentMap::Ptr p =
           envs->missingEnvs.lookup(MissingEnvironmentKey(ei))) {
     MOZ_ASSERT(CanUseDebugEnvironmentMaps(cx));
     return p->value();
   }
   return nullptr;
@@ -2530,17 +2531,17 @@ DebugEnvironments* DebugEnvironments::en
 
   debugEnv->initSnapshot(*snapshot);
 }
 
 /* static */ void DebugEnvironments::onPopCall(JSContext* cx,
                                                AbstractFramePtr frame) {
   assertSameCompartment(cx, frame);
 
-  DebugEnvironments* envs = cx->compartment()->debugEnvs;
+  DebugEnvironments* envs = cx->compartment()->debugEnvs.get();
   if (!envs) return;
 
   Rooted<DebugEnvironmentProxy*> debugEnv(cx, nullptr);
 
   FunctionScope* funScope = &frame.script()->bodyScope()->as<FunctionScope>();
   if (funScope->hasEnvironment()) {
     MOZ_ASSERT(frame.callee()->needsCallObject());
 
@@ -2567,26 +2568,26 @@ DebugEnvironments* DebugEnvironments::en
 
   if (debugEnv) DebugEnvironments::takeFrameSnapshot(cx, debugEnv, frame);
 }
 
 void DebugEnvironments::onPopLexical(JSContext* cx, AbstractFramePtr frame,
                                      jsbytecode* pc) {
   assertSameCompartment(cx, frame);
 
-  DebugEnvironments* envs = cx->compartment()->debugEnvs;
+  DebugEnvironments* envs = cx->compartment()->debugEnvs.get();
   if (!envs) return;
 
   EnvironmentIter ei(cx, frame, pc);
   onPopLexical(cx, ei);
 }
 
 template <typename Environment, typename Scope>
 void DebugEnvironments::onPopGeneric(JSContext* cx, const EnvironmentIter& ei) {
-  DebugEnvironments* envs = cx->compartment()->debugEnvs;
+  DebugEnvironments* envs = cx->compartment()->debugEnvs.get();
   if (!envs) return;
 
   MOZ_ASSERT(ei.withinInitialFrame());
   MOZ_ASSERT(ei.scope().is<Scope>());
 
   Rooted<Environment*> env(cx);
   if (MissingEnvironmentMap::Ptr p =
           envs->missingEnvs.lookup(MissingEnvironmentKey(ei))) {
@@ -2610,38 +2611,38 @@ void DebugEnvironments::onPopGeneric(JSC
 void DebugEnvironments::onPopLexical(JSContext* cx, const EnvironmentIter& ei) {
   onPopGeneric<LexicalEnvironmentObject, LexicalScope>(cx, ei);
 }
 
 void DebugEnvironments::onPopVar(JSContext* cx, AbstractFramePtr frame,
                                  jsbytecode* pc) {
   assertSameCompartment(cx, frame);
 
-  DebugEnvironments* envs = cx->compartment()->debugEnvs;
+  DebugEnvironments* envs = cx->compartment()->debugEnvs.get();
   if (!envs) return;
 
   EnvironmentIter ei(cx, frame, pc);
   onPopVar(cx, ei);
 }
 
 void DebugEnvironments::onPopVar(JSContext* cx, const EnvironmentIter& ei) {
   if (ei.scope().is<EvalScope>())
     onPopGeneric<VarEnvironmentObject, EvalScope>(cx, ei);
   else
     onPopGeneric<VarEnvironmentObject, VarScope>(cx, ei);
 }
 
 void DebugEnvironments::onPopWith(AbstractFramePtr frame) {
-  if (DebugEnvironments* envs = frame.compartment()->debugEnvs)
+  if (DebugEnvironments* envs = frame.compartment()->debugEnvs.get())
     envs->liveEnvs.remove(
         &frame.environmentChain()->as<WithEnvironmentObject>());
 }
 
 void DebugEnvironments::onCompartmentUnsetIsDebuggee(JSCompartment* c) {
-  if (DebugEnvironments* envs = c->debugEnvs) {
+  if (DebugEnvironments* envs = c->debugEnvs.get()) {
     envs->proxiedEnvs.clear();
     envs->missingEnvs.clear();
     envs->liveEnvs.clear();
   }
 }
 
 bool DebugEnvironments::updateLiveEnvironments(JSContext* cx) {
   if (!CheckRecursionLimit(cx)) return false;
@@ -2691,17 +2692,17 @@ bool DebugEnvironments::updateLiveEnviro
     frame.setPrevUpToDate();
   }
 
   return true;
 }
 
 LiveEnvironmentVal* DebugEnvironments::hasLiveEnvironment(
     EnvironmentObject& env) {
-  DebugEnvironments* envs = env.compartment()->debugEnvs;
+  DebugEnvironments* envs = env.compartment()->debugEnvs.get();
   if (!envs) return nullptr;
 
   if (LiveEnvironmentMap::Ptr p = envs->liveEnvs.lookup(&env))
     return &p->value();
 
   return nullptr;
 }
 
@@ -2725,17 +2726,17 @@ LiveEnvironmentVal* DebugEnvironments::h
 
     frame.unsetPrevUpToDate();
   }
 }
 
 /* static */ void DebugEnvironments::forwardLiveFrame(JSContext* cx,
                                                       AbstractFramePtr from,
                                                       AbstractFramePtr to) {
-  DebugEnvironments* envs = cx->compartment()->debugEnvs;
+  DebugEnvironments* envs = cx->compartment()->debugEnvs.get();
   if (!envs) return;
 
   for (MissingEnvironmentMap::Enum e(envs->missingEnvs); !e.empty();
        e.popFront()) {
     MissingEnvironmentKey key = e.front().key();
     if (key.frame() == from) {
       key.updateFrame(to);
       e.rekeyFront(key);
diff --git a/js/src/vm/EnvironmentObject.h b/js/src/vm/EnvironmentObject.h
--- a/js/src/vm/EnvironmentObject.h
+++ b/js/src/vm/EnvironmentObject.h
@@ -1165,17 +1165,9 @@ MOZ_MUST_USE bool GetFrameEnvironmentAnd
                                               MutableHandleScope scope);
 
 #ifdef DEBUG
 bool AnalyzeEntrainedVariables(JSContext* cx, HandleScript script);
 #endif
 
 }  // namespace js
 
-namespace JS {
-
-template <>
-struct DeletePolicy<js::DebugEnvironments>
-    : public js::GCManagedDeletePolicy<js::DebugEnvironments> {};
-
-}  // namespace JS
-
 #endif /* vm_EnvironmentObject_h */
diff --git a/js/src/vm/JSCompartment.cpp b/js/src/vm/JSCompartment.cpp
--- a/js/src/vm/JSCompartment.cpp
+++ b/js/src/vm/JSCompartment.cpp
@@ -44,25 +44,20 @@ using mozilla::PodArrayZero;
 
 JSCompartment::JSCompartment(Zone* zone)
     : zone_(zone),
       runtime_(zone->runtimeFromAnyThread()),
       data(nullptr),
       regExps(),
       globalWriteBarriered(0),
       detachedTypedObjects(0),
-      objectMetadataTable(nullptr),
       innerViews(zone),
-      lazyArrayBuffers(nullptr),
-      nonSyntacticLexicalEnvironments_(nullptr),
       gcIncomingGrayPointers(nullptr),
       validAccessPtr(nullptr),
-      debugEnvs(nullptr),
       enumerators(nullptr),
-      jitCompartment_(nullptr),
       lcovOutput() {
   runtime_->numCompartments++;
 }
 
 Realm::Realm(JS::Zone* zone, const JS::RealmOptions& options)
     : JSCompartment(zone),
       creationOptions_(options.creationOptions()),
       behaviors_(options.behaviors()),
@@ -80,38 +75,40 @@ Realm::~Realm() {
 }
 
 JSCompartment::~JSCompartment() {
   // Write the code coverage information in a file.
   JSRuntime* rt = runtimeFromMainThread();
   if (rt->lcovOutput().isEnabled())
     rt->lcovOutput().writeLCovResult(lcovOutput);
 
-  js_delete(jitCompartment_);
-  js_delete(debugEnvs);
-  js_delete(objectMetadataTable);
-  js_delete(lazyArrayBuffers);
-  js_delete(nonSyntacticLexicalEnvironments_);
-  js_free(enumerators);
+  MOZ_ASSERT(enumerators == iteratorSentinel_.get());
 
 #ifdef DEBUG
   // Avoid assertion destroying the unboxed layouts list if the embedding
   // leaked GC things.
   if (!rt->gc.shutdownCollectedEverything()) unboxedLayouts.clear();
 #endif
 
   runtime_->numCompartments--;
 }
 
 bool JSCompartment::init(JSContext* maybecx) {
   if (!crossCompartmentWrappers.init(0)) {
     if (maybecx) ReportOutOfMemory(maybecx);
     return false;
   }
 
+  NativeIteratorSentinel sentinel(NativeIterator::allocateSentinel(maybecx));
+  if (!sentinel)
+    return false;
+
+  iteratorSentinel_ = Move(sentinel);
+  enumerators = iteratorSentinel_.get();
+
   return true;
 }
 
 bool Realm::init(JSContext* maybecx) {
   // Initialize JSCompartment. This is temporary until Realm and
   // JSCompartment are completely separated.
   if (!JSCompartment::init(maybecx))
     return false;
@@ -122,19 +119,16 @@ bool Realm::init(JSContext* maybecx) {
    *
    * As a hack, we clear our timezone cache every time we create a new realm.
    * This ensures that the cache is always relatively fresh, but shouldn't
    * interfere with benchmarks that create tons of date objects (unless they
    * also create tons of iframes, which seems unlikely).
    */
   js::ResetTimeZoneInternal(ResetTimeZoneMode::DontResetIfOffsetUnchanged);
 
-  enumerators = NativeIterator::allocateSentinel(maybecx);
-  if (!enumerators) return false;
-
   if (!savedStacks_.init() || !varNames_.init() || !iteratorCache.init()) {
     if (maybecx) ReportOutOfMemory(maybecx);
     return false;
   }
 
   return true;
 }
 
@@ -170,27 +164,25 @@ jit::JitRuntime* JSRuntime::createJitRun
 }
 
 bool JSCompartment::ensureJitCompartmentExists(JSContext* cx) {
   using namespace js::jit;
   if (jitCompartment_) return true;
 
   if (!zone()->getJitZone(cx)) return false;
 
-  /* Set the compartment early, so linking works. */
-  jitCompartment_ = cx->new_<JitCompartment>();
+  UniquePtr<JitCompartment> jitComp = cx->make_unique<JitCompartment>();
+  if (!jitComp)
+    return false;
 
-  if (!jitCompartment_) return false;
-
-  if (!jitCompartment_->initialize(cx)) {
-    js_delete(jitCompartment_);
-    jitCompartment_ = nullptr;
+  if (!jitComp->initialize(cx)) {
     return false;
   }
 
+  jitCompartment_ = Move(jitComp);
   return true;
 }
 
 #ifdef JSGC_HASH_TABLE_CHECKS
 
 void js::DtoaCache::checkCacheAfterMovingGC() {
   MOZ_ASSERT(!s || !IsForwarded(s));
 }
@@ -480,20 +472,21 @@ bool JSCompartment::wrap(JSContext* cx, 
   }
   return true;
 }
 
 LexicalEnvironmentObject*
 JSCompartment::getOrCreateNonSyntacticLexicalEnvironment(
     JSContext* cx, HandleObject enclosing) {
   if (!nonSyntacticLexicalEnvironments_) {
-    nonSyntacticLexicalEnvironments_ = cx->new_<ObjectWeakMap>(cx);
-    if (!nonSyntacticLexicalEnvironments_ ||
-        !nonSyntacticLexicalEnvironments_->init())
+    auto map = cx->make_unique<ObjectWeakMap>(cx);
+    if (!map || !map->init())
       return nullptr;
+
+    nonSyntacticLexicalEnvironments_ = Move(map);
   }
 
   // If a wrapped WithEnvironmentObject was passed in, unwrap it, as we may
   // be creating different WithEnvironmentObject wrappers each time.
   RootedObject key(cx, enclosing);
   if (enclosing->is<WithEnvironmentObject>()) {
     MOZ_ASSERT(!enclosing->as<WithEnvironmentObject>().isSyntactic());
     key = &enclosing->as<WithEnvironmentObject>().object();
@@ -911,30 +904,25 @@ void Realm::forgetAllocationMetadataBuil
   // inline GC allocations when a metadata builder is present), but we do want
   // to cancel off-thread Ion compilations to avoid races when Ion calls
   // hasAllocationMetadataBuilder off-thread.
   CancelOffThreadIonCompile(this);
 
   allocationMetadataBuilder_ = nullptr;
 }
 
-void Realm::clearObjectMetadata() {
-  js_delete(objectMetadataTable);
-  objectMetadataTable = nullptr;
-}
-
 void Realm::setNewObjectMetadata(JSContext* cx, HandleObject obj) {
   assertSameCompartment(cx, this, obj);
 
   AutoEnterOOMUnsafeRegion oomUnsafe;
   if (JSObject* metadata =
           allocationMetadataBuilder_->build(cx, obj, oomUnsafe)) {
     assertSameCompartment(cx, metadata);
     if (!objectMetadataTable) {
-      objectMetadataTable = cx->new_<ObjectWeakMap>(cx);
+      objectMetadataTable = cx->make_unique<ObjectWeakMap>(cx);
       if (!objectMetadataTable || !objectMetadataTable->init())
         oomUnsafe.crash("setNewObjectMetadata");
     }
     if (!objectMetadataTable->add(cx, obj, metadata))
       oomUnsafe.crash("setNewObjectMetadata");
   }
 }
 
diff --git a/js/src/vm/JSCompartment.h b/js/src/vm/JSCompartment.h
--- a/js/src/vm/JSCompartment.h
+++ b/js/src/vm/JSCompartment.h
@@ -622,34 +622,34 @@ struct JSCompartment {
   js::ObjectGroupCompartment objectGroups;
 
 #ifdef JSGC_HASH_TABLE_CHECKS
   void checkWrapperMapAfterMovingGC();
 #endif
 
   // Keep track of the metadata objects which can be associated with each JS
   // object. Both keys and values are in this compartment.
-  js::ObjectWeakMap* objectMetadataTable;
+  js::UniquePtr<js::ObjectWeakMap> objectMetadataTable;
 
   // Map from array buffers to views sharing that storage.
   JS::WeakCache<js::InnerViewTable> innerViews;
 
   // Inline transparent typed objects do not initially have an array buffer,
   // but can have that buffer created lazily if it is accessed later. This
   // table manages references from such typed objects to their buffers.
-  js::ObjectWeakMap* lazyArrayBuffers;
+  js::UniquePtr<js::ObjectWeakMap> lazyArrayBuffers;
 
   // All unboxed layouts in the compartment.
   mozilla::LinkedList<js::UnboxedLayout> unboxedLayouts;
 
  protected:
   // All non-syntactic lexical environments in the compartment. These are kept
   // in a map because when loading scripts into a non-syntactic environment, we
   // need to use the same lexical environment to persist lexical bindings.
-  js::ObjectWeakMap* nonSyntacticLexicalEnvironments_;
+  js::UniquePtr<js::ObjectWeakMap> nonSyntacticLexicalEnvironments_;
 
  public:
   /*
    * During GC, stores the head of a list of incoming pointers from gray cells.
    *
    * The objects in the list are either cross-compartment wrappers, or
    * debugger wrapper objects.  The list link is either in the second extra
    * slot for the former, or a special slot for the latter.
@@ -761,39 +761,44 @@ struct JSCompartment {
 
   js::SavedStacks& savedStacks() { return savedStacks_; }
 
   void findOutgoingEdges(js::gc::ZoneComponentFinder& finder);
 
   static size_t offsetOfRegExps() { return offsetof(JSCompartment, regExps); }
 
   /* Bookkeeping information for debug scope objects. */
-  js::DebugEnvironments* debugEnvs;
+  js::UniquePtr<js::DebugEnvironments> debugEnvs;
 
   /*
    * List of potentially active iterators that may need deleted property
    * suppression.
    */
+ private:
+  using NativeIteratorSentinel =
+      js::UniquePtr<js::NativeIterator, JS::FreePolicy>;
+  NativeIteratorSentinel iteratorSentinel_;
+ public:
   js::NativeIterator* enumerators;
 
   MOZ_ALWAYS_INLINE bool objectMaybeInIteration(JSObject* obj);
 
   // These flags help us to discover if a compartment that shouldn't be alive
   // manages to outlive a GC. Note that these flags have to be on the
   // compartment, not the realm, because same-compartment realms can have
   // cross-realm pointers without wrappers.
   bool scheduledForDestruction = false;
   bool maybeAlive = true;
 
  protected:
-  js::jit::JitCompartment* jitCompartment_;
+  js::UniquePtr<js::jit::JitCompartment> jitCompartment_;
 
  public:
   bool ensureJitCompartmentExists(JSContext* cx);
-  js::jit::JitCompartment* jitCompartment() { return jitCompartment_; }
+  js::jit::JitCompartment* jitCompartment() { return jitCompartment_.get(); }
 
   // Aggregated output used to collect JSScript hit counts when code coverage
   // is enabled.
   js::coverage::LCovCompartment lcovOutput;
 };
 
 class JS::Realm : public JSCompartment {
   const JS::RealmCreationOptions creationOptions_;
@@ -992,17 +997,16 @@ class JS::Realm : public JSCompartment {
   }
   const void* addressOfMetadataBuilder() const {
     return &allocationMetadataBuilder_;
   }
   void setAllocationMetadataBuilder(
       const js::AllocationMetadataBuilder* builder);
   void forgetAllocationMetadataBuilder();
   void setNewObjectMetadata(JSContext* cx, JS::HandleObject obj);
-  void clearObjectMetadata();
 
   bool hasObjectPendingMetadata() const {
     return objectMetadataState_.is<js::PendingMetadata>();
   }
   void setObjectPendingMetadata(JSContext* cx, JSObject* obj) {
     if (!cx->helperThread()) {
       MOZ_ASSERT(objectMetadataState_.is<js::DelayMetadata>());
       objectMetadataState_ =
