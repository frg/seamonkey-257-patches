# HG changeset patch
# User Jeff Walden <jwalden@mit.edu>
# Date 1525410153 25200
# Node ID a959c06847baa3194ab9d9af0bcbc93a283feaf8
# Parent  fef6e6d7d7a40716b32256cb896dd9a4d0e1c0df
Bug 1459384 - Make FinishToken into a member function, not a lambda.  r=arai

diff --git a/js/src/frontend/TokenStream.cpp b/js/src/frontend/TokenStream.cpp
--- a/js/src/frontend/TokenStream.cpp
+++ b/js/src/frontend/TokenStream.cpp
@@ -1500,57 +1500,61 @@ MOZ_MUST_USE bool TokenStreamSpecific<Ch
   }
 
   noteBadToken.release();
   tp->type = TokenKind::Number;
   tp->setNumber(dval, decimalPoint);
   return true;
 }
 
+template<typename CharT, class AnyCharsAccess>
+void GeneralTokenStreamChars<CharT, AnyCharsAccess>::finishToken(
+    TokenKind* kind, Token* token, TokenStreamShared::Modifier modifier) {
+  anyCharsAccess().flags.isDirtyLine = true;
+
+  token->pos.end = sourceUnits.offset();
+#ifdef DEBUG
+  // Save the modifier used to get this token, so that if an ungetToken()
+  // occurs and then the token is re-gotten (or peeked, etc.), we can assert
+  // that both gets have used the same modifiers.
+  token->modifier = modifier;
+  token->modifierException = TokenStreamShared::NoException;
+#endif
+
+  MOZ_ASSERT(IsTokenSane(token));
+  *kind = token->type;
+}
+
 template <typename CharT, class AnyCharsAccess>
 MOZ_MUST_USE bool TokenStreamSpecific<CharT, AnyCharsAccess>::getTokenInternal(
     TokenKind* const ttp, const Modifier modifier) {
   // Assume we'll fail.  Success cases will overwrite this in |FinishToken|.
   MOZ_MAKE_MEM_UNDEFINED(ttp, sizeof(*ttp));
 
-  auto FinishToken = [this](TokenKind* ttp, Modifier modifier, Token* tp) {
-    this->anyCharsAccess().flags.isDirtyLine = true;
-    tp->pos.end = this->sourceUnits.offset();
-#ifdef DEBUG
-    // Save the modifier used to get this token, so that if an ungetToken()
-    // occurs and then the token is re-gotten (or peeked, etc.), we can assert
-    // that both gets have used the same modifiers.
-    tp->modifier = modifier;
-    tp->modifierException = NoException;
-#endif
-    MOZ_ASSERT(IsTokenSane(tp));
-    *ttp = tp->type;
-  };
-
   // Check if in the middle of a template string. Have to get this out of
   // the way first.
   if (MOZ_UNLIKELY(modifier == TemplateTail)) {
     Token* tp;
     if (!getStringOrTemplateToken('`', &tp)) {
       return false;
     }
 
-    FinishToken(ttp, modifier, tp);
+    finishToken(ttp, tp, modifier);
     return true;
   }
 
   // This loop runs more than once only when whitespace or comments are
   // encountered.
   do {
     if (MOZ_UNLIKELY(!sourceUnits.hasRawChars())) {
       TokenStart start(sourceUnits, 0);
       Token* tp = newToken(start);
       tp->type = TokenKind::Eof;
       anyCharsAccess().flags.isEOF = true;
-      FinishToken(ttp, modifier, tp);
+      finishToken(ttp, tp, modifier);
       return true;
     }
 
     int c = sourceUnits.getCodeUnit();
     MOZ_ASSERT(c != EOF);
 
 
     // Chars not in the range 0..127 are rare.  Getting them out of the way
@@ -1584,30 +1588,30 @@ MOZ_MUST_USE bool TokenStreamSpecific<Ch
                     "IdentifierStart contains '_', but as "
                     "!IsUnicodeIDStart('_'), ensure that '_' is never "
                     "handled here");
       if (unicode::IsUnicodeIDStart(char16_t(c))) {
         if (!identifierName(tp, identStart, IdentifierEscapes::None)) {
           return false;
         }
 
-        FinishToken(ttp, modifier, tp);
+        finishToken(ttp, tp, modifier);
         return true;
       }
 
       uint32_t codePoint = c;
       if (!matchMultiUnitCodePoint(c, &codePoint)) {
         return badToken();
       }
       if (codePoint && unicode::IsUnicodeIDStart(codePoint)) {
         if (!identifierName(tp, identStart, IdentifierEscapes::None)) {
           return false;
         }
 
-        FinishToken(ttp, modifier, tp);
+        finishToken(ttp, tp, modifier);
         return true;
       }
 
       ungetCodePointIgnoreEOL(codePoint);
       error(JSMSG_ILLEGAL_CHARACTER);
       return badToken();
     }
 
@@ -1633,17 +1637,17 @@ MOZ_MUST_USE bool TokenStreamSpecific<Ch
     FirstCharKind c1kind = FirstCharKind(firstCharKinds[c]);
 
     // Look for an unambiguous single-char token.
     //
     if (c1kind <= OneChar_Max) {
       TokenStart start(sourceUnits, -1);
       Token* tp = newToken(start);
       tp->type = TokenKind(c1kind);
-      FinishToken(ttp, modifier, tp);
+      finishToken(ttp, tp, modifier);
       return true;
     }
 
     // Skip over non-EOL whitespace chars.
     //
     if (c1kind == Space)
       continue;
 
@@ -1653,44 +1657,44 @@ MOZ_MUST_USE bool TokenStreamSpecific<Ch
       TokenStart start(sourceUnits, -1);
       Token* tp = newToken(start);
 
       if (!identifierName(tp, sourceUnits.addressOfNextCodeUnit() - 1,
                           IdentifierEscapes::None)) {
         return false;
       }
 
-      FinishToken(ttp, modifier, tp);
+      finishToken(ttp, tp, modifier);
       return true;
     }
 
     // Look for a decimal number.
     //
     if (c1kind == Dec) {
       TokenStart start(sourceUnits, -1);
       Token* tp = newToken(start);
 
       const CharT* numStart = sourceUnits.addressOfNextCodeUnit() - 1;
       if (!decimalNumber(c, tp, numStart)) {
         return false;
       }
 
-      FinishToken(ttp, modifier, tp);
+      finishToken(ttp, tp, modifier);
       return true;
     }
 
     // Look for a string or a template string.
     //
     if (c1kind == String) {
       Token* tp;
       if (!getStringOrTemplateToken(static_cast<char>(c), &tp)) {
         return false;
       }
 
-      FinishToken(ttp, modifier, tp);
+      finishToken(ttp, tp, modifier);
       return true;
     }
 
     // Skip over EOL chars, updating line state along the way.
     //
     if (c1kind == EOL) {
       // If it's a \r\n sequence: consume it and treat itas a single EOL.
       if (c == '\r' && sourceUnits.hasRawChars())
@@ -1776,31 +1780,31 @@ MOZ_MUST_USE bool TokenStreamSpecific<Ch
               return badToken();
             }
 
             // Use the decimal scanner for the rest of the number.
             if (!decimalNumber(c, tp, numStart)) {
               return false;
             }
 
-            FinishToken(ttp, modifier, tp);
+            finishToken(ttp, tp, modifier);
             return true;
           }
 
           c = getCharIgnoreEOL();
         } while (IsAsciiDigit(c));
       } else {
         // '0' not followed by [XxBbOo0-9];  scan as a decimal number.
         numStart = sourceUnits.addressOfNextCodeUnit() - 1;
 
         if (!decimalNumber(c, tp, numStart)) {
           return false;
         }
 
-        FinishToken(ttp, modifier, tp);
+        finishToken(ttp, tp, modifier);
         return true;
       }
       ungetCharIgnoreEOL(c);
 
       if (c != EOF) {
         if (unicode::IsIdentifierStart(char16_t(c))) {
           error(JSMSG_IDSTART_AFTER_NUMBER);
           return badToken();
@@ -1834,17 +1838,17 @@ MOZ_MUST_USE bool TokenStreamSpecific<Ch
       if (!GetPrefixInteger(anyCharsAccess().cx, numStart,
                             sourceUnits.addressOfNextCodeUnit(), radix, &dummy,
                             &dval)) {
         return badToken();
       }
 
       tp->type = TokenKind::Number;
       tp->setNumber(dval, NoDecimal);
-      FinishToken(ttp, modifier, tp);
+      finishToken(ttp, tp, modifier);
       return true;
     }
 
     // This handles everything else.
     //
     MOZ_ASSERT(c1kind == Other);
     TokenStart start(sourceUnits, -1);
     Token* tp = newToken(start);
@@ -1853,60 +1857,60 @@ MOZ_MUST_USE bool TokenStreamSpecific<Ch
         c = getCharIgnoreEOL();
         if (IsAsciiDigit(c)) {
           const CharT* numStart = sourceUnits.addressOfNextCodeUnit() - 2;
 
           if (!decimalNumber('.', tp, numStart)) {
             return false;
           }
 
-          FinishToken(ttp, modifier, tp);
+          finishToken(ttp, tp, modifier);
           return true;
         }
 
         if (c == '.') {
           if (matchChar('.')) {
             tp->type = TokenKind::TripleDot;
-            FinishToken(ttp, modifier, tp);
+            finishToken(ttp, tp, modifier);
             return true;
           }
         }
         ungetCharIgnoreEOL(c);
         tp->type = TokenKind::Dot;
-        FinishToken(ttp, modifier, tp);
+        finishToken(ttp, tp, modifier);
         return true;
 
       case '=':
         if (matchChar('='))
           tp->type = matchChar('=') ? TokenKind::StrictEq : TokenKind::Eq;
         else if (matchChar('>'))
           tp->type = TokenKind::Arrow;
         else
           tp->type = TokenKind::Assign;
-        FinishToken(ttp, modifier, tp);
+        finishToken(ttp, tp, modifier);
         return true;
 
       case '+':
         if (matchChar('+'))
           tp->type = TokenKind::Inc;
         else
           tp->type = matchChar('=') ? TokenKind::AddAssign : TokenKind::Add;
-        FinishToken(ttp, modifier, tp);
+        finishToken(ttp, tp, modifier);
         return true;
 
       case '\\': {
         uint32_t qc;
         if (uint32_t escapeLength = matchUnicodeEscapeIdStart(&qc)) {
           if (!identifierName(tp, sourceUnits.addressOfNextCodeUnit() -
                                   escapeLength - 1,
                               IdentifierEscapes::SawUnicodeEscape)) {
             return false;
           }
 
-          FinishToken(ttp, modifier, tp);
+          finishToken(ttp, tp, modifier);
           return true;
         }
 
         // We could point "into" a mistyped escape, e.g. for "\u{41H}" we
         // could point at the 'H'.  But we don't do that now, so the
         // character after the '\' isn't necessarily bad, so just point at
         // the start of the actually-invalid escape.
         ungetCharIgnoreEOL('\\');
@@ -1918,39 +1922,39 @@ MOZ_MUST_USE bool TokenStreamSpecific<Ch
         if (matchChar('|'))
           tp->type = TokenKind::Or;
 #ifdef ENABLE_PIPELINE_OPERATOR
         else if (matchChar('>'))
           tp->type = TokenKind::Pipeline;
 #endif
         else
           tp->type = matchChar('=') ? TokenKind::BitOrAssign : TokenKind::BitOr;
-        FinishToken(ttp, modifier, tp);
+        finishToken(ttp, tp, modifier);
         return true;
 
       case '^':
         tp->type = matchChar('=') ? TokenKind::BitXorAssign : TokenKind::BitXor;
-        FinishToken(ttp, modifier, tp);
+        finishToken(ttp, tp, modifier);
         return true;
 
       case '&':
         if (matchChar('&'))
           tp->type = TokenKind::And;
         else
           tp->type = matchChar('=') ? TokenKind::BitAndAssign
                                     : TokenKind::BitAnd;
-        FinishToken(ttp, modifier, tp);
+        finishToken(ttp, tp, modifier);
         return true;
 
       case '!':
         if (matchChar('='))
           tp->type = matchChar('=') ? TokenKind::StrictNe : TokenKind::Ne;
         else
           tp->type = TokenKind::Not;
-        FinishToken(ttp, modifier, tp);
+        finishToken(ttp, tp, modifier);
         return true;
 
       case '<':
         if (anyCharsAccess().options().allowHTMLComments) {
           // Treat HTML begin-comment as comment-till-end-of-line.
           if (matchChar('!')) {
             if (matchChar('-')) {
               if (matchChar('-')) {
@@ -1962,37 +1966,37 @@ MOZ_MUST_USE bool TokenStreamSpecific<Ch
             ungetCharIgnoreEOL('!');
           }
         }
         if (matchChar('<')) {
           tp->type = matchChar('=') ? TokenKind::LshAssign : TokenKind::Lsh;
         } else {
           tp->type = matchChar('=') ? TokenKind::Le : TokenKind::Lt;
         }
-        FinishToken(ttp, modifier, tp);
+        finishToken(ttp, tp, modifier);
         return true;
 
       case '>':
         if (matchChar('>')) {
           if (matchChar('>'))
             tp->type = matchChar('=') ? TokenKind::UrshAssign : TokenKind::Ursh;
           else
             tp->type = matchChar('=') ? TokenKind::RshAssign : TokenKind::Rsh;
         } else {
           tp->type = matchChar('=') ? TokenKind::Ge : TokenKind::Gt;
         }
-        FinishToken(ttp, modifier, tp);
+        finishToken(ttp, tp, modifier);
         return true;
 
       case '*':
         if (matchChar('*'))
           tp->type = matchChar('=') ? TokenKind::PowAssign : TokenKind::Pow;
         else
           tp->type = matchChar('=') ? TokenKind::MulAssign : TokenKind::Mul;
-        FinishToken(ttp, modifier, tp);
+        finishToken(ttp, tp, modifier);
         return true;
 
       case '/':
         // Look for a single-line comment.
         if (matchChar('/')) {
           c = getCharIgnoreEOL();
           if (c == '@' || c == '#') {
             bool shouldWarn = c == '@';
@@ -2103,44 +2107,44 @@ MOZ_MUST_USE bool TokenStreamSpecific<Ch
             }
 
             reflags = RegExpFlag(reflags | flag);
           }
           ungetCharIgnoreEOL(c);
 
           tp->type = TokenKind::RegExp;
           tp->setRegExpFlags(reflags);
-          FinishToken(ttp, modifier, tp);
+          finishToken(ttp, tp, modifier);
           return true;
         }
 
         tp->type = matchChar('=') ? TokenKind::DivAssign : TokenKind::Div;
-        FinishToken(ttp, modifier, tp);
+        finishToken(ttp, tp, modifier);
         return true;
 
       case '%':
         tp->type = matchChar('=') ? TokenKind::ModAssign : TokenKind::Mod;
-        FinishToken(ttp, modifier, tp);
+        finishToken(ttp, tp, modifier);
         return true;
 
       case '-':
         if (matchChar('-')) {
           if (anyCharsAccess().options().allowHTMLComments &&
               !anyCharsAccess().flags.isDirtyLine) {
             if (matchChar('>')) {
               consumeRestOfSingleLineComment();
               continue;
             }
           }
 
           tp->type = TokenKind::Dec;
         } else {
           tp->type = matchChar('=') ? TokenKind::SubAssign : TokenKind::Sub;
         }
-        FinishToken(ttp, modifier, tp);
+        finishToken(ttp, tp, modifier);
         return true;
 
       default:
         // We consumed a bad character/code point.  Put it back so the
         // error location is the bad character.
         ungetCodePointIgnoreEOL(c);
         error(JSMSG_ILLEGAL_CHARACTER);
         return badToken();
diff --git a/js/src/frontend/TokenStream.h b/js/src/frontend/TokenStream.h
--- a/js/src/frontend/TokenStream.h
+++ b/js/src/frontend/TokenStream.h
@@ -1054,16 +1054,19 @@ class GeneralTokenStreamChars : public T
   /**
    * Allocates a new Token starting at the current offset from the circular
    * buffer of Tokens in |anyCharsAccess()|, that begins at |start.offset()|.
    */
   Token* newToken(TokenStart start);
 
   MOZ_COLD bool badToken();
 
+  void finishToken(TokenKind* kind, Token* token,
+                   TokenStreamShared::Modifier modifier);
+
   int32_t getCharIgnoreEOL();
 
   void ungetChar(int32_t c);
 
   /**
    * Consume characters til EOL/EOF following the start of a single-line
    * comment, without consuming the EOL/EOF.
    *
@@ -1206,16 +1209,17 @@ class MOZ_STACK_CLASS TokenStreamSpecifi
   using CharsSharedBase::appendCodePointToTokenbuf;
   using CharsSharedBase::atomizeChars;
   using CharsSharedBase::copyTokenbufTo;
   using CharsSharedBase::sourceUnits;
   using CharsSharedBase::tokenbuf;
   using CharsSharedBase::ungetCharIgnoreEOL;
   using GeneralCharsBase::badToken;
   using GeneralCharsBase::consumeRestOfSingleLineComment;
+  using GeneralCharsBase::finishToken;
   using GeneralCharsBase::getCharIgnoreEOL;
   using GeneralCharsBase::newToken;
   using GeneralCharsBase::ungetChar;
 
   template<typename CharU>
   friend class TokenStreamPosition;
 
  public:
