# HG changeset patch
# User Thomas Daede <tdaede@mozilla.com>
# Date 1537392397 0
# Node ID 8a262cd03dfdb1cea13ac3472ac84de66708b952
# Parent  9d72fdbae57a36a5d3e4aac0225680382863ebbb
Bug 1489285: Update libaom vendor scripts. r=dminor

Differential Revision: https://phabricator.services.mozilla.com/D5216

diff --git a/media/libaom/cmakeparser.py b/media/libaom/cmakeparser.py
--- a/media/libaom/cmakeparser.py
+++ b/media/libaom/cmakeparser.py
@@ -156,16 +156,31 @@ def evaluate(variables, cache_variables,
             try:
                 cache = values.index('CACHE')
                 values = values[0:cache]
                 if not variables.has_key(variable):
                     variables[variable] = ' '.join(values)
                 cache_variables.append(variable)
             except ValueError:
                 variables[variable] = ' '.join(values)
+        # we need to emulate the behavior of these function calls
+        # because we don't support interpreting them directly
+        # see bug 1492292
+        elif command in ['set_aom_config_var', 'set_aom_detect_var']:
+            variable = arguments[0]
+            value = arguments[1]
+            if variable not in variables:
+                variables[variable] = value
+            cache_variables.append(variable)
+        elif command == 'set_aom_option_var':
+            # option vars cannot go into cache_variables
+            variable = arguments[0]
+            value = arguments[2]
+            if variable not in variables:
+                variables[variable] = value
         elif command == 'add_asm_library':
             try:
                 sources.extend(variables[arguments[1]].split(' '))
             except (IndexError, KeyError):
                 pass
         elif command == 'add_intrinsics_object_library':
             try:
                 sources.extend(variables[arguments[3]].split(' '))
diff --git a/media/libaom/generate_sources_mozbuild.py b/media/libaom/generate_sources_mozbuild.py
--- a/media/libaom/generate_sources_mozbuild.py
+++ b/media/libaom/generate_sources_mozbuild.py
@@ -57,41 +57,32 @@ def write_aom_config(system, arch, varia
 
 if __name__ == '__main__':
     import sys
 
     shared_variables = {
         'CMAKE_CURRENT_SOURCE_DIR': AOM_DIR,
         'CONFIG_AV1_DECODER': 1,
         'CONFIG_AV1_ENCODER': 0,
-        'CONFIG_BGSPRITE': 0,
-        'CONFIG_CDEF_SINGLEPASS': 0,
-        'CONFIG_CFL': 0,
-        'CONFIG_HASH_ME': 0,
-        'CONFIG_HIGH_BITDEPTH': 0,
+        'CONFIG_COLLECT_INTER_MODE_RD_STATS': 0,
         'CONFIG_INSPECTION': 0,
         'CONFIG_INTERNAL_STATS': 0,
         'CONFIG_LIBYUV': 0,
-        'CONFIG_LOWBITDEPTH': 0,
-        'CONFIG_LV_MAP': 0,
-        'CONFIG_MOTION_VAR': 0,
+        'CONFIG_LOWBITDEPTH': 1,
         'CONFIG_MULTITHREAD': 1,
-        'CONFIG_NCOBMC_ADAPT_WEIGHT': 0,
         'CONFIG_PIC': 0,
-        'CONFIG_PVQ': 0,
-        'CONFIG_UNIT_TESTS': 0,
         'CONFIG_WEBM_IO': 0,
-        'CONFIG_XIPHRC': 0,
         'CMAKE_CURRENT_BINARY_DIR': 'OBJDIR',
         'CMAKE_INSTALL_PREFIX': 'INSTALLDIR',
         'CMAKE_SYSTEM_NAME': 'Linux',
         'CMAKE_SYSTEM_PROCESSOR': 'x86_64',
         'ENABLE_EXAMPLES': 0,
         'ENABLE_TESTS': 0,
         'ENABLE_TOOLS': 0,
+        'ENABLE_DOCS': 0,
         'AOM_TEST_TEST_CMAKE_': 1, #prevent building tests
     }
 
     f = open('sources.mozbuild', 'wb')
     f.write('# This file is generated. Do not edit.\n\n')
     f.write('files = {\n')
 
     platforms = [
@@ -110,28 +101,28 @@ if __name__ == '__main__':
         variables = shared_variables.copy()
         variables['AOM_TARGET_CPU'] = cpu
 
         # We skip compiling test programs that detect these
         variables['HAVE_FEXCEPT'] = 1
         variables['INLINE'] = 'inline'
         if cpu == 'x86' and system == 'linux':
             variables['CONFIG_PIC'] = 1
+        if cpu == 'armv7':
+            variables['CONFIG_PIC'] = 1
         if system == 'win' and not arch.startswith('mingw'):
             variables['MSVC'] = 1
 
         cache_variables = []
         sources = cp.parse(variables, cache_variables,
                            os.path.join(AOM_DIR, 'CMakeLists.txt'))
 
         # Disable HAVE_UNISTD_H.
         cache_variables.remove('HAVE_UNISTD_H')
-
         write_aom_config(system, arch, variables, cache_variables)
-
         # Currently, the sources are the same for each supported cpu
         # regardless of operating system / compiler. If that changes, we'll
         # have to generate sources for each combination.
         if generate_sources:
             # Remove spurious sources and perl files
             sources = filter(lambda x: x.startswith(AOM_DIR), sources)
             sources = filter(lambda x: not x.endswith('.pl'), sources)
 
diff --git a/media/libaom/generate_sources_mozbuild.sh b/media/libaom/generate_sources_mozbuild.sh
--- a/media/libaom/generate_sources_mozbuild.sh
+++ b/media/libaom/generate_sources_mozbuild.sh
@@ -29,31 +29,31 @@ function write_license {
 # $1 - Header file directory.
 # $2 - Architecture.
 # $3 - Optional - any additional arguments to pass through.
 function gen_rtcd_header {
   echo "Generate $LIBAOM_CONFIG_DIR/$1/*_rtcd.h files."
 
   AOM_CONFIG=$BASE_DIR/$LIBAOM_CONFIG_DIR/$1/config/aom_config.h
 
-  $BASE_DIR/$LIBAOM_SRC_DIR/build/make/rtcd.pl \
+  $BASE_DIR/$LIBAOM_SRC_DIR/build/cmake/rtcd.pl \
     --arch=$2 \
-    --sym=aom_rtcd $3 \
+    --sym=av1_rtcd $3 \
     --config=$AOM_CONFIG \
     $BASE_DIR/$LIBAOM_SRC_DIR/av1/common/av1_rtcd_defs.pl \
     > $BASE_DIR/$LIBAOM_CONFIG_DIR/$1/config/av1_rtcd.h
 
-  $BASE_DIR/$LIBAOM_SRC_DIR/build/make/rtcd.pl \
+  $BASE_DIR/$LIBAOM_SRC_DIR/build/cmake/rtcd.pl \
     --arch=$2 \
     --sym=aom_scale_rtcd $3 \
     --config=$AOM_CONFIG \
     $BASE_DIR/$LIBAOM_SRC_DIR/aom_scale/aom_scale_rtcd.pl \
     > $BASE_DIR/$LIBAOM_CONFIG_DIR/$1/config/aom_scale_rtcd.h
 
-  $BASE_DIR/$LIBAOM_SRC_DIR/build/make/rtcd.pl \
+  $BASE_DIR/$LIBAOM_SRC_DIR/build/cmake/rtcd.pl \
     --arch=$2 \
     --sym=aom_dsp_rtcd $3 \
     --config=$AOM_CONFIG \
     $BASE_DIR/$LIBAOM_SRC_DIR/aom_dsp/aom_dsp_rtcd_defs.pl \
     > $BASE_DIR/$LIBAOM_CONFIG_DIR/$1/config/aom_dsp_rtcd.h
 }
 
 echo "Generating config files."

