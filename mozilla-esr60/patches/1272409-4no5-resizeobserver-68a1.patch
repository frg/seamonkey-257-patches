# HG changeset patch
# User Fariskhi Vidyan <farislab@gmail.com>
# Date 1556310558 0
# Node ID 87c7a189cbe0d917f6579f796e43a8e5c29e7681
# Parent  8fe8b854616d769788b1ea4f51f96266defa98f3
Bug 1272409 - Part 4: Integrate ResizeObserver with Document and reflow. r=dholbert,smaug

Depends on D27617

Differential Revision: https://phabricator.services.mozilla.com/D27618

diff --git a/dom/base/ResizeObserver.cpp b/dom/base/ResizeObserver.cpp
--- a/dom/base/ResizeObserver.cpp
+++ b/dom/base/ResizeObserver.cpp
@@ -75,17 +75,17 @@ already_AddRefed<ResizeObserver> ResizeO
   nsIDocument* doc = window->GetExtantDoc();
 
   if (!doc) {
     aRv.Throw(NS_ERROR_FAILURE);
     return nullptr;
   }
 
   RefPtr<ResizeObserver> observer = new ResizeObserver(window.forget(), aCb);
-  // TODO: Add the new ResizeObserver to document here in the later patch.
+  doc->AddResizeObserver(observer);
 
   return observer.forget();
 }
 
 void ResizeObserver::Observe(Element& aTarget, ErrorResult& aRv) {
   RefPtr<ResizeObservation> observation;
 
   if (mObservationMap.Get(&aTarget, getter_AddRefs(observation))) {
@@ -101,17 +101,17 @@ void ResizeObserver::Observe(Element& aT
   observation = new ResizeObservation(aTarget);
 
   mObservationMap.Put(&aTarget, observation);
   mObservationList.insertBack(observation);
 
   // Per the spec, we need to trigger notification in event loop that
   // contains ResizeObserver observe call even when resize/reflow does
   // not happen.
-  // TODO: Implement the notification scheduling in the later patch.
+  aTarget.OwnerDoc()->ScheduleResizeObserversNotification();
 }
 
 void ResizeObserver::Unobserve(Element& aTarget, ErrorResult& aRv) {
   RefPtr<ResizeObservation> observation;
   if (!mObservationMap.Remove(&aTarget, getter_AddRefs(observation))) {
     return;
   }
 
diff --git a/dom/base/nsDocument.cpp b/dom/base/nsDocument.cpp
--- a/dom/base/nsDocument.cpp
+++ b/dom/base/nsDocument.cpp
@@ -70,16 +70,18 @@
 #include "nsIFrame.h"
 #include "nsITabChild.h"
 
 #include "nsRange.h"
 #include "mozilla/dom/DocumentType.h"
 #include "mozilla/dom/NodeIterator.h"
 #include "mozilla/dom/Promise.h"
 #include "mozilla/dom/PromiseNativeHandler.h"
+#include "mozilla/dom/ResizeObserver.h"
+#include "mozilla/dom/ResizeObserverController.h"
 #include "mozilla/dom/TreeWalker.h"
 
 #include "nsIServiceManager.h"
 #include "mozilla/dom/ServiceWorkerManager.h"
 #include "imgLoader.h"
 
 #include "nsCanvasFrame.h"
 #include "nsContentCID.h"
@@ -1785,16 +1787,21 @@ NS_IMPL_CYCLE_COLLECTION_TRAVERSE_BEGIN_
   // this reference is managed by their AddListener and RemoveListener
   // methods.
   for (auto mql : tmp->mDOMMediaQueryLists) {
     if (mql->HasListeners()) {
       NS_CYCLE_COLLECTION_NOTE_EDGE_NAME(cb, "mDOMMediaQueryLists item");
       cb.NoteXPCOMChild(mql);
     }
   }
+
+  if (tmp->mResizeObserverController) {
+    tmp->mResizeObserverController->Traverse(cb);
+  }
+
 NS_IMPL_CYCLE_COLLECTION_TRAVERSE_END
 
 NS_IMPL_CYCLE_COLLECTION_CLASS(nsDocument)
 
 NS_IMPL_CYCLE_COLLECTION_TRACE_WRAPPERCACHE(nsDocument)
 
 NS_IMPL_CYCLE_COLLECTION_UNLINK_BEGIN(nsDocument)
   tmp->mInUnlinkOrDeletion = true;
@@ -1898,16 +1905,21 @@ NS_IMPL_CYCLE_COLLECTION_UNLINK_BEGIN(ns
   // methods.
   for (MediaQueryList* mql = tmp->mDOMMediaQueryLists.getFirst(); mql;) {
     MediaQueryList* next = mql->getNext();
     mql->Disconnect();
     mql = next;
   }
 
   tmp->mInUnlinkOrDeletion = false;
+
+  if (tmp->mResizeObserverController) {
+    tmp->mResizeObserverController->Unlink();
+  }
+
 NS_IMPL_CYCLE_COLLECTION_UNLINK_END
 
 nsresult nsDocument::Init() {
   if (mCSSLoader || mStyleImageLoader || mNodeInfoManager || mScriptLoader) {
     return NS_ERROR_ALREADY_INITIALIZED;
   }
 
   // Force initialization.
@@ -11787,16 +11799,32 @@ FlashClassification nsIDocument::Documen
     MOZ_ASSERT(
         result != FlashClassification::Unclassified,
         "nsDocument::GetPluginClassification should never return Unclassified");
   }
 
   return mFlashClassification;
 }
 
+void nsIDocument::AddResizeObserver(ResizeObserver* aResizeObserver) {
+  if (!mResizeObserverController) {
+    mResizeObserverController = MakeUnique<ResizeObserverController>(this);
+  }
+
+  mResizeObserverController->AddResizeObserver(aResizeObserver);
+}
+
+void nsIDocument::ScheduleResizeObserversNotification() const {
+  if (!mResizeObserverController) {
+    return;
+  }
+
+  mResizeObserverController->ScheduleNotification();
+}
+
 /**
  * Initializes |mIsThirdParty| if necessary and returns its value. The value
  * returned represents whether this document should be considered Third-Party.
  *
  * A top-level document cannot be a considered Third-Party; only subdocuments
  * may. For a subdocument to be considered Third-Party, it must meet ANY ONE
  * of the following requirements:
  *  - The document's parent is Third-Party
diff --git a/dom/base/nsIDocument.h b/dom/base/nsIDocument.h
--- a/dom/base/nsIDocument.h
+++ b/dom/base/nsIDocument.h
@@ -171,16 +171,18 @@ class Link;
 class Location;
 class MediaQueryList;
 class GlobalObject;
 class NodeFilter;
 class NodeIterator;
 enum class OrientationType : uint8_t;
 class ProcessingInstruction;
 class Promise;
+class ResizeObserver;
+class ResizeObserverController;
 class ScriptLoader;
 class Selection;
 class ServiceWorkerDescriptor;
 class StyleSheetList;
 class SVGDocument;
 class SVGSVGElement;
 class Touch;
 class TouchList;
@@ -3144,16 +3146,21 @@ class nsIDocument : public nsINode,
   // JS::DescribeScriptedCaller() returns, since these APIs are used to
   // determine whether some code is being called from a tracking script.
   void NoteScriptTrackingStatus(const nsACString& aURL, bool isTracking);
   bool IsScriptTracking(const nsACString& aURL) const;
 
   // For more information on Flash classification, see
   // toolkit/components/url-classifier/flash-block-lists.rst
   mozilla::dom::FlashClassification DocumentFlashClassification();
+
+  // ResizeObserver usage.
+  void AddResizeObserver(mozilla::dom::ResizeObserver* aResizeObserver);
+  void ScheduleResizeObserversNotification() const;
+
   bool IsThirdParty();
 
   bool IsScopedStyleEnabled();
 
   nsINode* GetServoRestyleRoot() const { return mServoRestyleRoot; }
 
   uint32_t GetServoRestyleRootDirtyBits() const {
     MOZ_ASSERT(mServoRestyleRoot);
@@ -3464,16 +3471,19 @@ class nsIDocument : public nsINode,
   mozilla::TimeStamp mLastFocusTime;
 
   mozilla::EventStates mDocumentState;
 
   RefPtr<mozilla::dom::Promise> mReadyForIdle;
 
   RefPtr<mozilla::dom::AboutCapabilities> mAboutCapabilities;
 
+  mozilla::UniquePtr<mozilla::dom::ResizeObserverController>
+      mResizeObserverController;
+
   // True if BIDI is enabled.
   bool mBidiEnabled : 1;
   // True if a MathML element has ever been owned by this document.
   bool mMathMLEnabled : 1;
 
   // True if this document is the initial document for a window.  This should
   // basically be true only for documents that exist in newly-opened windows or
   // documents created to satisfy a GetDocument() on a window when there's no
diff --git a/layout/base/PresShell.cpp b/layout/base/PresShell.cpp
--- a/layout/base/PresShell.cpp
+++ b/layout/base/PresShell.cpp
@@ -7987,16 +7987,20 @@ void PresShell::DidDoReflow(bool aInterr
   HandlePostedReflowCallbacks(aInterruptible);
 
   nsCOMPtr<nsIDocShell> docShell = mPresContext->GetDocShell();
   if (docShell) {
     DOMHighResTimeStamp now = GetPerformanceNowUnclamped();
     docShell->NotifyReflowObservers(aInterruptible, mLastReflowStart, now);
   }
 
+  if (!mPresContext->HasPendingInterrupt()) {
+    mDocument->ScheduleResizeObserversNotification();
+  }
+
   if (sSynthMouseMove) {
     SynthesizeMouseMove(false);
   }
 
   mPresContext->NotifyMissingFonts();
 }
 
 DOMHighResTimeStamp PresShell::GetPerformanceNowUnclamped() {
