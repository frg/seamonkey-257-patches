# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1527097543 -7200
# Node ID def02132a4cff5e03c4a95a3b3b8b1bf2d71a615
# Parent  333358a3abe7ed738e27e5d9f46bd0e47dd766cc
Bug 1461938 part 30 - Move globalWriteBarriered to JS::Realm. r=jonco

diff --git a/js/src/jit/CodeGenerator.cpp b/js/src/jit/CodeGenerator.cpp
--- a/js/src/jit/CodeGenerator.cpp
+++ b/js/src/jit/CodeGenerator.cpp
@@ -3878,24 +3878,30 @@ void CodeGenerator::visitOutOfLineCallPo
 
   masm.jump(ool->rejoin());
 }
 
 void CodeGenerator::maybeEmitGlobalBarrierCheck(const LAllocation* maybeGlobal,
                                                 OutOfLineCode* ool) {
   // Check whether an object is a global that we have already barriered before
   // calling into the VM.
+  //
+  // We only check for the script's global, not other globals within the same
+  // compartment, because we bake in a pointer to realm->globalWriteBarriered
+  // and doing that would be invalid for other realms because they could be
+  // collected before the Ion code is discarded.
 
   if (!maybeGlobal->isConstant()) return;
 
   JSObject* obj = &maybeGlobal->toConstant()->toObject();
-  if (!isGlobalObject(obj)) return;
-
-  JSCompartment* comp = obj->compartment();
-  auto addr = AbsoluteAddress(&comp->globalWriteBarriered);
+  if (gen->compartment->maybeGlobal() != obj)
+    return;
+
+  auto addr =
+      AbsoluteAddress(gen->compartment->addressOfGlobalWriteBarriered());
   masm.branch32(Assembler::NotEqual, addr, Imm32(0), ool->rejoin());
 }
 
 template <class LPostBarrierType, MIRType nurseryType>
 void CodeGenerator::visitPostWriteBarrierCommon(LPostBarrierType* lir,
                                                 OutOfLineCode* ool) {
   addOutOfLineCode(ool, lir->mir());
 
diff --git a/js/src/jit/CompileWrappers.cpp b/js/src/jit/CompileWrappers.cpp
--- a/js/src/jit/CompileWrappers.cpp
+++ b/js/src/jit/CompileWrappers.cpp
@@ -176,16 +176,20 @@ const JitCompartment* CompileCompartment
 const GlobalObject* CompileCompartment::maybeGlobal() {
   // This uses unsafeUnbarrieredMaybeGlobal() so as not to trigger the read
   // barrier on the global from off thread.  This is safe because we
   // abort Ion compilation when we GC.
   return JS::GetRealmForCompartment(compartment())->
              unsafeUnbarrieredMaybeGlobal();
 }
 
+const uint32_t* CompileCompartment::addressOfGlobalWriteBarriered() {
+  return &JS::GetRealmForCompartment(compartment())->globalWriteBarriered;
+}
+
 bool CompileCompartment::hasAllocationMetadataBuilder() {
   return JS::GetRealmForCompartment(compartment())->
              hasAllocationMetadataBuilder();
 }
 
 // Note: This function is thread-safe because setSingletonAsValue sets a boolean
 // variable to false, and this boolean variable has no way to be resetted to
 // true. So even if there is a concurrent write, this concurrent write will
diff --git a/js/src/jit/CompileWrappers.h b/js/src/jit/CompileWrappers.h
--- a/js/src/jit/CompileWrappers.h
+++ b/js/src/jit/CompileWrappers.h
@@ -96,16 +96,17 @@ class CompileCompartment {
   CompileZone* zone();
   CompileRuntime* runtime();
 
   const void* addressOfRandomNumberGenerator();
 
   const JitCompartment* jitCompartment();
 
   const GlobalObject* maybeGlobal();
+  const uint32_t* addressOfGlobalWriteBarriered();
 
   bool hasAllocationMetadataBuilder();
 
   // Mirror RealmOptions.
   void setSingletonsAsValues();
 };
 
 class JitCompileOptions {
diff --git a/js/src/jit/VMFunctions.cpp b/js/src/jit/VMFunctions.cpp
--- a/js/src/jit/VMFunctions.cpp
+++ b/js/src/jit/VMFunctions.cpp
@@ -658,19 +658,19 @@ template void PostWriteElementBarrier<In
                                                           int32_t index);
 
 template void PostWriteElementBarrier<IndexInBounds::Maybe>(JSRuntime* rt,
                                                             JSObject* obj,
                                                             int32_t index);
 
 void PostGlobalWriteBarrier(JSRuntime* rt, JSObject* obj) {
   MOZ_ASSERT(obj->is<GlobalObject>());
-  if (!obj->compartment()->globalWriteBarriered) {
+  if (!obj->realm()->globalWriteBarriered) {
     PostWriteBarrier(rt, obj);
-    obj->compartment()->globalWriteBarriered = 1;
+    obj->realm()->globalWriteBarriered = 1;
   }
 }
 
 int32_t GetIndexFromString(JSString* str) {
   // We shouldn't GC here as this is called directly from IC code.
   AutoUnsafeCallWithABI unsafe;
 
   if (!str->isFlat()) return -1;
diff --git a/js/src/vm/JSCompartment.cpp b/js/src/vm/JSCompartment.cpp
--- a/js/src/vm/JSCompartment.cpp
+++ b/js/src/vm/JSCompartment.cpp
@@ -42,17 +42,16 @@ using namespace js::jit;
 
 using mozilla::PodArrayZero;
 
 JSCompartment::JSCompartment(Zone* zone)
     : zone_(zone),
       runtime_(zone->runtimeFromAnyThread()),
       data(nullptr),
       regExps(),
-      globalWriteBarriered(0),
       detachedTypedObjects(0),
       innerViews(zone),
       gcIncomingGrayPointers(nullptr),
       enumerators(nullptr) {
   runtime_->numCompartments++;
 }
 
 Realm::Realm(JS::Zone* zone, const JS::RealmOptions& options)
@@ -646,24 +645,23 @@ void Realm::finishRoots() {
   clearScriptCounts();
   clearScriptNames();
 
   if (nonSyntacticLexicalEnvironments_)
     nonSyntacticLexicalEnvironments_->clear();
 }
 
 void JSCompartment::sweepAfterMinorGC(JSTracer* trc) {
-  globalWriteBarriered = 0;
-
   InnerViewTable& table = innerViews.get();
   if (table.needsSweepAfterMinorGC()) table.sweepAfterMinorGC();
 
   crossCompartmentWrappers.sweepAfterMinorGC(trc);
 
   Realm* realm = JS::GetRealmForCompartment(this);
+  realm->globalWriteBarriered = 0;
   realm->dtoaCache.purge();
 }
 
 void JSCompartment::sweepSavedStacks() { savedStacks_.sweep(); }
 
 void Realm::sweepGlobalObject() {
   if (global_ && IsAboutToBeFinalized(&global_)) global_.set(nullptr);
 }
diff --git a/js/src/vm/JSCompartment.h b/js/src/vm/JSCompartment.h
--- a/js/src/vm/JSCompartment.h
+++ b/js/src/vm/JSCompartment.h
@@ -588,25 +588,16 @@ struct JSCompartment {
  public:
   js::RegExpCompartment regExps;
 
   using IteratorCache =
       js::HashSet<js::PropertyIteratorObject*, js::IteratorHashPolicy,
                   js::SystemAllocPolicy>;
   IteratorCache iteratorCache;
 
-  /*
-   * For generational GC, record whether a write barrier has added this
-   * compartment's global to the store buffer since the last minor GC.
-   *
-   * This is used to avoid calling into the VM every time a nursery object is
-   * written to a property of the global.
-   */
-  uint32_t globalWriteBarriered;
-
   // Non-zero if the storage underlying any typed object in this compartment
   // might be detached.
   int32_t detachedTypedObjects;
 
   // Recompute the probability with which this compartment should record
   // profiling data (stack traces, allocations log, etc.) about each
   // allocation. We consult the probabilities requested by the Debugger
   // instances observing us, if any.
@@ -872,16 +863,25 @@ class JS::Realm : public JSCompartment {
    * Lazily initialized script source object to use for scripts cloned
    * from the self-hosting global.
    */
   js::ReadBarrieredScriptSourceObject selfHostingScriptSource { nullptr };
 
   // Last time at which an animation was played for this realm.
   int64_t lastAnimationTime = 0;
 
+  /*
+   * For generational GC, record whether a write barrier has added this
+   * realm's global to the store buffer since the last minor GC.
+   *
+   * This is used to avoid calling into the VM every time a nursery object is
+   * written to a property of the global.
+   */
+  uint32_t globalWriteBarriered = 0;
+
   uint32_t warnedAboutStringGenericsMethods = 0;
 #ifdef DEBUG
   bool firedOnNewGlobalObject = false;
 #endif
 
  private:
   void updateDebuggerObservesFlag(unsigned flag);
 
