# HG changeset patch
# User Jeff Walden <jwalden@mit.edu>
# Date 1528363017 25200
# Node ID b2b3d152d7eada7a400326875331b2d454564145
# Parent  3ddb1b2965bd5e591492f35df52b09a71b133fc1
Bug 1467336 - Rename TokenStreamSpecific::matchChar to TokenStreamCharsBase::matchCodeUnit to clarify its unit-matching nature.  r=arai

diff --git a/js/src/frontend/TokenStream.cpp b/js/src/frontend/TokenStream.cpp
--- a/js/src/frontend/TokenStream.cpp
+++ b/js/src/frontend/TokenStream.cpp
@@ -1084,17 +1084,17 @@ MOZ_MUST_USE bool TokenStreamSpecific<Ch
 
     if (c == EOF || unicode::IsSpaceOrBOM2(c)) break;
 
     consumeKnownChar(c);
 
     // Debugging directives can occur in both single- and multi-line
     // comments. If we're currently inside a multi-line comment, we also
     // need to recognize multi-line comment terminators.
-    if (isMultiline && c == '*' && matchChar('/')) {
+    if (isMultiline && c == '*' && matchCodeUnit('/')) {
       ungetCodeUnit('/');
       ungetCodeUnit('*');
       break;
     }
 
     if (!tokenbuf.append(c)) return false;
   } while (true);
 
@@ -1800,40 +1800,41 @@ MOZ_MUST_USE bool TokenStreamSpecific<Ch
         c = getCodeUnit();
         if (IsAsciiDigit(c)) {
           return decimalNumber('.', start,
                                sourceUnits.addressOfNextCodeUnit() - 2,
                                modifier, ttp);
         }
 
         if (c == '.') {
-          if (matchChar('.')) {
+          if (matchCodeUnit('.')) {
             simpleKind = TokenKind::TripleDot;
             break;
           }
         }
         ungetCodeUnit(c);
 
         simpleKind = TokenKind::Dot;
         break;
 
       case '=':
-        if (matchChar('='))
-          simpleKind = matchChar('=') ? TokenKind::StrictEq : TokenKind::Eq;
-        else if (matchChar('>'))
+        if (matchCodeUnit('='))
+          simpleKind = matchCodeUnit('=') ? TokenKind::StrictEq : TokenKind::Eq;
+        else if (matchCodeUnit('>'))
           simpleKind = TokenKind::Arrow;
         else
           simpleKind = TokenKind::Assign;
         break;
 
       case '+':
-        if (matchChar('+'))
+        if (matchCodeUnit('+'))
           simpleKind = TokenKind::Inc;
         else
-          simpleKind = matchChar('=') ? TokenKind::AddAssign : TokenKind::Add;
+          simpleKind = matchCodeUnit('=') ? TokenKind::AddAssign
+                                          : TokenKind::Add;
         break;
 
       case '\\': {
         uint32_t qc;
         if (uint32_t escapeLength = matchUnicodeEscapeIdStart(&qc)) {
           return identifierName(start,
                                 sourceUnits.addressOfNextCodeUnit() -
                                     escapeLength - 1,
@@ -1846,119 +1847,123 @@ MOZ_MUST_USE bool TokenStreamSpecific<Ch
         // character after the '\' isn't necessarily bad, so just point at
         // the start of the actually-invalid escape.
         ungetCodeUnit('\\');
         error(JSMSG_BAD_ESCAPE);
         return badToken();
       }
 
       case '|':
-        if (matchChar('|'))
+        if (matchCodeUnit('|'))
           simpleKind = TokenKind::Or;
 #ifdef ENABLE_PIPELINE_OPERATOR
-        else if (matchChar('>'))
+        else if (matchCodeUnit('>'))
           simpleKind = TokenKind::Pipeline;
 #endif
         else
-          simpleKind = matchChar('=') ? TokenKind::BitOrAssign
-                                      : TokenKind::BitOr;
+          simpleKind = matchCodeUnit('=') ? TokenKind::BitOrAssign
+                                          : TokenKind::BitOr;
         break;
 
       case '^':
-        simpleKind = matchChar('=') ? TokenKind::BitXorAssign
-                                    : TokenKind::BitXor;
+        simpleKind = matchCodeUnit('=') ? TokenKind::BitXorAssign
+                                        : TokenKind::BitXor;
         break;
 
       case '&':
-        if (matchChar('&'))
+        if (matchCodeUnit('&'))
           simpleKind = TokenKind::And;
         else
-          simpleKind = matchChar('=') ? TokenKind::BitAndAssign
-                                      : TokenKind::BitAnd;
+          simpleKind = matchCodeUnit('=') ? TokenKind::BitAndAssign
+                                          : TokenKind::BitAnd;
         break;
 
       case '!':
-        if (matchChar('='))
-          simpleKind = matchChar('=') ? TokenKind::StrictNe : TokenKind::Ne;
+        if (matchCodeUnit('='))
+          simpleKind = matchCodeUnit('=') ? TokenKind::StrictNe : TokenKind::Ne;
         else
           simpleKind = TokenKind::Not;
         break;
 
       case '<':
         if (anyCharsAccess().options().allowHTMLComments) {
           // Treat HTML begin-comment as comment-till-end-of-line.
-          if (matchChar('!')) {
-            if (matchChar('-')) {
-              if (matchChar('-')) {
+          if (matchCodeUnit('!')) {
+            if (matchCodeUnit('-')) {
+              if (matchCodeUnit('-')) {
                 consumeRestOfSingleLineComment();
                 continue;
               }
               ungetCodeUnit('-');
             }
             ungetCodeUnit('!');
           }
         }
-        if (matchChar('<')) {
-          simpleKind = matchChar('=') ? TokenKind::LshAssign : TokenKind::Lsh;
+        if (matchCodeUnit('<')) {
+          simpleKind = matchCodeUnit('=') ? TokenKind::LshAssign
+                                          : TokenKind::Lsh;
         } else {
-          simpleKind = matchChar('=') ? TokenKind::Le : TokenKind::Lt;
+          simpleKind = matchCodeUnit('=') ? TokenKind::Le : TokenKind::Lt;
         }
         break;
 
       case '>':
-        if (matchChar('>')) {
-          if (matchChar('>'))
-            simpleKind = matchChar('=') ? TokenKind::UrshAssign
-                                        : TokenKind::Ursh;
+        if (matchCodeUnit('>')) {
+          if (matchCodeUnit('>'))
+            simpleKind = matchCodeUnit('=') ? TokenKind::UrshAssign
+                                            : TokenKind::Ursh;
           else
-            simpleKind = matchChar('=') ? TokenKind::RshAssign : TokenKind::Rsh;
+            simpleKind = matchCodeUnit('=') ? TokenKind::RshAssign
+                                            : TokenKind::Rsh;
         } else {
-          simpleKind = matchChar('=') ? TokenKind::Ge : TokenKind::Gt;
+          simpleKind = matchCodeUnit('=') ? TokenKind::Ge : TokenKind::Gt;
         }
         break;
 
       case '*':
-        if (matchChar('*'))
-          simpleKind = matchChar('=') ? TokenKind::PowAssign : TokenKind::Pow;
+        if (matchCodeUnit('*'))
+          simpleKind = matchCodeUnit('=') ? TokenKind::PowAssign
+                                          : TokenKind::Pow;
         else
-          simpleKind = matchChar('=') ? TokenKind::MulAssign : TokenKind::Mul;
+          simpleKind = matchCodeUnit('=') ? TokenKind::MulAssign
+                                          : TokenKind::Mul;
         break;
 
       case '/':
         // Look for a single-line comment.
-        if (matchChar('/')) {
+        if (matchCodeUnit('/')) {
           c = getCodeUnit();
           if (c == '@' || c == '#') {
             bool shouldWarn = c == '@';
             if (!getDirectives(false, shouldWarn)) {
               return false;
             }
           } else {
             ungetCodeUnit(c);
           }
 
           consumeRestOfSingleLineComment();
           continue;
         }
 
         // Look for a multi-line comment.
-        if (matchChar('*')) {
+        if (matchCodeUnit('*')) {
           TokenStreamAnyChars& anyChars = anyCharsAccess();
           unsigned linenoBefore = anyChars.lineno;
 
           do {
             if (!getChar(&c))
               return badToken();
 
             if (c == EOF) {
               reportError(JSMSG_UNTERMINATED_COMMENT);
               return badToken();
             }
 
-            if (c == '*' && matchChar('/'))
+            if (c == '*' && matchCodeUnit('/'))
               break;
 
             if (c == '@' || c == '#') {
               bool shouldWarn = c == '@';
               if (!getDirectives(true, shouldWarn)) {
                 return false;
               }
             }
@@ -2037,36 +2042,37 @@ MOZ_MUST_USE bool TokenStreamSpecific<Ch
             reflags = RegExpFlag(reflags | flag);
           }
           ungetCodeUnit(c);
 
           newRegExpToken(reflags, start, modifier, ttp);
           return true;
         }
 
-        simpleKind = matchChar('=') ? TokenKind::DivAssign : TokenKind::Div;
+        simpleKind = matchCodeUnit('=') ? TokenKind::DivAssign : TokenKind::Div;
         break;
 
       case '%':
-        simpleKind = matchChar('=') ? TokenKind::ModAssign : TokenKind::Mod;
+        simpleKind = matchCodeUnit('=') ? TokenKind::ModAssign : TokenKind::Mod;
         break;
 
       case '-':
-        if (matchChar('-')) {
+        if (matchCodeUnit('-')) {
           if (anyCharsAccess().options().allowHTMLComments &&
               !anyCharsAccess().flags.isDirtyLine) {
-            if (matchChar('>')) {
+            if (matchCodeUnit('>')) {
               consumeRestOfSingleLineComment();
               continue;
             }
           }
 
           simpleKind = TokenKind::Dec;
         } else {
-          simpleKind = matchChar('=') ? TokenKind::SubAssign : TokenKind::Sub;
+          simpleKind = matchCodeUnit('=') ? TokenKind::SubAssign
+                                          : TokenKind::Sub;
         }
         break;
 
       default:
         // We consumed a bad character/code point.  Put it back so the
         // error location is the bad character.
         ungetCodePointIgnoreEOL(c);
         error(JSMSG_ILLEGAL_CHARACTER);
@@ -2334,17 +2340,17 @@ bool TokenStreamSpecific<CharT, AnyChars
       // U+2028 LINE SEPARATOR and U+2029 PARAGRAPH SEPARATOR encode
       // their literal values in template literals and (as of fairly
       // recently) string literals, but they still count as line
       // terminators when computing line/column coordinates.
       if (!updateLineInfoForEOL())
         return false;
 
       anyCharsAccess().updateFlagsForEOL();
-    } else if (parsingTemplate && c == '$' && matchChar('{')) {
+    } else if (parsingTemplate && c == '$' && matchCodeUnit('{')) {
       templateHead = true;
       break;
     }
 
     if (!tokenbuf.append(c)) {
       ReportOutOfMemory(anyCharsAccess().cx);
       return false;
     }
diff --git a/js/src/frontend/TokenStream.h b/js/src/frontend/TokenStream.h
--- a/js/src/frontend/TokenStream.h
+++ b/js/src/frontend/TokenStream.h
@@ -996,16 +996,24 @@ class TokenStreamCharsBase {
 
   MOZ_MUST_USE bool copyTokenbufTo(
       JSContext* cx, UniquePtr<char16_t[], JS::FreePolicy>* destination);
 
   using SourceUnits = frontend::SourceUnits<CharT>;
 
   MOZ_MUST_USE bool appendCodePointToTokenbuf(uint32_t codePoint);
 
+  // |expect| cannot be an EOL char.
+  bool matchCodeUnit(int32_t expect) {
+    MOZ_ASSERT(expect != EOF, "shouldn't be matching EOFs");
+    MOZ_ASSERT(!SourceUnits::isRawEOLChar(expect));
+    return MOZ_LIKELY(sourceUnits.hasRawChars()) &&
+           sourceUnits.matchCodeUnit(expect);
+  }
+
  protected:
   MOZ_MUST_USE bool fillWithTemplateStringContents(CharBuffer& charbuf,
                                                    const CharT* cur,
                                                    const CharT* end) {
     while (cur < end) {
       // U+2028 LINE SEPARATOR and U+2029 PARAGRAPH SEPARATOR are
       // interpreted literally inside template literal contents; only
       // literal CRLF sequences are normalized to '\n'.  See
@@ -1311,16 +1319,17 @@ class MOZ_STACK_CLASS TokenStreamSpecifi
   using CharsBase::getChar;
   using CharsBase::getCodePoint;
   using CharsBase::matchMultiUnitCodePoint;
   using CharsBase::ungetCodePointIgnoreEOL;
   using CharsSharedBase::appendCodePointToTokenbuf;
   using CharsSharedBase::atomizeChars;
   using CharsSharedBase::copyTokenbufTo;
   using CharsSharedBase::fillWithTemplateStringContents;
+  using CharsSharedBase::matchCodeUnit;
   using CharsSharedBase::sourceUnits;
   using CharsSharedBase::tokenbuf;
   using CharsSharedBase::ungetCodeUnit;
   using GeneralCharsBase::badToken;
   using GeneralCharsBase::consumeRestOfSingleLineComment;
   using GeneralCharsBase::getCodeUnit;
   using GeneralCharsBase::newAtomToken;
   using GeneralCharsBase::newNameToken;
@@ -1684,23 +1693,16 @@ class MOZ_STACK_CLASS TokenStreamSpecifi
   MOZ_MUST_USE bool getDirective(
       bool isMultiline, bool shouldWarnDeprecated, const char* directive,
       uint8_t directiveLength, const char* errorMsgPragma,
       UniquePtr<char16_t[], JS::FreePolicy>* destination);
   MOZ_MUST_USE bool getDisplayURL(bool isMultiline, bool shouldWarnDeprecated);
   MOZ_MUST_USE bool getSourceMappingURL(bool isMultiline,
                                         bool shouldWarnDeprecated);
 
-  // |expect| cannot be an EOL char.
-  bool matchChar(int32_t expect) {
-    MOZ_ASSERT(!SourceUnits::isRawEOLChar(expect));
-    return MOZ_LIKELY(sourceUnits.hasRawChars()) &&
-                      sourceUnits.matchCodeUnit(expect);
-  }
-
   void consumeKnownChar(int32_t expect) {
     int32_t c;
     MOZ_ALWAYS_TRUE(getChar(&c));
     MOZ_ASSERT(c == expect);
   }
 
   void consumeKnownCharIgnoreEOL(int32_t expect) {
 #ifdef DEBUG
