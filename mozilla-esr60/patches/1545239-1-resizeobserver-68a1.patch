# HG changeset patch
# User Boris Chiou <boris.chiou@gmail.com>
# Date 1557348771 0
# Node ID 2e3522f77b0f686663145bdce20ffceef3909896
# Parent  73c8347ecf988f4b2735ad335d68fef330cc1090
Bug 1545239 - Move GetNodeDepth into ResizeObserver.cpp r=dholbert

GetNodeDepth() is a special version for ResizeObserver to get the depth
of node (across Shadow DOM). Based on the comment in D27615, it's better
to move it into ResizeObserver.cpp.

Differential Revision: https://phabricator.services.mozilla.com/D28736

diff --git a/dom/base/ResizeObserver.cpp b/dom/base/ResizeObserver.cpp
--- a/dom/base/ResizeObserver.cpp
+++ b/dom/base/ResizeObserver.cpp
@@ -2,23 +2,53 @@
 /* vim:set ts=2 sw=2 sts=2 et cindent: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "mozilla/dom/ResizeObserver.h"
 
 #include "nsIDocument.h"
-#include "nsContentUtils.h"
+#include "nsIContentInlines.h"
 #include "nsSVGUtils.h"
 #include <limits>
 
 namespace mozilla {
 namespace dom {
 
+/**
+ * Returns the length of the parent-traversal path (in terms of the number of
+ * nodes) to an unparented/root node from aNode. An unparented/root node is
+ * considered to have a depth of 1, its children have a depth of 2, etc.
+ * aNode is expected to be non-null.
+ * Note: The shadow root is not part of the calculation because the caller,
+ * ResizeObserver, doesn't observe the shadow root, and only needs relative
+ * depths among all the observed targets. In other words, we calculate the
+ * depth of the flattened tree.
+ *
+ * However, these is a spec issue about how to handle shadow DOM case. We
+ * may need to update this function later:
+ *   https://github.com/w3c/csswg-drafts/issues/3840
+ *
+ * https://drafts.csswg.org/resize-observer/#calculate-depth-for-node-h
+ */
+static uint32_t GetNodeDepth(nsINode* aNode) {
+  uint32_t depth = 1;
+
+  MOZ_ASSERT(aNode, "Node shouldn't be null");
+
+  // Use GetFlattenedTreeParentNode to bypass the shadow root and cross the
+  // shadow boundary to calculate the node depth without the shadow root.
+  while ((aNode = aNode->GetFlattenedTreeParentNode())) {
+    ++depth;
+  }
+
+  return depth;
+}
+
 NS_IMPL_CYCLE_COLLECTION(ResizeObservation, mTarget)
 NS_IMPL_CYCLE_COLLECTION_ROOT_NATIVE(ResizeObservation, AddRef)
 NS_IMPL_CYCLE_COLLECTION_UNROOT_NATIVE(ResizeObservation, Release)
 
 bool ResizeObservation::IsActive() const {
   nsRect rect = GetTargetRect();
   return (rect.width != mBroadcastWidth || rect.height != mBroadcastHeight);
 }
@@ -131,17 +161,17 @@ void ResizeObserver::GatherActiveObserva
   mActiveTargets.Clear();
   mHasSkippedTargets = false;
 
   for (auto observation : mObservationList) {
     if (!observation->IsActive()) {
       continue;
     }
 
-    uint32_t targetDepth = nsContentUtils::GetNodeDepth(observation->Target());
+    uint32_t targetDepth = GetNodeDepth(observation->Target());
 
     if (targetDepth > aDepth) {
       mActiveTargets.AppendElement(observation);
     } else {
       mHasSkippedTargets = true;
     }
   }
 }
@@ -167,17 +197,17 @@ uint32_t ResizeObserver::BroadcastActive
       // Out of memory.
       break;
     }
 
     // Sync the broadcast size of observation so the next size inspection
     // will be based on the updated size from last delivered observations.
     observation->UpdateBroadcastSize(rect.Size());
 
-    uint32_t targetDepth = nsContentUtils::GetNodeDepth(observation->Target());
+    uint32_t targetDepth = GetNodeDepth(observation->Target());
 
     if (targetDepth < shallowestTargetDepth) {
       shallowestTargetDepth = targetDepth;
     }
   }
 
   RefPtr<ResizeObserverCallback> callback(mCallback);
   callback->Call(this, entries, *this);
diff --git a/dom/base/nsContentUtils.cpp b/dom/base/nsContentUtils.cpp
--- a/dom/base/nsContentUtils.cpp
+++ b/dom/base/nsContentUtils.cpp
@@ -10235,22 +10235,8 @@ nsContentUtils::TryGetTabChildGlobalAsEv
   return ++sInnerOrOuterWindowSerialCounter;
 }
 
 /* static */ void nsContentUtils::InnerOrOuterWindowDestroyed() {
   MOZ_ASSERT(NS_IsMainThread());
   MOZ_ASSERT(sInnerOrOuterWindowCount > 0);
   --sInnerOrOuterWindowCount;
 }
-/* static */
-uint32_t nsContentUtils::GetNodeDepth(nsINode* aNode) {
-  uint32_t depth = 1;
-
-  MOZ_ASSERT(aNode, "Node shouldn't be null");
-
-  // Use GetFlattenedTreeParentNode to bypass the shadow root and cross the
-  // shadow boundary to calculate the node depth without the shadow root.
-  while ((aNode = aNode->GetFlattenedTreeParentNode())) {
-    ++depth;
-  }
-
-  return depth;
-}
diff --git a/dom/base/nsContentUtils.h b/dom/base/nsContentUtils.h
--- a/dom/base/nsContentUtils.h
+++ b/dom/base/nsContentUtils.h
@@ -3176,34 +3176,16 @@ class nsContentUtils {
   static uint32_t InnerOrOuterWindowCreated();
   // Record that an inner or outer window has been destroyed.
   static void InnerOrOuterWindowDestroyed();
   // Get the current number of inner or outer windows.
   static int32_t GetCurrentInnerOrOuterWindowCount() {
     return sInnerOrOuterWindowCount;
   }
 
-  /**
-   * Returns the length of the parent-traversal path (in terms of the number of
-   * nodes) to an unparented/root node from aNode. An unparented/root node is
-   * considered to have a depth of 1, its children have a depth of 2, etc.
-   * aNode is expected to be non-null.
-   * Note: The shadow root is not part of the calculation because the caller,
-   * ResizeObserver, doesn't observe the shadow root, and only needs relative
-   * depths among all the observed targets. In other words, we calculate the
-   * depth of the flattened tree.
-   *
-   * However, these is a spec issue about how to handle shadow DOM case. We
-   * may need to update this function later:
-   *   https://github.com/w3c/csswg-drafts/issues/3840
-   *
-   * https://drafts.csswg.org/resize-observer/#calculate-depth-for-node-h
-   */
-  static uint32_t GetNodeDepth(nsINode* aNode);
-
  private:
   static bool InitializeEventTable();
 
   static nsresult EnsureStringBundle(PropertiesFile aFile);
 
   static bool CanCallerAccess(nsIPrincipal* aSubjectPrincipal,
                               nsIPrincipal* aPrincipal);
 
