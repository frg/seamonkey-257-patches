# HG changeset patch
# User Julian Seward <jseward@acm.org>
# Date 1529010013 -7200
# Node ID 24d8e05af3d950a18875414415ea923ccd4ae771
# Parent  dd7f068b5ade78b01b326969fc6d0635ae99ec26
Bug 1464790 - js/src/wasm/WasmTypes.h: define MaxMemoryAccessSize correctly.  r=luke.

Currently we have MaxMemoryAccessSize = sizeof(Val).  That doesn't seem
right, since it includes not only the size of the largest underlying value,
but also the size of the tag (Val::type_) field.  Indeed, printing
MaxMemoryAccessSize produces 24 on x86_64 and 20 on x86.  This patch fixes
it, and adds some sanity checks too.

diff --git a/js/src/wasm/WasmTypes.cpp b/js/src/wasm/WasmTypes.cpp
--- a/js/src/wasm/WasmTypes.cpp
+++ b/js/src/wasm/WasmTypes.cpp
@@ -41,22 +41,30 @@ using mozilla::MakeEnumeratedRange;
 // We have only tested WASM_HUGE_MEMORY on x64 and arm64.
 
 #if defined(WASM_HUGE_MEMORY)
 #if !(defined(JS_CODEGEN_X64) || defined(JS_CODEGEN_ARM64))
 #error "Not an expected configuration"
 #endif
 #endif
 
-// Another sanity check.
+// More sanity checks.
 
 static_assert(MaxMemoryInitialPages <=
                   ArrayBufferObject::MaxBufferByteLength / PageSize,
               "Memory sizing constraint");
 
+// All plausible targets must be able to do at least IEEE754 double
+// loads/stores, hence the lower limit of 8.  Some Intel processors support
+// AVX-512 loads/stores, hence the upper limit of 64.
+static_assert(MaxMemoryAccessSize >= 8,  "MaxMemoryAccessSize too low");
+static_assert(MaxMemoryAccessSize <= 64, "MaxMemoryAccessSize too high");
+static_assert((MaxMemoryAccessSize & (MaxMemoryAccessSize-1)) == 0,
+              "MaxMemoryAccessSize is not a power of two");
+
 void Val::writePayload(uint8_t* dst) const {
   switch (type_.code()) {
     case ValType::I32:
     case ValType::F32:
       memcpy(dst, &u.i32_, sizeof(u.i32_));
       return;
     case ValType::I64:
     case ValType::F64:
diff --git a/js/src/wasm/WasmTypes.h b/js/src/wasm/WasmTypes.h
--- a/js/src/wasm/WasmTypes.h
+++ b/js/src/wasm/WasmTypes.h
@@ -592,16 +592,17 @@ class Val {
     memcpy(u.i32x4_, i32x4, sizeof(u.i32x4_));
   }
   explicit Val(const F32x4& f32x4) : type_(ValType::F32x4) {
     memcpy(u.f32x4_, f32x4, sizeof(u.f32x4_));
   }
 
   ValType type() const { return type_; }
   bool isSimd() const { return IsSimdType(type()); }
+  static constexpr size_t sizeofLargestValue() { return sizeof(u); }
 
   uint32_t i32() const {
     MOZ_ASSERT(type_ == ValType::I32);
     return u.i32_;
   }
   uint64_t i64() const {
     MOZ_ASSERT(type_ == ValType::I64);
     return u.i64_;
@@ -1883,17 +1884,17 @@ static const unsigned PageSize = 64 * 10
 
 // Bounds checks always compare the base of the memory access with the bounds
 // check limit. If the memory access is unaligned, this means that, even if the
 // bounds check succeeds, a few bytes of the access can extend past the end of
 // memory. To guard against this, extra space is included in the guard region to
 // catch the overflow. MaxMemoryAccessSize is a conservative approximation of
 // the maximum guard space needed to catch all unaligned overflows.
 
-static const unsigned MaxMemoryAccessSize = sizeof(Val);
+static const unsigned MaxMemoryAccessSize = Val::sizeofLargestValue();
 
 #ifdef WASM_HUGE_MEMORY
 
 // On WASM_HUGE_MEMORY platforms, every asm.js or WebAssembly memory
 // unconditionally allocates a huge region of virtual memory of size
 // wasm::HugeMappedSize. This allows all memory resizing to work without
 // reallocation and provides enough guard space for all offsets to be folded
 // into memory accesses.
