# HG changeset patch
# User Boris Zbarsky <bzbarsky@mit.edu>
# Date 1521688408 14400
# Node ID 20d2f8bbd902d08511b4473290bce7a53b40d155
# Parent  1f3931e9ced7debca239e52248f624be058cec47
Bug 1442039 part 3.  Stop trying to resolve DOMConstructor on Xrays.  r=peterv

We don't resolve it normally, because nsDOMConstructorSH overrides
PostCreatePrototype to be a no-op, so nsWindowSH::GlobalResolve never actually
defines the relevant property on the window.  We also hide it in
nsWindowSH::NameStructEnabled.  But in the Xray-to-window case we attempt to
define it.  We shouldn't do that.

MozReview-Commit-ID: 3tnMnSQuvuT

diff --git a/dom/base/nsDOMClassInfo.cpp b/dom/base/nsDOMClassInfo.cpp
--- a/dom/base/nsDOMClassInfo.cpp
+++ b/dom/base/nsDOMClassInfo.cpp
@@ -1256,39 +1256,31 @@ nsresult nsWindowSH::GlobalResolve(
   // The class_name had better match our name
   MOZ_ASSERT(name.Equals(class_name));
 
   NS_ENSURE_TRUE(class_name, NS_ERROR_UNEXPECTED);
 
   nsresult rv = NS_OK;
 
   if (name_struct->mType == nsGlobalNameStruct::eTypeClassConstructor) {
+    // The only eTypeClassConstructor struct left is DOMConstructor.
+    MOZ_ASSERT(name.EqualsLiteral("DOMConstructor"));
+
     // Create the XPConnect prototype for our classinfo, PostCreateProto will
     // set up the prototype chain.  This will go ahead and define things on the
     // actual window's global.
     JS::Rooted<JSObject *> dot_prototype(cx);
     rv = GetXPCProto(nsDOMClassInfo::sXPConnect, cx, aWin, name_struct,
                      &dot_prototype);
     NS_ENSURE_SUCCESS(rv, rv);
     MOZ_ASSERT(dot_prototype);
 
-    bool isXray = xpc::WrapperFactory::IsXrayWrapper(obj);
-    MOZ_ASSERT_IF(obj != aWin->GetGlobalJSObject(), isXray);
-    if (!isXray) {
-      // GetXPCProto already defined the property for us
-      FillPropertyDescriptor(desc, obj, JS::UndefinedValue(), false);
-      return NS_OK;
-    }
-
-    // This is the Xray case.  Look up the constructor object for this
-    // prototype.
-    return ResolvePrototype(nsDOMClassInfo::sXPConnect, aWin, cx, obj,
-                            class_name,
-                            &sClassInfoData[name_struct->mDOMClassInfoID],
-                            name_struct, nameSpaceManager, dot_prototype, desc);
+    // GetXPCProto already defined the property for us if needed.
+    FillPropertyDescriptor(desc, obj, JS::UndefinedValue(), false);
+    return NS_OK;
   }
 
   if (name_struct->mType == nsGlobalNameStruct::eTypeProperty) {
     // Before defining a global property, check for a named subframe of the
     // same name. If it exists, we don't want to shadow it.
     if (nsCOMPtr<nsPIDOMWindowOuter> childWin = aWin->GetChildWindow(name)) {
       return NS_OK;
     }
