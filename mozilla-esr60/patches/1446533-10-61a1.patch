# HG changeset patch
# User Boris Zbarsky <bzbarsky@mit.edu>
# Date 1521488755 14400
# Node ID 6525e7ba0b1d25e045834a80cc2c441cb5429b14
# Parent  238914238db0509c0bfbf6541d3ac7194a49a2f7
Bug 1446533 part 10.  Remove remaining nsIDOMCharacterData uses in editor.  r=mystor

MozReview-Commit-ID: CfZNQRiDs43

diff --git a/editor/libeditor/EditorBase.cpp b/editor/libeditor/EditorBase.cpp
--- a/editor/libeditor/EditorBase.cpp
+++ b/editor/libeditor/EditorBase.cpp
@@ -42,16 +42,17 @@
 #include "mozilla/Preferences.h"            // for Preferences
 #include "mozilla/RangeBoundary.h"      // for RawRangeBoundary, RangeBoundary
 #include "mozilla/dom/Selection.h"      // for Selection, etc.
 #include "mozilla/Services.h"           // for GetObserverService
 #include "mozilla/TextComposition.h"    // for TextComposition
 #include "mozilla/TextInputListener.h"  // for TextInputListener
 #include "mozilla/TextServicesDocument.h"  // for TextServicesDocument
 #include "mozilla/TextEvents.h"
+#include "mozilla/dom/CharacterData.h"  // for CharacterData
 #include "mozilla/dom/Element.h"  // for Element, nsINode::AsElement
 #include "mozilla/dom/HTMLBodyElement.h"
 #include "mozilla/dom/Text.h"
 #include "mozilla/dom/Event.h"
 #include "nsAString.h"                // for nsAString::Length, etc.
 #include "nsCCUncollectableMarker.h"  // for nsCCUncollectableMarker
 #include "nsCaret.h"                  // for nsCaret
 #include "nsCaseTreatment.h"
@@ -64,17 +65,16 @@
 #include "nsError.h"                   // for NS_OK, etc.
 #include "nsFocusManager.h"            // for nsFocusManager
 #include "nsFrameSelection.h"          // for nsFrameSelection
 #include "nsGenericHTMLElement.h"      // for nsGenericHTMLElement
 #include "nsGkAtoms.h"                 // for nsGkAtoms, nsGkAtoms::dir
 #include "nsIAbsorbingTransaction.h"   // for nsIAbsorbingTransaction
 #include "nsAtom.h"                    // for nsAtom
 #include "nsIContent.h"                // for nsIContent
-#include "nsIDOMCharacterData.h"       // for nsIDOMCharacterData
 #include "nsIDOMDocument.h"            // for nsIDOMDocument
 #include "nsIDOMElement.h"             // for nsIDOMElement
 #include "nsIDOMEvent.h"               // for nsIDOMEvent
 #include "nsIDOMEventListener.h"       // for nsIDOMEventListener
 #include "nsIDOMEventTarget.h"         // for nsIDOMEventTarget
 #include "nsIDOMMouseEvent.h"          // for nsIDOMMouseEvent
 #include "nsIDOMNode.h"                // for nsIDOMNode, etc.
 #include "nsIDOMNodeList.h"            // for nsIDOMNodeList
@@ -2693,19 +2693,18 @@ nsresult EditorBase::InsertTextIntoTextN
     htmlEditRules->DidInsertText(insertedTextNode, insertedOffset,
                                  aStringToInsert);
   }
 
   // let listeners know what happened
   if (!mActionListeners.IsEmpty()) {
     AutoActionListenerArray listeners(mActionListeners);
     for (auto& listener : listeners) {
-      listener->DidInsertText(
-          static_cast<nsIDOMCharacterData*>(insertedTextNode->AsDOMNode()),
-          insertedOffset, aStringToInsert, rv);
+      listener->DidInsertText(insertedTextNode, insertedOffset,
+                              aStringToInsert, rv);
     }
   }
 
   // Added some cruft here for bug 43366.  Layout was crashing because we left
   // an empty text node lying around in the document.  So I delete empty text
   // nodes caused by IME.  I have to mark the IME transaction as "fixed", which
   // means that furure IME txns won't merge with it.  This is because we don't
   // want future IME txns trying to put their text into a node that is no
@@ -2812,18 +2811,17 @@ nsresult EditorBase::SetTextImpl(Selecti
   const uint32_t length = aCharData.Length();
 
   AutoRules beginRulesSniffing(this, EditAction::setText, nsIEditor::eNext);
 
   // Let listeners know what's up
   if (!mActionListeners.IsEmpty() && length) {
     AutoActionListenerArray listeners(mActionListeners);
     for (auto& listener : listeners) {
-      listener->WillDeleteText(
-          static_cast<nsIDOMCharacterData*>(aCharData.AsDOMNode()), 0, length);
+      listener->WillDeleteText(&aCharData, 0, length);
     }
   }
 
   // We don't support undo here, so we don't really need all of the transaction
   // machinery, therefore we can run our transaction directly, breaking all of
   // the rules!
   ErrorResult res;
   aCharData.SetData(aString, res);
@@ -2853,24 +2851,20 @@ nsresult EditorBase::SetTextImpl(Selecti
     }
   }
 
   // Let listeners know what happened
   if (!mActionListeners.IsEmpty()) {
     AutoActionListenerArray listeners(mActionListeners);
     for (auto& listener : listeners) {
       if (length) {
-        listener->DidDeleteText(
-            static_cast<nsIDOMCharacterData*>(aCharData.AsDOMNode()), 0, length,
-            rv);
+        listener->DidDeleteText(&aCharData, 0, length, rv);
       }
       if (!aString.IsEmpty()) {
-        listener->DidInsertText(
-            static_cast<nsIDOMCharacterData*>(aCharData.AsDOMNode()), 0,
-            aString, rv);
+        listener->DidInsertText(&aCharData, 0, aString, rv);
       }
     }
   }
 
   return rv;
 }
 
 nsresult EditorBase::DeleteText(CharacterData& aCharData, uint32_t aOffset,
@@ -2883,36 +2877,32 @@ nsresult EditorBase::DeleteText(Characte
 
   AutoRules beginRulesSniffing(this, EditAction::deleteText,
                                nsIEditor::ePrevious);
 
   // Let listeners know what's up
   if (!mActionListeners.IsEmpty()) {
     AutoActionListenerArray listeners(mActionListeners);
     for (auto& listener : listeners) {
-      listener->WillDeleteText(
-          static_cast<nsIDOMCharacterData*>(GetAsDOMNode(&aCharData)), aOffset,
-          aLength);
+      listener->WillDeleteText(&aCharData, aOffset, aLength);
     }
   }
 
   nsresult rv = DoTransaction(transaction);
 
   if (mRules && mRules->AsHTMLEditRules()) {
     RefPtr<HTMLEditRules> htmlEditRules = mRules->AsHTMLEditRules();
     htmlEditRules->DidDeleteText(&aCharData, aOffset, aLength);
   }
 
   // Let listeners know what happened
   if (!mActionListeners.IsEmpty()) {
     AutoActionListenerArray listeners(mActionListeners);
     for (auto& listener : listeners) {
-      listener->DidDeleteText(
-          static_cast<nsIDOMCharacterData*>(GetAsDOMNode(&aCharData)), aOffset,
-          aLength, rv);
+      listener->DidDeleteText(&aCharData, aOffset, aLength, rv);
     }
   }
 
   return rv;
 }
 
 struct SavedRange final {
   RefPtr<Selection> mSelection;
@@ -4058,17 +4048,18 @@ EditorBase::DeleteSelectionImpl(EDirecti
     deleteSelectionTransaction =
         CreateTxnForDeleteSelection(aAction, getter_AddRefs(deleteNode),
                                     &deleteCharOffset, &deleteCharLength);
     if (NS_WARN_IF(!deleteSelectionTransaction)) {
       return NS_ERROR_FAILURE;
     }
   }
 
-  nsCOMPtr<nsIDOMCharacterData> deleteCharData(do_QueryInterface(deleteNode));
+  RefPtr<CharacterData> deleteCharData =
+      CharacterData::FromContentOrNull(deleteNode);
   AutoRules beginRulesSniffing(this, EditAction::deleteSelection, aAction);
 
   if (mRules && mRules->AsHTMLEditRules()) {
     if (!deleteNode) {
       RefPtr<HTMLEditRules> htmlEditRules = mRules->AsHTMLEditRules();
       htmlEditRules->WillDeleteSelection(selection);
     } else if (!deleteCharData) {
       RefPtr<HTMLEditRules> htmlEditRules = mRules->AsHTMLEditRules();
diff --git a/editor/libeditor/HTMLEditRules.cpp b/editor/libeditor/HTMLEditRules.cpp
--- a/editor/libeditor/HTMLEditRules.cpp
+++ b/editor/libeditor/HTMLEditRules.cpp
@@ -34,17 +34,16 @@
 #include "nsContentUtils.h"
 #include "nsDebug.h"
 #include "nsError.h"
 #include "nsGkAtoms.h"
 #include "nsAtom.h"
 #include "nsIContent.h"
 #include "nsIContentIterator.h"
 #include "nsID.h"
-#include "nsIDOMCharacterData.h"
 #include "nsIDOMDocument.h"
 #include "nsIDOMElement.h"
 #include "nsIDOMNode.h"
 #include "nsIFrame.h"
 #include "nsIHTMLAbsPosEditor.h"
 #include "nsIHTMLDocument.h"
 #include "nsINode.h"
 #include "nsLiteralString.h"
diff --git a/editor/libeditor/HTMLEditRules.h b/editor/libeditor/HTMLEditRules.h
--- a/editor/libeditor/HTMLEditRules.h
+++ b/editor/libeditor/HTMLEditRules.h
@@ -13,17 +13,16 @@
 #include "nsCOMPtr.h"
 #include "nsIEditor.h"
 #include "nsIHTMLEditor.h"
 #include "nsISupportsImpl.h"
 #include "nsTArray.h"
 #include "nscore.h"
 
 class nsAtom;
-class nsIDOMCharacterData;
 class nsIDOMDocument;
 class nsIDOMElement;
 class nsIDOMNode;
 class nsIEditor;
 class nsINode;
 class nsRange;
 
 namespace mozilla {
diff --git a/editor/libeditor/JoinNodeTransaction.cpp b/editor/libeditor/JoinNodeTransaction.cpp
--- a/editor/libeditor/JoinNodeTransaction.cpp
+++ b/editor/libeditor/JoinNodeTransaction.cpp
@@ -1,21 +1,21 @@
 /* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "JoinNodeTransaction.h"
 
 #include "mozilla/EditorBase.h"  // for EditorBase
+#include "mozilla/dom/Text.h"
 #include "nsAString.h"
 #include "nsDebug.h"              // for NS_ASSERTION, etc.
 #include "nsError.h"              // for NS_ERROR_NULL_POINTER, etc.
 #include "nsIContent.h"           // for nsIContent
-#include "nsIDOMCharacterData.h"  // for nsIDOMCharacterData
 #include "nsIEditor.h"            // for EditorBase::IsModifiableNode
 #include "nsISupportsImpl.h"      // for QueryInterface, etc.
 
 namespace mozilla {
 
 using namespace dom;
 
 // static
diff --git a/editor/libeditor/SelectionState.cpp b/editor/libeditor/SelectionState.cpp
--- a/editor/libeditor/SelectionState.cpp
+++ b/editor/libeditor/SelectionState.cpp
@@ -8,17 +8,16 @@
 #include "mozilla/Assertions.h"     // for MOZ_ASSERT, etc.
 #include "mozilla/EditorUtils.h"    // for EditorUtils
 #include "mozilla/dom/Selection.h"  // for Selection
 #include "nsAString.h"              // for nsAString::Length
 #include "nsCycleCollectionParticipant.h"
 #include "nsDebug.h"              // for NS_ENSURE_TRUE, etc.
 #include "nsError.h"              // for NS_OK, etc.
 #include "nsIContent.h"           // for nsIContent
-#include "nsIDOMCharacterData.h"  // for nsIDOMCharacterData
 #include "nsIDOMNode.h"           // for nsIDOMNode
 #include "nsISupportsImpl.h"      // for nsRange::Release
 #include "nsRange.h"              // for nsRange
 
 namespace mozilla {
 
 using namespace dom;
 
@@ -427,22 +426,16 @@ nsresult RangeUpdater::SelAdjDeleteText(
       if (item->mEndOffset < 0) {
         item->mEndOffset = 0;
       }
     }
   }
   return NS_OK;
 }
 
-nsresult RangeUpdater::SelAdjDeleteText(nsIDOMCharacterData* aTextNode,
-                                        int32_t aOffset, int32_t aLength) {
-  nsCOMPtr<nsIContent> textNode = do_QueryInterface(aTextNode);
-  return SelAdjDeleteText(textNode, aOffset, aLength);
-}
-
 nsresult RangeUpdater::WillReplaceContainer() {
   if (mLock) {
     return NS_ERROR_UNEXPECTED;
   }
   mLock = true;
   return NS_OK;
 }
 
diff --git a/editor/libeditor/SelectionState.h b/editor/libeditor/SelectionState.h
--- a/editor/libeditor/SelectionState.h
+++ b/editor/libeditor/SelectionState.h
@@ -9,17 +9,16 @@
 #include "mozilla/EditorDOMPoint.h"
 #include "nsCOMPtr.h"
 #include "nsIDOMNode.h"
 #include "nsINode.h"
 #include "nsTArray.h"
 #include "nscore.h"
 
 class nsCycleCollectionTraversalCallback;
-class nsIDOMCharacterData;
 class nsRange;
 namespace mozilla {
 class RangeUpdater;
 namespace dom {
 class Selection;
 class Text;
 }  // namespace dom
 
@@ -108,18 +107,16 @@ class RangeUpdater final {
   nsresult SelAdjSplitNode(nsIContent& aRightNode, nsIContent* aNewLeftNode);
   nsresult SelAdjJoinNodes(nsINode& aLeftNode, nsINode& aRightNode,
                            nsINode& aParent, int32_t aOffset,
                            int32_t aOldLeftNodeLength);
   void SelAdjInsertText(dom::Text& aTextNode, int32_t aOffset,
                         const nsAString& aString);
   nsresult SelAdjDeleteText(nsIContent* aTextNode, int32_t aOffset,
                             int32_t aLength);
-  nsresult SelAdjDeleteText(nsIDOMCharacterData* aTextNode, int32_t aOffset,
-                            int32_t aLength);
   // the following gravity routines need will/did sandwiches, because the other
   // gravity routines will be called inside of these sandwiches, but should be
   // ignored.
   nsresult WillReplaceContainer();
   nsresult DidReplaceContainer(dom::Element* aOriginalNode,
                                dom::Element* aNewNode);
   nsresult WillRemoveContainer();
   nsresult DidRemoveContainer(nsINode* aNode, nsINode* aParent, int32_t aOffset,
diff --git a/editor/libeditor/TextEditorTest.cpp b/editor/libeditor/TextEditorTest.cpp
--- a/editor/libeditor/TextEditorTest.cpp
+++ b/editor/libeditor/TextEditorTest.cpp
@@ -8,17 +8,16 @@
 #include "TextEditorTest.h"
 
 #include <stdio.h>
 
 #include "nsDebug.h"
 #include "nsError.h"
 #include "nsGkAtoms.h"
 #include "nsIDocument.h"
-#include "nsIDOMCharacterData.h"
 #include "nsIDOMDocument.h"
 #include "nsIDOMNode.h"
 #include "nsIDOMNodeList.h"
 #include "nsIEditor.h"
 #include "nsIHTMLEditor.h"
 #include "nsINodeList.h"
 #include "nsIPlaintextEditor.h"
 #include "nsISelection.h"
diff --git a/editor/nsIEditActionListener.idl b/editor/nsIEditActionListener.idl
--- a/editor/nsIEditActionListener.idl
+++ b/editor/nsIEditActionListener.idl
@@ -74,47 +74,47 @@ interface nsIEditActionListener : nsISup
    */
   void DidJoinNodes(in nsIDOMNode aLeftNode,
                           in nsIDOMNode aRightNode,
                           in nsIDOMNode aParent,
                           in nsresult    aResult);
 
   /**
    * Called after the editor inserts text.
-   * @param aTextNode   This node getting inserted text
+   * @param aTextNode   This node getting inserted text.  Should be a CharacterData after bug 1444991.
    * @param aOffset     The offset in aTextNode to insert at.
    * @param aString     The string that gets inserted.
    * @param aResult     The result of the insert text operation.
    */
-  void DidInsertText(in nsIDOMCharacterData aTextNode,
-                           in long                aOffset,
-                           in DOMString           aString,
-                           in nsresult            aResult);
+  void DidInsertText(in nsISupports aTextNode,
+                     in long                aOffset,
+                     in DOMString           aString,
+                     in nsresult            aResult);
 
   /**
    * Called before the editor deletes text.
-   * @param aTextNode   This node getting text deleted
+   * @param aTextNode   This node getting text deleted.  Should be a CharacterData after bug 1444991.
    * @param aOffset     The offset in aTextNode to delete at.
    * @param aLength     The amount of text to delete.
    */
-  void WillDeleteText(in nsIDOMCharacterData aTextNode,
-                            in long                aOffset,
-                            in long                aLength);
+  void WillDeleteText(in nsISupports aTextNode,
+                      in long                aOffset,
+                      in long                aLength);
 
   /**
    * Called before the editor deletes text.
-   * @param aTextNode   This node getting text deleted
+   * @param aTextNode   This node getting text deleted.  Should be a CharacterData after bug 1444991.
    * @param aOffset     The offset in aTextNode to delete at.
    * @param aLength     The amount of text to delete.
    * @param aResult     The result of the delete text operation.
    */
-  void DidDeleteText(in nsIDOMCharacterData aTextNode,
-                           in long                aOffset,
-                           in long                aLength,
-                           in nsresult              aResult);
+  void DidDeleteText(in nsISupports aTextNode,
+                     in long                aOffset,
+                     in long                aLength,
+                     in nsresult              aResult);
 
   /**
    * Called before the editor deletes the selection.
    * @param aSelection   The selection to be deleted
    */
   void WillDeleteSelection(in nsISelection aSelection);
 
   /**
diff --git a/editor/spellchecker/TextServicesDocument.cpp b/editor/spellchecker/TextServicesDocument.cpp
--- a/editor/spellchecker/TextServicesDocument.cpp
+++ b/editor/spellchecker/TextServicesDocument.cpp
@@ -3106,32 +3106,31 @@ TextServicesDocument::DidJoinNodes(nsIDO
 
 NS_IMETHODIMP
 TextServicesDocument::DidCreateNode(const nsAString& aTag, nsIDOMNode* aNewNode,
                                     nsresult aResult) {
   return NS_OK;
 }
 
 NS_IMETHODIMP
-TextServicesDocument::DidInsertText(nsIDOMCharacterData* aTextNode,
-                                    int32_t aOffset, const nsAString& aString,
+TextServicesDocument::DidInsertText(nsISupports* aTextNode, int32_t aOffset,
+                                    const nsAString& aString,
                                     nsresult aResult) {
   return NS_OK;
 }
 
 NS_IMETHODIMP
-TextServicesDocument::WillDeleteText(nsIDOMCharacterData* aTextNode,
-                                     int32_t aOffset, int32_t aLength) {
+TextServicesDocument::WillDeleteText(nsISupports* aTextNode, int32_t aOffset,
+                                     int32_t aLength) {
   return NS_OK;
 }
 
 NS_IMETHODIMP
-TextServicesDocument::DidDeleteText(nsIDOMCharacterData* aTextNode,
-                                    int32_t aOffset, int32_t aLength,
-                                    nsresult aResult) {
+TextServicesDocument::DidDeleteText(nsISupports* aTextNode, int32_t aOffset,
+                                    int32_t aLength, nsresult aResult) {
   return NS_OK;
 }
 
 NS_IMETHODIMP
 TextServicesDocument::WillDeleteSelection(nsISelection* aSelection) {
   return NS_OK;
 }
 
diff --git a/editor/spellchecker/TextServicesDocument.h b/editor/spellchecker/TextServicesDocument.h
--- a/editor/spellchecker/TextServicesDocument.h
+++ b/editor/spellchecker/TextServicesDocument.h
@@ -11,17 +11,16 @@
 #include "nsIEditActionListener.h"
 #include "nsISupportsImpl.h"
 #include "nsStringFwd.h"
 #include "nsTArray.h"
 #include "nscore.h"
 
 class nsIContent;
 class nsIContentIterator;
-class nsIDOMCharacterData;
 class nsIDOMDocument;
 class nsIDOMNode;
 class nsIEditor;
 class nsINode;
 class nsISelection;
 class nsISelectionController;
 class nsITextServicesFilter;
 class nsRange;
