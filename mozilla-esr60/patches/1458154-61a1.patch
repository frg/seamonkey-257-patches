# HG changeset patch
# User Paul Bone <pbone@mozilla.com>
# Date 1518416253 -39600
# Node ID 3b9cbc054960cd02fcfea9015abb7fdc35ff3a4d
# Parent  e364be89f199aef67e143a00311f3ffc010c8176
Bug 1458154 - Add a tunable for idle nursery collection r=jonco

diff --git a/js/public/GCAPI.h b/js/public/GCAPI.h
--- a/js/public/GCAPI.h
+++ b/js/public/GCAPI.h
@@ -251,16 +251,25 @@ typedef enum JSGCParamKey {
   /**
    * Factor for triggering a GC based on JSGC_ALLOCATION_THRESHOLD.
    * Used if another GC (in different zones) is already running.
    *
    * Default: ZoneAllocThresholdFactorAvoidInterruptDefault
    * Pref: None
    */
   JSGC_ALLOCATION_THRESHOLD_FACTOR_AVOID_INTERRUPT = 26,
+
+  /**
+   * Attempt to run a minor GC in the idle time if the free space falls
+   * below this threshold.
+   *
+   * Default: NurseryChunkUsableSize / 4
+   * Pref: None
+   */
+  JSGC_NURSERY_FREE_THRESHOLD_FOR_IDLE_COLLECTION = 27,
 } JSGCParamKey;
 
 /*
  * Generic trace operation that calls JS::TraceEdge on each traceable thing's
  * location reachable from data.
  */
 typedef void (*JSTraceDataOp)(JSTracer* trc, void* data);
 
diff --git a/js/src/gc/GC.cpp b/js/src/gc/GC.cpp
--- a/js/src/gc/GC.cpp
+++ b/js/src/gc/GC.cpp
@@ -331,16 +331,20 @@ static const uint32_t MaxEmptyChunkCount
 static const int64_t DefaultTimeBudget = SliceBudget::UnlimitedTimeBudget;
 
 /* JSGC_MODE */
 static const JSGCMode Mode = JSGC_MODE_INCREMENTAL;
 
 /* JSGC_COMPACTING_ENABLED */
 static const bool CompactingEnabled = true;
 
+/* JSGC_NURSERY_FREE_THRESHOLD_FOR_IDLE_COLLECTION */
+static const uint32_t NurseryFreeThresholdForIdleCollection =
+    Nursery::NurseryChunkUsableSize / 4;
+
 }  // namespace TuningDefaults
 }  // namespace gc
 }  // namespace js
 
 /*
  * We start to incremental collection for a zone when a proportion of its
  * threshold is reached. This is configured by the
  * JSGC_ALLOCATION_THRESHOLD_FACTOR and
@@ -1367,16 +1371,21 @@ bool GCSchedulingTunables::setParameter(
       break;
     }
     case JSGC_MIN_EMPTY_CHUNK_COUNT:
       setMinEmptyChunkCount(value);
       break;
     case JSGC_MAX_EMPTY_CHUNK_COUNT:
       setMaxEmptyChunkCount(value);
       break;
+    case JSGC_NURSERY_FREE_THRESHOLD_FOR_IDLE_COLLECTION:
+      if (value > gcMaxNurseryBytes())
+        value = gcMaxNurseryBytes();
+      nurseryFreeThresholdForIdleCollection_ = value;
+      break;
     default:
       MOZ_CRASH("Unknown GC parameter.");
   }
 
   return true;
 }
 
 void GCSchedulingTunables::setMaxMallocBytes(size_t value) {
@@ -1445,17 +1454,19 @@ GCSchedulingTunables::GCSchedulingTunabl
       highFrequencyThresholdUsec_(TuningDefaults::HighFrequencyThresholdUsec),
       highFrequencyLowLimitBytes_(TuningDefaults::HighFrequencyLowLimitBytes),
       highFrequencyHighLimitBytes_(TuningDefaults::HighFrequencyHighLimitBytes),
       highFrequencyHeapGrowthMax_(TuningDefaults::HighFrequencyHeapGrowthMax),
       highFrequencyHeapGrowthMin_(TuningDefaults::HighFrequencyHeapGrowthMin),
       lowFrequencyHeapGrowth_(TuningDefaults::LowFrequencyHeapGrowth),
       dynamicMarkSliceEnabled_(TuningDefaults::DynamicMarkSliceEnabled),
       minEmptyChunkCount_(TuningDefaults::MinEmptyChunkCount),
-      maxEmptyChunkCount_(TuningDefaults::MaxEmptyChunkCount) {}
+      maxEmptyChunkCount_(TuningDefaults::MaxEmptyChunkCount),
+      nurseryFreeThresholdForIdleCollection_(
+          TuningDefaults::NurseryFreeThresholdForIdleCollection) {}
 
 void GCRuntime::resetParameter(JSGCParamKey key, AutoLockGC& lock) {
   switch (key) {
     case JSGC_MAX_MALLOC_BYTES:
       setMaxMallocBytes(TuningDefaults::MaxMallocBytes, lock);
       break;
     case JSGC_SLICE_TIME_BUDGET:
       defaultTimeBudget_ = TuningDefaults::DefaultTimeBudget;
@@ -1522,16 +1533,20 @@ void GCSchedulingTunables::resetParamete
           TuningDefaults::AllocThresholdFactorAvoidInterrupt;
       break;
     case JSGC_MIN_EMPTY_CHUNK_COUNT:
       setMinEmptyChunkCount(TuningDefaults::MinEmptyChunkCount);
       break;
     case JSGC_MAX_EMPTY_CHUNK_COUNT:
       setMaxEmptyChunkCount(TuningDefaults::MaxEmptyChunkCount);
       break;
+    case JSGC_NURSERY_FREE_THRESHOLD_FOR_IDLE_COLLECTION:
+      nurseryFreeThresholdForIdleCollection_ =
+          TuningDefaults::NurseryFreeThresholdForIdleCollection;
+      break;
     default:
       MOZ_CRASH("Unknown GC parameter.");
   }
 }
 
 uint32_t GCRuntime::getParameter(JSGCParamKey key, const AutoLockGC& lock) {
   switch (key) {
     case JSGC_MAX_BYTES:
diff --git a/js/src/gc/Nursery.cpp b/js/src/gc/Nursery.cpp
--- a/js/src/gc/Nursery.cpp
+++ b/js/src/gc/Nursery.cpp
@@ -605,16 +605,22 @@ inline void js::Nursery::startProfile(Pr
   startTimes_[key] = TimeStamp::Now();
 }
 
 inline void js::Nursery::endProfile(ProfileKey key) {
   profileDurations_[key] = TimeStamp::Now() - startTimes_[key];
   totalDurations_[key] += profileDurations_[key];
 }
 
+bool js::Nursery::needIdleTimeCollection() const {
+  uint32_t threshold =
+      runtime()->gc.tunables.nurseryFreeThresholdForIdleCollection();
+  return minorGCRequested() || freeSpace() < threshold;
+}
+
 static inline bool IsFullStoreBufferReason(JS::gcreason::Reason reason) {
   return reason == JS::gcreason::FULL_WHOLE_CELL_BUFFER ||
          reason == JS::gcreason::FULL_GENERIC_BUFFER ||
          reason == JS::gcreason::FULL_VALUE_BUFFER ||
          reason == JS::gcreason::FULL_CELL_PTR_BUFFER ||
          reason == JS::gcreason::FULL_SLOT_BUFFER ||
          reason == JS::gcreason::FULL_SHAPE_BUFFER;
 }
diff --git a/js/src/gc/Nursery.h b/js/src/gc/Nursery.h
--- a/js/src/gc/Nursery.h
+++ b/js/src/gc/Nursery.h
@@ -337,32 +337,25 @@ class Nursery {
   }
   JS::gcreason::Reason minorGCTriggerReason() const {
     return minorGCTriggerReason_;
   }
   void clearMinorGCRequest() {
     minorGCTriggerReason_ = JS::gcreason::NO_REASON;
   }
 
-  bool needIdleTimeCollection() const {
-    return minorGCRequested() || (freeSpace() < kIdleTimeCollectionThreshold);
-  }
+  bool needIdleTimeCollection() const;
 
   bool enableProfiling() const { return enableProfiling_; }
 
- private:
   /* The amount of space in the mapped nursery available to allocations. */
   static const size_t NurseryChunkUsableSize =
       gc::ChunkSize - gc::ChunkTrailerSize;
 
-  /* Attemp to run a minor GC in the idle time if the free space falls below
-   * this threshold. */
-  static constexpr size_t kIdleTimeCollectionThreshold =
-      NurseryChunkUsableSize / 4;
-
+ private:
   JSRuntime* runtime_;
 
   /* Vector of allocated chunks to allocate from. */
   Vector<NurseryChunk*, 0, SystemAllocPolicy> chunks_;
 
   /* Pointer to the first unallocated byte in the nursery. */
   uintptr_t position_;
 
diff --git a/js/src/gc/Scheduling.h b/js/src/gc/Scheduling.h
--- a/js/src/gc/Scheduling.h
+++ b/js/src/gc/Scheduling.h
@@ -418,16 +418,24 @@ class GCSchedulingTunables {
    * JSGC_MIN_EMPTY_CHUNK_COUNT
    * JSGC_MAX_EMPTY_CHUNK_COUNT
    *
    * Controls the number of empty chunks reserved for future allocation.
    */
   UnprotectedData<uint32_t> minEmptyChunkCount_;
   UnprotectedData<uint32_t> maxEmptyChunkCount_;
 
+  /*
+   * JSGC_NURSERY_FREE_THRESHOLD_FOR_IDLE_COLLECTION
+   *
+   * Attempt to run a minor GC in the idle time if the free space falls
+   * below this threshold.
+   */
+  UnprotectedData<uint32_t> nurseryFreeThresholdForIdleCollection_;
+
  public:
   GCSchedulingTunables();
 
   size_t gcMaxBytes() const { return gcMaxBytes_; }
   size_t maxMallocBytes() const { return maxMallocBytes_; }
   size_t gcMaxNurseryBytes() const { return gcMaxNurseryBytes_; }
   size_t gcZoneAllocThresholdBase() const { return gcZoneAllocThresholdBase_; }
   double allocThresholdFactor() const { return allocThresholdFactor_; }
@@ -452,16 +460,19 @@ class GCSchedulingTunables {
     return highFrequencyHeapGrowthMin_;
   }
   double lowFrequencyHeapGrowth() const { return lowFrequencyHeapGrowth_; }
   bool isDynamicMarkSliceEnabled() const { return dynamicMarkSliceEnabled_; }
   unsigned minEmptyChunkCount(const AutoLockGC&) const {
     return minEmptyChunkCount_;
   }
   unsigned maxEmptyChunkCount() const { return maxEmptyChunkCount_; }
+  uint32_t nurseryFreeThresholdForIdleCollection() const {
+    return nurseryFreeThresholdForIdleCollection_;
+  }
 
   MOZ_MUST_USE bool setParameter(JSGCParamKey key, uint32_t value,
                                  const AutoLockGC& lock);
   void resetParameter(JSGCParamKey key, const AutoLockGC& lock);
 
   void setMaxMallocBytes(size_t value);
 
  private:
