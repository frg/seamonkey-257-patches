# HG changeset patch
# User Markus Stange <mstange@themasta.com>
# Date 1550080241 0
# Node ID 61be6672af50f4772f5c3277bee000d6dd3d44ce
# Parent  8e98b1f316840fbb04128030ed7bd7634dff0be0
Bug 1491445 - Move any drawing from the ChildView into a new subview called PixelHostingView. r=spohl

This has two advantages:
 - The drawing will now correctly be placed "on top" of the vibrancy views.
 - We can turn this new view into a layer-hosting view. Layer-hosting views are
   supposed to be leaf NSViews; they shouldn't have any children.

Differential Revision: https://phabricator.services.mozilla.com/D19600

diff --git a/widget/cocoa/nsChildView.h b/widget/cocoa/nsChildView.h
--- a/widget/cocoa/nsChildView.h
+++ b/widget/cocoa/nsChildView.h
@@ -52,16 +52,18 @@ class GLManager;
 class IAPZCTreeManager;
 }  // namespace layers
 namespace widget {
 class RectTextureImage;
 class WidgetRenderingContext;
 }  // namespace widget
 }  // namespace mozilla
 
+@class PixelHostingView;
+
 @interface NSEvent (Undocumented)
 
 // Return Cocoa event's corresponding Carbon event.  Not initialized (on
 // synthetic events) until the OS actually "sends" the event.  This method
 // has been present in the same form since at least OS X 10.2.8.
 - (EventRef)_eventRef;
 
 // stage From 10.10.3 for force touch event
@@ -198,16 +200,20 @@ class WidgetRenderingContext;
   // CGContext painting (i.e. non-accelerated).
   CGImageRef mTopLeftCornerMask;
 
   // Subviews of self, which act as container views for vibrancy views and
   // non-draggable views.
   NSView* mVibrancyViewsContainer;      // [STRONG]
   NSView* mNonDraggableViewsContainer;  // [STRONG]
 
+  // The view that does our drawing. This is a subview of self so that it can
+  // be ordered on top of mVibrancyViewsContainer.
+  PixelHostingView* mPixelHostingView;
+
   // Last pressure stage by trackpad's force click
   NSInteger mLastPressureStage;
 }
 
 // class initialization
 + (void)initialize;
 
 + (void)registerViewForDraggedTypes:(NSView*)aView;
@@ -228,16 +234,17 @@ class WidgetRenderingContext;
 - (void)updateGLContext;
 - (void)_surfaceNeedsUpdate:(NSNotification*)notification;
 
 - (bool)preRender:(NSOpenGLContext*)aGLContext;
 - (void)postRender:(NSOpenGLContext*)aGLContext;
 
 - (NSView*)vibrancyViewsContainer;
 - (NSView*)nonDraggableViewsContainer;
+- (NSView*)pixelHostingView;
 
 - (BOOL)isCoveringTitlebar;
 
 - (void)viewWillStartLiveResize;
 - (void)viewDidEndLiveResize;
 
 - (NSColor*)vibrancyFillColorForThemeGeometryType:(nsITheme::ThemeGeometryType)aThemeGeometryType;
 
diff --git a/widget/cocoa/nsChildView.mm b/widget/cocoa/nsChildView.mm
--- a/widget/cocoa/nsChildView.mm
+++ b/widget/cocoa/nsChildView.mm
@@ -158,16 +158,21 @@ uint32_t nsChildView::sLastInputEventCou
 
 static bool sIsTabletPointerActivated = false;
 
 static uint32_t sUniqueKeyEventId = 0;
 
 static NSMutableDictionary* sNativeKeyEventsMap =
   [NSMutableDictionary dictionary];
 
+// The view that will do our drawing or host our NSOpenGLContext or Core Animation layer.
+@interface PixelHostingView : NSView {
+}
+@end
+
 @interface ChildView(Private)
 
 // sets up our view, attaching it to its owning gecko view
 - (id)initWithFrame:(NSRect)inFrame geckoChild:(nsChildView*)inChild;
 
 // set up a gecko mouse event based on a cocoa mouse event
 - (void) convertCocoaMouseWheelEvent:(NSEvent*)aMouseEvent
                         toGeckoEvent:(WidgetWheelEvent*)outWheelEvent;
@@ -970,18 +975,19 @@ nsChildView::Resize(double aWidth, doubl
 
   mBounds.width  = width;
   mBounds.height = height;
 
   ManipulateViewWithoutNeedingDisplay(mView, ^{
     [mView setFrame:DevPixelsToCocoaPoints(mBounds)];
   });
 
-  if (mVisible && aRepaint)
-    [mView setNeedsDisplay:YES];
+  if (mVisible && aRepaint) {
+    [[mView pixelHostingView] setNeedsDisplay:YES];
+  }
 
   NotifyRollupGeometryChange();
   ReportSizeEvent();
 
   NS_OBJC_END_TRY_ABORT_BLOCK;
 }
 
 void
@@ -1008,18 +1014,19 @@ nsChildView::Resize(double aX, double aY
     mBounds.width  = width;
     mBounds.height = height;
   }
 
   ManipulateViewWithoutNeedingDisplay(mView, ^{
     [mView setFrame:DevPixelsToCocoaPoints(mBounds)];
   });
 
-  if (mVisible && aRepaint)
-    [mView setNeedsDisplay:YES];
+  if (mVisible && aRepaint) {
+    [[mView pixelHostingView] setNeedsDisplay:YES];
+  }
 
   NotifyRollupGeometryChange();
   if (isMoving) {
     ReportMoveEvent();
     if (mOnDestroyCalled)
       return;
   }
   if (isResizing)
@@ -1372,17 +1379,17 @@ nsChildView::Invalidate(const LayoutDevi
   NS_OBJC_BEGIN_TRY_ABORT_BLOCK;
 
   if (!mView || !mVisible)
     return;
 
   NS_ASSERTION(GetLayerManager()->GetBackendType() != LayersBackend::LAYERS_CLIENT,
                "Shouldn't need to invalidate with accelerated OMTC layers!");
 
-  [mView setNeedsDisplayInRect:DevPixelsToCocoaPoints(aRect)];
+  [[mView pixelHostingView] setNeedsDisplayInRect:DevPixelsToCocoaPoints(aRect)];
 
   NS_OBJC_END_TRY_ABORT_BLOCK;
 }
 
 bool
 nsChildView::WidgetTypeSupportsAcceleration()
 {
   // We need to enable acceleration in popups which contain remote layer
@@ -3295,16 +3302,21 @@ NSEvent* gLastDragMouseDownEvent = nil;
     mVibrancyViewsContainer = [[ViewRegionContainerView alloc] initWithFrame:[self bounds]];
 
     [mNonDraggableViewsContainer setAutoresizingMask:NSViewWidthSizable | NSViewHeightSizable];
     [mVibrancyViewsContainer setAutoresizingMask:NSViewWidthSizable | NSViewHeightSizable];
 
     [self addSubview:mNonDraggableViewsContainer];
     [self addSubview:mVibrancyViewsContainer];
 
+    mPixelHostingView = [[PixelHostingView alloc] initWithFrame:[self bounds]];
+    [mPixelHostingView setAutoresizingMask:NSViewWidthSizable | NSViewHeightSizable];
+
+    [self addSubview:mPixelHostingView];
+
     mTopLeftCornerMask = NULL;
     mLastPressureStage = 0;
   }
 
   // register for things we'll take from other applications
   [ChildView registerViewForDraggedTypes:self];
 
   [[NSNotificationCenter defaultCenter] addObserver:self
@@ -3322,17 +3334,17 @@ NSEvent* gLastDragMouseDownEvent = nil;
   [[NSDistributedNotificationCenter defaultCenter] addObserver:self
                                                       selector:@selector(systemMetricsChanged)
                                                           name:@"AppleAquaScrollBarVariantChanged"
                                                         object:nil
                                             suspensionBehavior:NSNotificationSuspensionBehaviorDeliverImmediately];
   [[NSNotificationCenter defaultCenter] addObserver:self
                                            selector:@selector(_surfaceNeedsUpdate:)
                                                name:NSViewGlobalFrameDidChangeNotification
-                                             object:self];
+                                             object:mPixelHostingView];
 
   return self;
 
   NS_OBJC_END_TRY_ABORT_BLOCK_NIL;
 }
 
 // ComplexTextInputPanel's interpretKeyEvent hack won't work without this.
 // It makes calls to +[NSTextInputContext currentContext], deep in system
@@ -3415,16 +3427,20 @@ NSEvent* gLastDragMouseDownEvent = nil;
 - (NSView*)vibrancyViewsContainer {
   return mVibrancyViewsContainer;
 }
 
 - (NSView*)nonDraggableViewsContainer {
   return mNonDraggableViewsContainer;
 }
 
+- (NSView*)pixelHostingView {
+  return mPixelHostingView;
+}
+
 - (void)dealloc
 {
   NS_OBJC_BEGIN_TRY_ABORT_BLOCK;
 
   [mGLContext release];
   [mLastMouseDownEvent release];
   [mLastKeyDownEvent release];
   [mClickThroughMouseDownEvent release];
@@ -3432,16 +3448,18 @@ NSEvent* gLastDragMouseDownEvent = nil;
   ChildViewMouseTracker::OnDestroyView(self);
 
   [[NSNotificationCenter defaultCenter] removeObserver:self];
   [[NSDistributedNotificationCenter defaultCenter] removeObserver:self];
   [mVibrancyViewsContainer removeFromSuperview];
   [mVibrancyViewsContainer release];
   [mNonDraggableViewsContainer removeFromSuperview];
   [mNonDraggableViewsContainer release];
+  [mPixelHostingView removeFromSuperview];
+  [mPixelHostingView release];
 
   [super dealloc];
 
   NS_OBJC_END_TRY_ABORT_BLOCK;
 }
 
 - (void)widgetDestroyed
 {
@@ -3529,36 +3547,30 @@ NSEvent* gLastDragMouseDownEvent = nil;
 {
   // Return YES so that parts of this view can be draggable. The non-draggable
   // parts will be covered by NSViews that return NO from
   // mouseDownCanMoveWindow and thus override draggability from the inside.
   // These views are assembled in nsChildView::UpdateWindowDraggingRegion.
   return YES;
 }
 
--(void)updateGLContext
-{
-  [mGLContext setView:self];
+-(void)updateGLContext {
+  [mGLContext setView:mPixelHostingView];
   [mGLContext update];
 }
 
 - (void)_surfaceNeedsUpdate:(NSNotification*)notification
 {
   if (mGLContext) {
     CGLLockContext((CGLContextObj)[mGLContext CGLContextObj]);
     mNeedsGLUpdate = YES;
     CGLUnlockContext((CGLContextObj)[mGLContext CGLContextObj]);
   }
 }
 
-- (BOOL)wantsBestResolutionOpenGLSurface
-{
-  return nsCocoaUtils::HiDPIEnabled() ? YES : NO;
-}
-
 - (void)viewDidChangeBackingProperties
 {
   [super viewDidChangeBackingProperties];
   if (mGeckoChild) {
     // actually, it could be the color space that's changed,
     // but we can't tell the difference here except by retrieving
     // the backing scale factor and comparing to the old value
     mGeckoChild->BackingScaleFactorChanged();
@@ -3602,34 +3614,34 @@ NSEvent* gLastDragMouseDownEvent = nil;
   return mGeckoChild->VibrancyFillColorForThemeGeometryType(aThemeGeometryType);
 }
 
 - (LayoutDeviceIntRegion)nativeDirtyRegionWithBoundingRect:(NSRect)aRect
 {
   LayoutDeviceIntRect boundingRect = mGeckoChild->CocoaPointsToDevPixels(aRect);
   const NSRect *rects;
   NSInteger count;
-  [self getRectsBeingDrawn:&rects count:&count];
+  [mPixelHostingView getRectsBeingDrawn:&rects count:&count];
 
   if (count > MAX_RECTS_IN_REGION) {
     return boundingRect;
   }
 
   LayoutDeviceIntRegion region;
   for (NSInteger i = 0; i < count; ++i) {
     region.Or(region, mGeckoChild->CocoaPointsToDevPixels(rects[i]));
   }
   region.And(region, boundingRect);
   return region;
 }
 
 // The display system has told us that a portion of our view is dirty. Tell
 // gecko to paint it
-- (void)drawRect:(NSRect)aRect
-{
+// This method is called from mPixelHostingView's drawRect handler.
+- (void)doDrawRect:(NSRect)aRect {
   if (!NS_IsMainThread()) {
     // In the presence of CoreAnimation, this method can sometimes be called on
     // a non-main thread. Ignore those calls because Gecko can only react to
     // them on the main thread.
     return;
   }
 
   if (!mGeckoChild || !mGeckoChild->IsVisible())
@@ -3671,17 +3683,17 @@ NSEvent* gLastDragMouseDownEvent = nil;
 
   bool painted =
     mGeckoChild->PaintWindowInContext(cgContext, region, backingSize);
 
   // Undo the scale transform so that from now on the context is in
   // CocoaPoints again.
   CGContextRestoreGState(cgContext);
 
-  if (!painted && [self isOpaque]) {
+  if (!painted && [mPixelHostingView isOpaque]) {
     // Gecko refused to draw, but we've claimed to be opaque, so we have to
     // draw something--fill with white.
     CGContextSetRGBFillColor(cgContext, 1, 1, 1, 1);
     CGContextFillRect(cgContext, NSRectToCGRect(aRect));
   }
 
   if ([self isCoveringTitlebar]) {
     [self drawTitleString];
@@ -6568,16 +6580,36 @@ nsChildView::GetSelectionAsPlaintext(nsA
 #endif /* ACCESSIBILITY */
 
 + (uint32_t)sUniqueKeyEventId { return sUniqueKeyEventId; }
 
 + (NSMutableDictionary*)sNativeKeyEventsMap { return sNativeKeyEventsMap; }
 
 @end
 
+@implementation PixelHostingView
+
+- (BOOL)isFlipped {
+  return YES;
+}
+
+- (NSView*)hitTest:(NSPoint)aPoint {
+  return nil;
+}
+
+- (void)drawRect:(NSRect)aRect {
+  [(ChildView*)[self superview] doDrawRect:aRect];
+}
+
+- (BOOL)wantsBestResolutionOpenGLSurface {
+  return nsCocoaUtils::HiDPIEnabled() ? YES : NO;
+}
+
+@end
+
 #pragma mark -
 
 void
 ChildViewMouseTracker::OnDestroyView(ChildView* aView)
 {
   if (sLastMouseEventView == aView) {
     sLastMouseEventView = nil;
     [sLastMouseMoveEvent release];
