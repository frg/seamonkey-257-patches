# HG changeset patch
# User Andre Bargull <andre.bargull@gmail.com>
# Date 1531515617 25200
# Node ID 9999014fea3e359e0c50cec50e26840db6ca31c3
# Parent  99db4ddf926a4e66b58630df37cbb37a201b59de
Bug 1475678 - Part 4: Change size_t variables to uint32_t when the underlying type is int32/uint32. r=arai

diff --git a/js/src/builtin/Promise.cpp b/js/src/builtin/Promise.cpp
--- a/js/src/builtin/Promise.cpp
+++ b/js/src/builtin/Promise.cpp
@@ -426,24 +426,24 @@ enum ReactionRecordSlots {
   //   OnFulfilled or OnRejected handlers.
   ReactionRecordSlot_GeneratorOrPromiseToResolve,
 
   ReactionRecordSlots,
 };
 
 // ES2016, 25.4.1.2.
 class PromiseReactionRecord : public NativeObject {
-  static constexpr size_t REACTION_FLAG_RESOLVED = 0x1;
-  static constexpr size_t REACTION_FLAG_FULFILLED = 0x2;
-  static constexpr size_t REACTION_FLAG_DEFAULT_RESOLVING_HANDLER = 0x4;
-  static constexpr size_t REACTION_FLAG_ASYNC_FUNCTION = 0x8;
-  static constexpr size_t REACTION_FLAG_ASYNC_GENERATOR = 0x10;
-  static constexpr size_t REACTION_FLAG_DEBUGGER_DUMMY = 0x20;
-
-  void setFlagOnInitialState(size_t flag) {
+  static constexpr uint32_t REACTION_FLAG_RESOLVED = 0x1;
+  static constexpr uint32_t REACTION_FLAG_FULFILLED = 0x2;
+  static constexpr uint32_t REACTION_FLAG_DEFAULT_RESOLVING_HANDLER = 0x4;
+  static constexpr uint32_t REACTION_FLAG_ASYNC_FUNCTION = 0x8;
+  static constexpr uint32_t REACTION_FLAG_ASYNC_GENERATOR = 0x10;
+  static constexpr uint32_t REACTION_FLAG_DEBUGGER_DUMMY = 0x20;
+
+  void setFlagOnInitialState(uint32_t flag) {
     int32_t flags = this->flags();
     MOZ_ASSERT(flags == 0, "Can't modify with non-default flags");
     flags |= flag;
     setFixedSlot(ReactionRecordSlot_Flags, Int32Value(flags));
   }
 
  public:
   static const Class class_;
@@ -1187,22 +1187,22 @@ static MOZ_MUST_USE bool TriggerPromiseR
   RootedObject reactions(cx, &reactionsVal.toObject());
 
   if (reactions->is<PromiseReactionRecord>() || IsWrapper(reactions) ||
       JS_IsDeadWrapper(reactions)) {
     return EnqueuePromiseReactionJob(cx, reactions, valueOrReason, state);
   }
 
   HandleNativeObject reactionsList = reactions.as<NativeObject>();
-  size_t reactionsCount = reactionsList->getDenseInitializedLength();
+  uint32_t reactionsCount = reactionsList->getDenseInitializedLength();
   MOZ_ASSERT(reactionsCount > 1, "Reactions list should be created lazily");
 
   RootedObject reaction(cx);
   RootedValue reactionVal(cx);
-  for (size_t i = 0; i < reactionsCount; i++) {
+  for (uint32_t i = 0; i < reactionsCount; i++) {
     reactionVal = reactionsList->getDenseElement(i);
     MOZ_RELEASE_ASSERT(reactionVal.isObject());
     reaction = &reactionVal.toObject();
     if (!EnqueuePromiseReactionJob(cx, reaction, valueOrReason, state))
       return false;
   }
 
   return true;
@@ -1239,18 +1239,18 @@ static MOZ_MUST_USE bool DefaultResolvin
     if (!ok) {
       resolutionMode = RejectMode;
       if (!MaybeGetAndClearException(cx, &handlerResult))
         return false;
     }
   }
 
   // Steps 7-9.
-  size_t hookSlot = resolutionMode == RejectMode ? ReactionRecordSlot_Reject
-                                                 : ReactionRecordSlot_Resolve;
+  uint32_t hookSlot = resolutionMode == RejectMode ? ReactionRecordSlot_Reject
+                                                   : ReactionRecordSlot_Resolve;
   RootedObject callee(cx, reaction->getFixedSlot(hookSlot).toObjectOrNull());
   RootedObject promiseObj(cx, reaction->promise());
   if (!RunResolutionFunction(cx, callee, handlerResult, resolutionMode,
                              promiseObj))
     return false;
 
   rval.setUndefined();
   return true;
@@ -1431,18 +1431,18 @@ static bool PromiseReactionJob(JSContext
     args2[0].set(argument);
     if (!Call(cx, handlerVal, UndefinedHandleValue, args2, &handlerResult)) {
       resolutionMode = RejectMode;
       if (!MaybeGetAndClearException(cx, &handlerResult)) return false;
     }
   }
 
   // Steps 7-9.
-  size_t hookSlot = resolutionMode == RejectMode ? ReactionRecordSlot_Reject
-                                                 : ReactionRecordSlot_Resolve;
+  uint32_t hookSlot = resolutionMode == RejectMode ? ReactionRecordSlot_Reject
+                                                   : ReactionRecordSlot_Resolve;
   RootedObject callee(cx, reaction->getFixedSlot(hookSlot).toObjectOrNull());
   RootedObject promiseObj(cx, reaction->promise());
   if (!RunResolutionFunction(cx, callee, handlerResult, resolutionMode,
                              promiseObj))
     return false;
 
   args.rval().setUndefined();
   return true;
@@ -3779,19 +3779,19 @@ bool PromiseObject::dependentPromises(JS
 
     values[0].setObject(*promiseObj);
     return true;
   }
 
   uint32_t len = reactions->getDenseInitializedLength();
   MOZ_ASSERT(len >= 2);
 
-  size_t valuesIndex = 0;
+  uint32_t valuesIndex = 0;
   Rooted<PromiseReactionRecord*> reaction(cx);
-  for (size_t i = 0; i < len; i++) {
+  for (uint32_t i = 0; i < len; i++) {
     reaction =
         &reactions->getDenseElement(i).toObject().as<PromiseReactionRecord>();
 
     // Not all reactions have a Promise on them.
     RootedObject promiseObj(cx, reaction->promise());
     if (!promiseObj) continue;
     if (!values.growBy(1)) return false;
 
