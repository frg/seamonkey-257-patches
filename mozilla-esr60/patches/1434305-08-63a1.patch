# HG changeset patch
# User Tooru Fujisawa <arai_a@mac.com>
# Date 1532576177 -32400
# Node ID b67ea788a6f351a7c3c5347bc91c7c2a8741be77
# Parent  f171b6f94bdb61deb1262a1122127575270fb286
Bug 1434305 - Part 8: Support wrapping LazyScript in DebuggerScriptReferent. r=jimb

diff --git a/js/src/vm/Debugger.cpp b/js/src/vm/Debugger.cpp
--- a/js/src/vm/Debugger.cpp
+++ b/js/src/vm/Debugger.cpp
@@ -556,16 +556,17 @@ Debugger::Debugger(JSContext* cx, Native
       observedGCs(cx->zone()),
       allocationsLog(cx),
       trackingAllocationSites(false),
       allocationSamplingProbability(1.0),
       maxAllocationsLogLength(DEFAULT_MAX_LOG_LENGTH),
       allocationsLogOverflowed(false),
       frames(cx->zone()),
       scripts(cx),
+      lazyScripts(cx),
       sources(cx),
       objects(cx),
       environments(cx),
       wasmInstanceScripts(cx),
       wasmInstanceSources(cx),
 #ifdef NIGHTLY_BUILD
       traceLoggerLastDrainedSize(0),
       traceLoggerLastDrainedIteration(0),
@@ -601,18 +602,18 @@ Debugger::~Debugger() {
       cx->runtime()->onNewGlobalObjectWatchers().begin() ==
           JSRuntime::WatchersList::Iterator(this)) {
     cx->runtime()->onNewGlobalObjectWatchers().remove(this);
   }
 }
 
 bool Debugger::init(JSContext* cx) {
   if (!debuggees.init() || !debuggeeZones.init() || !frames.init() ||
-      !scripts.init() || !sources.init() || !objects.init() ||
-      !observedGCs.init() || !environments.init() ||
+      !scripts.init() || !lazyScripts.init() || !sources.init() ||
+      !objects.init() || !observedGCs.init() || !environments.init() ||
       !wasmInstanceScripts.init() || !wasmInstanceSources.init()) {
     ReportOutOfMemory(cx);
     return false;
   }
 
   cx->runtime()->debuggerList().insertBack(this);
   return true;
 }
@@ -2735,34 +2736,35 @@ void Debugger::removeAllocationsTracking
 }
 
 /*** Debugger JSObjects *****************************************************/
 
 void Debugger::traceCrossCompartmentEdges(JSTracer* trc) {
   objects.traceCrossCompartmentEdges<DebuggerObject_trace>(trc);
   environments.traceCrossCompartmentEdges<DebuggerEnv_trace>(trc);
   scripts.traceCrossCompartmentEdges<DebuggerScript_trace>(trc);
+  lazyScripts.traceCrossCompartmentEdges<DebuggerScript_trace>(trc);
   sources.traceCrossCompartmentEdges<DebuggerSource_trace>(trc);
   wasmInstanceScripts.traceCrossCompartmentEdges<DebuggerScript_trace>(trc);
   wasmInstanceSources.traceCrossCompartmentEdges<DebuggerSource_trace>(trc);
 }
 
 /*
  * Ordinarily, WeakMap keys and values are marked because at some point it was
  * discovered that the WeakMap was live; that is, some object containing the
  * WeakMap was marked during mark phase.
  *
  * However, during zone GC, we have to do something about cross-compartment
  * edges in non-GC'd compartments. Since the source may be live, we
  * conservatively assume it is and mark the edge.
  *
- * Each Debugger object keeps four cross-compartment WeakMaps: objects, scripts,
- * script source objects, and environments. They have the property that all
- * their values are in the same compartment as the Debugger object, but we have
- * to mark the keys and the private pointer in the wrapper object.
+ * Each Debugger object keeps five cross-compartment WeakMaps: objects, scripts,
+ * lazy scripts, script source objects, and environments. They have the property
+ * that all their values are in the same compartment as the Debugger object,
+ * but we have to mark the keys and the private pointer in the wrapper object.
  *
  * We must scan all Debugger objects regardless of whether they *currently* have
  * any debuggees in a compartment being GC'd, because the WeakMap entries
  * persist even when debuggees are removed.
  *
  * This happens during the initial mark phase, not iterative marking, because
  * all the edges being reported here are strong references.
  *
@@ -2931,16 +2933,19 @@ void Debugger::trace(JSTracer* trc) {
     }
   }
 
   allocationsLog.trace(trc);
 
   /* Trace the weak map from JSScript instances to Debugger.Script objects. */
   scripts.trace(trc);
 
+  // Trace the weak map from LazyScript instances to Debugger.Script objects.
+  lazyScripts.trace(trc);
+
   /* Trace the referent -> Debugger.Source weak map */
   sources.trace(trc);
 
   /* Trace the referent -> Debugger.Object weak map. */
   objects.trace(trc);
 
   /* Trace the referent -> Debugger.Environment weak map. */
   environments.trace(trc);
@@ -3006,16 +3011,17 @@ void Debugger::trace(JSTracer* trc) {
     } else {
       /*
        * For debugger cross compartment wrappers, add edges in the
        * opposite direction to those already added by
        * Compartment::findOutgoingEdges and above.  This ensure that
        * debuggers and their debuggees are finalized in the same group.
        */
       if (dbg->debuggeeZones.has(zone) || dbg->scripts.hasKeyInZone(zone) ||
+          dbg->lazyScripts.hasKeyInZone(zone) ||
           dbg->sources.hasKeyInZone(zone) || dbg->objects.hasKeyInZone(zone) ||
           dbg->environments.hasKeyInZone(zone) ||
           dbg->wasmInstanceScripts.hasKeyInZone(zone) ||
           dbg->wasmInstanceSources.hasKeyInZone(zone)) {
         finder.addEdgeTo(debuggerZone);
       }
     }
   }
@@ -4816,16 +4822,21 @@ void DebuggerScript_trace(JSTracer* trc,
   /* This comes from a private pointer, so no barrier needed. */
   gc::Cell* cell = GetScriptReferentCell(obj);
   if (cell) {
     if (cell->is<JSScript>()) {
       JSScript* script = cell->as<JSScript>();
       TraceManuallyBarrieredCrossCompartmentEdge(
           trc, obj, &script, "Debugger.Script script referent");
       obj->as<NativeObject>().setPrivateUnbarriered(script);
+    } else if (cell->is<LazyScript>()) {
+      LazyScript* lazyScript = cell->as<LazyScript>();
+      TraceManuallyBarrieredCrossCompartmentEdge(
+          trc, obj, &lazyScript, "Debugger.Script lazy script referent");
+      obj->as<NativeObject>().setPrivateUnbarriered(lazyScript);
     } else {
       JSObject* wasm = cell->as<JSObject>();
       TraceManuallyBarrieredCrossCompartmentEdge(
           trc, obj, &wasm, "Debugger.Script wasm referent");
       MOZ_ASSERT(wasm->is<WasmInstanceObject>());
       obj->as<NativeObject>().setPrivateUnbarriered(wasm);
     }
   }
@@ -4917,20 +4928,54 @@ JSObject* Debugger::wrapVariantReferent(
   return p->value();
 }
 
 JSObject* Debugger::wrapVariantReferent(
     JSContext* cx, Handle<DebuggerScriptReferent> referent) {
   JSObject* obj;
   if (referent.is<JSScript*>()) {
     Handle<JSScript*> untaggedReferent = referent.template as<JSScript*>();
+    if (untaggedReferent->maybeLazyScript()) {
+      // If the JSScript has corresponding LazyScript, wrap the LazyScript
+      // instead.
+      //
+      // This is necessary for Debugger.Script identity.  If we use both
+      // JSScript and LazyScript for same single script, those 2 wrapped
+      // scripts become not identical, while the referent script is
+      // actually identical.
+      //
+      // If a script has corresponding LazyScript and JSScript, the
+      // lifetime of the LazyScript is always longer than the JSScript.
+      // So we can use the LazyScript as a proxy for the JSScript.
+      Rooted<LazyScript*> lazyScript(cx, untaggedReferent->maybeLazyScript());
+      Rooted<DebuggerScriptReferent> lazyScriptReferent(cx, lazyScript.get());
+
+      Rooted<CrossCompartmentKey> key(cx,
+                                      CrossCompartmentKey(object, lazyScript));
+      obj = wrapVariantReferent<DebuggerScriptReferent, LazyScript*,
+                                LazyScriptWeakMap>(cx, lazyScripts, key,
+                                                   lazyScriptReferent);
+      MOZ_ASSERT_IF(obj, GetScriptReferent(obj) == lazyScriptReferent);
+      return obj;
+    } else {
+      // If the JSScript doesn't have corresponding LazyScript, the script
+      // is not lazifiable, and we can safely use JSScript as referent.
+      Rooted<CrossCompartmentKey> key(
+          cx, CrossCompartmentKey(object, untaggedReferent));
+      obj =
+          wrapVariantReferent<DebuggerScriptReferent, JSScript*, ScriptWeakMap>(
+              cx, scripts, key, referent);
+    }
+  } else if (referent.is<LazyScript*>()) {
+    Handle<LazyScript*> untaggedReferent = referent.template as<LazyScript*>();
     Rooted<CrossCompartmentKey> key(
         cx, CrossCompartmentKey(object, untaggedReferent));
-    obj = wrapVariantReferent<DebuggerScriptReferent, JSScript*, ScriptWeakMap>(
-        cx, scripts, key, referent);
+    obj =
+        wrapVariantReferent<DebuggerScriptReferent, LazyScript*,
+                            LazyScriptWeakMap>(cx, lazyScripts, key, referent);
   } else {
     Handle<WasmInstanceObject*> untaggedReferent =
         referent.template as<WasmInstanceObject*>();
     Rooted<CrossCompartmentKey> key(
         cx, CrossCompartmentKey(
                 object, untaggedReferent,
                 CrossCompartmentKey::DebuggerObjectKind::DebuggerWasmScript));
     obj = wrapVariantReferent<DebuggerScriptReferent, WasmInstanceObject*,
@@ -4941,16 +4986,22 @@ JSObject* Debugger::wrapVariantReferent(
   return obj;
 }
 
 JSObject* Debugger::wrapScript(JSContext* cx, HandleScript script) {
   Rooted<DebuggerScriptReferent> referent(cx, script.get());
   return wrapVariantReferent(cx, referent);
 }
 
+JSObject* Debugger::wrapLazyScript(JSContext* cx,
+                                   Handle<LazyScript*> lazyScript) {
+  Rooted<DebuggerScriptReferent> referent(cx, lazyScript.get());
+  return wrapVariantReferent(cx, referent);
+}
+
 JSObject* Debugger::wrapWasmScript(JSContext* cx,
                                    Handle<WasmInstanceObject*> wasmInstance) {
   Rooted<DebuggerScriptReferent> referent(cx, wasmInstance.get());
   return wrapVariantReferent(cx, referent);
 }
 
 static JSObject* DebuggerScript_check(JSContext* cx, const Value& v,
                                       const char* fnname) {
diff --git a/js/src/vm/Debugger.h b/js/src/vm/Debugger.h
--- a/js/src/vm/Debugger.h
+++ b/js/src/vm/Debugger.h
@@ -520,16 +520,19 @@ class Debugger : private mozilla::Linked
                   DefaultHasher<AbstractFramePtr>, ZoneAllocPolicy>
       FrameMap;
   FrameMap frames;
 
   /* An ephemeral map from JSScript* to Debugger.Script instances. */
   typedef DebuggerWeakMap<JSScript*> ScriptWeakMap;
   ScriptWeakMap scripts;
 
+  using LazyScriptWeakMap = DebuggerWeakMap<LazyScript*>;
+  LazyScriptWeakMap lazyScripts;
+
   /* The map from debuggee source script objects to their Debugger.Source
    * instances. */
   typedef DebuggerWeakMap<JSObject*, true> SourceWeakMap;
   SourceWeakMap sources;
 
   /* The map from debuggee objects to their Debugger.Object instances. */
   typedef DebuggerWeakMap<JSObject*> ObjectWeakMap;
   ObjectWeakMap objects;
@@ -1143,16 +1146,18 @@ class Debugger : private mozilla::Linked
 
   /*
    * Return the Debugger.Script object for |script|, or create a new one if
    * needed. The context |cx| must be in the debugger realm; |script| must be
    * a script in a debuggee realm.
    */
   JSObject* wrapScript(JSContext* cx, HandleScript script);
 
+  JSObject* wrapLazyScript(JSContext* cx, Handle<LazyScript*> script);
+
   /*
    * Return the Debugger.Script object for |wasmInstance| (the toplevel
    * script), synthesizing a new one if needed. The context |cx| must be in
    * the debugger compartment; |wasmInstance| must be a WasmInstanceObject in
    * the debuggee realm.
    */
   JSObject* wrapWasmScript(JSContext* cx,
                            Handle<WasmInstanceObject*> wasmInstance);
