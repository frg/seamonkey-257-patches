# HG changeset patch
# User Luke Wagner <luke@mozilla.com>
# Date 1522426612 18000
# Node ID fbcb35097910b0520521316da5f1219faf8aced7
# Parent  494733ef752b0015b1b7414f14dbaacd8807308a
Bug 1449189 - Baldr: remove ExclusiveData indirection (r=bbouvier)

diff --git a/js/src/vm/Initialization.cpp b/js/src/vm/Initialization.cpp
--- a/js/src/vm/Initialization.cpp
+++ b/js/src/vm/Initialization.cpp
@@ -97,18 +97,16 @@ JS_PUBLIC_API const char* JS::detail::In
 #if defined(DEBUG) || defined(JS_OOM_BREAKPOINT)
   RETURN_IF_FAIL(js::oom::InitThreadType());
 #endif
 
   js::InitMallocAllocator();
 
   RETURN_IF_FAIL(js::Mutex::Init());
 
-  RETURN_IF_FAIL(js::wasm::Init());
-
   js::gc::InitMemorySubsystem();  // Ensure gc::SystemPageSize() works.
 
   RETURN_IF_FAIL(js::jit::InitProcessExecutableMemory());
 
   RETURN_IF_FAIL(js::MemoryProtectionExceptionHandler::install());
 
   RETURN_IF_FAIL(js::jit::InitializeIon());
 
diff --git a/js/src/wasm/WasmInstance.cpp b/js/src/wasm/WasmInstance.cpp
--- a/js/src/wasm/WasmInstance.cpp
+++ b/js/src/wasm/WasmInstance.cpp
@@ -79,29 +79,17 @@ class SigIdSet {
     p->value()--;
     if (!p->value()) {
       js_delete(p->key());
       map_.remove(p);
     }
   }
 };
 
-ExclusiveData<SigIdSet>* sigIdSet = nullptr;
-
-bool js::wasm::InitSignatureSet() {
-  MOZ_ASSERT(!sigIdSet);
-  sigIdSet = js_new<ExclusiveData<SigIdSet>>(mutexid::WasmSigIdSet);
-  return sigIdSet != nullptr;
-}
-
-void js::wasm::ReleaseSignatureSet() {
-  MOZ_ASSERT(sigIdSet);
-  js_delete(sigIdSet);
-  sigIdSet = nullptr;
-}
+ExclusiveData<SigIdSet> sigIdSet(mutexid::WasmSigIdSet);
 
 const void** Instance::addressOfSigId(const SigIdDesc& sigId) const {
   return (const void**)(globalData() + sigId.globalDataOffset());
 }
 
 FuncImportTls& Instance::funcImportTls(const FuncImport& fi) {
   return *(FuncImportTls*)(globalData() + fi.tlsDataOffset());
 }
@@ -512,17 +500,17 @@ bool Instance::init(JSContext* cx) {
     return false;
 
   for (const SharedTable& table : tables_) {
     if (table->movingGrowable() && !table->addMovingGrowObserver(cx, object_))
       return false;
   }
 
   if (!metadata().sigIds.empty()) {
-    ExclusiveData<SigIdSet>::Guard lockedSigIdSet = sigIdSet->lock();
+    ExclusiveData<SigIdSet>::Guard lockedSigIdSet = sigIdSet.lock();
 
     if (!lockedSigIdSet->ensureInitialized(cx)) return false;
 
     for (const SigWithId& sig : metadata().sigIds) {
       const void* sigId;
       if (!lockedSigIdSet->allocateSigId(cx, sig, &sigId)) return false;
 
       *addressOfSigId(sig.id) = sigId;
@@ -544,17 +532,17 @@ Instance::~Instance() {
 
   for (unsigned i = 0; i < funcImports.length(); i++) {
     FuncImportTls& import = funcImportTls(funcImports[i]);
     if (import.baselineScript)
       import.baselineScript->removeDependentWasmImport(*this, i);
   }
 
   if (!metadata().sigIds.empty()) {
-    ExclusiveData<SigIdSet>::Guard lockedSigIdSet = sigIdSet->lock();
+    ExclusiveData<SigIdSet>::Guard lockedSigIdSet = sigIdSet.lock();
 
     for (const SigWithId& sig : metadata().sigIds) {
       if (const void* sigId = *addressOfSigId(sig.id))
         lockedSigIdSet->deallocateSigId(sig, sigId);
     }
   }
 }
 
diff --git a/js/src/wasm/WasmInstance.h b/js/src/wasm/WasmInstance.h
--- a/js/src/wasm/WasmInstance.h
+++ b/js/src/wasm/WasmInstance.h
@@ -162,15 +162,12 @@ class Instance {
                           int32_t value, int64_t timeout);
   static int32_t wait_i64(Instance* instance, uint32_t byteOffset,
                           int64_t value, int64_t timeout);
   static int32_t wake(Instance* instance, uint32_t byteOffset, int32_t count);
 };
 
 typedef UniquePtr<Instance> UniqueInstance;
 
-bool InitSignatureSet();
-void ReleaseSignatureSet();
-
 }  // namespace wasm
 }  // namespace js
 
 #endif  // wasm_instance_h
diff --git a/js/src/wasm/WasmProcess.cpp b/js/src/wasm/WasmProcess.cpp
--- a/js/src/wasm/WasmProcess.cpp
+++ b/js/src/wasm/WasmProcess.cpp
@@ -232,21 +232,18 @@ const CodeSegment* wasm::LookupCodeSegme
 }
 
 const Code* wasm::LookupCode(const void* pc,
                              const CodeRange** cr /* = nullptr */) {
   const CodeSegment* found = LookupCodeSegment(pc, cr);
   return found ? &found->code() : nullptr;
 }
 
-bool wasm::Init() { return InitSignatureSet(); }
-
 void wasm::ShutDown() {
   // If there are live runtimes then we are already pretty much leaking the
   // world, so to avoid spurious assertions (which are valid and valuable when
   // there are not live JSRuntimes), don't bother releasing anything here.
   if (JSRuntime::hasLiveRuntimes())
     return;
 
-  ReleaseSignatureSet();
   ReleaseBuiltinThunks();
   processCodeSegmentMap.freeAll();
 }
diff --git a/js/src/wasm/WasmProcess.h b/js/src/wasm/WasmProcess.h
--- a/js/src/wasm/WasmProcess.h
+++ b/js/src/wasm/WasmProcess.h
@@ -47,16 +47,14 @@ extern mozilla::Atomic<bool> CodeExists;
 
 bool RegisterCodeSegment(const CodeSegment* cs);
 
 void UnregisterCodeSegment(const CodeSegment* cs);
 
 // Called once before/after the last VM execution which could execute or compile
 // wasm.
 
-bool Init();
-
 void ShutDown();
 
 }  // namespace wasm
 }  // namespace js
 
 #endif  // wasm_process_h
