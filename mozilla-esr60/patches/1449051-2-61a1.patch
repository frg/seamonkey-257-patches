# HG changeset patch
# User Jeff Walden <jwalden@mit.edu>
# Date 1522195988 25200
# Node ID 92d1872f4a238b7f1bdfb0cc92432928bf619af0
# Parent  49c15c0d134fe8ea7d93bbb0ee734fbf0ae7c1cc
Bug 1449051 - Further minifications to JS::Value's internal structure.  r=jandem

diff --git a/js/public/Value.h b/js/public/Value.h
--- a/js/public/Value.h
+++ b/js/public/Value.h
@@ -786,38 +786,26 @@ class MOZ_NON_PARAM alignas(8) Value {
     MOZ_ASSERT((((uintptr_t)cell) >> JSVAL_TAG_SHIFT) == 0);
 #endif
     data.asBits =
         bitsFromTagAndPayload(JSVAL_TAG_PRIVATE_GCTHING, PayloadType(cell));
   }
 
   bool isPrivateGCThing() const { return toTag() == JSVAL_TAG_PRIVATE_GCTHING; }
 
-  const uintptr_t* payloadUIntPtr() const {
-#if defined(JS_NUNBOX32)
-    return &data.s.payload.uintptr;
-#elif defined(JS_PUNBOX64)
-    return &data.asUIntPtr;
-#endif
-  }
-
 #if !defined(_MSC_VER) && !defined(__sparc)
   // Value must be POD so that MSVC will pass it by value and not in memory
   // (bug 689101); the same is true for SPARC as well (bug 737344).  More
   // precisely, we don't want Value return values compiled as out params.
  private:
 #endif
 
   union layout {
     uint64_t asBits;
     double asDouble;
-    void* asPtr;
-#if defined(JS_PUNBOX64)
-    uintptr_t asUIntPtr;
-#endif  // defined(JS_PUNBOX64)
 
 #if defined(JS_PUNBOX64) && !defined(_WIN64)
     /* MSVC does not pack these correctly :-( */
     struct {
 #if MOZ_LITTLE_ENDIAN
       uint64_t   payload47 : 47;
       JSValueTag tag : 17;
 #else
@@ -846,17 +834,16 @@ class MOZ_NON_PARAM alignas(8) Value {
         uint32_t u32;
         uint32_t boo;  // Don't use |bool| -- it must be four bytes.
         JSString* str;
         JS::Symbol* sym;
         JSObject* obj;
         js::gc::Cell* cell;
         void* ptr;
         JSWhyMagic why;
-        uintptr_t uintptr;
       } payload;
 #if MOZ_LITTLE_ENDIAN
       JSValueTag tag;
 #endif  // MOZ_LITTLE_ENDIAN
 #endif  // defined(JS_PUNBOX64)
     } s;
 
     layout() : asBits(JSVAL_RAW64_UNDEFINED) {}
diff --git a/js/src/jit/JitFrames.cpp b/js/src/jit/JitFrames.cpp
--- a/js/src/jit/JitFrames.cpp
+++ b/js/src/jit/JitFrames.cpp
@@ -901,17 +901,17 @@ static void TraceIonJSFrame(JSTracer* tr
     JSValueTag tag = JSValueTag(ReadAllocation(frame, &type));
     uintptr_t rawPayload = ReadAllocation(frame, &payload);
 
     Value v = Value::fromTagAndPayload(tag, rawPayload);
     TraceRoot(trc, &v, "ion-torn-value");
 
     if (v != Value::fromTagAndPayload(tag, rawPayload)) {
       // GC moved the value, replace the stored payload.
-      rawPayload = *v.payloadUIntPtr();
+      rawPayload = v.toNunboxPayload();
       WriteAllocation(frame, &payload, rawPayload);
     }
   }
 #endif
 }
 
 static void TraceBailoutFrame(JSTracer* trc, const JSJitFrameIter& frame) {
   JitFrameLayout* layout = (JitFrameLayout*)frame.fp();
