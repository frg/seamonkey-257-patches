# HG changeset patch
# User David Keeler <dkeeler@mozilla.com>
# Date 1532114889 0
# Node ID 7635ef4a997ddfac1a33604bb799af8d71057847
# Parent  58f093907563875f45a7532b0d628e7e14d51660
bug 1300420 - add enterprise root support for OS X r=spohl,franziskus

If the preference security.enterprise_roots.enabled is set to true, the platform will import trusted TLS certificates from the OS X keystore.

Differential Revision: https://phabricator.services.mozilla.com/D2169

diff --git a/security/manager/ssl/EnterpriseRoots.cpp b/security/manager/ssl/EnterpriseRoots.cpp
--- a/security/manager/ssl/EnterpriseRoots.cpp
+++ b/security/manager/ssl/EnterpriseRoots.cpp
@@ -1,24 +1,29 @@
 /* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
  *
  * This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "EnterpriseRoots.h"
 
+#include "mozilla/ArrayUtils.h"
 #include "mozilla/Logging.h"
+#include "mozilla/Unused.h"
+
+#ifdef XP_MACOSX
+#include <Security/Security.h>
+#endif  // XP_MACOSX
 
 extern LazyLogModule gPIPNSSLog;
 
 using namespace mozilla;
 
 #ifdef XP_WIN
-
 const wchar_t* kWindowsDefaultRootStoreName = L"ROOT";
 NS_NAMED_LITERAL_CSTRING(kMicrosoftFamilySafetyCN, "Microsoft Family Safety");
 
 // Helper function to determine if the OS considers the given certificate to be
 // a trust anchor for TLS server auth certificates. This is to be used in the
 // context of importing what are presumed to be root certificates from the OS.
 // If this function returns true but it turns out that the given certificate is
 // in some way unsuitable to issue certificates, mozilla::pkix will never build
@@ -105,16 +110,20 @@ static void GatherEnterpriseRootsForLoca
                  locationFlag == CERT_SYSTEM_STORE_LOCAL_MACHINE_GROUP_POLICY ||
                  locationFlag == CERT_SYSTEM_STORE_LOCAL_MACHINE_ENTERPRISE,
              "unexpected locationFlag for GatherEnterpriseRootsForLocation");
   if (!(locationFlag == CERT_SYSTEM_STORE_LOCAL_MACHINE ||
         locationFlag == CERT_SYSTEM_STORE_LOCAL_MACHINE_GROUP_POLICY ||
         locationFlag == CERT_SYSTEM_STORE_LOCAL_MACHINE_ENTERPRISE)) {
     return;
   }
+  MOZ_ASSERT(roots, "roots unexpectedly NULL?");
+  if (!roots) {
+    return;
+  }
 
   DWORD flags =
       locationFlag | CERT_STORE_OPEN_EXISTING_FLAG | CERT_STORE_READONLY_FLAG;
   // The certificate store being opened should consist only of certificates
   // added by a user or administrator and not any certificates that are part
   // of Microsoft's root store program.
   // The 3rd parameter to CertOpenStore should be NULL according to
   // https://msdn.microsoft.com/en-us/library/windows/desktop/aa376559%28v=vs.85%29.aspx
@@ -144,20 +153,16 @@ static void GatherEnterpriseRootsForLoca
     // Don't import the Microsoft Family Safety root (this prevents the
     // Enterprise Roots feature from interacting poorly with the Family
     // Safety support).
     UniquePORTString subjectName(CERT_GetCommonName(&nssCertificate->subject));
     if (kMicrosoftFamilySafetyCN.Equals(subjectName.get())) {
       MOZ_LOG(gPIPNSSLog, LogLevel::Debug, ("skipping Family Safety Root"));
       continue;
     }
-    MOZ_ASSERT(roots, "roots unexpectedly NULL?");
-    if (!roots) {
-      return;
-    }
     if (CERT_AddCertToListTail(roots.get(), nssCertificate.get()) !=
         SECSuccess) {
       MOZ_LOG(gPIPNSSLog, LogLevel::Debug, ("couldn't add cert to list"));
       continue;
     }
     MOZ_LOG(gPIPNSSLog, LogLevel::Debug, ("Imported '%s'", subjectName.get()));
     numImported++;
     // now owned by roots
@@ -170,19 +175,117 @@ static void GatherEnterpriseRootsWindows
   GatherEnterpriseRootsForLocation(CERT_SYSTEM_STORE_LOCAL_MACHINE, roots);
   GatherEnterpriseRootsForLocation(CERT_SYSTEM_STORE_LOCAL_MACHINE_GROUP_POLICY,
                                    roots);
   GatherEnterpriseRootsForLocation(CERT_SYSTEM_STORE_LOCAL_MACHINE_ENTERPRISE,
                                    roots);
 }
 #endif  // XP_WIN
 
+#ifdef XP_MACOSX
+template<typename T>
+class ScopedCFType {
+public:
+  explicit ScopedCFType(T value)
+  : mValue(value)
+  {
+  }
+
+  ~ScopedCFType() { CFRelease((CFTypeRef)mValue); }
+
+  T get() { return mValue; }
+
+private:
+  T mValue;
+};
+
+OSStatus GatherEnterpriseRootsOSX(UniqueCERTCertList& roots) {
+  MOZ_ASSERT(roots, "roots unexpectedly NULL?");
+  if (!roots) {
+    return errSecBadReq;
+  }
+  // The following builds a search dictionary corresponding to:
+  // { class: "certificate",
+  //   match limit: "match all",
+  //   policy: "SSL (TLS)",
+  //   only include trusted certificates: true }
+  // This operates on items that have been added to the keychain and thus gives
+  // us all 3rd party certificates that have been trusted for SSL (TLS), which
+  // is what we want (thus we don't import built-in root certificates that ship
+  // with the OS).
+  const CFStringRef keys[] = {kSecClass, kSecMatchLimit, kSecMatchPolicy,
+                              kSecMatchTrustedOnly};
+  // https://developer.apple.com/documentation/security/1392592-secpolicycreatessl
+  ScopedCFType<SecPolicyRef> sslPolicy(SecPolicyCreateSSL(true, nullptr));
+  const void* values[] = {kSecClassCertificate, kSecMatchLimitAll,
+                          sslPolicy.get(), kCFBooleanTrue};
+  static_assert(ArrayLength(keys) == ArrayLength(values),
+                "mismatched SecItemCopyMatching key/value array sizes");
+  // https://developer.apple.com/documentation/corefoundation/1516782-cfdictionarycreate
+  ScopedCFType<CFDictionaryRef> searchDictionary(CFDictionaryCreate(
+      nullptr, (const void**)&keys, (const void**)&values, ArrayLength(keys),
+      &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks));
+  CFTypeRef items;
+  // https://developer.apple.com/documentation/security/1398306-secitemcopymatching
+  OSStatus rv = SecItemCopyMatching(searchDictionary.get(), &items);
+  if (rv != errSecSuccess) {
+    MOZ_LOG(gPIPNSSLog, LogLevel::Debug, ("SecItemCopyMatching failed"));
+    return rv;
+  }
+  // If given a match limit greater than 1 (which we did), SecItemCopyMatching
+  // returns a CFArrayRef.
+  ScopedCFType<CFArrayRef> arr(reinterpret_cast<CFArrayRef>(items));
+  CFIndex count = CFArrayGetCount(arr.get());
+  uint32_t numImported = 0;
+  for (CFIndex i = 0; i < count; i++) {
+    const CFTypeRef c = CFArrayGetValueAtIndex(arr.get(), i);
+    // Because we asked for certificates, each CFTypeRef in the array is really
+    // a SecCertificateRef.
+    const SecCertificateRef s = (const SecCertificateRef)c;
+    ScopedCFType<CFDataRef> der(SecCertificateCopyData(s));
+    const unsigned char* ptr = CFDataGetBytePtr(der.get());
+    unsigned int len = CFDataGetLength(der.get());
+    SECItem derItem = {
+        siBuffer,
+        const_cast<unsigned char*>(ptr),
+        len,
+    };
+    UniqueCERTCertificate cert(
+        CERT_NewTempCertificate(CERT_GetDefaultCertDB(), &derItem,
+                                nullptr,  // nickname unnecessary
+                                false,    // not permanent
+                                true));   // copy DER
+    if (!cert) {
+      MOZ_LOG(gPIPNSSLog, LogLevel::Debug,
+              ("couldn't decode 3rd party root certificate"));
+      continue;
+    }
+    UniquePORTString subjectName(CERT_GetCommonName(&cert->subject));
+    if (CERT_AddCertToListTail(roots.get(), cert.get()) != SECSuccess) {
+      MOZ_LOG(gPIPNSSLog, LogLevel::Debug, ("couldn't add cert to list"));
+      continue;
+    }
+    numImported++;
+    MOZ_LOG(gPIPNSSLog, LogLevel::Debug, ("Imported '%s'", subjectName.get()));
+    mozilla::Unused << cert.release();  // owned by roots
+  }
+  MOZ_LOG(gPIPNSSLog, LogLevel::Debug, ("imported %u roots", numImported));
+  return errSecSuccess;
+}
+#endif  // XP_MACOSX
+
 nsresult GatherEnterpriseRoots(UniqueCERTCertList& result) {
   UniqueCERTCertList roots(CERT_NewCertList());
   if (!roots) {
     return NS_ERROR_OUT_OF_MEMORY;
   }
 #ifdef XP_WIN
   GatherEnterpriseRootsWindows(roots);
 #endif  // XP_WIN
+#ifdef XP_MACOSX
+  OSStatus rv = GatherEnterpriseRootsOSX(roots);
+  if (rv != errSecSuccess) {
+    return NS_ERROR_FAILURE;
+  }
+#endif  // XP_MACOSX
   result = std::move(roots);
   return NS_OK;
 }
diff --git a/security/manager/ssl/moz.build b/security/manager/ssl/moz.build
--- a/security/manager/ssl/moz.build
+++ b/security/manager/ssl/moz.build
@@ -135,16 +135,21 @@ UNIFIED_SOURCES += [
     'PublicKeyPinningService.cpp',
     'RootCertificateTelemetryUtils.cpp',
     'SecretDecoderRing.cpp',
     'SharedSSLState.cpp',
     'SSLServerCertVerification.cpp',
     'TransportSecurityInfo.cpp',
 ]
 
+if CONFIG['OS_ARCH'] == 'Darwin':
+    OS_LIBS += [
+        '-framework Security'
+    ]
+
 IPDL_SOURCES += [
     'PPSMContentDownloader.ipdl',
 ]
 
 if CONFIG['MOZ_XUL']:
     UNIFIED_SOURCES += [
         'nsCertTree.cpp',
     ]
diff --git a/security/manager/ssl/nsNSSComponent.cpp b/security/manager/ssl/nsNSSComponent.cpp
--- a/security/manager/ssl/nsNSSComponent.cpp
+++ b/security/manager/ssl/nsNSSComponent.cpp
@@ -1843,22 +1843,20 @@ nsresult nsNSSComponent::InitializeNSS()
                            mContentSigningRootHash);
 
     mMitmCanaryIssuer.Truncate();
     Preferences::GetString("security.pki.mitm_canary_issuer",
                            mMitmCanaryIssuer);
     mMitmDetecionEnabled =
         Preferences::GetBool("security.pki.mitm_canary_issuer.enabled", true);
 
-#ifdef XP_WIN
     nsCOMPtr<nsINSSComponent> handle(this);
     NS_DispatchToCurrentThread(NS_NewRunnableFunction(
         "nsNSSComponent::TrustLoaded3rdPartyRoots",
         [handle]() { MOZ_ALWAYS_SUCCEEDS(handle->TrustLoaded3rdPartyRoots()); }));
-#endif  // XP_WIN
 
     // Set dynamic options from prefs. This has to run after
     // SSL_ConfigServerSessionIDCache.
     setValidationOptions(true, lock);
 
     RefPtr<LoadLoadableRootsTask> loadLoadableRootsTask(
       new LoadLoadableRootsTask(this));
     rv = loadLoadableRootsTask->Dispatch();
diff --git a/security/manager/ssl/tests/unit/xpcshell.ini b/security/manager/ssl/tests/unit/xpcshell.ini
--- a/security/manager/ssl/tests/unit/xpcshell.ini
+++ b/security/manager/ssl/tests/unit/xpcshell.ini
@@ -77,17 +77,19 @@ skip-if = !debug
 run-sequentially = hardcoded ports
 [test_datasignatureverifier.js]
 # Android always has and always will use the new format, so
 # this test doesn't apply.
 [test_db_format_pref_new.js]
 skip-if = toolkit == 'android'
 [test_der.js]
 [test_enterprise_roots.js]
-skip-if = os != 'win' # tests a Windows-specific feature
+# This feature is implemented for Windows and OS X. However, we don't currently
+# have a way to test it on OS X.
+skip-if = os != 'win'
 [test_ev_certs.js]
 tags = blocklist
 run-sequentially = hardcoded ports
 [test_forget_about_site_security_headers.js]
 skip-if = toolkit == 'android'
 [test_hash_algorithms.js]
 [test_hash_algorithms_wrap.js]
 # bug 1124289 - run_test_in_child violates the sandbox on android
