# HG changeset patch
# User Andre Bargull <andre.bargull@gmail.com>
# Date 1532070950 25200
# Node ID 9d5851d5a423b48c30a4242283c0048980fbb036
# Parent  7e4f39b5046038b9bb41a62036b80ffcac6877ee
Bug 1475678 - Part 11: Add cache for Promise property lookups. r=arai

diff --git a/js/src/builtin/Promise.cpp b/js/src/builtin/Promise.cpp
--- a/js/src/builtin/Promise.cpp
+++ b/js/src/builtin/Promise.cpp
@@ -725,16 +725,17 @@ static bool Promise_then(JSContext* cx, 
 static bool Promise_then_impl(JSContext* cx, HandleValue promiseVal,
                               HandleValue onFulfilled, HandleValue onRejected,
                               MutableHandleValue rval, bool rvalUsed);
 
 // ES2016, 25.4.1.3.2, steps 6-13.
 static MOZ_MUST_USE bool ResolvePromiseInternal(JSContext* cx,
                                                 HandleObject promise,
                                                 HandleValue resolutionVal) {
+  assertSameCompartment(cx, promise, resolutionVal);
   MOZ_ASSERT(!IsSettledMaybeWrappedPromise(promise));
 
   // Step 7 (reordered).
   if (!resolutionVal.isObject())
     return FulfillMaybeWrappedPromise(cx, promise, resolutionVal);
 
   RootedObject resolution(cx, &resolutionVal.toObject());
 
@@ -774,30 +775,25 @@ static MOZ_MUST_USE bool ResolvePromiseI
   }
 
   // Step 10 (implicit).
 
   // Step 11.
   if (!IsCallable(thenVal))
     return FulfillMaybeWrappedPromise(cx, promise, resolutionVal);
 
-  // If the resolution object is a built-in Promise object, possibly from a
-  // different realm in the same compartment, and the `then` property is the
-  // original Promise.prototype.then function from the current realm, we
-  // skip storing/calling it.
-  // And additionally require that |promise| itself is also a built-in
-  // Promise object from the same compartment, so the fast path doesn't need
-  // to cope with wrappers.
+  // If the resolution object is a built-in Promise object and the
+  // `then` property is the original Promise.prototype.then function
+  // from the current realm, we skip storing/calling it.
+  // Additionally we require that |promise| itself is also a built-in
+  // Promise object, so the fast path doesn't need to cope with wrappers.
   bool isBuiltinThen = false;
-  if (resolution->is<PromiseObject>() &&
-      resolution->as<PromiseObject>().compartment() == cx->compartment() &&
+  if (resolution->is<PromiseObject>() && promise->is<PromiseObject>() &&
       IsNativeFunction(thenVal, Promise_then) &&
-      thenVal.toObject().as<JSFunction>().realm() == cx->realm() &&
-      promise->is<PromiseObject>() &&
-      promise->as<PromiseObject>().compartment() == cx->compartment()) {
+      thenVal.toObject().as<JSFunction>().realm() == cx->realm()) {
     isBuiltinThen = true;
   }
 
   // Step 12.
   if (!isBuiltinThen) {
     RootedValue promiseVal(cx, ObjectValue(*promise));
     if (!EnqueuePromiseResolveThenableJob(cx, promiseVal, resolutionVal,
                                           thenVal))
@@ -2009,18 +2005,28 @@ static bool PromiseConstructor(JSContext
 
 // ES2016, 25.4.3.1. skipping creation of resolution functions and executor
 // function invocation.
 /* static */ PromiseObject* PromiseObject::createSkippingExecutor(
     JSContext* cx) {
   return CreatePromiseObjectWithoutResolutionFunctions(cx);
 }
 
+class MOZ_STACK_CLASS PromiseForOfIterator : public JS::ForOfIterator {
+ public:
+  using JS::ForOfIterator::ForOfIterator;
+
+  bool isOptimizedDenseArrayIteration() {
+    MOZ_ASSERT(valueIsIterable());
+    return index != NOT_ARRAY && IsPackedArray(iterator);
+  }
+};
+
 static MOZ_MUST_USE bool PerformPromiseAll(
-    JSContext *cx, JS::ForOfIterator& iterator, HandleObject C,
+    JSContext *cx, PromiseForOfIterator& iterator, HandleObject C,
     Handle<PromiseCapability> resultCapability, bool* done);
 
 // ES2016, 25.4.4.1.
 static bool Promise_static_all(JSContext* cx, unsigned argc, Value* vp) {
   CallArgs args = CallArgsFromVp(argc, vp);
   HandleValue iterable = args.get(0);
 
   // Step 2 (reordered).
@@ -2036,17 +2042,17 @@ static bool Promise_static_all(JSContext
   RootedObject C(cx, &CVal.toObject());
 
   // Step 3.
   Rooted<PromiseCapability> promiseCapability(cx);
   if (!NewPromiseCapability(cx, C, &promiseCapability, false))
     return false;
 
   // Steps 4-5.
-  JS::ForOfIterator iter(cx);
+  PromiseForOfIterator iter(cx);
   if (!iter.init(iterable, JS::ForOfIterator::AllowNonIterable))
     return AbruptRejectPromise(cx, args, promiseCapability);
 
   if (!iter.valueIsIterable()) {
     JS_ReportErrorNumberASCII(cx, GetErrorMessage, nullptr, JSMSG_NOT_ITERABLE,
                               "Argument of Promise.all");
     return AbruptRejectPromise(cx, args, promiseCapability);
   }
@@ -2240,31 +2246,47 @@ static MOZ_MUST_USE bool RunResolutionFu
 
   if (mode == ResolveMode) {
     return ResolvePromiseInternal(cx, promise, result);
   }
 
   return RejectPromiseInternal(cx, promise, result);
 }
 
+static MOZ_MUST_USE JSObject* CommonStaticResolveRejectImpl(
+    JSContext* cx, HandleValue thisVal, HandleValue argVal,
+    ResolutionMode mode);
+
 static bool IsPromiseSpecies(JSContext* cx, JSFunction* species);
 
 // ES2019 draft rev dd269df67d37409a6f2099a842b8f5c75ee6fc24
 // 25.6.4.1.1 Runtime Semantics: PerformPromiseAll, step 6.
 // 25.6.4.3.1 Runtime Semantics: PerformPromiseRace, step 3.
 template <typename T>
 static MOZ_MUST_USE bool CommonPerformPromiseAllRace(
-    JSContext *cx, JS::ForOfIterator& iterator, HandleObject C,
+    JSContext *cx, PromiseForOfIterator& iterator, HandleObject C,
     Handle<PromiseCapability> resultCapability, bool* done,
     bool resolveReturnsUndefined, T getResolveFun) {
   RootedObject promiseCtor(
       cx, GlobalObject::getOrCreatePromiseConstructor(cx, cx->global()));
   if (!promiseCtor)
     return false;
 
+  // Optimized dense array iteration ensures no side-effects take place
+  // during the iteration.
+  bool iterationMayHaveSideEffects = !iterator.isOptimizedDenseArrayIteration();
+
+  // Try to optimize when the Promise object is in its default state, seeded
+  // with |C == promiseCtor| because we can only perform this optimization
+  // for the builtin Promise constructor.
+  bool isDefaultPromiseState = C == promiseCtor;
+  bool validatePromiseState = true;
+
+  PromiseLookup& promiseLookup = cx->realm()->promiseLookup;
+
   RootedValue CVal(cx, ObjectValue(*C));
   HandleObject resultPromise = resultCapability.promise();
   RootedValue resolveFunVal(cx);
   RootedValue rejectFunVal(cx, ObjectValue(*resultCapability.reject()));
 
   // We're reusing rooted variables in the loop below, so we don't need to
   // declare a gazillion different rooted variables here. Rooted variables
   // which are reused include "Or" in their name.
@@ -2284,27 +2306,71 @@ static MOZ_MUST_USE bool CommonPerformPr
       // Steps c, g.
       return false;
     }
 
     // Step d.
     if (*done)
       return true;
 
-    // 25.6.4.1.1, step 6.i.
-    // 25.6.4.3.1, step 3.h.
-    // Sadly, because someone could have overridden
-    // "resolve" on the canonical Promise constructor.
-    RootedValue& staticResolve = resolveOrThen;
-    if (!GetProperty(cx, C, CVal, cx->names().resolve, &staticResolve))
-      return false;
+    // Set to false when we can skip the [[Get]] for "then" and instead
+    // use the built-in Promise.prototype.then function.
+    bool getThen = true;
+
+    if (isDefaultPromiseState && validatePromiseState)
+      isDefaultPromiseState = promiseLookup.isDefaultPromiseState(cx);
 
     RootedValue& nextPromise = nextValueOrNextPromise;
-    if (!Call(cx, staticResolve, CVal, nextValue, &nextPromise))
-      return false;
+    if (isDefaultPromiseState) {
+      PromiseObject* nextValuePromise = nullptr;
+      if (nextValue.isObject() && nextValue.toObject().is<PromiseObject>())
+        nextValuePromise = &nextValue.toObject().as<PromiseObject>();
+
+      if (nextValuePromise &&
+          promiseLookup.isDefaultInstanceWhenPromiseStateIsSane(
+              cx, nextValuePromise)) {
+        // The below steps don't produce any side-effects, so we can
+        // skip the Promise state revalidation in the next iteration
+        // when the iterator itself also doesn't produce any side-effects.
+        validatePromiseState = iterationMayHaveSideEffects;
+
+        // 25.6.4.1.1, step 6.i.
+        // 25.6.4.3.1, step 3.h.
+        // Promise.resolve is a no-op for the default case.
+        MOZ_ASSERT(&nextPromise.toObject() == nextValuePromise);
+
+        // `nextPromise` uses the built-in `then` function.
+        getThen = false;
+      } else {
+        // Need to revalidate the Promise state in the next iteration,
+        // because CommonStaticResolveRejectImpl may have modified it.
+        validatePromiseState = true;
+
+        // 25.6.4.1.1, step 6.i.
+        // 25.6.4.3.1, step 3.h.
+        // Inline the call to Promise.resolve.
+        JSObject* res = CommonStaticResolveRejectImpl(cx, CVal, nextValue,
+                                                      ResolveMode);
+        if (!res)
+          return false;
+
+        nextPromise.setObject(*res);
+      }
+    } else {
+      // 25.6.4.1.1, step 6.i.
+      // 25.6.4.3.1, step 3.h.
+      // Sadly, because someone could have overridden
+      // "resolve" on the canonical Promise constructor.
+      RootedValue& staticResolve = resolveOrThen;
+      if (!GetProperty(cx, C, CVal, cx->names().resolve, &staticResolve))
+        return false;
+
+      if (!Call(cx, staticResolve, CVal, nextValue, &nextPromise))
+        return false;
+    }
 
     // Get the resolve function for this iteration.
     // 25.6.4.1.1, steps 6.j-q.
     JSObject* resolveFun = getResolveFun();
     if (!resolveFun)
       return false;
     resolveFunVal.setObject(*resolveFun);
 
@@ -2319,39 +2385,55 @@ static MOZ_MUST_USE bool CommonPerformPr
 
     // 25.6.4.1.1, step 6.r.
     // 25.6.4.3.1, step 3.i.
     nextPromiseObj = ToObject(cx, nextPromise);
     if (!nextPromiseObj)
       return false;
 
     RootedValue& thenVal = resolveOrThen;
-    if (!GetProperty(cx, nextPromiseObj, nextPromise, cx->names().then,
-                     &thenVal))
-      return false;
+    bool isBuiltinThen;
+    if (getThen) {
+      // We don't use the Promise lookup cache here, because this code
+      // is only called when we had a lookup cache miss, so it's likely
+      // we'd get another cache miss when trying to use the cache here.
+      if (!GetProperty(cx, nextPromiseObj, nextPromise, cx->names().then,
+                       &thenVal))
+        return false;
+
+      // |nextPromise| is an unwrapped Promise, and |then| is the
+      // original |Promise.prototype.then|, inline it here.
+      isBuiltinThen = nextPromiseObj->is<PromiseObject>() &&
+                      IsNativeFunction(thenVal, Promise_then);
+    } else {
+      isBuiltinThen = true;
+    }
 
     // By default, the blocked promise is added as an extra entry to the
     // rejected promises list.
     bool addToDependent = true;
 
-    if (nextPromiseObj->is<PromiseObject>() &&
-        IsNativeFunction(thenVal, Promise_then)) {
-      // |nextPromise| is an unwrapped Promise, and |then| is the
-      // original |Promise.prototype.then|, inline it here.
+    if (isBuiltinThen) {
+      MOZ_ASSERT(nextPromise.isObject());
+      MOZ_ASSERT(&nextPromise.toObject() == nextPromiseObj);
 
       // 25.6.5.4, step 3.
       RootedObject& thenSpecies = thenSpeciesOrBlockedPromise;
-      thenSpecies = SpeciesConstructor(cx, nextPromiseObj, JSProto_Promise,
-                                       IsPromiseSpecies);
-      if (!thenSpecies)
-        return false;
-
-      // The fast path here and in NewPromiseCapability may not set
-      // the resolve and reject handlers, so we need to clear the fields
-      // in case they were set in the previous iteration.
+      if (getThen) {
+        thenSpecies = SpeciesConstructor(cx, nextPromiseObj, JSProto_Promise,
+                                         IsPromiseSpecies);
+        if (!thenSpecies)
+          return false;
+      } else {
+        thenSpecies = promiseCtor;
+      }
+
+      // The fast path here and the one in NewPromiseCapability may not
+      // set the resolve and reject handlers, so we need to clear the
+      // fields in case they were set in the previous iteration.
       thenCapability.resolve().set(nullptr);
       thenCapability.reject().set(nullptr);
 
       // Skip the creation of a built-in Promise object if:
       // 1. `thenSpecies` is the built-in Promise constructor.
       // 2. `resolveFun` doesn't return an object, which ensures no
       //    side-effects take place in ResolvePromiseInternal.
       // 3. The result promise is a built-in Promise object.
@@ -2434,17 +2516,17 @@ static MOZ_MUST_USE bool CommonPerformPr
           return false;
       }
     }
   }
 }
 
 // ES2016, 25.4.4.1.1.
 static MOZ_MUST_USE bool PerformPromiseAll(
-    JSContext* cx, JS::ForOfIterator& iterator, HandleObject C,
+    JSContext *cx, PromiseForOfIterator& iterator, HandleObject C,
     Handle<PromiseCapability> resultCapability, bool* done) {
   *done = false;
 
   // Step 1.
   MOZ_ASSERT(C->isConstructor());
 
   // Step 2 (omitted).
 
@@ -2637,17 +2719,17 @@ static bool PromiseAllResolveElementFunc
   }
 
   // Step 11.
   args.rval().setUndefined();
   return true;
 }
 
 static MOZ_MUST_USE bool PerformPromiseRace(
-    JSContext *cx, JS::ForOfIterator& iterator, HandleObject C,
+    JSContext *cx, PromiseForOfIterator& iterator, HandleObject C,
     Handle<PromiseCapability> resultCapability, bool* done);
 
 // ES2016, 25.4.4.3.
 static bool Promise_static_race(JSContext* cx, unsigned argc, Value* vp) {
   CallArgs args = CallArgsFromVp(argc, vp);
   HandleValue iterable = args.get(0);
 
   // Step 2 (reordered).
@@ -2663,17 +2745,17 @@ static bool Promise_static_race(JSContex
   RootedObject C(cx, &CVal.toObject());
 
   // Step 3.
   Rooted<PromiseCapability> promiseCapability(cx);
   if (!NewPromiseCapability(cx, C, &promiseCapability, false))
     return false;
 
   // Steps 4-5.
-  JS::ForOfIterator iter(cx);
+  PromiseForOfIterator iter(cx);
   if (!iter.init(iterable, JS::ForOfIterator::AllowNonIterable))
     return AbruptRejectPromise(cx, args, promiseCapability);
 
   if (!iter.valueIsIterable()) {
     JS_ReportErrorNumberASCII(cx, GetErrorMessage, nullptr, JSMSG_NOT_ITERABLE,
                               "Argument of Promise.race");
     return AbruptRejectPromise(cx, args, promiseCapability);
   }
@@ -2695,17 +2777,17 @@ static bool Promise_static_race(JSContex
 
   // Step 9.
   args.rval().setObject(*promiseCapability.promise());
   return true;
 }
 
 // ES2016, 25.4.4.3.1.
 static MOZ_MUST_USE bool PerformPromiseRace(
-    JSContext* cx, JS::ForOfIterator& iterator, HandleObject C,
+    JSContext *cx, PromiseForOfIterator& iterator, HandleObject C,
     Handle<PromiseCapability> resultCapability, bool* done) {
   *done = false;
 
   // Step 1.
   MOZ_ASSERT(C->isConstructor());
 
   // Step 2 (omitted).
 
@@ -2984,16 +3066,57 @@ static MOZ_MUST_USE bool OriginalPromise
     return false;
   }
 
   // Step 5.
   return PerformPromiseThenWithoutSettleHandlers(cx, promise, promiseToResolve,
                                                  resultCapability);
 }
 
+static bool CanCallOriginalPromiseThenBuiltin(JSContext* cx,
+                                              HandleValue promise) {
+  return promise.isObject() && promise.toObject().is<PromiseObject>() &&
+         cx->realm()->promiseLookup.isDefaultInstance(
+             cx, &promise.toObject().as<PromiseObject>());
+}
+
+// ES2016, 25.4.5.3., steps 3-5.
+static bool OriginalPromiseThenBuiltin(JSContext* cx, HandleValue promiseVal,
+                                       HandleValue onFulfilled,
+                                       HandleValue onRejected,
+                                       MutableHandleValue rval, bool rvalUsed) {
+  assertSameCompartment(cx, promiseVal, onFulfilled, onRejected);
+  MOZ_ASSERT(CanCallOriginalPromiseThenBuiltin(cx, promiseVal));
+
+  Rooted<PromiseObject*> promise(cx,
+                                 &promiseVal.toObject().as<PromiseObject>());
+
+  // Steps 3-4.
+  Rooted<PromiseCapability> resultCapability(cx);
+  if (rvalUsed) {
+    PromiseObject* resultPromise =
+        CreatePromiseObjectWithoutResolutionFunctions(cx);
+    if (!resultPromise)
+      return false;
+
+    resultCapability.promise().set(resultPromise);
+  }
+
+  // Step 5.
+  if (!PerformPromiseThen(cx, promise, onFulfilled, onRejected,
+                          resultCapability))
+    return false;
+
+  if (rvalUsed)
+    rval.setObject(*resultCapability.promise());
+  else
+    rval.setUndefined();
+  return true;
+}
+
 static MOZ_MUST_USE bool PerformPromiseThenWithReaction(
     JSContext* cx, Handle<PromiseObject*> promise,
     Handle<PromiseReactionRecord*> reaction);
 
 // Some async/await functions are implemented here instead of
 // js/src/builtin/AsyncFunction.cpp, to call Promise internal functions.
 
 // ES 2018 draft 14.6.11 and 14.7.14 step 1.
@@ -3507,31 +3630,37 @@ MOZ_MUST_USE bool js::AsyncGeneratorEnqu
   result.setObject(*resultPromise);
   return true;
 }
 
 static bool Promise_catch_impl(JSContext* cx, unsigned argc, Value* vp,
                                bool rvalUsed) {
   CallArgs args = CallArgsFromVp(argc, vp);
 
+  HandleValue thisVal = args.thisv();
+  HandleValue onFulfilled = UndefinedHandleValue;
+  HandleValue onRejected = args.get(0);
+
+  // Fast path when the default Promise state is intact.
+  if (CanCallOriginalPromiseThenBuiltin(cx, thisVal)) {
+    return OriginalPromiseThenBuiltin(cx, thisVal, onFulfilled, onRejected,
+                                      args.rval(), rvalUsed);
+  }
+
   // Step 1.
   RootedValue thenVal(cx);
-  if (!GetProperty(cx, args.thisv(), cx->names().then, &thenVal))
+  if (!GetProperty(cx, thisVal, cx->names().then, &thenVal))
     return false;
 
-  if (IsNativeFunction(thenVal, &Promise_then)) {
-    return Promise_then_impl(cx, args.thisv(), UndefinedHandleValue,
-                             args.get(0), args.rval(), rvalUsed);
-  }
-
-  FixedInvokeArgs<2> iargs(cx);
-  iargs[0].setUndefined();
-  iargs[1].set(args.get(0));
-
-  return Call(cx, thenVal, args.thisv(), iargs, args.rval());
+  if (IsNativeFunction(thenVal, &Promise_then))
+    return Promise_then_impl(cx, thisVal, onFulfilled, onRejected, args.rval(),
+                             rvalUsed);
+
+  return Call(cx, thenVal, thisVal, UndefinedHandleValue, onRejected,
+              args.rval());
 }
 
 static MOZ_ALWAYS_INLINE bool IsPromiseThenOrCatchRetValImplicitlyUsed(
     JSContext* cx) {
   // The returned promise of Promise#then and Promise#catch contains
   // stack info if async stack is enabled.  Even if their return value is not
   // used explicitly in the script, the stack info is observable in devtools
   // and profilers.  We shouldn't apply the optimization not to allocate the
@@ -3573,16 +3702,22 @@ static bool Promise_then_impl(JSContext*
   // Step 1 (implicit).
   // Step 2.
   if (!promiseVal.isObject()) {
     JS_ReportErrorNumberASCII(cx, GetErrorMessage, nullptr,
                               JSMSG_NOT_NONNULL_OBJECT,
                               "Receiver of Promise.prototype.then call");
     return false;
   }
+
+  // Fast path when the default Promise state is intact.
+  if (CanCallOriginalPromiseThenBuiltin(cx, promiseVal))
+    return OriginalPromiseThenBuiltin(cx, promiseVal, onFulfilled, onRejected,
+                                      rval, rvalUsed);
+
   RootedObject promiseObj(cx, &promiseVal.toObject());
   Rooted<PromiseObject*> promise(cx);
 
   if (promiseObj->is<PromiseObject>()) {
     promise = &promiseObj->as<PromiseObject>();
   } else {
     JSObject* unwrappedPromiseObj = CheckedUnwrap(promiseObj);
     if (!unwrappedPromiseObj) {
@@ -3925,16 +4060,237 @@ bool PromiseObject::dependentPromises(JS
   PromiseDebugInfo::setResolutionInfo(cx, promise);
 
   if (promise->state() == JS::PromiseState::Rejected && promise->isUnhandled())
     cx->runtime()->addUnhandledRejectedPromise(cx, promise);
 
   Debugger::onPromiseSettled(cx, promise);
 }
 
+JSFunction* js::PromiseLookup::getPromiseConstructor(JSContext* cx) {
+  const Value& val = cx->global()->getConstructor(JSProto_Promise);
+  return val.isObject() ? &val.toObject().as<JSFunction>() : nullptr;
+}
+
+NativeObject* js::PromiseLookup::getPromisePrototype(JSContext* cx) {
+  const Value& val = cx->global()->getPrototype(JSProto_Promise);
+  return val.isObject() ? &val.toObject().as<NativeObject>() : nullptr;
+}
+
+bool js::PromiseLookup::isDataPropertyNative(JSContext* cx, NativeObject* obj,
+                                             uint32_t slot, JSNative native) {
+  JSFunction* fun;
+  if (!IsFunctionObject(obj->getSlot(slot), &fun))
+    return false;
+  return fun->maybeNative() == native && fun->realm() == cx->realm();
+}
+
+bool js::PromiseLookup::isAccessorPropertyNative(JSContext* cx, Shape* shape,
+                                                 JSNative native) {
+  JSObject* getter = shape->getterObject();
+  return getter && IsNativeFunction(getter, native) &&
+         getter->as<JSFunction>().realm() == cx->realm();
+}
+
+void js::PromiseLookup::initialize(JSContext* cx) {
+  MOZ_ASSERT(state_ == State::Uninitialized);
+
+  // Get the canonical Promise.prototype.
+  NativeObject* promiseProto = getPromisePrototype(cx);
+
+  // Check condition 1:
+  // Leave the cache uninitialized if the Promise class itself is not yet
+  // initialized.
+  if (!promiseProto)
+    return;
+
+  // Get the canonical Promise constructor.
+  JSFunction* promiseCtor = getPromiseConstructor(cx);
+  MOZ_ASSERT(promiseCtor,
+             "The Promise constructor is initialized iff Promise.prototype is "
+             "initialized");
+
+  // Shortcut returns below means Promise[@@species] will never be
+  // optimizable, set to disabled now, and clear it later when we succeed.
+  state_ = State::Disabled;
+
+  // Check condition 2:
+  // Look up Promise.prototype.constructor and ensure it's a data property.
+  Shape* ctorShape = promiseProto->lookup(cx, cx->names().constructor);
+  if (!ctorShape || !ctorShape->isDataProperty())
+    return;
+
+  // Get the referred value, and ensure it holds the canonical Promise
+  // constructor.
+  JSFunction* ctorFun;
+  if (!IsFunctionObject(promiseProto->getSlot(ctorShape->slot()), &ctorFun))
+    return;
+  if (ctorFun != promiseCtor)
+    return;
+
+  // Check condition 3:
+  // Look up Promise.prototype.then and ensure it's a data property.
+  Shape* thenShape = promiseProto->lookup(cx, cx->names().then);
+  if (!thenShape || !thenShape->isDataProperty())
+    return;
+
+  // Get the referred value, and ensure it holds the canonical "then"
+  // function.
+  if (!isDataPropertyNative(cx, promiseProto, thenShape->slot(), Promise_then))
+    return;
+
+  // Check condition 4:
+  // Look up the '@@species' value on Promise.
+  Shape* speciesShape =
+      promiseCtor->lookup(cx, SYMBOL_TO_JSID(cx->wellKnownSymbols().species));
+  if (!speciesShape || !speciesShape->hasGetterObject())
+    return;
+
+  // Get the referred value, ensure it holds the canonical Promise[@@species]
+  // function.
+  if (!isAccessorPropertyNative(cx, speciesShape, Promise_static_species))
+    return;
+
+  // Check condition 5:
+  // Look up Promise.resolve and ensure it's a data property.
+  Shape* resolveShape = promiseCtor->lookup(cx, cx->names().resolve);
+  if (!resolveShape || !resolveShape->isDataProperty())
+    return;
+
+  // Get the referred value, and ensure it holds the canonical "resolve"
+  // function.
+  if (!isDataPropertyNative(cx, promiseCtor, resolveShape->slot(),
+                            Promise_static_resolve))
+    return;
+
+  // Store raw pointers below. This is okay to do here, because all objects
+  // are in the tenured heap.
+  MOZ_ASSERT(!IsInsideNursery(promiseCtor->lastProperty()));
+  MOZ_ASSERT(!IsInsideNursery(speciesShape));
+  MOZ_ASSERT(!IsInsideNursery(promiseProto->lastProperty()));
+
+  state_ = State::Initialized;
+  promiseConstructorShape_ = promiseCtor->lastProperty();
+#ifdef DEBUG
+  promiseSpeciesShape_ = speciesShape;
+#endif
+  promiseProtoShape_ = promiseProto->lastProperty();
+  promiseResolveSlot_ = resolveShape->slot();
+  promiseProtoConstructorSlot_ = ctorShape->slot();
+  promiseProtoThenSlot_ = thenShape->slot();
+}
+
+void js::PromiseLookup::reset() {
+  JS_POISON(this, 0xBB, sizeof(this), MemCheckKind::MakeUndefined);
+  state_ = State::Uninitialized;
+}
+
+bool js::PromiseLookup::isPromiseStateStillSane(JSContext* cx) {
+  MOZ_ASSERT(state_ == State::Initialized);
+
+  NativeObject* promiseProto = getPromisePrototype(cx);
+  MOZ_ASSERT(promiseProto);
+
+  NativeObject* promiseCtor = getPromiseConstructor(cx);
+  MOZ_ASSERT(promiseCtor);
+
+  // Ensure that Promise.prototype still has the expected shape.
+  if (promiseProto->lastProperty() != promiseProtoShape_)
+    return false;
+
+  // Ensure that Promise still has the expected shape.
+  if (promiseCtor->lastProperty() != promiseConstructorShape_)
+    return false;
+
+  // Ensure that Promise.prototype.constructor is the canonical constructor.
+  if (promiseProto->getSlot(promiseProtoConstructorSlot_) !=
+      ObjectValue(*promiseCtor))
+    return false;
+
+  // Ensure that Promise.prototype.then is the canonical "then" function.
+  if (!isDataPropertyNative(cx, promiseProto, promiseProtoThenSlot_,
+                            Promise_then))
+    return false;
+
+  // Ensure the species getter contains the canonical @@species function.
+  // Note: This is currently guaranteed to be always true, because modifying
+  // the getter property implies a new shape is generated. If this ever
+  // changes, convert this assertion into an if-statement.
+#ifdef DEBUG
+  MOZ_ASSERT(isAccessorPropertyNative(cx, promiseSpeciesShape_,
+                                      Promise_static_species));
+#endif
+
+  // Ensure that Promise.resolve is the canonical "resolve" function.
+  if (!isDataPropertyNative(cx, promiseCtor, promiseResolveSlot_,
+                            Promise_static_resolve))
+    return false;
+
+  return true;
+}
+
+bool js::PromiseLookup::ensureInitialized(JSContext* cx,
+                                          Reinitialize reinitialize) {
+  if (state_ == State::Uninitialized) {
+    // If the cache is not initialized, initialize it.
+    initialize(cx);
+  } else if (state_ == State::Initialized) {
+    if (reinitialize == Reinitialize::Allowed) {
+      if (!isPromiseStateStillSane(cx)) {
+        // If the promise state is no longer sane, reinitialize.
+        reset();
+        initialize(cx);
+      }
+    } else {
+      // When we're not allowed to reinitialize, the promise state must
+      // still be sane if the cache is already initialized.
+      MOZ_ASSERT(isPromiseStateStillSane(cx));
+    }
+  }
+
+  // If the cache is disabled or still uninitialized, don't bother trying to
+  // optimize.
+  if (state_ != State::Initialized)
+    return false;
+
+  // By the time we get here, we should have a sane promise state.
+  MOZ_ASSERT(isPromiseStateStillSane(cx));
+
+  return true;
+}
+
+bool js::PromiseLookup::isDefaultPromiseState(JSContext* cx) {
+  // Promise and Promise.prototype are in their default states iff the
+  // lookup cache was successfully initialized.
+  return ensureInitialized(cx, Reinitialize::Allowed);
+}
+
+bool js::PromiseLookup::hasDefaultProtoAndNoShadowedProperties(
+    JSContext* cx, PromiseObject* promise) {
+  // Ensure |promise|'s prototype is the actual Promise.prototype.
+  if (promise->staticPrototype() != getPromisePrototype(cx))
+    return false;
+
+  // Ensure |promise| doesn't define any own properties. This serves as a
+  // quick check to make sure |promise| doesn't define an own "constructor"
+  // or "then" property which may shadow Promise.prototype.constructor or
+  // Promise.prototype.then.
+  return promise->lastProperty()->isEmptyShape();
+}
+
+bool js::PromiseLookup::isDefaultInstance(JSContext* cx, PromiseObject* promise,
+                                          Reinitialize reinitialize) {
+  // Promise and Promise.prototype must be in their default states.
+  if (!ensureInitialized(cx, reinitialize))
+    return false;
+
+  // The object uses the default properties from Promise.prototype.
+  return hasDefaultProtoAndNoShadowedProperties(cx, promise);
+}
+
 OffThreadPromiseTask::OffThreadPromiseTask(JSContext* cx,
                                            Handle<PromiseObject*> promise)
     : runtime_(cx->runtime()), promise_(cx, promise), registered_(false) {
   MOZ_ASSERT(runtime_ == promise_->zone()->runtimeFromMainThread());
   MOZ_ASSERT(CurrentThreadCanAccessRuntime(runtime_));
   MOZ_ASSERT(cx->runtime()->offThreadPromiseState.ref().initialized());
 }
 
diff --git a/js/src/builtin/Promise.h b/js/src/builtin/Promise.h
--- a/js/src/builtin/Promise.h
+++ b/js/src/builtin/Promise.h
@@ -173,16 +173,150 @@ MOZ_MUST_USE bool AsyncGeneratorReject(
 MOZ_MUST_USE bool AsyncGeneratorEnqueue(JSContext* cx, HandleValue asyncGenVal,
                                         CompletionKind completionKind,
                                         HandleValue completionValue,
                                         MutableHandleValue result);
 
 bool AsyncFromSyncIteratorMethod(JSContext* cx, CallArgs& args,
                                  CompletionKind completionKind);
 
+class MOZ_NON_TEMPORARY_CLASS PromiseLookup final {
+  /*
+   * A PromiseLookup holds the following:
+   *
+   *  Promise's shape (promiseConstructorShape_)
+   *       To ensure that Promise has not been modified.
+   *
+   *  Promise.prototype's shape (promiseProtoShape_)
+   *      To ensure that Promise.prototype has not been modified.
+   *
+   *  Promise's shape for the @@species getter. (promiseSpeciesShape_)
+   *      To quickly retrieve the @@species getter for Promise.
+   *
+   *  Promise's slot number for resolve (promiseResolveSlot_)
+   *      To quickly retrieve the Promise.resolve function.
+   *
+   *  Promise.prototype's slot number for constructor
+   *  (promiseProtoConstructorSlot_)
+   *      To quickly retrieve the Promise.prototype.constructor property.
+   *
+   *  Promise.prototype's slot number for then (promiseProtoThenSlot_)
+   *      To quickly retrieve the Promise.prototype.then function.
+   *
+   * MOZ_INIT_OUTSIDE_CTOR fields below are set in |initialize()|.  The
+   * constructor only initializes a |state_| field, that defines whether the
+   * other fields are accessible.
+   */
+
+  // Shape of matching Promise object.
+  MOZ_INIT_OUTSIDE_CTOR Shape* promiseConstructorShape_;
+
+#ifdef DEBUG
+  // Accessor Shape containing the @@species property.
+  // See isPromiseStateStillSane() for why this field is debug-only.
+  MOZ_INIT_OUTSIDE_CTOR Shape* promiseSpeciesShape_;
+#endif
+
+  // Shape of matching Promise.prototype object.
+  MOZ_INIT_OUTSIDE_CTOR Shape* promiseProtoShape_;
+
+  // Slots Promise.resolve, Promise.prototype.constructor, and
+  // Promise.prototype.then.
+  MOZ_INIT_OUTSIDE_CTOR uint32_t promiseResolveSlot_;
+  MOZ_INIT_OUTSIDE_CTOR uint32_t promiseProtoConstructorSlot_;
+  MOZ_INIT_OUTSIDE_CTOR uint32_t promiseProtoThenSlot_;
+
+  enum class State : uint8_t {
+    // Flags marking the lazy initialization of the above fields.
+    Uninitialized,
+    Initialized,
+
+    // The disabled flag is set when we don't want to try optimizing
+    // anymore because core objects were changed.
+    Disabled
+  };
+
+  State state_ = State::Uninitialized;
+
+  // Initialize the internal fields.
+  //
+  // The cache is successfully initialized iff
+  // 1. Promise and Promise.prototype classes are initialized.
+  // 2. Promise.prototype.constructor is equal to Promise.
+  // 3. Promise.prototype.then is the original `then` function.
+  // 4. Promise[@@species] is the original @@species getter.
+  // 5. Promise.resolve is the original `resolve` function.
+  void initialize(JSContext* cx);
+
+  // Reset the cache.
+  void reset();
+
+  // Check if the global promise-related objects have not been messed with
+  // in a way that would disable this cache.
+  bool isPromiseStateStillSane(JSContext* cx);
+
+  // Flags to control whether or not ensureInitialized() is allowed to
+  // reinitialize the cache when the Promise state is no longer sane.
+  enum class Reinitialize : bool { Allowed, Disallowed };
+
+  // Return true if the lookup cache is properly initialized for usage.
+  bool ensureInitialized(JSContext* cx, Reinitialize reinitialize);
+
+  // Return true if the prototype of the given Promise object is
+  // Promise.prototype and the object doesn't shadow properties from
+  // Promise.prototype.
+  bool hasDefaultProtoAndNoShadowedProperties(JSContext* cx,
+                                              PromiseObject* promise);
+
+  // Return true if the given Promise object uses the default @@species,
+  // "constructor", and "then" properties.
+  bool isDefaultInstance(JSContext* cx, PromiseObject* promise,
+                         Reinitialize reinitialize);
+
+  // Return the built-in Promise constructor or null if not yet initialized.
+  static JSFunction* getPromiseConstructor(JSContext* cx);
+
+  // Return the built-in Promise prototype or null if not yet initialized.
+  static NativeObject* getPromisePrototype(JSContext* cx);
+
+  // Return true if the slot contains the given native.
+  static bool isDataPropertyNative(JSContext* cx, NativeObject* obj,
+                                   uint32_t slot, JSNative native);
+
+  // Return true if the accessor shape contains the given native.
+  static bool isAccessorPropertyNative(JSContext* cx, Shape* shape, JSNative native);
+
+ public:
+  /** Construct a |PromiseSpeciesLookup| in the uninitialized state. */
+  PromiseLookup() { reset(); }
+
+  // Return true if the Promise constructor and Promise.prototype still use
+  // the default built-in functions.
+  bool isDefaultPromiseState(JSContext* cx);
+
+  // Return true if the given Promise object uses the default @@species,
+  // "constructor", and "then" properties.
+  bool isDefaultInstance(JSContext* cx, PromiseObject* promise) {
+    return isDefaultInstance(cx, promise, Reinitialize::Allowed);
+  }
+
+  // Return true if the given Promise object uses the default @@species,
+  // "constructor", and "then" properties.
+  bool isDefaultInstanceWhenPromiseStateIsSane(JSContext* cx,
+                                               PromiseObject* promise) {
+    return isDefaultInstance(cx, promise, Reinitialize::Disallowed);
+  }
+
+  // Purge the cache and all info associated with it.
+  void purge() {
+    if (state_ == State::Initialized)
+      reset();
+  }
+};
+
 // An OffThreadPromiseTask holds a rooted Promise JSObject while executing an
 // off-thread task (defined by the subclass) that needs to resolve the Promise
 // on completion. Because OffThreadPromiseTask contains a PersistentRooted, it
 // must be destroyed on an active JSContext thread of the Promise's JSRuntime.
 // OffThreadPromiseTasks may be run off-thread in various ways (e.g., see
 // PromiseHelperTask). At any time, the task can be dispatched to an active
 // JSContext of the Promise's JSRuntime by calling dispatchResolve().
 
diff --git a/js/src/vm/Realm.cpp b/js/src/vm/Realm.cpp
--- a/js/src/vm/Realm.cpp
+++ b/js/src/vm/Realm.cpp
@@ -530,16 +530,17 @@ void Realm::checkScriptMapsAfterMovingGC
 #endif
 
 void Realm::purge() {
   dtoaCache.purge();
   newProxyCache.purge();
   objectGroups_.purge();
   objects_.iteratorCache.clearAndShrink();
   arraySpeciesLookup.purge();
+  promiseLookup.purge();
 }
 
 void Realm::clearTables() {
   global_.set(nullptr);
 
   // No scripts should have run in this realm. This is used when merging
   // a realm that has been used off thread into another realm and zone.
   compartment()->assertNoCrossCompartmentWrappers();
diff --git a/js/src/vm/Realm.h b/js/src/vm/Realm.h
--- a/js/src/vm/Realm.h
+++ b/js/src/vm/Realm.h
@@ -390,16 +390,17 @@ class JS::Realm : public JS::shadow::Rea
   // is enabled.
   js::coverage::LCovRealm lcovOutput;
 
   js::RegExpRealm regExps;
 
   js::DtoaCache dtoaCache;
   js::NewProxyCache newProxyCache;
   js::ArraySpeciesLookup arraySpeciesLookup;
+  js::PromiseLookup promiseLookup;
 
   js::PerformanceGroupHolder performanceMonitoring;
 
   js::UniquePtr<js::ScriptCountsMap> scriptCountsMap;
   js::UniquePtr<js::ScriptNameMap> scriptNameMap;
   js::UniquePtr<js::DebugScriptMap> debugScriptMap;
 
   /*
