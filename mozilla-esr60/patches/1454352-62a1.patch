# HG changeset patch
# User Eric Faust <efaustbmo@gmail.com>
# Date 1525955176 -3600
# Node ID 91ced8101f99044842e7e532edd9b45e54c35bc5
# Parent  1539678b031531454ba8d752231ca315cdea2135
Bug 1454352 - Add APIs for parallel decoding of BinAST data r=jonco

diff --git a/js/src/frontend/BytecodeCompiler.cpp b/js/src/frontend/BytecodeCompiler.cpp
--- a/js/src/frontend/BytecodeCompiler.cpp
+++ b/js/src/frontend/BytecodeCompiler.cpp
@@ -559,24 +559,24 @@ JSScript* frontend::CompileGlobalScript(
   assertException.reset();
   return script;
 }
 
 #if defined(JS_BUILD_BINAST)
 
 JSScript* frontend::CompileGlobalBinASTScript(
     JSContext* cx, LifoAlloc& alloc, const ReadOnlyCompileOptions& options,
-    const uint8_t* src, size_t len) {
+    const uint8_t* src, size_t len, ScriptSourceObject** sourceObjectOut) {
   AutoAssertReportedException assertException(cx);
 
   frontend::UsedNameTracker usedNames(cx);
   if (!usedNames.init())
     return nullptr;
 
-  RootedObject sourceObj(cx, CreateScriptSourceObject(cx, options));
+  RootedScriptSourceObject sourceObj(cx, CreateScriptSourceObject(cx, options));
 
   if (!sourceObj)
     return nullptr;
 
   RootedScript script(cx, JSScript::Create(cx, options, sourceObj, 0, len, 0,
                                            len));
 
   if (!script)
@@ -599,16 +599,19 @@ JSScript* frontend::CompileGlobalBinASTS
 
   ParseNode *pn = parsed.unwrap();
   if (!bce.emitScript(pn))
     return nullptr;
 
   if (!NameFunctions(cx, pn))
     return nullptr;
 
+  if (sourceObjectOut)
+    *sourceObjectOut = sourceObj;
+
   assertException.reset();
   return script;
 }
 
 #endif  // JS_BUILD_BINAST
 
 JSScript* frontend::CompileEvalScript(JSContext* cx, LifoAlloc& alloc,
                                       HandleObject environment,
diff --git a/js/src/frontend/BytecodeCompiler.h b/js/src/frontend/BytecodeCompiler.h
--- a/js/src/frontend/BytecodeCompiler.h
+++ b/js/src/frontend/BytecodeCompiler.h
@@ -32,19 +32,20 @@ class ParseNode;
 
 JSScript* CompileGlobalScript(JSContext* cx, LifoAlloc& alloc,
                               ScopeKind scopeKind,
                               const ReadOnlyCompileOptions& options,
                               SourceBufferHolder& srcBuf,
                               ScriptSourceObject** sourceObjectOut = nullptr);
 
 #if defined(JS_BUILD_BINAST)
-JSScript* CompileGlobalBinASTScript(JSContext *cx, LifoAlloc& alloc,
-                                    const ReadOnlyCompileOptions& options,
-                                    const uint8_t* src, size_t len);
+JSScript* CompileGlobalBinASTScript(
+    JSContext *cx, LifoAlloc& alloc, const ReadOnlyCompileOptions& options,
+    const uint8_t* src, size_t len,
+    ScriptSourceObject** sourceObjectOut = nullptr);
 #endif  // JS_BUILD_BINAST
 
 JSScript* CompileEvalScript(JSContext* cx, LifoAlloc& alloc,
                             HandleObject scopeChain, HandleScope enclosingScope,
                             const ReadOnlyCompileOptions& options,
                             SourceBufferHolder& srcBuf,
                             ScriptSourceObject** sourceObjectOut = nullptr);
 
diff --git a/js/src/jsapi.cpp b/js/src/jsapi.cpp
--- a/js/src/jsapi.cpp
+++ b/js/src/jsapi.cpp
@@ -3658,16 +3658,30 @@ JSScript* JS::DecodeBinAST(JSContext* cx
 JSScript* JS::DecodeBinAST(JSContext* cx, const ReadOnlyCompileOptions& options,
                            FILE* file) {
   FileContents fileContents(cx);
   if (!ReadCompleteFile(cx, file, fileContents))
     return nullptr;
 
   return DecodeBinAST(cx, options, fileContents.begin(), fileContents.length());
 }
+
+JS_PUBLIC_API bool JS::DecodeBinASTOffThread(
+    JSContext* cx, const ReadOnlyCompileOptions& options, const uint8_t* buf,
+    size_t length, OffThreadCompileCallback callback, void* callbackData) {
+  return StartOffThreadDecodeBinAST(cx, options, buf, length, callback,
+                                    callbackData);
+}
+
+JS_PUBLIC_API JSScript* JS::FinishOffThreadBinASTDecode(JSContext* cx,
+                                                        void* token) {
+  MOZ_ASSERT(cx);
+  MOZ_ASSERT(CurrentThreadCanAccessRuntime(cx->runtime()));
+  return HelperThreadState().finishBinASTDecodeTask(cx, token);
+}
 #endif /* JS_BUILD_BINAST */
 
 enum class OffThread {
   Compile,
   Decode,
 };
 
 static bool CanDoOffThread(JSContext* cx, const ReadOnlyCompileOptions& options,
diff --git a/js/src/jsapi.h b/js/src/jsapi.h
--- a/js/src/jsapi.h
+++ b/js/src/jsapi.h
@@ -4261,16 +4261,23 @@ namespace JS {
 
 extern JS_PUBLIC_API JSScript* DecodeBinAST(
     JSContext* cx, const ReadOnlyCompileOptions& options, FILE* file);
 
 extern JS_PUBLIC_API JSScript* DecodeBinAST(
     JSContext* cx, const ReadOnlyCompileOptions& options, const uint8_t* buf,
     size_t length);
 
+extern JS_PUBLIC_API bool DecodeBinASTOffThread(
+    JSContext* cx, const ReadOnlyCompileOptions& options, const uint8_t* buf,
+    size_t length, OffThreadCompileCallback callback, void* callbackData);
+
+extern JS_PUBLIC_API JSScript* FinishOffThreadBinASTDecode(JSContext* cx,
+                                                           void* token);
+
 } /* namespace JS */
 
 #endif /* JS_BUILD_BINAST */
 
 extern JS_PUBLIC_API bool JS_CheckForInterrupt(JSContext* cx);
 
 /*
  * These functions allow setting an interrupt callback that will be called
diff --git a/js/src/vm/HelperThreads.cpp b/js/src/vm/HelperThreads.cpp
--- a/js/src/vm/HelperThreads.cpp
+++ b/js/src/vm/HelperThreads.cpp
@@ -500,16 +500,39 @@ void ScriptDecodeTask::parse(JSContext* 
   XDRResult res = decoder.codeScript(&resultScript);
   MOZ_ASSERT(bool(resultScript) == res.isOk());
   if (res.isOk()) {
     scripts.infallibleAppend(resultScript);
     if (sourceObject) sourceObjects.infallibleAppend(sourceObject);
   }
 }
 
+#if defined(JS_BUILD_BINAST)
+BinASTDecodeTask::BinASTDecodeTask(JSContext* cx, const uint8_t* buf,
+                                   size_t length,
+                                   JS::OffThreadCompileCallback callback,
+                                   void* callbackData)
+    : ParseTask(ParseTaskKind::BinAST, cx, callback, callbackData),
+      data(buf, length) {}
+
+void BinASTDecodeTask::parse(JSContext* cx) {
+  RootedScriptSourceObject sourceObject(cx);
+
+  JSScript* script = frontend::CompileGlobalBinASTScript(cx, alloc, options,
+                                                         data.begin().get(),
+                                                         data.length(),
+                                                         &sourceObject.get());
+  if (script) {
+    scripts.infallibleAppend(script);
+    if (sourceObject)
+      sourceObjects.infallibleAppend(sourceObject);
+  }
+}
+#endif /* JS_BUILD_BINAST */
+
 MultiScriptsDecodeTask::MultiScriptsDecodeTask(
     JSContext* cx, JS::TranscodeSources& sources,
     JS::OffThreadCompileCallback callback, void* callbackData)
     : ParseTask(ParseTaskKind::MultiScriptsDecode, cx, callback, callbackData),
       sources(&sources) {}
 
 void MultiScriptsDecodeTask::parse(JSContext* cx) {
   if (!scripts.reserve(sources->length()) ||
@@ -778,16 +801,32 @@ bool js::StartOffThreadDecodeMultiScript
   ScopedJSDeletePtr<ParseTask> task;
   task = cx->new_<MultiScriptsDecodeTask>(cx, sources, callback, callbackData);
   if (!task || !StartOffThreadParseTask(cx, task, options)) return false;
 
   task.forget();
   return true;
 }
 
+#if defined(JS_BUILD_BINAST)
+bool js::StartOffThreadDecodeBinAST(JSContext* cx,
+                                    const ReadOnlyCompileOptions& options,
+                                    const uint8_t* buf, size_t length,
+                                    JS::OffThreadCompileCallback callback,
+                                    void *callbackData) {
+  ScopedJSDeletePtr<ParseTask> task;
+  task = cx->new_<BinASTDecodeTask>(cx, buf, length, callback, callbackData);
+  if (!task || !StartOffThreadParseTask(cx, task, options))
+    return false;
+
+  task.forget();
+  return true;
+}
+#endif /* JS_BUILD_BINAST */
+
 void js::EnqueuePendingParseTasksAfterGC(JSRuntime* rt) {
   MOZ_ASSERT(!OffThreadParsingMustWaitForGC(rt));
 
   GlobalHelperThreadState::ParseTaskVector newTasks;
   {
     AutoLockHelperThreadState lock;
     GlobalHelperThreadState::ParseTaskVector& waiting =
         HelperThreadState().parseWaitingOnGC(lock);
@@ -1551,16 +1590,25 @@ JSScript* GlobalHelperThreadState::finis
 
 JSScript* GlobalHelperThreadState::finishScriptDecodeTask(JSContext* cx,
                                                           void* token) {
   JSScript* script = finishParseTask(cx, ParseTaskKind::ScriptDecode, token);
   MOZ_ASSERT_IF(script, script->isGlobalCode());
   return script;
 }
 
+#if defined(JS_BUILD_BINAST)
+JSScript* GlobalHelperThreadState::finishBinASTDecodeTask(JSContext* cx,
+                                                          void* token) {
+  JSScript* script = finishParseTask(cx, ParseTaskKind::BinAST, token);
+  MOZ_ASSERT_IF(script, script->isGlobalCode());
+  return script;
+}
+#endif /* JS_BUILD_BINAST */
+
 bool GlobalHelperThreadState::finishMultiScriptsDecodeTask(
     JSContext* cx, void* token, MutableHandle<ScriptVector> scripts) {
   return finishParseTask(cx, ParseTaskKind::MultiScriptsDecode, token, scripts);
 }
 
 JSObject* GlobalHelperThreadState::finishModuleParseTask(JSContext* cx,
                                                          void* token) {
   JSScript* script = finishParseTask(cx, ParseTaskKind::Module, token);
diff --git a/js/src/vm/HelperThreads.h b/js/src/vm/HelperThreads.h
--- a/js/src/vm/HelperThreads.h
+++ b/js/src/vm/HelperThreads.h
@@ -39,17 +39,23 @@ struct ParseTask;
 struct PromiseHelperTask;
 namespace jit {
 class IonBuilder;
 }  // namespace jit
 namespace wasm {
 struct Tier2GeneratorTask;
 }  // namespace wasm
 
-enum class ParseTaskKind { Script, Module, ScriptDecode, MultiScriptsDecode };
+enum class ParseTaskKind {
+  Script,
+  Module,
+  ScriptDecode,
+  BinAST,
+  MultiScriptsDecode
+};
 
 namespace wasm {
 
 struct CompileTask;
 typedef Fifo<CompileTask*, 0, SystemAllocPolicy> CompileTaskPtrFifo;
 
 struct Tier2GeneratorTask {
   virtual ~Tier2GeneratorTask() = default;
@@ -308,16 +314,20 @@ class GlobalHelperThreadState {
   void trace(JSTracer* trc, js::gc::AutoTraceSession& session);
 
   JSScript* finishScriptParseTask(JSContext* cx, void* token);
   JSScript* finishScriptDecodeTask(JSContext* cx, void* token);
   bool finishMultiScriptsDecodeTask(JSContext* cx, void* token,
                                     MutableHandle<ScriptVector> scripts);
   JSObject* finishModuleParseTask(JSContext* cx, void* token);
 
+#if defined(JS_BUILD_BINAST)
+  JSScript* finishBinASTDecodeTask(JSContext* cx, void* token);
+#endif
+
   bool hasActiveThreads(const AutoLockHelperThreadState&);
   void waitForAllThreads();
   void waitForAllThreadsLocked(AutoLockHelperThreadState&);
 
   template <typename T>
   bool checkTaskThreadLimit(size_t maxThreads, bool isMaster = false) const;
 
  private:
@@ -572,16 +582,24 @@ bool StartOffThreadParseModule(JSContext
                                void* callbackData);
 
 bool StartOffThreadDecodeScript(JSContext* cx,
                                 const ReadOnlyCompileOptions& options,
                                 const JS::TranscodeRange& range,
                                 JS::OffThreadCompileCallback callback,
                                 void* callbackData);
 
+#if defined(JS_BUILD_BINAST)
+bool StartOffThreadDecodeBinAST(JSContext* cx,
+                                const ReadOnlyCompileOptions& options,
+                                const uint8_t* buf, size_t length,
+                                JS::OffThreadCompileCallback callback,
+                                void* callbackData);
+#endif /* JS_BUILD_BINAST */
+
 bool StartOffThreadDecodeMultiScripts(JSContext* cx,
                                       const ReadOnlyCompileOptions& options,
                                       JS::TranscodeSources& sources,
                                       JS::OffThreadCompileCallback callback,
                                       void* callbackData);
 
 /*
  * Called at the end of GC to enqueue any Parse tasks that were waiting on an
@@ -698,16 +716,26 @@ struct ModuleParseTask : public ParseTas
 struct ScriptDecodeTask : public ParseTask {
   const JS::TranscodeRange range;
 
   ScriptDecodeTask(JSContext* cx, const JS::TranscodeRange& range,
                    JS::OffThreadCompileCallback callback, void* callbackData);
   void parse(JSContext* cx) override;
 };
 
+#if defined(JS_BUILD_BINAST)
+struct BinASTDecodeTask : public ParseTask {
+  mozilla::Range<const uint8_t> data;
+
+  BinASTDecodeTask(JSContext* cx, const uint8_t* buf, size_t length,
+                   JS::OffThreadCompileCallback callback, void* callbackData);
+  void parse(JSContext* cx) override;
+};
+#endif /* JS_BUILD_BINAST */
+
 struct MultiScriptsDecodeTask : public ParseTask {
   JS::TranscodeSources* sources;
 
   MultiScriptsDecodeTask(JSContext* cx, JS::TranscodeSources& sources,
                          JS::OffThreadCompileCallback callback,
                          void* callbackData);
   void parse(JSContext* cx) override;
 };
