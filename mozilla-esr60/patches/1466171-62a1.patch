# HG changeset patch
# User Jon Coppeard <jcoppeard@mozilla.com>
# Date 1528122345 -3600
# Node ID 6d741e95201af5856af7c7a0477ddfa863e7d006
# Parent  366d2341aad78f6ea6abe92560a5fb0b93f4939a
Bug 1466171 - Allow collection of atoms while the main thread is parsing r=sfink

diff --git a/js/src/frontend/BinSource.cpp b/js/src/frontend/BinSource.cpp
--- a/js/src/frontend/BinSource.cpp
+++ b/js/src/frontend/BinSource.cpp
@@ -19,16 +19,17 @@
 #include "frontend/Parser.h"
 #include "frontend/SharedContext.h"
 
 #include "js/Result.h"
 #include "vm/RegExpObject.h"
 
 #include "frontend/ParseContext-inl.h"
 #include "frontend/ParseNode-inl.h"
+#include "vm/JSContext-inl.h"
 
 // # About compliance with EcmaScript
 //
 // For the moment, this parser implements ES5. Future versions will be extended
 // to ES6 and further on.
 //
 // By design, it does NOT implement Annex B.3.3. If possible, we would like
 // to avoid going down that rabbit hole.
@@ -69,16 +70,44 @@
 //
 // They should be treated lazily (whenever we open a subscope), like bindings.
 
 namespace js {
 namespace frontend {
 
 using UsedNamePtr = UsedNameTracker::UsedNameMap::Ptr;
 
+BinASTParserBase::BinASTParserBase(JSContext* cx, LifoAlloc& alloc,
+                                   UsedNameTracker& usedNames)
+    : AutoGCRooter(cx, AutoGCRooter::Tag::BinParser),
+      cx_(cx),
+      alloc_(alloc),
+      traceListHead_(nullptr),
+      usedNames_(usedNames),
+      nodeAlloc_(cx, alloc),
+      keepAtoms_(cx),
+      parseContext_(nullptr),
+      factory_(cx, alloc, nullptr, SourceKind::Binary) {
+  cx->frontendCollectionPool().addActiveCompilation();
+  tempPoolMark_ = alloc.mark();
+}
+
+BinASTParserBase::~BinASTParserBase() {
+  alloc_.release(tempPoolMark_);
+
+  /*
+   * The parser can allocate enormous amounts of memory for large functions.
+   * Eagerly free the memory now (which otherwise won't be freed until the
+   * next GC) to avoid unnecessary OOMs.
+   */
+  alloc_.freeAllIfHugeAndUnused();
+
+  cx_->frontendCollectionPool().removeActiveCompilation();
+}
+
 // ------------- Toplevel constructions
 
 template<typename Tok>
 JS::Result<ParseNode*> BinASTParser<Tok>::parse(const Vector<uint8_t>& data) {
   return parse(data.begin(), data.length());
 }
 
 template<typename Tok>
diff --git a/js/src/frontend/BinSource.h b/js/src/frontend/BinSource.h
--- a/js/src/frontend/BinSource.h
+++ b/js/src/frontend/BinSource.h
@@ -29,41 +29,18 @@
 #include "js/GCVector.h"
 #include "js/Result.h"
 
 namespace js {
 namespace frontend {
 
 class BinASTParserBase: private JS::AutoGCRooter {
  public:
-  BinASTParserBase(JSContext* cx, LifoAlloc& alloc, UsedNameTracker& usedNames)
-      : AutoGCRooter(cx, AutoGCRooter::Tag::BinParser),
-        cx_(cx),
-        alloc_(alloc),
-        traceListHead_(nullptr),
-        usedNames_(usedNames),
-        nodeAlloc_(cx, alloc),
-        keepAtoms_(cx),
-        parseContext_(nullptr),
-        factory_(cx, alloc, nullptr, SourceKind::Binary) {
-    cx->frontendCollectionPool().addActiveCompilation();
-    tempPoolMark_ = alloc.mark();
-  }
-  ~BinASTParserBase() {
-    alloc_.release(tempPoolMark_);
-
-    /*
-     * The parser can allocate enormous amounts of memory for large functions.
-     * Eagerly free the memory now (which otherwise won't be freed until the
-     * next GC) to avoid unnecessary OOMs.
-     */
-    alloc_.freeAllIfHugeAndUnused();
-
-    cx_->frontendCollectionPool().removeActiveCompilation();
-  }
+  BinASTParserBase(JSContext* cx, LifoAlloc& alloc, UsedNameTracker& usedNames);
+  ~BinASTParserBase();
 
  public:
   // Names
 
   bool hasUsedName(HandlePropertyName name);
 
   // --- GC.
 
diff --git a/js/src/frontend/SharedContext.h b/js/src/frontend/SharedContext.h
--- a/js/src/frontend/SharedContext.h
+++ b/js/src/frontend/SharedContext.h
@@ -367,29 +367,29 @@ class FunctionBox : public ObjectBox, pu
   // Whether this function has nested functions.
   bool hasInnerFunctions_ : 1;
 
   FunctionBox(JSContext* cx, ObjectBox* traceListHead, JSFunction* fun,
               uint32_t toStringStart, Directives directives, bool extraWarnings,
               GeneratorKind generatorKind, FunctionAsyncKind asyncKind);
 
   MutableHandle<LexicalScope::Data*> namedLambdaBindings() {
-    MOZ_ASSERT(context->keepAtoms);
+    MOZ_ASSERT(context->zone()->hasKeptAtoms());
     return MutableHandle<LexicalScope::Data*>::fromMarkedLocation(
         &namedLambdaBindings_);
   }
 
   MutableHandle<FunctionScope::Data*> functionScopeBindings() {
-    MOZ_ASSERT(context->keepAtoms);
+    MOZ_ASSERT(context->zone()->hasKeptAtoms());
     return MutableHandle<FunctionScope::Data*>::fromMarkedLocation(
         &functionScopeBindings_);
   }
 
   MutableHandle<VarScope::Data*> extraVarScopeBindings() {
-    MOZ_ASSERT(context->keepAtoms);
+    MOZ_ASSERT(context->zone()->hasKeptAtoms());
     return MutableHandle<VarScope::Data*>::fromMarkedLocation(
         &extraVarScopeBindings_);
   }
 
   void initFromLazyFunction();
   void initStandaloneFunction(Scope* enclosingScope);
   void initWithEnclosingParseContext(ParseContext* enclosing,
                                      FunctionSyntaxKind kind);
diff --git a/js/src/gc/GC.cpp b/js/src/gc/GC.cpp
--- a/js/src/gc/GC.cpp
+++ b/js/src/gc/GC.cpp
@@ -3091,21 +3091,19 @@ bool GCRuntime::triggerZoneGC(Zone* zone
 #ifdef JS_GC_ZEAL
   if (hasZealMode(ZealMode::Alloc)) {
     MOZ_RELEASE_ASSERT(triggerGC(reason));
     return true;
   }
 #endif
 
   if (zone->isAtomsZone()) {
-    /* We can't do a zone GC of the atoms zone. */
-    if (rt->mainContextFromOwnThread()->keepAtoms ||
-        rt->hasHelperThreadZones()) {
-      /* Skip GC and retrigger later, since atoms zone won't be collected
-       * if keepAtoms is true. */
+    /* We can't do a zone GC of just the atoms zone. */
+    if (rt->hasHelperThreadZones()) {
+      /* We can't collect atoms while off-thread parsing is allocating. */
       fullGCForAtomsRequested_ = true;
       return false;
     }
     stats().recordTrigger(used, threshold);
     MOZ_RELEASE_ASSERT(triggerGC(reason));
     return true;
   }
 
@@ -3634,17 +3632,17 @@ void GCRuntime::purgeRuntimeForMinorGC()
 
 void GCRuntime::purgeRuntime() {
   gcstats::AutoPhase ap(stats(), gcstats::PhaseKind::PURGE);
 
   for (GCRealmsIter realm(rt); !realm.done(); realm.next())
     realm->purge();
 
   for (GCZonesIter zone(rt); !zone.done(); zone.next()) {
-    zone->atomCache().clearAndShrink();
+    zone->purgeAtomCacheOrDefer();
     zone->externalStringCache().purge();
     zone->functionToStringCache().purge();
   }
 
   JSContext* cx = rt->mainContextFromOwnThread();
   freeUnusedLifoBlocksAfterSweeping(&cx->tempLifoAlloc());
   cx->interpreterStack().purge(rt);
   cx->frontendCollectionPool().purge();
diff --git a/js/src/gc/GCRuntime.h b/js/src/gc/GCRuntime.h
--- a/js/src/gc/GCRuntime.h
+++ b/js/src/gc/GCRuntime.h
@@ -577,16 +577,17 @@ class GCRuntime {
                                    AutoTraceSession& session);
   bool prepareZonesForCollection(JS::gcreason::Reason reason, bool* isFullOut,
                                  AutoLockForExclusiveAccess& lock);
   bool shouldPreserveJITCode(JS::Realm* realm, int64_t currentTime,
                              JS::gcreason::Reason reason,
                              bool canAllocateMoreCode);
   void traceRuntimeForMajorGC(JSTracer* trc, AutoTraceSession& session);
   void traceRuntimeAtoms(JSTracer* trc, AutoLockForExclusiveAccess& lock);
+  void traceKeptAtoms(JSTracer* trc);
   void traceRuntimeCommon(JSTracer* trc, TraceOrMarkRuntime traceOrMark,
                           AutoTraceSession& session);
   void maybeDoCycleCollection();
   void markCompartments();
   IncrementalProgress drainMarkStack(SliceBudget& sliceBudget,
                                      gcstats::PhaseKind phase);
   template <class CompartmentIterT>
   void markWeakReferences(gcstats::PhaseKind phase);
diff --git a/js/src/gc/RootMarking.cpp b/js/src/gc/RootMarking.cpp
--- a/js/src/gc/RootMarking.cpp
+++ b/js/src/gc/RootMarking.cpp
@@ -238,16 +238,17 @@ void js::gc::GCRuntime::traceRuntimeForM
   MOZ_ASSERT_IF(atomsZone->isCollecting(), session.maybeLock.isSome());
 
   // FinishRoots will have asserted that every root that we do not expect
   // is gone, so we can simply skip traceRuntime here.
   if (rt->isBeingDestroyed()) return;
 
   gcstats::AutoPhase ap(stats(), gcstats::PhaseKind::MARK_ROOTS);
   if (atomsZone->isCollecting()) traceRuntimeAtoms(trc, session.lock());
+  traceKeptAtoms(trc);
   JSCompartment::traceIncomingCrossCompartmentEdgesForZoneGC(trc);
   traceRuntimeCommon(trc, MarkRuntime, session);
 }
 
 void js::gc::GCRuntime::traceRuntimeForMinorGC(JSTracer* trc,
                                                AutoTraceSession& session) {
   MOZ_ASSERT(!TlsContext.get()->suppressGC);
 
@@ -287,16 +288,26 @@ void js::gc::GCRuntime::traceRuntimeAtom
                                           AutoLockForExclusiveAccess& lock) {
   gcstats::AutoPhase ap(stats(), gcstats::PhaseKind::MARK_RUNTIME_DATA);
   TracePermanentAtoms(trc);
   TraceAtoms(trc, lock);
   TraceWellKnownSymbols(trc);
   jit::JitRuntime::Trace(trc, lock);
 }
 
+void js::gc::GCRuntime::traceKeptAtoms(JSTracer* trc) {
+  // We don't have exact rooting information for atoms while parsing. When
+  // this is happeninng we set a flag on the zone and trace all atoms in the
+  // zone's cache.
+  for (GCZonesIter zone(trc->runtime()); !zone.done(); zone.next()) {
+    if (zone->hasKeptAtoms())
+      zone->traceAtomCache(trc);
+  }
+}
+
 void js::gc::GCRuntime::traceRuntimeCommon(JSTracer* trc,
                                            TraceOrMarkRuntime traceOrMark,
                                            AutoTraceSession& session) {
   {
     gcstats::AutoPhase ap(stats(), gcstats::PhaseKind::MARK_STACK);
 
     JSContext* cx = rt->mainContextFromOwnThread();
 
diff --git a/js/src/gc/Verifier.cpp b/js/src/gc/Verifier.cpp
--- a/js/src/gc/Verifier.cpp
+++ b/js/src/gc/Verifier.cpp
@@ -170,17 +170,17 @@ static VerifyNode* NextNode(VerifyNode* 
 
 void gc::GCRuntime::startVerifyPreBarriers() {
   if (verifyPreData || isIncrementalGCInProgress()) return;
 
   JSContext* cx = rt->mainContextFromOwnThread();
   if (temporaryAbortIfWasmGc(cx))
     return;
 
-  if (IsIncrementalGCUnsafe(rt) != AbortReason::None || cx->keepAtoms ||
+  if (IsIncrementalGCUnsafe(rt) != AbortReason::None ||
       rt->hasHelperThreadZones()) {
     return;
   }
 
   number++;
 
   VerifyPreTracer* trc = js_new<VerifyPreTracer>(rt);
   if (!trc) return;
@@ -329,17 +329,16 @@ void gc::GCRuntime::endVerifyPreBarriers
    */
   MOZ_ASSERT(trc->number == number);
   number++;
 
   verifyPreData = nullptr;
   incrementalState = State::NotActive;
 
   if (!compartmentCreated && IsIncrementalGCUnsafe(rt) == AbortReason::None &&
-      !rt->mainContextFromOwnThread()->keepAtoms &&
       !rt->hasHelperThreadZones()) {
     CheckEdgeTracer cetrc(rt);
 
     /* Start after the roots. */
     VerifyNode* node = NextNode(trc->root);
     while ((char*)node < trc->edgeptr) {
       cetrc.node = node;
       js::TraceChildren(&cetrc, node->thing, node->kind);
diff --git a/js/src/gc/Zone.cpp b/js/src/gc/Zone.cpp
--- a/js/src/gc/Zone.cpp
+++ b/js/src/gc/Zone.cpp
@@ -42,16 +42,18 @@ JS::Zone::Zone(JSRuntime* rt)
       weakCaches_(this),
       gcWeakKeys_(this, SystemAllocPolicy(), rt->randomHashCodeScrambler()),
       typeDescrObjects_(this, this),
       regExps(this),
       markedAtoms_(this),
       atomCache_(this),
       externalStringCache_(this),
       functionToStringCache_(this),
+      keepAtomsCount(this, 0),
+      purgeAtomsDeferred(this, 0),
       usage(&rt->gc.usage),
       threshold(),
       gcDelayBytes(0),
       tenuredStrings(this, 0),
       allocNurseryStrings(this, true),
       propertyTree_(this, this),
       baseShapes_(this, this),
       initialShapes_(this, this),
@@ -351,16 +353,45 @@ void Zone::setHelperThreadOwnerContext(J
 }
 
 bool Zone::ownedByCurrentHelperThread() {
   MOZ_ASSERT(usedByHelperThread());
   MOZ_ASSERT(TlsContext.get());
   return helperThreadOwnerContext_ == TlsContext.get();
 }
 
+void Zone::releaseAtoms() {
+  MOZ_ASSERT(hasKeptAtoms());
+
+  keepAtomsCount--;
+
+  if (!hasKeptAtoms() && purgeAtomsDeferred) {
+    atomCache().clearAndShrink();
+    purgeAtomsDeferred = false;
+  }
+}
+
+void Zone::purgeAtomCacheOrDefer() {
+  if (hasKeptAtoms()) {
+    purgeAtomsDeferred = true;
+    return;
+  }
+
+  atomCache().clearAndShrink();
+}
+
+void Zone::traceAtomCache(JSTracer* trc) {
+  MOZ_ASSERT(hasKeptAtoms());
+  for (auto r = atomCache().all(); !r.empty(); r.popFront()) {
+    JSAtom* atom = r.front().asPtrUnbarriered();
+    TraceRoot(trc, &atom, "kept atom");
+    MOZ_ASSERT(r.front().asPtrUnbarriered() == atom);
+  }
+}
+
 ZoneList::ZoneList() : head(nullptr), tail(nullptr) {}
 
 ZoneList::ZoneList(Zone* zone) : head(zone), tail(zone) {
   MOZ_RELEASE_ASSERT(!zone->isOnList());
   zone->listNext_ = nullptr;
 }
 
 ZoneList::~ZoneList() { MOZ_ASSERT(isEmpty()); }
diff --git a/js/src/gc/Zone.h b/js/src/gc/Zone.h
--- a/js/src/gc/Zone.h
+++ b/js/src/gc/Zone.h
@@ -485,34 +485,57 @@ struct Zone : public JS::shadow::Zone,
     jitCodeCounter.updateOnGCEnd(gc.tunables, lock);
   }
   js::gc::TriggerKind shouldTriggerGCForTooMuchMalloc() {
     auto& gc = runtimeFromAnyThread()->gc;
     return std::max(gcMallocCounter.shouldTriggerGC(gc.tunables),
                     jitCodeCounter.shouldTriggerGC(gc.tunables));
   }
 
+  void keepAtoms() { keepAtomsCount++; }
+  void releaseAtoms();
+  bool hasKeptAtoms() const { return keepAtomsCount; }
+
  private:
   // Bitmap of atoms marked by this zone.
   js::ZoneOrGCTaskData<js::SparseBitmap> markedAtoms_;
 
-  // Set of atoms recently used by this Zone. Purged on GC.
+  // Set of atoms recently used by this Zone. Purged on GC unless
+  // keepAtomsCount is non-zero.
   js::ZoneOrGCTaskData<js::AtomSet> atomCache_;
 
   // Cache storing allocated external strings. Purged on GC.
   js::ZoneOrGCTaskData<js::ExternalStringCache> externalStringCache_;
 
   // Cache for Function.prototype.toString. Purged on GC.
   js::ZoneOrGCTaskData<js::FunctionToStringCache> functionToStringCache_;
 
+  // Count of AutoKeepAtoms instances for this zone. When any instances exist,
+  // atoms in the runtime will be marked from this zone's atom mark bitmap,
+  // rather than when traced in the normal way. Threads parsing off the main
+  // thread do not increment this value, but the presence of any such threads
+  // also inhibits collection of atoms. We don't scan the stacks of exclusive
+  // threads, so we need to avoid collecting their objects in another way. The
+  // only GC thing pointers they have are to their exclusive compartment
+  // (which is not collected) or to the atoms compartment. Therefore, we avoid
+  // collecting the atoms zone when exclusive threads are running.
+  js::ZoneOrGCTaskData<unsigned> keepAtomsCount;
+
+  // Whether purging atoms was deferred due to keepAtoms being set. If this
+  // happen then the cache will be purged when keepAtoms drops to zero.
+  js::ZoneOrGCTaskData<bool> purgeAtomsDeferred;
+
  public:
   js::SparseBitmap& markedAtoms() { return markedAtoms_.ref(); }
 
   js::AtomSet& atomCache() { return atomCache_.ref(); }
 
+  void traceAtomCache(JSTracer* trc);
+  void purgeAtomCacheOrDefer();
+
   js::ExternalStringCache& externalStringCache() {
     return externalStringCache_.ref();
   };
 
   js::FunctionToStringCache& functionToStringCache() {
     return functionToStringCache_.ref();
   }
 
diff --git a/js/src/vm/JSAtom.cpp b/js/src/vm/JSAtom.cpp
--- a/js/src/vm/JSAtom.cpp
+++ b/js/src/vm/JSAtom.cpp
@@ -353,19 +353,22 @@ MOZ_ALWAYS_INLINE static JSAtom* Atomize
   // being initialized (in initializeAtoms()), |permanentAtoms| is not yet
   // initialized so this lookup is always skipped. Only once
   // transformToPermanentAtoms() is called does |permanentAtoms| get
   // initialized and then this lookup will go ahead.
   if (cx->isPermanentAtomsInitialized()) {
     AtomSet::Ptr pp = cx->permanentAtoms().readonlyThreadsafeLookup(lookup);
     if (pp) {
       JSAtom* atom = pp->asPtr(cx);
-      if (zonePtr)
-        mozilla::Unused << zone->atomCache().add(*zonePtr,
-                                                 AtomStateEntry(atom, false));
+      if (zonePtr &&
+          !zone->atomCache().add(*zonePtr, AtomStateEntry(atom, false))) {
+        ReportOutOfMemory(cx);
+        return nullptr;
+      }
+
       return atom;
     }
   }
 
   // Validate the length before taking the exclusive access lock, as throwing
   // an exception here may reenter this code.
   if (MOZ_UNLIKELY(!JSString::validateLength(cx, length))) return nullptr;
 
@@ -373,19 +376,21 @@ MOZ_ALWAYS_INLINE static JSAtom* Atomize
       AtomizeAndCopyCharsInner(cx, tbchars, length, pin, indexValue, lookup);
   if (!atom) return nullptr;
 
   if (MOZ_UNLIKELY(!cx->atomMarking().inlinedMarkAtomFallible(cx, atom))) {
     ReportOutOfMemory(cx);
     return nullptr;
   }
 
-  if (zonePtr)
-    mozilla::Unused << zone->atomCache().add(*zonePtr,
-                                             AtomStateEntry(atom, false));
+  if (zonePtr &&
+      !zone->atomCache().add(*zonePtr, AtomStateEntry(atom, false))) {
+    ReportOutOfMemory(cx);
+    return nullptr;
+  }
 
   return atom;
 }
 
 template <typename CharT>
 MOZ_ALWAYS_INLINE static JSAtom* AtomizeAndCopyCharsInner(
     JSContext* cx, const CharT* tbchars, size_t length, PinningBehavior pin,
     const Maybe<uint32_t>& indexValue, const AtomHasher::Lookup& lookup) {
diff --git a/js/src/vm/JSContext-inl.h b/js/src/vm/JSContext-inl.h
--- a/js/src/vm/JSContext-inl.h
+++ b/js/src/vm/JSContext-inl.h
@@ -504,9 +504,18 @@ inline JSScript* JSContext::currentScrip
     MOZ_ASSERT(script->containsPC(*ppc));
   }
 
   return script;
 }
 
 inline js::RuntimeCaches& JSContext::caches() { return runtime()->caches(); }
 
+inline js::AutoKeepAtoms::AutoKeepAtoms(JSContext* cx
+                                        MOZ_GUARD_OBJECT_NOTIFIER_PARAM_IN_IMPL)
+    : cx(cx) {
+  MOZ_GUARD_OBJECT_NOTIFIER_INIT;
+  cx->zone()->keepAtoms();
+}
+
+inline js::AutoKeepAtoms::~AutoKeepAtoms() { cx->zone()->releaseAtoms(); };
+
 #endif /* vm_JSContext_inl_h */
diff --git a/js/src/vm/JSContext.cpp b/js/src/vm/JSContext.cpp
--- a/js/src/vm/JSContext.cpp
+++ b/js/src/vm/JSContext.cpp
@@ -1120,17 +1120,16 @@ JSContext::JSContext(JSRuntime* runtime,
 #endif
 #if defined(DEBUG) || defined(JS_OOM_BREAKPOINT)
       runningOOMTest(false),
 #endif
       enableAccessValidation(false),
       inUnsafeRegion(0),
       generationalDisabled(0),
       compactingDisabledCount(0),
-      keepAtoms(0),
       suppressProfilerSampling(false),
       tempLifoAlloc_((size_t)TEMP_LIFO_ALLOC_PRIMARY_CHUNK_SIZE),
       debuggerMutations(0),
       ionPcScriptCache(nullptr),
       throwing(false),
       overRecursed_(false),
       propagatingForcedReturn_(false),
       liveVolatileJitFrameIter_(nullptr),
diff --git a/js/src/vm/JSContext.h b/js/src/vm/JSContext.h
--- a/js/src/vm/JSContext.h
+++ b/js/src/vm/JSContext.h
@@ -539,30 +539,17 @@ struct JSContext : public JS::RootingCon
 
   // Count of AutoDisableGenerationalGC instances on the thread's stack.
   js::ThreadData<unsigned> generationalDisabled;
 
   // Some code cannot tolerate compacting GC so it can be disabled temporarily
   // with AutoDisableCompactingGC which uses this counter.
   js::ThreadData<unsigned> compactingDisabledCount;
 
-  // Count of AutoKeepAtoms instances on the current thread's stack. When any
-  // instances exist, atoms in the runtime will not be collected. Threads
-  // parsing off the main thread do not increment this value, but the presence
-  // of any such threads also inhibits collection of atoms. We don't scan the
-  // stacks of exclusive threads, so we need to avoid collecting their
-  // objects in another way. The only GC thing pointers they have are to
-  // their exclusive compartment (which is not collected) or to the atoms
-  // compartment. Therefore, we avoid collecting the atoms zone when
-  // exclusive threads are running.
-  js::ThreadData<unsigned> keepAtoms;
-
-  bool canCollectAtoms() const {
-    return !keepAtoms && !runtime()->hasHelperThreadZones();
-  }
+  bool canCollectAtoms() const { return !runtime()->hasHelperThreadZones(); }
 
  private:
   // Pools used for recycling name maps and vectors when parsing and
   // emitting bytecode. Purged on GC when there are no active script
   // compilations.
   js::ThreadData<js::frontend::NameCollectionPool> frontendCollectionPool_;
 
  public:
@@ -1167,31 +1154,18 @@ class MOZ_RAII AutoLockScriptData {
   MOZ_DECL_USE_GUARD_OBJECT_NOTIFIER
 };
 
 class MOZ_RAII AutoKeepAtoms {
   JSContext* cx;
   MOZ_DECL_USE_GUARD_OBJECT_NOTIFIER
 
  public:
-  explicit AutoKeepAtoms(JSContext* cx MOZ_GUARD_OBJECT_NOTIFIER_PARAM)
-      : cx(cx) {
-    MOZ_GUARD_OBJECT_NOTIFIER_INIT;
-    cx->keepAtoms++;
-  }
-  ~AutoKeepAtoms() {
-    MOZ_ASSERT(cx->keepAtoms);
-    cx->keepAtoms--;
-
-    JSRuntime* rt = cx->runtime();
-    if (!cx->helperThread()) {
-      if (rt->gc.fullGCForAtomsRequested() && cx->canCollectAtoms())
-        rt->gc.triggerFullGCForAtoms(cx);
-    }
-  }
+  explicit inline AutoKeepAtoms(JSContext* cx MOZ_GUARD_OBJECT_NOTIFIER_PARAM);
+  inline ~AutoKeepAtoms();
 };
 
 // Debugging RAII class which marks the current thread as performing an Ion
 // compilation, for use by CurrentThreadCan{Read,Write}CompilationData
 class MOZ_RAII AutoEnterIonCompilation {
  public:
   explicit AutoEnterIonCompilation(
       bool safeForMinorGC MOZ_GUARD_OBJECT_NOTIFIER_PARAM) {
