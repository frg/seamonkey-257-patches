# HG changeset patch
# User Nika Layzell <nika@thelayzells.com>
# Date 1533251990 14400
# Node ID 8376dfb0af8293dfbb1550aed3b614c9c681523b
# Parent  a204eb0e3c1d71279f1e602cef4c4d24c426740c
Bug 1480624 - Part 2: Add a DestructValue method to XPConnect to run XPT Type destructors, r=mccr8

Summary:
DestructValue acts a lot like CleanupValue, however in addition to normal
cleanup work, it invokes the destructor of complex data types. This is important
to ensure that constructors and destructors are matched for these complex data
types.

CleanupValue is also used to clean up a value without destructing it, so cannot
be modified in-place.

Depends On D2689

Reviewers: mccr8!

Tags: #secure-revision

Bug #: 1480624

Differential Revision: https://phabricator.services.mozilla.com/D2690

diff --git a/js/xpconnect/src/XPCConvert.cpp b/js/xpconnect/src/XPCConvert.cpp
--- a/js/xpconnect/src/XPCConvert.cpp
+++ b/js/xpconnect/src/XPCConvert.cpp
@@ -1398,17 +1398,17 @@ bool XPCConvert::JSArray2Native(JS::Hand
   RootedValue current(cx);
   for (uint32_t i = 0; i < length; ++i) {
     if (!JS_GetElement(cx, jsarray, i, &current) ||
         !JSData2Native(aEltType.ElementPtr(buf, i), current, aEltType, aIID, 0,
                        pErr)) {
       // Array element conversion failed. Clean up all elements converted
       // before the error. Caller handles freeing 'buf'.
       for (uint32_t j = 0; j < i; ++j) {
-        CleanupValue(aEltType, aEltType.ElementPtr(buf, j));
+        DestructValue(aEltType, aEltType.ElementPtr(buf, j));
       }
       return false;
     }
   }
 
   return true;
 }
 
@@ -1460,29 +1460,29 @@ void xpc::InnerCleanupValue(const nsXPTT
       break;
 
     // Legacy Array Type
     case nsXPTType::T_LEGACY_ARRAY: {
       const nsXPTType& elty = aType.ArrayElementType();
       void* elements = *(void**)aValue;
 
       for (uint32_t i = 0; i < aArrayLen; ++i) {
-        CleanupValue(elty, elty.ElementPtr(elements, i));
+        DestructValue(elty, elty.ElementPtr(elements, i));
       }
       free(elements);
       break;
     }
 
     // Array Type
     case nsXPTType::T_ARRAY: {
       const nsXPTType& elty = aType.ArrayElementType();
       auto* array = (xpt::detail::UntypedTArray*)aValue;
 
       for (uint32_t i = 0; i < array->Length(); ++i) {
-        CleanupValue(elty, elty.ElementPtr(array->Elements(), i));
+        DestructValue(elty, elty.ElementPtr(array->Elements(), i));
       }
       array->Clear();
       break;
     }
 
     // Clear the JS::Value to `undefined`
     case nsXPTType::T_JSVAL:
       ((JS::Value*)aValue)->setUndefined();
@@ -1515,8 +1515,32 @@ XPT_FOR_EACH_COMPLEX_TYPE(XPT_INIT_TYPE)
 #undef XPT_INIT_TYPE
 
     // The remaining types have valid states where all bytes are '0'.
     default:
       aType.ZeroValue(aValue);
       break;
   }
 }
+
+// In XPT_FOR_EACH_COMPLEX_TYPE, typenames may be namespaced (such as
+// xpt::UntypedTArray). Namespaced typenames cannot be used to explicitly invoke
+// destructors, so this method acts as a helper to let us call the destructor of
+// these objects.
+template<typename T>
+static void _DestructValueHelper(void* aValue) {
+  static_cast<T*>(aValue)->~T();
+}
+
+void xpc::DestructValue(const nsXPTType& aType, void* aValue,
+                        uint32_t aArrayLen) {
+  // Get aValue into an clean, empty state.
+  xpc::CleanupValue(aType, aValue, aArrayLen);
+
+  // Run destructors on complex types.
+  switch (aType.Tag()) {
+#define XPT_RUN_DESTRUCTOR(tag, type) \
+    case tag: _DestructValueHelper<type>(aValue); break;
+XPT_FOR_EACH_COMPLEX_TYPE(XPT_RUN_DESTRUCTOR)
+#undef XPT_RUN_DESTRUCTOR
+    default: break; // dtor is a no-op on other types.
+  }
+}
diff --git a/js/xpconnect/src/XPCWrappedNative.cpp b/js/xpconnect/src/XPCWrappedNative.cpp
--- a/js/xpconnect/src/XPCWrappedNative.cpp
+++ b/js/xpconnect/src/XPCWrappedNative.cpp
@@ -1183,17 +1183,17 @@ bool CallMethodHelper::Call() {
 }
 
 CallMethodHelper::~CallMethodHelper() {
   for (nsXPTCVariant& param : mDispatchParams) {
     uint32_t arraylen = 0;
     if (!GetArraySizeFromParam(param.type, UndefinedHandleValue, &arraylen))
       continue;
 
-    xpc::CleanupValue(param.type, &param.val, arraylen);
+    xpc::DestructValue(param.type, &param.val, arraylen);
   }
 }
 
 bool CallMethodHelper::GetArraySizeFromParam(const nsXPTType& type,
                                              HandleValue maybeArray,
                                              uint32_t* result) {
   if (type.Tag() != nsXPTType::T_LEGACY_ARRAY &&
       type.Tag() != nsXPTType::T_PSTRING_SIZE_IS &&
diff --git a/js/xpconnect/src/xpcprivate.h b/js/xpconnect/src/xpcprivate.h
--- a/js/xpconnect/src/xpcprivate.h
+++ b/js/xpconnect/src/xpcprivate.h
@@ -2967,16 +2967,27 @@ void InnerCleanupValue(const nsXPTType& 
 // XPCConvert methods, or xpc::CleanupValue.
 //
 // The pointer `aValue` must point to a block of memory at least aType.Stride()
 // bytes large, and correctly aligned.
 //
 // This method accepts the same types as xpc::CleanupValue.
 void InitializeValue(const nsXPTType& aType, void* aValue);
 
+// If a value was initialized with InitializeValue, it should be destroyed with
+// DestructValue. This method acts like CleanupValue, except that destructors
+// for complex types are also invoked, leaving them in an invalid state.
+//
+// This method should be called when destroying types initialized with
+// InitializeValue.
+//
+// The pointer 'aValue' must point to a valid value of type 'aType'.
+void DestructValue(const nsXPTType& aType, void* aValue,
+                   uint32_t aArrayLen = 0);
+
 }  // namespace xpc
 
 namespace mozilla {
 namespace dom {
 extern bool DefineStaticJSVals(JSContext* cx);
 }  // namespace dom
 }  // namespace mozilla
 
