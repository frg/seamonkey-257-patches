# HG changeset patch
# User Kyle Machulis <kyle@nonpolynomial.com>
# Date 1546536948 0
# Node ID b83cac4f938ac130e9d8dbd9887f7051d1ddcb63
# Parent  88b976554a77c0466db470820ff298089da8a682
Bug 1095602 - Remove STATE_SECURE flags from nsIWebProgressListener; r=Ehsan

Only STATE_SECURE_HIGH is used, and that's only in instances where
STATE_IS_SECURE is also used, so we can remove the security level
flags and just assume STATE_IS_SECURE is also STATE_SECURE_HIGH.

Differential Revision: https://phabricator.services.mozilla.com/D15600

diff --git a/security/manager/ssl/nsNSSCallbacks.cpp b/security/manager/ssl/nsNSSCallbacks.cpp
--- a/security/manager/ssl/nsNSSCallbacks.cpp
+++ b/security/manager/ssl/nsNSSCallbacks.cpp
@@ -1266,18 +1266,17 @@ void HandshakeCallback(PRFileDesc* fd, v
                              ioLayerHelpers.treatUnsafeNegotiationAsBroken();
 
   RememberCertErrorsTable::GetInstance().LookupCertErrorBits(infoObject);
 
   uint32_t state;
   if (renegotiationUnsafe) {
     state = nsIWebProgressListener::STATE_IS_BROKEN;
   } else {
-    state = nsIWebProgressListener::STATE_IS_SECURE |
-            nsIWebProgressListener::STATE_SECURE_HIGH;
+    state = nsIWebProgressListener::STATE_IS_SECURE;
     SSLVersionRange defVersion;
     rv = SSL_VersionRangeGetDefault(ssl_variant_stream, &defVersion);
     if (rv == SECSuccess && versions.max >= defVersion.max) {
       // we know this site no longer requires a version fallback
       ioLayerHelpers.removeInsecureFallbackSite(infoObject->GetHostName(),
                                                 infoObject->GetPort());
     }
   }
diff --git a/security/manager/ssl/nsSecureBrowserUIImpl.cpp b/security/manager/ssl/nsSecureBrowserUIImpl.cpp
--- a/security/manager/ssl/nsSecureBrowserUIImpl.cpp
+++ b/security/manager/ssl/nsSecureBrowserUIImpl.cpp
@@ -162,17 +162,17 @@ nsresult nsSecureBrowserUIImpl::MapInter
       *aState = STATE_IS_BROKEN;
       break;
 
     case lis_mixed_security:
       *aState = STATE_IS_BROKEN;
       break;
 
     case lis_high_security:
-      *aState = STATE_IS_SECURE | STATE_SECURE_HIGH;
+      *aState = STATE_IS_SECURE;
       break;
 
     default:
     case lis_no_security:
       *aState = STATE_IS_INSECURE;
       break;
   }
 
diff --git a/uriloader/base/nsIWebProgressListener.idl b/uriloader/base/nsIWebProgressListener.idl
--- a/uriloader/base/nsIWebProgressListener.idl
+++ b/uriloader/base/nsIWebProgressListener.idl
@@ -231,41 +231,16 @@ interface nsIWebProgressListener : nsISu
    * change the top level security state of the connection.
    *
    * STATE_CERT_DISTRUST_IMMINENT
    *   The certificate in use will be distrusted in the near future.
    */
   const unsigned long STATE_CERT_DISTRUST_IMMINENT    = 0x00008000;
 
   /**
-   * Security Strength Flags
-   *
-   * These flags describe the security strength and accompany STATE_IS_SECURE
-   * in a call to the onSecurityChange method.  These flags are mutually
-   * exclusive.
-   *
-   * These flags are not meant to provide a precise description of data
-   * transfer security.  These are instead intended as a rough indicator that
-   * may be used to, for example, color code a security indicator or otherwise
-   * provide basic data transfer security feedback to the user.
-   *
-   * STATE_SECURE_HIGH
-   *   This flag indicates a high degree of security.
-   *
-   * STATE_SECURE_MED
-   *   This flag indicates a medium degree of security.
-   *
-   * STATE_SECURE_LOW
-   *   This flag indicates a low degree of security.
-   */
-  const unsigned long STATE_SECURE_HIGH     = 0x00040000;
-  const unsigned long STATE_SECURE_MED      = 0x00010000;
-  const unsigned long STATE_SECURE_LOW      = 0x00020000;
-
-  /**
     * State bits for EV == Extended Validation == High Assurance
     *
     * These flags describe the level of identity verification
     * in a call to the onSecurityChange method. 
     *
     * STATE_IDENTITY_EV_TOPLEVEL
     *   The topmost document uses an EV cert.
     *   NOTE: Available since Gecko 1.9
