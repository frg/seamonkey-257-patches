# HG changeset patch
# User Marco Bonardo <mbonardo@mozilla.com>
# Date 1580890241 -7200
# Node ID 42c51f5a5fb406f0dbb91a718424b914d4da912b
# Parent  c4702677dac90db54568c7e1168b56b584ac8bcc
Bug 1607902 - Fix Sqlite VFS zName parsing for Sqlite versions >= 3.31.0 (uplift only patch). r=asuth a=RyanVM

Sqlite 3.31.0 changed the zName layout, breaking our custom parsing of it.
Due to this the VFS started crashing on Sqlite library upgrade.

This fixes the parser, but only for Firefox versions it can be uplifted to,
we can't fix versions already released, that may start crashing once system
Sqlite gets updated to a newer version.

Differential Revision: https://phabricator.services.mozilla.com//D60952

diff --git a/storage/TelemetryVFS.cpp b/storage/TelemetryVFS.cpp
--- a/storage/TelemetryVFS.cpp
+++ b/storage/TelemetryVFS.cpp
@@ -276,34 +276,42 @@ const char *DatabasePathFromWALPath(cons
     // Change the cursors and go through the loop again.
     cursor--;
     dbPathCursor--;
   }
 
   MOZ_CRASH("Should never get here!");
 }
 
-already_AddRefed<QuotaObject> GetQuotaObjectFromNameAndParameters(
-    const char *zName, const char *zURIParameterKey) {
-  MOZ_ASSERT(zName);
-  MOZ_ASSERT(zURIParameterKey);
-
+already_AddRefed<QuotaObject> GetQuotaObjectFromName(const char* zName,
+                                                     bool deriveFromWal) {
+  // From Sqlite 3.31.0 the zName format changed to work consistently across
+  // database, wal and journal names.
+  // We must support both ways because this code will be uplifted to ensure
+  // that if system Sqlite is upgraded before us, we keep working properly.
+  // Once the Firefox minimum Sqlite version is 3.31.0, we can remove the else
+  // branch, DatabasePathFromWALPath, and the deriveFromWal argument.
+  const char* filename = zName;
+  if (deriveFromWal && sqlite3_libversion_number() < 3031000) {
+    filename = DatabasePathFromWALPath(zName);
+  }
+  MOZ_ASSERT(filename);
   const char *persistenceType =
-      sqlite3_uri_parameter(zURIParameterKey, "persistenceType");
+      sqlite3_uri_parameter(filename, "persistenceType");
   if (!persistenceType) {
     return nullptr;
   }
 
-  const char *group = sqlite3_uri_parameter(zURIParameterKey, "group");
+  const char* group = sqlite3_uri_parameter(filename, "group");
   if (!group) {
     NS_WARNING("SQLite URI had 'persistenceType' but not 'group'?!");
     return nullptr;
   }
 
-  const char *origin = sqlite3_uri_parameter(zURIParameterKey, "origin");
+  const char* origin = sqlite3_uri_parameter(filename, "origin");
   if (!origin) {
     NS_WARNING(
         "SQLite URI had 'persistenceType' and 'group' but not "
         "'origin'?!");
     return nullptr;
   }
 
   QuotaManager *quotaManager = QuotaManager::Get();
@@ -319,25 +327,17 @@ void MaybeEstablishQuotaControl(const ch
                                 int flags) {
   MOZ_ASSERT(pFile);
   MOZ_ASSERT(!pFile->quotaObject);
 
   if (!(flags & (SQLITE_OPEN_URI | SQLITE_OPEN_WAL))) {
     return;
   }
 
-  MOZ_ASSERT(zName);
-
-  const char *zURIParameterKey =
-      (flags & SQLITE_OPEN_WAL) ? DatabasePathFromWALPath(zName) : zName;
-
-  MOZ_ASSERT(zURIParameterKey);
-
-  pFile->quotaObject =
-      GetQuotaObjectFromNameAndParameters(zName, zURIParameterKey);
+  pFile->quotaObject = GetQuotaObjectFromName(zName, flags & SQLITE_OPEN_WAL);
 }
 
 /*
 ** Close a telemetry_file.
 */
 int xClose(sqlite3_file *pFile) {
   telemetry_file *p = (telemetry_file *)pFile;
   int rc;
@@ -662,20 +662,17 @@ int xOpen(sqlite3_vfs *vfs, const char *
 }
 
 int xDelete(sqlite3_vfs *vfs, const char *zName, int syncDir) {
   sqlite3_vfs *orig_vfs = static_cast<sqlite3_vfs *>(vfs->pAppData);
   int rc;
   RefPtr<QuotaObject> quotaObject;
 
   if (StringEndsWith(nsDependentCString(zName), NS_LITERAL_CSTRING("-wal"))) {
-    const char *zURIParameterKey = DatabasePathFromWALPath(zName);
-    MOZ_ASSERT(zURIParameterKey);
-
-    quotaObject = GetQuotaObjectFromNameAndParameters(zName, zURIParameterKey);
+    quotaObject = GetQuotaObjectFromName(zName, true);
   }
 
   rc = orig_vfs->xDelete(orig_vfs, zName, syncDir);
   if (rc == SQLITE_OK && quotaObject) {
     MOZ_ALWAYS_TRUE(quotaObject->MaybeUpdateSize(0, /* aTruncate */ true));
   }
 
   return rc;
