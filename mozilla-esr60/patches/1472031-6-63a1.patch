# HG changeset patch
# User Jeff Walden <jwalden@mit.edu>
# Date 1530227333 25200
# Node ID ba24991f7f4d976ee709a23d62b91fb39d4641d6
# Parent  c5a02ae247604bfbd02d4ffdd2ae3fce51d3231e
Bug 1472031 - Add SourceUnits::matchHexDigits and remove SourceUnits::peekCodeUnits used for the same ends (but that also performed a needless copy).  r=arai

diff --git a/js/src/frontend/TokenStream.cpp b/js/src/frontend/TokenStream.cpp
--- a/js/src/frontend/TokenStream.cpp
+++ b/js/src/frontend/TokenStream.cpp
@@ -929,23 +929,20 @@ uint32_t GeneralTokenStreamChars<CharT, 
   int32_t unit = getCodeUnit();
   if (unit != 'u') {
     // NOTE: |unit| may be EOF here.
     ungetCodeUnit(unit);
     MOZ_ASSERT(sourceUnits.previousCodeUnit() == '\\');
     return 0;
   }
 
-  CharT cp[3];
+  char16_t v;
   unit = getCodeUnit();
-  if (JS7_ISHEX(unit) && sourceUnits.peekCodeUnits(3, cp) && JS7_ISHEX(cp[0]) &&
-      JS7_ISHEX(cp[1]) && JS7_ISHEX(cp[2])) {
-    *codePoint = (JS7_UNHEX(unit) << 12) | (JS7_UNHEX(cp[0]) << 8) |
-                 (JS7_UNHEX(cp[1]) << 4) | JS7_UNHEX(cp[2]);
-    sourceUnits.skipCodeUnits(3);
+  if (JS7_ISHEX(unit) && sourceUnits.matchHexDigits(3, &v)) {
+    *codePoint = (JS7_UNHEX(unit) << 12) | v;
     return 5;
   }
 
   if (unit == '{')
     return matchExtendedUnicodeEscape(codePoint);
 
   // NOTE: |unit| may be EOF here, so this ungets either one or two units.
   ungetCodeUnit(unit);
@@ -2373,22 +2370,19 @@ bool TokenStreamSpecific<CharT, AnyChars
 
             continue;
           } // end of delimited Unicode escape handling
 
           // Otherwise it must be a fixed-length \uXXXX Unicode escape.
           // If it isn't, this is usually an error -- but if this is a
           // template literal, we must defer error reporting because
           // malformed escapes are okay in *tagged* template literals.
-          CharT cp[3];
-          if (JS7_ISHEX(c2) && sourceUnits.peekCodeUnits(3, cp) &&
-              JS7_ISHEX(cp[0]) && JS7_ISHEX(cp[1]) && JS7_ISHEX(cp[2])) {
-            unit = (JS7_UNHEX(c2) << 12) | (JS7_UNHEX(cp[0]) << 8) |
-                   (JS7_UNHEX(cp[1]) << 4) | JS7_UNHEX(cp[2]);
-            sourceUnits.skipCodeUnits(3);
+          char16_t v;
+          if (JS7_ISHEX(c2) && sourceUnits.matchHexDigits(3, &v)) {
+            unit = (JS7_UNHEX(c2) << 12) | v;
           } else {
             // Beware: |c2| may not be an ASCII code point here!
             ungetCodeUnit(c2);
             uint32_t start = sourceUnits.offset() - 2;
             if (parsingTemplate) {
               TokenStreamAnyChars& anyChars = anyCharsAccess();
               anyChars.setInvalidTemplateEscape(start,
                                                 InvalidEscapeType::Unicode);
@@ -2397,21 +2391,19 @@ bool TokenStreamSpecific<CharT, AnyChars
             reportInvalidEscapeError(start, InvalidEscapeType::Unicode);
             return false;
           }
           break;
         }  // case 'u'
 
         // Hexadecimal character specification.
         case 'x': {
-          CharT cp[2];
-          if (sourceUnits.peekCodeUnits(2, cp) && JS7_ISHEX(cp[0]) &&
-              JS7_ISHEX(cp[1])) {
-            unit = (JS7_UNHEX(cp[0]) << 4) + JS7_UNHEX(cp[1]);
-            sourceUnits.skipCodeUnits(2);
+          char16_t v;
+          if (sourceUnits.matchHexDigits(2, &v)) {
+            unit = v;
           } else {
             uint32_t start = sourceUnits.offset() - 2;
             if (parsingTemplate) {
               TokenStreamAnyChars& anyChars = anyCharsAccess();
               anyChars.setInvalidTemplateEscape(start,
                                                 InvalidEscapeType::Hexadecimal);
               continue;
             }
diff --git a/js/src/frontend/TokenStream.h b/js/src/frontend/TokenStream.h
--- a/js/src/frontend/TokenStream.h
+++ b/js/src/frontend/TokenStream.h
@@ -176,16 +176,17 @@
 #include <stdio.h>
 
 #include "jspubtd.h"
 
 #include "frontend/ErrorReporter.h"
 #include "frontend/TokenKind.h"
 #include "js/UniquePtr.h"
 #include "js/Vector.h"
+#include "util/Text.h"
 #include "util/Unicode.h"
 #include "vm/ErrorReporting.h"
 #include "vm/JSContext.h"
 #include "vm/RegExpShared.h"
 #include "vm/StringType.h"
 
 struct KeywordInfo;
 
@@ -942,22 +943,33 @@ class SourceUnits {
   CharT getCodeUnit() {
     return *ptr++;      // this will nullptr-crash if poisoned
   }
 
   CharT peekCodeUnit() const {
     return *ptr;        // this will nullptr-crash if poisoned
   }
 
-  bool peekCodeUnits(uint8_t n, CharT* out) const {
+  /** Match |n| hexadecimal digits and store their value in |*out|. */
+  bool matchHexDigits(uint8_t n, char16_t* out) {
     MOZ_ASSERT(ptr, "shouldn't peek into poisoned SourceUnits");
+    MOZ_ASSERT(n <= 4, "hexdigit value can't overflow char16_t");
     if (n > remaining())
       return false;
 
-    std::copy_n(ptr, n, out);
+    char16_t v = 0;
+    for (uint8_t i = 0; i < n; i++) {
+      if (!JS7_ISHEX(ptr[i]))
+        return false;
+
+      v = (v << 4) | JS7_UNHEX(ptr[i]);
+    }
+
+    *out = v;
+    ptr += n;
     return true;
   }
 
   bool matchCodeUnits(const char* chars, uint8_t length) {
     MOZ_ASSERT(ptr, "shouldn't match into poisoned SourceUnits");
     if (length > remaining())
       return false;
 
