# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1527681118 -7200
# Node ID 6a276f4e1d093a64cab8762a1ccc8f8bc161daa3
# Parent  7f95150643f5bc55fb35c6e855d7d4af782a8a88
Bug 1464036 part 2 - Give JSID_EMPTY its own jsid tag and clean up jsid code a bit. r=jonco

diff --git a/js/public/Id.h b/js/public/Id.h
--- a/js/public/Id.h
+++ b/js/public/Id.h
@@ -22,20 +22,25 @@
 
 #include "jstypes.h"
 
 #include "js/HeapAPI.h"
 #include "js/RootingAPI.h"
 #include "js/TypeDecls.h"
 #include "js/Utility.h"
 
+// All jsids with the low bit set are integer ids. This means the other type
+// tags must all be even.
+#define JSID_TYPE_INT_BIT 0x1
+
+// Use 0 for JSID_TYPE_STRING to avoid a bitwise op for atom <-> id conversions.
 #define JSID_TYPE_STRING 0x0
-#define JSID_TYPE_INT 0x1
 #define JSID_TYPE_VOID 0x2
 #define JSID_TYPE_SYMBOL 0x4
+#define JSID_TYPE_EMPTY 0x6
 #define JSID_TYPE_MASK 0x7
 
 struct jsid {
   size_t asBits;
 
   constexpr jsid() : asBits(JSID_TYPE_VOID) {}
 
   static constexpr MOZ_ALWAYS_INLINE jsid fromRawBits(size_t bits) {
@@ -49,64 +54,65 @@ struct jsid {
 } JS_HAZ_GC_POINTER;
 #define JSID_BITS(id) (id.asBits)
 
 // Avoid using canonical 'id' for jsid parameters since this is a magic word in
 // Objective-C++ which, apparently, wants to be able to #include jsapi.h.
 #define id iden
 
 static MOZ_ALWAYS_INLINE bool JSID_IS_STRING(jsid id) {
-  return (JSID_BITS(id) & JSID_TYPE_MASK) == 0;
+  return (JSID_BITS(id) & JSID_TYPE_MASK) == JSID_TYPE_STRING;
 }
 
 static MOZ_ALWAYS_INLINE JSString* JSID_TO_STRING(jsid id) {
+  // Use XOR instead of `& ~JSID_TYPE_MASK` because small immediates can be
+  // encoded more efficiently on some platorms.
   MOZ_ASSERT(JSID_IS_STRING(id));
-  return (JSString*)JSID_BITS(id);
+  return (JSString*)(JSID_BITS(id) ^ JSID_TYPE_STRING);
 }
 
 /**
  * Only JSStrings that have been interned via the JSAPI can be turned into
  * jsids by API clients.
  *
  * N.B. if a jsid is backed by a string which has not been interned, that
  * string must be appropriately rooted to avoid being collected by the GC.
  */
 JS_PUBLIC_API jsid INTERNED_STRING_TO_JSID(JSContext* cx, JSString* str);
 
 static MOZ_ALWAYS_INLINE bool JSID_IS_INT(jsid id) {
-  return !!(JSID_BITS(id) & JSID_TYPE_INT);
+  return !!(JSID_BITS(id) & JSID_TYPE_INT_BIT);
 }
 
 static MOZ_ALWAYS_INLINE int32_t JSID_TO_INT(jsid id) {
   MOZ_ASSERT(JSID_IS_INT(id));
   uint32_t bits = static_cast<uint32_t>(JSID_BITS(id)) >> 1;
   return static_cast<int32_t>(bits);
 }
 
 #define JSID_INT_MIN 0
 #define JSID_INT_MAX INT32_MAX
 
 static MOZ_ALWAYS_INLINE bool INT_FITS_IN_JSID(int32_t i) { return i >= 0; }
 
 static MOZ_ALWAYS_INLINE jsid INT_TO_JSID(int32_t i) {
   jsid id;
   MOZ_ASSERT(INT_FITS_IN_JSID(i));
-  uint32_t bits = (static_cast<uint32_t>(i) << 1) | JSID_TYPE_INT;
+  uint32_t bits = (static_cast<uint32_t>(i) << 1) | JSID_TYPE_INT_BIT;
   JSID_BITS(id) = static_cast<size_t>(bits);
   return id;
 }
 
 static MOZ_ALWAYS_INLINE bool JSID_IS_SYMBOL(jsid id) {
-  return (JSID_BITS(id) & JSID_TYPE_MASK) == JSID_TYPE_SYMBOL &&
-         JSID_BITS(id) != JSID_TYPE_SYMBOL;
+  return (JSID_BITS(id) & JSID_TYPE_MASK) == JSID_TYPE_SYMBOL;
 }
 
 static MOZ_ALWAYS_INLINE JS::Symbol* JSID_TO_SYMBOL(jsid id) {
   MOZ_ASSERT(JSID_IS_SYMBOL(id));
-  return (JS::Symbol*)(JSID_BITS(id) & ~(size_t)JSID_TYPE_MASK);
+  return (JS::Symbol*)(JSID_BITS(id) ^ JSID_TYPE_SYMBOL);
 }
 
 static MOZ_ALWAYS_INLINE jsid SYMBOL_TO_JSID(JS::Symbol* sym) {
   jsid id;
   MOZ_ASSERT(sym != nullptr);
   MOZ_ASSERT((size_t(sym) & JSID_TYPE_MASK) == 0);
   MOZ_ASSERT(!js::gc::IsInsideNursery(reinterpret_cast<js::gc::Cell*>(sym)));
   JSID_BITS(id) = (size_t(sym) | JSID_TYPE_SYMBOL);
@@ -120,27 +126,29 @@ static MOZ_ALWAYS_INLINE bool JSID_IS_GC
 static MOZ_ALWAYS_INLINE JS::GCCellPtr JSID_TO_GCTHING(jsid id) {
   void* thing = (void*)(JSID_BITS(id) & ~(size_t)JSID_TYPE_MASK);
   if (JSID_IS_STRING(id)) return JS::GCCellPtr(thing, JS::TraceKind::String);
   MOZ_ASSERT(JSID_IS_SYMBOL(id));
   return JS::GCCellPtr(thing, JS::TraceKind::Symbol);
 }
 
 static MOZ_ALWAYS_INLINE bool JSID_IS_VOID(const jsid id) {
-  MOZ_ASSERT_IF(((size_t)JSID_BITS(id) & JSID_TYPE_MASK) == JSID_TYPE_VOID,
+  MOZ_ASSERT_IF((JSID_BITS(id) & JSID_TYPE_MASK) == JSID_TYPE_VOID,
                 JSID_BITS(id) == JSID_TYPE_VOID);
-  return (size_t)JSID_BITS(id) == JSID_TYPE_VOID;
+  return JSID_BITS(id) == JSID_TYPE_VOID;
 }
 
 static MOZ_ALWAYS_INLINE bool JSID_IS_EMPTY(const jsid id) {
-  return (size_t)JSID_BITS(id) == JSID_TYPE_SYMBOL;
+  MOZ_ASSERT_IF((JSID_BITS(id) & JSID_TYPE_MASK) == JSID_TYPE_EMPTY,
+                JSID_BITS(id) == JSID_TYPE_EMPTY);
+  return JSID_BITS(id) == JSID_TYPE_EMPTY;
 }
 
 constexpr const jsid JSID_VOID;
-extern JS_PUBLIC_DATA const jsid JSID_EMPTY;
+constexpr const jsid JSID_EMPTY = jsid::fromRawBits(JSID_TYPE_EMPTY);
 
 extern JS_PUBLIC_DATA const JS::HandleId JSID_VOIDHANDLE;
 extern JS_PUBLIC_DATA const JS::HandleId JSID_EMPTYHANDLE;
 
 namespace JS {
 
 template <>
 struct GCPolicy<jsid> {
diff --git a/js/src/jit/MacroAssembler.cpp b/js/src/jit/MacroAssembler.cpp
--- a/js/src/jit/MacroAssembler.cpp
+++ b/js/src/jit/MacroAssembler.cpp
@@ -2809,17 +2809,18 @@ void MacroAssembler::Push(jsid id, Regis
     // If we're pushing a gcthing, then we can't just push the tagged jsid
     // value since the GC won't have any idea that the push instruction
     // carries a reference to a gcthing.  Need to unpack the pointer,
     // push it using ImmGCPtr, and then rematerialize the id at runtime.
 
     if (JSID_IS_STRING(id)) {
       JSString* str = JSID_TO_STRING(id);
       MOZ_ASSERT(((size_t)str & JSID_TYPE_MASK) == 0);
-      MOZ_ASSERT(JSID_TYPE_STRING == 0x0);
+      static_assert(JSID_TYPE_STRING == 0,
+                    "need to orPtr JSID_TYPE_STRING tag if it's not 0");
       Push(ImmGCPtr(str));
     } else {
       MOZ_ASSERT(JSID_IS_SYMBOL(id));
       JS::Symbol* sym = JSID_TO_SYMBOL(id);
       movePtr(ImmGCPtr(sym), scratchReg);
       orPtr(Imm32(JSID_TYPE_SYMBOL), scratchReg);
       Push(scratchReg);
     }
diff --git a/js/src/jsapi.h b/js/src/jsapi.h
--- a/js/src/jsapi.h
+++ b/js/src/jsapi.h
@@ -4670,17 +4670,17 @@ extern JS_PUBLIC_API JSFlatString* JS_Fl
 extern JS_PUBLIC_API const JS::Latin1Char* JS_GetLatin1FlatStringChars(
     const JS::AutoRequireNoGC& nogc, JSFlatString* str);
 
 extern JS_PUBLIC_API const char16_t* JS_GetTwoByteFlatStringChars(
     const JS::AutoRequireNoGC& nogc, JSFlatString* str);
 
 static MOZ_ALWAYS_INLINE JSFlatString* JSID_TO_FLAT_STRING(jsid id) {
   MOZ_ASSERT(JSID_IS_STRING(id));
-  return (JSFlatString*)(JSID_BITS(id));
+  return (JSFlatString*)JSID_TO_STRING(id);
 }
 
 static MOZ_ALWAYS_INLINE JSFlatString* JS_ASSERT_STRING_IS_FLAT(JSString* str) {
   MOZ_ASSERT(JS_StringIsFlat(str));
   return (JSFlatString*)str;
 }
 
 static MOZ_ALWAYS_INLINE JSString* JS_FORGET_STRING_FLATNESS(
diff --git a/js/src/jsfriendapi.h b/js/src/jsfriendapi.h
--- a/js/src/jsfriendapi.h
+++ b/js/src/jsfriendapi.h
@@ -2469,37 +2469,37 @@ bool IdMatchesAtom(jsid id, JSString* at
  *
  * - When given a PropertyName*, NameToId can be used which which does not need
  *   to do any dynamic checks.
  *
  * Thus, it is only the rare third case which needs this function, which
  * handles any JSAtom* that is known not to be representable with an int jsid.
  */
 static MOZ_ALWAYS_INLINE jsid NON_INTEGER_ATOM_TO_JSID(JSAtom* atom) {
-  MOZ_ASSERT(((size_t)atom & 0x7) == 0);
-  jsid id = JSID_FROM_BITS((size_t)atom);
+  MOZ_ASSERT(((size_t)atom & JSID_TYPE_MASK) == 0);
+  jsid id = JSID_FROM_BITS((size_t)atom | JSID_TYPE_STRING);
   MOZ_ASSERT(js::detail::IdMatchesAtom(id, atom));
   return id;
 }
 
 static MOZ_ALWAYS_INLINE jsid NON_INTEGER_ATOM_TO_JSID(JSString* atom) {
-  MOZ_ASSERT(((size_t)atom & 0x7) == 0);
-  jsid id = JSID_FROM_BITS((size_t)atom);
+  MOZ_ASSERT(((size_t)atom & JSID_TYPE_MASK) == 0);
+  jsid id = JSID_FROM_BITS((size_t)atom | JSID_TYPE_STRING);
   MOZ_ASSERT(js::detail::IdMatchesAtom(id, atom));
   return id;
 }
 
 /* All strings stored in jsids are atomized, but are not necessarily property
  * names. */
 static MOZ_ALWAYS_INLINE bool JSID_IS_ATOM(jsid id) {
   return JSID_IS_STRING(id);
 }
 
 static MOZ_ALWAYS_INLINE bool JSID_IS_ATOM(jsid id, JSAtom* atom) {
-  return id == JSID_FROM_BITS((size_t)atom);
+  return id == NON_INTEGER_ATOM_TO_JSID(atom);
 }
 
 static MOZ_ALWAYS_INLINE JSAtom* JSID_TO_ATOM(jsid id) {
   return (JSAtom*)JSID_TO_STRING(id);
 }
 
 JS_STATIC_ASSERT(sizeof(jsid) == sizeof(void*));
 
diff --git a/js/src/vm/Id.cpp b/js/src/vm/Id.cpp
--- a/js/src/vm/Id.cpp
+++ b/js/src/vm/Id.cpp
@@ -2,16 +2,14 @@
  * vim: set ts=8 sts=4 et sw=4 tw=99:
  * This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "js/Id.h"
 #include "js/RootingAPI.h"
 
-const jsid JSID_EMPTY = jsid::fromRawBits(size_t(JSID_TYPE_SYMBOL));
-
 static const jsid voidIdValue = JSID_VOID;
 static const jsid emptyIdValue = JSID_EMPTY;
 const JS::HandleId JSID_VOIDHANDLE =
     JS::HandleId::fromMarkedLocation(&voidIdValue);
 const JS::HandleId JSID_EMPTYHANDLE =
     JS::HandleId::fromMarkedLocation(&emptyIdValue);
diff --git a/js/src/vm/JSAtom-inl.h b/js/src/vm/JSAtom-inl.h
--- a/js/src/vm/JSAtom-inl.h
+++ b/js/src/vm/JSAtom-inl.h
@@ -20,17 +20,17 @@ namespace js {
 
 inline jsid AtomToId(JSAtom* atom) {
   JS_STATIC_ASSERT(JSID_INT_MIN == 0);
 
   uint32_t index;
   if (atom->isIndex(&index) && index <= JSID_INT_MAX)
     return INT_TO_JSID(int32_t(index));
 
-  return JSID_FROM_BITS(size_t(atom));
+  return JSID_FROM_BITS(size_t(atom) | JSID_TYPE_STRING);
 }
 
 // Use the NameToId method instead!
 inline jsid AtomToId(PropertyName* name) = delete;
 
 inline bool ValueToIdPure(const Value& v, jsid* id) {
   if (v.isString()) {
     if (v.toString()->isAtom()) {
diff --git a/js/src/vm/JSAtom.cpp b/js/src/vm/JSAtom.cpp
--- a/js/src/vm/JSAtom.cpp
+++ b/js/src/vm/JSAtom.cpp
@@ -545,17 +545,17 @@ bool js::IndexToIdSlow(JSContext* cx, ui
 
   char16_t buf[UINT32_CHAR_BUFFER_LENGTH];
   RangedPtr<char16_t> end(ArrayEnd(buf), buf, ArrayEnd(buf));
   RangedPtr<char16_t> start = BackfillIndexInCharBuffer(index, end);
 
   JSAtom* atom = AtomizeChars(cx, start.get(), end - start);
   if (!atom) return false;
 
-  idp.set(JSID_FROM_BITS((size_t)atom));
+  idp.set(JSID_FROM_BITS((size_t)atom | JSID_TYPE_STRING));
   return true;
 }
 
 template <AllowGC allowGC>
 static JSAtom* ToAtomSlow(
     JSContext* cx, typename MaybeRooted<Value, allowGC>::HandleType arg) {
   MOZ_ASSERT(!arg.isString());
 
