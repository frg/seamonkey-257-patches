# HG changeset patch
# User Boris Zbarsky <bzbarsky@mit.edu>
# Date 1527906944 14400
# Node ID 5aebceb74c3a300842742f0dcc2cbee67da31d97
# Parent  6f7f12c84c8beb41a3c2d12b1ada7267bbc767be
Bug 1466213 part 2.  Remove nsIDOMGeoPositionError.  r=qdot

diff --git a/dom/geolocation/PositionError.cpp b/dom/geolocation/PositionError.cpp
--- a/dom/geolocation/PositionError.cpp
+++ b/dom/geolocation/PositionError.cpp
@@ -6,54 +6,39 @@
 
 #include "mozilla/dom/PositionError.h"
 #include "mozilla/dom/PositionErrorBinding.h"
 #include "nsGeolocation.h"
 
 namespace mozilla {
 namespace dom {
 
-NS_INTERFACE_MAP_BEGIN_CYCLE_COLLECTION(PositionError)
-  NS_WRAPPERCACHE_INTERFACE_MAP_ENTRY
-  NS_INTERFACE_MAP_ENTRY_AMBIGUOUS(nsISupports, nsIDOMGeoPositionError)
-  NS_INTERFACE_MAP_ENTRY(nsIDOMGeoPositionError)
-NS_INTERFACE_MAP_END
-
 NS_IMPL_CYCLE_COLLECTION_WRAPPERCACHE(PositionError, mParent)
-NS_IMPL_CYCLE_COLLECTING_ADDREF(PositionError)
-NS_IMPL_CYCLE_COLLECTING_RELEASE(PositionError)
+NS_IMPL_CYCLE_COLLECTION_ROOT_NATIVE(PositionError, AddRef)
+NS_IMPL_CYCLE_COLLECTION_UNROOT_NATIVE(PositionError, Release)
 
 PositionError::PositionError(Geolocation* aParent, int16_t aCode)
     : mCode(aCode), mParent(aParent) {}
 
 PositionError::~PositionError() = default;
 
-NS_IMETHODIMP
-PositionError::GetCode(int16_t* aCode) {
-  NS_ENSURE_ARG_POINTER(aCode);
-  *aCode = Code();
-  return NS_OK;
-}
-
-NS_IMETHODIMP
-PositionError::GetMessage(nsAString& aMessage) {
+void PositionError::GetMessage(nsAString& aMessage) const {
   switch (mCode) {
-    case nsIDOMGeoPositionError::PERMISSION_DENIED:
+    case PositionErrorBinding::PERMISSION_DENIED:
       aMessage = NS_LITERAL_STRING("User denied geolocation prompt");
       break;
-    case nsIDOMGeoPositionError::POSITION_UNAVAILABLE:
+    case PositionErrorBinding::POSITION_UNAVAILABLE:
       aMessage = NS_LITERAL_STRING("Unknown error acquiring position");
       break;
-    case nsIDOMGeoPositionError::TIMEOUT:
+    case PositionErrorBinding::TIMEOUT:
       aMessage = NS_LITERAL_STRING("Position acquisition timed out");
       break;
     default:
       break;
   }
-  return NS_OK;
 }
 
 nsWrapperCache* PositionError::GetParentObject() const { return mParent; }
 
 JSObject* PositionError::WrapObject(JSContext* aCx,
                                     JS::Handle<JSObject*> aGivenProto) {
   return PositionErrorBinding::Wrap(aCx, this, aGivenProto);
 }
diff --git a/dom/geolocation/PositionError.h b/dom/geolocation/PositionError.h
--- a/dom/geolocation/PositionError.h
+++ b/dom/geolocation/PositionError.h
@@ -2,49 +2,47 @@
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef mozilla_dom_PositionError_h
 #define mozilla_dom_PositionError_h
 
-#include "nsIDOMGeoPositionError.h"
 #include "nsWrapperCache.h"
 #include "nsISupportsImpl.h"
 #include "nsCycleCollectionParticipant.h"
 #include "mozilla/dom/CallbackObject.h"
 
 class nsIDOMGeoPositionErrorCallback;
 
 namespace mozilla {
 namespace dom {
 class PositionErrorCallback;
 class Geolocation;
 typedef CallbackObjectHolder<PositionErrorCallback,
                              nsIDOMGeoPositionErrorCallback>
     GeoPositionErrorCallback;
 
-class PositionError final : public nsIDOMGeoPositionError,
-                            public nsWrapperCache {
+class PositionError final : public nsWrapperCache {
  public:
-  NS_DECL_CYCLE_COLLECTING_ISUPPORTS
-  NS_DECL_CYCLE_COLLECTION_SCRIPT_HOLDER_CLASS(PositionError)
-
-  NS_DECL_NSIDOMGEOPOSITIONERROR
+  NS_INLINE_DECL_CYCLE_COLLECTING_NATIVE_REFCOUNTING(PositionError)
+  NS_DECL_CYCLE_COLLECTION_SCRIPT_HOLDER_NATIVE_CLASS(PositionError)
 
   PositionError(Geolocation* aParent, int16_t aCode);
 
   nsWrapperCache* GetParentObject() const;
 
   virtual JSObject* WrapObject(JSContext* aCx,
                                JS::Handle<JSObject*> aGivenProto) override;
 
   int16_t Code() const { return mCode; }
 
+  void GetMessage(nsAString& aMessage) const;
+
   void NotifyCallback(const GeoPositionErrorCallback& callback);
 
  private:
   ~PositionError();
   int16_t mCode;
   RefPtr<Geolocation> mParent;
 };
 
diff --git a/dom/geolocation/nsGeolocation.cpp b/dom/geolocation/nsGeolocation.cpp
--- a/dom/geolocation/nsGeolocation.cpp
+++ b/dom/geolocation/nsGeolocation.cpp
@@ -5,29 +5,29 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "nsGeolocation.h"
 
 #include "mozilla/ClearOnShutdown.h"
 #include "mozilla/dom/ContentChild.h"
 #include "mozilla/dom/PermissionMessageUtils.h"
 #include "mozilla/dom/PositionError.h"
+#include "mozilla/dom/PositionErrorBinding.h"
 #include "mozilla/Preferences.h"
 #include "mozilla/Services.h"
 #include "mozilla/Telemetry.h"
 #include "mozilla/UniquePtr.h"
 #include "mozilla/Unused.h"
 #include "mozilla/WeakPtr.h"
 #include "mozilla/EventStateManager.h"
 #include "nsComponentManagerUtils.h"
 #include "nsContentPermissionHelper.h"
 #include "nsContentUtils.h"
 #include "nsGlobalWindow.h"
 #include "nsIDocument.h"
-#include "nsIDOMGeoPositionError.h"
 #include "nsINamed.h"
 #include "nsIObserverService.h"
 #include "nsIScriptError.h"
 #include "nsPIDOMWindow.h"
 #include "nsServiceManagerUtils.h"
 #include "nsThreadUtils.h"
 #include "nsXULAppAPI.h"
 
@@ -242,17 +242,17 @@ NS_INTERFACE_MAP_END
 
 NS_IMPL_CYCLE_COLLECTING_ADDREF(nsGeolocationRequest)
 NS_IMPL_CYCLE_COLLECTING_RELEASE(nsGeolocationRequest)
 NS_IMPL_CYCLE_COLLECTION(nsGeolocationRequest, mCallback, mErrorCallback,
                          mLocator)
 
 void nsGeolocationRequest::Notify() {
   SetTimeoutTimer();
-  NotifyErrorAndShutdown(nsIDOMGeoPositionError::TIMEOUT);
+  NotifyErrorAndShutdown(PositionErrorBinding::TIMEOUT);
 }
 
 void nsGeolocationRequest::NotifyErrorAndShutdown(uint16_t aErrorCode) {
   MOZ_ASSERT(!mShutdown, "timeout after shutdown");
   if (!mIsWatchPositionRequest) {
     Shutdown();
     mLocator->RemoveRequest(this);
   }
@@ -310,17 +310,17 @@ nsGeolocationRequest::Cancel() {
     Telemetry::Accumulate(Telemetry::GEOLOCATION_REQUEST_GRANTED,
                           mProtocolType);
   }
 
   if (mLocator->ClearPendingRequest(this)) {
     return NS_OK;
   }
 
-  NotifyError(nsIDOMGeoPositionError::PERMISSION_DENIED);
+  NotifyError(PositionErrorBinding::PERMISSION_DENIED);
   return NS_OK;
 }
 
 NS_IMETHODIMP
 nsGeolocationRequest::Allow(JS::HandleValue aChoices) {
   MOZ_ASSERT(aChoices.isUndefined());
 
   if (mRequester) {
@@ -385,27 +385,27 @@ nsGeolocationRequest::Allow(JS::HandleVa
     if (!mIsWatchPositionRequest) {
       return NS_OK;
     }
 
   } else {
     // if it is not a watch request and timeout is 0,
     // invoke the errorCallback (if present) with TIMEOUT code
     if (mOptions && mOptions->mTimeout == 0 && !mIsWatchPositionRequest) {
-      NotifyError(nsIDOMGeoPositionError::TIMEOUT);
+      NotifyError(PositionErrorBinding::TIMEOUT);
       return NS_OK;
     }
   }
 
   // Kick off the geo device, if it isn't already running
   nsresult rv = gs->StartDevice(GetPrincipal());
 
   if (NS_FAILED(rv)) {
     // Location provider error
-    NotifyError(nsIDOMGeoPositionError::POSITION_UNAVAILABLE);
+    NotifyError(PositionErrorBinding::POSITION_UNAVAILABLE);
     return NS_OK;
   }
 
   if (mIsWatchPositionRequest || !canUseCache) {
     // let the locator know we're pending
     // we will now be owned by the locator
     mLocator->NotifyAllowedRequest(this);
   }
@@ -467,17 +467,17 @@ void nsGeolocationRequest::SendLocation(
     nsCOMPtr<nsIDOMGeoPositionCoords> coords;
     aPosition->GetCoords(getter_AddRefs(coords));
     if (coords) {
       wrapped = new mozilla::dom::Position(ToSupports(mLocator), aPosition);
     }
   }
 
   if (!wrapped) {
-    NotifyError(nsIDOMGeoPositionError::POSITION_UNAVAILABLE);
+    NotifyError(PositionErrorBinding::POSITION_UNAVAILABLE);
     return;
   }
 
   if (!mIsWatchPositionRequest) {
     // Cancel timer and position updates in case the position
     // callback spins the event loop
     Shutdown();
   }
@@ -732,17 +732,17 @@ nsresult nsGeolocationService::StartDevi
   if (!mProvider) {
     return NS_ERROR_FAILURE;
   }
 
   nsresult rv;
 
   if (NS_FAILED(rv = mProvider->Startup()) ||
       NS_FAILED(rv = mProvider->Watch(this))) {
-    NotifyError(nsIDOMGeoPositionError::POSITION_UNAVAILABLE);
+    NotifyError(PositionErrorBinding::POSITION_UNAVAILABLE);
     return rv;
   }
 
   obs->NotifyObservers(mProvider, "geolocation-device-events", u"starting");
 
   return NS_OK;
 }
 
diff --git a/dom/interfaces/geolocation/moz.build b/dom/interfaces/geolocation/moz.build
--- a/dom/interfaces/geolocation/moz.build
+++ b/dom/interfaces/geolocation/moz.build
@@ -7,14 +7,13 @@
 with Files("**"):
     BUG_COMPONENT = ("Core", "Geolocation")
 
 XPIDL_SOURCES += [
     'nsIDOMGeoGeolocation.idl',
     'nsIDOMGeoPosition.idl',
     'nsIDOMGeoPositionCallback.idl',
     'nsIDOMGeoPositionCoords.idl',
-    'nsIDOMGeoPositionError.idl',
     'nsIDOMGeoPositionErrorCallback.idl',
 ]
 
 XPIDL_MODULE = 'dom_geolocation'
 
diff --git a/dom/interfaces/geolocation/nsIDOMGeoPositionError.idl b/dom/interfaces/geolocation/nsIDOMGeoPositionError.idl
deleted file mode 100644
--- a/dom/interfaces/geolocation/nsIDOMGeoPositionError.idl
+++ /dev/null
@@ -1,24 +0,0 @@
-/* This Source Code Form is subject to the terms of the Mozilla Public
- * License, v. 2.0. If a copy of the MPL was not distributed with this
- * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
-
-
-#include "domstubs.idl"
-
-// undef the GetMessage macro defined in winuser.h from the MS Platform SDK
-%{C++
-#ifdef GetMessage
-#undef GetMessage
-#endif
-%}
-
-[shim(PositionError), uuid(85255CC3-07BA-49FD-BC9B-18D2963DAF7F)]
-interface nsIDOMGeoPositionError : nsISupports
-{
-  const unsigned short PERMISSION_DENIED  = 1;
-  const unsigned short POSITION_UNAVAILABLE = 2;
-  const unsigned short TIMEOUT = 3;
-
-  readonly attribute short code;
-  readonly attribute AString message;
-};
diff --git a/dom/interfaces/geolocation/nsIDOMGeoPositionErrorCallback.idl b/dom/interfaces/geolocation/nsIDOMGeoPositionErrorCallback.idl
--- a/dom/interfaces/geolocation/nsIDOMGeoPositionErrorCallback.idl
+++ b/dom/interfaces/geolocation/nsIDOMGeoPositionErrorCallback.idl
@@ -1,13 +1,13 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 
 #include "domstubs.idl"
 
-interface nsIDOMGeoPositionError;
+webidl PositionError;
 
 [scriptable, function, uuid(7D9B09D9-4843-43EB-A7A7-67F7DDA6B3C4)]
 interface nsIDOMGeoPositionErrorCallback : nsISupports {
-  void handleEvent(in nsIDOMGeoPositionError positionError);
+  void handleEvent(in PositionError positionError);
 };
diff --git a/dom/ipc/ContentParent.cpp b/dom/ipc/ContentParent.cpp
--- a/dom/ipc/ContentParent.cpp
+++ b/dom/ipc/ContentParent.cpp
@@ -45,16 +45,17 @@
 #include "mozilla/dom/ExternalHelperAppParent.h"
 #include "mozilla/dom/GetFilesHelper.h"
 #include "mozilla/dom/GeolocationBinding.h"
 #include "mozilla/dom/MemoryReportRequest.h"
 #include "mozilla/dom/Notification.h"
 #include "mozilla/dom/PContentBridgeParent.h"
 #include "mozilla/dom/PContentPermissionRequestParent.h"
 #include "mozilla/dom/PCycleCollectWithLogsParent.h"
+#include "mozilla/dom/PositionError.h"
 #include "mozilla/dom/ServiceWorkerRegistrar.h"
 #include "mozilla/dom/power/PowerManagerService.h"
 #include "mozilla/dom/Permissions.h"
 #include "mozilla/dom/PresentationParent.h"
 #include "mozilla/dom/PPresentationParent.h"
 #include "mozilla/dom/PushNotifier.h"
 #include "mozilla/dom/quota/QuotaManagerService.h"
 #include "mozilla/dom/URLClassifierParent.h"
@@ -117,17 +118,16 @@
 #include "nsIClipboard.h"
 #include "nsICookie.h"
 #include "nsContentPermissionHelper.h"
 #include "nsIContentProcess.h"
 #include "nsICycleCollectorListener.h"
 #include "nsIDocShellTreeOwner.h"
 #include "nsIDocument.h"
 #include "nsIDOMGeoGeolocation.h"
-#include "nsIDOMGeoPositionError.h"
 #include "nsIDragService.h"
 #include "mozilla/dom/WakeLock.h"
 #include "nsIDOMWindow.h"
 #include "nsIExternalProtocolService.h"
 #include "nsIFormProcessor.h"
 #include "nsIGfxInfo.h"
 #include "nsIIdleService.h"
 #include "nsIInterfaceRequestorUtils.h"
@@ -3518,22 +3518,18 @@ mozilla::ipc::IPCResult ContentParent::R
 
 NS_IMETHODIMP
 ContentParent::HandleEvent(nsIDOMGeoPosition* postion) {
   Unused << SendGeolocationUpdate(GeoPosition(postion));
   return NS_OK;
 }
 
 NS_IMETHODIMP
-ContentParent::HandleEvent(nsIDOMGeoPositionError* postionError) {
-  int16_t errorCode;
-  nsresult rv;
-  rv = postionError->GetCode(&errorCode);
-  NS_ENSURE_SUCCESS(rv, rv);
-  Unused << SendGeolocationError(errorCode);
+ContentParent::HandleEvent(PositionError* positionError) {
+  Unused << SendGeolocationError(positionError->Code());
   return NS_OK;
 }
 
 nsConsoleService* ContentParent::GetConsoleService() {
   if (mConsoleService) {
     return mConsoleService.get();
   }
 
diff --git a/dom/system/NetworkGeolocationProvider.js b/dom/system/NetworkGeolocationProvider.js
--- a/dom/system/NetworkGeolocationProvider.js
+++ b/dom/system/NetworkGeolocationProvider.js
@@ -4,17 +4,18 @@
 
 "use strict";
 
 ChromeUtils.import("resource://gre/modules/XPCOMUtils.jsm");
 ChromeUtils.import("resource://gre/modules/Services.jsm");
 
 Cu.importGlobalProperties(["XMLHttpRequest"]);
 
-const POSITION_UNAVAILABLE = Ci.nsIDOMGeoPositionError.POSITION_UNAVAILABLE;
+// PositionError has no interface object, so we can't use that here.
+const POSITION_UNAVAILABLE = 2;
 
 var gLoggingEnabled = false;
 
 /*
    The gLocationRequestTimeout controls how long we wait on receiving an update
    from the Wifi subsystem.  If this timer fires, we believe the Wifi scan has
    had a problem and we no longer can use Wifi to position the user this time
    around (we will continue to be hopeful that Wifi will recover).
diff --git a/dom/system/linux/GpsdLocationProvider.cpp b/dom/system/linux/GpsdLocationProvider.cpp
--- a/dom/system/linux/GpsdLocationProvider.cpp
+++ b/dom/system/linux/GpsdLocationProvider.cpp
@@ -6,18 +6,18 @@
 
 #include "GpsdLocationProvider.h"
 #include <errno.h>
 #include <gps.h>
 #include "MLSFallback.h"
 #include "mozilla/Atomics.h"
 #include "mozilla/FloatingPoint.h"
 #include "mozilla/LazyIdleThread.h"
+#include "mozilla/dom/PositionErrorBinding.h"
 #include "nsGeoPosition.h"
-#include "nsIDOMGeoPositionError.h"
 #include "nsProxyRelease.h"
 #include "nsThreadUtils.h"
 
 namespace mozilla {
 namespace dom {
 
 //
 // MLSGeolocationUpdate
@@ -157,17 +157,17 @@ class GpsdLocationProvider::PollRunnable
   NS_IMETHOD Run() override {
     int err;
 
     switch (GPSD_API_MAJOR_VERSION) {
       case 5:
         err = PollLoop5();
         break;
       default:
-        err = nsIDOMGeoPositionError::POSITION_UNAVAILABLE;
+        err = PositionErrorBinding::POSITION_UNAVAILABLE;
         break;
     }
 
     if (err) {
       NS_DispatchToMainThread(
           MakeAndAddRef<NotifyErrorRunnable>(mLocationProvider, err));
     }
 
@@ -270,34 +270,34 @@ class GpsdLocationProvider::PollRunnable
                             PR_Now() / PR_USEC_PER_MSEC)));
     }
 
     gps_stream(&gpsData, WATCH_DISABLE, NULL);
     gps_close(&gpsData);
 
     return err;
 #else
-    return nsIDOMGeoPositionError::POSITION_UNAVAILABLE;
+    return PositionErrorBinding::POSITION_UNAVAILABLE;
 #endif  // GPSD_MAJOR_API_VERSION
   }
 
   static int ErrnoToError(int aErrno) {
     switch (aErrno) {
       case EACCES:
         MOZ_FALLTHROUGH;
       case EPERM:
         MOZ_FALLTHROUGH;
       case EROFS:
-        return nsIDOMGeoPositionError::PERMISSION_DENIED;
+        return PositionErrorBinding::PERMISSION_DENIED;
       case ETIME:
         MOZ_FALLTHROUGH;
       case ETIMEDOUT:
-        return nsIDOMGeoPositionError::TIMEOUT;
+        return PositionErrorBinding::TIMEOUT;
       default:
-        return nsIDOMGeoPositionError::POSITION_UNAVAILABLE;
+        return PositionErrorBinding::POSITION_UNAVAILABLE;
     }
   }
 
  private:
   nsMainThreadPtrHandle<GpsdLocationProvider> mLocationProvider;
   Atomic<bool> mRunning;
 };
 
diff --git a/dom/system/mac/CoreLocationLocationProvider.mm b/dom/system/mac/CoreLocationLocationProvider.mm
--- a/dom/system/mac/CoreLocationLocationProvider.mm
+++ b/dom/system/mac/CoreLocationLocationProvider.mm
@@ -4,21 +4,21 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "nsAutoPtr.h"
 #include "nsCOMPtr.h"
 #include "nsGeoPosition.h"
 #include "nsIConsoleService.h"
 #include "nsServiceManagerUtils.h"
-#include "nsIDOMGeoPositionError.h"
 #include "CoreLocationLocationProvider.h"
 #include "nsCocoaFeatures.h"
 #include "prtime.h"
 #include "mozilla/Telemetry.h"
+#include "mozilla/dom/PositionErrorBinding.h"
 #include "MLSFallback.h"
 
 #include <CoreLocation/CLError.h>
 #include <CoreLocation/CLLocation.h>
 #include <CoreLocation/CLLocationManager.h>
 #include <CoreLocation/CLLocationManagerDelegate.h>
 
 #include <objc/objc.h>
@@ -62,17 +62,17 @@ static const CLLocationAccuracy kDEFAULT
   NS_ENSURE_TRUE_VOID(console);
 
   NSString* message =
     [@"Failed to acquire position: " stringByAppendingString: [aError localizedDescription]];
 
   console->LogStringMessage(NS_ConvertUTF8toUTF16([message UTF8String]).get());
 
   if ([aError code] == kCLErrorDenied) {
-    mProvider->NotifyError(nsIDOMGeoPositionError::PERMISSION_DENIED);
+    mProvider->NotifyError(dom::PositionErrorBinding::PERMISSION_DENIED);
     return;
   }
 
   // The CL provider does not fallback to GeoIP, so use NetworkGeolocationProvider for this.
   // The concept here is: on error, hand off geolocation to MLS, which will then report
   // back a location or error. We can't call this with no delay however, as this method
   // is called with an error code of 0 in both failed geolocation cases, and also when
   // geolocation is not immediately available.
diff --git a/dom/system/windows/WindowsLocationProvider.cpp b/dom/system/windows/WindowsLocationProvider.cpp
--- a/dom/system/windows/WindowsLocationProvider.cpp
+++ b/dom/system/windows/WindowsLocationProvider.cpp
@@ -1,21 +1,21 @@
 /* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "WindowsLocationProvider.h"
 #include "nsGeoPosition.h"
-#include "nsIDOMGeoPositionError.h"
 #include "nsComponentManagerUtils.h"
 #include "prtime.h"
 #include "MLSFallback.h"
 #include "mozilla/Telemetry.h"
+#include "mozilla/dom/PositionErrorBinding.h"
 
 namespace mozilla {
 namespace dom {
 
 NS_IMPL_ISUPPORTS(WindowsLocationProvider::MLSUpdate, nsIGeolocationUpdate);
 
 WindowsLocationProvider::MLSUpdate::MLSUpdate(nsIGeolocationUpdate* aCallback)
     : mCallback(aCallback) {}
@@ -112,21 +112,21 @@ LocationEvent::OnStatusChanged(REFIID aR
     return S_OK;
   }
 
   // Cannot watch location by MLS provider.  We must return error by
   // Location API.
   uint16_t err;
   switch (aStatus) {
     case REPORT_ACCESS_DENIED:
-      err = nsIDOMGeoPositionError::PERMISSION_DENIED;
+      err = PositionErrorBinding::PERMISSION_DENIED;
       break;
     case REPORT_NOT_SUPPORTED:
     case REPORT_ERROR:
-      err = nsIDOMGeoPositionError::POSITION_UNAVAILABLE;
+      err = PositionErrorBinding::POSITION_UNAVAILABLE;
       break;
     default:
       return S_OK;
   }
   mCallback->NotifyError(err);
   return S_OK;
 }
 
diff --git a/dom/tests/mochitest/geolocation/test_errorcheck.html b/dom/tests/mochitest/geolocation/test_errorcheck.html
--- a/dom/tests/mochitest/geolocation/test_errorcheck.html
+++ b/dom/tests/mochitest/geolocation/test_errorcheck.html
@@ -25,18 +25,21 @@ resume_geolocationProvider(function() {
   force_prompt(true, test1);
 });
 
 function test1() {
     send404_geolocationProvider(test2);
 }
 
 function errorCallback(error) {
-    is(error.code, 
-        SpecialPowers.Ci.nsIDOMGeoPositionError.POSITION_UNAVAILABLE, "Geolocation error handler fired");
+    // PositionError has no interface object, so we can't get constants off that.
+    is(error.code, error.POSITION_UNAVAILABLE,
+       "Geolocation error handler fired");
+    is(error.POSITION_UNAVAILABLE, 2,
+       "Value of POSITION_UNAVAILABLE should be correct");
     SimpleTest.finish();
 }
 
 function successCallback(position) {
     test2();
 }
 
 function test2() {
diff --git a/dom/tests/unit/test_geolocation_position_unavailable.js b/dom/tests/unit/test_geolocation_position_unavailable.js
--- a/dom/tests/unit/test_geolocation_position_unavailable.js
+++ b/dom/tests/unit/test_geolocation_position_unavailable.js
@@ -1,15 +1,17 @@
 function successCallback() {
   Assert.ok(false);
   do_test_finished();
 }
 
 function errorCallback(err) {
-  Assert.equal(Ci.nsIDOMGeoPositionError.POSITION_UNAVAILABLE, err.code);
+  // PositionError has no interface object, so we can't get constants off that.
+  Assert.equal(err.POSITION_UNAVAILABLE, err.code);
+  Assert.equal(2, err.code);
   do_test_finished();
 }
 
 function run_test()
 {
   do_test_pending();
 
   if (Cc["@mozilla.org/xre/app-info;1"].getService(Ci.nsIXULRuntime)
