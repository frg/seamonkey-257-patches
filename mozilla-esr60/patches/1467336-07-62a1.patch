# HG changeset patch
# User Jeff Walden <jwalden@mit.edu>
# Date 1528367044 25200
# Node ID 5a295d6c16887bec916975e723825881142a50f2
# Parent  9b0a03d933e7e8ec17206ec403be292c09a9b703
Bug 1467336 - Process the code units comprising regular expression literals in a manner that optimizes the pure-ASCII case.  r=arai

diff --git a/js/src/frontend/TokenStream.cpp b/js/src/frontend/TokenStream.cpp
--- a/js/src/frontend/TokenStream.cpp
+++ b/js/src/frontend/TokenStream.cpp
@@ -603,16 +603,29 @@ void TokenStreamChars<char16_t, AnyChars
   unicode::UTF16Encode(codePoint, units, &numUnits);
 
   MOZ_ASSERT(numUnits == 1 || numUnits == 2);
 
   while (numUnits-- > 0)
     ungetCodeUnit(units[numUnits]);
 }
 
+template<class AnyCharsAccess>
+void TokenStreamChars<char16_t, AnyCharsAccess>::ungetLineTerminator() {
+  sourceUnits.ungetCodeUnit();
+
+  char16_t last = sourceUnits.peekCodeUnit();
+  MOZ_ASSERT(SourceUnits::isRawEOLChar(last));
+
+  if (last == '\n')
+    sourceUnits.ungetOptionalCRBeforeLF();
+
+  anyCharsAccess().undoInternalUpdateLineInfoForEOL();
+}
+
 // Return true iff |n| raw characters can be read from this without reading past
 // EOF, and copy those characters into |cp| if so.  The characters are not
 // consumed: use skipChars(n) to do so after checking that the consumed
 // characters had appropriate values.
 template <typename CharT, class AnyCharsAccess>
 bool TokenStreamSpecific<CharT, AnyCharsAccess>::peekChars(int n, CharT* cp) {
   int i;
   for (i = 0; i < n; i++) {
@@ -2025,47 +2038,81 @@ MOZ_MUST_USE bool TokenStreamSpecific<Ch
 
           continue;
         }
 
         // Look for a regexp.
         if (modifier == Operand) {
           tokenbuf.clear();
 
+          auto ProcessNonAsciiCodePoint = [this](CharT lead) {
+            int32_t codePoint;
+            if (!this->getNonAsciiCodePoint(lead, &codePoint))
+              return false;
+
+            if (codePoint == '\n') {
+              this->ungetLineTerminator();
+              this->reportError(JSMSG_UNTERMINATED_REGEXP);
+              return false;
+            }
+
+            return this->appendCodePointToTokenbuf(codePoint);
+          };
+
+          auto ReportUnterminatedRegExp = [this](CharT unit) {
+            this->ungetCodeUnit(unit);
+            this->error(JSMSG_UNTERMINATED_REGEXP);
+          };
+
           bool inCharClass = false;
           do {
-            if (!getChar(&c)) {
+            int32_t unit = getCodeUnit();
+            if (unit == EOF) {
+              ReportUnterminatedRegExp(unit);
               return badToken();
             }
 
-            if (c == '\\') {
-              if (!tokenbuf.append(c)) {
-                return badToken();
+            if (MOZ_LIKELY(isAsciiCodePoint(unit))) {
+              if (unit == '\\')  {
+                if (!tokenbuf.append(unit))
+                  return badToken();
+
+                unit = getCodeUnit();
+                if (unit == EOF) {
+                  ReportUnterminatedRegExp(unit);
+                  return badToken();
+                }
+
+                // Fallthrough only handles ASCII code points, so
+                // deal with non-ASCII and skip everything else.
+                if (MOZ_UNLIKELY(!isAsciiCodePoint(unit))) {
+                  if (!ProcessNonAsciiCodePoint(unit))
+                    return badToken();
+
+                  continue;
+                }
+              } else if (unit == '[') {
+                inCharClass = true;
+              } else if (unit == ']') {
+                inCharClass = false;
+              } else if (unit == '/' && !inCharClass) {
+                // For IE compat, allow unescaped / in char classes.
+                break;
               }
 
-              if (!getChar(&c)) {
+              if (unit == '\r' || unit == '\n') {
+                ReportUnterminatedRegExp(unit);
                 return badToken();
               }
-            } else if (c == '[') {
-              inCharClass = true;
-            } else if (c == ']') {
-              inCharClass = false;
-            } else if (c == '/' && !inCharClass) {
-              // For IE compat, allow unescaped / in char classes.
-              break;
-            }
-
-            if (c == '\n' || c == EOF) {
-              ungetChar(c);
-              reportError(JSMSG_UNTERMINATED_REGEXP);
-              return badToken();
-            }
-
-            if (!tokenbuf.append(c)) {
-              return badToken();
+
+              if (!tokenbuf.append(unit))
+                return badToken();
+            } else {
+              if (!ProcessNonAsciiCodePoint(unit))
+                return badToken();
             }
           } while (true);
 
           RegExpFlag reflags = NoFlags;
           while (true) {
             RegExpFlag flag;
             c = getCodeUnit();
             if (c == 'g')
diff --git a/js/src/frontend/TokenStream.h b/js/src/frontend/TokenStream.h
--- a/js/src/frontend/TokenStream.h
+++ b/js/src/frontend/TokenStream.h
@@ -1303,16 +1303,23 @@ class TokenStreamChars<char16_t, AnyChar
    *
    * If a LineTerminatorSequence was consumed, also update line/column info.
    *
    * This may change the current |sourceUnits| offset.
    */
   MOZ_MUST_USE bool getNonAsciiCodePoint(char16_t lead, int32_t* cp);
 
   void ungetCodePointIgnoreEOL(uint32_t codePoint);
+
+  /**
+   * Unget a just-gotten LineTerminator sequence: '\r', '\n', '\r\n', or
+   * a Unicode line/paragraph separator, also undoing line/column information
+   * changes reflecting that LineTerminator.
+   */
+  void ungetLineTerminator();
 };
 
 // TokenStream is the lexical scanner for JavaScript source text.
 //
 // It takes a buffer of CharT characters (currently only char16_t encoding
 // UTF-16, but we're adding either UTF-8 or Latin-1 single-byte text soon) and
 // linearly scans it into |Token|s.
 //
