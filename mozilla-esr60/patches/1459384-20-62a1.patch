# HG changeset patch
# User Jeff Walden <jwalden@mit.edu>
# Date 1525415368 25200
# Node ID 91303e1d488f27a83186981dbf875a954a6be64a
# Parent  1958bfe4d06ff47bb5a85e2ec662fef28e5cedb7
Bug 1459384 - Allocate tokens only when the token's contents are fully determined, rather than early and sometimes over-eagerly.  r=arai

diff --git a/js/src/frontend/TokenKind.h b/js/src/frontend/TokenKind.h
--- a/js/src/frontend/TokenKind.h
+++ b/js/src/frontend/TokenKind.h
@@ -2,16 +2,18 @@
  * vim: set ts=8 sts=4 et sw=4 tw=99:
  * This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef frontend_TokenKind_h
 #define frontend_TokenKind_h
 
+#include <stdint.h>
+
 /*
  * List of token kinds and their ranges.
  *
  * The format for each line is:
  *
  *   MACRO(<TOKEN_KIND_NAME>, <DESCRIPTION>)
  *
  * or
@@ -230,17 +232,17 @@
 #define FOR_EACH_TOKEN_KIND(MACRO) \
   FOR_EACH_TOKEN_KIND_WITH_RANGE(MACRO, TOKEN_KIND_RANGE_EMIT_NONE)
 
 namespace js {
 namespace frontend {
 
 // Values of this type are used to index into arrays such as isExprEnding[],
 // so the first value must be zero.
-enum class TokenKind {
+enum class TokenKind : uint8_t {
 #define EMIT_ENUM(name, desc) name,
 #define EMIT_ENUM_RANGE(name, value) name = value,
   FOR_EACH_TOKEN_KIND_WITH_RANGE(EMIT_ENUM, EMIT_ENUM_RANGE)
 #undef EMIT_ENUM
 #undef EMIT_ENUM_RANGE
       Limit  // domain size
 };
 
diff --git a/js/src/frontend/TokenStream.cpp b/js/src/frontend/TokenStream.cpp
--- a/js/src/frontend/TokenStream.cpp
+++ b/js/src/frontend/TokenStream.cpp
@@ -1134,51 +1134,53 @@ bool TokenStreamSpecific<CharT, AnyChars
   return getDirective(isMultiline, shouldWarnDeprecated,
                       sourceMappingURLDirective,
                       sourceMappingURLDirectiveLength, "sourceMappingURL",
                       &anyCharsAccess().sourceMapURL_);
 }
 
 template <typename CharT, class AnyCharsAccess>
 MOZ_ALWAYS_INLINE Token*
-GeneralTokenStreamChars<CharT, AnyCharsAccess>::newToken(TokenStart start) {
-  Token* tp = anyCharsAccess().allocateToken();
+GeneralTokenStreamChars<CharT, AnyCharsAccess>::newTokenInternal(
+    TokenKind kind, TokenStart start, TokenKind* out) {
+  MOZ_ASSERT(kind < TokenKind::Limit);
+  MOZ_ASSERT(kind != TokenKind::Eol,
+             "TokenKind::Eol should never be used in an actual Token, only "
+             "returned by peekTokenSameLine()");
+
+  TokenStreamAnyChars& anyChars = anyCharsAccess();
+  anyChars.flags.isDirtyLine = true;
 
-  // NOTE: tp->pos.end is not set until the very end of getTokenInternal().
-  tp->pos.begin = start.offset();
+  Token* token = anyChars.allocateToken();
+
+  *out = token->type = kind;
+  token->pos = TokenPos(start.offset(), this->sourceUnits.offset());
+  MOZ_ASSERT(token->pos.begin <= token->pos.end);
 
-  return tp;
+  // NOTE: |token->modifier| and |token->modifierException| are set in
+  //       |newToken()| so that optimized, non-debug code won't do any work
+  //       to pass a modifier-argument that will never be used.
+
+  return token;
 }
 
 template<typename CharT, class AnyCharsAccess>
 MOZ_COLD bool GeneralTokenStreamChars<CharT, AnyCharsAccess>::badToken() {
   // We didn't get a token, so don't set |flags.isDirtyLine|.
   anyCharsAccess().flags.hadError = true;
 
   // Poisoning sourceUnits on error establishes an invariant: once an
   // erroneous token has been seen, sourceUnits will not be consulted again.
   // This is true because the parser will deal with the illegal token by
   // aborting parsing immediately.
   sourceUnits.poisonInDebug();
 
   return false;
 };
 
-#ifdef DEBUG
-static bool IsTokenSane(Token* tp) {
-  // Nb: TokenKind::Eol should never be used in an actual Token;
-  // it should only be returned as a TokenKind from peekTokenSameLine().
-  if (tp->type >= TokenKind::Limit || tp->type == TokenKind::Eol) return false;
-
-  if (tp->pos.end < tp->pos.begin) return false;
-
-  return true;
-}
-#endif
-
 template <>
 MOZ_MUST_USE bool TokenStreamCharsBase<char16_t>::appendCodePointToTokenbuf(
     uint32_t codePoint) {
   char16_t units[2];
   unsigned numUnits = 0;
   unicode::UTF16Encode(codePoint, units, &numUnits);
 
   MOZ_ASSERT(numUnits == 1 || numUnits == 2,
@@ -1236,17 +1238,18 @@ bool TokenStreamSpecific<CharT, AnyChars
     if (!appendCodePointToTokenbuf(codePoint)) return false;
   }
 
   return true;
 }
 
 template<typename CharT, class AnyCharsAccess>
 MOZ_MUST_USE bool TokenStreamSpecific<CharT, AnyCharsAccess>::identifierName(
-    Token* token, const CharT* identStart, IdentifierEscapes escaping) {
+    TokenStart start, const CharT* identStart, IdentifierEscapes escaping,
+    Modifier modifier, TokenKind* out) {
   // Run the bad-token code for every path out of this function except the
   // two success-cases.
   auto noteBadToken = MakeScopeExit([this]() {
     this->badToken();
   });
 
   int c;
   while (true) {
@@ -1286,28 +1289,27 @@ MOZ_MUST_USE bool TokenStreamSpecific<Ch
   } else {
     // Escape-free identifiers can be created directly from sourceUnits.
     chars = identStart;
     length = sourceUnits.addressOfNextCodeUnit() - identStart;
 
     // Represent reserved words lacking escapes as reserved word tokens.
     if (const ReservedWordInfo* rw = FindReservedWord(chars, length)) {
       noteBadToken.release();
-      token->type = rw->tokentype;
+      newSimpleToken(rw->tokentype, start, modifier, out);
       return true;
     }
   }
 
   JSAtom* atom = atomizeChars(anyCharsAccess().cx, chars, length);
   if (!atom)
     return false;
 
   noteBadToken.release();
-  token->type = TokenKind::Name;
-  token->setName(atom->asPropertyName());
+  newNameToken(atom->asPropertyName(), start, modifier, out);
   return true;
 }
 
 enum FirstCharKind {
   // A char16_t has the 'OneChar' kind if it, by itself, constitutes a valid
   // token that cannot also be a prefix of a longer token.  E.g. ';' has the
   // OneChar kind, but '+' does not, because '++' and '+=' are valid longer
   // tokens
@@ -1397,22 +1399,22 @@ template<typename CharT, class AnyCharsA
 void GeneralTokenStreamChars<CharT,
                              AnyCharsAccess>::consumeRestOfSingleLineComment() {
   int32_t c;
   do {
     c = getCharIgnoreEOL();
   } while (c != EOF && !SourceUnits::isRawEOLChar(c));
 
   ungetCharIgnoreEOL(c);
-  anyCharsAccess().deallocateToken();
 }
 
 template<typename CharT, class AnyCharsAccess>
 MOZ_MUST_USE bool TokenStreamSpecific<CharT, AnyCharsAccess>::decimalNumber(
-    int c, Token* tp, const CharT* numStart) {
+    int c, TokenStart start, const CharT* numStart, Modifier modifier,
+    TokenKind* out) {
   // Run the bad-token code for every path out of this function except the
   // one success-case.
   auto noteBadToken = MakeScopeExit([this]() {
     this->badToken();
   });
 
   // Consume integral component digits.
   while (IsAsciiDigit(c))
@@ -1494,60 +1496,42 @@ MOZ_MUST_USE bool TokenStreamSpecific<Ch
     } else {
       // If not a multi-unit code point, we only need to unget the single
       // code unit consumed.
       ungetCharIgnoreEOL(c);
     }
   }
 
   noteBadToken.release();
-  tp->type = TokenKind::Number;
-  tp->setNumber(dval, decimalPoint);
+  newNumberToken(dval, decimalPoint, start, modifier, out);
   return true;
 }
 
-template<typename CharT, class AnyCharsAccess>
-void GeneralTokenStreamChars<CharT, AnyCharsAccess>::finishToken(
-    TokenKind* kind, Token* token, TokenStreamShared::Modifier modifier) {
-  anyCharsAccess().flags.isDirtyLine = true;
-
-  token->pos.end = sourceUnits.offset();
-#ifdef DEBUG
-  // Save the modifier used to get this token, so that if an ungetToken()
-  // occurs and then the token is re-gotten (or peeked, etc.), we can assert
-  // that both gets have used the same modifiers.
-  token->modifier = modifier;
-  token->modifierException = TokenStreamShared::NoException;
-#endif
-
-  MOZ_ASSERT(IsTokenSane(token));
-  *kind = token->type;
-}
-
 template <typename CharT, class AnyCharsAccess>
 MOZ_MUST_USE bool TokenStreamSpecific<CharT, AnyCharsAccess>::getTokenInternal(
     TokenKind* const ttp, const Modifier modifier) {
-  // Assume we'll fail.  Success cases will overwrite this in |FinishToken|.
+  // Assume we'll fail: success cases will overwrite this.
+#ifdef DEBUG
+  *ttp = TokenKind::Limit;
+#endif
   MOZ_MAKE_MEM_UNDEFINED(ttp, sizeof(*ttp));
 
   // Check if in the middle of a template string. Have to get this out of
   // the way first.
   if (MOZ_UNLIKELY(modifier == TemplateTail)) {
     return getStringOrTemplateToken('`', modifier, ttp);
   }
 
   // This loop runs more than once only when whitespace or comments are
   // encountered.
   do {
     if (MOZ_UNLIKELY(!sourceUnits.hasRawChars())) {
+      anyCharsAccess().flags.isEOF = true;
       TokenStart start(sourceUnits, 0);
-      Token* tp = newToken(start);
-      tp->type = TokenKind::Eof;
-      anyCharsAccess().flags.isEOF = true;
-      finishToken(ttp, tp, modifier);
+      newSimpleToken(TokenKind::Eof, start, modifier, ttp);
       return true;
     }
 
     int c = sourceUnits.getCodeUnit();
     MOZ_ASSERT(c != EOF);
 
 
     // Chars not in the range 0..127 are rare.  Getting them out of the way
@@ -1560,52 +1544,42 @@ MOZ_MUST_USE bool TokenStreamSpecific<Ch
           }
 
           anyCharsAccess().updateFlagsForEOL();
         }
 
         continue;
       }
 
+      // If there's an identifier here (and no error occurs), it starts
+      // at the previous code unit.
       TokenStart start(sourceUnits, -1);
-      Token* tp = newToken(start);
-
-      // If the first codepoint is really the start of an identifier, the
-      // identifier starts at the previous raw char.  If it isn't, it's a
-      // bad char and this assignment won't be examined anyway.
       const CharT* identStart = sourceUnits.addressOfNextCodeUnit() - 1;
 
       static_assert('$' < 128,
                     "IdentifierStart contains '$', but as "
                     "!IsUnicodeIDStart('$'), ensure that '$' is never "
                     "handled here");
       static_assert('_' < 128,
                     "IdentifierStart contains '_', but as "
                     "!IsUnicodeIDStart('_'), ensure that '_' is never "
                     "handled here");
       if (unicode::IsUnicodeIDStart(char16_t(c))) {
-        if (!identifierName(tp, identStart, IdentifierEscapes::None)) {
-          return false;
-        }
-
-        finishToken(ttp, tp, modifier);
-        return true;
+        return identifierName(start, identStart, IdentifierEscapes::None,
+                              modifier, ttp);
       }
 
       uint32_t codePoint = c;
       if (!matchMultiUnitCodePoint(c, &codePoint)) {
         return badToken();
       }
+
       if (codePoint && unicode::IsUnicodeIDStart(codePoint)) {
-        if (!identifierName(tp, identStart, IdentifierEscapes::None)) {
-          return false;
-        }
-
-        finishToken(ttp, tp, modifier);
-        return true;
+        return identifierName(start, identStart, IdentifierEscapes::None,
+                              modifier, ttp);
       }
 
       ungetCodePointIgnoreEOL(codePoint);
       error(JSMSG_ILLEGAL_CHARACTER);
       return badToken();
     }
 
     // Get the token kind, based on the first char.  The ordering of c1kind
@@ -1628,67 +1602,51 @@ MOZ_MUST_USE bool TokenStreamSpecific<Ch
     // |String|).  |Other| is biggish, but no other token kind is common
     // enough for it to be worth adding extra values to FirstCharKind.
     FirstCharKind c1kind = FirstCharKind(firstCharKinds[c]);
 
     // Look for an unambiguous single-char token.
     //
     if (c1kind <= OneChar_Max) {
       TokenStart start(sourceUnits, -1);
-      Token* tp = newToken(start);
-      tp->type = TokenKind(c1kind);
-      finishToken(ttp, tp, modifier);
+      newSimpleToken(TokenKind(c1kind), start, modifier, ttp);
       return true;
     }
 
     // Skip over non-EOL whitespace chars.
     //
     if (c1kind == Space)
       continue;
 
     // Look for an identifier.
     //
     if (c1kind == Ident) {
       TokenStart start(sourceUnits, -1);
-      Token* tp = newToken(start);
-
-      if (!identifierName(tp, sourceUnits.addressOfNextCodeUnit() - 1,
-                          IdentifierEscapes::None)) {
-        return false;
-      }
-
-      finishToken(ttp, tp, modifier);
-      return true;
+      return identifierName(start, sourceUnits.addressOfNextCodeUnit() - 1,
+                            IdentifierEscapes::None, modifier, ttp);
     }
 
     // Look for a decimal number.
     //
     if (c1kind == Dec) {
       TokenStart start(sourceUnits, -1);
-      Token* tp = newToken(start);
-
       const CharT* numStart = sourceUnits.addressOfNextCodeUnit() - 1;
-      if (!decimalNumber(c, tp, numStart)) {
-        return false;
-      }
-
-      finishToken(ttp, tp, modifier);
-      return true;
+      return decimalNumber(c, start, numStart, modifier, ttp);
     }
 
     // Look for a string or a template string.
     //
     if (c1kind == String) {
       return getStringOrTemplateToken(static_cast<char>(c), modifier, ttp);
     }
 
     // Skip over EOL chars, updating line state along the way.
     //
     if (c1kind == EOL) {
-      // If it's a \r\n sequence: consume it and treat itas a single EOL.
+      // If it's a \r\n sequence, consume it as a single EOL.
       if (c == '\r' && sourceUnits.hasRawChars())
         sourceUnits.matchCodeUnit('\n');
 
       if (!updateLineInfoForEOL()) {
         return badToken();
       }
 
       anyCharsAccess().updateFlagsForEOL();
@@ -1696,17 +1654,16 @@ MOZ_MUST_USE bool TokenStreamSpecific<Ch
     }
 
     // From a '0', look for a hexadecimal, binary, octal, or "noctal" (a
     // number starting with '0' that contains '8' or '9' and is treated as
     // decimal) number.
     //
     if (c1kind == ZeroDigit) {
       TokenStart start(sourceUnits, -1);
-      Token* tp = newToken(start);
 
       int radix;
       const CharT* numStart;
       c = getCharIgnoreEOL();
       if (c == 'x' || c == 'X') {
         radix = 16;
         c = getCharIgnoreEOL();
         if (!JS7_ISHEX(c)) {
@@ -1763,36 +1720,26 @@ MOZ_MUST_USE bool TokenStreamSpecific<Ch
           // makes our behaviour a superset of the ECMA numeric grammar. We
           // might not always be so permissive, so we warn about it.
           if (c >= '8') {
             if (!warning(JSMSG_BAD_OCTAL, c == '8' ? "08" : "09")) {
               return badToken();
             }
 
             // Use the decimal scanner for the rest of the number.
-            if (!decimalNumber(c, tp, numStart)) {
-              return false;
-            }
-
-            finishToken(ttp, tp, modifier);
-            return true;
+            return decimalNumber(c, start, numStart, modifier, ttp);
           }
 
           c = getCharIgnoreEOL();
         } while (IsAsciiDigit(c));
       } else {
         // '0' not followed by [XxBbOo0-9];  scan as a decimal number.
         numStart = sourceUnits.addressOfNextCodeUnit() - 1;
 
-        if (!decimalNumber(c, tp, numStart)) {
-          return false;
-        }
-
-        finishToken(ttp, tp, modifier);
-        return true;
+        return decimalNumber(c, start, numStart, modifier, ttp);
       }
       ungetCharIgnoreEOL(c);
 
       if (c != EOF) {
         if (unicode::IsIdentifierStart(char16_t(c))) {
           error(JSMSG_IDSTART_AFTER_NUMBER);
           return badToken();
         }
@@ -1823,168 +1770,158 @@ MOZ_MUST_USE bool TokenStreamSpecific<Ch
       double dval;
       const char16_t* dummy;
       if (!GetPrefixInteger(anyCharsAccess().cx, numStart,
                             sourceUnits.addressOfNextCodeUnit(), radix, &dummy,
                             &dval)) {
         return badToken();
       }
 
-      tp->type = TokenKind::Number;
-      tp->setNumber(dval, NoDecimal);
-      finishToken(ttp, tp, modifier);
+      newNumberToken(dval, NoDecimal, start, modifier, ttp);
       return true;
     }
 
-    // This handles everything else.
+    MOZ_ASSERT(c1kind == Other);
+
+    // This handles everything else.  Simple tokens distinguished solely by
+    // TokenKind should set |simpleKind| and break, to share simple-token
+    // creation code for all such tokens.  All other tokens must be handled
+    // by returning (or by continuing from the loop enclosing this).
     //
-    MOZ_ASSERT(c1kind == Other);
     TokenStart start(sourceUnits, -1);
-    Token* tp = newToken(start);
+    TokenKind simpleKind;
+#ifdef DEBUG
+    simpleKind = TokenKind::Limit;  // sentinel value for code after switch
+#endif
     switch (c) {
       case '.':
         c = getCharIgnoreEOL();
         if (IsAsciiDigit(c)) {
-          const CharT* numStart = sourceUnits.addressOfNextCodeUnit() - 2;
-
-          if (!decimalNumber('.', tp, numStart)) {
-            return false;
-          }
-
-          finishToken(ttp, tp, modifier);
-          return true;
+          return decimalNumber('.', start,
+                               sourceUnits.addressOfNextCodeUnit() - 2,
+                               modifier, ttp);
         }
 
         if (c == '.') {
           if (matchChar('.')) {
-            tp->type = TokenKind::TripleDot;
-            finishToken(ttp, tp, modifier);
-            return true;
+            simpleKind = TokenKind::TripleDot;
+            break;
           }
         }
         ungetCharIgnoreEOL(c);
-        tp->type = TokenKind::Dot;
-        finishToken(ttp, tp, modifier);
-        return true;
+
+        simpleKind = TokenKind::Dot;
+        break;
 
       case '=':
         if (matchChar('='))
-          tp->type = matchChar('=') ? TokenKind::StrictEq : TokenKind::Eq;
+          simpleKind = matchChar('=') ? TokenKind::StrictEq : TokenKind::Eq;
         else if (matchChar('>'))
-          tp->type = TokenKind::Arrow;
+          simpleKind = TokenKind::Arrow;
         else
-          tp->type = TokenKind::Assign;
-        finishToken(ttp, tp, modifier);
-        return true;
+          simpleKind = TokenKind::Assign;
+        break;
 
       case '+':
         if (matchChar('+'))
-          tp->type = TokenKind::Inc;
+          simpleKind = TokenKind::Inc;
         else
-          tp->type = matchChar('=') ? TokenKind::AddAssign : TokenKind::Add;
-        finishToken(ttp, tp, modifier);
-        return true;
+          simpleKind = matchChar('=') ? TokenKind::AddAssign : TokenKind::Add;
+        break;
 
       case '\\': {
         uint32_t qc;
         if (uint32_t escapeLength = matchUnicodeEscapeIdStart(&qc)) {
-          if (!identifierName(tp, sourceUnits.addressOfNextCodeUnit() -
-                                  escapeLength - 1,
-                              IdentifierEscapes::SawUnicodeEscape)) {
-            return false;
-          }
-
-          finishToken(ttp, tp, modifier);
-          return true;
+          return identifierName(start,
+                                sourceUnits.addressOfNextCodeUnit() -
+                                    escapeLength - 1,
+                                IdentifierEscapes::SawUnicodeEscape, modifier,
+                                ttp);
         }
 
         // We could point "into" a mistyped escape, e.g. for "\u{41H}" we
         // could point at the 'H'.  But we don't do that now, so the
         // character after the '\' isn't necessarily bad, so just point at
         // the start of the actually-invalid escape.
         ungetCharIgnoreEOL('\\');
         error(JSMSG_BAD_ESCAPE);
         return badToken();
       }
 
       case '|':
         if (matchChar('|'))
-          tp->type = TokenKind::Or;
+          simpleKind = TokenKind::Or;
 #ifdef ENABLE_PIPELINE_OPERATOR
         else if (matchChar('>'))
-          tp->type = TokenKind::Pipeline;
+          simpleKind = TokenKind::Pipeline;
 #endif
         else
-          tp->type = matchChar('=') ? TokenKind::BitOrAssign : TokenKind::BitOr;
-        finishToken(ttp, tp, modifier);
-        return true;
+          simpleKind = matchChar('=') ? TokenKind::BitOrAssign
+                                      : TokenKind::BitOr;
+        break;
 
       case '^':
-        tp->type = matchChar('=') ? TokenKind::BitXorAssign : TokenKind::BitXor;
-        finishToken(ttp, tp, modifier);
-        return true;
+        simpleKind = matchChar('=') ? TokenKind::BitXorAssign
+                                    : TokenKind::BitXor;
+        break;
 
       case '&':
         if (matchChar('&'))
-          tp->type = TokenKind::And;
+          simpleKind = TokenKind::And;
         else
-          tp->type = matchChar('=') ? TokenKind::BitAndAssign
-                                    : TokenKind::BitAnd;
-        finishToken(ttp, tp, modifier);
-        return true;
+          simpleKind = matchChar('=') ? TokenKind::BitAndAssign
+                                      : TokenKind::BitAnd;
+        break;
 
       case '!':
         if (matchChar('='))
-          tp->type = matchChar('=') ? TokenKind::StrictNe : TokenKind::Ne;
+          simpleKind = matchChar('=') ? TokenKind::StrictNe : TokenKind::Ne;
         else
-          tp->type = TokenKind::Not;
-        finishToken(ttp, tp, modifier);
-        return true;
+          simpleKind = TokenKind::Not;
+        break;
 
       case '<':
         if (anyCharsAccess().options().allowHTMLComments) {
           // Treat HTML begin-comment as comment-till-end-of-line.
           if (matchChar('!')) {
             if (matchChar('-')) {
               if (matchChar('-')) {
                 consumeRestOfSingleLineComment();
                 continue;
               }
               ungetCharIgnoreEOL('-');
             }
             ungetCharIgnoreEOL('!');
           }
         }
         if (matchChar('<')) {
-          tp->type = matchChar('=') ? TokenKind::LshAssign : TokenKind::Lsh;
+          simpleKind = matchChar('=') ? TokenKind::LshAssign : TokenKind::Lsh;
         } else {
-          tp->type = matchChar('=') ? TokenKind::Le : TokenKind::Lt;
+          simpleKind = matchChar('=') ? TokenKind::Le : TokenKind::Lt;
         }
-        finishToken(ttp, tp, modifier);
-        return true;
+        break;
 
       case '>':
         if (matchChar('>')) {
           if (matchChar('>'))
-            tp->type = matchChar('=') ? TokenKind::UrshAssign : TokenKind::Ursh;
+            simpleKind = matchChar('=') ? TokenKind::UrshAssign
+                                        : TokenKind::Ursh;
           else
-            tp->type = matchChar('=') ? TokenKind::RshAssign : TokenKind::Rsh;
+            simpleKind = matchChar('=') ? TokenKind::RshAssign : TokenKind::Rsh;
         } else {
-          tp->type = matchChar('=') ? TokenKind::Ge : TokenKind::Gt;
+          simpleKind = matchChar('=') ? TokenKind::Ge : TokenKind::Gt;
         }
-        finishToken(ttp, tp, modifier);
-        return true;
+        break;
 
       case '*':
         if (matchChar('*'))
-          tp->type = matchChar('=') ? TokenKind::PowAssign : TokenKind::Pow;
+          simpleKind = matchChar('=') ? TokenKind::PowAssign : TokenKind::Pow;
         else
-          tp->type = matchChar('=') ? TokenKind::MulAssign : TokenKind::Mul;
-        finishToken(ttp, tp, modifier);
-        return true;
+          simpleKind = matchChar('=') ? TokenKind::MulAssign : TokenKind::Mul;
+        break;
 
       case '/':
         // Look for a single-line comment.
         if (matchChar('/')) {
           c = getCharIgnoreEOL();
           if (c == '@' || c == '#') {
             bool shouldWarn = c == '@';
             if (!getDirectives(false, shouldWarn)) {
@@ -2021,18 +1958,16 @@ MOZ_MUST_USE bool TokenStreamSpecific<Ch
                 return false;
               }
             }
           } while (true);
 
           if (linenoBefore != anyChars.lineno)
             anyChars.updateFlagsForEOL();
 
-          // Effects of |newToken(-1)| before the switch must be undone.
-          anyChars.deallocateToken();
           continue;
         }
 
         // Look for a regexp.
         if (modifier == Operand) {
           tokenbuf.clear();
 
           bool inCharClass = false;
@@ -2094,76 +2029,71 @@ MOZ_MUST_USE bool TokenStreamSpecific<Ch
               errorAt(sourceUnits.offset() - 1, JSMSG_BAD_REGEXP_FLAG, buf);
               return badToken();
             }
 
             reflags = RegExpFlag(reflags | flag);
           }
           ungetCharIgnoreEOL(c);
 
-          tp->type = TokenKind::RegExp;
-          tp->setRegExpFlags(reflags);
-          finishToken(ttp, tp, modifier);
+          newRegExpToken(reflags, start, modifier, ttp);
           return true;
         }
 
-        tp->type = matchChar('=') ? TokenKind::DivAssign : TokenKind::Div;
-        finishToken(ttp, tp, modifier);
-        return true;
+        simpleKind = matchChar('=') ? TokenKind::DivAssign : TokenKind::Div;
+        break;
 
       case '%':
-        tp->type = matchChar('=') ? TokenKind::ModAssign : TokenKind::Mod;
-        finishToken(ttp, tp, modifier);
-        return true;
+        simpleKind = matchChar('=') ? TokenKind::ModAssign : TokenKind::Mod;
+        break;
 
       case '-':
         if (matchChar('-')) {
           if (anyCharsAccess().options().allowHTMLComments &&
               !anyCharsAccess().flags.isDirtyLine) {
             if (matchChar('>')) {
               consumeRestOfSingleLineComment();
               continue;
             }
           }
 
-          tp->type = TokenKind::Dec;
+          simpleKind = TokenKind::Dec;
         } else {
-          tp->type = matchChar('=') ? TokenKind::SubAssign : TokenKind::Sub;
+          simpleKind = matchChar('=') ? TokenKind::SubAssign : TokenKind::Sub;
         }
-        finishToken(ttp, tp, modifier);
-        return true;
+        break;
 
       default:
         // We consumed a bad character/code point.  Put it back so the
         // error location is the bad character.
         ungetCodePointIgnoreEOL(c);
         error(JSMSG_ILLEGAL_CHARACTER);
         return badToken();
     }
 
-    MOZ_CRASH("should either have called |FinishToken()| and returned "
-              "true, returned |badToken()|, returned false after failure "
-              "of a function that would have called |badToken()|, or "
-              "continued following whitespace or a comment");
+    MOZ_ASSERT(simpleKind != TokenKind::Limit,
+               "switch-statement should have set |simpleKind| before breaking");
+
+    newSimpleToken(simpleKind, start, modifier, ttp);
+    return true;
   } while (true);
 }
 
 template <typename CharT, class AnyCharsAccess>
 bool TokenStreamSpecific<CharT, AnyCharsAccess>::getStringOrTemplateToken(
     char untilChar, Modifier modifier, TokenKind* out) {
   MOZ_ASSERT(untilChar == '\'' || untilChar == '"' || untilChar == '`',
              "unexpected string/template literal delimiter");
 
   int c;
 
   bool parsingTemplate = (untilChar == '`');
   bool templateHead = false;
 
   TokenStart start(sourceUnits, -1);
-  Token* token = newToken(start);
   tokenbuf.clear();
 
   // Run the bad-token code for every path out of this function except the
   // one success-case.
   auto noteBadToken = MakeScopeExit([this]() {
     this->badToken();
   });
 
@@ -2404,30 +2334,24 @@ bool TokenStreamSpecific<CharT, AnyChars
       return false;
     }
   }
 
   JSAtom* atom =
       atomizeChars(anyCharsAccess().cx, tokenbuf.begin(), tokenbuf.length());
   if (!atom) return false;
 
-  if (!parsingTemplate) {
-    MOZ_ASSERT(!templateHead);
+  noteBadToken.release();
+
+  MOZ_ASSERT_IF(!parsingTemplate, !templateHead);
 
-    token->type = TokenKind::String;
-  } else {
-    if (templateHead)
-      token->type = TokenKind::TemplateHead;
-    else
-      token->type = TokenKind::NoSubsTemplate;
-  }
-
-  noteBadToken.release();
-  token->setAtom(atom);
-  finishToken(out, token, modifier);
+  TokenKind kind = !parsingTemplate ? TokenKind::String
+                                    : templateHead ? TokenKind::TemplateHead
+                                                   : TokenKind::NoSubsTemplate;
+  newAtomToken(kind, atom, start, modifier, out);
   return true;
 }
 
 const char* TokenKindToDesc(TokenKind tt) {
   switch (tt) {
 #define EMIT_CASE(name, desc) \
   case TokenKind::name:       \
     return desc;
diff --git a/js/src/frontend/TokenStream.h b/js/src/frontend/TokenStream.h
--- a/js/src/frontend/TokenStream.h
+++ b/js/src/frontend/TokenStream.h
@@ -789,29 +789,21 @@ class TokenStreamAnyChars : public Token
     advanceCursor();
 
     Token* tp = &tokens[cursor()];
     MOZ_MAKE_MEM_UNDEFINED(tp, sizeof(*tp));
 
     return tp;
   }
 
-  // Undoes the effect of |allocateToken()|.
-  //
-  // This function is inadequate if that token is usable as lookahead.  As a
-  // general rule, you probably want |ungetToken()| below instead.
-  void deallocateToken() {
-    retractCursor();
-  }
-
   // Push the last scanned token back into the stream.
   void ungetToken() {
     MOZ_ASSERT(lookahead < maxLookahead);
     lookahead++;
-    deallocateToken();
+    retractCursor();
   }
 
  public:
   MOZ_MUST_USE bool compileWarning(ErrorMetadata&& metadata,
                                    UniquePtr<JSErrorNotes> notes,
                                    unsigned flags, unsigned errorNumber,
                                    va_list args);
 
@@ -1030,16 +1022,37 @@ class TokenStart {
 
   uint32_t offset() const { return startOffset_; }
 };
 
 template <typename CharT, class AnyCharsAccess>
 class GeneralTokenStreamChars : public TokenStreamCharsBase<CharT> {
   using CharsSharedBase = TokenStreamCharsBase<CharT>;
 
+  Token* newTokenInternal(TokenKind kind, TokenStart start, TokenKind* out);
+
+  /**
+   * Allocates a new Token from the given offset to the current offset,
+   * ascribes it the given kind, and sets |*out| to that kind.
+   */
+  Token* newToken(TokenKind kind, TokenStart start,
+                  TokenStreamShared::Modifier modifier, TokenKind* out) {
+    Token* token = newTokenInternal(kind, start, out);
+
+#ifdef DEBUG
+    // Save the modifier used to get this token, so that if an ungetToken()
+    // occurs and then the token is re-gotten (or peeked, etc.), we can
+    // assert both gets used compatible modifiers.
+    token->modifier = modifier;
+    token->modifierException = TokenStreamShared::NoException;
+#endif
+
+    return token;
+  }
+
  protected:
   using typename CharsSharedBase::SourceUnits;
 
   using CharsSharedBase::sourceUnits;
   using CharsSharedBase::ungetCharIgnoreEOL;
 
  public:
   using CharsSharedBase::CharsSharedBase;
@@ -1058,38 +1071,57 @@ class GeneralTokenStreamChars : public T
   TokenStreamSpecific* asSpecific() {
     static_assert(
         mozilla::IsBaseOf<GeneralTokenStreamChars, TokenStreamSpecific>::value,
         "static_cast below presumes an inheritance relationship");
 
     return static_cast<TokenStreamSpecific*>(this);
   }
 
-  /**
-   * Allocates a new Token starting at the current offset from the circular
-   * buffer of Tokens in |anyCharsAccess()|, that begins at |start.offset()|.
-   */
-  Token* newToken(TokenStart start);
+  void newSimpleToken(TokenKind kind, TokenStart start,
+                      TokenStreamShared::Modifier modifier, TokenKind* out) {
+    newToken(kind, start, modifier, out);
+  }
+
+  void newNumberToken(double dval, DecimalPoint decimalPoint, TokenStart start,
+                      TokenStreamShared::Modifier modifier, TokenKind* out) {
+    Token* token = newToken(TokenKind::Number, start, modifier, out);
+    token->setNumber(dval, decimalPoint);
+  }
+
+  void newAtomToken(TokenKind kind, JSAtom* atom, TokenStart start,
+                    TokenStreamShared::Modifier modifier, TokenKind* out) {
+    MOZ_ASSERT(kind == TokenKind::String || kind == TokenKind::TemplateHead ||
+               kind == TokenKind::NoSubsTemplate);
+
+    Token* token = newToken(kind, start, modifier, out);
+    token->setAtom(atom);
+  }
+
+  void newNameToken(PropertyName* name, TokenStart start,
+                    TokenStreamShared::Modifier modifier, TokenKind* out) {
+    Token* token = newToken(TokenKind::Name, start, modifier, out);
+    token->setName(name);
+  }
+
+  void newRegExpToken(RegExpFlag reflags, TokenStart start,
+                      TokenStreamShared::Modifier modifier, TokenKind* out) {
+    Token* token = newToken(TokenKind::RegExp, start, modifier, out);
+    token->setRegExpFlags(reflags);
+  }
 
   MOZ_COLD bool badToken();
 
-  void finishToken(TokenKind* kind, Token* token,
-                   TokenStreamShared::Modifier modifier);
-
   int32_t getCharIgnoreEOL();
 
   void ungetChar(int32_t c);
 
   /**
    * Consume characters til EOL/EOF following the start of a single-line
    * comment, without consuming the EOL/EOF.
-   *
-   * This function presumes |newToken()| was over-optimistically called and
-   * undoes that call.  If you call this function in any other situation,
-   * you're gonna have a bad time.
    */
   void consumeRestOfSingleLineComment();
 };
 
 template <typename CharT, class AnyCharsAccess>
 class TokenStreamChars;
 
 template <class AnyCharsAccess>
@@ -1221,19 +1253,22 @@ class MOZ_STACK_CLASS TokenStreamSpecifi
   using CharsSharedBase::appendCodePointToTokenbuf;
   using CharsSharedBase::atomizeChars;
   using CharsSharedBase::copyTokenbufTo;
   using CharsSharedBase::sourceUnits;
   using CharsSharedBase::tokenbuf;
   using CharsSharedBase::ungetCharIgnoreEOL;
   using GeneralCharsBase::badToken;
   using GeneralCharsBase::consumeRestOfSingleLineComment;
-  using GeneralCharsBase::finishToken;
   using GeneralCharsBase::getCharIgnoreEOL;
-  using GeneralCharsBase::newToken;
+  using GeneralCharsBase::newAtomToken;
+  using GeneralCharsBase::newNameToken;
+  using GeneralCharsBase::newNumberToken;
+  using GeneralCharsBase::newRegExpToken;
+  using GeneralCharsBase::newSimpleToken;
   using GeneralCharsBase::ungetChar;
 
   template<typename CharU>
   friend class TokenStreamPosition;
 
  public:
   TokenStreamSpecific(JSContext* cx, const ReadOnlyCompileOptions& options,
                       const CharT* base, size_t length);
@@ -1409,17 +1444,19 @@ class MOZ_STACK_CLASS TokenStreamSpecifi
    *
    *   In this case, the next |getCharIgnoreEOL()| returns the code unit
    *   after |c|: '.', '6', or '+' in the examples above.
    *
    * This interface is super-hairy and horribly stateful.  Unfortunately, its
    * hair merely reflects the intricacy of ECMAScript numeric literal syntax.
    * And incredibly, it *improves* on the goto-based horror that predated it.
    */
-  MOZ_MUST_USE bool decimalNumber(int c, Token* tp, const CharT* numStart);
+  MOZ_MUST_USE bool decimalNumber(int c, TokenStart start,
+                                  const CharT* numStart, Modifier modifier,
+                                  TokenKind* out);
 
  public:
   // Advance to the next token.  If the token stream encountered an error,
   // return false.  Otherwise return true and store the token kind in |*ttp|.
   MOZ_MUST_USE bool getToken(TokenKind* ttp, Modifier modifier = None) {
     // Check for a pushed-back token resulting from mismatching lookahead.
     TokenStreamAnyChars& anyChars = anyCharsAccess();
     if (anyChars.lookahead != 0) {
@@ -1564,18 +1601,19 @@ class MOZ_STACK_CLASS TokenStreamSpecifi
   MOZ_MUST_USE bool seek(const Position& pos, const TokenStreamAnyChars& other);
 
   const CharT* codeUnitPtrAt(size_t offset) const {
     return sourceUnits.codeUnitPtrAt(offset);
   }
 
   const CharT* rawLimit() const { return sourceUnits.limit(); }
 
-  MOZ_MUST_USE bool identifierName(Token* token, const CharT* identStart,
-                                   IdentifierEscapes escaping);
+  MOZ_MUST_USE bool identifierName(TokenStart start, const CharT* identStart,
+                                   IdentifierEscapes escaping,
+                                   Modifier modifier, TokenKind* out);
 
   MOZ_MUST_USE bool getTokenInternal(TokenKind* const ttp,
                                      const Modifier modifier);
 
   MOZ_MUST_USE bool getStringOrTemplateToken(char untilChar, Modifier modifier,
                                              TokenKind* out);
 
   // Try to get the next character, normalizing '\r', '\r\n', and '\n' into
