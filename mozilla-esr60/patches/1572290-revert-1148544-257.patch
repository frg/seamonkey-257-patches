# HG changeset patch
# User Dmitry Butskoy <dmitry@butskoy.name>
# Date 1578832125 -3600
# Parent  20477f89c518bbab8795a81d3bcf2687736db9fd
Bug 1572290 - Site specific User Agent correction. Revert Bug 1148544 for SeaMonkey. r=frg a=frg

diff --git a/netwerk/base/nsILoadGroup.idl b/netwerk/base/nsILoadGroup.idl
--- a/netwerk/base/nsILoadGroup.idl
+++ b/netwerk/base/nsILoadGroup.idl
@@ -90,15 +90,9 @@ interface nsILoadGroup : nsIRequest
      * to the group - typically via nsIDocShell::defaultLoadFlags on a new
      * docShell.
      * Note that these flags are *not* added to the default request for the
      * load group; it is expected the default request will already have these
      * flags (again, courtesy of setting nsIDocShell::defaultLoadFlags before
      * the docShell has created the default request.)
      */
     attribute nsLoadFlags defaultLoadFlags;
-
-    /**
-     * The cached user agent override created by UserAgentOverrides.jsm. Used
-     * for all sub-resource requests in the loadgroup.
-     */
-    attribute ACString userAgentOverrideCache;
 };
diff --git a/netwerk/base/nsLoadGroup.cpp b/netwerk/base/nsLoadGroup.cpp
--- a/netwerk/base/nsLoadGroup.cpp
+++ b/netwerk/base/nsLoadGroup.cpp
@@ -741,29 +741,16 @@ nsLoadGroup::GetDefaultLoadFlags(uint32_
 }
 
 NS_IMETHODIMP
 nsLoadGroup::SetDefaultLoadFlags(uint32_t aFlags) {
   mDefaultLoadFlags = aFlags;
   return NS_OK;
 }
 
-NS_IMETHODIMP
-nsLoadGroup::GetUserAgentOverrideCache(nsACString &aUserAgentOverrideCache) {
-  aUserAgentOverrideCache = mUserAgentOverrideCache;
-  return NS_OK;
-}
-
-NS_IMETHODIMP
-nsLoadGroup::SetUserAgentOverrideCache(
-    const nsACString &aUserAgentOverrideCache) {
-  mUserAgentOverrideCache = aUserAgentOverrideCache;
-  return NS_OK;
-}
-
 ////////////////////////////////////////////////////////////////////////////////
 
 void nsLoadGroup::TelemetryReport() {
   nsresult defaultStatus = NS_ERROR_INVALID_ARG;
   // We should only report HTTP_PAGE_* telemetry if the defaultRequest was
   // actually successful.
   if (mDefaultLoadRequest) {
     mDefaultLoadRequest->GetStatus(&defaultStatus);
diff --git a/netwerk/base/nsLoadGroup.h b/netwerk/base/nsLoadGroup.h
--- a/netwerk/base/nsLoadGroup.h
+++ b/netwerk/base/nsLoadGroup.h
@@ -89,16 +89,14 @@ class nsLoadGroup : public nsILoadGroup,
   /* Telemetry */
   mozilla::TimeStamp mDefaultRequestCreationTime;
   bool mDefaultLoadIsTimed;
   uint32_t mTimedRequests;
   uint32_t mCachedRequests;
 
   /* For nsPILoadGroupInternal */
   uint32_t mTimedNonCachedRequestsUntilOnEndPageLoad;
-
-  nsCString mUserAgentOverrideCache;
 };
 
 }  // namespace net
 }  // namespace mozilla
 
 #endif  // nsLoadGroup_h__
diff --git a/netwerk/protocol/http/UserAgentOverrides.jsm b/netwerk/protocol/http/UserAgentOverrides.jsm
--- a/netwerk/protocol/http/UserAgentOverrides.jsm
+++ b/netwerk/protocol/http/UserAgentOverrides.jsm
@@ -36,19 +36,19 @@ var UserAgentOverrides = {
       return;
 
     gPrefBranch = Services.prefs.getBranch("general.useragent.override.");
     gPrefBranch.addObserver("", buildOverrides);
 
     Services.prefs.addObserver(PREF_OVERRIDES_ENABLED, buildOverrides);
 
     try {
-      Services.obs.addObserver(HTTP_on_useragent_request, "http-on-useragent-request");
+      Services.obs.addObserver(HTTP_on_modify_request, "http-on-modify-request");
     } catch (x) {
-      // The http-on-useragent-request notification is disallowed in content processes.
+      // The http-on-modify-request notification is disallowed in content processes.
     }
 
     try {
       UserAgentUpdates.init(function(overrides) {
         gOverrideForHostCache.clear();
         if (overrides) {
           for (let domain in overrides) {
             overrides[domain] = getUserAgentFromOverride(overrides[domain]);
@@ -115,17 +115,17 @@ var UserAgentOverrides = {
     if (!gInitialized)
       return;
     gInitialized = false;
 
     gPrefBranch.removeObserver("", buildOverrides);
 
     Services.prefs.removeObserver(PREF_OVERRIDES_ENABLED, buildOverrides);
 
-    Services.obs.removeObserver(HTTP_on_useragent_request, "http-on-useragent-request");
+    Services.obs.removeObserver(HTTP_on_modify_request, "http-on-modify-request");
   }
 };
 
 function getUserAgentFromOverride(override)
 {
   let userAgent = gBuiltUAs.get(override);
   if (userAgent !== undefined) {
     return userAgent;
@@ -155,17 +155,17 @@ function buildOverrides() {
     let userAgent = getUserAgentFromOverride(override);
 
     if (userAgent != DEFAULT_UA) {
       gOverrides.set(domain, userAgent);
     }
   }
 }
 
-function HTTP_on_useragent_request(aSubject, aTopic, aData) {
+function HTTP_on_modify_request(aSubject, aTopic, aData) {
   let channel = aSubject.QueryInterface(Ci.nsIHttpChannel);
 
   for (let callback of gOverrideFunctions) {
     let modifiedUA = callback(channel, DEFAULT_UA);
     if (modifiedUA) {
       channel.setRequestHeader("User-Agent", modifiedUA, false);
       return;
     }
diff --git a/netwerk/protocol/http/nsHttpChannel.cpp b/netwerk/protocol/http/nsHttpChannel.cpp
--- a/netwerk/protocol/http/nsHttpChannel.cpp
+++ b/netwerk/protocol/http/nsHttpChannel.cpp
@@ -6012,18 +6012,16 @@ nsresult nsHttpChannel::BeginConnect() {
   if (NS_FAILED(rv)) {
     LOG(("nsHttpChannel %p AddAuthorizationHeaders failed (%08x)", this,
          static_cast<uint32_t>(rv)));
   }
 
   // notify "http-on-modify-request" observers
   CallOnModifyRequestObservers();
 
-  SetLoadGroupUserAgentOverride();
-
   // Check if request was cancelled during on-modify-request or on-useragent.
   if (mCanceled) {
     return mStatus;
   }
 
   if (mSuspendCount) {
     LOG(("Waiting until resume BeginConnect [this=%p]\n", this));
     MOZ_ASSERT(!mCallOnResume);
@@ -8371,58 +8369,16 @@ void nsHttpChannel::MaybeWarnAboutAppCac
     // provide an additional warning for developers of removal
     if (!IsHTTPS() &&
         Preferences::GetBool("browser.cache.offline.insecure.enable")) {
       warner->IssueWarning(nsIDocument::eAppCacheInsecure, true);
     }
   }
 }
 
-void nsHttpChannel::SetLoadGroupUserAgentOverride() {
-  nsCOMPtr<nsIURI> uri;
-  GetURI(getter_AddRefs(uri));
-  nsAutoCString uriScheme;
-  if (uri) {
-    uri->GetScheme(uriScheme);
-  }
-
-  // We don't need a UA for file: protocols.
-  if (uriScheme.EqualsLiteral("file")) {
-    gHttpHandler->OnUserAgentRequest(this);
-    return;
-  }
-
-  nsIRequestContextService *rcsvc = gHttpHandler->GetRequestContextService();
-  nsCOMPtr<nsIRequestContext> rc;
-  if (rcsvc) {
-    rcsvc->GetRequestContext(mRequestContextID, getter_AddRefs(rc));
-  }
-
-  nsAutoCString ua;
-  if (nsContentUtils::IsNonSubresourceRequest(this)) {
-    gHttpHandler->OnUserAgentRequest(this);
-    if (rc) {
-      GetRequestHeader(NS_LITERAL_CSTRING("User-Agent"), ua);
-      rc->SetUserAgentOverride(ua);
-    }
-  } else {
-    GetRequestHeader(NS_LITERAL_CSTRING("User-Agent"), ua);
-    // Don't overwrite the UA if it is already set (eg by an XHR with explicit
-    // UA).
-    if (ua.IsEmpty()) {
-      if (rc) {
-        rc->GetUserAgentOverride(ua);
-        SetRequestHeader(NS_LITERAL_CSTRING("User-Agent"), ua, false);
-      } else {
-        gHttpHandler->OnUserAgentRequest(this);
-      }
-    }
-  }
-}
-
 // Step 10 of HTTP-network-or-cache fetch
 void nsHttpChannel::SetOriginHeader() {
   if (mRequestHead.IsGet() || mRequestHead.IsHead()) {
     return;
   }
   nsAutoCString existingHeader;
   Unused << mRequestHead.GetHeader(nsHttp::Origin, existingHeader);
   if (!existingHeader.IsEmpty()) {
diff --git a/netwerk/protocol/http/nsHttpChannel.h b/netwerk/protocol/http/nsHttpChannel.h
--- a/netwerk/protocol/http/nsHttpChannel.h
+++ b/netwerk/protocol/http/nsHttpChannel.h
@@ -471,18 +471,16 @@ class nsHttpChannel final : public HttpB
   MOZ_MUST_USE nsresult OpenCacheInputStream(nsICacheEntry *cacheEntry,
                                              bool startBuffering,
                                              bool checkingAppCacheEntry);
 
   void SetPushedStream(Http2PushedStreamWrapper *stream);
 
   void MaybeWarnAboutAppCache();
 
-  void SetLoadGroupUserAgentOverride();
-
   void SetOriginHeader();
   void SetDoNotTrack();
 
   already_AddRefed<nsChannelClassifier> GetOrCreateChannelClassifier();
 
   // Start an internal redirect to a new InterceptedHttpChannel which will
   // resolve in firing a ServiceWorker FetchEvent.
   MOZ_MUST_USE nsresult RedirectToInterceptedChannel();
diff --git a/netwerk/protocol/http/nsHttpHandler.h b/netwerk/protocol/http/nsHttpHandler.h
--- a/netwerk/protocol/http/nsHttpHandler.h
+++ b/netwerk/protocol/http/nsHttpHandler.h
@@ -320,21 +320,16 @@ class nsHttpHandler final : public nsIHt
     NotifyObservers(chan, NS_HTTP_ON_MODIFY_REQUEST_TOPIC);
   }
 
   // Called by the channel before writing a request
   void OnStopRequest(nsIHttpChannel *chan) {
     NotifyObservers(chan, NS_HTTP_ON_STOP_REQUEST_TOPIC);
   }
 
-  // Called by the channel and cached in the loadGroup
-  void OnUserAgentRequest(nsIHttpChannel *chan) {
-    NotifyObservers(chan, NS_HTTP_ON_USERAGENT_REQUEST_TOPIC);
-  }
-
   // Called by the channel before setting up the transaction
   void OnBeforeConnect(nsIHttpChannel *chan) {
     NotifyObservers(chan, NS_HTTP_ON_BEFORE_CONNECT_TOPIC);
   }
 
   // Called by the channel once headers are available
   void OnExamineResponse(nsIHttpChannel *chan) {
     NotifyObservers(chan, NS_HTTP_ON_EXAMINE_RESPONSE_TOPIC);
diff --git a/netwerk/protocol/http/nsIHttpProtocolHandler.idl b/netwerk/protocol/http/nsIHttpProtocolHandler.idl
--- a/netwerk/protocol/http/nsIHttpProtocolHandler.idl
+++ b/netwerk/protocol/http/nsIHttpProtocolHandler.idl
@@ -119,24 +119,15 @@ interface nsIHttpProtocolHandler : nsIPr
 /**
  * The observer of this topic is notified before data is read from the cache.
  * The notification is sent if and only if there is no network communication
  * at all.
  */
 #define NS_HTTP_ON_EXAMINE_CACHED_RESPONSE_TOPIC "http-on-examine-cached-response"
 
 /**
- * Before an HTTP request corresponding to a channel with the LOAD_DOCUMENT_URI
- * flag is sent to the server, this observer topic is notified. The observer of
- * this topic can then choose to modify the user agent for this request before
- * the request is actually sent to the server. Additionally, the modified user
- * agent will be propagated to sub-resource requests from the same load group.
- */
-#define NS_HTTP_ON_USERAGENT_REQUEST_TOPIC "http-on-useragent-request"
-
-/**
  * This topic is notified for every http channel right after it called
  * OnStopRequest on its listener, regardless whether it was finished
  * successfully, failed or has been canceled.
  */
 #define NS_HTTP_ON_STOP_REQUEST_TOPIC "http-on-stop-request"
 
 %}
