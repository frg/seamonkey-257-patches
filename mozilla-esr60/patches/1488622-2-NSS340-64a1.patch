# HG changeset patch
# User J.C. Jones <jjones@mozilla.com>
# Date 1536255760 25200
#      Thu Sep 06 10:42:40 2018 -0700
# Node ID 24058c477407069c999e9fdc52d335f54543bbec
# Parent  8be67ab3d7d34909c6e52b7e6984b6ff84cc470a
Bug 1488622 - land NSS 8f6014565b91 UPGRADE_NSS_RELEASE, r=me

diff --git a/security/nss/TAG-INFO b/security/nss/TAG-INFO
--- a/security/nss/TAG-INFO
+++ b/security/nss/TAG-INFO
@@ -1,1 +1,1 @@
-229a3a57f42a
+8f6014565b91
diff --git a/security/nss/coreconf/coreconf.dep b/security/nss/coreconf/coreconf.dep
--- a/security/nss/coreconf/coreconf.dep
+++ b/security/nss/coreconf/coreconf.dep
@@ -5,8 +5,9 @@
 
 /*
  * A dummy header file that is a dependency for all the object files.
  * Used to force a full recompilation of NSS in Mozilla's Tinderbox
  * depend builds.  See comments in rules.mk.
  */
 
 #error "Do not include this header file."
+
diff --git a/security/nss/gtests/ssl_gtest/ssl_resumption_unittest.cc b/security/nss/gtests/ssl_gtest/ssl_resumption_unittest.cc
--- a/security/nss/gtests/ssl_gtest/ssl_resumption_unittest.cc
+++ b/security/nss/gtests/ssl_gtest/ssl_resumption_unittest.cc
@@ -166,33 +166,155 @@ TEST_P(TlsConnectGenericResumption, Conn
 
   Reset();
   ConfigureSessionCache(RESUME_NONE, RESUME_BOTH);
   ExpectResumption(RESUME_NONE);
   Connect();
   SendReceive();
 }
 
-TEST_P(TlsConnectGenericPre13, ConnectResumeWithHigherVersion) {
+TEST_P(TlsConnectGenericPre13, ResumeWithHigherVersionTls13) {
+  uint16_t lower_version = version_;
+  ConfigureSessionCache(RESUME_BOTH, RESUME_BOTH);
+  Connect();
+  SendReceive();
+  CheckKeys();
+
+  Reset();
+  ConfigureSessionCache(RESUME_BOTH, RESUME_BOTH);
+  EnsureTlsSetup();
+  auto psk_ext = std::make_shared<TlsExtensionCapture>(
+      client_, ssl_tls13_pre_shared_key_xtn);
+  auto ticket_ext =
+      std::make_shared<TlsExtensionCapture>(client_, ssl_session_ticket_xtn);
+  client_->SetFilter(std::make_shared<ChainedPacketFilter>(
+      ChainedPacketFilterInit({psk_ext, ticket_ext})));
+  SetExpectedVersion(SSL_LIBRARY_VERSION_TLS_1_3);
+  client_->SetVersionRange(lower_version, SSL_LIBRARY_VERSION_TLS_1_3);
+  server_->SetVersionRange(lower_version, SSL_LIBRARY_VERSION_TLS_1_3);
+  ExpectResumption(RESUME_NONE);
+  Connect();
+
+  // The client shouldn't have sent a PSK, though it will send a ticket.
+  EXPECT_FALSE(psk_ext->captured());
+  EXPECT_TRUE(ticket_ext->captured());
+}
+
+class CaptureSessionId : public TlsHandshakeFilter {
+ public:
+  CaptureSessionId(const std::shared_ptr<TlsAgent>& a)
+      : TlsHandshakeFilter(
+            a, {kTlsHandshakeClientHello, kTlsHandshakeServerHello}),
+        sid_() {}
+
+  const DataBuffer& sid() const { return sid_; }
+
+ protected:
+  PacketFilter::Action FilterHandshake(const HandshakeHeader& header,
+                                       const DataBuffer& input,
+                                       DataBuffer* output) override {
+    // The session_id is in the same place in both Hello messages:
+    size_t offset = 2 + 32;  // Version(2) + Random(32)
+    uint32_t len = 0;
+    EXPECT_TRUE(input.Read(offset, 1, &len));
+    offset++;
+    if (input.len() < offset + len) {
+      ADD_FAILURE() << "session_id overflows the Hello message";
+      return KEEP;
+    }
+    sid_.Assign(input.data() + offset, len);
+    return KEEP;
+  }
+
+ private:
+  DataBuffer sid_;
+};
+
+// Attempting to resume from TLS 1.2 when 1.3 is possible should not result in
+// resumption, though it will appear to be TLS 1.3 compatibility mode if the
+// server uses a session ID.
+TEST_P(TlsConnectGenericPre13, ResumeWithHigherVersionTls13SessionId) {
+  uint16_t lower_version = version_;
   ConfigureSessionCache(RESUME_SESSIONID, RESUME_SESSIONID);
-  ConfigureVersion(SSL_LIBRARY_VERSION_TLS_1_1);
-  SetExpectedVersion(SSL_LIBRARY_VERSION_TLS_1_1);
+  auto original_sid = MakeTlsFilter<CaptureSessionId>(server_);
+  Connect();
+  CheckKeys();
+  EXPECT_EQ(32U, original_sid->sid().len());
+
+  // The client should now attempt to resume with the session ID from the last
+  // connection.  This looks like compatibility mode, we just want to ensure
+  // that we get TLS 1.3 rather than 1.2 (and no resumption).
+  Reset();
+  auto client_sid = MakeTlsFilter<CaptureSessionId>(client_);
+  auto server_sid = MakeTlsFilter<CaptureSessionId>(server_);
+  ConfigureSessionCache(RESUME_SESSIONID, RESUME_SESSIONID);
+  SetExpectedVersion(SSL_LIBRARY_VERSION_TLS_1_3);
+  client_->SetVersionRange(lower_version, SSL_LIBRARY_VERSION_TLS_1_3);
+  server_->SetVersionRange(lower_version, SSL_LIBRARY_VERSION_TLS_1_3);
+  ExpectResumption(RESUME_NONE);
+
+  Connect();
+  SendReceive();
+
+  EXPECT_EQ(client_sid->sid(), original_sid->sid());
+  if (variant_ == ssl_variant_stream) {
+    EXPECT_EQ(client_sid->sid(), server_sid->sid());
+  } else {
+    // DTLS servers don't echo the session ID.
+    EXPECT_EQ(0U, server_sid->sid().len());
+  }
+}
+
+TEST_P(TlsConnectPre12, ResumeWithHigherVersionTls12) {
+  uint16_t lower_version = version_;
+  ConfigureSessionCache(RESUME_BOTH, RESUME_BOTH);
   Connect();
 
   Reset();
+  ConfigureSessionCache(RESUME_BOTH, RESUME_BOTH);
   EnsureTlsSetup();
-  SetExpectedVersion(SSL_LIBRARY_VERSION_TLS_1_2);
-  client_->SetVersionRange(SSL_LIBRARY_VERSION_TLS_1_1,
-                           SSL_LIBRARY_VERSION_TLS_1_2);
-  server_->SetVersionRange(SSL_LIBRARY_VERSION_TLS_1_1,
-                           SSL_LIBRARY_VERSION_TLS_1_2);
+  SetExpectedVersion(SSL_LIBRARY_VERSION_TLS_1_3);
+  client_->SetVersionRange(lower_version, SSL_LIBRARY_VERSION_TLS_1_3);
+  server_->SetVersionRange(lower_version, SSL_LIBRARY_VERSION_TLS_1_3);
   ExpectResumption(RESUME_NONE);
   Connect();
 }
 
+TEST_P(TlsConnectGenericPre13, ResumeWithLowerVersionFromTls13) {
+  uint16_t original_version = version_;
+  ConfigureSessionCache(RESUME_BOTH, RESUME_BOTH);
+  ConfigureVersion(SSL_LIBRARY_VERSION_TLS_1_3);
+  Connect();
+  SendReceive();
+  CheckKeys();
+
+  Reset();
+  ConfigureSessionCache(RESUME_BOTH, RESUME_BOTH);
+  ConfigureVersion(original_version);
+  ExpectResumption(RESUME_NONE);
+  Connect();
+  SendReceive();
+}
+
+TEST_P(TlsConnectPre12, ResumeWithLowerVersionFromTls12) {
+  uint16_t original_version = version_;
+  ConfigureSessionCache(RESUME_BOTH, RESUME_BOTH);
+  ConfigureVersion(SSL_LIBRARY_VERSION_TLS_1_2);
+  Connect();
+  SendReceive();
+  CheckKeys();
+
+  Reset();
+  ConfigureSessionCache(RESUME_BOTH, RESUME_BOTH);
+  ConfigureVersion(original_version);
+  ExpectResumption(RESUME_NONE);
+  Connect();
+  SendReceive();
+}
+
 TEST_P(TlsConnectGeneric, ConnectResumeClientBothTicketServerTicketForget) {
   // This causes a ticket resumption.
   ConfigureSessionCache(RESUME_BOTH, RESUME_TICKET);
   Connect();
   SendReceive();
 
   Reset();
   ClearServerCache();
diff --git a/security/nss/lib/freebl/freebl_base.gypi b/security/nss/lib/freebl/freebl_base.gypi
--- a/security/nss/lib/freebl/freebl_base.gypi
+++ b/security/nss/lib/freebl/freebl_base.gypi
@@ -109,18 +109,17 @@
             'arcfour-amd64-masm.asm',
             'mpi/mpi_amd64.c',
             'mpi/mpi_amd64_masm.asm',
             'mpi/mp_comba_amd64_masm.asm',
             'intel-aes-x64-masm.asm',
             'intel-gcm-x64-masm.asm',
           ],
         }],
-	      [ 'cc_use_gnu_ld!=1 and target_arch!="x64"', {
-          # not x64
+        [ 'cc_use_gnu_ld!=1 and target_arch=="ia32"', {
           'sources': [
             'mpi/mpi_x86_asm.c',
             'intel-aes-x86-masm.asm',
             'intel-gcm-x86-masm.asm',
           ],
         }],
         [ 'cc_use_gnu_ld==1', {
           # mingw
diff --git a/security/nss/lib/ssl/ssl3con.c b/security/nss/lib/ssl/ssl3con.c
--- a/security/nss/lib/ssl/ssl3con.c
+++ b/security/nss/lib/ssl/ssl3con.c
@@ -6381,25 +6381,28 @@ ssl_CheckServerSessionIdCorrectness(sslS
             PRUint8 buf[SSL3_SESSIONID_BYTES];
             ssl_MakeFakeSid(ss, buf);
             sidMatch = PORT_Memcmp(buf, sidBytes->data, sidBytes->len) == 0;
         }
     }
 
     /* TLS 1.2: Session ID shouldn't match if we sent a fake. */
     if (ss->version < SSL_LIBRARY_VERSION_TLS_1_3) {
-        return !sentFakeSid || !sidMatch;
+        if (sentFakeSid) {
+            return !sidMatch;
+        }
+        return PR_TRUE;
     }
 
     /* TLS 1.3: We sent a session ID.  The server's should match. */
-    if (sentRealSid || sentFakeSid) {
+    if (!IS_DTLS(ss) && (sentRealSid || sentFakeSid)) {
         return sidMatch;
     }
 
-    /* TLS 1.3: The server shouldn't send a session ID. */
+    /* TLS 1.3 (no SID)/DTLS 1.3: The server shouldn't send a session ID. */
     return sidBytes->len == 0;
 }
 
 static SECStatus
 ssl_CheckServerRandom(sslSocket *ss)
 {
     /* Check the ServerHello.random per [RFC 8446 Section 4.1.3].
      *
diff --git a/security/nss/lib/ssl/tls13con.c b/security/nss/lib/ssl/tls13con.c
--- a/security/nss/lib/ssl/tls13con.c
+++ b/security/nss/lib/ssl/tls13con.c
@@ -2494,16 +2494,17 @@ tls13_HandleServerHelloPart2(sslSocket *
             PORT_Assert(ss->statelessResume);
             PK11_FreeSymKey(ss->ssl3.hs.currentSecret);
             ss->ssl3.hs.currentSecret = NULL;
         }
         ss->statelessResume = PR_FALSE;
     }
 
     if (ss->statelessResume) {
+        PORT_Assert(sid->version >= SSL_LIBRARY_VERSION_TLS_1_3);
         if (tls13_GetHash(ss) !=
             tls13_GetHashForCipherSuite(sid->u.ssl3.cipherSuite)) {
             FATAL_ERROR(ss, SSL_ERROR_RX_MALFORMED_SERVER_HELLO,
                         illegal_parameter);
             return SECFailure;
         }
     }
 
diff --git a/security/nss/lib/ssl/tls13exthandle.c b/security/nss/lib/ssl/tls13exthandle.c
--- a/security/nss/lib/ssl/tls13exthandle.c
+++ b/security/nss/lib/ssl/tls13exthandle.c
@@ -391,16 +391,17 @@ tls13_ClientSendPreSharedKeyXtn(const ss
     }
 
     /* Save where this extension starts so that if we have to add padding, it
      * can be inserted before this extension. */
     PORT_Assert(buf->len >= 4);
     xtnData->lastXtnOffset = buf->len - 4;
 
     PORT_Assert(ss->vrange.max >= SSL_LIBRARY_VERSION_TLS_1_3);
+    PORT_Assert(ss->sec.ci.sid->version >= SSL_LIBRARY_VERSION_TLS_1_3);
 
     /* Send a single ticket identity. */
     session_ticket = &ss->sec.ci.sid->u.ssl3.locked.sessionTicket;
     rv = sslBuffer_AppendNumber(buf, 2 +                              /* identity length */
                                          session_ticket->ticket.len + /* ticket */
                                          4 /* obfuscated_ticket_age */,
                                 2);
     if (rv != SECSuccess)
