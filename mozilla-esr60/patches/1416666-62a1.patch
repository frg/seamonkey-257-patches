# HG changeset patch
# User Andi-Bogdan Postelnicu <bpostelnicu@mozilla.com>
# Date 1525782929 -10800
# Node ID e2b1051804d110424c746f4f3e8a3aa4396fb63b
# Parent  03627a11f930a10b36ddb062ac19eb32dea2be80
Bug 1416666 - Avoid doing a memset on a non-POD structure.  r=jwalden

diff --git a/js/public/HashTable.h b/js/public/HashTable.h
--- a/js/public/HashTable.h
+++ b/js/public/HashTable.h
@@ -7,16 +7,17 @@
 #ifndef js_HashTable_h
 #define js_HashTable_h
 
 #include "mozilla/Alignment.h"
 #include "mozilla/Assertions.h"
 #include "mozilla/Attributes.h"
 #include "mozilla/Casting.h"
 #include "mozilla/HashFunctions.h"
+#include "mozilla/MemoryChecking.h"
 #include "mozilla/MemoryReporting.h"
 #include "mozilla/Move.h"
 #include "mozilla/Opaque.h"
 #include "mozilla/PodOperations.h"
 #include "mozilla/ReentrancyGuard.h"
 #include "mozilla/TemplateLib.h"
 #include "mozilla/TypeTraits.h"
 #include "mozilla/UniquePtr.h"
@@ -782,26 +783,32 @@ class HashTableEntry {
   static const HashNumber sCollisionBit = 1;
 
   static bool isLiveHash(HashNumber hash) { return hash > sRemovedKey; }
 
   HashTableEntry(const HashTableEntry&) = delete;
   void operator=(const HashTableEntry&) = delete;
   ~HashTableEntry() = delete;
 
+  void destroyStoredT() {
+    mem.addr()->~T();
+    MOZ_MAKE_MEM_UNDEFINED(mem.addr(), sizeof(*mem.addr()));
+  }
+
  public:
   // NB: HashTableEntry is treated as a POD: no constructor or destructor calls.
 
   void destroyIfLive() {
-    if (isLive()) mem.addr()->~T();
+    if (isLive())
+      destroyStoredT();
   }
 
   void destroy() {
     MOZ_ASSERT(isLive());
-    mem.addr()->~T();
+    destroyStoredT();
   }
 
   void swap(HashTableEntry* other) {
     if (this == other) return;
     MOZ_ASSERT(isLive());
     if (other->isLive()) {
       mozilla::Swap(*mem.addr(), *other->mem.addr());
     } else {
@@ -819,28 +826,34 @@ class HashTableEntry {
     MOZ_ASSERT(isLive());
     return *mem.addr();
   }
 
   bool isFree() const { return keyHash == sFreeKey; }
   void clearLive() {
     MOZ_ASSERT(isLive());
     keyHash = sFreeKey;
-    mem.addr()->~T();
+    destroyStoredT();
   }
   void clear() {
-    if (isLive()) mem.addr()->~T();
+    if (isLive())
+      destroyStoredT();
+
+    MOZ_MAKE_MEM_UNDEFINED(this, sizeof(*this));
     keyHash = sFreeKey;
   }
+
   bool isRemoved() const { return keyHash == sRemovedKey; }
+
   void removeLive() {
     MOZ_ASSERT(isLive());
     keyHash = sRemovedKey;
-    mem.addr()->~T();
+    destroyStoredT();
   }
+
   bool isLive() const { return isLiveHash(keyHash); }
   void setCollision() {
     MOZ_ASSERT(isLive());
     keyHash |= sCollisionBit;
   }
   void unsetCollision() { keyHash &= ~sCollisionBit; }
   bool hasCollision() const { return keyHash & sCollisionBit; }
   bool matchHash(HashNumber hn) { return (keyHash & ~sCollisionBit) == hn; }
@@ -1604,23 +1617,20 @@ class HashTable : private AllocPolicy {
     entryCount++;
 #ifdef JS_DEBUG
     mutationCount++;
 #endif
   }
 
  public:
   void clear() {
-    if (mozilla::IsPod<Entry>::value) {
-      memset(table, 0, sizeof(*table) * capacity());
-    } else {
-      uint32_t tableCapacity = capacity();
-      Entry* end = table + tableCapacity;
-      for (Entry* e = table; e < end; ++e) e->clear();
-    }
+    Entry* end = table + capacity();
+    for (Entry* e = table; e < end; ++e)
+      e->clear();
+
     removedCount = 0;
     entryCount = 0;
 #ifdef JS_DEBUG
     mutationCount++;
 #endif
   }
 
   void clearAndShrink() {
