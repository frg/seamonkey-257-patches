# HG changeset patch
# User Boris Zbarsky <bzbarsky@mit.edu>
# Date 1524279885 14400
# Node ID 07be8e0c8d40dea981555098f9abb32ae6faa83a
# Parent  02e8c26820aea021988f9c9476401e0bd0df44f4
Bug 1453869 part 13.  Make DOMParser store an nsIGlobalObject* as mOwner.  r=mrbkap

Incidentally, we can remove mScriptHandlingObject, because mOwner is always the same object anyway.

MozReview-Commit-ID: 1txkjkKvBsi

diff --git a/dom/base/DOMParser.cpp b/dom/base/DOMParser.cpp
--- a/dom/base/DOMParser.cpp
+++ b/dom/base/DOMParser.cpp
@@ -23,17 +23,17 @@
 #include "NullPrincipal.h"
 #include "mozilla/LoadInfo.h"
 #include "mozilla/dom/BindingUtils.h"
 #include "mozilla/dom/ScriptSettings.h"
 
 using namespace mozilla;
 using namespace mozilla::dom;
 
-DOMParser::DOMParser(nsISupports* aOwner, nsIPrincipal* aDocPrincipal)
+DOMParser::DOMParser(nsIGlobalObject* aOwner, nsIPrincipal* aDocPrincipal)
   : mOwner(aOwner),
     mPrincipal(aDocPrincipal),
     mAttemptedInit(false),
     mForceEnableXULXBL(false) {
   MOZ_ASSERT(aOwner);
   MOZ_ASSERT(aDocPrincipal);
 }
 
@@ -227,17 +227,16 @@ nsresult DOMParser::Init(nsIURI* documen
 
   if (!mDocumentURI) {
     mPrincipal->GetURI(getter_AddRefs(mDocumentURI));
     if (!mDocumentURI) {
       return NS_ERROR_INVALID_ARG;
     }
   }
 
-  mScriptHandlingObject = do_GetWeakReference(aScriptObject);
   mBaseURI = baseURI;
 
   MOZ_ASSERT(mPrincipal, "Must have principal");
   MOZ_ASSERT(mDocumentURI, "Must have document URI");
   return NS_OK;
 }
 
 /*static */ already_AddRefed<DOMParser> DOMParser::Constructor(
@@ -261,36 +260,35 @@ nsresult DOMParser::Init(nsIURI* documen
     baseURI = window->GetDocBaseURI();
     documentURI = window->GetDocumentURI();
     if (!documentURI) {
       rv.Throw(NS_ERROR_UNEXPECTED);
       return nullptr;
     }
   }
 
-  RefPtr<DOMParser> domParser = new DOMParser(aOwner.GetAsSupports(),
-                                              docPrincipal);
+  nsCOMPtr<nsIGlobalObject> global = do_QueryInterface(aOwner.GetAsSupports());
+  RefPtr<DOMParser> domParser = new DOMParser(global, docPrincipal);
 
-  nsCOMPtr<nsIGlobalObject> global = do_QueryInterface(aOwner.GetAsSupports());
   rv = domParser->Init(documentURI, baseURI, global);
   if (rv.Failed()) {
     return nullptr;
   }
   return domParser.forget();
 }
 
 already_AddRefed<nsIDocument> DOMParser::SetUpDocument(DocumentFlavor aFlavor,
                                                        ErrorResult& aRv) {
-  // We should really QI to nsIGlobalObject here, but nsDocument gets confused
+  // We should really just use mOwner here, but nsDocument gets confused
   // if we pass it a scriptHandlingObject that doesn't QI to
   // nsIScriptGlobalObject, and test_isequalnode.js (an xpcshell test without
   // a window global) breaks. The correct solution is just to wean nsDocument
   // off of nsIScriptGlobalObject, but that's a yak to shave another day.
   nsCOMPtr<nsIScriptGlobalObject> scriptHandlingObject =
-      do_QueryReferent(mScriptHandlingObject);
+      do_QueryInterface(mOwner);
 
   // Try to inherit a style backend.
   NS_ASSERTION(mPrincipal, "Must have principal by now");
   NS_ASSERTION(mDocumentURI, "Must have document URI by now");
 
   nsCOMPtr<nsIDOMDocument> domDoc;
   nsresult rv = NS_NewDOMDocument(getter_AddRefs(domDoc), EmptyString(),
                                   EmptyString(), nullptr, mDocumentURI,
diff --git a/dom/base/DOMParser.h b/dom/base/DOMParser.h
--- a/dom/base/DOMParser.h
+++ b/dom/base/DOMParser.h
@@ -13,16 +13,17 @@
 #include "nsWeakReference.h"
 #include "nsWrapperCache.h"
 #include "mozilla/ErrorResult.h"
 #include "mozilla/Span.h"
 #include "mozilla/dom/DOMParserBinding.h"
 #include "mozilla/dom/TypedArray.h"
 
 class nsIDocument;
+class nsIGlobalObject;
 
 namespace mozilla {
 namespace dom {
 
 class DOMParser final : public nsIDOMParser,
                         public nsSupportsWeakReference,
                         public nsWrapperCache {
   typedef mozilla::dom::GlobalObject GlobalObject;
@@ -53,25 +54,25 @@ class DOMParser final : public nsIDOMPar
       const Uint8Array& aBuf, SupportedType aType, ErrorResult& aRv);
 
   already_AddRefed<nsIDocument> ParseFromStream(
       nsIInputStream* aStream, const nsAString& aCharset,
       int32_t aContentLength, SupportedType aType, ErrorResult& aRv);
 
   void ForceEnableXULXBL() { mForceEnableXULXBL = true; }
 
-  nsISupports* GetParentObject() const { return mOwner; }
+  nsIGlobalObject* GetParentObject() const { return mOwner; }
 
   virtual JSObject* WrapObject(JSContext* aCx,
                                JS::Handle<JSObject*> aGivenProto) override {
     return mozilla::dom::DOMParserBinding::Wrap(aCx, this, aGivenProto);
   }
 
  private:
-  DOMParser(nsISupports* aOwner, nsIPrincipal* aDocPrincipal);
+  DOMParser(nsIGlobalObject* aOwner, nsIPrincipal* aDocPrincipal);
 
   /**
    * Initialize the principal and document and base URIs that the parser should
    * use for documents it creates.  If this is not called, then a null
    * principal and its URI will be used.  When creating a DOMParser via the JS
    * constructor, this will be called automatically.  This method may only be
    * called once.  If this method fails, all following parse attempts will
    * fail.
@@ -97,21 +98,20 @@ class DOMParser final : public nsIDOMPar
         : mAttemptedInit(aAttemptedInit) {}
 
     ~AttemptedInitMarker() { *mAttemptedInit = true; }
 
    private:
     bool* mAttemptedInit;
   };
 
-  nsCOMPtr<nsISupports> mOwner;
+  nsCOMPtr<nsIGlobalObject> mOwner;
   nsCOMPtr<nsIPrincipal> mPrincipal;
   nsCOMPtr<nsIURI> mDocumentURI;
   nsCOMPtr<nsIURI> mBaseURI;
-  nsWeakPtr mScriptHandlingObject;
 
   bool mAttemptedInit;
   bool mForceEnableXULXBL;
 };
 
 }  // namespace dom
 }  // namespace mozilla
 
