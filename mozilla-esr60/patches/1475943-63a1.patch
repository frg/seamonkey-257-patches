# HG changeset patch
# User Benjamin Bouvier <benj@benj.me>
# Date 1532022998 -7200
# Node ID 5b31b8bf2d680fba2f46cfa99576f1027bed328f
# Parent  ccb0d8efb3ca04185a4d4a230c937f5ec16f7b65
Bug 1475943: Don't trace a wasm global object if it's being created; r=jseward

diff --git a/js/src/jit-test/tests/wasm/gc/fuzz-gc-while-allocating-global.js b/js/src/jit-test/tests/wasm/gc/fuzz-gc-while-allocating-global.js
new file mode 100644
--- /dev/null
+++ b/js/src/jit-test/tests/wasm/gc/fuzz-gc-while-allocating-global.js
@@ -0,0 +1,7 @@
+if (!wasmGcEnabled()) {
+    quit(0);
+}
+
+enableShellAllocationMetadataBuilder();
+gczeal(9, 1);
+new WebAssembly.Global({ value: 'i32' }, 42);
diff --git a/js/src/wasm/WasmJS.cpp b/js/src/wasm/WasmJS.cpp
--- a/js/src/wasm/WasmJS.cpp
+++ b/js/src/wasm/WasmJS.cpp
@@ -1926,16 +1926,22 @@ const ClassOps WasmGlobalObject::classOp
 const Class WasmGlobalObject::class_ = {
     "WebAssembly.Global",
     JSCLASS_HAS_RESERVED_SLOTS(WasmGlobalObject::RESERVED_SLOTS) |
     JSCLASS_BACKGROUND_FINALIZE,
     &WasmGlobalObject::classOps_};
 
 /* static */ void WasmGlobalObject::trace(JSTracer* trc, JSObject* obj) {
   WasmGlobalObject* global = reinterpret_cast<WasmGlobalObject*>(obj);
+  if (global->isNewborn()) {
+    // This can happen while we're allocating the object, in which case
+    // every single slot of the object is not defined yet. In particular,
+    // there's nothing to trace yet.
+    return;
+  }
   switch (global->type().code()) {
     case ValType::AnyRef:
       if (global->cell()->ptr)
         TraceManuallyBarrieredEdge(trc, &global->cell()->ptr,
                                    "wasm anyref global");
       break;
     case ValType::I32:
     case ValType::F32:
@@ -2002,16 +2008,18 @@ const Class WasmGlobalObject::class_ = {
       MOZ_CRASH("Ref NYI");
   }
 
   obj->initReservedSlot(TYPE_SLOT,
                         Int32Value(int32_t(val.type().bitsUnsafe())));
   obj->initReservedSlot(MUTABLE_SLOT, JS::BooleanValue(isMutable));
   obj->initReservedSlot(CELL_SLOT, PrivateValue(cell));
 
+  MOZ_ASSERT(!obj->isNewborn());
+
   return obj;
 }
 
 /* static */ bool WasmGlobalObject::construct(JSContext* cx, unsigned argc,
                                               Value* vp) {
   CallArgs args = CallArgsFromVp(argc, vp);
 
   if (!ThrowIfNotConstructing(cx, args, "Global")) return false;
