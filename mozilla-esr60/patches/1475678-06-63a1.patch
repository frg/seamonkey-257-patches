# HG changeset patch
# User Andre Bargull <andre.bargull@gmail.com>
# Date 1532070858 25200
# Node ID 297e8205c591485cca7094cd704b440b84782f15
# Parent  7eecedb301711af43693a1251d28eade4f73a79b
Bug 1475678 - Part 6: Remove unnecessary rooting in Promise code. r=arai

diff --git a/js/src/builtin/Promise.cpp b/js/src/builtin/Promise.cpp
--- a/js/src/builtin/Promise.cpp
+++ b/js/src/builtin/Promise.cpp
@@ -160,18 +160,18 @@ class PromiseAllDataHolder : public Nati
 const Class PromiseAllDataHolder::class_ = {
     "PromiseAllDataHolder",
     JSCLASS_HAS_RESERVED_SLOTS(PromiseAllDataHolderSlots)};
 
 static PromiseAllDataHolder* NewPromiseAllDataHolder(JSContext* cx,
                                                      HandleObject resultPromise,
                                                      HandleValue valuesArray,
                                                      HandleObject resolve) {
-  Rooted<PromiseAllDataHolder*> dataHolder(
-      cx, NewObjectWithClassProto<PromiseAllDataHolder>(cx));
+  PromiseAllDataHolder* dataHolder =
+      NewObjectWithClassProto<PromiseAllDataHolder>(cx);
   if (!dataHolder) return nullptr;
 
   assertSameCompartment(cx, resultPromise);
   assertSameCompartment(cx, valuesArray);
   assertSameCompartment(cx, resolve);
 
   dataHolder->setFixedSlot(PromiseAllDataHolderSlot_Promise,
                            ObjectValue(*resultPromise));
@@ -601,38 +601,39 @@ static MOZ_MUST_USE bool RejectMaybeWrap
 static MOZ_MUST_USE bool RejectPromiseInternal(JSContext* cx,
                                                Handle<PromiseObject*> promise,
                                                HandleValue reason);
 
 // ES2016, 25.4.1.3.1.
 static bool RejectPromiseFunction(JSContext* cx, unsigned argc, Value* vp) {
   CallArgs args = CallArgsFromVp(argc, vp);
 
-  RootedFunction reject(cx, &args.callee().as<JSFunction>());
+  JSFunction* reject = &args.callee().as<JSFunction>();
   HandleValue reasonVal = args.get(0);
 
   // Steps 1-2.
-  RootedValue promiseVal(cx,
-                         reject->getExtendedSlot(RejectFunctionSlot_Promise));
+  const Value& promiseVal = reject->getExtendedSlot(RejectFunctionSlot_Promise);
 
   // Steps 3-4.
   // If the Promise isn't available anymore, it has been resolved and the
   // reference to it removed to make it eligible for collection.
   if (promiseVal.isUndefined()) {
     args.rval().setUndefined();
     return true;
   }
 
+  // Store the promise value in |promise| before ClearResolutionFunctionSlots
+  // removes the reference.
+  RootedObject promise(cx, &promiseVal.toObject());
+
   // Step 5.
   // Here, we only remove the Promise reference from the resolution
   // functions. Actually marking it as fulfilled/rejected happens later.
   ClearResolutionFunctionSlots(reject);
 
-  RootedObject promise(cx, &promiseVal.toObject());
-
   // In some cases the Promise reference on the resolution function won't
   // have been removed during resolution, so we need to check that here,
   // too.
   if (IsSettledMaybeWrappedPromise(promise)) {
     args.rval().setUndefined();
     return true;
   }
 
@@ -742,17 +743,17 @@ static MOZ_MUST_USE bool ResolvePromiseI
   // Step 13.
   return true;
 }
 
 // ES2016, 25.4.1.3.2.
 static bool ResolvePromiseFunction(JSContext* cx, unsigned argc, Value* vp) {
   CallArgs args = CallArgsFromVp(argc, vp);
 
-  RootedFunction resolve(cx, &args.callee().as<JSFunction>());
+  JSFunction* resolve = &args.callee().as<JSFunction>();
   HandleValue resolutionVal = args.get(0);
 
   // Steps 3-4 (reordered).
   // We use the reference to the reject function as a signal for whether
   // the resolve or reject function was already called, at which point
   // the references on each of the functions are cleared.
   if (!resolve->getExtendedSlot(ResolveFunctionSlot_RejectFunction)
            .isObject()) {
@@ -835,23 +836,21 @@ MOZ_MUST_USE static bool EnqueuePromiseR
 
   // If we have a handler callback, we enter that handler's compartment so
   // that the promise reaction job function is created in that compartment.
   // That guarantees that the embedding ends up with the right entry global.
   // This is relevant for some html APIs like fetch that derive information
   // from said global.
   mozilla::Maybe<AutoRealm> ar2;
   if (handler.isObject()) {
-    RootedObject handlerObj(cx, &handler.toObject());
-
     // The unwrapping has to be unchecked because we specifically want to
     // be able to use handlers with wrappers that would only allow calls.
     // E.g., it's ok to have a handler from a chrome compartment in a
     // reaction to a content compartment's Promise instance.
-    handlerObj = UncheckedUnwrap(handlerObj);
+    JSObject* handlerObj = UncheckedUnwrap(&handler.toObject());
     MOZ_ASSERT(handlerObj);
     ar2.emplace(cx, handlerObj);
 
     // We need to wrap the reaction to store it on the job function.
     if (!cx->compartment()->wrap(cx, &reactionVal)) return false;
   }
 
   // Create the JS function to call when the job is triggered.
@@ -881,18 +880,17 @@ MOZ_MUST_USE static bool EnqueuePromiseR
     if (!cx->compartment()->wrap(cx, &promise)) return false;
   }
 
   // Using objectFromIncumbentGlobal, we can derive the incumbent global by
   // unwrapping and then getting the global. This is very convoluted, but
   // much better than having to store the original global as a private value
   // because we couldn't wrap it to store it as a normal JS value.
   RootedObject global(cx);
-  RootedObject objectFromIncumbentGlobal(cx, reaction->incumbentGlobalObject());
-  if (objectFromIncumbentGlobal) {
+  if (JSObject* objectFromIncumbentGlobal = reaction->incumbentGlobalObject()) {
     objectFromIncumbentGlobal = CheckedUnwrap(objectFromIncumbentGlobal);
     MOZ_ASSERT(objectFromIncumbentGlobal);
     global = &objectFromIncumbentGlobal->nonCCWGlobal();
   }
 
   // Note: the global we pass here might be from a different compartment
   // than job and promise. While it's somewhat unusual to pass objects
   // from multiple compartments, in this case we specifically need the
@@ -1072,27 +1070,27 @@ static MOZ_MUST_USE bool NewPromiseCapab
   // Step 5 (omitted).
 
   // Step 6.
   FixedConstructArgs<1> cargs(cx);
   cargs[0].setObject(*executor);
   if (!Construct(cx, cVal, cargs, cVal, promise)) return false;
 
   // Step 7.
-  RootedValue resolveVal(
-      cx, executor->getExtendedSlot(GetCapabilitiesExecutorSlots_Resolve));
+  const Value& resolveVal =
+      executor->getExtendedSlot(GetCapabilitiesExecutorSlots_Resolve);
   if (!IsCallable(resolveVal)) {
     JS_ReportErrorNumberASCII(cx, GetErrorMessage, nullptr,
                               JSMSG_PROMISE_RESOLVE_FUNCTION_NOT_CALLABLE);
     return false;
   }
 
   // Step 8.
-  RootedValue rejectVal(
-      cx, executor->getExtendedSlot(GetCapabilitiesExecutorSlots_Reject));
+  const Value& rejectVal =
+      executor->getExtendedSlot(GetCapabilitiesExecutorSlots_Reject);
   if (!IsCallable(rejectVal)) {
     JS_ReportErrorNumberASCII(cx, GetErrorMessage, nullptr,
                               JSMSG_PROMISE_REJECT_FUNCTION_NOT_CALLABLE);
     return false;
   }
 
   // Step 9 (well, the equivalent for all of promiseCapabilities' fields.)
   resolve.set(&resolveVal.toObject());
@@ -1100,17 +1098,17 @@ static MOZ_MUST_USE bool NewPromiseCapab
 
   // Step 10.
   return true;
 }
 
 // ES2016, 25.4.1.5.1.
 static bool GetCapabilitiesExecutor(JSContext* cx, unsigned argc, Value* vp) {
   CallArgs args = CallArgsFromVp(argc, vp);
-  RootedFunction F(cx, &args.callee().as<JSFunction>());
+  JSFunction* F = &args.callee().as<JSFunction>();
 
   // Steps 1-2 (implicit).
 
   // Steps 3-4.
   if (!F->getExtendedSlot(GetCapabilitiesExecutorSlots_Resolve).isUndefined() ||
       !F->getExtendedSlot(GetCapabilitiesExecutorSlots_Reject).isUndefined()) {
     JS_ReportErrorNumberASCII(cx, GetErrorMessage, nullptr,
                               JSMSG_PROMISE_CAPABILITY_HAS_SOMETHING_ALREADY);
@@ -1153,17 +1151,17 @@ static MOZ_MUST_USE bool RejectMaybeWrap
     // Promise's reaction handler wants to do anything useful with it. To
     // avoid that situation, we synthesize a generic error that doesn't
     // expose any privileged information but can safely be used in the
     // rejection handler.
     if (!promise->compartment()->wrap(cx, &reason)) return false;
     if (reason.isObject() && !CheckedUnwrap(&reason.toObject())) {
       // Report the existing reason, so we don't just drop it on the
       // floor.
-      RootedObject realReason(cx, UncheckedUnwrap(&reason.toObject()));
+      JSObject* realReason = UncheckedUnwrap(&reason.toObject());
       RootedValue realReasonVal(cx, ObjectValue(*realReason));
       RootedObject realGlobal(cx, &realReason->nonCCWGlobal());
       ReportErrorToGlobal(cx, realGlobal, realReasonVal);
 
       // Async stacks are only properly adopted if there's at least one
       // interpreter frame active right now. If a thenable job with a
       // throwing `then` function got us here, that'll not be the case,
       // so we add one by throwing the error from self-hosted code.
@@ -1191,19 +1189,18 @@ static MOZ_MUST_USE bool TriggerPromiseR
     return EnqueuePromiseReactionJob(cx, reactions, valueOrReason, state);
   }
 
   HandleNativeObject reactionsList = reactions.as<NativeObject>();
   uint32_t reactionsCount = reactionsList->getDenseInitializedLength();
   MOZ_ASSERT(reactionsCount > 1, "Reactions list should be created lazily");
 
   RootedObject reaction(cx);
-  RootedValue reactionVal(cx);
   for (uint32_t i = 0; i < reactionsCount; i++) {
-    reactionVal = reactionsList->getDenseElement(i);
+    const Value& reactionVal = reactionsList->getDenseElement(i);
     MOZ_RELEASE_ASSERT(reactionVal.isObject());
     reaction = &reactionVal.toObject();
     if (!EnqueuePromiseReactionJob(cx, reaction, valueOrReason, state))
       return false;
   }
 
   return true;
 }
@@ -1412,17 +1409,17 @@ static bool PromiseReactionJob(JSContext
     } else {
       MOZ_ASSERT(
           handlerNum == PromiseHandlerAsyncFromSyncIteratorValueUnwrapDone ||
           handlerNum == PromiseHandlerAsyncFromSyncIteratorValueUnwrapNotDone);
 
       bool done =
           handlerNum == PromiseHandlerAsyncFromSyncIteratorValueUnwrapDone;
       // Async Iteration proposal 11.1.3.2.5 step 1.
-      RootedObject resultObj(cx, CreateIterResultObject(cx, argument, done));
+      JSObject* resultObj = CreateIterResultObject(cx, argument, done);
       if (!resultObj) return false;
 
       handlerResult = ObjectValue(*resultObj);
     }
   } else {
     MOZ_ASSERT(handlerVal.isObject());
     MOZ_ASSERT(IsCallable(handlerVal));
 
@@ -1821,18 +1818,18 @@ static bool PromiseConstructor(JSContext
     JSObject* unwrappedNewTarget = CheckedUnwrap(newTarget);
     MOZ_ASSERT(unwrappedNewTarget);
     MOZ_ASSERT(unwrappedNewTarget != newTarget);
 
     newTarget = unwrappedNewTarget;
     {
       AutoRealm ar(cx, newTarget);
       Handle<GlobalObject*> global = cx->global();
-      RootedObject promiseCtor(
-          cx, GlobalObject::getOrCreatePromiseConstructor(cx, global));
+      JSFunction* promiseCtor =
+           GlobalObject::getOrCreatePromiseConstructor(cx, global);
       if (!promiseCtor) return false;
 
       // Promise subclasses don't get the special Xray treatment, so
       // we only need to do the complex wrapping and unwrapping scheme
       // described above for instances of Promise itself.
       if (newTarget == promiseCtor) {
         needsWrapping = true;
         proto = GlobalObject::getOrCreatePromisePrototype(cx, cx->global());
@@ -2068,17 +2065,16 @@ MOZ_MUST_USE JSObject* js::GetWaitForAll
     // every step of the iterator.  In particular, this holds the
     // remainingElementsCount (as an integer reserved slot), the array of
     // values, and the resolve function from our PromiseCapability.
     RootedValue valuesArrayVal(cx, ObjectValue(*valuesArray));
     Rooted<PromiseAllDataHolder*> dataHolder(
         cx,
         NewPromiseAllDataHolder(cx, resultPromise, valuesArrayVal, resolve));
     if (!dataHolder) return nullptr;
-    RootedValue dataHolderVal(cx, ObjectValue(*dataHolder));
 
     // Sub-step 5 (inline in loop-header below).
 
     // Sub-step 6.
     for (uint32_t index = 0; index < promiseCount; index++) {
       // Steps a-c (omitted).
       // Step d (implemented after the loop).
       // Steps e-g (omitted).
@@ -2093,17 +2089,17 @@ MOZ_MUST_USE JSObject* js::GetWaitForAll
       RootedFunction resolveFunc(
           cx,
           NewNativeFunction(cx, PromiseAllResolveElementFunction, 1, nullptr,
                             gc::AllocKind::FUNCTION_EXTENDED, GenericObject));
       if (!resolveFunc) return nullptr;
 
       // Steps k-o.
       resolveFunc->setExtendedSlot(PromiseAllResolveElementFunctionSlot_Data,
-                                   dataHolderVal);
+                                   ObjectValue(*dataHolder));
       resolveFunc->setExtendedSlot(
           PromiseAllResolveElementFunctionSlot_ElementIndex, Int32Value(index));
 
       // Step p.
       dataHolder->increaseRemainingCount();
 
       // Step q, very roughly.
       RootedValue resolveFunVal(cx, ObjectValue(*resolveFunc));
@@ -2186,22 +2182,16 @@ static MOZ_MUST_USE bool RunResolutionFu
 static MOZ_MUST_USE bool PerformPromiseAll(JSContext* cx,
                                            JS::ForOfIterator& iterator,
                                            HandleObject C,
                                            HandleObject promiseObj,
                                            HandleObject resolve,
                                            HandleObject reject, bool* done) {
   *done = false;
 
-  RootedObject unwrappedPromiseObj(cx);
-  if (IsWrapper(promiseObj)) {
-    unwrappedPromiseObj = CheckedUnwrap(promiseObj);
-    MOZ_ASSERT(unwrappedPromiseObj);
-  }
-
   // Step 1.
   MOZ_ASSERT(C->isConstructor());
   RootedValue CVal(cx, ObjectValue(*C));
 
   // Step 2 (omitted).
 
   // Step 3.
   // We have to be very careful about which compartments we create things in
@@ -2219,17 +2209,20 @@ static MOZ_MUST_USE bool PerformPromiseA
   // cross-compartment wrapper to the values array in the holder.  This
   // should be OK because the only things we hand the
   // PromiseAllResolveElement function to are the "then" calls we do and in
   // the case when the Promise's compartment is not the current compartment
   // those are happening over Xrays anyway, which means they get the
   // canonical "then" function and content can't see our
   // PromiseAllResolveElement.
   RootedObject valuesArray(cx);
-  if (unwrappedPromiseObj) {
+  if (IsWrapper(promiseObj)) {
+    JSObject* unwrappedPromiseObj = CheckedUnwrap(promiseObj);
+    MOZ_ASSERT(unwrappedPromiseObj);
+
     JSAutoRealm ar(cx, unwrappedPromiseObj);
     valuesArray = NewDenseFullyAllocatedArray(cx, 0);
   } else {
     valuesArray = NewDenseFullyAllocatedArray(cx, 0);
   }
   if (!valuesArray) return false;
 
   RootedValue valuesArrayVal(cx, ObjectValue(*valuesArray));
@@ -2238,25 +2231,27 @@ static MOZ_MUST_USE bool PerformPromiseA
   // Step 4.
   // Create our data holder that holds all the things shared across
   // every step of the iterator.  In particular, this holds the
   // remainingElementsCount (as an integer reserved slot), the array of
   // values, and the resolve function from our PromiseCapability.
   Rooted<PromiseAllDataHolder*> dataHolder(
       cx, NewPromiseAllDataHolder(cx, promiseObj, valuesArrayVal, resolve));
   if (!dataHolder) return false;
-  RootedValue dataHolderVal(cx, ObjectValue(*dataHolder));
 
   // Step 5.
   uint32_t index = 0;
 
   // Step 6.
   RootedValue nextValue(cx);
+  RootedValue nextPromise(cx);
   RootedId indexId(cx);
   RootedValue rejectFunVal(cx, ObjectValue(*reject));
+  RootedValue resolveFunVal(cx);
+  RootedValue staticResolve(cx);
 
   while (true) {
     // Steps a-c, e-g.
     if (!iterator.next(&nextValue, done)) {
       // Steps b, f.
       *done = true;
 
       // Steps c, g.
@@ -2288,65 +2283,63 @@ static MOZ_MUST_USE bool PerformPromiseA
       indexId = INT_TO_JSID(index);
       if (!DefineDataProperty(cx, valuesArray, indexId, UndefinedHandleValue))
         return false;
     }
 
     // Step i.
     // Sadly, because someone could have overridden
     // "resolve" on the canonical Promise constructor.
-    RootedValue nextPromise(cx);
-    RootedValue staticResolve(cx);
     if (!GetProperty(cx, C, CVal, cx->names().resolve, &staticResolve))
       return false;
 
     FixedInvokeArgs<1> resolveArgs(cx);
     resolveArgs[0].set(nextValue);
     if (!Call(cx, staticResolve, CVal, resolveArgs, &nextPromise)) return false;
 
     // Step j.
-    RootedFunction resolveFunc(
-        cx, NewNativeFunction(cx, PromiseAllResolveElementFunction, 1, nullptr,
-                              gc::AllocKind::FUNCTION_EXTENDED, GenericObject));
+    JSFunction* resolveFunc =
+        NewNativeFunction(cx, PromiseAllResolveElementFunction, 1, nullptr,
+                          gc::AllocKind::FUNCTION_EXTENDED, GenericObject);
     if (!resolveFunc) return false;
 
     // Steps k,m,n.
     resolveFunc->setExtendedSlot(PromiseAllResolveElementFunctionSlot_Data,
-                                 dataHolderVal);
+                                 ObjectValue(*dataHolder));
 
     // Step l.
     resolveFunc->setExtendedSlot(
         PromiseAllResolveElementFunctionSlot_ElementIndex, Int32Value(index));
 
     // Steps o-p.
     dataHolder->increaseRemainingCount();
 
     // Step q.
-    RootedValue resolveFunVal(cx, ObjectValue(*resolveFunc));
+    resolveFunVal.setObject(*resolveFunc);
     if (!BlockOnPromise(cx, nextPromise, promiseObj, resolveFunVal,
                         rejectFunVal, true))
       return false;
 
     // Step r.
     index++;
     MOZ_ASSERT(index > 0);
   }
 }
 
 // ES2016, 25.4.4.1.2.
 static bool PromiseAllResolveElementFunction(JSContext* cx, unsigned argc,
                                              Value* vp) {
   CallArgs args = CallArgsFromVp(argc, vp);
 
-  RootedFunction resolve(cx, &args.callee().as<JSFunction>());
+  JSFunction* resolve = &args.callee().as<JSFunction>();
   RootedValue xVal(cx, args.get(0));
 
   // Step 1.
-  RootedValue dataVal(
-      cx, resolve->getExtendedSlot(PromiseAllResolveElementFunctionSlot_Data));
+  const Value& dataVal =
+      resolve->getExtendedSlot(PromiseAllResolveElementFunctionSlot_Data);
 
   // Step 2.
   // We use the existence of the data holder as a signal for whether the
   // Promise was already resolved. Upon resolution, it's reset to
   // `undefined`.
   if (dataVal.isUndefined()) {
     args.rval().setUndefined();
     return true;
@@ -2558,17 +2551,17 @@ static MOZ_MUST_USE JSObject* CommonStat
     } else if (IsWrapper(xObj)) {
       // Treat instances of Promise from other compartments as Promises
       // here, too.
       // It's important to do the GetProperty for the `constructor`
       // below through the wrapper, because wrappers can change the
       // outcome, so instead of unwrapping and then performing the
       // GetProperty, just check here and then operate on the original
       // object again.
-      RootedObject unwrappedObject(cx, CheckedUnwrap(xObj));
+      JSObject* unwrappedObject = CheckedUnwrap(xObj);
       if (unwrappedObject && unwrappedObject->is<PromiseObject>())
         isPromise = true;
     }
     if (isPromise) {
       RootedValue ctorVal(cx);
       if (!GetProperty(cx, xObj, xObj, cx->names().constructor, &ctorVal))
         return nullptr;
       if (ctorVal == thisVal) return xObj;
@@ -2613,17 +2606,17 @@ static bool Promise_reject(JSContext* cx
   return true;
 }
 
 /**
  * Unforgeable version of ES2016, 25.4.4.4, Promise.reject.
  */
 /* static */ JSObject* PromiseObject::unforgeableReject(JSContext* cx,
                                                         HandleValue value) {
-  RootedObject promiseCtor(cx, JS::GetPromiseConstructor(cx));
+  JSObject* promiseCtor = JS::GetPromiseConstructor(cx);
   if (!promiseCtor) return nullptr;
   RootedValue cVal(cx, ObjectValue(*promiseCtor));
   return CommonStaticResolveRejectImpl(cx, cVal, value, RejectMode);
 }
 
 /**
  * ES2016, 25.4.4.5, Promise.resolve.
  */
@@ -2638,17 +2631,17 @@ static bool Promise_static_resolve(JSCon
   return true;
 }
 
 /**
  * Unforgeable version of ES2016, 25.4.4.5, Promise.resolve.
  */
 /* static */ JSObject* PromiseObject::unforgeableResolve(JSContext* cx,
                                                          HandleValue value) {
-  RootedObject promiseCtor(cx, JS::GetPromiseConstructor(cx));
+  JSObject* promiseCtor = JS::GetPromiseConstructor(cx);
   if (!promiseCtor) return nullptr;
   RootedValue cVal(cx, ObjectValue(*promiseCtor));
   return CommonStaticResolveRejectImpl(cx, cVal, value, ResolveMode);
 }
 
 /**
  * ES2016, 25.4.4.6 get Promise [ @@species ]
  */
@@ -2697,18 +2690,18 @@ static PromiseReactionRecord* NewReactio
   MOZ_ASSERT(onFulfilled.isNull() == onRejected.isNull());
 
   RootedObject incumbentGlobalObject(cx);
   if (incumbentGlobalObjectOption == IncumbentGlobalObject::Yes) {
     if (!GetObjectFromIncumbentGlobal(cx, &incumbentGlobalObject))
       return nullptr;
   }
 
-  Rooted<PromiseReactionRecord*> reaction(
-      cx, NewObjectWithClassProto<PromiseReactionRecord>(cx));
+  PromiseReactionRecord* reaction =
+      NewObjectWithClassProto<PromiseReactionRecord>(cx);
   if (!reaction) return nullptr;
 
   assertSameCompartment(cx, resultPromise);
   assertSameCompartment(cx, onFulfilled);
   assertSameCompartment(cx, onRejected);
   assertSameCompartment(cx, resolve);
   assertSameCompartment(cx, reject);
   assertSameCompartment(cx, incumbentGlobalObject);
@@ -2964,17 +2957,17 @@ bool js::AsyncFromSyncIteratorMethod(JSC
   } else if (completionKind == CompletionKind::Return) {
     // 11.1.3.2.2 steps 5-6.
     if (!GetProperty(cx, iter, iter, cx->names().return_, &func))
       return AbruptRejectPromise(cx, args, resultPromise, nullptr);
 
     // Step 7.
     if (func.isNullOrUndefined()) {
       // Step 7.a.
-      RootedObject resultObj(cx, CreateIterResultObject(cx, args.get(0), true));
+      JSObject* resultObj = CreateIterResultObject(cx, args.get(0), true);
       if (!resultObj)
         return AbruptRejectPromise(cx, args, resultPromise, nullptr);
 
       RootedValue resultVal(cx, ObjectValue(*resultObj));
 
       // Step 7.b.
       if (!ResolvePromiseInternal(cx, resultPromise, resultVal))
         return AbruptRejectPromise(cx, args, resultPromise, nullptr);
@@ -3104,18 +3097,18 @@ static MOZ_MUST_USE bool AsyncGeneratorR
         HandleValue exception = valueOrException;
 
         // Step 1 (implicit).
 
         // Steps 2-3.
         MOZ_ASSERT(!asyncGenObj->isQueueEmpty());
 
         // Step 4.
-        Rooted<AsyncGeneratorRequest*> request(
-            cx, AsyncGeneratorObject::dequeueRequest(cx, asyncGenObj));
+        AsyncGeneratorRequest* request =
+            AsyncGeneratorObject::dequeueRequest(cx, asyncGenObj);
         if (!request) return false;
 
         // Step 5.
         Rooted<PromiseObject*> resultPromise(cx, request->promise());
 
         asyncGenObj->cacheRequest(request);
 
         // Step 6.
@@ -3130,27 +3123,27 @@ static MOZ_MUST_USE bool AsyncGeneratorR
         HandleValue value = valueOrException;
 
         // Step 1 (implicit).
 
         // Steps 2-3.
         MOZ_ASSERT(!asyncGenObj->isQueueEmpty());
 
         // Step 4.
-        Rooted<AsyncGeneratorRequest*> request(
-            cx, AsyncGeneratorObject::dequeueRequest(cx, asyncGenObj));
+        AsyncGeneratorRequest* request =
+            AsyncGeneratorObject::dequeueRequest(cx, asyncGenObj);
         if (!request) return false;
 
         // Step 5.
         Rooted<PromiseObject*> resultPromise(cx, request->promise());
 
         asyncGenObj->cacheRequest(request);
 
         // Step 6.
-        RootedObject resultObj(cx, CreateIterResultObject(cx, value, done));
+        JSObject* resultObj = CreateIterResultObject(cx, value, done);
         if (!resultObj) return false;
 
         RootedValue resultValue(cx, ObjectValue(*resultObj));
 
         // Step 7.
         if (!ResolvePromiseInternal(cx, resultPromise, resultValue))
           return false;
 
@@ -3389,17 +3382,17 @@ static bool Promise_then_impl(JSContext*
     return false;
   }
   RootedObject promiseObj(cx, &promiseVal.toObject());
   Rooted<PromiseObject*> promise(cx);
 
   if (promiseObj->is<PromiseObject>()) {
     promise = &promiseObj->as<PromiseObject>();
   } else {
-    RootedObject unwrappedPromiseObj(cx, CheckedUnwrap(promiseObj));
+    JSObject* unwrappedPromiseObj = CheckedUnwrap(promiseObj);
     if (!unwrappedPromiseObj) {
       ReportAccessDenied(cx);
       return false;
     }
     if (!unwrappedPromiseObj->is<PromiseObject>()) {
       JS_ReportErrorNumberASCII(cx, GetErrorMessage, nullptr,
                                 JSMSG_INCOMPATIBLE_PROTO, "Promise", "then",
                                 "value");
@@ -3696,18 +3689,17 @@ static MOZ_MUST_USE bool AddPromiseReact
       return false;
     }
     MOZ_RELEASE_ASSERT(reactionsObj->is<PromiseReactionRecord>());
   }
 
   if (reactionsObj->is<PromiseReactionRecord>()) {
     // If a single reaction existed so far, create a list and store the
     // old and the new reaction in it.
-    RootedNativeObject reactions(cx);
-    reactions = NewDenseFullyAllocatedArray(cx, 2);
+    ArrayObject* reactions = NewDenseFullyAllocatedArray(cx, 2);
     if (!reactions) return false;
 
     reactions->setDenseInitializedLength(2);
     reactions->initDenseElement(0, reactionsVal);
     reactions->initDenseElement(1, reactionVal);
 
     promise->setFixedSlot(PromiseSlot_ReactionsOrResult,
                           ObjectValue(*reactions));
@@ -3783,20 +3775,20 @@ bool PromiseObject::dependentPromises(JS
     values[0].setObject(*promiseObj);
     return true;
   }
 
   uint32_t len = reactions->getDenseInitializedLength();
   MOZ_ASSERT(len >= 2);
 
   uint32_t valuesIndex = 0;
-  Rooted<PromiseReactionRecord*> reaction(cx);
   for (uint32_t i = 0; i < len; i++) {
-    reaction =
-        &reactions->getDenseElement(i).toObject().as<PromiseReactionRecord>();
+    const Value& element = reactions->getDenseElement(i);
+    PromiseReactionRecord* reaction =
+        &element.toObject().as<PromiseReactionRecord>();
 
     // Not all reactions have a Promise on them.
     RootedObject promiseObj(cx, reaction->promise());
     if (!promiseObj) continue;
     if (!values.growBy(1)) return false;
 
     values[valuesIndex++].setObject(*promiseObj);
   }
@@ -3808,17 +3800,17 @@ bool PromiseObject::dependentPromises(JS
                                          Handle<PromiseObject*> promise,
                                          HandleValue resolutionValue) {
   MOZ_ASSERT(!PromiseHasAnyFlag(*promise, PROMISE_FLAG_ASYNC));
   if (promise->state() != JS::PromiseState::Pending) return true;
 
   if (PromiseHasAnyFlag(*promise, PROMISE_FLAG_DEFAULT_RESOLVING_FUNCTIONS))
     return ResolvePromiseInternal(cx, promise, resolutionValue);
 
-  RootedObject resolveFun(cx, GetResolveFunctionFromPromise(promise));
+  JSFunction* resolveFun = GetResolveFunctionFromPromise(promise);
   if (!resolveFun)
     return true;
 
   RootedValue funVal(cx, ObjectValue(*resolveFun));
 
   // For xray'd Promises, the resolve fun may have been created in another
   // compartment. For the call below to work in that case, wrap the
   // function into the current compartment.
