# HG changeset patch
# User Andre Bargull <andre.bargull@gmail.com>
# Date 1527587721 25200
# Node ID 9529d02de6347c89c9544e6b1152d22743dcc4a9
# Parent  cd746df2864b5b99017fbcde2f86bd8293bf9422
Bug 1450085 - Part 2: Replace ReportValueErrorFlags with ReportValueError where applicable. r=tcampbell

diff --git a/js/src/builtin/ReflectParse.cpp b/js/src/builtin/ReflectParse.cpp
--- a/js/src/builtin/ReflectParse.cpp
+++ b/js/src/builtin/ReflectParse.cpp
@@ -300,19 +300,18 @@ class NodeBuilder {
       if (!GetPropertyDefault(cx, userobj, id, nullVal, &funv)) return false;
 
       if (funv.isNullOrUndefined()) {
         callbacks[i].setNull();
         continue;
       }
 
       if (!funv.isObject() || !funv.toObject().is<JSFunction>()) {
-        ReportValueErrorFlags(cx, JSREPORT_ERROR, JSMSG_NOT_FUNCTION,
-                              JSDVG_SEARCH_STACK, funv, nullptr, nullptr,
-                              nullptr);
+        ReportValueError(cx, JSMSG_NOT_FUNCTION, JSDVG_SEARCH_STACK, funv,
+                         nullptr);
         return false;
       }
 
       callbacks[i].set(funv);
     }
 
     return true;
   }
@@ -2958,19 +2957,18 @@ static bool reflect_parse(JSContext* cx,
   bool loc = true;
   RootedObject builder(cx);
   ParseTarget target = ParseTarget::Script;
 
   RootedValue arg(cx, args.get(1));
 
   if (!arg.isNullOrUndefined()) {
     if (!arg.isObject()) {
-      ReportValueErrorFlags(cx, JSREPORT_ERROR, JSMSG_UNEXPECTED_TYPE,
-                            JSDVG_SEARCH_STACK, arg, nullptr, "not an object",
-                            nullptr);
+      ReportValueError(cx, JSMSG_UNEXPECTED_TYPE, JSDVG_SEARCH_STACK, arg,
+                       nullptr, "not an object");
       return false;
     }
 
     RootedObject config(cx, &arg.toObject());
 
     RootedValue prop(cx);
 
     /* config.loc */
@@ -3007,34 +3005,32 @@ static bool reflect_parse(JSContext* cx,
     /* config.builder */
     RootedId builderId(cx, NameToId(cx->names().builder));
     RootedValue nullVal(cx, NullValue());
     if (!GetPropertyDefault(cx, config, builderId, nullVal, &prop))
       return false;
 
     if (!prop.isNullOrUndefined()) {
       if (!prop.isObject()) {
-        ReportValueErrorFlags(cx, JSREPORT_ERROR, JSMSG_UNEXPECTED_TYPE,
-                              JSDVG_SEARCH_STACK, prop, nullptr,
-                              "not an object", nullptr);
+        ReportValueError(cx, JSMSG_UNEXPECTED_TYPE, JSDVG_SEARCH_STACK, prop,
+                         nullptr, "not an object");
         return false;
       }
       builder = &prop.toObject();
     }
 
     /* config.target */
     RootedId targetId(cx, NameToId(cx->names().target));
     RootedValue scriptVal(cx, StringValue(cx->names().script));
     if (!GetPropertyDefault(cx, config, targetId, scriptVal, &prop))
       return false;
 
     if (!prop.isString()) {
-      ReportValueErrorFlags(cx, JSREPORT_ERROR, JSMSG_UNEXPECTED_TYPE,
-                            JSDVG_SEARCH_STACK, prop, nullptr,
-                            "not 'script' or 'module'", nullptr);
+      ReportValueError(cx, JSMSG_UNEXPECTED_TYPE, JSDVG_SEARCH_STACK, prop,
+                       nullptr, "not 'script' or 'module'");
       return false;
     }
 
     RootedString stringProp(cx, prop.toString());
     bool isScript = false;
     bool isModule = false;
     if (!EqualStrings(cx, stringProp, cx->names().script, &isScript))
       return false;
diff --git a/js/src/builtin/Symbol.cpp b/js/src/builtin/Symbol.cpp
--- a/js/src/builtin/Symbol.cpp
+++ b/js/src/builtin/Symbol.cpp
@@ -123,19 +123,18 @@ bool SymbolObject::for_(JSContext* cx, u
 
 // ES6 rev 25 (2014 May 22) 19.4.2.7
 bool SymbolObject::keyFor(JSContext* cx, unsigned argc, Value* vp) {
   CallArgs args = CallArgsFromVp(argc, vp);
 
   // step 1
   HandleValue arg = args.get(0);
   if (!arg.isSymbol()) {
-    ReportValueErrorFlags(cx, JSREPORT_ERROR, JSMSG_UNEXPECTED_TYPE,
-                          JSDVG_SEARCH_STACK, arg, nullptr, "not a symbol",
-                          nullptr);
+    ReportValueError(cx, JSMSG_UNEXPECTED_TYPE, JSDVG_SEARCH_STACK, arg,
+                     nullptr, "not a symbol");
     return false;
   }
 
   // step 2
   if (arg.toSymbol()->code() == JS::SymbolCode::InSymbolRegistry) {
 #ifdef DEBUG
     RootedString desc(cx, arg.toSymbol()->description());
     MOZ_ASSERT(Symbol::for_(cx, desc) == arg.toSymbol());
diff --git a/js/src/builtin/TestingFunctions.cpp b/js/src/builtin/TestingFunctions.cpp
--- a/js/src/builtin/TestingFunctions.cpp
+++ b/js/src/builtin/TestingFunctions.cpp
@@ -1174,31 +1174,29 @@ static bool ClearSavedFrames(JSContext* 
 static bool SaveStack(JSContext* cx, unsigned argc, Value* vp) {
   CallArgs args = CallArgsFromVp(argc, vp);
 
   JS::StackCapture capture((JS::AllFrames()));
   if (args.length() >= 1) {
     double maxDouble;
     if (!ToNumber(cx, args[0], &maxDouble)) return false;
     if (mozilla::IsNaN(maxDouble) || maxDouble < 0 || maxDouble > UINT32_MAX) {
-      ReportValueErrorFlags(cx, JSREPORT_ERROR, JSMSG_UNEXPECTED_TYPE,
-                            JSDVG_SEARCH_STACK, args[0], nullptr,
-                            "not a valid maximum frame count", NULL);
+      ReportValueError(cx, JSMSG_UNEXPECTED_TYPE, JSDVG_SEARCH_STACK, args[0],
+                       nullptr, "not a valid maximum frame count");
       return false;
     }
     uint32_t max = uint32_t(maxDouble);
     if (max > 0) capture = JS::StackCapture(JS::MaxFrames(max));
   }
 
   RootedObject compartmentObject(cx);
   if (args.length() >= 2) {
     if (!args[1].isObject()) {
-      ReportValueErrorFlags(cx, JSREPORT_ERROR, JSMSG_UNEXPECTED_TYPE,
-                            JSDVG_SEARCH_STACK, args[0], nullptr,
-                            "not an object", NULL);
+      ReportValueError(cx, JSMSG_UNEXPECTED_TYPE, JSDVG_SEARCH_STACK, args[0],
+                       nullptr, "not an object");
       return false;
     }
     compartmentObject = UncheckedUnwrap(&args[1].toObject());
     if (!compartmentObject) return false;
   }
 
   RootedObject stack(cx);
   {
@@ -3290,26 +3288,24 @@ static bool FindPath(JSContext* cx, unsi
                               "findPath", "1", "");
     return false;
   }
 
   // We don't ToString non-objects given as 'start' or 'target', because this
   // test is all about object identity, and ToString doesn't preserve that.
   // Non-GCThing endpoints don't make much sense.
   if (!args[0].isObject() && !args[0].isString() && !args[0].isSymbol()) {
-    ReportValueErrorFlags(cx, JSREPORT_ERROR, JSMSG_UNEXPECTED_TYPE,
-                          JSDVG_SEARCH_STACK, args[0], nullptr,
-                          "not an object, string, or symbol", NULL);
+    ReportValueError(cx, JSMSG_UNEXPECTED_TYPE, JSDVG_SEARCH_STACK, args[0],
+                     nullptr, "not an object, string, or symbol");
     return false;
   }
 
   if (!args[1].isObject() && !args[1].isString() && !args[1].isSymbol()) {
-    ReportValueErrorFlags(cx, JSREPORT_ERROR, JSMSG_UNEXPECTED_TYPE,
-                          JSDVG_SEARCH_STACK, args[0], nullptr,
-                          "not an object, string, or symbol", NULL);
+    ReportValueError(cx, JSMSG_UNEXPECTED_TYPE, JSDVG_SEARCH_STACK, args[0],
+                     nullptr, "not an object, string, or symbol");
     return false;
   }
 
   Rooted<GCVector<Value>> nodes(cx, GCVector<Value>(cx));
   Vector<heaptools::EdgeName> edges(cx);
 
   {
     // We can't tolerate the GC moving things around while we're searching
@@ -3388,53 +3384,50 @@ static bool FindPath(JSContext* cx, unsi
 static bool ShortestPaths(JSContext* cx, unsigned argc, Value* vp) {
   CallArgs args = CallArgsFromVp(argc, vp);
   if (!args.requireAtLeast(cx, "shortestPaths", 3)) return false;
 
   // We don't ToString non-objects given as 'start' or 'target', because this
   // test is all about object identity, and ToString doesn't preserve that.
   // Non-GCThing endpoints don't make much sense.
   if (!args[0].isObject() && !args[0].isString() && !args[0].isSymbol()) {
-    ReportValueErrorFlags(cx, JSREPORT_ERROR, JSMSG_UNEXPECTED_TYPE,
-                          JSDVG_SEARCH_STACK, args[0], nullptr,
-                          "not an object, string, or symbol", nullptr);
+    ReportValueError(cx, JSMSG_UNEXPECTED_TYPE, JSDVG_SEARCH_STACK, args[0],
+                     nullptr, "not an object, string, or symbol");
     return false;
   }
 
   if (!args[1].isObject() || !args[1].toObject().is<ArrayObject>()) {
-    ReportValueErrorFlags(cx, JSREPORT_ERROR, JSMSG_UNEXPECTED_TYPE,
-                          JSDVG_SEARCH_STACK, args[1], nullptr,
-                          "not an array object", nullptr);
+    ReportValueError(cx, JSMSG_UNEXPECTED_TYPE, JSDVG_SEARCH_STACK, args[1],
+                     nullptr, "not an array object");
     return false;
   }
 
   RootedArrayObject objs(cx, &args[1].toObject().as<ArrayObject>());
   size_t length = objs->getDenseInitializedLength();
   if (length == 0) {
-    ReportValueErrorFlags(
-        cx, JSREPORT_ERROR, JSMSG_UNEXPECTED_TYPE, JSDVG_SEARCH_STACK, args[1],
-        nullptr, "not a dense array object with one or more elements", nullptr);
+    ReportValueError(cx, JSMSG_UNEXPECTED_TYPE, JSDVG_SEARCH_STACK, args[1],
+                     nullptr,
+                     "not a dense array object with one or more elements");
     return false;
   }
 
   for (size_t i = 0; i < length; i++) {
     RootedValue el(cx, objs->getDenseElement(i));
     if (!el.isObject() && !el.isString() && !el.isSymbol()) {
       JS_ReportErrorASCII(cx,
                           "Each target must be an object, string, or symbol");
       return false;
     }
   }
 
   int32_t maxNumPaths;
   if (!JS::ToInt32(cx, args[2], &maxNumPaths)) return false;
   if (maxNumPaths <= 0) {
-    ReportValueErrorFlags(cx, JSREPORT_ERROR, JSMSG_UNEXPECTED_TYPE,
-                          JSDVG_SEARCH_STACK, args[2], nullptr,
-                          "not greater than 0", nullptr);
+    ReportValueError(cx, JSMSG_UNEXPECTED_TYPE, JSDVG_SEARCH_STACK, args[2],
+                     nullptr, "not greater than 0");
     return false;
   }
 
   // We accumulate the results into a GC-stable form, due to the fact that the
   // JS::ubi::ShortestPaths lifetime (when operating on the live heap graph)
   // is bounded within an AutoCheckCannotGC.
   Rooted<GCVector<GCVector<GCVector<Value>>>> values(
       cx, GCVector<GCVector<GCVector<Value>>>(cx));
diff --git a/js/src/vm/Debugger.cpp b/js/src/vm/Debugger.cpp
--- a/js/src/vm/Debugger.cpp
+++ b/js/src/vm/Debugger.cpp
@@ -228,19 +228,18 @@ static inline JSScript* GetOrCreateFunct
   return fun->nonLazyScript();
 }
 
 static bool ValueToIdentifier(JSContext* cx, HandleValue v,
                               MutableHandleId id) {
   if (!ValueToId<CanGC>(cx, v, id)) return false;
   if (!JSID_IS_ATOM(id) || !IsIdentifier(JSID_TO_ATOM(id))) {
     RootedValue val(cx, v);
-    ReportValueErrorFlags(cx, JSREPORT_ERROR, JSMSG_UNEXPECTED_TYPE,
-                          JSDVG_SEARCH_STACK, val, nullptr, "not an identifier",
-                          nullptr);
+    ReportValueError(cx, JSMSG_UNEXPECTED_TYPE, JSDVG_SEARCH_STACK, val,
+                     nullptr, "not an identifier");
     return false;
   }
   return true;
 }
 
 class js::AutoRestoreRealmDebugMode {
   Realm* realm_;
   unsigned bits_;
@@ -450,23 +449,21 @@ static bool RequireGlobalObject(JSContex
 
     /* ... and WindowProxies around Windows. */
     if (IsWindowProxy(obj)) {
       obj = ToWindowIfWindowProxy(obj);
       isWindowProxy = "a WindowProxy referring to ";
     }
 
     if (obj->is<GlobalObject>()) {
-      ReportValueErrorFlags(cx, JSREPORT_ERROR, JSMSG_DEBUG_WRAPPER_IN_WAY,
-                            JSDVG_SEARCH_STACK, dbgobj, nullptr, isWrapper,
-                            isWindowProxy);
+      ReportValueError(cx, JSMSG_DEBUG_WRAPPER_IN_WAY, JSDVG_SEARCH_STACK,
+                       dbgobj, nullptr, isWrapper, isWindowProxy);
     } else {
-      ReportValueErrorFlags(cx, JSREPORT_ERROR, JSMSG_DEBUG_BAD_REFERENT,
-                            JSDVG_SEARCH_STACK, dbgobj, nullptr,
-                            "a global object", nullptr);
+      ReportValueError(cx, JSMSG_DEBUG_BAD_REFERENT, JSDVG_SEARCH_STACK, dbgobj,
+                       nullptr, "a global object");
     }
     return false;
   }
 
   return true;
 }
 
 /*** Breakpoints ************************************************************/
@@ -4952,19 +4949,18 @@ static JSObject* DebuggerScript_check(JS
 template <typename ReferentT>
 static JSObject* DebuggerScript_checkThis(JSContext* cx, const CallArgs& args,
                                           const char* fnname,
                                           const char* refname) {
   JSObject* thisobj = DebuggerScript_check(cx, args.thisv(), fnname);
   if (!thisobj) return nullptr;
 
   if (!GetScriptReferent(thisobj).is<ReferentT>()) {
-    ReportValueErrorFlags(cx, JSREPORT_ERROR, JSMSG_DEBUG_BAD_REFERENT,
-                          JSDVG_SEARCH_STACK, args.thisv(), nullptr, refname,
-                          nullptr);
+    ReportValueError(cx, JSMSG_DEBUG_BAD_REFERENT, JSDVG_SEARCH_STACK,
+                     args.thisv(), nullptr, refname);
     return nullptr;
   }
 
   return thisobj;
 }
 
 #define THIS_DEBUGSCRIPT_REFERENT(cx, argc, vp, fnname, args, obj, referent) \
   CallArgs args = CallArgsFromVp(argc, vp);                                  \
@@ -6491,19 +6487,18 @@ template <typename ReferentT>
 static NativeObject* DebuggerSource_checkThis(JSContext* cx,
                                               const CallArgs& args,
                                               const char* fnname,
                                               const char* refname) {
   NativeObject* thisobj = DebuggerSource_check(cx, args.thisv(), fnname);
   if (!thisobj) return nullptr;
 
   if (!GetSourceReferent(thisobj).is<ReferentT>()) {
-    ReportValueErrorFlags(cx, JSREPORT_ERROR, JSMSG_DEBUG_BAD_REFERENT,
-                          JSDVG_SEARCH_STACK, args.thisv(), nullptr, refname,
-                          nullptr);
+    ReportValueError(cx, JSMSG_DEBUG_BAD_REFERENT, JSDVG_SEARCH_STACK,
+                     args.thisv(), nullptr, refname);
     return nullptr;
   }
 
   return thisobj;
 }
 
 #define THIS_DEBUGSOURCE_REFERENT(cx, argc, vp, fnname, args, obj, referent)  \
   CallArgs args = CallArgsFromVp(argc, vp);                                   \
@@ -6565,19 +6560,18 @@ static bool DebuggerSource_getText(JSCon
   obj->setReservedSlot(JSSLOT_DEBUGSOURCE_TEXT, args.rval());
   return true;
 }
 
 static bool DebuggerSource_getBinary(JSContext* cx, unsigned argc, Value* vp) {
   THIS_DEBUGSOURCE_REFERENT(cx, argc, vp, "(get binary)", args, obj, referent);
 
   if (!referent.is<WasmInstanceObject*>()) {
-    ReportValueErrorFlags(cx, JSREPORT_ERROR, JSMSG_DEBUG_BAD_REFERENT,
-                          JSDVG_SEARCH_STACK, args.thisv(), nullptr,
-                          "a wasm source", nullptr);
+    ReportValueError(cx, JSMSG_DEBUG_BAD_REFERENT, JSDVG_SEARCH_STACK,
+                     args.thisv(), nullptr, "a wasm source");
     return false;
   }
 
   RootedWasmInstanceObject wasmInstance(cx, referent.as<WasmInstanceObject*>());
   if (!wasmInstance->instance().debug().binarySource()) {
     JS_ReportErrorNumberASCII(cx, GetErrorMessage, nullptr,
                               JSMSG_DEBUG_NO_BINARY_SOURCE);
     return false;
@@ -7479,19 +7473,18 @@ FrameIter::Data* DebuggerFrame::frameIte
   return true;
 }
 
 /* static */ bool DebuggerFrame::requireScriptReferent(
     JSContext* cx, HandleDebuggerFrame frame) {
   AbstractFramePtr referent = DebuggerFrame::getReferent(frame);
   if (!referent.hasScript()) {
     RootedValue frameobj(cx, ObjectValue(*frame));
-    ReportValueErrorFlags(cx, JSREPORT_ERROR, JSMSG_DEBUG_BAD_REFERENT,
-                          JSDVG_SEARCH_STACK, frameobj, nullptr,
-                          "a script frame", nullptr);
+    ReportValueError(cx, JSMSG_DEBUG_BAD_REFERENT, JSDVG_SEARCH_STACK, frameobj,
+                     nullptr, "a script frame");
     return false;
   }
   return true;
 }
 
 void DebuggerFrame::freeFrameIterData(FreeOp* fop) {
   if (FrameIter::Data* data = frameIterData()) {
     fop->delete_(data);
@@ -9874,23 +9867,21 @@ double DebuggerObject::promiseTimeToReso
     /* ... and WindowProxies around Windows. */
     if (IsWindowProxy(referent)) {
       referent = ToWindowIfWindowProxy(referent);
       isWindowProxy = "a WindowProxy referring to ";
     }
 
     RootedValue dbgobj(cx, ObjectValue(*object));
     if (referent->is<GlobalObject>()) {
-      ReportValueErrorFlags(cx, JSREPORT_ERROR, JSMSG_DEBUG_WRAPPER_IN_WAY,
-                            JSDVG_SEARCH_STACK, dbgobj, nullptr, isWrapper,
-                            isWindowProxy);
+      ReportValueError(cx, JSMSG_DEBUG_WRAPPER_IN_WAY, JSDVG_SEARCH_STACK,
+                       dbgobj, nullptr, isWrapper, isWindowProxy);
     } else {
-      ReportValueErrorFlags(cx, JSREPORT_ERROR, JSMSG_DEBUG_BAD_REFERENT,
-                            JSDVG_SEARCH_STACK, dbgobj, nullptr,
-                            "a global object", nullptr);
+      ReportValueError(cx, JSMSG_DEBUG_BAD_REFERENT, JSDVG_SEARCH_STACK, dbgobj,
+                       nullptr, "a global object");
     }
     return false;
   }
 
   return true;
 }
 
 /* static */ bool DebuggerObject::requirePromise(JSContext* cx,
diff --git a/js/src/vm/JSObject.cpp b/js/src/vm/JSObject.cpp
--- a/js/src/vm/JSObject.cpp
+++ b/js/src/vm/JSObject.cpp
@@ -2403,39 +2403,16 @@ bool js::HasOwnDataPropertyPure(JSContex
   PropertyResult prop;
   if (!LookupOwnPropertyPure(cx, obj, id, &prop)) return false;
 
   *result = prop && !prop.isDenseOrTypedArrayElement() &&
             prop.shape()->isDataProperty();
   return true;
 }
 
-/* static */ bool JSObject::reportReadOnly(JSContext* cx, jsid id,
-                                           unsigned report) {
-  RootedValue val(cx, IdToValue(id));
-  return ReportValueErrorFlags(cx, report, JSMSG_READ_ONLY, JSDVG_IGNORE_STACK,
-                               val, nullptr, nullptr, nullptr);
-}
-
-/* static */ bool JSObject::reportNotConfigurable(JSContext* cx, jsid id,
-                                                  unsigned report) {
-  RootedValue val(cx, IdToValue(id));
-  return ReportValueErrorFlags(cx, report, JSMSG_CANT_DELETE,
-                               JSDVG_IGNORE_STACK, val, nullptr, nullptr,
-                               nullptr);
-}
-
-/* static */ bool JSObject::reportNotExtensible(JSContext* cx, HandleObject obj,
-                                                unsigned report) {
-  RootedValue val(cx, ObjectValue(*obj));
-  return ReportValueErrorFlags(cx, report, JSMSG_OBJECT_NOT_EXTENSIBLE,
-                               JSDVG_IGNORE_STACK, val, nullptr, nullptr,
-                               nullptr);
-}
-
 bool js::GetPrototypeIfOrdinary(JSContext* cx, HandleObject obj,
                                 bool* isOrdinary, MutableHandleObject protop) {
   if (obj->is<js::ProxyObject>())
     return js::Proxy::getPrototypeIfOrdinary(cx, obj, isOrdinary, protop);
 
   *isOrdinary = true;
   protop.set(obj->staticPrototype());
   return true;
diff --git a/js/src/vm/JSObject.h b/js/src/vm/JSObject.h
--- a/js/src/vm/JSObject.h
+++ b/js/src/vm/JSObject.h
@@ -473,23 +473,16 @@ class JSObject : public js::gc::Cell {
   MOZ_ALWAYS_INLINE bool isCallable() const;
   MOZ_ALWAYS_INLINE bool isConstructor() const;
   MOZ_ALWAYS_INLINE JSNative callHook() const;
   MOZ_ALWAYS_INLINE JSNative constructHook() const;
 
   MOZ_ALWAYS_INLINE void finalize(js::FreeOp* fop);
 
  public:
-  static bool reportReadOnly(JSContext* cx, jsid id,
-                             unsigned report = JSREPORT_ERROR);
-  static bool reportNotConfigurable(JSContext* cx, jsid id,
-                                    unsigned report = JSREPORT_ERROR);
-  static bool reportNotExtensible(JSContext* cx, js::HandleObject obj,
-                                  unsigned report = JSREPORT_ERROR);
-
   static bool nonNativeSetProperty(JSContext* cx, js::HandleObject obj,
                                    js::HandleId id, js::HandleValue v,
                                    js::HandleValue receiver,
                                    JS::ObjectOpResult& result);
   static bool nonNativeSetElement(JSContext* cx, js::HandleObject obj,
                                   uint32_t index, js::HandleValue v,
                                   js::HandleValue receiver,
                                   JS::ObjectOpResult& result);
diff --git a/js/src/vm/SelfHosting.cpp b/js/src/vm/SelfHosting.cpp
--- a/js/src/vm/SelfHosting.cpp
+++ b/js/src/vm/SelfHosting.cpp
@@ -2749,19 +2749,19 @@ static bool VerifyGlobalNames(JSContext*
           break;
         }
       }
     }
   }
 
   if (nameMissing) {
     RootedValue value(cx, IdToValue(id));
-    return ReportValueErrorFlags(
-        cx, JSREPORT_ERROR, JSMSG_NO_SUCH_SELF_HOSTED_PROP, JSDVG_IGNORE_STACK,
-        value, nullptr, nullptr, nullptr);
+    ReportValueError(cx, JSMSG_NO_SUCH_SELF_HOSTED_PROP, JSDVG_IGNORE_STACK,
+                     value, nullptr);
+    return false;
   }
 #endif  // DEBUG
 
   return true;
 }
 
 bool JSRuntime::initSelfHosting(JSContext* cx) {
   MOZ_ASSERT(!selfHostingGlobal_);
