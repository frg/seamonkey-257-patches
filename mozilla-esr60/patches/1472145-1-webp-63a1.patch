# HG changeset patch
# User Andrew Osmond <aosmond@mozilla.com>
# Date 1530318605 14400
# Node ID 2a57944df012f4654b8f929132d7e3c5ab4193d0
# Parent  3a5494b4659cfe7303a8a906dd6d2c14e31bdc95
Bug 1472145 - Part 1. Add support for identifying the WebP images MIME type. r=tnikkel

diff --git a/image/imgLoader.cpp b/image/imgLoader.cpp
--- a/image/imgLoader.cpp
+++ b/image/imgLoader.cpp
@@ -2487,16 +2487,21 @@ nsresult imgLoader::GetMimeTypeFromConte
     aContentType.AssignLiteral(IMAGE_BMP);
 
     // ICOs always begin with a 2-byte 0 followed by a 2-byte 1.
     // CURs begin with 2-byte 0 followed by 2-byte 2.
   } else if (aLength >= 4 && (!memcmp(aContents, "\000\000\001\000", 4) ||
                               !memcmp(aContents, "\000\000\002\000", 4))) {
     aContentType.AssignLiteral(IMAGE_ICO);
 
+  // WebPs always begin with RIFF, a 32-bit length, and WEBP.
+  } else if (aLength >= 12 && !memcmp(aContents, "RIFF", 4) &&
+                              !memcmp(aContents + 8, "WEBP", 4)) {
+    aContentType.AssignLiteral(IMAGE_WEBP);
+
   } else {
     /* none of the above?  I give up */
     return NS_ERROR_NOT_AVAILABLE;
   }
 
   return NS_OK;
 }
 
diff --git a/image/test/gtest/TestLoader.cpp b/image/test/gtest/TestLoader.cpp
new file mode 100644
--- /dev/null
+++ b/image/test/gtest/TestLoader.cpp
@@ -0,0 +1,84 @@
+/* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
+/* vim: set ts=8 sts=2 et sw=2 tw=80: */
+/* This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+#include "gtest/gtest.h"
+
+#include "Common.h"
+#include "imgLoader.h"
+#include "nsMimeTypes.h"
+#include "nsString.h"
+
+using namespace mozilla;
+using namespace mozilla::image;
+
+static void
+CheckMimeType(const char* aContents, size_t aLength, const char* aExpected)
+{
+  nsAutoCString detected;
+  nsresult rv = imgLoader::GetMimeTypeFromContent(aContents, aLength, detected);
+  if (aExpected) {
+    ASSERT_TRUE(NS_SUCCEEDED(rv));
+    EXPECT_TRUE(detected.EqualsASCII(aExpected));
+  } else {
+    ASSERT_TRUE(NS_FAILED(rv));
+    EXPECT_TRUE(detected.IsEmpty());
+  }
+}
+
+class ImageLoader : public ::testing::Test
+{
+protected:
+  AutoInitializeImageLib mInit;
+};
+
+TEST_F(ImageLoader, DetectGIF)
+{
+  const char buffer[] = "GIF87a";
+  CheckMimeType(buffer, sizeof(buffer), IMAGE_GIF);
+}
+
+TEST_F(ImageLoader, DetectPNG)
+{
+  const char buffer[] = "\x89\x50\x4E\x47\x0D\x0A\x1A\x0A";
+  CheckMimeType(buffer, sizeof(buffer), IMAGE_PNG);
+}
+
+TEST_F(ImageLoader, DetectJPEG)
+{
+  const char buffer[] = "\xFF\xD8\xFF";
+  CheckMimeType(buffer, sizeof(buffer), IMAGE_JPEG);
+}
+
+TEST_F(ImageLoader, DetectART)
+{
+  const char buffer[] = "\x4A\x47\xFF\xFF\x00";
+  CheckMimeType(buffer, sizeof(buffer), IMAGE_ART);
+}
+
+TEST_F(ImageLoader, DetectBMP)
+{
+  const char buffer[] = "BM";
+  CheckMimeType(buffer, sizeof(buffer), IMAGE_BMP);
+}
+
+TEST_F(ImageLoader, DetectICO)
+{
+  const char buffer[] = "\x00\x00\x01\x00";
+  CheckMimeType(buffer, sizeof(buffer), IMAGE_ICO);
+}
+
+TEST_F(ImageLoader, DetectWebP)
+{
+  const char buffer[] = "RIFF\xFF\xFF\xFF\xFFWEBPVP8L";
+  CheckMimeType(buffer, sizeof(buffer), IMAGE_WEBP);
+}
+
+TEST_F(ImageLoader, DetectNone)
+{
+  const char buffer[] = "abcdefghijklmnop";
+  CheckMimeType(buffer, sizeof(buffer), nullptr);
+}
+
diff --git a/image/test/gtest/moz.build b/image/test/gtest/moz.build
--- a/image/test/gtest/moz.build
+++ b/image/test/gtest/moz.build
@@ -10,16 +10,17 @@ UNIFIED_SOURCES = [
     'Common.cpp',
     'TestADAM7InterpolatingFilter.cpp',
     'TestAnimationFrameBuffer.cpp',
     'TestContainers.cpp',
     'TestCopyOnWrite.cpp',
     'TestDecoders.cpp',
     'TestDecodeToSurface.cpp',
     'TestDeinterlacingFilter.cpp',
+    'TestLoader.cpp',
     'TestMetadata.cpp',
     'TestRemoveFrameRectFilter.cpp',
     'TestSourceBuffer.cpp',
     'TestStreamingLexer.cpp',
     'TestSurfaceCache.cpp',
     'TestSurfaceSink.cpp',
 ]
 
diff --git a/netwerk/mime/nsMimeTypes.h b/netwerk/mime/nsMimeTypes.h
--- a/netwerk/mime/nsMimeTypes.h
+++ b/netwerk/mime/nsMimeTypes.h
@@ -109,16 +109,17 @@
 #define IMAGE_BMP "image/bmp"
 #define IMAGE_BMP_MS "image/x-ms-bmp"
 #define IMAGE_ICO "image/x-icon"
 #define IMAGE_ICO_MS "image/vnd.microsoft.icon"
 #define IMAGE_ICON_MS "image/icon"
 #define IMAGE_MNG "video/x-mng"
 #define IMAGE_JNG "image/x-jng"
 #define IMAGE_SVG_XML "image/svg+xml"
+#define IMAGE_WEBP "image/webp"
 
 #define MESSAGE_EXTERNAL_BODY "message/external-body"
 #define MESSAGE_NEWS "message/news"
 #define MESSAGE_RFC822 "message/rfc822"
 
 #define MULTIPART_ALTERNATIVE "multipart/alternative"
 #define MULTIPART_APPLEDOUBLE "multipart/appledouble"
 #define MULTIPART_DIGEST "multipart/digest"
diff --git a/uriloader/exthandler/nsExternalHelperAppService.cpp b/uriloader/exthandler/nsExternalHelperAppService.cpp
--- a/uriloader/exthandler/nsExternalHelperAppService.cpp
+++ b/uriloader/exthandler/nsExternalHelperAppService.cpp
@@ -523,16 +523,17 @@ static const nsExtraMimeTypeEntry extraM
     {IMAGE_GIF, "gif", "GIF Image"},
     {IMAGE_ICO, "ico,cur", "ICO Image"},
     {IMAGE_JPEG, "jpeg,jpg,jfif,pjpeg,pjp", "JPEG Image"},
     {IMAGE_PNG, "png", "PNG Image"},
     {IMAGE_APNG, "apng", "APNG Image"},
     {IMAGE_TIFF, "tiff,tif", "TIFF Image"},
     {IMAGE_XBM, "xbm", "XBM Image"},
     {IMAGE_SVG_XML, "svg", "Scalable Vector Graphics"},
+    {IMAGE_WEBP, "webp", "WebP Image" },
     {MESSAGE_RFC822, "eml", "RFC-822 data"},
     {TEXT_PLAIN, "txt,text", "Text File"},
     {APPLICATION_JSON, "json", "JavaScript Object Notation"},
     {TEXT_VTT, "vtt", "Web Video Text Tracks"},
     {TEXT_CACHE_MANIFEST, "appcache", "Application Cache Manifest"},
     {TEXT_HTML, "html,htm,shtml,ehtml", "HyperText Markup Language"},
     {"application/xhtml+xml", "xhtml,xht",
      "Extensible HyperText Markup Language"},
