# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1528992451 25200
# Node ID 5a19b871c5908ec6a282db75d896c1f3d50a8b92
# Parent  9d6e0692724f041945f3954062a01fc74aef7885
Bug 1468252 part 7 - Replace more environment->global() calls. r=luke

diff --git a/js/src/vm/Debugger.cpp b/js/src/vm/Debugger.cpp
--- a/js/src/vm/Debugger.cpp
+++ b/js/src/vm/Debugger.cpp
@@ -9984,17 +9984,17 @@ static DebuggerEnvironment* DebuggerEnvi
 
   /*
    * Forbid access to Debugger.Environment objects that are not debuggee
    * environments.
    */
   if (requireDebuggee) {
     Rooted<Env*> env(cx, static_cast<Env*>(nthisobj->getPrivate()));
     if (!Debugger::fromChildJSObject(nthisobj)->observesGlobal(
-            &env->global())) {
+            &env->nonCCWGlobal())) {
       JS_ReportErrorNumberASCII(cx, GetErrorMessage, nullptr,
                                 JSMSG_DEBUG_NOT_DEBUGGEE,
                                 "Debugger.Environment", "environment");
       return nullptr;
     }
   }
 
   return nthisobj;
@@ -10301,17 +10301,17 @@ bool DebuggerEnvironment::getCallee(JSCo
 
   return owner()->wrapDebuggeeObject(cx, callee, result);
 }
 
 bool DebuggerEnvironment::isDebuggee() const {
   MOZ_ASSERT(referent());
   MOZ_ASSERT(!referent()->is<EnvironmentObject>());
 
-  return owner()->observesGlobal(&referent()->global());
+  return owner()->observesGlobal(&referent()->nonCCWGlobal());
 }
 
 bool DebuggerEnvironment::isOptimized() const {
   return referent()->is<DebugEnvironmentProxy>() &&
          referent()->as<DebugEnvironmentProxy>().isOptimizedOut();
 }
 
 /* static */ bool DebuggerEnvironment::getNames(
diff --git a/js/src/vm/EnvironmentObject-inl.h b/js/src/vm/EnvironmentObject-inl.h
--- a/js/src/vm/EnvironmentObject-inl.h
+++ b/js/src/vm/EnvironmentObject-inl.h
@@ -66,12 +66,12 @@ inline JSObject* JSObject::enclosingEnvi
     return &as<js::EnvironmentObject>().enclosingEnvironment();
 
   if (is<js::DebugEnvironmentProxy>())
     return &as<js::DebugEnvironmentProxy>().enclosingEnvironment();
 
   if (is<js::GlobalObject>()) return nullptr;
 
   MOZ_ASSERT_IF(is<JSFunction>(), as<JSFunction>().isInterpreted());
-  return &global();
+  return &nonCCWGlobal();
 }
 
 #endif /* vm_EnvironmentObject_inl_h */
diff --git a/js/src/vm/Interpreter.cpp b/js/src/vm/Interpreter.cpp
--- a/js/src/vm/Interpreter.cpp
+++ b/js/src/vm/Interpreter.cpp
@@ -215,17 +215,17 @@ static inline bool GetNameOperation(JSCo
    * used for GNAME opcodes where the bytecode emitter has determined a
    * name access must be on the global. It also insulates us from bugs
    * in the emitter: type inference will assume that GNAME opcodes are
    * accessing the global object, and the inferred behavior should match
    * the actual behavior even if the id could be found on the env chain
    * before the global object.
    */
   if (IsGlobalOp(JSOp(*pc)) && !fp->script()->hasNonSyntacticScope())
-    envChain = &envChain->global().lexicalEnvironment();
+    envChain = &cx->global()->lexicalEnvironment();
 
   /* Kludge to allow (typeof foo == "undefined") tests. */
   JSOp op2 = JSOp(pc[JSOP_GETNAME_LENGTH]);
   if (op2 == JSOP_TYPEOF)
     return GetEnvironmentName<GetNameMode::TypeOf>(cx, envChain, name, vp);
   return GetEnvironmentName<GetNameMode::Normal>(cx, envChain, name, vp);
 }
 
