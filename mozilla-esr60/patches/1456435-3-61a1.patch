# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1524571974 -7200
# Node ID 144000c6901bc15ff984525c7f50996f870c9133
# Parent  3085492c47090b27544fb9fb01cc1d823cecb2c5
Bug 1456435: Less bool outparam in Loader too. r=heycam

MozReview-Commit-ID: D5A2BxwHGjn

diff --git a/dom/base/nsContentSink.cpp b/dom/base/nsContentSink.cpp
--- a/dom/base/nsContentSink.cpp
+++ b/dom/base/nsContentSink.cpp
@@ -749,25 +749,25 @@ nsresult nsContentSink::ProcessStyleLink
 
   mozilla::net::ReferrerPolicy referrerPolicy =
       mozilla::net::AttributeReferrerPolicyFromString(aReferrerPolicy);
   if (referrerPolicy == net::RP_Unset) {
     referrerPolicy = mDocument->GetReferrerPolicy();
   }
   // If this is a fragment parser, we don't want to observe.
   // We don't support CORS for processing instructions
-  bool isAlternate;
+  css::Loader::IsAlternate isAlternate;
   rv = mCSSLoader->LoadStyleLink(nullptr, url, nullptr, aTitle, aMedia,
                                  aAlternate, CORS_NONE, referrerPolicy,
                                  /* integrity = */ EmptyString(),
                                  mRunsToCompletion ? nullptr : this,
                                  &isAlternate);
   NS_ENSURE_SUCCESS(rv, rv);
 
-  if (!isAlternate && !mRunsToCompletion) {
+  if (isAlternate == css::Loader::IsAlternate::No && !mRunsToCompletion) {
     ++mPendingSheetCount;
     mScriptLoader->AddParserBlockingScriptExecutionBlocker();
   }
 
   return NS_OK;
 }
 
 nsresult nsContentSink::ProcessMETATag(nsIContent* aContent) {
diff --git a/dom/base/nsStyleLinkElement.cpp b/dom/base/nsStyleLinkElement.cpp
--- a/dom/base/nsStyleLinkElement.cpp
+++ b/dom/base/nsStyleLinkElement.cpp
@@ -393,17 +393,17 @@ nsStyleLinkElement::DoUpdateStyleSheet(n
   // referrerpolicy attribute, ignore this and use the document's referrer
   // policy
 
   net::ReferrerPolicy referrerPolicy = GetLinkReferrerPolicy();
   if (referrerPolicy == net::RP_Unset) {
     referrerPolicy = doc->GetReferrerPolicy();
   }
 
-  bool isAlternate;
+  IsAlternate isAlternate = IsAlternate::No;
   if (isInline) {
     nsAutoString text;
     if (!nsContentUtils::GetNodeTextContent(thisContent, false, text,
                                             fallible)) {
       return Err(NS_ERROR_OUT_OF_MEMORY);
     }
 
     MOZ_ASSERT(thisContent->NodeInfo()->NameAtom() != nsGkAtoms::link,
@@ -446,11 +446,10 @@ nsStyleLinkElement::DoUpdateStyleSheet(n
       // Don't propagate LoadStyleLink() errors further than this, since some
       // consumers (e.g. nsXMLContentSink) will completely abort on innocuous
       // things like a stylesheet load being blocked by the security system.
       return Update { };
     }
   }
 
   auto willNotify = doneLoading ? WillNotify::No : WillNotify::Yes;
-  auto alternate = isAlternate ? IsAlternate::Yes : IsAlternate::No;
-  return Update { willNotify, alternate };
+  return Update { willNotify, isAlternate };
 }
diff --git a/layout/style/Loader.cpp b/layout/style/Loader.cpp
--- a/layout/style/Loader.cpp
+++ b/layout/style/Loader.cpp
@@ -415,17 +415,18 @@ nsresult Loader::SetPreferredSheet(const
   if (mSheets) {
     LoadDataArray arr(mSheets->mPendingDatas.Count());
     for (auto iter = mSheets->mPendingDatas.Iter(); !iter.Done(); iter.Next()) {
       SheetLoadData* data = iter.Data();
       MOZ_ASSERT(data, "Must have a data");
 
       // Note that we don't want to affect what the selected style set is, so
       // use true for aHasAlternateRel.
-      if (!data->mLoader->IsAlternate(data->mTitle, true)) {
+      auto isAlternate = data->mLoader->IsAlternateSheet(data->mTitle, true);
+      if (isAlternate == IsAlternate::No) {
         arr.AppendElement(data);
         iter.Remove();
       }
     }
 
     mDatasToNotifyOn += arr.Length();
     for (uint32_t i = 0; i < arr.Length(); ++i) {
       --mDatasToNotifyOn;
@@ -801,38 +802,43 @@ nsresult SheetLoadData::VerifySheetReady
   }
 
   // Enough to set the URIs on mSheet, since any sibling datas we have share
   // the same mInner as mSheet and will thus get the same URI.
   mSheet->SetURIs(channelURI, originalURI, channelURI);
   return NS_OK_PARSE_SHEET;
 }
 
-bool Loader::IsAlternate(const nsAString& aTitle, bool aHasAlternateRel) {
+Loader::IsAlternate
+Loader::IsAlternateSheet(const nsAString& aTitle, bool aHasAlternateRel) {
   // A sheet is alternate if it has a nonempty title that doesn't match the
   // currently selected style set.  But if there _is_ no currently selected
   // style set, the sheet wasn't marked as an alternate explicitly, and aTitle
   // is nonempty, we should select the style set corresponding to aTitle, since
   // that's a preferred sheet.
   //
   // FIXME(emilio): This should return false for Shadow DOM regardless of the
   // document.
   if (aTitle.IsEmpty()) {
-    return false;
+    return IsAlternate::No;
   }
 
   if (!aHasAlternateRel && mDocument && mPreferredSheet.IsEmpty()) {
     // There's no preferred set yet, and we now have a sheet with a title.
     // Make that be the preferred set.
     mDocument->SetHeaderData(nsGkAtoms::headerDefaultStyle, aTitle);
-    // We're definitely not an alternate
-    return false;
+    // We're definitely not an alternate.
+    return IsAlternate::No;
   }
 
-  return !aTitle.Equals(mPreferredSheet);
+  if (aTitle.Equals(mPreferredSheet)) {
+    return IsAlternate::No;
+  }
+
+  return IsAlternate::Yes;
 }
 
 nsresult Loader::ObsoleteSheet(nsIURI* aURI) {
   if (!mSheets) {
     return NS_OK;
   }
   if (!aURI) {
     return NS_ERROR_INVALID_ARG;
@@ -884,31 +890,32 @@ nsresult Loader::CheckContentPolicy(nsIP
  * CreateSheet().
  */
 nsresult Loader::CreateSheet(nsIURI* aURI, nsIContent* aLinkingContent,
                              nsIPrincipal* aLoaderPrincipal,
                              css::SheetParsingMode aParsingMode,
                              CORSMode aCORSMode, ReferrerPolicy aReferrerPolicy,
                              const nsAString& aIntegrity, bool aSyncLoad,
                              bool aHasAlternateRel, const nsAString& aTitle,
-                             StyleSheetState& aSheetState, bool* aIsAlternate,
+                             StyleSheetState& aSheetState,
+                             IsAlternate* aIsAlternate,
                              RefPtr<StyleSheet>* aSheet) {
   LOG(("css::Loader::CreateSheet"));
   NS_PRECONDITION(aSheet, "Null out param!");
 
   if (!mSheets) {
     mSheets = new Sheets();
   }
 
   *aSheet = nullptr;
   aSheetState = eSheetStateUnknown;
 
   // Check the alternate state before doing anything else, because it
   // can mess with our hashtables.
-  *aIsAlternate = IsAlternate(aTitle, aHasAlternateRel);
+  *aIsAlternate = IsAlternateSheet(aTitle, aHasAlternateRel);
 
   if (aURI) {
     aSheetState = eSheetComplete;
     RefPtr<StyleSheet> sheet;
 
     // First, the XUL cache
 #ifdef MOZ_XUL
     if (IsChromeURI(aURI)) {
@@ -1081,31 +1088,31 @@ nsresult Loader::CreateSheet(nsIURI* aUR
 
 /**
  * PrepareSheet() handles setting the media and title on the sheet, as
  * well as setting the enabled state based on the title and whether
  * the sheet had "alternate" in its rel.
  */
 void Loader::PrepareSheet(StyleSheet* aSheet, const nsAString& aTitle,
                           const nsAString& aMediaString, MediaList* aMediaList,
-                          bool aIsAlternate) {
+                          IsAlternate aIsAlternate) {
   NS_PRECONDITION(aSheet, "Must have a sheet!");
 
   RefPtr<MediaList> mediaList(aMediaList);
 
   if (!aMediaString.IsEmpty()) {
     NS_ASSERTION(!aMediaList,
                  "must not provide both aMediaString and aMediaList");
     mediaList = MediaList::Create(GetStyleBackendType(), aMediaString);
   }
 
   aSheet->SetMedia(mediaList);
 
   aSheet->SetTitle(aTitle);
-  aSheet->SetEnabled(!aIsAlternate);
+  aSheet->SetEnabled(aIsAlternate == IsAlternate::No);
 }
 
 /**
  * InsertSheetInDoc handles ordering of sheets in the document.  Here
  * we have two types of sheets -- those with linking elements and
  * those without.  The latter are loaded by Link: headers.
  * The following constraints are observed:
  * 1) Any sheet with a linking element comes after all sheets without
@@ -1875,17 +1882,17 @@ void Loader::MarkLoadTreeFailed(SheetLoa
 }
 
 nsresult Loader::LoadInlineStyle(nsIContent* aElement, const nsAString& aBuffer,
                                  nsIPrincipal* aTriggeringPrincipal,
                                  uint32_t aLineNumber, const nsAString& aTitle,
                                  const nsAString& aMedia,
                                  ReferrerPolicy aReferrerPolicy,
                                  nsICSSLoaderObserver* aObserver,
-                                 bool* aCompleted, bool* aIsAlternate) {
+                                 bool* aCompleted, IsAlternate* aIsAlternate) {
   LOG(("css::Loader::LoadInlineStyle"));
 
   *aCompleted = true;
 
   if (!mEnabled) {
     LOG_WARN(("  Not enabled"));
     return NS_ERROR_NOT_AVAILABLE;
   }
@@ -1905,17 +1912,17 @@ nsresult Loader::LoadInlineStyle(nsICont
   nsresult rv = CreateSheet(nullptr, aElement, nullptr, eAuthorSheetFeatures,
                             CORS_NONE, aReferrerPolicy,
                             EmptyString(),  // no inline integrity checks
                             false, false, aTitle, state, aIsAlternate, &sheet);
   NS_ENSURE_SUCCESS(rv, rv);
   NS_ASSERTION(state == eSheetNeedsParser,
                "Inline sheets should not be cached");
 
-  LOG(("  Sheet is alternate: %d", *aIsAlternate));
+  LOG(("  Sheet is alternate: %d", static_cast<int>(*aIsAlternate)));
 
   PrepareSheet(sheet, aTitle, aMedia, nullptr, *aIsAlternate);
 
   if (aElement->HasFlag(NODE_IS_IN_SHADOW_TREE)) {
     ShadowRoot* containingShadow = aElement->GetContainingShadow();
     MOZ_ASSERT(containingShadow);
     containingShadow->InsertSheet(sheet, aElement);
   } else {
@@ -1928,17 +1935,18 @@ nsresult Loader::LoadInlineStyle(nsICont
     // The triggering principal may be an expanded principal, which is safe to
     // use for URL security checks, but not as the loader principal for a
     // stylesheet. So treat this as principal inheritance, and downgrade if
     // necessary.
     principal = BasePrincipal::Cast(aTriggeringPrincipal)->PrincipalToInherit();
   }
 
   SheetLoadData* data = new SheetLoadData(
-      this, aTitle, nullptr, sheet, owningElement, *aIsAlternate, aObserver,
+      this, aTitle, nullptr, sheet, owningElement,
+      *aIsAlternate == IsAlternate::Yes, aObserver,
       nullptr, static_cast<nsINode*>(aElement));
 
   // We never actually load this, so just set its principal directly
   sheet->SetPrincipal(principal);
 
   NS_ADDREF(data);
   data->mLineNumber = aLineNumber;
   // Parse completion releases the load data.
@@ -1959,17 +1967,17 @@ nsresult Loader::LoadInlineStyle(nsICont
 
 nsresult Loader::LoadStyleLink(nsIContent* aElement, nsIURI* aURL,
                                nsIPrincipal* aTriggeringPrincipal,
                                const nsAString& aTitle, const nsAString& aMedia,
                                bool aHasAlternateRel, CORSMode aCORSMode,
                                ReferrerPolicy aReferrerPolicy,
                                const nsAString& aIntegrity,
                                nsICSSLoaderObserver* aObserver,
-                               bool* aIsAlternate) {
+                               IsAlternate* aIsAlternate) {
   NS_PRECONDITION(aURL, "Must have URL to load");
   LOG(("css::Loader::LoadStyleLink"));
   LOG_URI("  Link uri: '%s'", aURL);
   LOG(("  Link title: '%s'", NS_ConvertUTF16toUTF8(aTitle).get()));
   LOG(("  Link media: '%s'", NS_ConvertUTF16toUTF8(aMedia).get()));
   LOG(("  Link alternate rel: %d", aHasAlternateRel));
 
   if (!mEnabled) {
@@ -2010,17 +2018,17 @@ nsresult Loader::LoadStyleLink(nsIConten
 
   StyleSheetState state;
   RefPtr<StyleSheet> sheet;
   rv = CreateSheet(aURL, aElement, principal, eAuthorSheetFeatures, aCORSMode,
                    aReferrerPolicy, aIntegrity, false, aHasAlternateRel, aTitle,
                    state, aIsAlternate, &sheet);
   NS_ENSURE_SUCCESS(rv, rv);
 
-  LOG(("  Sheet is alternate: %d", *aIsAlternate));
+  LOG(("  Sheet is alternate: %d", static_cast<int>(*aIsAlternate)));
 
   PrepareSheet(sheet, aTitle, aMedia, nullptr, *aIsAlternate);
 
   rv = InsertSheetInDoc(sheet, aElement, mDocument);
   NS_ENSURE_SUCCESS(rv, rv);
 
   nsCOMPtr<nsIStyleSheetLinkingElement> owningElement(
       do_QueryInterface(aElement));
@@ -2033,23 +2041,25 @@ nsresult Loader::LoadStyleLink(nsIConten
     }
 
     return NS_OK;
   }
 
   // Now we need to actually load it
   nsCOMPtr<nsINode> requestingNode = do_QueryInterface(context);
   SheetLoadData* data =
-      new SheetLoadData(this, aTitle, aURL, sheet, owningElement, *aIsAlternate,
+      new SheetLoadData(this, aTitle, aURL, sheet, owningElement,
+                        *aIsAlternate == IsAlternate::Yes,
                         aObserver, principal, requestingNode);
   NS_ADDREF(data);
 
   // If we have to parse and it's an alternate non-inline, defer it
   if (aURL && state == eSheetNeedsParser &&
-      mSheets->mLoadingDatas.Count() != 0 && *aIsAlternate) {
+      mSheets->mLoadingDatas.Count() != 0 &&
+      *aIsAlternate == IsAlternate::Yes) {
     LOG(("  Deferring alternate sheet load"));
     URIPrincipalReferrerPolicyAndCORSModeHashKey key(
         data->mURI, data->mLoaderPrincipal, data->mSheet->GetCORSMode(),
         data->mSheet->GetReferrerPolicy());
     mSheets->mPendingDatas.Put(&key, data);
 
     data->mMustNotify = true;
     return NS_OK;
@@ -2169,17 +2179,17 @@ nsresult Loader::LoadChildSheet(StyleShe
 #ifdef MOZ_OLD_STYLE
       aGeckoParentRule->SetSheet(sheet->AsGecko());
 #else
       MOZ_CRASH("old style system disabled");
 #endif
     }
     state = eSheetComplete;
   } else {
-    bool isAlternate;
+    IsAlternate isAlternate;
     const nsAString& empty = EmptyString();
     // For now, use CORS_NONE for child sheets
     rv = CreateSheet(aURL, nullptr, principal, aParentSheet->ParsingMode(),
                      CORS_NONE, aParentSheet->GetReferrerPolicy(),
                      EmptyString(),  // integrity is only checked on main sheet
                      aParentData ? aParentData->mSyncLoad : false, false, empty,
                      state, &isAlternate, &sheet);
     NS_ENSURE_SUCCESS(rv, rv);
@@ -2284,32 +2294,32 @@ nsresult Loader::InternalLoadNonDocument
 
   nsCOMPtr<nsIPrincipal> loadingPrincipal =
       (aOriginPrincipal && mDocument ? mDocument->NodePrincipal() : nullptr);
   nsresult rv = CheckContentPolicy(loadingPrincipal, aOriginPrincipal, aURL,
                                    mDocument, aIsPreload);
   NS_ENSURE_SUCCESS(rv, rv);
 
   StyleSheetState state;
-  bool isAlternate;
   RefPtr<StyleSheet> sheet;
   bool syncLoad = (aObserver == nullptr);
   const nsAString& empty = EmptyString();
+  IsAlternate isAlternate;
 
   rv = CreateSheet(aURL, nullptr, aOriginPrincipal, aParsingMode, aCORSMode,
                    aReferrerPolicy, aIntegrity, syncLoad, false, empty, state,
                    &isAlternate, &sheet);
   NS_ENSURE_SUCCESS(rv, rv);
 
   PrepareSheet(sheet, empty, empty, nullptr, isAlternate);
 
   if (state == eSheetComplete) {
     LOG(("  Sheet already complete"));
     if (aObserver || !mObservers.IsEmpty()) {
-      rv = PostLoadEvent(aURL, sheet, aObserver, false, nullptr);
+      rv = PostLoadEvent(aURL, sheet, aObserver, IsAlternate::No, nullptr);
     }
     if (aSheet) {
       sheet.swap(*aSheet);
     }
     return rv;
   }
 
   SheetLoadData* data = new SheetLoadData(
@@ -2327,26 +2337,27 @@ nsresult Loader::InternalLoadNonDocument
     data->mMustNotify = true;
   }
 
   return rv;
 }
 
 nsresult Loader::PostLoadEvent(nsIURI* aURI, StyleSheet* aSheet,
                                nsICSSLoaderObserver* aObserver,
-                               bool aWasAlternate,
+                               IsAlternate aWasAlternate,
                                nsIStyleSheetLinkingElement* aElement) {
   LOG(("css::Loader::PostLoadEvent"));
   NS_PRECONDITION(aSheet, "Must have sheet");
   NS_PRECONDITION(aObserver || !mObservers.IsEmpty() || aElement,
                   "Must have observer or element");
 
   RefPtr<SheetLoadData> evt = new SheetLoadData(
       this, EmptyString(),  // title doesn't matter here
-      aURI, aSheet, aElement, aWasAlternate, aObserver, nullptr, mDocument);
+      aURI, aSheet, aElement, aWasAlternate == IsAlternate::Yes, aObserver,
+      nullptr, mDocument);
 
   if (!mPostedEvents.AppendElement(evt)) {
     return NS_ERROR_OUT_OF_MEMORY;
   }
 
   nsresult rv;
   RefPtr<SheetLoadData> runnable(evt);
   if (mDocument) {
diff --git a/layout/style/Loader.h b/layout/style/Loader.h
--- a/layout/style/Loader.h
+++ b/layout/style/Loader.h
@@ -14,30 +14,30 @@
 #include "nsCompatibility.h"
 #include "nsCycleCollectionParticipant.h"
 #include "nsDataHashtable.h"
 #include "nsRefPtrHashtable.h"
 #include "nsStringFwd.h"
 #include "nsTArray.h"
 #include "nsTObserverArray.h"
 #include "nsURIHashKey.h"
+#include "nsIStyleSheetLinkingElement.h"
 #include "mozilla/Attributes.h"
 #include "mozilla/CORSMode.h"
 #include "mozilla/StyleSheetInlines.h"
 #include "mozilla/Maybe.h"
 #include "mozilla/MemoryReporting.h"
 #include "mozilla/StyleBackendType.h"
 #include "mozilla/StyleSheet.h"
 #include "mozilla/net/ReferrerPolicy.h"
 
 class nsICSSLoaderObserver;
 class nsIConsoleReportCollector;
 class nsIContent;
 class nsIDocument;
-class nsIStyleSheetLinkingElement;
 
 namespace mozilla {
 namespace dom {
 class DocGroup;
 class Element;
 }  // namespace dom
 }  // namespace mozilla
 
@@ -186,16 +186,18 @@ enum StyleSheetState {
   eSheetLoading,
   eSheetComplete
 };
 
 class Loader final {
   typedef mozilla::net::ReferrerPolicy ReferrerPolicy;
 
  public:
+  typedef nsIStyleSheetLinkingElement::IsAlternate IsAlternate;
+
   // aDocGroup is used for dispatching SheetLoadData in PostLoadEvent(). It
   // can be null if you want to use this constructor, and there's no
   // document when the Loader is constructed.
   Loader(StyleBackendType aType, mozilla::dom::DocGroup* aDocGroup);
 
   explicit Loader(nsIDocument*);
 
  private:
@@ -241,17 +243,17 @@ class Loader final {
    *        alternate sheet.
    */
   nsresult LoadInlineStyle(nsIContent* aElement, const nsAString& aBuffer,
                            nsIPrincipal* aTriggeringPrincipal,
                            uint32_t aLineNumber, const nsAString& aTitle,
                            const nsAString& aMedia,
                            ReferrerPolicy aReferrerPolicy,
                            nsICSSLoaderObserver* aObserver, bool* aCompleted,
-                           bool* aIsAlternate);
+                           IsAlternate* aIsAlternate);
 
   /**
    * Load a linked (document) stylesheet.  If a successful result is returned,
    * aObserver is guaranteed to be notified asynchronously once the sheet is
    * loaded and marked complete.  If an error is returned, aObserver will not
    * be notified.  In addition to loading the sheet, this method will insert it
    * into the stylesheet list of this CSSLoader's document.
    *
@@ -272,17 +274,18 @@ class Loader final {
    *        aHasAlternateRel.
    */
   nsresult LoadStyleLink(nsIContent* aElement, nsIURI* aURL,
                          nsIPrincipal* aTriggeringPrincipal,
                          const nsAString& aTitle, const nsAString& aMedia,
                          bool aHasAlternateRel, CORSMode aCORSMode,
                          ReferrerPolicy aReferrerPolicy,
                          const nsAString& aIntegrity,
-                         nsICSSLoaderObserver* aObserver, bool* aIsAlternate);
+                         nsICSSLoaderObserver* aObserver,
+                         IsAlternate* aIsAlternate);
 
   /**
    * Load a child (@import-ed) style sheet.  In addition to loading the sheet,
    * this method will insert it into the child sheet list of aParentSheet.  If
    * there is no sheet currently being parsed and the child sheet is not
    * complete when this method returns, then when the child sheet becomes
    * complete aParentSheet will be QIed to nsICSSLoaderObserver and
    * asynchronously notified, just like for LoadStyleLink.  Note that if the
@@ -449,17 +452,17 @@ class Loader final {
    */
   void RemoveObserver(nsICSSLoaderObserver* aObserver);
 
   // These interfaces are public only for the benefit of static functions
   // within nsCSSLoader.cpp.
 
   // IsAlternate can change our currently selected style set if none
   // is selected and aHasAlternateRel is false.
-  bool IsAlternate(const nsAString& aTitle, bool aHasAlternateRel);
+  IsAlternate IsAlternateSheet(const nsAString& aTitle, bool aHasAlternateRel);
 
   typedef nsTArray<RefPtr<SheetLoadData>> LoadDataArray;
 
   // Measure our size.
   size_t SizeOfIncludingThis(mozilla::MallocSizeOf aMallocSizeOf) const;
 
   // Marks all the sheets at the given URI obsolete, and removes them from the
   // cache.
@@ -487,26 +490,27 @@ class Loader final {
   // if aURI is not null.
   // *aIsAlternate is set based on aTitle and aHasAlternateRel.
   nsresult CreateSheet(nsIURI* aURI, nsIContent* aLinkingContent,
                        nsIPrincipal* aLoaderPrincipal,
                        css::SheetParsingMode aParsingMode, CORSMode aCORSMode,
                        ReferrerPolicy aReferrerPolicy,
                        const nsAString& aIntegrity, bool aSyncLoad,
                        bool aHasAlternateRel, const nsAString& aTitle,
-                       StyleSheetState& aSheetState, bool* aIsAlternate,
+                       StyleSheetState& aSheetState,
+                       IsAlternate* aIsAlternate,
                        RefPtr<StyleSheet>* aSheet);
 
   // Pass in either a media string or the MediaList from the CSSParser.  Don't
   // pass both.
   //
   // This method will set the sheet's enabled state based on aIsAlternate
   void PrepareSheet(StyleSheet* aSheet, const nsAString& aTitle,
                     const nsAString& aMediaString, dom::MediaList* aMediaList,
-                    bool aIsAlternate);
+                    IsAlternate);
 
   nsresult InsertSheetInDoc(StyleSheet* aSheet, nsIContent* aLinkingContent,
                             nsIDocument* aDocument);
 
   nsresult InsertChildSheet(StyleSheet* aSheet, StyleSheet* aParentSheet,
                             ImportRule* aGeckoParentRule);
 
   nsresult InternalLoadNonDocumentSheet(
@@ -520,17 +524,18 @@ class Loader final {
   // Post a load event for aObserver to be notified about aSheet.  The
   // notification will be sent with status NS_OK unless the load event is
   // canceled at some point (in which case it will be sent with
   // NS_BINDING_ABORTED).  aWasAlternate indicates the state when the load was
   // initiated, not the state at some later time.  aURI should be the URI the
   // sheet was loaded from (may be null for inline sheets).  aElement is the
   // owning element for this sheet.
   nsresult PostLoadEvent(nsIURI* aURI, StyleSheet* aSheet,
-                         nsICSSLoaderObserver* aObserver, bool aWasAlternate,
+                         nsICSSLoaderObserver* aObserver,
+                         IsAlternate aWasAlternate,
                          nsIStyleSheetLinkingElement* aElement);
 
   // Start the loads of all the sheets in mPendingDatas
   void StartAlternateLoads();
 
   // Handle an event posted by PostLoadEvent
   void HandleLoadEvent(SheetLoadData* aEvent);
 
