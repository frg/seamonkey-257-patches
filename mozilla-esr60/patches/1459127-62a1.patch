# HG changeset patch
# User Tooru Fujisawa <arai_a@mac.com>
# Date 1526015036 -32400
# Node ID f6d3ea212415298d10b0ba08a479c7f1a5d02030
# Parent  c96f5c6157e01f17bc7bccb43dd5c1e6aef94209
Bug 1459127 - Store ScriptSourceObject reference into LazyScript inside LazyScript. r=jimb

diff --git a/js/src/builtin/ReflectParse.cpp b/js/src/builtin/ReflectParse.cpp
--- a/js/src/builtin/ReflectParse.cpp
+++ b/js/src/builtin/ReflectParse.cpp
@@ -2,16 +2,17 @@
  * vim: set ts=8 sts=4 et sw=4 tw=99:
  * This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 /* JS reflection package. */
 
 #include "mozilla/DebugOnly.h"
+#include "mozilla/Maybe.h"
 #include "mozilla/Move.h"
 
 #include <stdlib.h>
 
 #include "jspubtd.h"
 
 #include "builtin/Array.h"
 #include "builtin/Reflect.h"
@@ -3064,19 +3065,25 @@ static bool reflect_parse(JSContext* cx,
 
   CompileOptions options(cx);
   options.setFileAndLine(filename, lineno);
   options.setCanLazilyParse(false);
   options.allowHTMLComments = target == ParseTarget::Script;
   mozilla::Range<const char16_t> chars = linearChars.twoByteRange();
   UsedNameTracker usedNames(cx);
   if (!usedNames.init()) return false;
+
+  RootedScriptSourceObject sourceObject(
+      cx, frontend::CreateScriptSourceObject(cx, options, mozilla::Nothing()));
+  if (!sourceObject)
+    return false;
+
   Parser<FullParseHandler, char16_t> parser(
       cx, cx->tempLifoAlloc(), options, chars.begin().get(), chars.length(),
-      /* foldConstants = */ false, usedNames, nullptr, nullptr);
+      /* foldConstants = */ false, usedNames, nullptr, nullptr, sourceObject);
   if (!parser.checkOptions()) return false;
 
   serialize.setParser(&parser);
 
   ParseNode* pn;
   if (target == ParseTarget::Script) {
     pn = parser.parse();
     if (!pn) return false;
diff --git a/js/src/frontend/BytecodeCompiler.cpp b/js/src/frontend/BytecodeCompiler.cpp
--- a/js/src/frontend/BytecodeCompiler.cpp
+++ b/js/src/frontend/BytecodeCompiler.cpp
@@ -216,24 +216,25 @@ bool BytecodeCompiler::canLazilyParse() 
 
 bool BytecodeCompiler::createParser() {
   usedNames.emplace(cx);
   if (!usedNames->init()) return false;
 
   if (canLazilyParse()) {
     syntaxParser.emplace(
         cx, alloc, options, sourceBuffer.get(), sourceBuffer.length(),
-        /* foldConstants = */ false, *usedNames, nullptr, nullptr);
+        /* foldConstants = */ false, *usedNames, nullptr, nullptr,
+        sourceObject);
 
     if (!syntaxParser->checkOptions()) return false;
   }
 
   parser.emplace(cx, alloc, options, sourceBuffer.get(), sourceBuffer.length(),
                  /* foldConstants = */ true, *usedNames,
-                 syntaxParser.ptrOr(nullptr), nullptr);
+                 syntaxParser.ptrOr(nullptr), nullptr, sourceObject);
   parser->ss = scriptSource;
   return parser->checkOptions();
 }
 
 bool BytecodeCompiler::createSourceAndParser(
     const Maybe<uint32_t>& parameterListEnd /* = Nothing() */) {
   return createScriptSource(parameterListEnd) && createParser();
 }
@@ -708,30 +709,29 @@ bool frontend::CompileLazyFunction(JSCon
         cx->runningWithTrustedPrincipals()
             ? JS_TELEMETRY_PRIVILEGED_PARSER_COMPILE_LAZY_AFTER_MS
             : JS_TELEMETRY_WEB_PARSER_COMPILE_LAZY_AFTER_MS;
     cx->runtime()->addTelemetry(HISTOGRAM, delta.ToMilliseconds());
   }
 
   UsedNameTracker usedNames(cx);
   if (!usedNames.init()) return false;
+
+  RootedScriptSourceObject sourceObject(cx, &lazy->sourceObject());
   Parser<FullParseHandler, char16_t> parser(
       cx, cx->tempLifoAlloc(), options, chars, length,
-      /* foldConstants = */ true, usedNames, nullptr, lazy);
+      /* foldConstants = */ true, usedNames, nullptr, lazy, sourceObject);
   if (!parser.checkOptions()) return false;
 
   Rooted<JSFunction*> fun(cx, lazy->functionNonDelazifying());
   ParseNode* pn =
       parser.standaloneLazyFunction(fun, lazy->toStringStart(), lazy->strict(),
                                     lazy->generatorKind(), lazy->asyncKind());
   if (!pn) return false;
 
-  RootedScriptSourceObject sourceObject(cx, lazy->sourceObject());
-  MOZ_ASSERT(sourceObject);
-
   Rooted<JSScript*> script(
       cx,
       JSScript::Create(cx, options, sourceObject, lazy->sourceStart(),
                        lazy->sourceEnd(), lazy->toStringStart(),
                        lazy->toStringEnd()));
   if (!script) return false;
 
   if (lazy->isLikelyConstructorWrapper()) script->setLikelyConstructorWrapper();
diff --git a/js/src/frontend/BytecodeEmitter.cpp b/js/src/frontend/BytecodeEmitter.cpp
--- a/js/src/frontend/BytecodeEmitter.cpp
+++ b/js/src/frontend/BytecodeEmitter.cpp
@@ -7304,19 +7304,17 @@ MOZ_NEVER_INLINE bool BytecodeEmitter::e
     if (fun->isInterpretedLazy()) {
       // We need to update the static scope chain regardless of whether
       // the LazyScript has already been initialized, due to the case
       // where we previously successfully compiled an inner function's
       // lazy script but failed to compile the outer script after the
       // fact. If we attempt to compile the outer script again, the
       // static scope chain will be newly allocated and will mismatch
       // the previously compiled LazyScript's.
-      ScriptSourceObject* source =
-          &script->sourceObject()->as<ScriptSourceObject>();
-      fun->lazyScript()->setEnclosingScopeAndSource(innermostScope(), source);
+      fun->lazyScript()->setEnclosingScope(innermostScope());
       if (emittingRunOnceLambda) fun->lazyScript()->setTreatAsRunOnce();
     } else {
       MOZ_ASSERT_IF(outersc->strict(), funbox->strictScript);
 
       // Inherit most things (principals, version, etc) from the
       // parent.  Use default values for the rest.
       Rooted<JSScript*> parent(cx, script);
       MOZ_ASSERT(parent->mutedErrors() == parser->options().mutedErrors());
diff --git a/js/src/frontend/Parser.cpp b/js/src/frontend/Parser.cpp
--- a/js/src/frontend/Parser.cpp
+++ b/js/src/frontend/Parser.cpp
@@ -723,25 +723,27 @@ void ParserBase::errorNoOffset(unsigned 
   ReportCompileError(context, Move(metadata), nullptr, JSREPORT_ERROR,
                      errorNumber, args);
 
   va_end(args);
 }
 
 ParserBase::ParserBase(JSContext* cx, LifoAlloc& alloc,
                        const ReadOnlyCompileOptions& options,
-                       bool foldConstants, UsedNameTracker& usedNames)
+                       bool foldConstants, UsedNameTracker& usedNames,
+                       ScriptSourceObject* sourceObject)
     : AutoGCRooter(cx, PARSER),
       context(cx),
       alloc(alloc),
       anyChars(cx, options, thisForCtor()),
       traceListHead(nullptr),
       pc(nullptr),
       usedNames(usedNames),
       ss(nullptr),
+      sourceObject(cx, sourceObject),
       keepAtoms(cx),
       foldConstants(foldConstants),
 #ifdef DEBUG
       checkOptionsCalled(false),
 #endif
       isUnexpectedEOF_(false),
       awaitHandling_(AwaitIsName) {
   cx->frontendCollectionPool().addActiveCompilation();
@@ -770,27 +772,28 @@ ParserBase::~ParserBase() {
 
   context->frontendCollectionPool().removeActiveCompilation();
 }
 
 template <class ParseHandler>
 PerHandlerParser<ParseHandler>::PerHandlerParser(
     JSContext* cx, LifoAlloc& alloc, const ReadOnlyCompileOptions& options,
     bool foldConstants, UsedNameTracker& usedNames,
-    LazyScript* lazyOuterFunction)
-    : ParserBase(cx, alloc, options, foldConstants, usedNames),
+    LazyScript* lazyOuterFunction, ScriptSourceObject* sourceObject)
+    : ParserBase(cx, alloc, options, foldConstants, usedNames, sourceObject),
       handler(cx, alloc, lazyOuterFunction) {}
 
 template <class ParseHandler, typename CharT>
 GeneralParser<ParseHandler, CharT>::GeneralParser(
     JSContext* cx, LifoAlloc& alloc, const ReadOnlyCompileOptions& options,
     const CharT* chars, size_t length, bool foldConstants,
     UsedNameTracker& usedNames, SyntaxParser* syntaxParser,
-    LazyScript* lazyOuterFunction)
-    : Base(cx, alloc, options, foldConstants, usedNames, lazyOuterFunction),
+    LazyScript* lazyOuterFunction, ScriptSourceObject* sourceObject)
+    : Base(cx, alloc, options, foldConstants, usedNames, lazyOuterFunction,
+           sourceObject),
       tokenStream(cx, options, chars, length) {
   // The Mozilla specific JSOPTION_EXTRA_WARNINGS option adds extra warnings
   // which are not generated if functions are parsed lazily. Note that the
   // standard "use strict" does not inhibit lazy parsing.
   if (options.extraWarningsOption)
     disableSyntaxParser();
   else
     setSyntaxParser(syntaxParser);
@@ -2303,19 +2306,19 @@ bool PerHandlerParser<SyntaxParseHandler
           LazyScript::NumInnerFunctionsLimit) {
     MOZ_ALWAYS_FALSE(abortIfSyntaxParser());
     return false;
   }
 
   FunctionBox* funbox = pc->functionBox();
   RootedFunction fun(context, funbox->function());
   LazyScript* lazy = LazyScript::Create(
-      context, fun, pc->closedOverBindingsForLazy(), pc->innerFunctionsForLazy,
-      funbox->bufStart, funbox->bufEnd, funbox->toStringStart,
-      funbox->startLine, funbox->startColumn);
+      context, fun, sourceObject, pc->closedOverBindingsForLazy(),
+      pc->innerFunctionsForLazy, funbox->bufStart, funbox->bufEnd,
+      funbox->toStringStart, funbox->startLine, funbox->startColumn);
   if (!lazy) return false;
 
   // Flags that need to be copied into the JSScript when we do the full
   // parse.
   if (pc->sc()->strict()) lazy->setStrict();
   lazy->setGeneratorKind(funbox->generatorKind());
   lazy->setAsyncKind(funbox->asyncKind());
   if (funbox->hasRest()) lazy->setHasRest();
diff --git a/js/src/frontend/Parser.h b/js/src/frontend/Parser.h
--- a/js/src/frontend/Parser.h
+++ b/js/src/frontend/Parser.h
@@ -240,17 +240,18 @@ enum AwaitHandling : uint8_t {
   AwaitIsName,
   AwaitIsKeyword,
   AwaitIsModuleKeyword
 };
 
 template <class ParseHandler, typename CharT>
 class AutoAwaitIsKeyword;
 
-class ParserBase : public StrictModeGetter, private JS::AutoGCRooter {
+class MOZ_STACK_CLASS ParserBase
+    : public StrictModeGetter, private JS::AutoGCRooter {
  private:
   ParserBase* thisForCtor() { return this; }
 
   // This is needed to cast a parser to JS::AutoGCRooter.
   friend void js::frontend::TraceParser(JSTracer* trc,
                                         JS::AutoGCRooter* parser);
 
  public:
@@ -267,16 +268,18 @@ class ParserBase : public StrictModeGett
   /* innermost parse context (stack-allocated) */
   ParseContext* pc;
 
   // For tracking used names in this parsing session.
   UsedNameTracker& usedNames;
 
   ScriptSource* ss;
 
+  RootedScriptSourceObject sourceObject;
+
   /* Root atoms and objects allocated for the parsed tree. */
   AutoKeepAtoms keepAtoms;
 
   /* Perform constant-folding; must be true when interfacing with the emitter.
    */
   const bool foldConstants : 1;
 
  protected:
@@ -293,17 +296,17 @@ class ParserBase : public StrictModeGett
  public:
   bool awaitIsKeyword() const { return awaitHandling_ != AwaitIsName; }
 
   template <class, typename>
   friend class AutoAwaitIsKeyword;
 
   ParserBase(JSContext* cx, LifoAlloc& alloc,
              const ReadOnlyCompileOptions& options, bool foldConstants,
-             UsedNameTracker& usedNames);
+             UsedNameTracker& usedNames, ScriptSourceObject* sourceObject);
   ~ParserBase();
 
   bool checkOptions();
 
   void trace(JSTracer* trc);
 
   const char* getFilename() const { return anyChars.getFilename(); }
   TokenPos pos() const { return anyChars.currentToken().pos; }
@@ -416,17 +419,17 @@ inline ParseContext::VarScope::VarScope(
 }
 
 enum FunctionCallBehavior {
   PermitAssignmentToFunctionCalls,
   ForbidAssignmentToFunctionCalls
 };
 
 template <class ParseHandler>
-class PerHandlerParser : public ParserBase {
+class MOZ_STACK_CLASS PerHandlerParser : public ParserBase {
  private:
   using Node = typename ParseHandler::Node;
 
  protected:
   /* State specific to the kind of parse being performed. */
   ParseHandler handler;
 
   // When ParseHandler is FullParseHandler:
@@ -446,17 +449,18 @@ class PerHandlerParser : public ParserBa
   // here, then intermediate all access to this field through accessors in
   // |GeneralParser<ParseHandler, CharT>| that impose the real type on this
   // field.
   void* internalSyntaxParser_;
 
  protected:
   PerHandlerParser(JSContext* cx, LifoAlloc& alloc,
                    const ReadOnlyCompileOptions& options, bool foldConstants,
-                   UsedNameTracker& usedNames, LazyScript* lazyOuterFunction);
+                   UsedNameTracker& usedNames, LazyScript* lazyOuterFunction,
+                   ScriptSourceObject* sourceObject);
 
   static Node null() { return ParseHandler::null(); }
 
   Node stringLiteral();
 
   const char* nameIsArgumentsOrEval(Node node);
 
   bool noteDestructuredPositionalFormalParameter(Node fn, Node destruct);
@@ -601,17 +605,17 @@ enum YieldHandling { YieldIsName, YieldI
 enum InHandling { InAllowed, InProhibited };
 enum DefaultHandling { NameRequired, AllowDefaultName };
 enum TripledotHandling { TripledotAllowed, TripledotProhibited };
 
 template <class ParseHandler, typename CharT>
 class Parser;
 
 template <class ParseHandler, typename CharT>
-class GeneralParser : public PerHandlerParser<ParseHandler> {
+class MOZ_STACK_CLASS GeneralParser : public PerHandlerParser<ParseHandler> {
  public:
   using TokenStream =
       TokenStreamSpecific<CharT, ParserAnyCharsAccess<GeneralParser>>;
 
  private:
   using Base = PerHandlerParser<ParseHandler>;
   using FinalParser = Parser<ParseHandler, CharT>;
   using Node = typename ParseHandler::Node;
@@ -832,17 +836,18 @@ class GeneralParser : public PerHandlerP
 
  public:
   TokenStream tokenStream;
 
  public:
   GeneralParser(JSContext* cx, LifoAlloc& alloc,
                 const ReadOnlyCompileOptions& options, const CharT* chars,
                 size_t length, bool foldConstants, UsedNameTracker& usedNames,
-                SyntaxParser* syntaxParser, LazyScript* lazyOuterFunction);
+                SyntaxParser* syntaxParser, LazyScript* lazyOuterFunction,
+                ScriptSourceObject* sourceObject);
 
   inline void setAwaitHandling(AwaitHandling awaitHandling);
 
   /*
    * Parse a top-level JS script.
    */
   Node parse();
 
@@ -1241,17 +1246,17 @@ class GeneralParser : public PerHandlerP
   bool noteDeclaredName(HandlePropertyName name, DeclarationKind kind,
                         TokenPos pos);
 
  private:
   inline bool asmJS(Node list);
 };
 
 template <typename CharT>
-class Parser<SyntaxParseHandler, CharT> final
+class MOZ_STACK_CLASS Parser<SyntaxParseHandler, CharT> final
     : public GeneralParser<SyntaxParseHandler, CharT> {
   using Base = GeneralParser<SyntaxParseHandler, CharT>;
   using Node = SyntaxParseHandler::Node;
 
   using SyntaxParser = Parser<SyntaxParseHandler, CharT>;
 
   // Numerous Base::* functions have bodies like
   //
@@ -1351,17 +1356,17 @@ class Parser<SyntaxParseHandler, CharT> 
                              FunctionSyntaxKind kind, bool tryAnnexB);
 
   bool asmJS(Node list);
 
   // Functions present only in Parser<SyntaxParseHandler, CharT>.
 };
 
 template <typename CharT>
-class Parser<FullParseHandler, CharT> final
+class MOZ_STACK_CLASS Parser<FullParseHandler, CharT> final
     : public GeneralParser<FullParseHandler, CharT> {
   using Base = GeneralParser<FullParseHandler, CharT>;
   using Node = FullParseHandler::Node;
 
   using SyntaxParser = Parser<SyntaxParseHandler, CharT>;
 
   // Numerous Base::* functions have bodies like
   //
diff --git a/js/src/jsapi-tests/testBinASTReader.cpp b/js/src/jsapi-tests/testBinASTReader.cpp
--- a/js/src/jsapi-tests/testBinASTReader.cpp
+++ b/js/src/jsapi-tests/testBinASTReader.cpp
@@ -13,16 +13,18 @@
 #include <unistd.h>
 
 #elif defined(XP_WIN)
 
 #include <windows.h>
 
 #endif
 
+#include "mozilla/Maybe.h"
+
 #include "jsapi.h"
 
 #include "frontend/BinSource.h"
 #include "frontend/FullParseHandler.h"
 #include "frontend/ParseContext.h"
 #include "frontend/Parser.h"
 #include "gc/Zone.h"
 #include "js/Vector.h"
@@ -143,20 +145,27 @@ void runTestFromPath(JSContext* cx, cons
     readFull(cx, txtPath.begin(), txtSource);
 
     // Parse text file.
     CompileOptions txtOptions(cx);
     txtOptions.setFileAndLine(txtPath.begin(), 0);
 
     UsedNameTracker txtUsedNames(cx);
     if (!txtUsedNames.init()) MOZ_CRASH("Couldn't initialize used names");
+
+    RootedScriptSourceObject sourceObject(
+        cx,
+        frontend::CreateScriptSourceObject(cx, txtOptions, mozilla::Nothing()));
+    if (!sourceObject)
+      MOZ_CRASH("Couldn't initialize ScriptSourceObject");
+
     js::frontend::Parser<js::frontend::FullParseHandler, char16_t> txtParser(
         cx, allocScope.alloc(), txtOptions, txtSource.begin(),
         txtSource.length(), /* foldConstants = */ false, txtUsedNames, nullptr,
-        nullptr);
+        nullptr, sourceObject);
     if (!txtParser.checkOptions())
       MOZ_CRASH("Bad options");
 
     // Will be deallocated once `parser` goes out of scope.
     auto txtParsed = txtParser.parse();
     RootedValue txtExn(cx);
     if (!txtParsed) {
       // Save exception for more detailed error message, if necessary.
diff --git a/js/src/jsapi.cpp b/js/src/jsapi.cpp
--- a/js/src/jsapi.cpp
+++ b/js/src/jsapi.cpp
@@ -3860,19 +3860,25 @@ JS_PUBLIC_API bool JS_BufferIsCompilable
 
   // Return true on any out-of-memory error or non-EOF-related syntax error, so
   // our caller doesn't try to collect more buffered source.
   bool result = true;
 
   CompileOptions options(cx);
   frontend::UsedNameTracker usedNames(cx);
   if (!usedNames.init()) return false;
+
+  RootedScriptSourceObject sourceObject(
+      cx, frontend::CreateScriptSourceObject(cx, options, mozilla::Nothing()));
+  if (!sourceObject)
+   return false;
+
   frontend::Parser<frontend::FullParseHandler, char16_t> parser(
       cx, cx->tempLifoAlloc(), options, chars, length,
-      /* foldConstants = */ true, usedNames, nullptr, nullptr);
+      /* foldConstants = */ true, usedNames, nullptr, nullptr, sourceObject);
   JS::WarningReporter older = JS::SetWarningReporter(cx, nullptr);
   if (!parser.checkOptions() || !parser.parse()) {
     // We ran into an error. If it was because we ran out of source, we
     // return false so our caller knows to try to collect more buffered
     // source.
     if (parser.isUnexpectedEOF()) result = false;
 
     cx->clearPendingException();
diff --git a/js/src/shell/js.cpp b/js/src/shell/js.cpp
--- a/js/src/shell/js.cpp
+++ b/js/src/shell/js.cpp
@@ -4074,19 +4074,25 @@ static bool Parse(JSContext* cx, unsigne
 
   CompileOptions options(cx);
   options.setIntroductionType("js shell parse")
          .setFileAndLine("<string>", 1)
          .setAllowSyntaxParser(allowSyntaxParser);
 
   UsedNameTracker usedNames(cx);
   if (!usedNames.init()) return false;
+
+  RootedScriptSourceObject sourceObject(
+      cx, frontend::CreateScriptSourceObject(cx, options, Nothing()));
+  if (!sourceObject)
+    return false;
+
   Parser<FullParseHandler, char16_t> parser(
       cx, cx->tempLifoAlloc(), options, chars, length,
-      /* foldConstants = */ false, usedNames, nullptr, nullptr);
+      /* foldConstants = */ false, usedNames, nullptr, nullptr, sourceObject);
   if (!parser.checkOptions()) return false;
 
   ParseNode* pn =
       parser.parse();  // Deallocated once `parser` goes out of scope.
   if (!pn) return false;
 #ifdef DEBUG
   js::Fprinter out(stderr);
   DumpParseTree(pn, out);
@@ -4119,19 +4125,25 @@ static bool SyntaxParse(JSContext* cx, u
 
   AutoStableStringChars stableChars(cx);
   if (!stableChars.initTwoByte(cx, scriptContents)) return false;
 
   const char16_t* chars = stableChars.twoByteRange().begin().get();
   size_t length = scriptContents->length();
   UsedNameTracker usedNames(cx);
   if (!usedNames.init()) return false;
+
+  RootedScriptSourceObject sourceObject(
+      cx, frontend::CreateScriptSourceObject(cx, options, Nothing()));
+  if (!sourceObject)
+    return false;
+
   Parser<frontend::SyntaxParseHandler, char16_t> parser(
       cx, cx->tempLifoAlloc(), options, chars, length, false, usedNames,
-      nullptr, nullptr);
+      nullptr, nullptr, sourceObject);
   if (!parser.checkOptions()) return false;
 
   bool succeeded = parser.parse();
   if (cx->isExceptionPending()) return false;
 
   if (!succeeded && !parser.hadAbortedSyntaxParse()) {
     // If no exception is posted, either there was an OOM or a language
     // feature unhandled by the syntax parser was encountered.
diff --git a/js/src/vm/Debugger.cpp b/js/src/vm/Debugger.cpp
--- a/js/src/vm/Debugger.cpp
+++ b/js/src/vm/Debugger.cpp
@@ -4648,19 +4648,25 @@ bool Debugger::isCompilableUnit(JSContex
   AutoStableStringChars chars(cx);
   if (!chars.initTwoByte(cx, str)) return false;
 
   bool result = true;
 
   CompileOptions options(cx);
   frontend::UsedNameTracker usedNames(cx);
   if (!usedNames.init()) return false;
+
+  RootedScriptSourceObject sourceObject(
+      cx, frontend::CreateScriptSourceObject(cx, options, Nothing()));
+  if (!sourceObject)
+    return false;
+
   frontend::Parser<frontend::FullParseHandler, char16_t> parser(
       cx, cx->tempLifoAlloc(), options, chars.twoByteChars(), length,
-      /* foldConstants = */ true, usedNames, nullptr, nullptr);
+      /* foldConstants = */ true, usedNames, nullptr, nullptr, sourceObject);
   JS::WarningReporter older = JS::SetWarningReporter(cx, nullptr);
   if (!parser.checkOptions() || !parser.parse()) {
     // We ran into an error. If it was because we ran out of memory we report
     // it in the usual way.
     if (cx->isThrowingOutOfMemory()) {
       JS::SetWarningReporter(cx, older);
       return false;
     }
diff --git a/js/src/vm/JSFunction.cpp b/js/src/vm/JSFunction.cpp
--- a/js/src/vm/JSFunction.cpp
+++ b/js/src/vm/JSFunction.cpp
@@ -1486,17 +1486,17 @@ static JSAtom* AppendBoundFunctionPrefix
       // Remember the lazy script on the compiled script, so it can be
       // stored on the function again in case of re-lazification.
       // Only functions without inner functions are re-lazified.
       script->setLazyScript(lazy);
     }
 
     // XDR the newly delazified function.
     if (script->scriptSource()->hasEncoder()) {
-      RootedScriptSourceObject sourceObject(cx, lazy->sourceObject());
+      RootedScriptSourceObject sourceObject(cx, &lazy->sourceObject());
       if (!script->scriptSource()->xdrEncodeFunction(cx, fun, sourceObject))
         return false;
     }
 
     return true;
   }
 
   /* Lazily cloned self-hosted script. */
diff --git a/js/src/vm/JSScript.cpp b/js/src/vm/JSScript.cpp
--- a/js/src/vm/JSScript.cpp
+++ b/js/src/vm/JSScript.cpp
@@ -843,16 +843,18 @@ template XDRResult js::XDRScript(XDRStat
                                  HandleScriptSourceObject, HandleFunction,
                                  MutableHandleScript);
 
 template <XDRMode mode>
 XDRResult js::XDRLazyScript(XDRState<mode>* xdr, HandleScope enclosingScope,
                             HandleScriptSourceObject sourceObject,
                             HandleFunction fun,
                             MutableHandle<LazyScript*> lazy) {
+  MOZ_ASSERT_IF(mode == XDR_DECODE, sourceObject);
+
   JSContext* cx = xdr->cx();
 
   {
     uint32_t sourceStart;
     uint32_t sourceEnd;
     uint32_t toStringStart;
     uint32_t toStringEnd;
     uint32_t lineno;
@@ -900,17 +902,17 @@ XDRResult js::XDRLazyScript(XDRState<mod
   // Code inner functions.
   {
     RootedFunction func(cx);
     GCPtrFunction* innerFunctions = lazy->innerFunctions();
     size_t numInnerFunctions = lazy->numInnerFunctions();
     for (size_t i = 0; i < numInnerFunctions; i++) {
       if (mode == XDR_ENCODE) func = innerFunctions[i];
 
-      MOZ_TRY(XDRInterpretedFunction(xdr, nullptr, nullptr, &func));
+      MOZ_TRY(XDRInterpretedFunction(xdr, nullptr, sourceObject, &func));
 
       if (mode == XDR_DECODE) innerFunctions[i] = func;
     }
   }
 
   return Ok();
 }
 
@@ -3773,74 +3775,72 @@ bool JSScript::formalIsAliased(unsigned 
   }
   MOZ_CRASH("Argument slot not found");
 }
 
 bool JSScript::formalLivesInArgumentsObject(unsigned argSlot) {
   return argsObjAliasesFormals() && !formalIsAliased(argSlot);
 }
 
-LazyScript::LazyScript(JSFunction* fun, void* table, uint64_t packedFields,
-                       uint32_t sourceStart, uint32_t sourceEnd,
-                       uint32_t toStringStart, uint32_t lineno, uint32_t column)
+LazyScript::LazyScript(JSFunction* fun, ScriptSourceObject& sourceObject,
+                       void* table, uint64_t packedFields, uint32_t sourceStart,
+                       uint32_t sourceEnd, uint32_t toStringStart,
+                       uint32_t lineno, uint32_t column)
     : script_(nullptr),
       function_(fun),
       enclosingScope_(nullptr),
-      sourceObject_(nullptr),
+      sourceObject_(&sourceObject),
       table_(table),
       packedFields_(packedFields),
       sourceStart_(sourceStart),
       sourceEnd_(sourceEnd),
       toStringStart_(toStringStart),
       toStringEnd_(sourceEnd),
       lineno_(lineno),
       column_(column) {
+  MOZ_ASSERT(function_);
+  MOZ_ASSERT(sourceObject_);
+  MOZ_ASSERT(function_->compartment() == sourceObject_->compartment());
   MOZ_ASSERT(sourceStart <= sourceEnd);
   MOZ_ASSERT(toStringStart <= sourceStart);
 }
 
 void LazyScript::initScript(JSScript* script) {
   MOZ_ASSERT(script);
   MOZ_ASSERT(!script_.unbarrieredGet());
   script_.set(script);
 }
 
 void LazyScript::resetScript() {
   MOZ_ASSERT(script_.unbarrieredGet());
   script_.set(nullptr);
 }
 
-void LazyScript::setEnclosingScopeAndSource(Scope* enclosingScope,
-                                            ScriptSourceObject* sourceObject) {
-  MOZ_ASSERT(function_->compartment() == sourceObject->compartment());
+void LazyScript::setEnclosingScope(Scope* enclosingScope) {
   // This method may be called to update the enclosing scope. See comment
   // above the callsite in BytecodeEmitter::emitFunction.
-  MOZ_ASSERT_IF(sourceObject_,
-                sourceObject_ == sourceObject && enclosingScope_);
-  MOZ_ASSERT_IF(!sourceObject_, !enclosingScope_);
-
   enclosingScope_ = enclosingScope;
-  sourceObject_ = sourceObject;
 }
 
-ScriptSourceObject* LazyScript::sourceObject() const {
-  return sourceObject_ ? &sourceObject_->as<ScriptSourceObject>() : nullptr;
+ScriptSourceObject& LazyScript::sourceObject() const {
+  return sourceObject_->as<ScriptSourceObject>();
 }
 
 ScriptSource* LazyScript::maybeForwardedScriptSource() const {
-  JSObject* source = MaybeForwarded(sourceObject());
+  JSObject* source = MaybeForwarded(&sourceObject());
   return UncheckedUnwrapWithoutExpose(source)
       ->as<ScriptSourceObject>()
       .source();
 }
 
 /* static */ LazyScript* LazyScript::CreateRaw(
-    JSContext* cx, HandleFunction fun, uint64_t packedFields,
-    uint32_t sourceStart, uint32_t sourceEnd, uint32_t toStringStart,
-    uint32_t lineno, uint32_t column) {
+    JSContext* cx, HandleFunction fun, HandleScriptSourceObject sourceObject,
+    uint64_t packedFields, uint32_t sourceStart, uint32_t sourceEnd,
+    uint32_t toStringStart, uint32_t lineno, uint32_t column) {
+  MOZ_ASSERT(sourceObject);
   union {
     PackedView p;
     uint64_t packed;
   };
 
   packed = packedFields;
 
   // Reset runtime flags to obtain a fresh LazyScript.
@@ -3857,22 +3857,23 @@ ScriptSource* LazyScript::maybeForwarded
     return nullptr;
   }
 
   LazyScript* res = Allocate<LazyScript>(cx);
   if (!res) return nullptr;
 
   cx->compartment()->scheduleDelazificationForDebugger();
 
-  return new (res) LazyScript(fun, table.forget(), packed, sourceStart,
-                              sourceEnd, toStringStart, lineno, column);
+  return new (res) LazyScript(fun, *sourceObject, table.forget(), packed,
+                              sourceStart, sourceEnd, toStringStart, lineno,
+                              column);
 }
 
 /* static */ LazyScript* LazyScript::Create(
-    JSContext* cx, HandleFunction fun,
+    JSContext* cx, HandleFunction fun, HandleScriptSourceObject sourceObject,
     const frontend::AtomVector& closedOverBindings,
     Handle<GCVector<JSFunction*, 8>> innerFunctions, uint32_t sourceStart,
     uint32_t sourceEnd, uint32_t toStringStart, uint32_t lineno,
     uint32_t column) {
   union {
     PackedView p;
     uint64_t packedFields;
   };
@@ -3887,19 +3888,19 @@ ScriptSource* LazyScript::maybeForwarded
   p.strict = false;
   p.bindingsAccessedDynamically = false;
   p.hasDebuggerStatement = false;
   p.hasDirectEval = false;
   p.isLikelyConstructorWrapper = false;
   p.isDerivedClassConstructor = false;
   p.needsHomeObject = false;
 
-  LazyScript* res = LazyScript::CreateRaw(cx, fun, packedFields, sourceStart,
-                                          sourceEnd, toStringStart, lineno,
-                                          column);
+  LazyScript* res = LazyScript::CreateRaw(cx, fun, sourceObject, packedFields,
+                                          sourceStart, sourceEnd, toStringStart,
+                                          lineno, column);
   if (!res) return nullptr;
 
   JSAtom** resClosedOverBindings = res->closedOverBindings();
   for (size_t i = 0; i < res->numClosedOverBindings(); i++)
     resClosedOverBindings[i] = closedOverBindings[i];
 
   GCPtrFunction* resInnerFunctions = res->innerFunctions();
   for (size_t i = 0; i < res->numInnerFunctions(); i++)
@@ -3915,41 +3916,39 @@ ScriptSource* LazyScript::maybeForwarded
     uint32_t toStringStart, uint32_t lineno, uint32_t column) {
   // Dummy atom which is not a valid property name.
   RootedAtom dummyAtom(cx, cx->names().comma);
 
   // Dummy function which is not a valid function as this is the one which is
   // holding this lazy script.
   HandleFunction dummyFun = fun;
 
-  LazyScript* res = LazyScript::CreateRaw(cx, fun, packedFields, sourceStart,
-                                          sourceEnd, toStringStart, lineno,
-                                          column);
+  LazyScript* res = LazyScript::CreateRaw(cx, fun, sourceObject, packedFields,
+                                          sourceStart, sourceEnd, toStringStart,
+                                          lineno, column);
   if (!res) return nullptr;
 
   // Fill with dummies, to be GC-safe after the initialization of the free
   // variables and inner functions.
   size_t i, num;
   JSAtom** closedOverBindings = res->closedOverBindings();
   for (i = 0, num = res->numClosedOverBindings(); i < num; i++)
     closedOverBindings[i] = dummyAtom;
 
   GCPtrFunction* functions = res->innerFunctions();
   for (i = 0, num = res->numInnerFunctions(); i < num; i++)
     functions[i].init(dummyFun);
 
-  // Set the enclosing scope and source object of the lazy function. These
-  // values should only be non-null if we have a non-lazy enclosing script.
-  // LazyScript::isEnclosingScriptLazy relies on the source object being null
-  // if we're nested inside another lazy function.
-  MOZ_ASSERT(!!sourceObject == !!enclosingScope);
-  MOZ_ASSERT(!res->sourceObject());
+  // Set the enclosing scope of the lazy function. This value should only be
+  // non-null if we have a non-lazy enclosing script.
+  // LazyScript::isEnclosingScriptLazy relies on the enclosing scope being
+  // null if we're nested inside another lazy function.
   MOZ_ASSERT(!res->enclosingScope());
-  if (sourceObject)
-    res->setEnclosingScopeAndSource(enclosingScope, sourceObject);
+  if (enclosingScope)
+    res->setEnclosingScope(enclosingScope);
 
   MOZ_ASSERT(!res->hasScript());
   if (script) res->initScript(script);
 
   return res;
 }
 
 void LazyScript::initRuntimeFields(uint64_t packedFields) {
@@ -4067,16 +4066,13 @@ const char* JS::ubi::Concrete<JSScript>:
 JS::ubi::Node::Size JS::ubi::Concrete<js::LazyScript>::size(
     mozilla::MallocSizeOf mallocSizeOf) const {
   Size size = js::gc::Arena::thingSize(get().asTenured().getAllocKind());
   size += get().sizeOfExcludingThis(mallocSizeOf);
   return size;
 }
 
 const char* JS::ubi::Concrete<js::LazyScript>::scriptFilename() const {
-  auto sourceObject = get().sourceObject();
-  if (!sourceObject) return nullptr;
-
-  auto source = sourceObject->source();
+  auto source = get().sourceObject().source();
   if (!source) return nullptr;
 
   return source->filename();
 }
diff --git a/js/src/vm/JSScript.h b/js/src/vm/JSScript.h
--- a/js/src/vm/JSScript.h
+++ b/js/src/vm/JSScript.h
@@ -1975,36 +1975,38 @@ class LazyScript : public gc::TenuredCel
   uint32_t sourceEnd_;
   uint32_t toStringStart_;
   uint32_t toStringEnd_;
   // Line and column of |begin_| position, that is the position where we
   // start parsing.
   uint32_t lineno_;
   uint32_t column_;
 
-  LazyScript(JSFunction* fun, void* table, uint64_t packedFields,
-             uint32_t begin, uint32_t end, uint32_t toStringStart,
-             uint32_t lineno, uint32_t column);
+  LazyScript(JSFunction* fun, ScriptSourceObject& sourceObject, void* table,
+             uint64_t packedFields, uint32_t begin, uint32_t end,
+             uint32_t toStringStart, uint32_t lineno, uint32_t column);
 
   // Create a LazyScript without initializing the closedOverBindings and the
   // innerFunctions. To be GC-safe, the caller must initialize both vectors
   // with valid atoms and functions.
   static LazyScript* CreateRaw(JSContext* cx, HandleFunction fun,
+                               HandleScriptSourceObject sourceObject,
                                uint64_t packedData, uint32_t begin,
                                uint32_t end, uint32_t toStringStart,
                                uint32_t lineno, uint32_t column);
 
  public:
   static const uint32_t NumClosedOverBindingsLimit =
       1 << NumClosedOverBindingsBits;
   static const uint32_t NumInnerFunctionsLimit = 1 << NumInnerFunctionsBits;
 
   // Create a LazyScript and initialize closedOverBindings and innerFunctions
   // with the provided vectors.
   static LazyScript* Create(JSContext* cx, HandleFunction fun,
+                            HandleScriptSourceObject sourceObject,
                             const frontend::AtomVector& closedOverBindings,
                             Handle<GCVector<JSFunction*, 8>> innerFunctions,
                             uint32_t begin, uint32_t end,
                             uint32_t toStringStart, uint32_t lineno,
                             uint32_t column);
 
   // Create a LazyScript and initialize the closedOverBindings and the
   // innerFunctions with dummy values to be replaced in a later initialization
@@ -2034,23 +2036,22 @@ class LazyScript : public gc::TenuredCel
   JSScript* maybeScript() { return script_; }
   const JSScript* maybeScriptUnbarriered() const {
     return script_.unbarrieredGet();
   }
   bool hasScript() const { return bool(script_); }
 
   Scope* enclosingScope() const { return enclosingScope_; }
 
-  ScriptSourceObject* sourceObject() const;
-  ScriptSource* scriptSource() const { return sourceObject()->source(); }
+  ScriptSourceObject& sourceObject() const;
+  ScriptSource* scriptSource() const { return sourceObject().source(); }
   ScriptSource* maybeForwardedScriptSource() const;
   bool mutedErrors() const { return scriptSource()->mutedErrors(); }
 
-  void setEnclosingScopeAndSource(Scope* enclosingScope,
-                                  ScriptSourceObject* sourceObject);
+  void setEnclosingScope(Scope* enclosingScope);
 
   uint32_t numClosedOverBindings() const { return p_.numClosedOverBindings; }
   JSAtom** closedOverBindings() { return (JSAtom**)table_; }
 
   uint32_t numInnerFunctions() const { return p_.numInnerFunctions; }
   GCPtrFunction* innerFunctions() {
     return (GCPtrFunction*)&closedOverBindings()[numClosedOverBindings()];
   }
@@ -2140,17 +2141,17 @@ class LazyScript : public gc::TenuredCel
   }
 
   // Returns true if the enclosing script failed to compile.
   // See the comment in the definition for more details.
   bool hasUncompletedEnclosingScript() const;
 
   // Returns true if the enclosing script is also lazy.
   bool isEnclosingScriptLazy() const {
-    return !sourceObject_;
+    return !enclosingScope_;
   }
 
   friend class GCMarker;
   void traceChildren(JSTracer* trc);
   void finalize(js::FreeOp* fop);
 
   static const JS::TraceKind TraceKind = JS::TraceKind::LazyScript;
 
