# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1553122050 0
# Node ID a3b448c3db0f55a8f520c54d90cb7592486b4f44
# Parent  18278585a47a070f557c197b4555c0d3464d3995
Bug 1537576 - fix some rust-nightly warnings about unused doc comments. r=boris

Differential Revision: https://phabricator.services.mozilla.com/D24287

diff --git a/servo/components/style/bloom.rs b/servo/components/style/bloom.rs
--- a/servo/components/style/bloom.rs
+++ b/servo/components/style/bloom.rs
@@ -9,21 +9,23 @@
 
 use atomic_refcell::{AtomicRefMut, AtomicRefCell};
 use dom::{SendElement, TElement};
 use owning_ref::OwningHandle;
 use selectors::bloom::BloomFilter;
 use servo_arc::Arc;
 use smallvec::SmallVec;
 
-/// Bloom filters are large allocations, so we store them in thread-local storage
-/// such that they can be reused across style traversals. StyleBloom is responsible
-/// for ensuring that the bloom filter is zeroed when it is dropped.
-thread_local!(static BLOOM_KEY: Arc<AtomicRefCell<BloomFilter>> =
-              Arc::new(AtomicRefCell::new(BloomFilter::new())));
+thread_local! {
+    /// Bloom filters are large allocations, so we store them in thread-local storage
+    /// such that they can be reused across style traversals. StyleBloom is responsible
+    /// for ensuring that the bloom filter is zeroed when it is dropped.
+    static BLOOM_KEY: Arc<AtomicRefCell<BloomFilter>> =
+              Arc::new(AtomicRefCell::new(BloomFilter::new()));
+}
 
 /// A struct that allows us to fast-reject deep descendant selectors avoiding
 /// selector-matching.
 ///
 /// This is implemented using a counting bloom filter, and it's a standard
 /// optimization. See Gecko's `AncestorFilter`, and Blink's and WebKit's
 /// `SelectorFilter`.
 ///
diff --git a/servo/components/style/font_face.rs b/servo/components/style/font_face.rs
--- a/servo/components/style/font_face.rs
+++ b/servo/components/style/font_face.rs
@@ -354,17 +354,16 @@ macro_rules! font_face_descriptors {
                 pub fn $m_ident(&self) -> &$m_ty {
                     self.0 .$m_ident.as_ref().unwrap()
                 }
             )*
         }
     }
 }
 
-/// css-name rust_identifier: Type,
 #[cfg(feature = "gecko")]
 font_face_descriptors! {
     mandatory descriptors = [
         /// The name of this font face
         "font-family" family / mFamily: FamilyName,
 
         /// The alternative sources for this font face.
         "src" sources / mSrc: Vec<Source>,
diff --git a/servo/components/style/stylist.rs b/servo/components/style/stylist.rs
--- a/servo/components/style/stylist.rs
+++ b/servo/components/style/stylist.rs
@@ -55,18 +55,18 @@ use thread_state::{self, ThreadState};
 /// The type of the stylesheets that the stylist contains.
 #[cfg(feature = "servo")]
 pub type StylistSheet = ::stylesheets::DocumentStyleSheet;
 
 /// The type of the stylesheets that the stylist contains.
 #[cfg(feature = "gecko")]
 pub type StylistSheet = ::gecko::data::GeckoStyleSheet;
 
-/// A cache of computed user-agent data, to be shared across documents.
 lazy_static! {
+    /// A cache of computed user-agent data, to be shared across documents.
     static ref UA_CASCADE_DATA_CACHE: Mutex<UserAgentCascadeDataCache> =
         Mutex::new(UserAgentCascadeDataCache::new());
 }
 
 struct UserAgentCascadeDataCache {
     entries: Vec<Arc<UserAgentCascadeData>>,
 }
 
