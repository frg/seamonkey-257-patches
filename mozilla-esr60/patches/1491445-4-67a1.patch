# HG changeset patch
# User Markus Stange <mstange@themasta.com>
# Date 1550083880 0
# Node ID be263e3d8bdfc0b6c072ffad2736999b9652d036
# Parent  541c31dd54018965061c0648818aa11aa584b64e
Bug 1491445 - Make the ChildView always non-opaque and let NSVisualEffectView handle clearing of vibrant areas. r=spohl

This is a clearer implementation that achieves the same thing.

Moreover, disabling the clearing by overriding drawRect wouldn't work in
CoreAnimation windows because in CoreAnimation windows, the clearing happens
through a property of the NSVisualEffectView's CALayer, and not through the
view's drawRect implementation - drawRect probably isn't even called in that
context.

Differential Revision: https://phabricator.services.mozilla.com/D19601

diff --git a/widget/cocoa/VibrancyManager.h b/widget/cocoa/VibrancyManager.h
--- a/widget/cocoa/VibrancyManager.h
+++ b/widget/cocoa/VibrancyManager.h
@@ -72,24 +72,16 @@ class VibrancyManager {
    * @param aRegion The vibrant area, in device pixels.
    */
   void UpdateVibrantRegion(VibrancyType aType,
                            const LayoutDeviceIntRegion& aRegion);
 
   bool HasVibrantRegions() { return !mVibrantRegions.IsEmpty(); }
 
   /**
-   * Clear the vibrant areas that we know about.
-   * The clearing happens in the current NSGraphicsContext. If you call this
-   * from within an -[NSView drawRect:] implementation, the currrent
-   * NSGraphicsContext is already correctly set to the window drawing context.
-   */
-  void ClearVibrantAreas() const;
-
-  /**
    * Return the fill color that should be drawn on top of the cleared window
    * parts. Usually this would be drawn by -[NSVisualEffectView drawRect:].
    * The returned color is opaque if the system-wide "Reduce transparency"
    * preference is set.
    */
   NSColor* VibrancyFillColorForType(VibrancyType aType);
 
   /**
@@ -107,18 +99,16 @@ class VibrancyManager {
    * @param aIsContainer Whether this NSView will have child views. This value
    *                     affects hit testing: Container views will pass through
    *                     hit testing requests to their children, and leaf views
    *                     will be transparent to hit testing.
    */
   static NSView* CreateEffectView(VibrancyType aType, BOOL aIsContainer = NO);
 
  protected:
-  void ClearVibrantRegion(const LayoutDeviceIntRegion& aVibrantRegion) const;
-
   const nsChildView& mCoordinateConverter;
   NSView* mContainerView;
   nsClassHashtable<nsUint32HashKey, ViewRegion> mVibrantRegions;
 };
 
 }  // namespace mozilla
 
 #endif  // VibrancyManager_h
diff --git a/widget/cocoa/VibrancyManager.mm b/widget/cocoa/VibrancyManager.mm
--- a/widget/cocoa/VibrancyManager.mm
+++ b/widget/cocoa/VibrancyManager.mm
@@ -19,34 +19,16 @@ VibrancyManager::UpdateVibrantRegion(Vib
     return;
   }
   auto& vr = *mVibrantRegions.LookupOrAdd(uint32_t(aType));
   vr.UpdateRegion(aRegion, mCoordinateConverter, mContainerView, ^() {
     return this->CreateEffectView(aType);
   });
 }
 
-void
-VibrancyManager::ClearVibrantAreas() const
-{
-  for (auto iter = mVibrantRegions.ConstIter(); !iter.Done(); iter.Next()) {
-    ClearVibrantRegion(iter.UserData()->Region());
-  }
-}
-
-void
-VibrancyManager::ClearVibrantRegion(const LayoutDeviceIntRegion& aVibrantRegion) const
-{
-  [[NSColor clearColor] set];
-
-  for (auto iter = aVibrantRegion.RectIter(); !iter.Done(); iter.Next()) {
-    NSRectFill(mCoordinateConverter.DevPixelsToCocoaPoints(iter.Get()));
-  }
-}
-
 @interface NSView(CurrentFillColor)
 - (NSColor*)_currentFillColor;
 @end
 
 static NSColor*
 AdjustedColor(NSColor* aFillColor, VibrancyType aType)
 {
   if (aType == VibrancyType::MENU && [aFillColor alphaComponent] == 1.0) {
@@ -60,69 +42,52 @@ AdjustedColor(NSColor* aFillColor, Vibra
 }
 
 NSColor*
 VibrancyManager::VibrancyFillColorForType(VibrancyType aType)
 {
   NSView* view = mVibrantRegions.LookupOrAdd(uint32_t(aType))->GetAnyView();
 
   if (view && [view respondsToSelector:@selector(_currentFillColor)]) {
-    // -[NSVisualEffectView _currentFillColor] is the color that our view
-    // would draw during its drawRect implementation, if we hadn't
-    // disabled that.
+    // -[NSVisualEffectView _currentFillColor] is the color that the view
+    // draws in its drawRect implementation.
     return AdjustedColor([view _currentFillColor], aType);
   }
   return [NSColor whiteColor];
 }
 
-static void
-DrawRectNothing(id self, SEL _cmd, NSRect aRect)
-{
-  // The super implementation would clear the background.
-  // That's fine for views that are placed below their content, but our
-  // setup is different: Our drawn content is drawn to mContainerView, which
-  // sits below this EffectView. So we must not clear the background here,
-  // because we'd erase that drawn content.
-  // Of course the regular content drawing still needs to clear the background
-  // behind vibrant areas. This is taken care of by having nsNativeThemeCocoa
-  // return true from NeedToClearBackgroundBehindWidget for vibrant widgets.
-}
-
 static NSView*
 HitTestNil(id self, SEL _cmd, NSPoint aPoint)
 {
   // This view must be transparent to mouse events.
   return nil;
 }
 
 static BOOL
 AllowsVibrancyYes(id self, SEL _cmd)
 {
   // Means that the foreground is blended using a vibrant blend mode.
   return YES;
 }
 
 static Class
-CreateEffectViewClass(BOOL aForegroundVibrancy, BOOL aIsContainer)
-{
-  // Create a class called EffectView that inherits from NSVisualEffectView
-  // and overrides the methods -[NSVisualEffectView drawRect:] and
-  // -[NSView hitTest:].
+CreateEffectViewClass(BOOL aForegroundVibrancy, BOOL aIsContainer) {
+  // Create a class that inherits from NSVisualEffectView and overrides the
+  // methods -[NSView hitTest:] and  -[NSVisualEffectView allowsVibrancy].
   Class NSVisualEffectViewClass = NSClassFromString(@"NSVisualEffectView");
   const char* className = aForegroundVibrancy
     ? "EffectViewWithForegroundVibrancy" : "EffectViewWithoutForegroundVibrancy";
   Class EffectViewClass = objc_allocateClassPair(NSVisualEffectViewClass, className, 0);
-  class_addMethod(EffectViewClass, @selector(drawRect:), (IMP)DrawRectNothing,
-                  "v@:{CGRect={CGPoint=dd}{CGSize=dd}}");
   if (!aIsContainer) {
+    // Make this view transparent to mouse events.
     class_addMethod(EffectViewClass, @selector(hitTest:), (IMP)HitTestNil,
                     "@@:{CGPoint=dd}");
   }
   if (aForegroundVibrancy) {
-    // Also override the -[NSView allowsVibrancy] method to return YES.
+    // Override the -[NSView allowsVibrancy] method to return YES.
     class_addMethod(EffectViewClass, @selector(allowsVibrancy), (IMP)AllowsVibrancyYes, "I@:");
   }
   return EffectViewClass;
 }
 
 static id
 AppearanceForVibrancyType(VibrancyType aType)
 {
diff --git a/widget/cocoa/nsChildView.h b/widget/cocoa/nsChildView.h
--- a/widget/cocoa/nsChildView.h
+++ b/widget/cocoa/nsChildView.h
@@ -472,17 +472,16 @@ class nsChildView final : public nsBaseW
   NSView<mozView>* GetEditorView();
 
   nsCocoaWindow* GetXULWindowWidget() const;
 
   virtual void ReparentNativeWidget(nsIWidget* aNewParent) override;
 
   mozilla::widget::TextInputHandler* GetTextInputHandler() { return mTextInputHandler; }
 
-  void ClearVibrantAreas();
   NSColor* VibrancyFillColorForThemeGeometryType(nsITheme::ThemeGeometryType aThemeGeometryType);
   NSColor* VibrancyFontSmoothingBackgroundColorForThemeGeometryType(
       nsITheme::ThemeGeometryType aThemeGeometryType);
 
   // unit conversion convenience functions
   int32_t CocoaPointsToDevPixels(CGFloat aPts) const {
     return nsCocoaUtils::CocoaPointsToDevPixels(aPts, BackingScaleFactor());
   }
diff --git a/widget/cocoa/nsChildView.mm b/widget/cocoa/nsChildView.mm
--- a/widget/cocoa/nsChildView.mm
+++ b/widget/cocoa/nsChildView.mm
@@ -2612,24 +2612,16 @@ nsChildView::UpdateVibrancy(const nsTArr
   vm.UpdateVibrantRegion(VibrancyType::TOOLTIP, tooltipRegion);
   vm.UpdateVibrantRegion(VibrancyType::HIGHLIGHTED_MENUITEM, highlightedMenuItemRegion);
   vm.UpdateVibrantRegion(VibrancyType::SHEET, sheetRegion);
   vm.UpdateVibrantRegion(VibrancyType::SOURCE_LIST, sourceListRegion);
   vm.UpdateVibrantRegion(VibrancyType::SOURCE_LIST_SELECTION, sourceListSelectionRegion);
   vm.UpdateVibrantRegion(VibrancyType::ACTIVE_SOURCE_LIST_SELECTION, activeSourceListSelectionRegion);
 }
 
-void
-nsChildView::ClearVibrantAreas()
-{
-  if (VibrancyManager::SystemSupportsVibrancy()) {
-    EnsureVibrancyManager().ClearVibrantAreas();
-  }
-}
-
 NSColor*
 nsChildView::VibrancyFillColorForThemeGeometryType(nsITheme::ThemeGeometryType aThemeGeometryType)
 {
   if (VibrancyManager::SystemSupportsVibrancy()) {
     Maybe<VibrancyType> vibrancyType =
       ThemeGeometryTypeToVibrancyType(aThemeGeometryType);
     MOZ_RELEASE_ASSERT(vibrancyType, "should only encounter vibrant theme geometry types here");
     return EnsureVibrancyManager().VibrancyFillColorForType(*vibrancyType);
@@ -3512,21 +3504,16 @@ NSEvent* gLastDragMouseDownEvent = nil;
 
 // Make the origin of this view the topLeft corner (gecko origin) rather
 // than the bottomLeft corner (standard cocoa origin).
 - (BOOL)isFlipped
 {
   return YES;
 }
 
-- (BOOL)isOpaque
-{
-  return [[self window] isOpaque];
-}
-
 // We accept key and mouse events, so don't keep passing them up the chain. Allow
 // this to be a 'focused' widget for event dispatch.
 - (BOOL)acceptsFirstResponder
 {
   return YES;
 }
 
 // Accept mouse down events on background windows
@@ -3646,25 +3633,16 @@ NSEvent* gLastDragMouseDownEvent = nil;
 
   if (!mGeckoChild || !mGeckoChild->IsVisible())
     return;
 
   CGContextRef cgContext =
     (CGContextRef)[[NSGraphicsContext currentContext] graphicsPort];
 
   if ([self isUsingOpenGL]) {
-    // Since this view is usually declared as opaque, the window's pixel
-    // buffer may now contain garbage which we need to prevent from reaching
-    // the screen. The only place where garbage can show is in the window
-    // corners and the vibrant regions of the window - the rest of the window
-    // is covered by opaque content in our OpenGL surface.
-    // So we need to clear the pixel buffer contents in these areas.
-    mGeckoChild->ClearVibrantAreas();
-    [self clearCorners];
-
     // Force a sync OMTC composite into the OpenGL context and return.
     LayoutDeviceIntRect geckoBounds = mGeckoChild->GetBounds();
     LayoutDeviceIntRegion region(geckoBounds);
     mGeckoChild->PaintWindow(region);
     return;
   }
 
   AUTO_PROFILER_LABEL("ChildView::drawRect", GRAPHICS);
@@ -3728,46 +3706,16 @@ NSEvent* gLastDragMouseDownEvent = nil;
   CGLLockContext((CGLContextObj)[mGLContext CGLContextObj]);
   // Make the context opaque for fullscreen (since it performs better), and transparent
   // for windowed (since we need it for rounded corners).
   GLint opaque = aOpaque ? 1 : 0;
   [mGLContext setValues:&opaque forParameter:NSOpenGLCPSurfaceOpacity];
   CGLUnlockContext((CGLContextObj)[mGLContext CGLContextObj]);
 }
 
-// Accelerated windows have two NSSurfaces:
-//  (1) The window's pixel buffer in the back and
-//  (2) the OpenGL view in the front.
-// These two surfaces are composited by the window manager. Drawing into the
-// CGContext which is provided by drawRect ends up in (1).
-// When our window has rounded corners, the OpenGL view has transparent pixels
-// in the corners. In these places the contents of the window's pixel buffer
-// can show through. So we need to make sure that the pixel buffer is
-// transparent in the corners so that no garbage reaches the screen.
-// The contents of the pixel buffer in the rest of the window don't matter
-// because they're covered by opaque pixels of the OpenGL context.
-// Making the corners transparent works even though our window is
-// declared "opaque" (in the NSWindow's isOpaque method).
-- (void)clearCorners
-{
-  CGFloat radius = [self cornerRadius];
-  CGFloat w = [self bounds].size.width, h = [self bounds].size.height;
-  [[NSColor clearColor] set];
-
-  if ([self isCoveringTitlebar]) {
-    NSRectFill(NSMakeRect(0, 0, radius, radius));
-    NSRectFill(NSMakeRect(w - radius, 0, radius, radius));
-  }
-
-  if ([self hasRoundedBottomCorners]) {
-    NSRectFill(NSMakeRect(0, h - radius, radius, radius));
-    NSRectFill(NSMakeRect(w - radius, h - radius, radius, radius));
-  }
-}
-
 // This is the analog of nsChildView::MaybeDrawRoundedCorners for CGContexts.
 // We only need to mask the top corners here because Cocoa does the masking
 // for the window's bottom corners automatically (starting with 10.7).
 - (void)maskTopCornersInContext:(CGContextRef)aContext
 {
   CGFloat radius = [self cornerRadius];
   int32_t devPixelCornerRadius = mGeckoChild->CocoaPointsToDevPixels(radius);
 
