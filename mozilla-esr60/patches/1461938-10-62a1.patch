# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1526994053 -7200
# Node ID 4bceed84e6393e669e8c58091a4faa94a7bbe63c
# Parent  4b2d782cad5b59f6ccb9c8ef28665a303bf132a2
Bug 1461938 part 10 - Move {maps,sets}WithNurseryMemory from JSCompartment to Nursery. r=jonco

diff --git a/js/src/builtin/MapObject.cpp b/js/src/builtin/MapObject.cpp
--- a/js/src/builtin/MapObject.cpp
+++ b/js/src/builtin/MapObject.cpp
@@ -204,17 +204,17 @@ MapIteratorObject* MapIteratorObject::cr
     // Range. Try again in the tenured heap.
     MOZ_ASSERT(objectKind == GenericObject);
     objectKind = TenuredObject;
   }
 
   bool insideNursery = IsInsideNursery(iterobj);
   MOZ_ASSERT(insideNursery == nursery.isInside(buffer));
   if (insideNursery && !HasNurseryMemory(mapobj.get())) {
-    if (!cx->compartment()->addMapWithNurseryMemory(mapobj)) {
+    if (!cx->nursery().addMapWithNurseryMemory(mapobj)) {
       ReportOutOfMemory(cx);
       return nullptr;
     }
     SetHasNurseryMemory(mapobj.get(), true);
   }
 
   auto range = data->createRange(buffer, insideNursery);
   iterobj->setSlot(RangeSlot, PrivateValue(range));
@@ -540,17 +540,17 @@ MapObject* MapObject::create(JSContext* 
     ReportOutOfMemory(cx);
     return nullptr;
   }
 
   MapObject* mapObj = NewObjectWithClassProto<MapObject>(cx, proto);
   if (!mapObj) return nullptr;
 
   bool insideNursery = IsInsideNursery(mapObj);
-  if (insideNursery && !cx->compartment()->addMapWithNurseryMemory(mapObj)) {
+  if (insideNursery && !cx->nursery().addMapWithNurseryMemory(mapObj)) {
     ReportOutOfMemory(cx);
     return nullptr;
   }
 
   mapObj->initPrivate(map.release());
   mapObj->initReservedSlot(NurseryKeysSlot, PrivateValue(nullptr));
   mapObj->initReservedSlot(HasNurseryMemorySlot,
                            JS::BooleanValue(insideNursery));
@@ -906,17 +906,17 @@ SetIteratorObject* SetIteratorObject::cr
     // Range. Try again in the tenured heap.
     MOZ_ASSERT(objectKind == GenericObject);
     objectKind = TenuredObject;
   }
 
   bool insideNursery = IsInsideNursery(iterobj);
   MOZ_ASSERT(insideNursery == nursery.isInside(buffer));
   if (insideNursery && !HasNurseryMemory(setobj.get())) {
-    if (!cx->compartment()->addSetWithNurseryMemory(setobj)) {
+    if (!cx->nursery().addSetWithNurseryMemory(setobj)) {
       ReportOutOfMemory(cx);
       return nullptr;
     }
     SetHasNurseryMemory(setobj.get(), true);
   }
 
   auto range = data->createRange(buffer, insideNursery);
   iterobj->setSlot(RangeSlot, PrivateValue(range));
@@ -1098,17 +1098,17 @@ SetObject* SetObject::create(JSContext* 
     ReportOutOfMemory(cx);
     return nullptr;
   }
 
   SetObject* obj = NewObjectWithClassProto<SetObject>(cx, proto);
   if (!obj) return nullptr;
 
   bool insideNursery = IsInsideNursery(obj);
-  if (insideNursery && !cx->compartment()->addSetWithNurseryMemory(obj)) {
+  if (insideNursery && !cx->nursery().addSetWithNurseryMemory(obj)) {
     ReportOutOfMemory(cx);
     return nullptr;
   }
 
   obj->initPrivate(set.release());
   obj->initReservedSlot(NurseryKeysSlot, PrivateValue(nullptr));
   obj->initReservedSlot(HasNurseryMemorySlot, JS::BooleanValue(insideNursery));
   return obj;
diff --git a/js/src/gc/Nursery.cpp b/js/src/gc/Nursery.cpp
--- a/js/src/gc/Nursery.cpp
+++ b/js/src/gc/Nursery.cpp
@@ -931,16 +931,17 @@ void js::Nursery::sweep(JSTracer* trc) {
     }
   }
   cellsWithUid_.clear();
 
   for (CompartmentsIter c(runtime(), SkipAtoms); !c.done(); c.next())
     c->sweepAfterMinorGC(trc);
 
   sweepDictionaryModeObjects();
+  sweepMapAndSetObjects();
 }
 
 void js::Nursery::clear() {
 #if defined(JS_GC_ZEAL) || defined(JS_CRASH_DIAGNOSTICS)
   /* Poison the nursery contents so touching a freed object will crash. */
   for (unsigned i = currentStartChunk_; i < allocatedChunkCount(); ++i)
     chunk(i).poisonAfterSweep();
 #endif
@@ -1114,16 +1115,28 @@ void js::Nursery::sweepDictionaryModeObj
     if (!IsForwarded(obj))
       obj->sweepDictionaryListPointer();
     else
       Forwarded(obj)->updateDictionaryListPointerAfterMinorGC(obj);
   }
   dictionaryModeObjects_.clear();
 }
 
+void js::Nursery::sweepMapAndSetObjects() {
+  auto fop = runtime_->defaultFreeOp();
+
+  for (auto mapobj : mapsWithNurseryMemory_)
+    MapObject::sweepAfterMinorGC(fop, mapobj);
+  mapsWithNurseryMemory_.clearAndFree();
+
+  for (auto setobj : setsWithNurseryMemory_)
+    SetObject::sweepAfterMinorGC(fop, setobj);
+  setsWithNurseryMemory_.clearAndFree();
+}
+
 JS_PUBLIC_API void JS::EnableNurseryStrings(JSContext* cx) {
   AutoEmptyNursery empty(cx);
   ReleaseAllJITCode(cx->runtime()->defaultFreeOp());
   cx->runtime()->gc.nursery().enableStrings();
 }
 
 JS_PUBLIC_API void JS::DisableNurseryStrings(JSContext* cx) {
   AutoEmptyNursery empty(cx);
diff --git a/js/src/gc/Nursery.h b/js/src/gc/Nursery.h
--- a/js/src/gc/Nursery.h
+++ b/js/src/gc/Nursery.h
@@ -47,16 +47,18 @@ namespace js {
 class AutoLockGCBgAlloc;
 class ObjectElements;
 class PlainObject;
 class NativeObject;
 class Nursery;
 struct NurseryChunk;
 class HeapSlot;
 class JSONPrinter;
+class MapObject;
+class SetObject;
 
 void SetGCZeal(JSRuntime*, uint8_t, uint32_t);
 
 namespace gc {
 class AutoMaybeStartBackgroundAllocation;
 struct Cell;
 class MinorCollectionTracer;
 class RelocationOverlay;
@@ -341,16 +343,27 @@ class Nursery {
   void clearMinorGCRequest() {
     minorGCTriggerReason_ = JS::gcreason::NO_REASON;
   }
 
   bool needIdleTimeCollection() const;
 
   bool enableProfiling() const { return enableProfiling_; }
 
+  bool addMapWithNurseryMemory(MapObject* obj) {
+    MOZ_ASSERT_IF(!mapsWithNurseryMemory_.empty(),
+                  mapsWithNurseryMemory_.back() != obj);
+    return mapsWithNurseryMemory_.append(obj);
+  }
+  bool addSetWithNurseryMemory(SetObject* obj) {
+    MOZ_ASSERT_IF(!setsWithNurseryMemory_.empty(),
+                  setsWithNurseryMemory_.back() != obj);
+    return setsWithNurseryMemory_.append(obj);
+  }
+
   /* The amount of space in the mapped nursery available to allocations. */
   static const size_t NurseryChunkUsableSize =
       gc::ChunkSize - gc::ChunkTrailerSize;
 
  private:
   JSRuntime* runtime_;
 
   /* Vector of allocated chunks to allocate from. */
@@ -490,16 +503,23 @@ class Nursery {
    *       stable object hashing and we have to break the cycle somehow.
    */
   using CellsWithUniqueIdVector = Vector<gc::Cell*, 8, SystemAllocPolicy>;
   CellsWithUniqueIdVector cellsWithUid_;
 
   using NativeObjectVector = Vector<NativeObject*, 0, SystemAllocPolicy>;
   NativeObjectVector dictionaryModeObjects_;
 
+  /*
+   * Lists of map and set objects allocated in the nursery or with iterators
+   * allocated there. Such objects need to be swept after minor GC.
+   */
+  Vector<MapObject*, 0, SystemAllocPolicy> mapsWithNurseryMemory_;
+  Vector<SetObject*, 0, SystemAllocPolicy> setsWithNurseryMemory_;
+
 #ifdef JS_GC_ZEAL
   struct Canary;
   Canary* lastCanary_;
 #endif
 
   NurseryChunk& chunk(unsigned index) const { return *chunks_[index]; }
 
   void setCurrentChunk(unsigned chunkno);
@@ -554,16 +574,17 @@ class Nursery {
 
   /*
    * Frees all non-live nursery-allocated things at the end of a minor
    * collection.
    */
   void clear();
 
   void sweepDictionaryModeObjects();
+  void sweepMapAndSetObjects();
 
   /* Change the allocable space provided by the nursery. */
   void maybeResizeNursery(JS::gcreason::Reason reason);
   void growAllocableSpace();
   void shrinkAllocableSpace(unsigned newCount);
   void minimizeAllocableSpace();
 
   // Free the chunks starting at firstFreeChunk until the end of the chunks
diff --git a/js/src/vm/JSCompartment.cpp b/js/src/vm/JSCompartment.cpp
--- a/js/src/vm/JSCompartment.cpp
+++ b/js/src/vm/JSCompartment.cpp
@@ -684,17 +684,16 @@ void Realm::finishRoots() {
 void JSCompartment::sweepAfterMinorGC(JSTracer* trc) {
   globalWriteBarriered = 0;
 
   InnerViewTable& table = innerViews.get();
   if (table.needsSweepAfterMinorGC()) table.sweepAfterMinorGC();
 
   crossCompartmentWrappers.sweepAfterMinorGC(trc);
   dtoaCache.purge();
-  sweepMapAndSetObjectsAfterMinorGC();
 }
 
 void JSCompartment::sweepSavedStacks() { savedStacks_.sweep(); }
 
 void Realm::sweepGlobalObject() {
   if (global_ && IsAboutToBeFinalized(&global_)) global_.set(nullptr);
 }
 
@@ -739,28 +738,16 @@ void JSCompartment::sweepNativeIterators
  * markCrossCompartmentWrappers.
  */
 void JSCompartment::sweepCrossCompartmentWrappers() {
   crossCompartmentWrappers.sweep();
 }
 
 void Realm::sweepVarNames() { varNames_.sweep(); }
 
-void JSCompartment::sweepMapAndSetObjectsAfterMinorGC() {
-  auto fop = runtime_->defaultFreeOp();
-
-  for (auto mapobj : mapsWithNurseryMemory)
-    MapObject::sweepAfterMinorGC(fop, mapobj);
-  mapsWithNurseryMemory.clearAndFree();
-
-  for (auto setobj : setsWithNurseryMemory)
-    SetObject::sweepAfterMinorGC(fop, setobj);
-  setsWithNurseryMemory.clearAndFree();
-}
-
 namespace {
 struct TraceRootFunctor {
   JSTracer* trc;
   const char* name;
   TraceRootFunctor(JSTracer* trc, const char* name) : trc(trc), name(name) {}
   template <class T>
   void operator()(T* t) {
     return TraceRoot(trc, t, name);
diff --git a/js/src/vm/JSCompartment.h b/js/src/vm/JSCompartment.h
--- a/js/src/vm/JSCompartment.h
+++ b/js/src/vm/JSCompartment.h
@@ -847,17 +847,16 @@ struct JSCompartment {
    * called in per-zone GCs to prevent the wrappers' outgoing edges from
    * dangling (full GCs naturally follow pointers across compartments) and
    * when compacting to update cross-compartment pointers.
    */
   void traceOutgoingCrossCompartmentWrappers(JSTracer* trc);
   static void traceIncomingCrossCompartmentEdgesForZoneGC(JSTracer* trc);
 
   void sweepAfterMinorGC(JSTracer* trc);
-  void sweepMapAndSetObjectsAfterMinorGC();
 
   void sweepCrossCompartmentWrappers();
   void sweepSavedStacks();
   void sweepSelfHostingScriptSource();
   void sweepJitCompartment();
   void sweepRegExps();
   void sweepDebugEnvironments();
   void sweepNativeIterators();
@@ -1074,36 +1073,16 @@ struct JSCompartment {
 
   static const size_t IterResultObjectValueSlot = 0;
   static const size_t IterResultObjectDoneSlot = 1;
   js::NativeObject* getOrCreateIterResultTemplateObject(JSContext* cx);
 
   // Aggregated output used to collect JSScript hit counts when code coverage
   // is enabled.
   js::coverage::LCovCompartment lcovOutput;
-
-  bool addMapWithNurseryMemory(js::MapObject* obj) {
-    MOZ_ASSERT_IF(!mapsWithNurseryMemory.empty(),
-                  mapsWithNurseryMemory.back() != obj);
-    return mapsWithNurseryMemory.append(obj);
-  }
-
-  bool addSetWithNurseryMemory(js::SetObject* obj) {
-    MOZ_ASSERT_IF(!setsWithNurseryMemory.empty(),
-                  setsWithNurseryMemory.back() != obj);
-    return setsWithNurseryMemory.append(obj);
-  }
-
- private:
-  /*
-   * Lists of map and set objects allocated in the nursery or with iterators
-   * allocated there. Such objects need to be swept after minor GC.
-   */
-  js::Vector<js::MapObject*, 0, js::SystemAllocPolicy> mapsWithNurseryMemory;
-  js::Vector<js::SetObject*, 0, js::SystemAllocPolicy> setsWithNurseryMemory;
 };
 
 class JS::Realm : public JSCompartment {
   const JS::RealmCreationOptions creationOptions_;
   JS::RealmBehaviors behaviors_;
 
   friend struct ::JSContext;
   js::ReadBarrieredGlobalObject global_;
