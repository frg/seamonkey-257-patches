# HG changeset patch
# User Jeff Walden <jwalden@mit.edu>
# Date 1530225554 25200
# Node ID dad92f9be9b723945131b9065d30333de2f0cd13
# Parent  aece599612767397ca4dc1e32d3e95bdded91ac8
Bug 1472031 - Avoid copying potential directives in TokenStreamSpecific::getDirective.  r=arai

diff --git a/js/src/frontend/TokenStream.cpp b/js/src/frontend/TokenStream.cpp
--- a/js/src/frontend/TokenStream.cpp
+++ b/js/src/frontend/TokenStream.cpp
@@ -1020,27 +1020,16 @@ bool GeneralTokenStreamChars<CharT, AnyC
 
     sourceUnits.unskipCodeUnits(length);
   }
 
   MOZ_ASSERT(sourceUnits.previousCodeUnit() == '\\');
   return false;
 }
 
-// Helper function which returns true if the first length(q) characters in p are
-// the same as the characters in q.
-template <typename CharT>
-static bool CharsMatch(const CharT* p, const char* q) {
-  while (*q) {
-    if (*p++ != *q++) return false;
-  }
-
-  return true;
-}
-
 template <typename CharT, class AnyCharsAccess>
 bool TokenStreamSpecific<CharT, AnyCharsAccess>::getDirectives(
     bool isMultiline, bool shouldWarnDeprecated) {
   // Match directive comments used in debugging, such as "//# sourceURL" and
   // "//# sourceMappingURL". Use of "//@" instead of "//#" is deprecated.
   //
   // To avoid a crashing bug in IE, several JavaScript transpilers wrap single
   // line comments containing a source mapping URL inside a multiline
@@ -1067,33 +1056,26 @@ MOZ_MUST_USE bool TokenStreamCharsShared
   return true;
 }
 
 template <typename CharT, class AnyCharsAccess>
 MOZ_MUST_USE bool TokenStreamSpecific<CharT, AnyCharsAccess>::getDirective(
     bool isMultiline, bool shouldWarnDeprecated, const char* directive,
     uint8_t directiveLength, const char* errorMsgPragma,
     UniquePtr<char16_t[], JS::FreePolicy>* destination) {
-  MOZ_ASSERT(directiveLength <= 18);
-  char16_t peeked[18];
-
-  // If there aren't enough code units left, it can't be the desired
-  // directive.  (Note that |directive| must be ASCII, so there are no
-  // tricky encoding issues to consider.)
-  if (!sourceUnits.peekCodeUnits(directiveLength, peeked))
-    return true;
-
-  // It's also not the desired directive if the code units don't match.
-  if (!CharsMatch(peeked, directive)) return true;
+  // Stop if we don't find |directive|.  (Note that |directive| must be
+  // ASCII, so there are no tricky encoding issues to consider in matching
+  // UTF-8/16-agnostically.)
+  if (!sourceUnits.matchCodeUnits(directive, directiveLength))
+     return true;
 
   if (shouldWarnDeprecated) {
     if (!warning(JSMSG_DEPRECATED_PRAGMA, errorMsgPragma)) return false;
   }
 
-  sourceUnits.skipCodeUnits(directiveLength);
   charBuffer.clear();
 
   do {
     int32_t unit = peekCodeUnit();
     if (unit == EOF)
       break;
 
     if (MOZ_LIKELY(isAsciiCodePoint(unit))) {
diff --git a/js/src/frontend/TokenStream.h b/js/src/frontend/TokenStream.h
--- a/js/src/frontend/TokenStream.h
+++ b/js/src/frontend/TokenStream.h
@@ -951,16 +951,33 @@ class SourceUnits {
     MOZ_ASSERT(ptr, "shouldn't peek into poisoned SourceUnits");
     if (n > remaining())
       return false;
 
     std::copy_n(ptr, n, out);
     return true;
   }
 
+  bool matchCodeUnits(const char* chars, uint8_t length) {
+    MOZ_ASSERT(ptr, "shouldn't match into poisoned SourceUnits");
+    if (length > remaining())
+      return false;
+
+    const CharT* start = ptr;
+    const CharT* end = ptr + length;
+    while (ptr < end) {
+      if (*ptr++ != CharT(*chars++)) {
+        ptr = start;
+        return false;
+      }
+    }
+
+    return true;
+  }
+
   void skipCodeUnits(uint32_t n) {
     MOZ_ASSERT(ptr, "shouldn't use poisoned SourceUnits");
     MOZ_ASSERT(n <= remaining(), "shouldn't skip beyond end of SourceUnits");
     ptr += n;
   }
 
   void unskipCodeUnits(uint32_t n) {
     MOZ_ASSERT(ptr, "shouldn't use poisoned SourceUnits");
