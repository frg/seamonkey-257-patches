# HG changeset patch
# User Lars T Hansen <lhansen@mozilla.com>
# Date 1520945761 -3600
# Node ID 9a9390557aabdbca55af4a48c2183d47a7e0581a
# Parent  f6bca0fd82a990f4fdc387318f3584b380e9ffd9
Bug 1333018 - Clean up by-ref and by-pointer arguments.  r=bbouvier

diff --git a/js/src/wasm/WasmBaselineCompile.cpp b/js/src/wasm/WasmBaselineCompile.cpp
--- a/js/src/wasm/WasmBaselineCompile.cpp
+++ b/js/src/wasm/WasmBaselineCompile.cpp
@@ -1033,17 +1033,17 @@ class BaseStackFrame {
   // necessary alignment.
 
   uint32_t initialSize() const {
     MOZ_ASSERT(localSize_ != UINT32_MAX);
 
     return localSize_;
   }
 
-  void zeroLocals(BaseRegAlloc& ra);
+  void zeroLocals(BaseRegAlloc* ra);
 
   void loadLocalI32(const Local& src, RegI32 dest) {
     masm.load32(Address(sp_, localOffset(src)), dest);
   }
 
 #ifndef JS_PUNBOX64
   void loadLocalI64Low(const Local& src, RegI32 dest) {
     masm.load32(Address(sp_, localOffset(src) + INT64LOW_OFFSET), dest);
@@ -1280,17 +1280,17 @@ class BaseStackFrame {
   }
 
   // This is always equivalent to a sequence of masm.freeStack() calls.
   void freeArgAreaAndPopBytes(size_t argSize, size_t dropSize) {
     if (argSize + dropSize) masm.freeStack(argSize + dropSize);
   }
 };
 
-void BaseStackFrame::zeroLocals(BaseRegAlloc& ra) {
+void BaseStackFrame::zeroLocals(BaseRegAlloc* ra) {
   MOZ_ASSERT(varLow_ != UINT32_MAX);
 
   if (varLow_ == varHigh_) return;
 
   static const uint32_t wordSize = sizeof(void*);
 
   // The adjustments to 'low' by the size of the item being stored compensates
   // for the fact that locals offsets are the offsets from Frame to the bytes
@@ -1326,64 +1326,64 @@ void BaseStackFrame::zeroLocals(BaseRegA
   }
 
   // For other cases, it's best to have a zero in a register.
   //
   // One can do more here with SIMD registers (store 16 bytes at a time) or
   // with instructions like STRD on ARM (store 8 bytes at a time), but that's
   // for another day.
 
-  RegI32 zero = ra.needI32();
+  RegI32 zero = ra->needI32();
   masm.mov(ImmWord(0), zero);
 
   // For the general case we want to have a loop body of UNROLL_LIMIT stores
   // and then a tail of less than UNROLL_LIMIT stores.  When initWords is less
   // than 2*UNROLL_LIMIT the loop trip count is at most 1 and there is no
   // benefit to having the pointer calculations and the compare-and-branch.
   // So we completely unroll when we have initWords < 2 * UNROLL_LIMIT.  (In
   // this case we'll end up using 32-bit offsets on x64 for up to half of the
   // stores, though.)
 
   // Fully-unrolled case.
 
   if (initWords < 2 * UNROLL_LIMIT) {
     for (uint32_t i = low; i < high; i += wordSize)
       masm.storePtr(zero, Address(sp_, localOffset(i + wordSize)));
-    ra.freeI32(zero);
+    ra->freeI32(zero);
     return;
   }
 
   // Unrolled loop with a tail. Stores will use negative offsets. That's OK
   // for x86 and ARM, at least.
 
   // Compute pointer to the highest-addressed slot on the frame.
-  RegI32 p = ra.needI32();
+  RegI32 p = ra->needI32();
   masm.computeEffectiveAddress(Address(sp_, localOffset(low + wordSize)), p);
 
   // Compute pointer to the lowest-addressed slot on the frame that will be
   // initialized by the loop body.
-  RegI32 lim = ra.needI32();
+  RegI32 lim = ra->needI32();
   masm.computeEffectiveAddress(Address(sp_, localOffset(loopHigh + wordSize)),
                                lim);
 
   // The loop body.  Eventually we'll have p == lim and exit the loop.
   Label again;
   masm.bind(&again);
   for (uint32_t i = 0; i < UNROLL_LIMIT; ++i)
     masm.storePtr(zero, Address(p, -(wordSize * i)));
   masm.subPtr(Imm32(UNROLL_LIMIT * wordSize), p);
   masm.branchPtr(Assembler::LessThan, lim, p, &again);
 
   // The tail.
   for (uint32_t i = 0; i < tailWords; ++i)
     masm.storePtr(zero, Address(p, -(wordSize * i)));
 
-  ra.freeI32(p);
-  ra.freeI32(lim);
-  ra.freeI32(zero);
+  ra->freeI32(p);
+  ra->freeI32(lim);
+  ra->freeI32(zero);
 }
 
 // The baseline compiler proper.
 
 class BaseCompiler final : public BaseCompilerInterface {
   typedef BaseStackFrame::Local Local;
   typedef Vector<NonAssertingLabel, 8, SystemAllocPolicy> LabelVector;
   typedef Vector<MIRType, 8, SystemAllocPolicy> MIRTypeVector;
@@ -1932,65 +1932,85 @@ class BaseCompiler final : public BaseCo
 
   Vector<Stk, 8, SystemAllocPolicy> stk_;
 
   Stk& push() {
     stk_.infallibleEmplaceBack(Stk());
     return stk_.back();
   }
 
-  void loadConstI32(Stk& src, RegI32 dest) { moveImm32(src.i32val(), dest); }
-
-  void loadMemI32(Stk& src, RegI32 dest) { fr.loadStackI32(src.offs(), dest); }
-
-  void loadLocalI32(Stk& src, RegI32 dest) {
+  void loadConstI32(const Stk& src, RegI32 dest) {
+    moveImm32(src.i32val(), dest);
+  }
+
+  void loadMemI32(const Stk& src, RegI32 dest) {
+    fr.loadStackI32(src.offs(), dest);
+  }
+
+  void loadLocalI32(const Stk& src, RegI32 dest) {
     fr.loadLocalI32(localFromSlot(src.slot(), MIRType::Int32), dest);
   }
 
-  void loadRegisterI32(Stk& src, RegI32 dest) { moveI32(src.i32reg(), dest); }
-
-  void loadConstI64(Stk& src, RegI64 dest) { moveImm64(src.i64val(), dest); }
-
-  void loadMemI64(Stk& src, RegI64 dest) { fr.loadStackI64(src.offs(), dest); }
-
-  void loadLocalI64(Stk& src, RegI64 dest) {
+  void loadRegisterI32(const Stk& src, RegI32 dest) {
+    moveI32(src.i32reg(), dest);
+  }
+
+  void loadConstI64(const Stk& src, RegI64 dest) {
+    moveImm64(src.i64val(), dest);
+  }
+
+  void loadMemI64(const Stk& src, RegI64 dest) {
+    fr.loadStackI64(src.offs(), dest);
+  }
+
+  void loadLocalI64(const Stk& src, RegI64 dest) {
     fr.loadLocalI64(localFromSlot(src.slot(), MIRType::Int64), dest);
   }
 
-  void loadRegisterI64(Stk& src, RegI64 dest) { moveI64(src.i64reg(), dest); }
-
-  void loadConstF64(Stk& src, RegF64 dest) {
+  void loadRegisterI64(const Stk& src, RegI64 dest) {
+    moveI64(src.i64reg(), dest);
+  }
+
+  void loadConstF64(const Stk& src, RegF64 dest) {
     double d;
     src.f64val(&d);
     masm.loadConstantDouble(d, dest);
   }
 
-  void loadMemF64(Stk& src, RegF64 dest) { fr.loadStackF64(src.offs(), dest); }
-
-  void loadLocalF64(Stk& src, RegF64 dest) {
+  void loadMemF64(const Stk& src, RegF64 dest) {
+    fr.loadStackF64(src.offs(), dest);
+  }
+
+  void loadLocalF64(const Stk& src, RegF64 dest) {
     fr.loadLocalF64(localFromSlot(src.slot(), MIRType::Double), dest);
   }
 
-  void loadRegisterF64(Stk& src, RegF64 dest) { moveF64(src.f64reg(), dest); }
-
-  void loadConstF32(Stk& src, RegF32 dest) {
+  void loadRegisterF64(const Stk& src, RegF64 dest) {
+    moveF64(src.f64reg(), dest);
+  }
+
+  void loadConstF32(const Stk& src, RegF32 dest) {
     float f;
     src.f32val(&f);
     masm.loadConstantFloat32(f, dest);
   }
 
-  void loadMemF32(Stk& src, RegF32 dest) { fr.loadStackF32(src.offs(), dest); }
-
-  void loadLocalF32(Stk& src, RegF32 dest) {
+  void loadMemF32(const Stk& src, RegF32 dest) {
+    fr.loadStackF32(src.offs(), dest);
+  }
+
+  void loadLocalF32(const Stk& src, RegF32 dest) {
     fr.loadLocalF32(localFromSlot(src.slot(), MIRType::Float32), dest);
   }
 
-  void loadRegisterF32(Stk& src, RegF32 dest) { moveF32(src.f32reg(), dest); }
-
-  void loadI32(Stk& src, RegI32 dest) {
+  void loadRegisterF32(const Stk& src, RegF32 dest) {
+    moveF32(src.f32reg(), dest);
+  }
+
+  void loadI32(const Stk& src, RegI32 dest) {
     switch (src.kind()) {
       case Stk::ConstI32:
         loadConstI32(src, dest);
         break;
       case Stk::MemI32:
         loadMemI32(src, dest);
         break;
       case Stk::LocalI32:
@@ -2000,17 +2020,17 @@ class BaseCompiler final : public BaseCo
         loadRegisterI32(src, dest);
         break;
       case Stk::None:
       default:
         MOZ_CRASH("Compiler bug: Expected I32 on stack");
     }
   }
 
-  void loadI64(Stk& src, RegI64 dest) {
+  void loadI64(const Stk& src, RegI64 dest) {
     switch (src.kind()) {
       case Stk::ConstI64:
         loadConstI64(src, dest);
         break;
       case Stk::MemI64:
         loadMemI64(src, dest);
         break;
       case Stk::LocalI64:
@@ -2021,17 +2041,17 @@ class BaseCompiler final : public BaseCo
         break;
       case Stk::None:
       default:
         MOZ_CRASH("Compiler bug: Expected I64 on stack");
     }
   }
 
 #if !defined(JS_PUNBOX64)
-  void loadI64Low(Stk& src, RegI32 dest) {
+  void loadI64Low(const Stk& src, RegI32 dest) {
     switch (src.kind()) {
       case Stk::ConstI64:
         moveImm32(int32_t(src.i64val()), dest);
         break;
       case Stk::MemI64:
         fr.loadStackI64Low(src.offs(), dest);
         break;
       case Stk::LocalI64:
@@ -2041,17 +2061,17 @@ class BaseCompiler final : public BaseCo
         moveI32(RegI32(src.i64reg().low), dest);
         break;
       case Stk::None:
       default:
         MOZ_CRASH("Compiler bug: Expected I64 on stack");
     }
   }
 
-  void loadI64High(Stk& src, RegI32 dest) {
+  void loadI64High(const Stk& src, RegI32 dest) {
     switch (src.kind()) {
       case Stk::ConstI64:
         moveImm32(int32_t(src.i64val() >> 32), dest);
         break;
       case Stk::MemI64:
         fr.loadStackI64High(src.offs(), dest);
         break;
       case Stk::LocalI64:
@@ -2062,17 +2082,17 @@ class BaseCompiler final : public BaseCo
         break;
       case Stk::None:
       default:
         MOZ_CRASH("Compiler bug: Expected I64 on stack");
     }
   }
 #endif
 
-  void loadF64(Stk& src, RegF64 dest) {
+  void loadF64(const Stk& src, RegF64 dest) {
     switch (src.kind()) {
       case Stk::ConstF64:
         loadConstF64(src, dest);
         break;
       case Stk::MemF64:
         loadMemF64(src, dest);
         break;
       case Stk::LocalF64:
@@ -2082,17 +2102,17 @@ class BaseCompiler final : public BaseCo
         loadRegisterF64(src, dest);
         break;
       case Stk::None:
       default:
         MOZ_CRASH("Compiler bug: expected F64 on stack");
     }
   }
 
-  void loadF32(Stk& src, RegF32 dest) {
+  void loadF32(const Stk& src, RegF32 dest) {
     switch (src.kind()) {
       case Stk::ConstF32:
         loadConstF32(src, dest);
         break;
       case Stk::MemF32:
         loadMemF32(src, dest);
         break;
       case Stk::LocalF32:
@@ -2308,17 +2328,17 @@ class BaseCompiler final : public BaseCo
   void pushLocalF32(uint32_t slot) {
     Stk& x = push();
     x.setSlot(Stk::LocalF32, slot);
   }
 
   // Call only from other popI32() variants.
   // v must be the stack top.  May pop the CPU stack.
 
-  void popI32(Stk& v, RegI32 dest) {
+  void popI32(const Stk& v, RegI32 dest) {
     MOZ_ASSERT(&v == &stk_.back());
     switch (v.kind()) {
       case Stk::ConstI32:
         loadConstI32(v, dest);
         break;
       case Stk::LocalI32:
         loadLocalI32(v, dest);
         break;
@@ -2356,17 +2376,17 @@ class BaseCompiler final : public BaseCo
 
     stk_.popBack();
     return specific;
   }
 
   // Call only from other popI64() variants.
   // v must be the stack top.  May pop the CPU stack.
 
-  void popI64(Stk& v, RegI64 dest) {
+  void popI64(const Stk& v, RegI64 dest) {
     MOZ_ASSERT(&v == &stk_.back());
     switch (v.kind()) {
       case Stk::ConstI64:
         loadConstI64(v, dest);
         break;
       case Stk::LocalI64:
         loadLocalI64(v, dest);
         break;
@@ -2414,17 +2434,17 @@ class BaseCompiler final : public BaseCo
 
     stk_.popBack();
     return specific;
   }
 
   // Call only from other popF64() variants.
   // v must be the stack top.  May pop the CPU stack.
 
-  void popF64(Stk& v, RegF64 dest) {
+  void popF64(const Stk& v, RegF64 dest) {
     MOZ_ASSERT(&v == &stk_.back());
     switch (v.kind()) {
       case Stk::ConstF64:
         loadConstF64(v, dest);
         break;
       case Stk::LocalF64:
         loadLocalF64(v, dest);
         break;
@@ -2462,17 +2482,17 @@ class BaseCompiler final : public BaseCo
 
     stk_.popBack();
     return specific;
   }
 
   // Call only from other popF32() variants.
   // v must be the stack top.  May pop the CPU stack.
 
-  void popF32(Stk& v, RegF32 dest) {
+  void popF32(const Stk& v, RegF32 dest) {
     MOZ_ASSERT(&v == &stk_.back());
     switch (v.kind()) {
       case Stk::ConstF32:
         loadConstF32(v, dest);
         break;
       case Stk::LocalF32:
         loadLocalF32(v, dest);
         break;
@@ -2871,17 +2891,17 @@ class BaseCompiler final : public BaseCo
         case MIRType::Float32:
           if (i->argInRegister()) fr.storeLocalF32(RegF32(i->fpu()), l);
           break;
         default:
           MOZ_CRASH("Function argument type");
       }
     }
 
-    fr.zeroLocals(ra);
+    fr.zeroLocals(&ra);
 
     if (debugEnabled_) insertBreakablePoint(CallSiteDesc::EnterFrame);
   }
 
   void saveResult() {
     MOZ_ASSERT(debugEnabled_);
     size_t debugFrameOffset = masm.framePushed() - DebugFrame::offsetOfFrame();
     Address resultsAddress(masm.getStackPointer(),
@@ -3053,25 +3073,25 @@ class BaseCompiler final : public BaseCo
 
   template <class T>
   size_t stackArgAreaSize(const T& args) {
     ABIArgIter<const T> i(args);
     while (!i.done()) i++;
     return AlignBytes(i.stackBytesConsumedSoFar(), 16u);
   }
 
-  void startCallArgs(FunctionCall& call, size_t stackArgAreaSize) {
-    call.stackArgAreaSize = stackArgAreaSize;
-
-    size_t adjustment = call.stackArgAreaSize + call.frameAlignAdjustment;
+  void startCallArgs(size_t stackArgAreaSize, FunctionCall* call) {
+    call->stackArgAreaSize = stackArgAreaSize;
+
+    size_t adjustment = call->stackArgAreaSize + call->frameAlignAdjustment;
     fr.allocArgArea(adjustment);
   }
 
-  const ABIArg reservePointerArgument(FunctionCall& call) {
-    return call.abi.next(MIRType::Pointer);
+  const ABIArg reservePointerArgument(FunctionCall* call) {
+    return call->abi.next(MIRType::Pointer);
   }
 
   // TODO / OPTIMIZE (Bug 1316821): Note passArg is used only in one place.
   // (Or it was, until Luke wandered through, but that can be fixed again.)
   // I'm not saying we should manually inline it, but we could hoist the
   // dispatch into the caller and have type-specific implementations of
   // passArg: passArgI32(), etc.  Then those might be inlined, at least in PGO
   // builds.
@@ -3085,32 +3105,32 @@ class BaseCompiler final : public BaseCo
   // argument types (read from the input stream) leads to a cached
   // entry for stackArgAreaSize() and for how to pass arguments...
   //
   // But at least we could reduce the cost of stackArgAreaSize() by
   // first reading the argument types into a (reusable) vector, then
   // we have the outgoing size at low cost, and then we can pass
   // args based on the info we read.
 
-  void passArg(FunctionCall& call, ValType type, Stk& arg) {
+  void passArg(ValType type, const Stk& arg, FunctionCall* call) {
     switch (type) {
       case ValType::I32: {
-        ABIArg argLoc = call.abi.next(MIRType::Int32);
+        ABIArg argLoc = call->abi.next(MIRType::Int32);
         if (argLoc.kind() == ABIArg::Stack) {
           ScratchI32 scratch(*this);
           loadI32(arg, scratch);
           masm.store32(scratch, Address(masm.getStackPointer(),
                                         argLoc.offsetFromArgBase()));
         } else {
           loadI32(arg, RegI32(argLoc.gpr()));
         }
         break;
       }
       case ValType::I64: {
-        ABIArg argLoc = call.abi.next(MIRType::Int64);
+        ABIArg argLoc = call->abi.next(MIRType::Int64);
         if (argLoc.kind() == ABIArg::Stack) {
           ScratchI32 scratch(*this);
 #ifdef JS_PUNBOX64
           loadI64(arg, fromI32(scratch));
           masm.storePtr(scratch, Address(masm.getStackPointer(),
                                          argLoc.offsetFromArgBase()));
 #else
           loadI64Low(arg, scratch);
@@ -3121,17 +3141,17 @@ class BaseCompiler final : public BaseCo
                                                  argLoc.offsetFromArgBase())));
 #endif
         } else {
           loadI64(arg, RegI64(argLoc.gpr64()));
         }
         break;
       }
       case ValType::F64: {
-        ABIArg argLoc = call.abi.next(MIRType::Double);
+        ABIArg argLoc = call->abi.next(MIRType::Double);
         switch (argLoc.kind()) {
           case ABIArg::Stack: {
             ScratchF64 scratch(*this);
             loadF64(arg, scratch);
             masm.storeDouble(scratch, Address(masm.getStackPointer(),
                                               argLoc.offsetFromArgBase()));
             break;
           }
@@ -3162,17 +3182,17 @@ class BaseCompiler final : public BaseCo
             MOZ_CRASH("Unexpected parameter passing discipline");
           }
           case ABIArg::Uninitialized:
             MOZ_CRASH("Uninitialized ABIArg kind");
         }
         break;
       }
       case ValType::F32: {
-        ABIArg argLoc = call.abi.next(MIRType::Float32);
+        ABIArg argLoc = call->abi.next(MIRType::Float32);
         switch (argLoc.kind()) {
           case ABIArg::Stack: {
             ScratchF32 scratch(*this);
             loadF32(arg, scratch);
             masm.storeFloat32(scratch, Address(masm.getStackPointer(),
                                                argLoc.offsetFromArgBase()));
             break;
           }
@@ -3208,17 +3228,17 @@ class BaseCompiler final : public BaseCo
 
   void callSymbolic(SymbolicAddress callee, const FunctionCall& call) {
     CallSiteDesc desc(call.lineOrBytecode, CallSiteDesc::Symbolic);
     masm.call(desc, callee);
   }
 
   // Precondition: sync()
 
-  void callIndirect(uint32_t sigIndex, Stk& indexVal,
+  void callIndirect(uint32_t sigIndex, const Stk& indexVal,
                     const FunctionCall& call) {
     const SigWithId& sig = env_.sigs[sigIndex];
     MOZ_ASSERT(sig.id.kind() != SigIdDesc::Kind::None);
 
     MOZ_ASSERT(env_.tables.length() == 1);
     const TableDesc& table = env_.tables[0];
 
     loadI32(indexVal, RegI32(WasmTableCallIndexReg));
@@ -5085,17 +5105,17 @@ class BaseCompiler final : public BaseCo
   MOZ_MUST_USE bool emitElse();
   MOZ_MUST_USE bool emitEnd();
   MOZ_MUST_USE bool emitBr();
   MOZ_MUST_USE bool emitBrIf();
   MOZ_MUST_USE bool emitBrTable();
   MOZ_MUST_USE bool emitDrop();
   MOZ_MUST_USE bool emitReturn();
   MOZ_MUST_USE bool emitCallArgs(const ValTypeVector& args,
-                                 FunctionCall& baselineCall);
+                                 FunctionCall* baselineCall);
   MOZ_MUST_USE bool emitCall();
   MOZ_MUST_USE bool emitCallIndirect();
   MOZ_MUST_USE bool emitUnaryMathBuiltinCall(SymbolicAddress callee,
                                              ValType operandType);
   MOZ_MUST_USE bool emitGetLocal();
   MOZ_MUST_USE bool emitSetLocal();
   MOZ_MUST_USE bool emitTeeLocal();
   MOZ_MUST_USE bool emitGetGlobal();
@@ -6855,24 +6875,24 @@ bool BaseCompiler::emitReturn() {
 
   doReturn(sig().ret(), PopStack(true));
   deadCode_ = true;
 
   return true;
 }
 
 bool BaseCompiler::emitCallArgs(const ValTypeVector& argTypes,
-                                FunctionCall& baselineCall) {
+                                FunctionCall* baselineCall) {
   MOZ_ASSERT(!deadCode_);
 
-  startCallArgs(baselineCall, stackArgAreaSize(argTypes));
+  startCallArgs(stackArgAreaSize(argTypes), baselineCall);
 
   uint32_t numArgs = argTypes.length();
   for (size_t i = 0; i < numArgs; ++i)
-    passArg(baselineCall, argTypes[i], peek(numArgs - 1 - i));
+    passArg(argTypes[i], peek(numArgs - 1 - i), baselineCall);
 
   masm.loadWasmTlsRegFromFrame();
   return true;
 }
 
 void BaseCompiler::pushReturned(const FunctionCall& call, ExprType type) {
   switch (type) {
     case ExprType::Void:
@@ -6932,17 +6952,18 @@ bool BaseCompiler::emitCall() {
 
   uint32_t numArgs = sig.args().length();
   size_t stackSpace = stackConsumed(numArgs);
 
   FunctionCall baselineCall(lineOrBytecode);
   beginCall(baselineCall, UseABI::Wasm,
             import ? InterModule::True : InterModule::False);
 
-  if (!emitCallArgs(sig.args(), baselineCall)) return false;
+  if (!emitCallArgs(sig.args(), &baselineCall))
+    return false;
 
   if (import)
     callImport(env_.funcImportGlobalDataOffsets[funcIndex], baselineCall);
   else
     callDefinition(funcIndex, baselineCall);
 
   endCall(baselineCall, stackSpace);
 
@@ -6976,17 +6997,18 @@ bool BaseCompiler::emitCallIndirect() {
   // callee if it is on top.  Note this only pops the compiler's stack,
   // not the CPU stack.
 
   Stk callee = stk_.popCopy();
 
   FunctionCall baselineCall(lineOrBytecode);
   beginCall(baselineCall, UseABI::Wasm, InterModule::True);
 
-  if (!emitCallArgs(sig.args(), baselineCall)) return false;
+  if (!emitCallArgs(sig.args(), &baselineCall))
+    return false;
 
   callIndirect(sigIndex, callee, baselineCall);
 
   endCall(baselineCall, stackSpace);
 
   popValueStackBy(numArgs);
 
   if (!IsVoid(sig.ret())) pushReturned(baselineCall, sig.ret());
@@ -7030,17 +7052,18 @@ bool BaseCompiler::emitUnaryMathBuiltinC
   ExprType retType =
       operandType == ValType::F32 ? ExprType::F32 : ExprType::F64;
   uint32_t numArgs = signature.length();
   size_t stackSpace = stackConsumed(numArgs);
 
   FunctionCall baselineCall(lineOrBytecode);
   beginCall(baselineCall, UseABI::System, InterModule::False);
 
-  if (!emitCallArgs(signature, baselineCall)) return false;
+  if (!emitCallArgs(signature, &baselineCall))
+    return false;
 
   builtinCall(callee, baselineCall);
 
   endCall(baselineCall, stackSpace);
 
   popValueStackBy(numArgs);
 
   pushReturned(baselineCall, retType);
@@ -7824,32 +7847,32 @@ void BaseCompiler::emitInstanceCall(uint
   sync();
 
   uint32_t numArgs = sig.length() - 1 /* instance */;
   size_t stackSpace = stackConsumed(numArgs);
 
   FunctionCall baselineCall(lineOrBytecode);
   beginCall(baselineCall, UseABI::System, InterModule::True);
 
-  ABIArg instanceArg = reservePointerArgument(baselineCall);
-
-  startCallArgs(baselineCall, stackArgAreaSize(sig));
+  ABIArg instanceArg = reservePointerArgument(&baselineCall);
+
+  startCallArgs(stackArgAreaSize(sig), &baselineCall);
   for (uint32_t i = 1; i < sig.length(); i++) {
     ValType t;
     switch (sig[i]) {
       case MIRType::Int32:
         t = ValType::I32;
         break;
       case MIRType::Int64:
         t = ValType::I64;
         break;
       default:
         MOZ_CRASH("Unexpected type");
     }
-    passArg(baselineCall, t, peek(numArgs - i));
+    passArg(t, peek(numArgs - i), &baselineCall);
   }
   builtinInstanceMethodCall(builtin, instanceArg, baselineCall);
   endCall(baselineCall, stackSpace);
 
   popValueStackBy(numArgs);
 
   pushReturned(baselineCall, retType);
 }
