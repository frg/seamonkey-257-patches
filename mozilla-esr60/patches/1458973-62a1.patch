# HG changeset patch
# User Tooru Fujisawa <arai_a@mac.com>
# Date 1525758763 -32400
# Node ID 4dc436553912f37245271228859ca880da76b335
# Parent  96a978bae9e965a550d31d208d64b7757d1c0fb6
Bug 1458973 - Rename {Rooted,Handle}ScriptSource to {Rooted,Handle}ScriptSourceObject. r=jimb

diff --git a/js/src/frontend/BytecodeCompiler.cpp b/js/src/frontend/BytecodeCompiler.cpp
--- a/js/src/frontend/BytecodeCompiler.cpp
+++ b/js/src/frontend/BytecodeCompiler.cpp
@@ -82,17 +82,17 @@ class MOZ_STACK_CLASS BytecodeCompiler {
 
   JSContext* cx;
   LifoAlloc& alloc;
   const ReadOnlyCompileOptions& options;
   SourceBufferHolder& sourceBuffer;
 
   RootedScope enclosingScope;
 
-  RootedScriptSource sourceObject;
+  RootedScriptSourceObject sourceObject;
   ScriptSource* scriptSource;
 
   Maybe<UsedNameTracker> usedNames;
   Maybe<Parser<SyntaxParseHandler, char16_t>> syntaxParser;
   Maybe<Parser<FullParseHandler, char16_t>> parser;
 
   Directives directives;
 
@@ -459,17 +459,17 @@ ScriptSourceObject* frontend::CreateScri
     JSContext* cx, const ReadOnlyCompileOptions& options,
     const Maybe<uint32_t>& parameterListEnd /* = Nothing() */) {
   ScriptSource* ss = cx->new_<ScriptSource>();
   if (!ss) return nullptr;
   ScriptSourceHolder ssHolder(ss);
 
   if (!ss->initFromOptions(cx, options, parameterListEnd)) return nullptr;
 
-  RootedScriptSource sso(cx, ScriptSourceObject::create(cx, ss));
+  RootedScriptSourceObject sso(cx, ScriptSourceObject::create(cx, ss));
   if (!sso) return nullptr;
 
   // Off-thread compilations do all their GC heap allocation, including the
   // SSO, in a temporary compartment. Hence, for the SSO to refer to the
   // gc-heap-allocated values in |options|, it would need cross-compartment
   // wrappers from the temporary compartment to the real compartment --- which
   // would then be inappropriate once we merged the temporary and real
   // compartments.
@@ -714,17 +714,17 @@ bool frontend::CompileLazyFunction(JSCon
   if (!parser.checkOptions()) return false;
 
   Rooted<JSFunction*> fun(cx, lazy->functionNonDelazifying());
   ParseNode* pn =
       parser.standaloneLazyFunction(fun, lazy->toStringStart(), lazy->strict(),
                                     lazy->generatorKind(), lazy->asyncKind());
   if (!pn) return false;
 
-  RootedScriptSource sourceObject(cx, lazy->sourceObject());
+  RootedScriptSourceObject sourceObject(cx, lazy->sourceObject());
   MOZ_ASSERT(sourceObject);
 
   Rooted<JSScript*> script(
       cx,
       JSScript::Create(cx, options, sourceObject, lazy->sourceStart(),
                        lazy->sourceEnd(), lazy->toStringStart(),
                        lazy->toStringEnd()));
   if (!script) return false;
diff --git a/js/src/gc/Rooting.h b/js/src/gc/Rooting.h
--- a/js/src/gc/Rooting.h
+++ b/js/src/gc/Rooting.h
@@ -36,17 +36,17 @@ typedef JS::Handle<NativeObject*> Handle
 typedef JS::Handle<Shape*> HandleShape;
 typedef JS::Handle<ObjectGroup*> HandleObjectGroup;
 typedef JS::Handle<JSAtom*> HandleAtom;
 typedef JS::Handle<JSLinearString*> HandleLinearString;
 typedef JS::Handle<PropertyName*> HandlePropertyName;
 typedef JS::Handle<ArrayObject*> HandleArrayObject;
 typedef JS::Handle<PlainObject*> HandlePlainObject;
 typedef JS::Handle<SavedFrame*> HandleSavedFrame;
-typedef JS::Handle<ScriptSourceObject*> HandleScriptSource;
+typedef JS::Handle<ScriptSourceObject*> HandleScriptSourceObject;
 typedef JS::Handle<DebuggerArguments*> HandleDebuggerArguments;
 typedef JS::Handle<DebuggerEnvironment*> HandleDebuggerEnvironment;
 typedef JS::Handle<DebuggerFrame*> HandleDebuggerFrame;
 typedef JS::Handle<DebuggerObject*> HandleDebuggerObject;
 typedef JS::Handle<Scope*> HandleScope;
 
 typedef JS::MutableHandle<Shape*> MutableHandleShape;
 typedef JS::MutableHandle<JSAtom*> MutableHandleAtom;
@@ -65,17 +65,17 @@ typedef JS::Rooted<Shape*> RootedShape;
 typedef JS::Rooted<ObjectGroup*> RootedObjectGroup;
 typedef JS::Rooted<JSAtom*> RootedAtom;
 typedef JS::Rooted<JSLinearString*> RootedLinearString;
 typedef JS::Rooted<PropertyName*> RootedPropertyName;
 typedef JS::Rooted<ArrayObject*> RootedArrayObject;
 typedef JS::Rooted<GlobalObject*> RootedGlobalObject;
 typedef JS::Rooted<PlainObject*> RootedPlainObject;
 typedef JS::Rooted<SavedFrame*> RootedSavedFrame;
-typedef JS::Rooted<ScriptSourceObject*> RootedScriptSource;
+typedef JS::Rooted<ScriptSourceObject*> RootedScriptSourceObject;
 typedef JS::Rooted<DebuggerArguments*> RootedDebuggerArguments;
 typedef JS::Rooted<DebuggerEnvironment*> RootedDebuggerEnvironment;
 typedef JS::Rooted<DebuggerFrame*> RootedDebuggerFrame;
 typedef JS::Rooted<DebuggerObject*> RootedDebuggerObject;
 typedef JS::Rooted<Scope*> RootedScope;
 
 typedef JS::GCVector<JSFunction*> FunctionVector;
 typedef JS::GCVector<PropertyName*> PropertyNameVector;
diff --git a/js/src/jsapi.cpp b/js/src/jsapi.cpp
--- a/js/src/jsapi.cpp
+++ b/js/src/jsapi.cpp
@@ -4040,17 +4040,18 @@ JS_PUBLIC_API bool JS::CompileFunction(J
 
 JS_PUBLIC_API bool JS::InitScriptSourceElement(JSContext* cx,
                                                HandleScript script,
                                                HandleObject element,
                                                HandleString elementAttrName) {
   MOZ_ASSERT(cx);
   MOZ_ASSERT(CurrentThreadCanAccessRuntime(cx->runtime()));
 
-  RootedScriptSource sso(cx, &script->sourceObject()->as<ScriptSourceObject>());
+  RootedScriptSourceObject sso(
+      cx, &script->sourceObject()->as<ScriptSourceObject>());
   return ScriptSourceObject::initElementProperties(cx, sso, element,
                                                    elementAttrName);
 }
 
 JS_PUBLIC_API void JS::ExposeScriptToDebugger(JSContext* cx,
                                               HandleScript script) {
   MOZ_ASSERT(cx);
   MOZ_ASSERT(CurrentThreadCanAccessRuntime(cx->runtime()));
diff --git a/js/src/vm/Debugger.cpp b/js/src/vm/Debugger.cpp
--- a/js/src/vm/Debugger.cpp
+++ b/js/src/vm/Debugger.cpp
@@ -5039,17 +5039,17 @@ class DebuggerScriptGetSourceMatcher {
 
  public:
   DebuggerScriptGetSourceMatcher(JSContext* cx, Debugger* dbg)
       : cx_(cx), dbg_(dbg) {}
 
   using ReturnType = JSObject*;
 
   ReturnType match(HandleScript script) {
-    RootedScriptSource source(
+    RootedScriptSourceObject source(
         cx_,
         &UncheckedUnwrap(script->sourceObject())->as<ScriptSourceObject>());
     return dbg_->wrapSource(cx_, source);
   }
 
   ReturnType match(Handle<WasmInstanceObject*> wasmInstance) {
     return dbg_->wrapWasmSource(cx_, wasmInstance);
   }
@@ -6346,17 +6346,17 @@ void DebuggerSource_trace(JSTracer* trc,
 }
 
 class SetDebuggerSourcePrivateMatcher {
   NativeObject* obj_;
 
  public:
   explicit SetDebuggerSourcePrivateMatcher(NativeObject* obj) : obj_(obj) {}
   using ReturnType = void;
-  ReturnType match(HandleScriptSource source) {
+  ReturnType match(HandleScriptSourceObject source) {
     obj_->setPrivateGCThing(source);
   }
   ReturnType match(Handle<WasmInstanceObject*> instance) {
     obj_->setPrivateGCThing(instance);
   }
 };
 
 NativeObject* Debugger::newDebuggerSource(
@@ -6398,17 +6398,17 @@ JSObject* Debugger::wrapVariantReferent(
     obj = wrapVariantReferent<DebuggerSourceReferent, WasmInstanceObject*,
                               WasmInstanceWeakMap>(cx, wasmInstanceSources, key,
                                                    referent);
   }
   MOZ_ASSERT_IF(obj, GetSourceReferent(obj) == referent);
   return obj;
 }
 
-JSObject* Debugger::wrapSource(JSContext* cx, HandleScriptSource source) {
+JSObject* Debugger::wrapSource(JSContext* cx, HandleScriptSourceObject source) {
   Rooted<DebuggerSourceReferent> referent(cx, source.get());
   return wrapVariantReferent(cx, referent);
 }
 
 JSObject* Debugger::wrapWasmSource(JSContext* cx,
                                    Handle<WasmInstanceObject*> wasmInstance) {
   Rooted<DebuggerSourceReferent> referent(cx, wasmInstance.get());
   return wrapVariantReferent(cx, referent);
@@ -6467,28 +6467,28 @@ static NativeObject* DebuggerSource_chec
   if (!obj) return false;                                                     \
   Rooted<DebuggerSourceReferent> referent(cx, GetSourceReferent(obj))
 
 #define THIS_DEBUGSOURCE_SOURCE(cx, argc, vp, fnname, args, obj, sourceObject) \
   CallArgs args = CallArgsFromVp(argc, vp);                                    \
   RootedNativeObject obj(cx, DebuggerSource_checkThis<ScriptSourceObject*>(    \
                                  cx, args, fnname, "a JS source"));            \
   if (!obj) return false;                                                      \
-  RootedScriptSource sourceObject(                                             \
+  RootedScriptSourceObject sourceObject(                                       \
       cx, GetSourceReferent(obj).as<ScriptSourceObject*>())
 
 class DebuggerSourceGetTextMatcher {
   JSContext* cx_;
 
  public:
   explicit DebuggerSourceGetTextMatcher(JSContext* cx) : cx_(cx) {}
 
   using ReturnType = JSString*;
 
-  ReturnType match(HandleScriptSource sourceObject) {
+  ReturnType match(HandleScriptSourceObject sourceObject) {
     ScriptSource* ss = sourceObject->source();
     bool hasSourceData = ss->hasSourceData();
     if (!ss->hasSourceData() && !JSScript::loadSource(cx_, ss, &hasSourceData))
       return nullptr;
     if (!hasSourceData) return NewStringCopyZ<CanGC>(cx_, "[no source]");
 
     if (ss->isFunctionBody()) return ss->functionBodyString(cx_);
 
@@ -6554,17 +6554,17 @@ static bool DebuggerSource_getBinary(JSC
 class DebuggerSourceGetURLMatcher {
   JSContext* cx_;
 
  public:
   explicit DebuggerSourceGetURLMatcher(JSContext* cx) : cx_(cx) {}
 
   using ReturnType = Maybe<JSString*>;
 
-  ReturnType match(HandleScriptSource sourceObject) {
+  ReturnType match(HandleScriptSourceObject sourceObject) {
     ScriptSource* ss = sourceObject->source();
     MOZ_ASSERT(ss);
     if (ss->filename()) {
       JSString* str = NewStringCopyZ<CanGC>(cx_, ss->filename());
       return Some(str);
     }
     return Nothing();
   }
@@ -6592,17 +6592,17 @@ static bool DebuggerSource_getURL(JSCont
   } else {
     args.rval().setNull();
   }
   return true;
 }
 
 struct DebuggerSourceGetDisplayURLMatcher {
   using ReturnType = const char16_t*;
-  ReturnType match(HandleScriptSource sourceObject) {
+  ReturnType match(HandleScriptSourceObject sourceObject) {
     ScriptSource* ss = sourceObject->source();
     MOZ_ASSERT(ss);
     return ss->hasDisplayURL() ? ss->displayURL() : nullptr;
   }
   ReturnType match(Handle<WasmInstanceObject*> wasmInstance) {
     return wasmInstance->instance().metadata().displayURL();
   }
 };
@@ -6619,17 +6619,17 @@ static bool DebuggerSource_getDisplayURL
   } else {
     args.rval().setNull();
   }
   return true;
 }
 
 struct DebuggerSourceGetElementMatcher {
   using ReturnType = JSObject*;
-  ReturnType match(HandleScriptSource sourceObject) {
+  ReturnType match(HandleScriptSourceObject sourceObject) {
     return sourceObject->element();
   }
   ReturnType match(Handle<WasmInstanceObject*> wasmInstance) { return nullptr; }
 };
 
 static bool DebuggerSource_getElement(JSContext* cx, unsigned argc, Value* vp) {
   THIS_DEBUGSOURCE_REFERENT(cx, argc, vp, "(get element)", args, obj, referent);
 
@@ -6641,17 +6641,17 @@ static bool DebuggerSource_getElement(JS
   } else {
     args.rval().setUndefined();
   }
   return true;
 }
 
 struct DebuggerSourceGetElementPropertyMatcher {
   using ReturnType = Value;
-  ReturnType match(HandleScriptSource sourceObject) {
+  ReturnType match(HandleScriptSourceObject sourceObject) {
     return sourceObject->elementAttributeName();
   }
   ReturnType match(Handle<WasmInstanceObject*> wasmInstance) {
     return UndefinedValue();
   }
 };
 
 static bool DebuggerSource_getElementProperty(JSContext* cx, unsigned argc,
@@ -6670,17 +6670,17 @@ class DebuggerSourceGetIntroductionScrip
 
  public:
   DebuggerSourceGetIntroductionScriptMatcher(JSContext* cx, Debugger* dbg,
                                              MutableHandleValue rval)
       : cx_(cx), dbg_(dbg), rval_(rval) {}
 
   using ReturnType = bool;
 
-  ReturnType match(HandleScriptSource sourceObject) {
+  ReturnType match(HandleScriptSourceObject sourceObject) {
     RootedScript script(cx_, sourceObject->introductionScript());
     if (script) {
       RootedObject scriptDO(cx_, dbg_->wrapScript(cx_, script));
       if (!scriptDO) return false;
       rval_.setObject(*scriptDO);
     } else {
       rval_.setUndefined();
     }
@@ -6701,17 +6701,17 @@ static bool DebuggerSource_getIntroducti
                             referent);
   Debugger* dbg = Debugger::fromChildJSObject(obj);
   DebuggerSourceGetIntroductionScriptMatcher matcher(cx, dbg, args.rval());
   return referent.match(matcher);
 }
 
 struct DebuggerGetIntroductionOffsetMatcher {
   using ReturnType = Value;
-  ReturnType match(HandleScriptSource sourceObject) {
+  ReturnType match(HandleScriptSourceObject sourceObject) {
     // Regardless of what's recorded in the ScriptSourceObject and
     // ScriptSource, only hand out the introduction offset if we also have
     // the script within which it applies.
     ScriptSource* ss = sourceObject->source();
     if (ss->hasIntroductionOffset() && sourceObject->introductionScript())
       return Int32Value(ss->introductionOffset());
     return UndefinedValue();
   }
@@ -6726,17 +6726,17 @@ static bool DebuggerSource_getIntroducti
                             referent);
   DebuggerGetIntroductionOffsetMatcher matcher;
   args.rval().set(referent.match(matcher));
   return true;
 }
 
 struct DebuggerSourceGetIntroductionTypeMatcher {
   using ReturnType = const char*;
-  ReturnType match(HandleScriptSource sourceObject) {
+  ReturnType match(HandleScriptSourceObject sourceObject) {
     ScriptSource* ss = sourceObject->source();
     MOZ_ASSERT(ss);
     return ss->hasIntroductionType() ? ss->introductionType() : nullptr;
   }
   ReturnType match(Handle<WasmInstanceObject*> wasmInstance) { return "wasm"; }
 };
 
 static bool DebuggerSource_getIntroductionType(JSContext* cx, unsigned argc,
@@ -6781,17 +6781,17 @@ class DebuggerSourceGetSourceMapURLMatch
   MutableHandleString result_;
 
  public:
   explicit DebuggerSourceGetSourceMapURLMatcher(JSContext* cx,
                                                 MutableHandleString result)
       : cx_(cx), result_(result) {}
 
   using ReturnType = bool;
-  ReturnType match(HandleScriptSource sourceObject) {
+  ReturnType match(HandleScriptSourceObject sourceObject) {
     ScriptSource* ss = sourceObject->source();
     MOZ_ASSERT(ss);
     if (!ss->hasSourceMapURL()) {
       result_.set(nullptr);
       return true;
     }
     JSString* str = JS_NewUCStringCopyZ(cx_, ss->sourceMapURL());
     if (!str) return false;
diff --git a/js/src/vm/Debugger.h b/js/src/vm/Debugger.h
--- a/js/src/vm/Debugger.h
+++ b/js/src/vm/Debugger.h
@@ -1147,17 +1147,17 @@ class Debugger : private mozilla::Linked
   JSObject* wrapWasmScript(JSContext* cx,
                            Handle<WasmInstanceObject*> wasmInstance);
 
   /*
    * Return the Debugger.Source object for |source|, or create a new one if
    * needed. The context |cx| must be in the debugger compartment; |source|
    * must be a script source object in a debuggee compartment.
    */
-  JSObject* wrapSource(JSContext* cx, js::HandleScriptSource source);
+  JSObject* wrapSource(JSContext* cx, js::HandleScriptSourceObject source);
 
   /*
    * Return the Debugger.Source object for |wasmInstance| (the entire module),
    * synthesizing a new one if needed. The context |cx| must be in the
    * debugger compartment; |wasmInstance| must be a WasmInstanceObject in the
    * debuggee compartment.
    */
   JSObject* wrapWasmSource(JSContext* cx,
diff --git a/js/src/vm/HelperThreads.cpp b/js/src/vm/HelperThreads.cpp
--- a/js/src/vm/HelperThreads.cpp
+++ b/js/src/vm/HelperThreads.cpp
@@ -404,17 +404,17 @@ bool ParseTask::init(JSContext* cx, cons
 }
 
 void ParseTask::activate(JSRuntime* rt) {
   rt->setUsedByHelperThread(parseGlobal->zone());
 }
 
 bool ParseTask::finish(JSContext* cx) {
   for (auto& sourceObject : sourceObjects) {
-    RootedScriptSource sso(cx, sourceObject);
+    RootedScriptSourceObject sso(cx, sourceObject);
     if (!ScriptSourceObject::initFromOptions(cx, sso, options)) return false;
     if (!sso->source()->tryCompressOffThread(cx)) return false;
   }
 
   return true;
 }
 
 ParseTask::~ParseTask() {
diff --git a/js/src/vm/JSFunction.cpp b/js/src/vm/JSFunction.cpp
--- a/js/src/vm/JSFunction.cpp
+++ b/js/src/vm/JSFunction.cpp
@@ -503,17 +503,17 @@ static bool fun_resolve(JSContext* cx, H
   }
 
   return true;
 }
 
 template <XDRMode mode>
 XDRResult js::XDRInterpretedFunction(XDRState<mode>* xdr,
                                      HandleScope enclosingScope,
-                                     HandleScriptSource sourceObject,
+                                     HandleScriptSourceObject sourceObject,
                                      MutableHandleFunction objp) {
   enum FirstWordFlag {
     HasAtom = 0x1,
     HasGeneratorProto = 0x2,
     IsLazy = 0x4,
     HasSingletonType = 0x8
   };
 
@@ -619,21 +619,23 @@ XDRResult js::XDRInterpretedFunction(XDR
   // Required by AutoXDRTree to copy & paste snipet of sub-trees while keeping
   // the alignment.
   MOZ_TRY(xdr->codeAlign(sizeof(js::XDRAlignment)));
 
   return Ok();
 }
 
 template XDRResult js::XDRInterpretedFunction(XDRState<XDR_ENCODE>*,
-                                              HandleScope, HandleScriptSource,
+                                              HandleScope,
+                                              HandleScriptSourceObject,
                                               MutableHandleFunction);
 
 template XDRResult js::XDRInterpretedFunction(XDRState<XDR_DECODE>*,
-                                              HandleScope, HandleScriptSource,
+                                              HandleScope,
+                                              HandleScriptSourceObject,
                                               MutableHandleFunction);
 
 /* ES6 (04-25-16) 19.2.3.6 Function.prototype [ @@hasInstance ] */
 bool js::fun_symbolHasInstance(JSContext* cx, unsigned argc, Value* vp) {
   CallArgs args = CallArgsFromVp(argc, vp);
 
   if (args.length() < 1) {
     args.rval().setBoolean(false);
@@ -779,17 +781,17 @@ static JSObject* CreateFunctionPrototype
   ScriptSource* ss = cx->new_<ScriptSource>();
   if (!ss) return nullptr;
   ScriptSourceHolder ssHolder(ss);
   if (!ss->setSource(cx, mozilla::Move(source), sourceLen)) return nullptr;
 
   CompileOptions options(cx);
   options.setIntroductionType("Function.prototype").setNoScriptRval(true);
   if (!ss->initFromOptions(cx, options)) return nullptr;
-  RootedScriptSource sourceObject(cx, ScriptSourceObject::create(cx, ss));
+  RootedScriptSourceObject sourceObject(cx, ScriptSourceObject::create(cx, ss));
   if (!sourceObject ||
       !ScriptSourceObject::initFromOptions(cx, sourceObject, options))
     return nullptr;
 
   RootedScript script(cx, JSScript::Create(cx, options, sourceObject, begin,
                                            ss->length(), 0, ss->length()));
   if (!script || !JSScript::initFunctionPrototype(cx, script, functionProto))
     return nullptr;
@@ -1484,17 +1486,17 @@ static JSAtom* AppendBoundFunctionPrefix
       // Remember the lazy script on the compiled script, so it can be
       // stored on the function again in case of re-lazification.
       // Only functions without inner functions are re-lazified.
       script->setLazyScript(lazy);
     }
 
     // XDR the newly delazified function.
     if (script->scriptSource()->hasEncoder()) {
-      RootedScriptSource sourceObject(cx, lazy->sourceObject());
+      RootedScriptSourceObject sourceObject(cx, lazy->sourceObject());
       if (!script->scriptSource()->xdrEncodeFunction(cx, fun, sourceObject))
         return false;
     }
 
     return true;
   }
 
   /* Lazily cloned self-hosted script. */
diff --git a/js/src/vm/JSFunction.h b/js/src/vm/JSFunction.h
--- a/js/src/vm/JSFunction.h
+++ b/js/src/vm/JSFunction.h
@@ -948,17 +948,17 @@ inline const js::Value& JSFunction::getE
 
 namespace js {
 
 JSString* FunctionToString(JSContext* cx, HandleFunction fun, bool isToSource);
 
 template <XDRMode mode>
 XDRResult XDRInterpretedFunction(XDRState<mode>* xdr,
                                  HandleScope enclosingScope,
-                                 HandleScriptSource sourceObject,
+                                 HandleScriptSourceObject sourceObject,
                                  MutableHandleFunction objp);
 
 /*
  * Report an error that call.thisv is not compatible with the specified class,
  * assuming that the method (clasp->name).prototype.<name of callee function>
  * is what was called.
  */
 extern void ReportIncompatibleMethod(JSContext* cx, const CallArgs& args,
diff --git a/js/src/vm/JSScript.cpp b/js/src/vm/JSScript.cpp
--- a/js/src/vm/JSScript.cpp
+++ b/js/src/vm/JSScript.cpp
@@ -244,17 +244,17 @@ static XDRResult XDRRelazificationInfo(X
       // relazify scripts with inner functions.  See
       // JSFunction::createScriptForLazilyInterpretedFunction.
       MOZ_ASSERT(lazy->numInnerFunctions() == 0);
     }
 
     MOZ_TRY(xdr->codeUint64(&packedFields));
 
     if (mode == XDR_DECODE) {
-      RootedScriptSource sourceObject(cx, &script->scriptSourceUnwrap());
+      RootedScriptSourceObject sourceObject(cx, &script->scriptSourceUnwrap());
       lazy.set(LazyScript::Create(cx, fun, script, enclosingScope, sourceObject,
                                   packedFields, sourceStart, sourceEnd,
                                   toStringStart, lineno, column));
       if (!lazy)
         return xdr->fail(JS::TranscodeResult_Throw);
 
       lazy->setToStringEnd(toStringEnd);
 
@@ -284,18 +284,18 @@ static inline uint32_t FindScopeIndex(JS
 
   MOZ_CRASH("Scope not found");
 }
 
 enum XDRClassKind { CK_RegexpObject, CK_JSFunction, CK_JSObject };
 
 template <XDRMode mode>
 XDRResult js::XDRScript(XDRState<mode>* xdr, HandleScope scriptEnclosingScope,
-                        HandleScriptSource sourceObjectArg, HandleFunction fun,
-                        MutableHandleScript scriptp) {
+                        HandleScriptSourceObject sourceObjectArg,
+                        HandleFunction fun, MutableHandleScript scriptp) {
   /* NB: Keep this in sync with CopyScript. */
 
   enum ScriptBits {
     NoScriptRval,
     Strict,
     ContainsDynamicNameAccess,
     FunHasExtensibleScope,
     FunHasAnyAliasedFormal,
@@ -431,17 +431,17 @@ XDRResult js::XDRScript(XDRState<mode>* 
   MOZ_TRY(xdr->codeUint32(&ntrynotes));
   MOZ_TRY(xdr->codeUint32(&nscopenotes));
   MOZ_TRY(xdr->codeUint32(&nyieldoffsets));
   MOZ_TRY(xdr->codeUint32(&nTypeSets));
   MOZ_TRY(xdr->codeUint32(&funLength));
   MOZ_TRY(xdr->codeUint32(&scriptBits));
 
   MOZ_ASSERT(!!(scriptBits & (1 << OwnSource)) == !sourceObjectArg);
-  RootedScriptSource sourceObject(cx, sourceObjectArg);
+  RootedScriptSourceObject sourceObject(cx, sourceObjectArg);
 
   if (mode == XDR_DECODE) {
     // When loading from the bytecode cache, we get the CompileOptions from
     // the document. If the noScriptRval or selfHostingMode flag doesn't
     // match, we should fail. This only applies to the top-level and not
     // its inner functions.
     mozilla::Maybe<CompileOptions> options;
     if (xdr->hasOptions() && (scriptBits & (1 << OwnSource))) {
@@ -831,26 +831,27 @@ XDRResult js::XDRScript(XDRState<mode>* 
     /* see BytecodeEmitter::tellDebuggerAboutCompiledScript */
     if (!fun && !cx->helperThread()) Debugger::onNewScript(cx, script);
   }
 
   return Ok();
 }
 
 template XDRResult js::XDRScript(XDRState<XDR_ENCODE>*, HandleScope,
-                                 HandleScriptSource, HandleFunction,
+                                 HandleScriptSourceObject, HandleFunction,
                                  MutableHandleScript);
 
 template XDRResult js::XDRScript(XDRState<XDR_DECODE>*, HandleScope,
-                                 HandleScriptSource, HandleFunction,
+                                 HandleScriptSourceObject, HandleFunction,
                                  MutableHandleScript);
 
 template <XDRMode mode>
 XDRResult js::XDRLazyScript(XDRState<mode>* xdr, HandleScope enclosingScope,
-                            HandleScriptSource sourceObject, HandleFunction fun,
+                            HandleScriptSourceObject sourceObject,
+                            HandleFunction fun,
                             MutableHandle<LazyScript*> lazy) {
   JSContext* cx = xdr->cx();
 
   {
     uint32_t sourceStart;
     uint32_t sourceEnd;
     uint32_t toStringStart;
     uint32_t toStringEnd;
@@ -909,21 +910,21 @@ XDRResult js::XDRLazyScript(XDRState<mod
       if (mode == XDR_DECODE) innerFunctions[i] = func;
     }
   }
 
   return Ok();
 }
 
 template XDRResult js::XDRLazyScript(XDRState<XDR_ENCODE>*, HandleScope,
-                                     HandleScriptSource, HandleFunction,
+                                     HandleScriptSourceObject, HandleFunction,
                                      MutableHandle<LazyScript*>);
 
 template XDRResult js::XDRLazyScript(XDRState<XDR_DECODE>*, HandleScope,
-                                     HandleScriptSource, HandleFunction,
+                                     HandleScriptSourceObject, HandleFunction,
                                      MutableHandle<LazyScript*>);
 
 void JSScript::setSourceObject(JSObject* object) {
   MOZ_ASSERT(compartment() == object->compartment());
   sourceObject_ = object;
 }
 
 void JSScript::setDefaultClassConstructorSpan(JSObject* sourceObject,
@@ -1238,17 +1239,17 @@ static const ClassOps ScriptSourceObject
 const Class ScriptSourceObject::class_ = {
     "ScriptSource",
     JSCLASS_HAS_RESERVED_SLOTS(RESERVED_SLOTS) | JSCLASS_IS_ANONYMOUS |
         JSCLASS_FOREGROUND_FINALIZE,
     &ScriptSourceObjectClassOps};
 
 ScriptSourceObject* ScriptSourceObject::create(JSContext* cx,
                                                ScriptSource* source) {
-  RootedScriptSource sourceObject(
+  RootedScriptSourceObject sourceObject(
       cx, NewObjectWithGivenProto<ScriptSourceObject>(cx, nullptr));
   if (!sourceObject)
     return nullptr;
 
   source->incref();  // The matching decref is in ScriptSourceObject::finalize.
   sourceObject->initReservedSlot(SOURCE_SLOT, PrivateValue(source));
 
   // The remaining slots should eventually be populated by a call to
@@ -1258,17 +1259,17 @@ ScriptSourceObject* ScriptSourceObject::
                                  MagicValue(JS_GENERIC_MAGIC));
   sourceObject->initReservedSlot(INTRODUCTION_SCRIPT_SLOT,
                                  MagicValue(JS_GENERIC_MAGIC));
 
   return sourceObject;
 }
 
 /* static */ bool ScriptSourceObject::initFromOptions(
-    JSContext* cx, HandleScriptSource source,
+    JSContext* cx, HandleScriptSourceObject source,
     const ReadOnlyCompileOptions& options) {
   releaseAssertSameCompartment(cx, source);
   MOZ_ASSERT(source->getReservedSlot(ELEMENT_SLOT).isMagic(JS_GENERIC_MAGIC));
   MOZ_ASSERT(
       source->getReservedSlot(ELEMENT_PROPERTY_SLOT).isMagic(JS_GENERIC_MAGIC));
   MOZ_ASSERT(source->getReservedSlot(INTRODUCTION_SCRIPT_SLOT)
                  .isMagic(JS_GENERIC_MAGIC));
 
@@ -1288,17 +1289,17 @@ ScriptSourceObject* ScriptSourceObject::
     introductionScript.setPrivateGCThing(options.introductionScript());
   }
   source->setReservedSlot(INTRODUCTION_SCRIPT_SLOT, introductionScript);
 
   return true;
 }
 
 /* static */ bool ScriptSourceObject::initElementProperties(
-    JSContext* cx, HandleScriptSource source, HandleObject element,
+    JSContext* cx, HandleScriptSourceObject source, HandleObject element,
     HandleString elementAttrName) {
   RootedValue elementValue(cx, ObjectOrNullValue(element));
   if (!cx->compartment()->wrap(cx, &elementValue)) return false;
 
   RootedValue nameValue(cx);
   if (elementAttrName) nameValue = StringValue(elementAttrName);
   if (!cx->compartment()->wrap(cx, &nameValue)) return false;
 
@@ -1848,17 +1849,17 @@ bool ScriptSource::xdrEncodeTopLevel(JSC
     return false;
   }
 
   failureCase.release();
   return true;
 }
 
 bool ScriptSource::xdrEncodeFunction(JSContext* cx, HandleFunction fun,
-                                     HandleScriptSource sourceObject) {
+                                     HandleScriptSourceObject sourceObject) {
   MOZ_ASSERT(sourceObject->source() == this);
   MOZ_ASSERT(hasEncoder());
   auto failureCase =
       mozilla::MakeScopeExit([&] { xdrEncoder_.reset(nullptr); });
 
   RootedFunction f(cx, fun);
   XDRResult res = xdrEncoder_->codeFunction(&f, sourceObject);
   if (res.isErr()) {
@@ -3904,17 +3905,17 @@ ScriptSource* LazyScript::maybeForwarded
   for (size_t i = 0; i < res->numInnerFunctions(); i++)
     resInnerFunctions[i].init(innerFunctions[i]);
 
   return res;
 }
 
 /* static */ LazyScript* LazyScript::Create(
     JSContext* cx, HandleFunction fun, HandleScript script,
-    HandleScope enclosingScope, HandleScriptSource sourceObject,
+    HandleScope enclosingScope, HandleScriptSourceObject sourceObject,
     uint64_t packedFields, uint32_t sourceStart, uint32_t sourceEnd,
     uint32_t toStringStart, uint32_t lineno, uint32_t column) {
   // Dummy atom which is not a valid property name.
   RootedAtom dummyAtom(cx, cx->names().comma);
 
   // Dummy function which is not a valid function as this is the one which is
   // holding this lazy script.
   HandleFunction dummyFun = fun;
diff --git a/js/src/vm/JSScript.h b/js/src/vm/JSScript.h
--- a/js/src/vm/JSScript.h
+++ b/js/src/vm/JSScript.h
@@ -627,17 +627,17 @@ class ScriptSource {
 
   // Encode a delazified JSFunction.  In case of errors, the XDR encoder is
   // freed and the |buffer| provided as argument to |xdrEncodeTopLevel| is
   // considered undefined.
   //
   // The |sourceObject| argument is the object holding the current
   // ScriptSource.
   bool xdrEncodeFunction(JSContext* cx, HandleFunction fun,
-                         HandleScriptSource sourceObject);
+                         HandleScriptSourceObject sourceObject);
 
   // Linearize the encoded content in the |buffer| provided as argument to
   // |xdrEncodeTopLevel|, and free the XDR encoder.  In case of errors, the
   // |buffer| is considered undefined.
   bool xdrFinalizeEncoder(JS::TranscodeBuffer& buffer);
 
   const mozilla::TimeStamp parseEnded() const { return parseEnded_; }
   // Inform `this` source that it has been fully parsed.
@@ -672,20 +672,21 @@ class ScriptSourceObject : public Native
   static const Class class_;
 
   static void trace(JSTracer* trc, JSObject* obj);
   static void finalize(FreeOp* fop, JSObject* obj);
   static ScriptSourceObject* create(JSContext* cx, ScriptSource* source);
 
   // Initialize those properties of this ScriptSourceObject whose values
   // are provided by |options|, re-wrapping as necessary.
-  static bool initFromOptions(JSContext* cx, HandleScriptSource source,
+  static bool initFromOptions(JSContext* cx, HandleScriptSourceObject source,
                               const ReadOnlyCompileOptions& options);
 
-  static bool initElementProperties(JSContext* cx, HandleScriptSource source,
+  static bool initElementProperties(JSContext* cx,
+                                    HandleScriptSourceObject source,
                                     HandleObject element,
                                     HandleString elementAttrName);
 
   ScriptSource* source() const {
     return static_cast<ScriptSource*>(getReservedSlot(SOURCE_SLOT).toPrivate());
   }
   JSObject* element() const {
     return getReservedSlot(ELEMENT_SLOT).toObjectOrNull();
@@ -713,23 +714,23 @@ enum class FunctionAsyncKind : bool { Sy
 
 /*
  * NB: after a successful XDR_DECODE, XDRScript callers must do any required
  * subsequent set-up of owning function or script object and then call
  * CallNewScriptHook.
  */
 template <XDRMode mode>
 XDRResult XDRScript(XDRState<mode>* xdr, HandleScope enclosingScope,
-                    HandleScriptSource sourceObject, HandleFunction fun,
+                    HandleScriptSourceObject sourceObject, HandleFunction fun,
                     MutableHandleScript scriptp);
 
 template <XDRMode mode>
 XDRResult XDRLazyScript(XDRState<mode>* xdr, HandleScope enclosingScope,
-                        HandleScriptSource sourceObject, HandleFunction fun,
-                        MutableHandle<LazyScript*> lazy);
+                        HandleScriptSourceObject sourceObject,
+                        HandleFunction fun, MutableHandle<LazyScript*> lazy);
 
 /*
  * Code any constant value.
  */
 template <XDRMode mode>
 XDRResult XDRScriptConst(XDRState<mode>* xdr, MutableHandleValue vp);
 
 /*
@@ -816,17 +817,17 @@ extern void SweepScriptData(JSRuntime* r
 extern void FreeScriptData(JSRuntime* rt);
 
 } /* namespace js */
 
 class JSScript : public js::gc::TenuredCell {
   template <js::XDRMode mode>
   friend js::XDRResult js::XDRScript(js::XDRState<mode>* xdr,
                                      js::HandleScope enclosingScope,
-                                     js::HandleScriptSource sourceObject,
+                                     js::HandleScriptSourceObject sourceObject,
                                      js::HandleFunction fun,
                                      js::MutableHandleScript scriptp);
 
   friend bool js::detail::CopyScript(
       JSContext* cx, js::HandleScript src, js::HandleScript dst,
       js::MutableHandle<JS::GCVector<js::Scope*>> scopes);
 
  private:
@@ -2011,17 +2012,17 @@ class LazyScript : public gc::TenuredCel
   //
   // The "script" argument to this function can be null.  If it's non-null,
   // then this LazyScript should be associated with the given JSScript.
   //
   // The sourceObject and enclosingScope arguments may be null if the
   // enclosing function is also lazy.
   static LazyScript* Create(JSContext* cx, HandleFunction fun,
                             HandleScript script, HandleScope enclosingScope,
-                            HandleScriptSource sourceObject,
+                            HandleScriptSourceObject sourceObject,
                             uint64_t packedData, uint32_t begin, uint32_t end,
                             uint32_t toStringStart, uint32_t lineno,
                             uint32_t column);
 
   void initRuntimeFields(uint64_t packedFields);
 
   static inline JSFunction* functionDelazifying(JSContext* cx,
                                                 Handle<LazyScript*>);
diff --git a/js/src/vm/Xdr.cpp b/js/src/vm/Xdr.cpp
--- a/js/src/vm/Xdr.cpp
+++ b/js/src/vm/Xdr.cpp
@@ -109,17 +109,17 @@ static XDRResult VersionCheck(XDRState<m
       return xdr->fail(JS::TranscodeResult_Failure_BadBuildId);
   }
 
   return Ok();
 }
 
 template <XDRMode mode>
 XDRResult XDRState<mode>::codeFunction(MutableHandleFunction funp,
-                                       HandleScriptSource sourceObject) {
+                                       HandleScriptSourceObject sourceObject) {
   TraceLoggerThread* logger = TraceLoggerForCurrentThread(cx());
   TraceLoggerTextId event = mode == XDR_DECODE ? TraceLogger_DecodeFunction
                                                : TraceLogger_EncodeFunction;
   AutoTraceLog tl(logger, event);
 
 #ifdef DEBUG
   auto sanityCheck = mozilla::MakeScopeExit([&] {
     MOZ_ASSERT(validateResultCode(cx(), resultCode()));
diff --git a/js/src/vm/Xdr.h b/js/src/vm/Xdr.h
--- a/js/src/vm/Xdr.h
+++ b/js/src/vm/Xdr.h
@@ -429,17 +429,17 @@ class XDRState : public XDRCoderBase {
     }
     return Ok();
   }
 
   XDRResult codeChars(const JS::Latin1Char* chars, size_t nchars);
   XDRResult codeChars(char16_t* chars, size_t nchars);
 
   XDRResult codeFunction(JS::MutableHandleFunction objp,
-                         HandleScriptSource sourceObject = nullptr);
+                         HandleScriptSourceObject sourceObject = nullptr);
   XDRResult codeScript(MutableHandleScript scriptp);
 };
 
 using XDREncoder = XDRState<XDR_ENCODE>;
 using XDRDecoder = XDRState<XDR_DECODE>;
 
 class XDROffThreadDecoder : public XDRDecoder {
   const ReadOnlyCompileOptions* options_;
