# HG changeset patch
# User Boris Zbarsky <bzbarsky@mit.edu>
# Date 1522974581 14400
# Node ID aec6cc599e20fe4448a46bb80c02dca789eb84b8
# Parent  8fbaa7ae5f516121831d1c13478e9119a00b9a73
Bug 1451516 part 4.  Stop code-generating generic lenient getters.  r=qdot

diff --git a/dom/bindings/BindingUtils.cpp b/dom/bindings/BindingUtils.cpp
--- a/dom/bindings/BindingUtils.cpp
+++ b/dom/bindings/BindingUtils.cpp
@@ -2725,16 +2725,37 @@ struct MaybeGlobalThisPolicy : public No
     return aArgs.thisv().isObject() ?
       &aArgs.thisv().toObject() :
       js::GetGlobalForObjectCrossCompartment(&aArgs.callee());
   }
 
   // We want the HandleInvalidThis of NormalThisPolicy.
 };
 
+// There are some LenientThis things on globals, so we inherit from
+// MaybeGlobalThisPolicy.
+struct LenientThisPolicy : public MaybeGlobalThisPolicy
+{
+  // We want the HasValidThisValue of MaybeGlobalThisPolicy.
+
+  // We want the ExtractThisObject of MaybeGlobalThisPolicy.
+
+  static bool HandleInvalidThis(JSContext* aCx, const JS::CallArgs& aArgs,
+                                bool aSecurityError,
+                                prototypes::ID aProtoId)
+  {
+    MOZ_ASSERT(!JS_IsExceptionPending(aCx));
+    if (!ReportLenientThisUnwrappingFailure(aCx, &aArgs.callee())) {
+      return false;
+    }
+    aArgs.rval().set(JS::UndefinedValue());
+    return true;
+  }
+};
+
 /**
  * An ExceptionPolicy struct provides a single HandleException method which is
  * used to handle an exception, if any.  The method is given the current
  * success/failure boolean so it can decide whether there is in fact an
  * exception involved.
  */
 struct ThrowExceptions {
   // This needs to be inlined because it's called even on no-exceptions
@@ -2806,16 +2827,20 @@ bool GenericGetter(JSContext* cx, unsign
 template bool GenericGetter<NormalThisPolicy, ThrowExceptions>(
     JSContext* cx, unsigned argc, JS::Value* vp);
 template bool GenericGetter<NormalThisPolicy, ConvertExceptionsToPromises>(
     JSContext* cx, unsigned argc, JS::Value* vp);
 template bool GenericGetter<MaybeGlobalThisPolicy, ThrowExceptions>(
     JSContext* cx, unsigned argc, JS::Value* vp);
 template bool GenericGetter<MaybeGlobalThisPolicy, ConvertExceptionsToPromises>(
     JSContext* cx, unsigned argc, JS::Value* vp);
+template bool GenericGetter<LenientThisPolicy, ThrowExceptions>(
+    JSContext* cx, unsigned argc, JS::Value* vp);
+// There aren't any [LenientThis] Promise-returning getters, so don't
+// bother instantiating that specialization.
 
 }  // namespace binding_detail
 
 bool GenericBindingSetter(JSContext* cx, unsigned argc, JS::Value* vp) {
   JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
   const JSJitInfo* info = FUNCTION_VALUE_TO_JITINFO(args.calleev());
   prototypes::ID protoID = static_cast<prototypes::ID>(info->protoID);
   if (!args.thisv().isObject()) {
diff --git a/dom/bindings/BindingUtils.h b/dom/bindings/BindingUtils.h
--- a/dom/bindings/BindingUtils.h
+++ b/dom/bindings/BindingUtils.h
@@ -2823,16 +2823,19 @@ bool GenericGetter(JSContext* cx, unsign
 
 // A this-extraction policy for normal getters/setters/methods.
 struct NormalThisPolicy;
 
 // A this-extraction policy for getters/setters/methods on interfaces
 // that are on some global's proto chain.
 struct MaybeGlobalThisPolicy;
 
+// A this-extraction policy for lenient getters/setters.
+struct LenientThisPolicy;
+
 // An exception-reporting policy for normal getters/setters/methods.
 struct ThrowExceptions;
 
 // An exception-handling policy for Promise-returning getters/methods.
 struct ConvertExceptionsToPromises;
 }  // namespace binding_detail
 
 bool GenericBindingSetter(JSContext* cx, unsigned argc, JS::Value* vp);
diff --git a/dom/bindings/Codegen.py b/dom/bindings/Codegen.py
--- a/dom/bindings/Codegen.py
+++ b/dom/bindings/Codegen.py
@@ -2615,23 +2615,18 @@ class AttrDefiner(PropertyDefiner):
                 jitinfo = "nullptr"
             else:
                 if attr.type.isPromise():
                     exceptionPolicy = "ConvertExceptionsToPromises"
                 else:
                     exceptionPolicy = "ThrowExceptions"
 
                 if attr.hasLenientThis():
-                    if attr.type.isPromise():
-                        raise TypeError("Don't know how to handle "
-                                        "[LenientThis] Promise-returning "
-                                        "attribute %s.%s" %
-                                        (self.descriptor.name,
-                                         attr.identifier.name))
-                    accessor = "genericLenientGetter"
+                    accessor = ("GenericGetter<LenientThisPolicy, %s>" %
+                                exceptionPolicy)
                 elif attr.getExtendedAttribute("CrossOriginReadable"):
                     if attr.type.isPromise():
                         raise TypeError("Don't know how to handle "
                                         "cross-origin Promise-returning "
                                         "attribute %s.%s" %
                                         (self.descriptor.name,
                                          attr.identifier.name))
                     accessor = "genericCrossOriginGetter"
@@ -8979,40 +8974,28 @@ class CGStaticMethod(CGAbstractStaticBin
         CGAbstractStaticBindingMethod.__init__(self, descriptor, name)
 
     def generate_code(self):
         nativeName = CGSpecializedMethod.makeNativeName(self.descriptor,
                                                         self.method)
         return CGMethodCall(nativeName, True, self.descriptor, self.method)
 
 
-class CGGenericGetter(CGAbstractBindingMethod):
+class CGGenericCrossOriginGetter(CGAbstractBindingMethod):
     """
     A class for generating the C++ code for an IDL attribute getter.
     """
-    def __init__(self, descriptor, lenientThis=False, allowCrossOriginThis=False):
-        if lenientThis:
-            name = "genericLenientGetter"
-            unwrapFailureCode = dedent("""
-                MOZ_ASSERT(!JS_IsExceptionPending(cx));
-                if (!ReportLenientThisUnwrappingFailure(cx, &args.callee())) {
-                  return false;
-                }
-                args.rval().set(JS::UndefinedValue());
-                return true;
-                """)
-        else:
-            assert allowCrossOriginThis
-            name = "genericCrossOriginGetter"
-            unwrapFailureCode = (
-                'return ThrowInvalidThis(cx, args, %%(securityError)s, "%s");\n' %
-                descriptor.interface.identifier.name)
+    def __init__(self, descriptor):
+        name = "genericCrossOriginGetter"
+        unwrapFailureCode = (
+            'return ThrowInvalidThis(cx, args, %%(securityError)s, "%s");\n' %
+            descriptor.interface.identifier.name)
         CGAbstractBindingMethod.__init__(self, descriptor, name, JSNativeArguments(),
                                          unwrapFailureCode,
-                                         allowCrossOriginThis=allowCrossOriginThis)
+                                         allowCrossOriginThis=True)
 
     def generate_code(self):
         return CGGeneric(dedent("""
             const JSJitInfo *info = FUNCTION_VALUE_TO_JITINFO(args.calleev());
             MOZ_ASSERT(info->type() == JSJitInfo::Getter);
             JSJitGetterOp getter = info->getter;
             bool ok = getter(cx, obj, self, JSJitGetterCallArgs(args));
             #ifdef DEBUG
@@ -12445,17 +12428,16 @@ def stripTrailingWhitespace(text):
     return '\n'.join(line.rstrip() for line in lines) + tail
 
 
 class MemberProperties:
     def __init__(self):
         self.isGenericMethod = False
         self.isCrossOriginMethod = False
         self.isPromiseReturningMethod = False
-        self.isLenientGetter = False
         self.isCrossOriginGetter = False
         self.isGenericSetter = False
         self.isLenientSetter = False
         self.isCrossOriginSetter = False
         self.isJsonifier = False
 
 
 def memberProperties(m, descriptor):
@@ -12470,19 +12452,17 @@ def memberProperties(m, descriptor):
                     if m.returnsPromise():
                         props.isPromiseReturningMethod = True
                     else:
                         props.isGenericMethod = True
                 if m.getExtendedAttribute("CrossOriginCallable"):
                     props.isCrossOriginMethod = True
     elif m.isAttr():
         if not m.isStatic() and descriptor.interface.hasInterfacePrototypeObject():
-            if m.hasLenientThis():
-                props.isLenientGetter = True
-            elif m.getExtendedAttribute("CrossOriginReadable"):
+            if m.getExtendedAttribute("CrossOriginReadable"):
                 props.isCrossOriginGetter = True
         if not m.readonly:
             if not m.isStatic() and descriptor.interface.hasInterfacePrototypeObject():
                 if m.hasLenientThis():
                     props.isLenientSetter = True
                 elif IsCrossOriginWritable(m, descriptor):
                     props.isCrossOriginSetter = True
                 elif descriptor.needsSpecialGenericOps():
@@ -12514,19 +12494,18 @@ class CGDescriptor(CGThing):
         parent = descriptor.interface.parent
         if parent:
             cgThings.append(CGGeneric("static_assert(IsRefcounted<NativeType>::value == IsRefcounted<%s::NativeType>::value,\n"
                                       "              \"Can't inherit from an interface with a different ownership model.\");\n" %
                                       toBindingNamespace(descriptor.parentPrototypeName)))
 
         # These are set to true if at least one non-static
         # method/getter/setter or jsonifier exist on the interface.
-        (hasMethod, hasLenientGetter, hasSetter, hasLenientSetter,
-            hasPromiseReturningMethod) = (
-                False, False, False, False, False)
+        (hasMethod, hasSetter, hasLenientSetter, hasPromiseReturningMethod) = (
+                False, False, False, False)
         jsonifierMethod = None
         crossOriginMethods, crossOriginGetters, crossOriginSetters = set(), set(), set()
         unscopableNames = list()
         for n in descriptor.interface.namedConstructors:
             cgThings.append(CGClassConstructor(descriptor, n,
                                                NamedConstructorName(n)))
         if (descriptor.interface.hasInterfaceObject() and
             descriptor.interface.hasInterfacePrototypeObject()):
@@ -12601,36 +12580,32 @@ class CGDescriptor(CGThing):
                     descriptor.interface.hasInterfacePrototypeObject()):
                     cgThings.append(CGMemberJITInfo(descriptor, m))
             if m.isConst() and m.type.isPrimitive():
                 cgThings.append(CGConstDefinition(m))
 
             hasMethod = hasMethod or props.isGenericMethod
             hasPromiseReturningMethod = (hasPromiseReturningMethod or
                                          props.isPromiseReturningMethod)
-            hasLenientGetter = hasLenientGetter or props.isLenientGetter
             hasSetter = hasSetter or props.isGenericSetter
             hasLenientSetter = hasLenientSetter or props.isLenientSetter
 
         if jsonifierMethod:
             cgThings.append(CGJsonifyAttributesMethod(descriptor))
             cgThings.append(CGJsonifierMethod(descriptor, jsonifierMethod))
             cgThings.append(CGMemberJITInfo(descriptor, jsonifierMethod))
         if hasMethod:
             cgThings.append(CGGenericMethod(descriptor))
         if hasPromiseReturningMethod:
             cgThings.append(CGGenericPromiseReturningMethod(descriptor))
         if len(crossOriginMethods):
             cgThings.append(CGGenericMethod(descriptor,
                                             allowCrossOriginThis=True))
-        if hasLenientGetter:
-            cgThings.append(CGGenericGetter(descriptor, lenientThis=True))
         if len(crossOriginGetters):
-            cgThings.append(CGGenericGetter(descriptor,
-                                            allowCrossOriginThis=True))
+            cgThings.append(CGGenericCrossOriginGetter(descriptor))
         if hasSetter:
             cgThings.append(CGGenericSetter(descriptor))
         if hasLenientSetter:
             cgThings.append(CGGenericSetter(descriptor, lenientThis=True))
         if len(crossOriginSetters):
             cgThings.append(CGGenericSetter(descriptor,
                                             allowCrossOriginThis=True))
 
