# HG changeset patch
# User Magnus Melin <mkmelin+mozilla@iki.fi>
# Date 1574326944 -7200
# Node ID 42ac954abcd09689cd9325c02dbda168baab96f6
# Parent  bd70068e556cd03385f47e98cdcd79ea62c1ab94
Bug 1597933 - use fetch + URLSearchParms instead of Http.jsm to request OAuth2 access token. r=Fallen

diff --git a/mailnews/base/util/OAuth2.jsm b/mailnews/base/util/OAuth2.jsm
--- a/mailnews/base/util/OAuth2.jsm
+++ b/mailnews/base/util/OAuth2.jsm
@@ -3,21 +3,22 @@
  * You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 /**
  * Provides OAuth 2.0 authentication.
  * @see RFC 6749
  */
 var EXPORTED_SYMBOLS = ["OAuth2"];
 
-ChromeUtils.import("resource://gre/modules/Http.jsm");
 ChromeUtils.import("resource://gre/modules/Services.jsm");
 ChromeUtils.import("resource://gre/modules/XPCOMUtils.jsm");
 ChromeUtils.import("resource:///modules/gloda/log4moz.js");
 
+Cu.importGlobalProperties(["fetch"]);
+
 // Only allow one connecting window per endpoint.
 var gConnecting = {};
 
 function OAuth2(aBaseURI, aScope, aAppKey, aAppSecret) {
     this.authURI = aBaseURI + "oauth2/auth";
     this.tokenURI = aBaseURI + "oauth2/token";
     this.consumerKey = aAppKey;
     this.consumerSecret = aAppSecret;
@@ -165,62 +166,62 @@ OAuth2.prototype = {
         this.onAuthorizationFailed(null, aURL);
       }
     },
 
     onAuthorizationFailed: function(aError, aData) {
         this.connectFailureCallback(aData);
     },
 
-    /**
-     * Request a new access token, or refresh an existing one.
-     * @param {string} aCode - The token issued to the client.
-     * @param {boolean} aRefresh - Whether it's a refresh of a token or not.
-     */
-    requestAccessToken(aCode, aRefresh) {
-      // @see RFC 6749 section 4.1.3. Access Token Request
-      // @see RFC 6749 section 6. Refreshing an Access Token
-      let params = [
-        ["client_id", this.consumerKey],
-        ["client_secret", this.consumerSecret],
-      ];
+  /**
+   * Request a new access token, or refresh an existing one.
+   * @param {string} aCode - The token issued to the client.
+   * @param {boolean} aRefresh - Whether it's a refresh of a token or not.
+   */
+  requestAccessToken(aCode, aRefresh) {
+    // @see RFC 6749 section 4.1.3. Access Token Request
+    // @see RFC 6749 section 6. Refreshing an Access Token
+
+    let data = new URLSearchParams();
+    data.append("client_id", this.consumerKey);
+    data.append("client_secret", this.consumerSecret);
 
-      if (aRefresh) {
-        params.push(["grant_type", "refresh_token"]);
-        params.push(["refresh_token", aCode]);
-      } else {
-        params.push(["grant_type", "authorization_code"]);
-        params.push(["code", aCode]);
-        params.push(["redirect_uri", this.completionURI]);
-      }
+    if (aRefresh) {
+      data.append("grant_type", "refresh_token");
+      data.append("refresh_token", aCode);
+    } else {
+      data.append("grant_type", "authorization_code");
+      data.append("code", aCode);
+      data.append("redirect_uri", this.completionURI);
+    }
 
-      let options = {
-        postData: params,
-        onLoad: this.onAccessTokenReceived.bind(this),
-        onError: this.onAccessTokenFailed.bind(this)
-      }
-      httpRequest(this.tokenURI, options);
-    },
-
-    onAccessTokenFailed: function onAccessTokenFailed(aError, aData) {
-        if (aError != "offline") {
-            this.refreshToken = null;
-        }
-        this.connectFailureCallback(aData);
-    },
-
-    onAccessTokenReceived: function onRequestTokenReceived(aData) {
-        let result = JSON.parse(aData);
-
+    this.log.info(
+      `Making access token request to the token endpoint: ${this.tokenURI}`
+    );
+    fetch(this.tokenURI, {
+      method: "POST",
+      cache: "no-cache",
+      body: data,
+    })
+      .then(response => response.json())
+      .then(result => {
+        this.log.info("The authorization server issued an access token.");
         this.accessToken = result.access_token;
         if ("refresh_token" in result) {
-            this.refreshToken = result.refresh_token;
+          this.refreshToken = result.refresh_token;
         }
         if ("expires_in" in result) {
-            this.tokenExpires = (new Date()).getTime() + (result.expires_in * 1000);
+          this.tokenExpires = new Date().getTime() + result.expires_in * 1000;
         } else {
-            this.tokenExpires = Number.MAX_VALUE;
+          this.tokenExpires = Number.MAX_VALUE;
         }
         this.tokenType = result.token_type;
-
         this.connectSuccessCallback();
-    }
+      })
+      .catch(err => {
+        // Getting an access token failed.
+        this.log.info(
+          `The authorization server returned an error response: ${err}`
+        );
+        this.connectFailureCallback(err);
+      });
+  },
 };
