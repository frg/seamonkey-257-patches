# HG changeset patch
# User Ian Neal <iann_cvs@blueyonder.co.uk>
# Date 1581955595 0
# Parent  b4dc712b572900a8ffcc1d8ca93413e495cc2206
Bug 1616035 - Port |Bug 491577 - add API for deleting a single closed window| and |Bug 415941 – nsISessionStore documentation: clarify the meaning of "window"| to SeaMonkey. r=frg a=frg

diff --git a/suite/components/sessionstore/nsISessionStore.idl b/suite/components/sessionstore/nsISessionStore.idl
--- a/suite/components/sessionstore/nsISessionStore.idl
+++ b/suite/components/sessionstore/nsISessionStore.idl
@@ -5,20 +5,32 @@
 #include "nsISupports.idl"
 
 interface nsIDOMWindow;
 interface nsIDOMNode;
 
 /**
  * nsISessionStore keeps track of the current browsing state - i.e.
  * tab history, cookies, scroll state, form data, POSTDATA and window features
- * - and allows to restore everything into one window.
+ * - and allows to restore everything into one browser window.
+ *
+ * The nsISessionStore API operates mostly on browser windows and the tabbrowser
+ * tabs contained in them:
+ *
+ * * "Browser windows" are those DOM windows having loaded
+ * chrome://navigator/content/navigator.xul . From overlays you can just pass
+ * the global |window| object to the API, though (or |top| from a sidebar).
+ * From elsewhere you can get browser windows through the nsIWindowMediator
+ * by looking for "navigator:browser" windows.
+ *
+ * * "Tabbrowser tabs" are all the child nodes of a browser window's
+ * |getBrowser().tabContainer| such as e.g. |getBrowser().selectedTab|.
  */
 
-[scriptable, uuid(a2f14785-c4d4-4bac-b048-a849d2e74513)]
+[scriptable, uuid(27a8bd2b-dd76-4cee-82eb-a25f6a94478f)]
 interface nsISessionStore : nsISupports
 {
   /**
    * Initialize the service
    */
   void init(in nsIDOMWindow aWindow);
 
   /**
@@ -36,145 +48,98 @@ interface nsISessionStore : nsISupports
    *
    * Note: This will throw if there is no previous state to restore. Check with
    * canRestoreLastSession first to avoid thrown errors.
    */
   void restoreLastSession();
 
   /**
    * Get the current browsing state.
-   * @return a JSON string representing the session state.
+   * @returns a JSON string representing the session state.
    */
   AString getBrowserState();
 
   /**
    * Set the browsing state.
    * This will immediately restore the state of the whole application to the state
    * passed in, *replacing* the current session.
    *
    * @param aState is a JSON string representing the session state.
    */
   void setBrowserState(in AString aState);
 
   /**
-   * @param aWindow is the window whose state is to be returned.
+   * @param aWindow is the browser window whose state is to be returned.
    *
-   * @return a JSON string representing a session state with only one window.
+   * @returns a JSON string representing a session state with only one window.
    */
   AString getWindowState(in nsIDOMWindow aWindow);
 
   /**
-   * @param aWindow    is the window whose state is to be set.
+   * @param aWindow    is the browser window whose state is to be set.
    * @param aState     is a JSON string representing a session state.
    * @param aOverwrite boolean overwrite existing tabs
    */
   void setWindowState(in nsIDOMWindow aWindow, in AString aState, in boolean aOverwrite);
 
   /**
-   * @param aTab is the tab whose state is to be returned.
+   * @param aTab is the tabbrowser tab whose state is to be returned.
    *
-   * @return a JSON string representing the state of the tab
+   * @returns a JSON string representing the state of the tab
    *         (note: doesn't contain cookies - if you need them, use getWindowState instead).
    */
   AString getTabState(in nsIDOMNode aTab);
 
   /**
-   * @param aTab   is the tab whose state is to be set.
+   * @param aTab   is the tabbrowser tab whose state is to be set.
    * @param aState is a JSON string representing a session state.
    */
   void setTabState(in nsIDOMNode aTab, in AString aState);
 
   /**
    * Duplicates a given tab as thoroughly as possible.
    *
-   * @param aWindow  is the window into which the tab will be duplicated.
+   * @param aWindow  is the browser window into which the tab will be duplicated.
    *                 Pass null if you want to create a new window.
-   * @param aTab     is the tab to duplicate (can be from a different window).
+   * @param aTab     is the tabbrowser tab to duplicate (can be from a different window).
    * @param aDelta   is the offset to the history entry that you want to load.
    * @param aRelated is a flag to be passed to addTab().
-   * @return a reference to the newly created tab, or null if opening a window.
+   * @returns a reference to the newly created tab, or null if opening a window.
    */
   nsIDOMNode duplicateTab(in nsIDOMWindow aWindow, in nsIDOMNode aTab,
                           [optional] in long aDelta,
                           [optional] in boolean aRelated);
 
   /**
-   * Get the number of restore-able tabs for a window
+   * Get the number of restore-able tabs for a browser window
    */
   unsigned long getClosedTabCount(in nsIDOMWindow aWindow);
 
   /**
    * Get closed tab data
-   * @return a JSON string representing the list of closed tabs.
+   *
+   * @param aWindow is the browser window for which to get closed tab data
+   * @returns a JSON string representing the list of closed tabs.
    */
   AString getClosedTabData(in nsIDOMWindow aWindow);
 
   /**
-   * @param aWindow
-   *          The window to reopen a closed tab in.
-   * @param aIndex
-   *          Indicates the window to be restored (FIFO ordered).
+   * @param aWindow is the browser window to reopen a closed tab in.
+   * @param aIndex  is the index of the tab to be restored (FIFO ordered).
    * @returns a reference to the reopened tab.
    */
   nsIDOMNode undoCloseTab(in nsIDOMWindow aWindow, in unsigned long aIndex);
 
   /**
    * @param aWindow is the browser window associated with the closed tab.
    * @param aIndex  is the index of the closed tab to be removed (FIFO ordered).
    */
   nsIDOMNode forgetClosedTab(in nsIDOMWindow aWindow, in unsigned long aIndex);
 
   /**
-   * @param aWindow is the window to get the value for.
-   * @param aKey    is the value's name.
-   *
-   * @return A string value or "" if none is set.
-   */
-  AString getWindowValue(in nsIDOMWindow aWindow, in AString aKey);
-
-  /**
-   * @param aWindow      is the window to set the value for.
-   * @param aKey         is the value's name.
-   * @param aStringValue is the value itself (use toSource/eval before setting JS objects).
-   */
-  void setWindowValue(in nsIDOMWindow aWindow, in AString aKey, in AString aStringValue);
-
-  /**
-   * @param aWindow is the window to get the value for.
-   * @param aKey    is the value's name.
-   */
-  void deleteWindowValue(in nsIDOMWindow aWindow, in AString aKey);
-
-  /**
-   * @param aTab is the tab to get the value for.
-   * @param aKey is the value's name.
-   *
-   * @return A string value or "" if none is set.
-   */
-  AString getTabValue(in nsIDOMNode aTab, in AString aKey);
-
-  /**
-   * @param aTab         is the tab to set the value for.
-   * @param aKey         is the value's name.
-   * @param aStringValue is the value itself (use toSource/eval before setting JS objects).
-   */
-  void setTabValue(in nsIDOMNode aTab, in AString aKey, in AString aStringValue);
-
-  /**
-   * @param aTab is the tab to get the value for.
-   * @param aKey is the value's name.
-   */
-  void deleteTabValue(in nsIDOMNode aTab, in AString aKey);
-
-  /**
-   * @param aName is the name of the attribute to save/restore for all xul:tabs.
-   */
-  void persistTabAttribute(in AString aName);
-
-  /**
    * Get the number of restore-able windows
    */
   unsigned long getClosedWindowCount();
 
   /**
    * Get closed windows data
    *
    * @returns a JSON string representing the list of closed windows.
@@ -183,14 +148,69 @@ interface nsISessionStore : nsISupports
 
   /**
    * @param aIndex is the index of the windows to be restored (FIFO ordered).
    * @returns the nsIDOMWindow object of the reopened window
    */
   nsIDOMWindow undoCloseWindow(in unsigned long aIndex);
 
   /**
+   * @param aIndex  is the index of the closed window to be removed (FIFO ordered).
+   *
+   * @throws NS_ERROR_INVALID_ARG
+   *   when aIndex does not map to a closed window
+   */
+  nsIDOMNode forgetClosedWindow(in unsigned long aIndex);
+
+  /**
+   * @param aWindow is the window to get the value for.
+   * @param aKey    is the value's name.
+   *
+   * @returns A string value or an empty string if none is set.
+   */
+  AString getWindowValue(in nsIDOMWindow aWindow, in AString aKey);
+
+  /**
+   * @param aWindow      is the browser window to set the value for.
+   * @param aKey         is the value's name.
+   * @param aStringValue is the value itself (use toSource/eval before setting JS objects).
+   */
+  void setWindowValue(in nsIDOMWindow aWindow, in AString aKey, in AString aStringValue);
+
+  /**
+   * @param aWindow is the browser window to get the value for.
+   * @param aKey    is the value's name.
+   */
+  void deleteWindowValue(in nsIDOMWindow aWindow, in AString aKey);
+
+  /**
+   * @param aTab is the tabbrowser tab to get the value for.
+   * @param aKey is the value's name.
+   *
+   * @returns A string value or an empty string if none is set.
+   */
+  AString getTabValue(in nsIDOMNode aTab, in AString aKey);
+
+  /**
+   * @param aTab         is the tabbrowser tab to set the value for.
+   * @param aKey         is the value's name.
+   * @param aStringValue is the value itself (use toSource/eval before setting JS objects).
+   */
+  void setTabValue(in nsIDOMNode aTab, in AString aKey, in AString aStringValue);
+
+  /**
+   * @param aTab is the tabbrowser tab to get the value for.
+   * @param aKey is the value's name.
+   */
+  void deleteTabValue(in nsIDOMNode aTab, in AString aKey);
+
+  /**
+   * @param aName is the name of the attribute to save/restore for all tabbrowser tabs.
+   */
+  void persistTabAttribute(in AString aName);
+
+  /**
    * Returns true if the last window was closed and should be restored
    *
    * @returns true if the last window was closed and should be restored
    */
   boolean doRestoreLastWindow();
 };
diff --git a/suite/components/sessionstore/nsSessionStore.js b/suite/components/sessionstore/nsSessionStore.js
--- a/suite/components/sessionstore/nsSessionStore.js
+++ b/suite/components/sessionstore/nsSessionStore.js
@@ -1115,16 +1115,26 @@ SessionStoreService.prototype = {
 
     // reopen the window
     let state = { windows: this._closedWindows.splice(aIndex, 1) };
     let window = this._openWindowWithState(state);
     this.windowToFocus = window;
     return window;
   },
 
+  forgetClosedWindow: function sss_forgetClosedWindow(aIndex) {
+    // default to the most-recently closed window
+    aIndex = aIndex || 0;
+    if (!(aIndex in this._closedWindows))
+      throw (Components.returnCode = Cr.NS_ERROR_INVALID_ARG);
+
+    // remove closed window from the array
+    this._closedWindows.splice(aIndex, 1);
+  },
+
   getWindowValue: function sss_getWindowValue(aWindow, aKey) {
     if ("__SSi" in aWindow) {
       var data = this._windows[aWindow.__SSi].extData || {};
       return data[aKey] || "";
     }
     if (DyingWindowCache.has(aWindow)) {
       let data = DyingWindowCache.get(aWindow).extData || {};
       return data[aKey] || "";
diff --git a/suite/components/tests/browser/browser.ini b/suite/components/tests/browser/browser.ini
--- a/suite/components/tests/browser/browser.ini
+++ b/suite/components/tests/browser/browser.ini
@@ -32,16 +32,17 @@ support-files = browser_463206_sample.ht
 support-files = browser_466937_sample.html
 [browser_477657.js]
 [browser_480893.js]
 [browser_483330.js]
 [browser_485482.js]
 support-files = browser_485482_sample.html
 [browser_490040.js]
 [browser_491168.js]
+[browser_491577.js]
 [browser_493467.js]
 [browser_500328.js]
 [browser_514751.js]
 [browser_522545.js]
 [browser_524745.js]
 [browser_526613.js]
 [browser_528776.js]
 [browser_581937.js]
diff --git a/suite/components/tests/browser/browser_491577.js b/suite/components/tests/browser/browser_491577.js
new file mode 100644
--- /dev/null
+++ b/suite/components/tests/browser/browser_491577.js
@@ -0,0 +1,119 @@
+/* This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+function test() {
+  /** Test for Bug 491577 **/
+
+  // test setup
+  waitForExplicitFinish();
+
+  const REMEMBER = Date.now(), FORGET = Math.random();
+  let test_state = {
+    windows: [ { tabs: [{ entries: [{ url: "http://example.com/", triggeringPrincipal_base64 }] }], selected: 1 } ],
+    _closedWindows: [
+      // _closedWindows[0]
+      {
+        tabs: [
+          { entries: [{ url: "http://example.com/", triggeringPrincipal_base64, title: "title" }] },
+          { entries: [{ url: "http://mozilla.org/", triggeringPrincipal_base64, title: "title" }] }
+        ],
+        selected: 2,
+        title: FORGET,
+        _closedTabs: []
+      },
+      // _closedWindows[1]
+      {
+        tabs: [
+         { entries: [{ url: "http://mozilla.org/", triggeringPrincipal_base64, title: "title" }] },
+         { entries: [{ url: "http://example.com/", triggeringPrincipal_base64, title: "title" }] },
+         { entries: [{ url: "http://mozilla.org/", triggeringPrincipal_base64, title: "title" }] },
+        ],
+        selected: 3,
+        title: REMEMBER,
+        _closedTabs: []
+      },
+      // _closedWindows[2]
+      {
+        tabs: [
+          { entries: [{ url: "http://example.com/", triggeringPrincipal_base64, title: "title" }] }
+        ],
+        selected: 1,
+        title: FORGET,
+        _closedTabs: [
+          {
+            state: {
+              entries: [
+                { url: "http://mozilla.org/", triggeringPrincipal_base64, title: "title" },
+                { url: "http://mozilla.org/again", triggeringPrincipal_base64, title: "title" }
+              ]
+            },
+            pos: 1,
+            title: "title"
+          },
+          {
+            state: {
+              entries: [
+                { url: "http://example.com", triggeringPrincipal_base64, title: "title" }
+              ]
+            },
+            title: "title"
+          }
+        ]
+      }
+    ]
+  };
+  let remember_count = 1;
+
+  function countByTitle(aClosedWindowList, aTitle) {
+    return aClosedWindowList.filter(aData => aData.title == aTitle).length;
+  }
+
+  function testForError(aFunction) {
+    try {
+      aFunction();
+      return false;
+    } catch (ex) {
+      return ex.name == "NS_ERROR_ILLEGAL_VALUE";
+    }
+  }
+
+  // open a window and add the above closed window list
+  let newWin = openDialog(location, "_blank", "chrome,all,dialog=no");
+  promiseWindowLoaded(newWin).then(() => {
+    gPrefService.setIntPref("browser.sessionstore.max_windows_undo",
+                            test_state._closedWindows.length);
+    ss.setWindowState(newWin, JSON.stringify(test_state), true);
+
+    let closedWindows = JSON.parse(ss.getClosedWindowData());
+    is(closedWindows.length, test_state._closedWindows.length,
+       "Closed window list has the expected length");
+    is(countByTitle(closedWindows, FORGET),
+       test_state._closedWindows.length - remember_count,
+       "The correct amount of windows are to be forgotten");
+    is(countByTitle(closedWindows, REMEMBER), remember_count,
+       "Everything is set up.");
+
+    // all of the following calls with illegal arguments should throw NS_ERROR_ILLEGAL_VALUE
+    ok(testForError(() => ss.forgetClosedWindow(-1)),
+       "Invalid window for forgetClosedWindow throws");
+    ok(testForError(() => ss.forgetClosedWindow(test_state._closedWindows.length + 1)),
+       "Invalid window for forgetClosedWindow throws");
+
+    // Remove third window, then first window
+    ss.forgetClosedWindow(2);
+    ss.forgetClosedWindow(null);
+
+    closedWindows = JSON.parse(ss.getClosedWindowData());
+    is(closedWindows.length, remember_count,
+       "The correct amount of windows were removed");
+    is(countByTitle(closedWindows, FORGET), 0,
+       "All windows specifically forgotten were indeed removed");
+    is(countByTitle(closedWindows, REMEMBER), remember_count,
+       "... and windows not specifically forgetten weren't.");
+
+    // clean up
+    gPrefService.clearUserPref("browser.sessionstore.max_windows_undo");
+    BrowserTestUtils.closeWindow(newWin).then(finish);
+  });
+}
